﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void OldGUIExamplesCS::Start()
extern void OldGUIExamplesCS_Start_mC33821BD3ADFD74B0B1BDD411FE35F29173AAE16 (void);
// 0x00000002 System.Void OldGUIExamplesCS::catMoved()
extern void OldGUIExamplesCS_catMoved_m0AFE2D767EEFFE2B23F700545EDCADBA4FDE4495 (void);
// 0x00000003 System.Void OldGUIExamplesCS::OnGUI()
extern void OldGUIExamplesCS_OnGUI_mFA5EA2B56824C4712729521C4545139AB167C4AA (void);
// 0x00000004 System.Void OldGUIExamplesCS::.ctor()
extern void OldGUIExamplesCS__ctor_m805206E5EF03EF6452AC2A3979FD378F1CB86998 (void);
// 0x00000005 System.Void TestingPunch::Start()
extern void TestingPunch_Start_m51A034719AB8587292861C585C70970EDEBE8877 (void);
// 0x00000006 System.Void TestingPunch::Update()
extern void TestingPunch_Update_m953417D5B76254AF9BF4E2BA996801A997F9D6D6 (void);
// 0x00000007 System.Void TestingPunch::tweenStatically(UnityEngine.GameObject)
extern void TestingPunch_tweenStatically_m0E13525CEE807E5FA77B94242B182868B1D129A0 (void);
// 0x00000008 System.Void TestingPunch::enterMiniGameStart(System.Object)
extern void TestingPunch_enterMiniGameStart_m017AD4178C7F47FB535AFCA0B39AB97A58F0D9BE (void);
// 0x00000009 System.Void TestingPunch::updateColor(UnityEngine.Color)
extern void TestingPunch_updateColor_mB0688DDA58E8C0CBA76525C4B31BD18EB3A2E4FC (void);
// 0x0000000A System.Void TestingPunch::delayedMethod(System.Object)
extern void TestingPunch_delayedMethod_mC7F5E34BF17C44BC3B311081B8ED1897F1CC08C3 (void);
// 0x0000000B System.Void TestingPunch::destroyOnComp(System.Object)
extern void TestingPunch_destroyOnComp_mB95F605847D2D64B1F4BF239600FAF788917BF44 (void);
// 0x0000000C System.String TestingPunch::curveToString(UnityEngine.AnimationCurve)
extern void TestingPunch_curveToString_mA840114D184FEA8169EE6B379C4513E282E09511 (void);
// 0x0000000D System.Void TestingPunch::.ctor()
extern void TestingPunch__ctor_m8AA41432E6E7079F6B683958FECD329615A21169 (void);
// 0x0000000E System.Void TestingPunch::<Update>b__4_0()
extern void TestingPunch_U3CUpdateU3Eb__4_0_m573E0207272AF5B03F77EBE674FC9B7C0B49791D (void);
// 0x0000000F System.Void TestingPunch::<Update>b__4_3()
extern void TestingPunch_U3CUpdateU3Eb__4_3_m4CC34D2970098316C32EBE67B368AB199A20A2E1 (void);
// 0x00000010 System.Void TestingPunch::<Update>b__4_7(UnityEngine.Vector2)
extern void TestingPunch_U3CUpdateU3Eb__4_7_m2E2AD49C20FF476ED389803BE00C0C87495D0DFC (void);
// 0x00000011 System.Void TestingPunch/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m6EF45FEFD275B14E2DFC1358F9AF89B57EF810AC (void);
// 0x00000012 System.Void TestingPunch/<>c__DisplayClass4_0::<Update>b__2(System.Single)
extern void U3CU3Ec__DisplayClass4_0_U3CUpdateU3Eb__2_m59E4E1443D5262BE2A23D2C28AB670EA4071ABF4 (void);
// 0x00000013 System.Void TestingPunch/<>c__DisplayClass4_1::.ctor()
extern void U3CU3Ec__DisplayClass4_1__ctor_mB4665A6E7864E352E8943BE5EFEDF0E53D1685AC (void);
// 0x00000014 System.Void TestingPunch/<>c__DisplayClass4_1::<Update>b__6()
extern void U3CU3Ec__DisplayClass4_1_U3CUpdateU3Eb__6_m954F24C94FA12A5815D1FED30C2506F75E8628F2 (void);
// 0x00000015 System.Void TestingPunch/<>c__DisplayClass4_2::.ctor()
extern void U3CU3Ec__DisplayClass4_2__ctor_m1584CFD1510446CA5FCCF444CC4C3A8416419C58 (void);
// 0x00000016 System.Void TestingPunch/<>c__DisplayClass4_2::<Update>b__8(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass4_2_U3CUpdateU3Eb__8_m06A77409E2EEAED1E858D86BDDD2CC58DE2CE83A (void);
// 0x00000017 System.Void TestingPunch/<>c::.cctor()
extern void U3CU3Ec__cctor_m66A08467936DA18B932008F1E6DC72EEB39F16A1 (void);
// 0x00000018 System.Void TestingPunch/<>c::.ctor()
extern void U3CU3Ec__ctor_mF00B82FD7B30077FBDE19AD680A208A4C3BA70F9 (void);
// 0x00000019 System.Void TestingPunch/<>c::<Update>b__4_1()
extern void U3CU3Ec_U3CUpdateU3Eb__4_1_mC4766FA978E4EE87A4519997A0A18B9FF21F128A (void);
// 0x0000001A System.Void TestingPunch/<>c::<Update>b__4_4(System.Single)
extern void U3CU3Ec_U3CUpdateU3Eb__4_4_mB29A35054C7AD0CC10D5E31667EDF5249A05D507 (void);
// 0x0000001B System.Void TestingPunch/<>c::<Update>b__4_5()
extern void U3CU3Ec_U3CUpdateU3Eb__4_5_mDC2E23D6DCE6159A01A4E5965CA5654E521A6203 (void);
// 0x0000001C System.Void TestingPunch/<>c::<tweenStatically>b__5_0(System.Single)
extern void U3CU3Ec_U3CtweenStaticallyU3Eb__5_0_m412A7565AC15AED058D5BD67A3BFF97E6450F355 (void);
// 0x0000001D System.Void TestingRigidbodyCS::Start()
extern void TestingRigidbodyCS_Start_m995B801673B71100B5483E039BFCF522BA624548 (void);
// 0x0000001E System.Void TestingRigidbodyCS::Update()
extern void TestingRigidbodyCS_Update_m8DD8457212175A81186F50921B999350FA9DEC0F (void);
// 0x0000001F System.Void TestingRigidbodyCS::.ctor()
extern void TestingRigidbodyCS__ctor_mDE99A4FEAFB79D731E586C9AF3656849987B741B (void);
// 0x00000020 System.Void Following::Start()
extern void Following_Start_m5CD86C24B70F949375792EFAEF58F657F06DBA18 (void);
// 0x00000021 System.Void Following::Update()
extern void Following_Update_mEB56FD4D3CE1029AB038A8DA8CE6C6704A9541DC (void);
// 0x00000022 System.Void Following::moveArrow()
extern void Following_moveArrow_mF5878CA6A071D36230DD6FA4209FFA765BAC640F (void);
// 0x00000023 System.Void Following::.ctor()
extern void Following__ctor_mA110CFAE2A03042B9F1FE53338D377A005CBAB13 (void);
// 0x00000024 System.Void GeneralAdvancedTechniques::Start()
extern void GeneralAdvancedTechniques_Start_m132CD028BB491EE7C59B5F5F1D1AB47EB7DAE306 (void);
// 0x00000025 System.Void GeneralAdvancedTechniques::.ctor()
extern void GeneralAdvancedTechniques__ctor_mB828FC801FA5649B544E8FF31A6E1FF8916563D8 (void);
// 0x00000026 System.Void GeneralAdvancedTechniques::<Start>b__10_0(System.Single)
extern void GeneralAdvancedTechniques_U3CStartU3Eb__10_0_mBBEFB6358D20EC1EFC4D6B51139D0DEEDFF86AD2 (void);
// 0x00000027 System.Void GeneralBasic::Start()
extern void GeneralBasic_Start_m159ABB4F96835B58FEB058E24A576462A119E871 (void);
// 0x00000028 System.Void GeneralBasic::advancedExamples()
extern void GeneralBasic_advancedExamples_mB7CE50C2F062F66FD7D0E1BD403DFDACE1D099AF (void);
// 0x00000029 System.Void GeneralBasic::.ctor()
extern void GeneralBasic__ctor_mF2DBA4586E298C9F216C4680C14837E6B5E095AD (void);
// 0x0000002A System.Void GeneralBasic::<advancedExamples>b__2_0()
extern void GeneralBasic_U3CadvancedExamplesU3Eb__2_0_m706BD8DE22C36E9A7EF6326EF1B7EAB6C780BF3D (void);
// 0x0000002B System.Void GeneralBasic/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mAE48CD4D687E71BADBB3C871A2194F0803D1EDC0 (void);
// 0x0000002C System.Void GeneralBasic/<>c__DisplayClass2_0::<advancedExamples>b__1()
extern void U3CU3Ec__DisplayClass2_0_U3CadvancedExamplesU3Eb__1_m9425EEE45EC19E72004ADE4D0B203F5448C21B27 (void);
// 0x0000002D System.Void GeneralBasic/<>c__DisplayClass2_0::<advancedExamples>b__2()
extern void U3CU3Ec__DisplayClass2_0_U3CadvancedExamplesU3Eb__2_mA4BA962A162AB1425F97630A3FB0FD395D77444C (void);
// 0x0000002E System.Void GeneralBasics2d::Start()
extern void GeneralBasics2d_Start_mD2ED7FA046D4AD2C347896AFF54984E3BDC73876 (void);
// 0x0000002F UnityEngine.GameObject GeneralBasics2d::createSpriteDude(System.String,UnityEngine.Vector3,System.Boolean)
extern void GeneralBasics2d_createSpriteDude_m5D7A1E41EEAADFE587F9AB375AA4C3B7617DF7B3 (void);
// 0x00000030 System.Void GeneralBasics2d::advancedExamples()
extern void GeneralBasics2d_advancedExamples_m7A48E45BB9836372021B76A523A2594A1E66F1EA (void);
// 0x00000031 System.Void GeneralBasics2d::.ctor()
extern void GeneralBasics2d__ctor_m5BBA8B80514CB8E8098C55AF472FADC2BA164C2A (void);
// 0x00000032 System.Void GeneralBasics2d::<advancedExamples>b__4_0()
extern void GeneralBasics2d_U3CadvancedExamplesU3Eb__4_0_mFB34B228D53D66D2DA60CBE36C863AAC135AAE41 (void);
// 0x00000033 System.Void GeneralBasics2d/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mF77915960E9F4C762CB8473CB99645EB20C2BB06 (void);
// 0x00000034 System.Void GeneralBasics2d/<>c__DisplayClass4_0::<advancedExamples>b__1()
extern void U3CU3Ec__DisplayClass4_0_U3CadvancedExamplesU3Eb__1_m34824CEE9AF028A0C15E9EF50B69F24B63AD82C2 (void);
// 0x00000035 System.Void GeneralBasics2d/<>c__DisplayClass4_0::<advancedExamples>b__2()
extern void U3CU3Ec__DisplayClass4_0_U3CadvancedExamplesU3Eb__2_m9486B659998B862E7D8D7F60D70F7E60023F33C1 (void);
// 0x00000036 System.Void GeneralCameraShake::Start()
extern void GeneralCameraShake_Start_mF7CB74F9B674B375BDCB29EC25FF2B568431E543 (void);
// 0x00000037 System.Void GeneralCameraShake::bigGuyJump()
extern void GeneralCameraShake_bigGuyJump_m502DE5D59ED9FEAD8F13549A9A2105E86D1F90E3 (void);
// 0x00000038 System.Void GeneralCameraShake::.ctor()
extern void GeneralCameraShake__ctor_m6B09C9BCA8DDFD913791AD123731FFC86E274FAD (void);
// 0x00000039 System.Void GeneralCameraShake/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m3C8CF484C54E2EE225CC0D8A11A5BAEB2686C557 (void);
// 0x0000003A System.Void GeneralCameraShake/<>c__DisplayClass4_0::<bigGuyJump>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CbigGuyJumpU3Eb__0_mA78BEF951754FC194ABA798EF9E551E30CD1DF91 (void);
// 0x0000003B System.Void GeneralCameraShake/<>c__DisplayClass4_0::<bigGuyJump>b__1()
extern void U3CU3Ec__DisplayClass4_0_U3CbigGuyJumpU3Eb__1_m4A2B4F54066C9921E28893C3580BB18C47C8E99F (void);
// 0x0000003C System.Void GeneralCameraShake/<>c__DisplayClass4_1::.ctor()
extern void U3CU3Ec__DisplayClass4_1__ctor_mE5D0BDBB5AC685B299E3E124147549F48D5360FD (void);
// 0x0000003D System.Void GeneralCameraShake/<>c__DisplayClass4_1::<bigGuyJump>b__2(System.Single)
extern void U3CU3Ec__DisplayClass4_1_U3CbigGuyJumpU3Eb__2_mE50BF2533D5A8E4215CA056E6C5A7B707C31615B (void);
// 0x0000003E System.Void GeneralEasingTypes::Start()
extern void GeneralEasingTypes_Start_m9918519E1FE8506C66C09A06FC0973544C9B6CFE (void);
// 0x0000003F System.Void GeneralEasingTypes::demoEaseTypes()
extern void GeneralEasingTypes_demoEaseTypes_mFAB0A2AA4E562BA44E4D99BB36EFC8BCC92CBD4E (void);
// 0x00000040 System.Void GeneralEasingTypes::resetLines()
extern void GeneralEasingTypes_resetLines_mF6F826BE8342FEBEBB97FE69AEDCD749AD38E290 (void);
// 0x00000041 System.Void GeneralEasingTypes::.ctor()
extern void GeneralEasingTypes__ctor_m826931FD7A0F6840EC3920EF742AE00023053424 (void);
// 0x00000042 System.Void GeneralEasingTypes/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m67D836D2CA426B728355DCE885351EF9BA44FB17 (void);
// 0x00000043 System.Void GeneralEasingTypes/<>c__DisplayClass4_0::<demoEaseTypes>b__0(System.Single)
extern void U3CU3Ec__DisplayClass4_0_U3CdemoEaseTypesU3Eb__0_mDA8F2DD5BE6F9B20420A94430FE2C634CC07EED3 (void);
// 0x00000044 System.Void GeneralEventsListeners::Awake()
extern void GeneralEventsListeners_Awake_m929CA13301F3FA1A5C1AA230681377F30E0E78C4 (void);
// 0x00000045 System.Void GeneralEventsListeners::Start()
extern void GeneralEventsListeners_Start_m1C63BB1DBC231496398533A9D365C0B2EAD4BE80 (void);
// 0x00000046 System.Void GeneralEventsListeners::jumpUp(LTEvent)
extern void GeneralEventsListeners_jumpUp_m2CBD7F9072D9EB312F608BFC683E63D3DFF1DB28 (void);
// 0x00000047 System.Void GeneralEventsListeners::changeColor(LTEvent)
extern void GeneralEventsListeners_changeColor_mF0F3863B481DE533D2DA0CCF3424BD1B231268BE (void);
// 0x00000048 System.Void GeneralEventsListeners::OnCollisionEnter(UnityEngine.Collision)
extern void GeneralEventsListeners_OnCollisionEnter_m296C0AD01860A6C98C1E293DD67D880BB0590E91 (void);
// 0x00000049 System.Void GeneralEventsListeners::OnCollisionStay(UnityEngine.Collision)
extern void GeneralEventsListeners_OnCollisionStay_m290268F1B803DC1E306F52423DA7A365442C7C31 (void);
// 0x0000004A System.Void GeneralEventsListeners::FixedUpdate()
extern void GeneralEventsListeners_FixedUpdate_m903B40B7AC9A96613D21A365E8BC005E87AA8439 (void);
// 0x0000004B System.Void GeneralEventsListeners::OnMouseDown()
extern void GeneralEventsListeners_OnMouseDown_m6B7190177327186B2D363437F5B96595367C7454 (void);
// 0x0000004C System.Void GeneralEventsListeners::.ctor()
extern void GeneralEventsListeners__ctor_m7B76E509C73273AA56871D0D0E9772F6FC2A3056 (void);
// 0x0000004D System.Void GeneralEventsListeners::<changeColor>b__8_0(UnityEngine.Color)
extern void GeneralEventsListeners_U3CchangeColorU3Eb__8_0_m8D567E7710ED42E4ED9DF3074F1F9E4AF19AF1A1 (void);
// 0x0000004E System.Void GeneralSequencer::Start()
extern void GeneralSequencer_Start_m8B0E62A3511525F37EF49CF5BA670A8FDC005FBC (void);
// 0x0000004F System.Void GeneralSequencer::.ctor()
extern void GeneralSequencer__ctor_mFD32ABD60398858ED0D676B843D61702B7FB1F75 (void);
// 0x00000050 System.Void GeneralSequencer::<Start>b__4_0()
extern void GeneralSequencer_U3CStartU3Eb__4_0_m01F09B25E4652D0AD12402145A95897D0E65C36F (void);
// 0x00000051 System.Void GeneralSimpleUI::Start()
extern void GeneralSimpleUI_Start_mE89FC53D2284D832DC21032BAB637D0C4075AC6D (void);
// 0x00000052 System.Void GeneralSimpleUI::.ctor()
extern void GeneralSimpleUI__ctor_mC03751F3B550ACC5742FCB2C9B381E1EA841C085 (void);
// 0x00000053 System.Void GeneralSimpleUI::<Start>b__1_0(UnityEngine.Vector2)
extern void GeneralSimpleUI_U3CStartU3Eb__1_0_mCC061524D85ACD8BAF6FE7D8CE1F2277735B3FC8 (void);
// 0x00000054 System.Void GeneralSimpleUI::<Start>b__1_2(UnityEngine.Vector3)
extern void GeneralSimpleUI_U3CStartU3Eb__1_2_m32632A4DB6420C70DED2CA0F5D16A52ABED4D202 (void);
// 0x00000055 System.Void GeneralSimpleUI::<Start>b__1_3(UnityEngine.Color)
extern void GeneralSimpleUI_U3CStartU3Eb__1_3_m81A54F1B0397D24C3ABC0367E5F3A96CD8938D25 (void);
// 0x00000056 System.Void GeneralSimpleUI/<>c::.cctor()
extern void U3CU3Ec__cctor_m2F58606DC92BEEAFADDAB84BDE6B467660090F3A (void);
// 0x00000057 System.Void GeneralSimpleUI/<>c::.ctor()
extern void U3CU3Ec__ctor_mB27080DC10CB152A62D2B7D7F2BFFA689E952C73 (void);
// 0x00000058 System.Void GeneralSimpleUI/<>c::<Start>b__1_1(System.Single)
extern void U3CU3Ec_U3CStartU3Eb__1_1_m3121B206ADF49779458AA5961E16BC6F6DFC8277 (void);
// 0x00000059 System.Void GeneralUISpace::Start()
extern void GeneralUISpace_Start_m5EE834954E03550B87B33D931A593651703C2223 (void);
// 0x0000005A System.Void GeneralUISpace::.ctor()
extern void GeneralUISpace__ctor_m007D52C81E8F67F6C5FDBD433EBDD1F82281CE87 (void);
// 0x0000005B System.Void GeneralUISpace/<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_mBE556FCA9CC905E8AFDA137DCA924101E479875C (void);
// 0x0000005C System.Void GeneralUISpace/<>c__DisplayClass15_0::<Start>b__0(System.Single)
extern void U3CU3Ec__DisplayClass15_0_U3CStartU3Eb__0_m6F18D230FF8599CCA7A14221973A08E0447954B1 (void);
// 0x0000005D System.Void LogoCinematic::Awake()
extern void LogoCinematic_Awake_mC405113467B061C77307CFD90E064F5A651B3D22 (void);
// 0x0000005E System.Void LogoCinematic::Start()
extern void LogoCinematic_Start_m3E319B3DEC812349F8C37171CA5D4081276F7826 (void);
// 0x0000005F System.Void LogoCinematic::playBoom()
extern void LogoCinematic_playBoom_m787C8F6D4E17645300701C39C99D4E37893FEFAF (void);
// 0x00000060 System.Void LogoCinematic::.ctor()
extern void LogoCinematic__ctor_m4106D7C04CED9DBA0EA597C3A8961A01C01B88AD (void);
// 0x00000061 System.Void PathBezier2d::Start()
extern void PathBezier2d_Start_m6F6F8E4B60B7D179AE36BEE9EC0A264ED03D6849 (void);
// 0x00000062 System.Void PathBezier2d::OnDrawGizmos()
extern void PathBezier2d_OnDrawGizmos_m69311F42A05B822B9FCBD8CC4F0F4C4804AC9354 (void);
// 0x00000063 System.Void PathBezier2d::.ctor()
extern void PathBezier2d__ctor_m28D0B7A13B4DA0EDA3A009A105016F4A7E143055 (void);
// 0x00000064 System.Void ExampleSpline::Start()
extern void ExampleSpline_Start_mA17AF5D470B3BCA08101E4A82E8EB7A0BAABEAB6 (void);
// 0x00000065 System.Void ExampleSpline::Update()
extern void ExampleSpline_Update_m356EAE7A488D899AAA63B3F9CC96E19743AE7EC6 (void);
// 0x00000066 System.Void ExampleSpline::OnDrawGizmos()
extern void ExampleSpline_OnDrawGizmos_m1479029D4155936B69C362144D347542E05CC0DF (void);
// 0x00000067 System.Void ExampleSpline::.ctor()
extern void ExampleSpline__ctor_m9FCDF08DBF16C46F63F1AF63EF143E3DAC295EE1 (void);
// 0x00000068 System.Void PathSpline2d::Start()
extern void PathSpline2d_Start_m1EBB4A3F12FFD7F33EC3D10C0955B5D421DE5B5D (void);
// 0x00000069 System.Void PathSpline2d::OnDrawGizmos()
extern void PathSpline2d_OnDrawGizmos_mF49ABC37A9C2C3B83BB5BF1419067E2FD8AB1B77 (void);
// 0x0000006A System.Void PathSpline2d::.ctor()
extern void PathSpline2d__ctor_mD3756639777CCF9D251F157BCF32647C9F0AB7C6 (void);
// 0x0000006B System.Void PathSplineEndless::Start()
extern void PathSplineEndless_Start_mAB94E60786A238B143854F91E30BB877D5730800 (void);
// 0x0000006C System.Void PathSplineEndless::Update()
extern void PathSplineEndless_Update_m5C2C0D01E2D3AF4FFD2AAF23756C62733DB1509A (void);
// 0x0000006D UnityEngine.GameObject PathSplineEndless::objectQueue(UnityEngine.GameObject[],System.Int32&)
extern void PathSplineEndless_objectQueue_m4F43CF1A70A9897A13A3B385DCE841DA5141C524 (void);
// 0x0000006E System.Void PathSplineEndless::addRandomTrackPoint()
extern void PathSplineEndless_addRandomTrackPoint_m3A914064336F71FAC30C4919147216682994A961 (void);
// 0x0000006F System.Void PathSplineEndless::refreshSpline()
extern void PathSplineEndless_refreshSpline_m8B64174788E0276D3C61E2F66C85EF7378EA30C2 (void);
// 0x00000070 System.Void PathSplineEndless::playSwish()
extern void PathSplineEndless_playSwish_m968F1F1AED0F8421A2834347C318076A3CE747BD (void);
// 0x00000071 System.Void PathSplineEndless::.ctor()
extern void PathSplineEndless__ctor_m411B70A0AC412E9518BBBDC687C25CD1B388E6C9 (void);
// 0x00000072 System.Void PathSplineEndless::<Start>b__17_0(System.Single)
extern void PathSplineEndless_U3CStartU3Eb__17_0_m89EDA38D298348E747F008DED4C1CF7D4F815D60 (void);
// 0x00000073 System.Void PathSplinePerformance::Start()
extern void PathSplinePerformance_Start_m6A0FB9A0FCA5D3B1AFDF6C03C8B8CD3DCFC57040 (void);
// 0x00000074 System.Void PathSplinePerformance::Update()
extern void PathSplinePerformance_Update_m5416A63FFB450EABE065F6C5C3312B7C048EF5A7 (void);
// 0x00000075 System.Void PathSplinePerformance::OnDrawGizmos()
extern void PathSplinePerformance_OnDrawGizmos_m490615FEB9C7E53A19C0DA922E9A7077A1A66274 (void);
// 0x00000076 System.Void PathSplinePerformance::playSwish()
extern void PathSplinePerformance_playSwish_mDCC9C70227FFF9F7969D9D8330E5B485B954DC7A (void);
// 0x00000077 System.Void PathSplinePerformance::.ctor()
extern void PathSplinePerformance__ctor_m3918B4846029379DBF03985747088A8F4E734CBB (void);
// 0x00000078 System.Void PathSplineTrack::Start()
extern void PathSplineTrack_Start_m63CB9A3917A58E8791AA4DED6921EB32280FFF7F (void);
// 0x00000079 System.Void PathSplineTrack::Update()
extern void PathSplineTrack_Update_m00439912F9ADA883049E7B01A7453419BDB6AE79 (void);
// 0x0000007A System.Void PathSplineTrack::OnDrawGizmos()
extern void PathSplineTrack_OnDrawGizmos_mD7F0DCDB7C1B0CB6ED0F1A4A20E2389A14127D5F (void);
// 0x0000007B System.Void PathSplineTrack::playSwish()
extern void PathSplineTrack_playSwish_mEADD52F2DF6153507245FCEF25CCE5AAC216627A (void);
// 0x0000007C System.Void PathSplineTrack::.ctor()
extern void PathSplineTrack__ctor_m7F933342B4D6E1B74306EF85FC59C7F70E5EF982 (void);
// 0x0000007D System.Void PathSplines::OnEnable()
extern void PathSplines_OnEnable_mBAC54412DA75F41A2961B5D2A8D33971CA42EF78 (void);
// 0x0000007E System.Void PathSplines::Start()
extern void PathSplines_Start_m7E196F5CDC4C203D9187A30B90FC02414FBDF186 (void);
// 0x0000007F System.Void PathSplines::Update()
extern void PathSplines_Update_m2D049CD05DCD8B298477D7A6F6EEF97F16A140EA (void);
// 0x00000080 System.Void PathSplines::OnDrawGizmos()
extern void PathSplines_OnDrawGizmos_m2099220E2C8911102EA2BE1EFC5153CDE8FF6283 (void);
// 0x00000081 System.Void PathSplines::.ctor()
extern void PathSplines__ctor_m4BAB7527375E54EAD3B636B8AB1F5AEBF88AB565 (void);
// 0x00000082 System.Void PathSplines::<Start>b__4_0()
extern void PathSplines_U3CStartU3Eb__4_0_mC304E252D7336E181BBEF0F27B79311DEDF4BB0B (void);
// 0x00000083 System.Void TestingZLegacy::Awake()
extern void TestingZLegacy_Awake_m6ACDE967AF5970766B8EFFCC196B677E07DB0C10 (void);
// 0x00000084 System.Void TestingZLegacy::Start()
extern void TestingZLegacy_Start_mE43112422475CBDC67D4CE4680D0B64C38D23BA6 (void);
// 0x00000085 System.Void TestingZLegacy::pauseNow()
extern void TestingZLegacy_pauseNow_mCC617C16BCE35E2D901183CFAEF9F51D7805DA61 (void);
// 0x00000086 System.Void TestingZLegacy::OnGUI()
extern void TestingZLegacy_OnGUI_mAF981918615B59787EED9DC6EB2F232D6A4F6E5F (void);
// 0x00000087 System.Void TestingZLegacy::endlessCallback()
extern void TestingZLegacy_endlessCallback_m96FFBCC19A52A227D879CA4C9A4D784078F0FB65 (void);
// 0x00000088 System.Void TestingZLegacy::cycleThroughExamples()
extern void TestingZLegacy_cycleThroughExamples_mE124EAAF3FE6EDE035F8D620E0FF877916FFCA2A (void);
// 0x00000089 System.Void TestingZLegacy::updateValue3Example()
extern void TestingZLegacy_updateValue3Example_m690CC88D964055BB090736A4D7D080E043E15D82 (void);
// 0x0000008A System.Void TestingZLegacy::updateValue3ExampleUpdate(UnityEngine.Vector3)
extern void TestingZLegacy_updateValue3ExampleUpdate_m28A23A4EFF68505C400DC9F5BBBD6ADA3ACAEE62 (void);
// 0x0000008B System.Void TestingZLegacy::updateValue3ExampleCallback(UnityEngine.Vector3)
extern void TestingZLegacy_updateValue3ExampleCallback_m25E92C1FD9658ABBED9080D237D95E6BCFC57E12 (void);
// 0x0000008C System.Void TestingZLegacy::loopTestClamp()
extern void TestingZLegacy_loopTestClamp_m1EC272930E9852F0D980AC98F1633B4F2841C450 (void);
// 0x0000008D System.Void TestingZLegacy::loopTestPingPong()
extern void TestingZLegacy_loopTestPingPong_mC5B0F54C34F86BD154F6A0BB43C88FC7913344B9 (void);
// 0x0000008E System.Void TestingZLegacy::colorExample()
extern void TestingZLegacy_colorExample_m9C06FD132681F8FF64F1498BFC2811EFD82E4D28 (void);
// 0x0000008F System.Void TestingZLegacy::moveOnACurveExample()
extern void TestingZLegacy_moveOnACurveExample_m89E0D3F9175DBDEAB39A915BCCD5AF06FCD2357F (void);
// 0x00000090 System.Void TestingZLegacy::customTweenExample()
extern void TestingZLegacy_customTweenExample_mF418B80DCF007D21FBD0663BB9F002CF18EAEB0A (void);
// 0x00000091 System.Void TestingZLegacy::moveExample()
extern void TestingZLegacy_moveExample_m5A0E16D835C2A10B7042F64D1B6CD0C1320B1F78 (void);
// 0x00000092 System.Void TestingZLegacy::rotateExample()
extern void TestingZLegacy_rotateExample_m61CF7E2DC245EBF480DCCD0369ABA0F332658917 (void);
// 0x00000093 System.Void TestingZLegacy::rotateOnUpdate(System.Single)
extern void TestingZLegacy_rotateOnUpdate_mE9D7799638F85C32FB7715059ACBDF657C89B7CE (void);
// 0x00000094 System.Void TestingZLegacy::rotateFinished(System.Object)
extern void TestingZLegacy_rotateFinished_mE2E799232C3119A15368CBABBF594C57799D5683 (void);
// 0x00000095 System.Void TestingZLegacy::scaleExample()
extern void TestingZLegacy_scaleExample_mD080FABF7510CFB95F324A1B4BB1C4277937EFF9 (void);
// 0x00000096 System.Void TestingZLegacy::updateValueExample()
extern void TestingZLegacy_updateValueExample_m9DBE93DA0EC50F7E6764A27DE1E87F12ACF8DA2A (void);
// 0x00000097 System.Void TestingZLegacy::updateValueExampleCallback(System.Single,System.Object)
extern void TestingZLegacy_updateValueExampleCallback_mEB667645443DDFD352588C65ACBBE1C500E97C4C (void);
// 0x00000098 System.Void TestingZLegacy::delayedCallExample()
extern void TestingZLegacy_delayedCallExample_m51E456D34EDF2598C8EF549CDDB2376F607C3CC1 (void);
// 0x00000099 System.Void TestingZLegacy::delayedCallExampleCallback()
extern void TestingZLegacy_delayedCallExampleCallback_m15CE90D43A85804204B22169D73FDE3EA46F609A (void);
// 0x0000009A System.Void TestingZLegacy::alphaExample()
extern void TestingZLegacy_alphaExample_mA4ED4E57F148B4EF6E2B1B608BAEE0EEF5AE7143 (void);
// 0x0000009B System.Void TestingZLegacy::moveLocalExample()
extern void TestingZLegacy_moveLocalExample_mE6E3C5CD62456482BADA24B0CA93C0962125B80F (void);
// 0x0000009C System.Void TestingZLegacy::rotateAroundExample()
extern void TestingZLegacy_rotateAroundExample_mB3F00FEDCDD63C7BFD293C187BA847BF15F7ED3C (void);
// 0x0000009D System.Void TestingZLegacy::loopPause()
extern void TestingZLegacy_loopPause_mC83ACD076E19354DC2CFF3D3E409DA2901CE229E (void);
// 0x0000009E System.Void TestingZLegacy::loopResume()
extern void TestingZLegacy_loopResume_mBA3E805FE386DFC40F4F795223744619E9048A7E (void);
// 0x0000009F System.Void TestingZLegacy::punchTest()
extern void TestingZLegacy_punchTest_m1266ADD2035D0E9971ADA805B0A1636D35ABF2B2 (void);
// 0x000000A0 System.Void TestingZLegacy::.ctor()
extern void TestingZLegacy__ctor_m1CA1AEE9AA84B7551AFA5A034BF4E648AE662D9E (void);
// 0x000000A1 System.Void TestingZLegacy/NextFunc::.ctor(System.Object,System.IntPtr)
extern void NextFunc__ctor_mD13871807D65A0BB2C5E0F567E5FB021AF248AED (void);
// 0x000000A2 System.Void TestingZLegacy/NextFunc::Invoke()
extern void NextFunc_Invoke_m8D7A63BD3F35D7D22BC9DAF5241C637889044719 (void);
// 0x000000A3 System.IAsyncResult TestingZLegacy/NextFunc::BeginInvoke(System.AsyncCallback,System.Object)
extern void NextFunc_BeginInvoke_mB38D1030FE53CEF432C2E6D1569F0EBAD0F1B5F0 (void);
// 0x000000A4 System.Void TestingZLegacy/NextFunc::EndInvoke(System.IAsyncResult)
extern void NextFunc_EndInvoke_mFE2435A3542567B6D23B6E63B50BF9EC6D55609B (void);
// 0x000000A5 System.Void TestingZLegacy/<>c::.cctor()
extern void U3CU3Ec__cctor_mBD80E302F3A69C0C61FDB9987E223DF4A13097D9 (void);
// 0x000000A6 System.Void TestingZLegacy/<>c::.ctor()
extern void U3CU3Ec__ctor_mD7A097986D81133EA1341850CD4003072DC490BB (void);
// 0x000000A7 System.Void TestingZLegacy/<>c::<cycleThroughExamples>b__20_0(System.Single)
extern void U3CU3Ec_U3CcycleThroughExamplesU3Eb__20_0_m99E554155125675C01E3E61098DA676F0B69E649 (void);
// 0x000000A8 System.Void TestingZLegacyExt::Awake()
extern void TestingZLegacyExt_Awake_m7D6F79923E270BE07F7B475DBFE0A7CA94534FCB (void);
// 0x000000A9 System.Void TestingZLegacyExt::Start()
extern void TestingZLegacyExt_Start_m86BD1A532532B1F08B6ECF7457358F6CB1410E5E (void);
// 0x000000AA System.Void TestingZLegacyExt::pauseNow()
extern void TestingZLegacyExt_pauseNow_m0A9E3639505AB9DDE82956D7FB34C37758982180 (void);
// 0x000000AB System.Void TestingZLegacyExt::OnGUI()
extern void TestingZLegacyExt_OnGUI_m25777A6897DF3B16F25546C5CAFBF80FB60923AA (void);
// 0x000000AC System.Void TestingZLegacyExt::endlessCallback()
extern void TestingZLegacyExt_endlessCallback_m73A1A18DA9DD7642231DE5BBBF35F45DB785A077 (void);
// 0x000000AD System.Void TestingZLegacyExt::cycleThroughExamples()
extern void TestingZLegacyExt_cycleThroughExamples_m15FB09793AAA4E36F905F6D81C254E0B763F391D (void);
// 0x000000AE System.Void TestingZLegacyExt::updateValue3Example()
extern void TestingZLegacyExt_updateValue3Example_m15F0FA3A4491DBB842544D0A878B2C533640B4AE (void);
// 0x000000AF System.Void TestingZLegacyExt::updateValue3ExampleUpdate(UnityEngine.Vector3)
extern void TestingZLegacyExt_updateValue3ExampleUpdate_m46BCB3BF2A85D38DE7CE46720DF912FDA7DB84FE (void);
// 0x000000B0 System.Void TestingZLegacyExt::updateValue3ExampleCallback(UnityEngine.Vector3)
extern void TestingZLegacyExt_updateValue3ExampleCallback_m7B2BA08D3878E1672F358452FA11C48F8D686B76 (void);
// 0x000000B1 System.Void TestingZLegacyExt::loopTestClamp()
extern void TestingZLegacyExt_loopTestClamp_mA4A7563CB2448E2D1EC1394494E21C79399BAC06 (void);
// 0x000000B2 System.Void TestingZLegacyExt::loopTestPingPong()
extern void TestingZLegacyExt_loopTestPingPong_m6523FCB9E3A1AC5035882C67F6EF5B8A00AAC5D3 (void);
// 0x000000B3 System.Void TestingZLegacyExt::colorExample()
extern void TestingZLegacyExt_colorExample_mCF5A6A5FAA5CC4EC32FFD99062DA09D220B776A2 (void);
// 0x000000B4 System.Void TestingZLegacyExt::moveOnACurveExample()
extern void TestingZLegacyExt_moveOnACurveExample_m8E7EA9EADCB22EB3DC2DDC7FD27BD05050C2FB44 (void);
// 0x000000B5 System.Void TestingZLegacyExt::customTweenExample()
extern void TestingZLegacyExt_customTweenExample_mE32AD842F2ACF6BB96A100E29B784B942004386F (void);
// 0x000000B6 System.Void TestingZLegacyExt::moveExample()
extern void TestingZLegacyExt_moveExample_mB59B97379A841F1AB17E8E9CE8BC7AFF0DB9F3C8 (void);
// 0x000000B7 System.Void TestingZLegacyExt::rotateExample()
extern void TestingZLegacyExt_rotateExample_m15D1742A434CC70072A4C96DFEDB623B1BD8C02F (void);
// 0x000000B8 System.Void TestingZLegacyExt::rotateOnUpdate(System.Single)
extern void TestingZLegacyExt_rotateOnUpdate_m854E95A564B027980BE0FFF08EF1A20CA3EF5EC7 (void);
// 0x000000B9 System.Void TestingZLegacyExt::rotateFinished(System.Object)
extern void TestingZLegacyExt_rotateFinished_m78E88F2F51C8018BBA472122955C89DCD3C13B53 (void);
// 0x000000BA System.Void TestingZLegacyExt::scaleExample()
extern void TestingZLegacyExt_scaleExample_mDDF64026F75968C35562CEE5C1B2F84874C76C1A (void);
// 0x000000BB System.Void TestingZLegacyExt::updateValueExample()
extern void TestingZLegacyExt_updateValueExample_mD778D12570BC222BB68613D27BD4B5B0DCD9E76C (void);
// 0x000000BC System.Void TestingZLegacyExt::updateValueExampleCallback(System.Single,System.Object)
extern void TestingZLegacyExt_updateValueExampleCallback_m273FBEFFA8289DCB71813A6C4FF5C66ABD2C5003 (void);
// 0x000000BD System.Void TestingZLegacyExt::delayedCallExample()
extern void TestingZLegacyExt_delayedCallExample_m92DECA48CA80DF5FF46B873EF87AC2A564AA6452 (void);
// 0x000000BE System.Void TestingZLegacyExt::delayedCallExampleCallback()
extern void TestingZLegacyExt_delayedCallExampleCallback_m2F2F9A40BA43F410DABE187FEA3861FCF719058B (void);
// 0x000000BF System.Void TestingZLegacyExt::alphaExample()
extern void TestingZLegacyExt_alphaExample_m0875576982CCE840BF262C71817239AB7EA6C8C2 (void);
// 0x000000C0 System.Void TestingZLegacyExt::moveLocalExample()
extern void TestingZLegacyExt_moveLocalExample_m81967DF8243173690E01BCD0C9DEDF141C080813 (void);
// 0x000000C1 System.Void TestingZLegacyExt::rotateAroundExample()
extern void TestingZLegacyExt_rotateAroundExample_mA9188C5A54C644D2B78F8956DAB3B245B975BCA1 (void);
// 0x000000C2 System.Void TestingZLegacyExt::loopPause()
extern void TestingZLegacyExt_loopPause_m04999A727B7B5D4D7C04BA4D24C78FD7C35FCB42 (void);
// 0x000000C3 System.Void TestingZLegacyExt::loopResume()
extern void TestingZLegacyExt_loopResume_mAD5DEA4AB4F83437FE25A8C8BBE141FB885CB6CF (void);
// 0x000000C4 System.Void TestingZLegacyExt::punchTest()
extern void TestingZLegacyExt_punchTest_mEE9802D38D39AB2BE2BE50BD88F901A16383B87B (void);
// 0x000000C5 System.Void TestingZLegacyExt::.ctor()
extern void TestingZLegacyExt__ctor_m5E300E444E4496DA32F5F4A1F8B48051126D8B61 (void);
// 0x000000C6 System.Void TestingZLegacyExt/NextFunc::.ctor(System.Object,System.IntPtr)
extern void NextFunc__ctor_m0008349A4886ECE27F576CFE82C9A888798F60BA (void);
// 0x000000C7 System.Void TestingZLegacyExt/NextFunc::Invoke()
extern void NextFunc_Invoke_mFB6931473FA9F6AD02BE8B752A4D051ECB660ECF (void);
// 0x000000C8 System.IAsyncResult TestingZLegacyExt/NextFunc::BeginInvoke(System.AsyncCallback,System.Object)
extern void NextFunc_BeginInvoke_mD91FEF6AFFC5695F493E6950FAAA8A6B72DB37CD (void);
// 0x000000C9 System.Void TestingZLegacyExt/NextFunc::EndInvoke(System.IAsyncResult)
extern void NextFunc_EndInvoke_m96DB2010CA80A82BDCD35903E0CE02CE8AC78296 (void);
// 0x000000CA System.Void TestingZLegacyExt/<>c::.cctor()
extern void U3CU3Ec__cctor_mD11D91D44BCDECB51A3CF966B686782B4F5330A7 (void);
// 0x000000CB System.Void TestingZLegacyExt/<>c::.ctor()
extern void U3CU3Ec__ctor_mC490B74A87268C68E2428BE981C75A67BE4460A7 (void);
// 0x000000CC System.Void TestingZLegacyExt/<>c::<cycleThroughExamples>b__20_0(System.Single)
extern void U3CU3Ec_U3CcycleThroughExamplesU3Eb__20_0_m232F589CF9029278662FD1D5BB31DB7E6A2973F5 (void);
// 0x000000CD UnityEngine.Vector3 LTDescr::get_from()
extern void LTDescr_get_from_m5C46DBE96932E1F21CB71C1174B3294CF8F80E4A (void);
// 0x000000CE System.Void LTDescr::set_from(UnityEngine.Vector3)
extern void LTDescr_set_from_mCEA64BFED572C9327D1186A8DB46FFB56836E4F5 (void);
// 0x000000CF UnityEngine.Vector3 LTDescr::get_to()
extern void LTDescr_get_to_m3887E20BF193FF607A48502338A46C90948F5FA5 (void);
// 0x000000D0 System.Void LTDescr::set_to(UnityEngine.Vector3)
extern void LTDescr_set_to_m12DADAD8999E97D8D46F4219632BFAB4FCA41796 (void);
// 0x000000D1 LTDescr/ActionMethodDelegate LTDescr::get_easeInternal()
extern void LTDescr_get_easeInternal_m263F859E9225DF85EFD75C079CD5073AE647B284 (void);
// 0x000000D2 System.Void LTDescr::set_easeInternal(LTDescr/ActionMethodDelegate)
extern void LTDescr_set_easeInternal_m40A3FEDCAA0EE7EB86E804641367E50E34C0CDB9 (void);
// 0x000000D3 LTDescr/ActionMethodDelegate LTDescr::get_initInternal()
extern void LTDescr_get_initInternal_m3E9E4486FBFCFFDB8A3A91EB00DBEBA29916C943 (void);
// 0x000000D4 System.Void LTDescr::set_initInternal(LTDescr/ActionMethodDelegate)
extern void LTDescr_set_initInternal_m21E7CD474330836550F6298EF7F0F2902BC3EE60 (void);
// 0x000000D5 UnityEngine.Transform LTDescr::get_toTrans()
extern void LTDescr_get_toTrans_m03763D518E68ED5106B72B7DEBBD29765285CB78 (void);
// 0x000000D6 System.String LTDescr::ToString()
extern void LTDescr_ToString_m8D2047DA16E09587E6709505E2328386ED0BADA7 (void);
// 0x000000D7 System.Void LTDescr::.ctor()
extern void LTDescr__ctor_mD2905F6F8358B3AD8C5784EEA14DE3C05769538B (void);
// 0x000000D8 LTDescr LTDescr::cancel(UnityEngine.GameObject)
extern void LTDescr_cancel_mC12FD3EE1E42AFCC7AA536A6D344BE3AEA86AE70 (void);
// 0x000000D9 System.Int32 LTDescr::get_uniqueId()
extern void LTDescr_get_uniqueId_mCCC4A89D6863332459B9771B4E636F951B2AAD3B (void);
// 0x000000DA System.Int32 LTDescr::get_id()
extern void LTDescr_get_id_m71271377DB5ECF8CF10782531395CCB0B5478E09 (void);
// 0x000000DB LTDescrOptional LTDescr::get_optional()
extern void LTDescr_get_optional_m0475EBA98D33B8F970E159516CC6BBEC331BE807 (void);
// 0x000000DC System.Void LTDescr::set_optional(LTDescrOptional)
extern void LTDescr_set_optional_m17425AF517082967050C04F2C3B3D2D574DDB2B0 (void);
// 0x000000DD System.Void LTDescr::reset()
extern void LTDescr_reset_mDC3B5AEC9CB77A12D954B5BB283B062A950BC8DE (void);
// 0x000000DE LTDescr LTDescr::setFollow()
extern void LTDescr_setFollow_m984BCBD0F7B73FB1E6ABAF2C1B4CA1EE506546DC (void);
// 0x000000DF LTDescr LTDescr::setMoveX()
extern void LTDescr_setMoveX_m76CE7ACEF44A88ED9BC3E747549EE2CFCB2013A2 (void);
// 0x000000E0 LTDescr LTDescr::setMoveY()
extern void LTDescr_setMoveY_m09BC6472FAD45D9AB081F3D63C49C09E1369205D (void);
// 0x000000E1 LTDescr LTDescr::setMoveZ()
extern void LTDescr_setMoveZ_m421A6F5AFA9E4627D93946E2797FE1C4DB561DF4 (void);
// 0x000000E2 LTDescr LTDescr::setMoveLocalX()
extern void LTDescr_setMoveLocalX_m75617B5FB504CAF9FF78274042314A7A20388ED6 (void);
// 0x000000E3 LTDescr LTDescr::setMoveLocalY()
extern void LTDescr_setMoveLocalY_m22DCFF89FDE31EBEB557B9964E55DFC264110243 (void);
// 0x000000E4 LTDescr LTDescr::setMoveLocalZ()
extern void LTDescr_setMoveLocalZ_m5B79B96D94FF6B39A2CFEFF240E3C88D6388F6A2 (void);
// 0x000000E5 System.Void LTDescr::initFromInternal()
extern void LTDescr_initFromInternal_m639FAC099FE64CC165535CCE0719472B44AC7F22 (void);
// 0x000000E6 LTDescr LTDescr::setOffset(UnityEngine.Vector3)
extern void LTDescr_setOffset_mB2DAAF3FEB7717194BBA1C04741D4EF68C8F952D (void);
// 0x000000E7 LTDescr LTDescr::setMoveCurved()
extern void LTDescr_setMoveCurved_m0DBB6E2C2BAA1806B5027FFA8DC167215CC9425E (void);
// 0x000000E8 LTDescr LTDescr::setMoveCurvedLocal()
extern void LTDescr_setMoveCurvedLocal_m81E717B0084EB49305FF083C0CADA94DCD974319 (void);
// 0x000000E9 LTDescr LTDescr::setMoveSpline()
extern void LTDescr_setMoveSpline_m0927D09871DEF890A033AC185FD26BCDBE1911C7 (void);
// 0x000000EA LTDescr LTDescr::setMoveSplineLocal()
extern void LTDescr_setMoveSplineLocal_mC0E62F0B0E9597DAAE5027FAB2F649EAF2E13034 (void);
// 0x000000EB LTDescr LTDescr::setScaleX()
extern void LTDescr_setScaleX_m7CFF1D6BE843E9721DB183EB147279B195953B7B (void);
// 0x000000EC LTDescr LTDescr::setScaleY()
extern void LTDescr_setScaleY_mBC9B04832806ACB660D3E326621711B15CF7F791 (void);
// 0x000000ED LTDescr LTDescr::setScaleZ()
extern void LTDescr_setScaleZ_m0401AF8B4C25CB3AA2CDCBF4C2DBD75166897C0D (void);
// 0x000000EE LTDescr LTDescr::setRotateX()
extern void LTDescr_setRotateX_m26F91A22B505D763B444E6523057008EAD6FD7B3 (void);
// 0x000000EF LTDescr LTDescr::setRotateY()
extern void LTDescr_setRotateY_m52016439FDA153BB871679C34D65946434862E66 (void);
// 0x000000F0 LTDescr LTDescr::setRotateZ()
extern void LTDescr_setRotateZ_mBC6D82441A461E415CB255195BD66FBC4AF132D9 (void);
// 0x000000F1 LTDescr LTDescr::setRotateAround()
extern void LTDescr_setRotateAround_m346F3BDC7A38E146F5EF8FACC1DBA07BD3684918 (void);
// 0x000000F2 LTDescr LTDescr::setRotateAroundLocal()
extern void LTDescr_setRotateAroundLocal_mBE18B57E33C1118A01FBDB41A99DE69746C9C865 (void);
// 0x000000F3 LTDescr LTDescr::setAlpha()
extern void LTDescr_setAlpha_m291AFD44926FB68DD36823B9D142AA276D072962 (void);
// 0x000000F4 LTDescr LTDescr::setTextAlpha()
extern void LTDescr_setTextAlpha_m569C24FD4701EFE34149E4803E189518B383B0B1 (void);
// 0x000000F5 LTDescr LTDescr::setAlphaVertex()
extern void LTDescr_setAlphaVertex_mEF508AADA9444AAB3946ED1C2A3506094A541E4A (void);
// 0x000000F6 LTDescr LTDescr::setColor()
extern void LTDescr_setColor_mA47DB19E9808DD9AAA142AB9CC3D33C4A0EFAB04 (void);
// 0x000000F7 LTDescr LTDescr::setCallbackColor()
extern void LTDescr_setCallbackColor_m9EA1B5111A4241C55BC10C0705147A33BA07AC33 (void);
// 0x000000F8 LTDescr LTDescr::setTextColor()
extern void LTDescr_setTextColor_m3A20272C52CF7141B376DCFFB58C4BA642DD4A83 (void);
// 0x000000F9 LTDescr LTDescr::setCanvasAlpha()
extern void LTDescr_setCanvasAlpha_mD05551595E5C4D79C3242EEEEA99AD01274C0BF1 (void);
// 0x000000FA LTDescr LTDescr::setCanvasGroupAlpha()
extern void LTDescr_setCanvasGroupAlpha_mF556BF3EE226BAC974CC36C638CECD86BD29E2B3 (void);
// 0x000000FB LTDescr LTDescr::setCanvasColor()
extern void LTDescr_setCanvasColor_m52C8833AC9DCC36676E9795E24F31D7955CE17C2 (void);
// 0x000000FC LTDescr LTDescr::setCanvasMoveX()
extern void LTDescr_setCanvasMoveX_m7B0F43CC9D9B78A933E1B5EE3CF4A9944129B11F (void);
// 0x000000FD LTDescr LTDescr::setCanvasMoveY()
extern void LTDescr_setCanvasMoveY_m5A9AAC03AB65F7B5E14CA663EE591F9FF8FDE5A9 (void);
// 0x000000FE LTDescr LTDescr::setCanvasMoveZ()
extern void LTDescr_setCanvasMoveZ_mEBDF9362DB395299B0E6C4EF45A8E5DF8FCA5CE0 (void);
// 0x000000FF System.Void LTDescr::initCanvasRotateAround()
extern void LTDescr_initCanvasRotateAround_mDF5155DE268AB0DB414A892643342B04C3C80430 (void);
// 0x00000100 LTDescr LTDescr::setCanvasRotateAround()
extern void LTDescr_setCanvasRotateAround_mD875CE01D408277380BA7E1A90855FAB176930E5 (void);
// 0x00000101 LTDescr LTDescr::setCanvasRotateAroundLocal()
extern void LTDescr_setCanvasRotateAroundLocal_m0FB16DFF151E8479B394929E477812B908E71811 (void);
// 0x00000102 LTDescr LTDescr::setCanvasPlaySprite()
extern void LTDescr_setCanvasPlaySprite_m5507D16A5CA3D57966B06FE22CF280B3635D272D (void);
// 0x00000103 LTDescr LTDescr::setCanvasMove()
extern void LTDescr_setCanvasMove_mBA28E1B08C4CDD0493F4AEE9032DB47D8DE96A57 (void);
// 0x00000104 LTDescr LTDescr::setCanvasScale()
extern void LTDescr_setCanvasScale_m25D2078C0E8BC068AB18C945177B424FD8BDF35D (void);
// 0x00000105 LTDescr LTDescr::setCanvasSizeDelta()
extern void LTDescr_setCanvasSizeDelta_mD3C9CBB1C8F686F2E253C22DB1A2C841C39D00F3 (void);
// 0x00000106 System.Void LTDescr::callback()
extern void LTDescr_callback_mCC3CF88649A2DB3AADEA6D010D9E1D6F8F0B50A1 (void);
// 0x00000107 LTDescr LTDescr::setCallback()
extern void LTDescr_setCallback_mD0C03116D293C89C840A0EB7D7276987E5704AE4 (void);
// 0x00000108 LTDescr LTDescr::setValue3()
extern void LTDescr_setValue3_mDCA24E0028F363246355199865E0E9C795919C87 (void);
// 0x00000109 LTDescr LTDescr::setMove()
extern void LTDescr_setMove_m0A39B796C492B5F74EAC7A88B614DABEEC83E31D (void);
// 0x0000010A LTDescr LTDescr::setMoveLocal()
extern void LTDescr_setMoveLocal_m7979A95A8C3E83116630A1972484ACB778E042B0 (void);
// 0x0000010B LTDescr LTDescr::setMoveToTransform()
extern void LTDescr_setMoveToTransform_m44267D9670BA0AA3F282AF4654C65465EBE562CD (void);
// 0x0000010C LTDescr LTDescr::setRotate()
extern void LTDescr_setRotate_m481E57B448F3773EB7E8FEDB4D54061FC454EE79 (void);
// 0x0000010D LTDescr LTDescr::setRotateLocal()
extern void LTDescr_setRotateLocal_m2C363D0AC041E31D2D2ED4143CFFBA71E6625EB8 (void);
// 0x0000010E LTDescr LTDescr::setScale()
extern void LTDescr_setScale_m0E480B54D7D9C2B8268023050E9231052B87BCA9 (void);
// 0x0000010F LTDescr LTDescr::setGUIMove()
extern void LTDescr_setGUIMove_mCEF7E5A5AABE334A23DE8CDBE26EDD4658B06CA7 (void);
// 0x00000110 LTDescr LTDescr::setGUIMoveMargin()
extern void LTDescr_setGUIMoveMargin_mFA7CF549318E4EA41AEF0A72DE52926C88AE3D28 (void);
// 0x00000111 LTDescr LTDescr::setGUIScale()
extern void LTDescr_setGUIScale_m428EBDD7206CF4353C456CDD2554ACAD5989457A (void);
// 0x00000112 LTDescr LTDescr::setGUIAlpha()
extern void LTDescr_setGUIAlpha_m316B0C88A1C8D63CA862FEF28A6AC263D043B4A5 (void);
// 0x00000113 LTDescr LTDescr::setGUIRotate()
extern void LTDescr_setGUIRotate_m5C0C9A675F084647D909DA88099F8A056CC18DD0 (void);
// 0x00000114 LTDescr LTDescr::setDelayedSound()
extern void LTDescr_setDelayedSound_m1D0040CD3D5090096914684D47D8076A21A0D732 (void);
// 0x00000115 LTDescr LTDescr::setTarget(UnityEngine.Transform)
extern void LTDescr_setTarget_m5F34436BE6EF6A6E6804AA39FD773562BF873647 (void);
// 0x00000116 System.Void LTDescr::init()
extern void LTDescr_init_m9C699F46C58C3BEE7F19E14E4B737FBC34685B60 (void);
// 0x00000117 System.Void LTDescr::initSpeed()
extern void LTDescr_initSpeed_m4F4E1F0B62028C76C78B000403BDDFBABCB30754 (void);
// 0x00000118 LTDescr LTDescr::updateNow()
extern void LTDescr_updateNow_m7A17C67327BF1CA89C94257396A0B64F916FD330 (void);
// 0x00000119 System.Boolean LTDescr::updateInternal()
extern void LTDescr_updateInternal_m6F69EAADDCA9F3FC16D96351A97EB4D0DA935A76 (void);
// 0x0000011A System.Void LTDescr::callOnCompletes()
extern void LTDescr_callOnCompletes_m5F22E85C54CBAEA43EB1E2972B309DE885B6BC72 (void);
// 0x0000011B LTDescr LTDescr::setFromColor(UnityEngine.Color)
extern void LTDescr_setFromColor_m93EB8D7B171E28CC66A866DFE42C4637D17BF9B7 (void);
// 0x0000011C System.Void LTDescr::alphaRecursive(UnityEngine.Transform,System.Single,System.Boolean)
extern void LTDescr_alphaRecursive_m9862620727DE002BA5334449EE51E47FB9C35BDE (void);
// 0x0000011D System.Void LTDescr::colorRecursive(UnityEngine.Transform,UnityEngine.Color,System.Boolean)
extern void LTDescr_colorRecursive_m66AF77EFA7536C295518504A00B20968E3F773AD (void);
// 0x0000011E System.Void LTDescr::alphaRecursive(UnityEngine.RectTransform,System.Single,System.Int32)
extern void LTDescr_alphaRecursive_mDE10D7A23D1710400E453C787924FB83F89EFDDD (void);
// 0x0000011F System.Void LTDescr::alphaRecursiveSprite(UnityEngine.Transform,System.Single)
extern void LTDescr_alphaRecursiveSprite_mD677C77EFB7426B437BF89A60BE8D29B6E3ED014 (void);
// 0x00000120 System.Void LTDescr::colorRecursiveSprite(UnityEngine.Transform,UnityEngine.Color)
extern void LTDescr_colorRecursiveSprite_m2BFB41154D3FBB7D70FF5F2BBB8C3B0F4FC02699 (void);
// 0x00000121 System.Void LTDescr::colorRecursive(UnityEngine.RectTransform,UnityEngine.Color)
extern void LTDescr_colorRecursive_mDFBBC244E1A546B872218F43CE4D3E230C07DC35 (void);
// 0x00000122 System.Void LTDescr::textAlphaChildrenRecursive(UnityEngine.Transform,System.Single,System.Boolean)
extern void LTDescr_textAlphaChildrenRecursive_m30329E963CF1363F6841959DD91EED9FF7EA320B (void);
// 0x00000123 System.Void LTDescr::textAlphaRecursive(UnityEngine.Transform,System.Single,System.Boolean)
extern void LTDescr_textAlphaRecursive_m12B0431A56E4A23B20528CD399BFA2CE80658E3F (void);
// 0x00000124 System.Void LTDescr::textColorRecursive(UnityEngine.Transform,UnityEngine.Color)
extern void LTDescr_textColorRecursive_mFA16970EB10C436367105DAEF64A22B7C584CCF0 (void);
// 0x00000125 UnityEngine.Color LTDescr::tweenColor(LTDescr,System.Single)
extern void LTDescr_tweenColor_m3020E4C1CC76332688E53BF95D72BD1B3CB461A9 (void);
// 0x00000126 LTDescr LTDescr::pause()
extern void LTDescr_pause_m831C5988C87832DF30D6566086DD174849A8712A (void);
// 0x00000127 LTDescr LTDescr::resume()
extern void LTDescr_resume_mBE4F94BDD8338970F8DA5167345E87C4B58B30F8 (void);
// 0x00000128 LTDescr LTDescr::setAxis(UnityEngine.Vector3)
extern void LTDescr_setAxis_m786C5D48517CA736F5B704D876E4E69234E087E3 (void);
// 0x00000129 LTDescr LTDescr::setDelay(System.Single)
extern void LTDescr_setDelay_m55BD6D8AB740123B2EE42BA1721C7E6E29504110 (void);
// 0x0000012A LTDescr LTDescr::setEase(LeanTweenType)
extern void LTDescr_setEase_mDE953D5A1E2D1234C5CFD2F0CDB6F32B787ACD0C (void);
// 0x0000012B LTDescr LTDescr::setEaseLinear()
extern void LTDescr_setEaseLinear_m02CEA15BDCDC76F3FFE4AF8C8C7D4C4BE696A2E0 (void);
// 0x0000012C LTDescr LTDescr::setEaseSpring()
extern void LTDescr_setEaseSpring_mB49258E88824A2EF22CA9CFA751754D59C00646D (void);
// 0x0000012D LTDescr LTDescr::setEaseInQuad()
extern void LTDescr_setEaseInQuad_m1BD7E1556B02807399E80DB203B9553AA766B35E (void);
// 0x0000012E LTDescr LTDescr::setEaseOutQuad()
extern void LTDescr_setEaseOutQuad_m9FD9511C826BB2309FB7ADA82620AC454C207960 (void);
// 0x0000012F LTDescr LTDescr::setEaseInOutQuad()
extern void LTDescr_setEaseInOutQuad_mAD680A08BE87DD7F92202EEEA941DBD71A78B92A (void);
// 0x00000130 LTDescr LTDescr::setEaseInCubic()
extern void LTDescr_setEaseInCubic_mDB7AC25B20EDCDA9371911EFEA0D073548C13681 (void);
// 0x00000131 LTDescr LTDescr::setEaseOutCubic()
extern void LTDescr_setEaseOutCubic_m405E1A83E795CC30CA245DA33133F21D9FCD03A9 (void);
// 0x00000132 LTDescr LTDescr::setEaseInOutCubic()
extern void LTDescr_setEaseInOutCubic_mD69B9E20BD56C26709938CE025470378595D804A (void);
// 0x00000133 LTDescr LTDescr::setEaseInQuart()
extern void LTDescr_setEaseInQuart_m8B52F62F717ED83CB7CD00E869E548FC5C60E0FD (void);
// 0x00000134 LTDescr LTDescr::setEaseOutQuart()
extern void LTDescr_setEaseOutQuart_m926A771E75DCEF5025BA544D0D4814D802658967 (void);
// 0x00000135 LTDescr LTDescr::setEaseInOutQuart()
extern void LTDescr_setEaseInOutQuart_m36299EB57FE5C7B772B07760AD79A4758A407EAB (void);
// 0x00000136 LTDescr LTDescr::setEaseInQuint()
extern void LTDescr_setEaseInQuint_mC37282FD3528ABDE0B9ABBECD0EE4158EB78BB34 (void);
// 0x00000137 LTDescr LTDescr::setEaseOutQuint()
extern void LTDescr_setEaseOutQuint_mB5DF94AAAA9986D805ABD7B8705957F00C9FA298 (void);
// 0x00000138 LTDescr LTDescr::setEaseInOutQuint()
extern void LTDescr_setEaseInOutQuint_mD107E1CB8DA7D291E2507DE864FE3BA7690D3667 (void);
// 0x00000139 LTDescr LTDescr::setEaseInSine()
extern void LTDescr_setEaseInSine_m558572F5D792761C77311F93E823AA522ABFA821 (void);
// 0x0000013A LTDescr LTDescr::setEaseOutSine()
extern void LTDescr_setEaseOutSine_m04078632ABF4A13E48FFC566DADAE650E3531D8B (void);
// 0x0000013B LTDescr LTDescr::setEaseInOutSine()
extern void LTDescr_setEaseInOutSine_mCBF1B47223AB0E40EC462E99311B5BAC92CAF0F4 (void);
// 0x0000013C LTDescr LTDescr::setEaseInExpo()
extern void LTDescr_setEaseInExpo_m031D49646619C11E4CD9124E33ACDE7455963D61 (void);
// 0x0000013D LTDescr LTDescr::setEaseOutExpo()
extern void LTDescr_setEaseOutExpo_mD8C3A31B2E8E473F34744C5BD4855A28385777F5 (void);
// 0x0000013E LTDescr LTDescr::setEaseInOutExpo()
extern void LTDescr_setEaseInOutExpo_mC6247CF20D6C40011D6A31C5AFB4E93C2E27D83B (void);
// 0x0000013F LTDescr LTDescr::setEaseInCirc()
extern void LTDescr_setEaseInCirc_m64F3ED13ADA1CF100ADAD67A01A5C57FC073ECFF (void);
// 0x00000140 LTDescr LTDescr::setEaseOutCirc()
extern void LTDescr_setEaseOutCirc_m3FA5A19F0A8F63DE7F2FA3AE57A1E0DF371451DF (void);
// 0x00000141 LTDescr LTDescr::setEaseInOutCirc()
extern void LTDescr_setEaseInOutCirc_m82DC1F56BD70CAAA7CFC0786FC31565D4B7D1F8C (void);
// 0x00000142 LTDescr LTDescr::setEaseInBounce()
extern void LTDescr_setEaseInBounce_m0A8189AD4174381A97F215E75C5D40485395A549 (void);
// 0x00000143 LTDescr LTDescr::setEaseOutBounce()
extern void LTDescr_setEaseOutBounce_m3E4A3DBF4712B6F566D5FFC5B8AFD96DB06FA374 (void);
// 0x00000144 LTDescr LTDescr::setEaseInOutBounce()
extern void LTDescr_setEaseInOutBounce_mEC4B3DC43EE8829DB40045AE072895AB782B9FDC (void);
// 0x00000145 LTDescr LTDescr::setEaseInBack()
extern void LTDescr_setEaseInBack_m7A4BBFA0DA727FA51678D7B4561F588CFB1454E6 (void);
// 0x00000146 LTDescr LTDescr::setEaseOutBack()
extern void LTDescr_setEaseOutBack_m53F3499FE76CAEA5F462EC96113A265E15438418 (void);
// 0x00000147 LTDescr LTDescr::setEaseInOutBack()
extern void LTDescr_setEaseInOutBack_m1970B0DA7214B401D9894AD2BDC75A96016FDBBE (void);
// 0x00000148 LTDescr LTDescr::setEaseInElastic()
extern void LTDescr_setEaseInElastic_m9D356852FCC3BEB9002EC375047192B257CDE9A6 (void);
// 0x00000149 LTDescr LTDescr::setEaseOutElastic()
extern void LTDescr_setEaseOutElastic_m97DCC808E07D990199DD09AE961EF7CFEB401698 (void);
// 0x0000014A LTDescr LTDescr::setEaseInOutElastic()
extern void LTDescr_setEaseInOutElastic_m1CC9921334D9A64FF1F4C2E5285F3E5712C6D401 (void);
// 0x0000014B LTDescr LTDescr::setEasePunch()
extern void LTDescr_setEasePunch_m1DF3D2CFEE5E2CB6EEAF5F485DF326F8B2A08B66 (void);
// 0x0000014C LTDescr LTDescr::setEaseShake()
extern void LTDescr_setEaseShake_mAC2E54A2A115410297235E85E9567AD5D96CC86E (void);
// 0x0000014D UnityEngine.Vector3 LTDescr::tweenOnCurve()
extern void LTDescr_tweenOnCurve_mAAE98016598CDD3C0E19EC9016CDAAC27290FE40 (void);
// 0x0000014E UnityEngine.Vector3 LTDescr::easeInOutQuad()
extern void LTDescr_easeInOutQuad_mA1B3167A272546E8F4AD51BA4456D911E7A97CAA (void);
// 0x0000014F UnityEngine.Vector3 LTDescr::easeInQuad()
extern void LTDescr_easeInQuad_m8B8A3D80C2483095A2E9319E0E560A16A5D45E03 (void);
// 0x00000150 UnityEngine.Vector3 LTDescr::easeOutQuad()
extern void LTDescr_easeOutQuad_mA14EA059114AEE2C71D891DA1D723554387FB0FC (void);
// 0x00000151 UnityEngine.Vector3 LTDescr::easeLinear()
extern void LTDescr_easeLinear_mAFF573D79A9494A37F171FDCFB1A843E4A099BBD (void);
// 0x00000152 UnityEngine.Vector3 LTDescr::easeSpring()
extern void LTDescr_easeSpring_mBCC692E63F1CA607883E42FD5EF44799798CF544 (void);
// 0x00000153 UnityEngine.Vector3 LTDescr::easeInCubic()
extern void LTDescr_easeInCubic_mA1F3409DFC4FAB01F0E1D7D5C13AF3EF533A592C (void);
// 0x00000154 UnityEngine.Vector3 LTDescr::easeOutCubic()
extern void LTDescr_easeOutCubic_mD3D32B7F8A774C06F28C78EFD8B35F82ABE0B2BD (void);
// 0x00000155 UnityEngine.Vector3 LTDescr::easeInOutCubic()
extern void LTDescr_easeInOutCubic_m10079E2EE4532DD46450DE324ACA7091D4082449 (void);
// 0x00000156 UnityEngine.Vector3 LTDescr::easeInQuart()
extern void LTDescr_easeInQuart_m7F3F07BAF2296FE3EF14D9315A45B101B4A88C48 (void);
// 0x00000157 UnityEngine.Vector3 LTDescr::easeOutQuart()
extern void LTDescr_easeOutQuart_m19C687BA558F3962E3D94A9224DFB21AEB8DB6B4 (void);
// 0x00000158 UnityEngine.Vector3 LTDescr::easeInOutQuart()
extern void LTDescr_easeInOutQuart_m4A83DAC8CFD32AC6A3F5BE9847FAB5B397567EA0 (void);
// 0x00000159 UnityEngine.Vector3 LTDescr::easeInQuint()
extern void LTDescr_easeInQuint_m8DA64234B559AEBF6F300EF6FB4A168247F54DD3 (void);
// 0x0000015A UnityEngine.Vector3 LTDescr::easeOutQuint()
extern void LTDescr_easeOutQuint_m72712C773DCFA24B83AF2013806A795B3874B4E2 (void);
// 0x0000015B UnityEngine.Vector3 LTDescr::easeInOutQuint()
extern void LTDescr_easeInOutQuint_m860D58335F979A03E4BDA78BA9D37BF3F9CAE3C0 (void);
// 0x0000015C UnityEngine.Vector3 LTDescr::easeInSine()
extern void LTDescr_easeInSine_mCF9996B096BEB3C47C523DB7DC3DDE5EEEB362CF (void);
// 0x0000015D UnityEngine.Vector3 LTDescr::easeOutSine()
extern void LTDescr_easeOutSine_m56F54AD9F205357269FB50FC3DB13551BBDE7AA8 (void);
// 0x0000015E UnityEngine.Vector3 LTDescr::easeInOutSine()
extern void LTDescr_easeInOutSine_m4ACFEF2C6BDF120DAA6C76E563B6AE6CECD5380F (void);
// 0x0000015F UnityEngine.Vector3 LTDescr::easeInExpo()
extern void LTDescr_easeInExpo_m07C8C2CF2B118A52CBDC361310B8D8C3DF366A52 (void);
// 0x00000160 UnityEngine.Vector3 LTDescr::easeOutExpo()
extern void LTDescr_easeOutExpo_mA866024A98FD2A2CC8808917B983BE6D3BF4156A (void);
// 0x00000161 UnityEngine.Vector3 LTDescr::easeInOutExpo()
extern void LTDescr_easeInOutExpo_mBEC5D63CA1D9C4E0F6EB2C0935E6D2550E8FBBC9 (void);
// 0x00000162 UnityEngine.Vector3 LTDescr::easeInCirc()
extern void LTDescr_easeInCirc_m5DB7FCD1C107646BCBF7BACAE93191864E5A7BDA (void);
// 0x00000163 UnityEngine.Vector3 LTDescr::easeOutCirc()
extern void LTDescr_easeOutCirc_mEA8558C7773468D1860A2BDC636FA5EC2C8791D8 (void);
// 0x00000164 UnityEngine.Vector3 LTDescr::easeInOutCirc()
extern void LTDescr_easeInOutCirc_m49B59C6FB90730D195CFE5910C1A1596F18385C3 (void);
// 0x00000165 UnityEngine.Vector3 LTDescr::easeInBounce()
extern void LTDescr_easeInBounce_mA53E96538907F54DF3E668026D7F62D8A346B325 (void);
// 0x00000166 UnityEngine.Vector3 LTDescr::easeOutBounce()
extern void LTDescr_easeOutBounce_mB2732EE22499FB0ED83AFEF85FD4896E629A19C8 (void);
// 0x00000167 UnityEngine.Vector3 LTDescr::easeInOutBounce()
extern void LTDescr_easeInOutBounce_m5F2B5E4F09C66FA720583B13EE7DEBF46E4B4250 (void);
// 0x00000168 UnityEngine.Vector3 LTDescr::easeInBack()
extern void LTDescr_easeInBack_m1AD66A3F3E0513ACF1D06C01909DB989D263ACFC (void);
// 0x00000169 UnityEngine.Vector3 LTDescr::easeOutBack()
extern void LTDescr_easeOutBack_m971AF92F07A84D5E01B10F1CED6F43F02217CA0B (void);
// 0x0000016A UnityEngine.Vector3 LTDescr::easeInOutBack()
extern void LTDescr_easeInOutBack_m9AEAAB6088AA50FAE1F839D39D70B79250467DC1 (void);
// 0x0000016B UnityEngine.Vector3 LTDescr::easeInElastic()
extern void LTDescr_easeInElastic_mA980A8C764DFBC3A2DCE4CC566A3AE5D8DBB4F15 (void);
// 0x0000016C UnityEngine.Vector3 LTDescr::easeOutElastic()
extern void LTDescr_easeOutElastic_m4E6536A1BEA97546DDB61146FD6A78BBF31E1207 (void);
// 0x0000016D UnityEngine.Vector3 LTDescr::easeInOutElastic()
extern void LTDescr_easeInOutElastic_mA4E464B18D106514E4092266A28D8EA4138858CA (void);
// 0x0000016E LTDescr LTDescr::setOvershoot(System.Single)
extern void LTDescr_setOvershoot_m47B052637792C78A0D5195306CCA86CB9E7A96A8 (void);
// 0x0000016F LTDescr LTDescr::setPeriod(System.Single)
extern void LTDescr_setPeriod_mC03D996C9712715E08646FDBD5421719CFB8F3DE (void);
// 0x00000170 LTDescr LTDescr::setScale(System.Single)
extern void LTDescr_setScale_m20ABE114A3769DF22F968A3BAE3B969D7405C84D (void);
// 0x00000171 LTDescr LTDescr::setEase(UnityEngine.AnimationCurve)
extern void LTDescr_setEase_m85FF6788C6DD67CB57F667E80E456C7CE86C9E3F (void);
// 0x00000172 LTDescr LTDescr::setTo(UnityEngine.Vector3)
extern void LTDescr_setTo_mC614978FF31785D8135380F7BF1FEF8ADD2F365D (void);
// 0x00000173 LTDescr LTDescr::setTo(UnityEngine.Transform)
extern void LTDescr_setTo_mFE5077089EAAEED3A0B4E00827BF4867FA5E4E76 (void);
// 0x00000174 LTDescr LTDescr::setFrom(UnityEngine.Vector3)
extern void LTDescr_setFrom_mBFD923D83E18FB53AAFB9293C1F13175DCE15D34 (void);
// 0x00000175 LTDescr LTDescr::setFrom(System.Single)
extern void LTDescr_setFrom_m82D22FCE616A6826A3007BCF148DA437F16BC0F8 (void);
// 0x00000176 LTDescr LTDescr::setDiff(UnityEngine.Vector3)
extern void LTDescr_setDiff_mAF9E8982F70181B987022FA76170A8DD7C61FA5E (void);
// 0x00000177 LTDescr LTDescr::setHasInitialized(System.Boolean)
extern void LTDescr_setHasInitialized_mC54A9AAD341DE33BC04734061E6EFE42E89402F6 (void);
// 0x00000178 LTDescr LTDescr::setId(System.UInt32,System.UInt32)
extern void LTDescr_setId_mB492FBFB9D2FE6ED7CDD7EEA9F3A547A4BC596D0 (void);
// 0x00000179 LTDescr LTDescr::setPassed(System.Single)
extern void LTDescr_setPassed_mC3016CADF8995A54ABF2D5D95908EE63D3298E65 (void);
// 0x0000017A LTDescr LTDescr::setTime(System.Single)
extern void LTDescr_setTime_m0DE4EDC9DA5F9D6B5255BCA73A198E18F0A320A4 (void);
// 0x0000017B LTDescr LTDescr::setSpeed(System.Single)
extern void LTDescr_setSpeed_mF8F0031D23893E09F0E258367FEA33FB175CC269 (void);
// 0x0000017C LTDescr LTDescr::setRepeat(System.Int32)
extern void LTDescr_setRepeat_m7D66B000EF60DCC943D848C1BF53BD7C7BEF8313 (void);
// 0x0000017D LTDescr LTDescr::setLoopType(LeanTweenType)
extern void LTDescr_setLoopType_mF2FC7F646DDB9AD46FE9FB3972C4B2EAA5634C77 (void);
// 0x0000017E LTDescr LTDescr::setUseEstimatedTime(System.Boolean)
extern void LTDescr_setUseEstimatedTime_m1882A739CA3F9CE2F6215C61E92D4D5108F352EE (void);
// 0x0000017F LTDescr LTDescr::setIgnoreTimeScale(System.Boolean)
extern void LTDescr_setIgnoreTimeScale_mDD2DF875144DFBA017697139ECD83164182DD6C9 (void);
// 0x00000180 LTDescr LTDescr::setUseFrames(System.Boolean)
extern void LTDescr_setUseFrames_m356FC10CEEBD2F74F93F14EDB27B58A87C67901E (void);
// 0x00000181 LTDescr LTDescr::setUseManualTime(System.Boolean)
extern void LTDescr_setUseManualTime_mAC4545BA99BA9C7117151D155C4664F6D7E933D2 (void);
// 0x00000182 LTDescr LTDescr::setLoopCount(System.Int32)
extern void LTDescr_setLoopCount_m658F924D41197D6DDDEAC18C37849D2341C1C993 (void);
// 0x00000183 LTDescr LTDescr::setLoopOnce()
extern void LTDescr_setLoopOnce_mBF4016C6E09D73B6C4210585FEC20D250BBEBE60 (void);
// 0x00000184 LTDescr LTDescr::setLoopClamp()
extern void LTDescr_setLoopClamp_m596650592727CC3F3C5513F54432D93CEA45666E (void);
// 0x00000185 LTDescr LTDescr::setLoopClamp(System.Int32)
extern void LTDescr_setLoopClamp_mEA5F7A4F29DFEC798D1DBF7CACC4EA95E5681733 (void);
// 0x00000186 LTDescr LTDescr::setLoopPingPong()
extern void LTDescr_setLoopPingPong_mB14D4AD3878CB744ADBB3F0B933E1C50CACEDC18 (void);
// 0x00000187 LTDescr LTDescr::setLoopPingPong(System.Int32)
extern void LTDescr_setLoopPingPong_mDBEA094A372AACBB02EFFA679DBF2C551928140F (void);
// 0x00000188 LTDescr LTDescr::setOnComplete(System.Action)
extern void LTDescr_setOnComplete_mBD0B6BAC2B05C7AE12E93F478BC6F0F41A33C44F (void);
// 0x00000189 LTDescr LTDescr::setOnComplete(System.Action`1<System.Object>)
extern void LTDescr_setOnComplete_m61B62A9BB1DE6DC69EB97F111139AB379A2AB33C (void);
// 0x0000018A LTDescr LTDescr::setOnComplete(System.Action`1<System.Object>,System.Object)
extern void LTDescr_setOnComplete_mD8043AC13DAB80A85715B3B5D2E273754A93157D (void);
// 0x0000018B LTDescr LTDescr::setOnCompleteParam(System.Object)
extern void LTDescr_setOnCompleteParam_m0DF2C3997947B6F72DFD4AA1BAEC841B8B84DD63 (void);
// 0x0000018C LTDescr LTDescr::setOnUpdate(System.Action`1<System.Single>)
extern void LTDescr_setOnUpdate_m55FBE2275AB200DCFB3793FA745A238491E7072B (void);
// 0x0000018D LTDescr LTDescr::setOnUpdateRatio(System.Action`2<System.Single,System.Single>)
extern void LTDescr_setOnUpdateRatio_m52F3EB3754460E5DA2EC48CDD69FAEF7953CC1EB (void);
// 0x0000018E LTDescr LTDescr::setOnUpdateObject(System.Action`2<System.Single,System.Object>)
extern void LTDescr_setOnUpdateObject_m5B410690D035323DF02535DBFDD06EFE602E4F45 (void);
// 0x0000018F LTDescr LTDescr::setOnUpdateVector2(System.Action`1<UnityEngine.Vector2>)
extern void LTDescr_setOnUpdateVector2_m0FB697AF241FBDB5958D2FA52C5BF964FDC9F3A8 (void);
// 0x00000190 LTDescr LTDescr::setOnUpdateVector3(System.Action`1<UnityEngine.Vector3>)
extern void LTDescr_setOnUpdateVector3_mFFF939DCA282CC77307788DDE78DAC3FE8801920 (void);
// 0x00000191 LTDescr LTDescr::setOnUpdateColor(System.Action`1<UnityEngine.Color>)
extern void LTDescr_setOnUpdateColor_m162951739730174C9B12F55091940A39981C1BF7 (void);
// 0x00000192 LTDescr LTDescr::setOnUpdateColor(System.Action`2<UnityEngine.Color,System.Object>)
extern void LTDescr_setOnUpdateColor_m3CE8B6B35EA308221FF7739A6DDFFE8B308DAAAB (void);
// 0x00000193 LTDescr LTDescr::setOnUpdate(System.Action`1<UnityEngine.Color>)
extern void LTDescr_setOnUpdate_mB7154EAF06F1366C24E3E8EABFDF53169A1EC1B5 (void);
// 0x00000194 LTDescr LTDescr::setOnUpdate(System.Action`2<UnityEngine.Color,System.Object>)
extern void LTDescr_setOnUpdate_m487CA9638173DAF55C9DF3B81F7D97232222D88B (void);
// 0x00000195 LTDescr LTDescr::setOnUpdate(System.Action`2<System.Single,System.Object>,System.Object)
extern void LTDescr_setOnUpdate_m8A27C956F125846A8713234EDB95FCA045C3BF50 (void);
// 0x00000196 LTDescr LTDescr::setOnUpdate(System.Action`2<UnityEngine.Vector3,System.Object>,System.Object)
extern void LTDescr_setOnUpdate_mF6CF9481ADCA4D3270B5E21634B8D924D1FD7DD2 (void);
// 0x00000197 LTDescr LTDescr::setOnUpdate(System.Action`1<UnityEngine.Vector2>,System.Object)
extern void LTDescr_setOnUpdate_m5404E20426F6927BD37BD1EA3B0BBBF1642CE37F (void);
// 0x00000198 LTDescr LTDescr::setOnUpdate(System.Action`1<UnityEngine.Vector3>,System.Object)
extern void LTDescr_setOnUpdate_m5356685ADFD38D45881211740E990617C0F2A80D (void);
// 0x00000199 LTDescr LTDescr::setOnUpdateParam(System.Object)
extern void LTDescr_setOnUpdateParam_mE61939C5A8C86D2603FD2F9ED73ACF3CF1FB1D69 (void);
// 0x0000019A LTDescr LTDescr::setOrientToPath(System.Boolean)
extern void LTDescr_setOrientToPath_mECE38AB1BC1313F39FD2FE3D582AB18A849826EE (void);
// 0x0000019B LTDescr LTDescr::setOrientToPath2d(System.Boolean)
extern void LTDescr_setOrientToPath2d_m08D9F50871013C10D1961AFF0A7759A8D92B1294 (void);
// 0x0000019C LTDescr LTDescr::setRect(LTRect)
extern void LTDescr_setRect_m1E626946F828ADB3D7E7A70D4E43B1CA7B2009F3 (void);
// 0x0000019D LTDescr LTDescr::setRect(UnityEngine.Rect)
extern void LTDescr_setRect_m98165DCDF51B68BCB9CAEC7EAC5C364D2E26758A (void);
// 0x0000019E LTDescr LTDescr::setPath(LTBezierPath)
extern void LTDescr_setPath_m9BFB77CBAE51ED839078FD49C7611ED8323AC154 (void);
// 0x0000019F LTDescr LTDescr::setPoint(UnityEngine.Vector3)
extern void LTDescr_setPoint_mB0744AAA4A452543F68A3126C05CD8487BEF7F6E (void);
// 0x000001A0 LTDescr LTDescr::setDestroyOnComplete(System.Boolean)
extern void LTDescr_setDestroyOnComplete_m697253D565C9D2CDA484A82E1005660595CEDA06 (void);
// 0x000001A1 LTDescr LTDescr::setAudio(System.Object)
extern void LTDescr_setAudio_m86759A48D1B7D26395A5B28AB5450E84AE46AECE (void);
// 0x000001A2 LTDescr LTDescr::setOnCompleteOnRepeat(System.Boolean)
extern void LTDescr_setOnCompleteOnRepeat_m28F4EE66B0CA50AC6FA5A9391EEAD0B62E85A3C1 (void);
// 0x000001A3 LTDescr LTDescr::setOnCompleteOnStart(System.Boolean)
extern void LTDescr_setOnCompleteOnStart_m2FB448E21905E33DCE1F35EBBD8E39097AC35938 (void);
// 0x000001A4 LTDescr LTDescr::setRect(UnityEngine.RectTransform)
extern void LTDescr_setRect_m0CDE3901FF03E6216DD7E6DEC6E65E7DEAF39AB6 (void);
// 0x000001A5 LTDescr LTDescr::setSprites(UnityEngine.Sprite[])
extern void LTDescr_setSprites_mC83C58D4DC03CDB0E8529A2DCDE37270089B0A41 (void);
// 0x000001A6 LTDescr LTDescr::setFrameRate(System.Single)
extern void LTDescr_setFrameRate_m0A569EDD28DDFD74BD71E0E5CF4730F178220AF9 (void);
// 0x000001A7 LTDescr LTDescr::setOnStart(System.Action)
extern void LTDescr_setOnStart_m37383DCD917312D49E2291F3A47F8F9361155A1A (void);
// 0x000001A8 LTDescr LTDescr::setDirection(System.Single)
extern void LTDescr_setDirection_m3B76149226B433591EC98766C8B14D63BDB7F193 (void);
// 0x000001A9 LTDescr LTDescr::setRecursive(System.Boolean)
extern void LTDescr_setRecursive_mC2895C6EEB59EF79044495FA8BB6ACE914CAB402 (void);
// 0x000001AA System.Void LTDescr::<setMoveX>b__73_0()
extern void LTDescr_U3CsetMoveXU3Eb__73_0_m39FBF744669510650F08E0F2B4E87CDEECA7440A (void);
// 0x000001AB System.Void LTDescr::<setMoveX>b__73_1()
extern void LTDescr_U3CsetMoveXU3Eb__73_1_mB4AD58106C3D205E5D4763F3F1CCA63B2F99EB78 (void);
// 0x000001AC System.Void LTDescr::<setMoveY>b__74_0()
extern void LTDescr_U3CsetMoveYU3Eb__74_0_mA6309FBB2AF9AEEB859024E69AC8ABD6D102AD87 (void);
// 0x000001AD System.Void LTDescr::<setMoveY>b__74_1()
extern void LTDescr_U3CsetMoveYU3Eb__74_1_m7A2A40165003571F84F057D3F39BD77A23B2F0E9 (void);
// 0x000001AE System.Void LTDescr::<setMoveZ>b__75_0()
extern void LTDescr_U3CsetMoveZU3Eb__75_0_mFE473C92996CA9EBA69397DA460F4B16EDE7D03C (void);
// 0x000001AF System.Void LTDescr::<setMoveZ>b__75_1()
extern void LTDescr_U3CsetMoveZU3Eb__75_1_mF78C868A90B38D04ADAD2AD93D536B1791B3C671 (void);
// 0x000001B0 System.Void LTDescr::<setMoveLocalX>b__76_0()
extern void LTDescr_U3CsetMoveLocalXU3Eb__76_0_m8A895E545E84F266EA0E0963C68274D9545D17BE (void);
// 0x000001B1 System.Void LTDescr::<setMoveLocalX>b__76_1()
extern void LTDescr_U3CsetMoveLocalXU3Eb__76_1_m17F56B25DDE0E4E6D6CC186BF9A9A617957C2FD6 (void);
// 0x000001B2 System.Void LTDescr::<setMoveLocalY>b__77_0()
extern void LTDescr_U3CsetMoveLocalYU3Eb__77_0_m8D00D189D0ADCF6C778EFF39DCA1EA55C03E34F4 (void);
// 0x000001B3 System.Void LTDescr::<setMoveLocalY>b__77_1()
extern void LTDescr_U3CsetMoveLocalYU3Eb__77_1_m67ADD5EAA303DB53DA4B896F2080F3E6975C76A6 (void);
// 0x000001B4 System.Void LTDescr::<setMoveLocalZ>b__78_0()
extern void LTDescr_U3CsetMoveLocalZU3Eb__78_0_mFD9EBD1D9EF08CB93FDCCE06D88DC940852C03DF (void);
// 0x000001B5 System.Void LTDescr::<setMoveLocalZ>b__78_1()
extern void LTDescr_U3CsetMoveLocalZU3Eb__78_1_mF3D05D5EA84618939D8F8C0F9F06EE5D6294D77D (void);
// 0x000001B6 System.Void LTDescr::<setMoveCurved>b__81_0()
extern void LTDescr_U3CsetMoveCurvedU3Eb__81_0_m8BD948AD31BE03BCEF9D75091AECC3DF8B4A50E3 (void);
// 0x000001B7 System.Void LTDescr::<setMoveCurvedLocal>b__82_0()
extern void LTDescr_U3CsetMoveCurvedLocalU3Eb__82_0_mE35CF92421F9DBB5687F1F1608AFBE9F86C80378 (void);
// 0x000001B8 System.Void LTDescr::<setMoveSpline>b__83_0()
extern void LTDescr_U3CsetMoveSplineU3Eb__83_0_mD9ACDB850FD978554B5465139A4E1EDD844CA1BB (void);
// 0x000001B9 System.Void LTDescr::<setMoveSplineLocal>b__84_0()
extern void LTDescr_U3CsetMoveSplineLocalU3Eb__84_0_m3219BB023C406080046DE37EBCF0CEDB8E323D1A (void);
// 0x000001BA System.Void LTDescr::<setScaleX>b__85_0()
extern void LTDescr_U3CsetScaleXU3Eb__85_0_mDF33F215E49CCD02AE02FDD79827006CB41DF18F (void);
// 0x000001BB System.Void LTDescr::<setScaleX>b__85_1()
extern void LTDescr_U3CsetScaleXU3Eb__85_1_m332A3ADAFDC200BF9AC11279593B5968DC537623 (void);
// 0x000001BC System.Void LTDescr::<setScaleY>b__86_0()
extern void LTDescr_U3CsetScaleYU3Eb__86_0_m6A34E540DCDF955D835FCBD7FEF708257E07FE31 (void);
// 0x000001BD System.Void LTDescr::<setScaleY>b__86_1()
extern void LTDescr_U3CsetScaleYU3Eb__86_1_mE1BCEE0DADE70D6CA3873140143141D9B51DBDB5 (void);
// 0x000001BE System.Void LTDescr::<setScaleZ>b__87_0()
extern void LTDescr_U3CsetScaleZU3Eb__87_0_mAEDB3F2903B250EC9885F64B147EDBDD4881E812 (void);
// 0x000001BF System.Void LTDescr::<setScaleZ>b__87_1()
extern void LTDescr_U3CsetScaleZU3Eb__87_1_mEFAE5A48C329D33CAF5F7003D48DD0B9C581AB85 (void);
// 0x000001C0 System.Void LTDescr::<setRotateX>b__88_0()
extern void LTDescr_U3CsetRotateXU3Eb__88_0_mD0976C92A94804F53718DEE26B131989A067EF7A (void);
// 0x000001C1 System.Void LTDescr::<setRotateX>b__88_1()
extern void LTDescr_U3CsetRotateXU3Eb__88_1_m7C171EAF890568C630E3BE1EC70C97A0C55F0984 (void);
// 0x000001C2 System.Void LTDescr::<setRotateY>b__89_0()
extern void LTDescr_U3CsetRotateYU3Eb__89_0_m1B2E4D04C35A72D3957FEB294B306BF741228F1B (void);
// 0x000001C3 System.Void LTDescr::<setRotateY>b__89_1()
extern void LTDescr_U3CsetRotateYU3Eb__89_1_m67AC1C0BCE4902B58FA999FC9475BF42D391F167 (void);
// 0x000001C4 System.Void LTDescr::<setRotateZ>b__90_0()
extern void LTDescr_U3CsetRotateZU3Eb__90_0_m92CFF50F82E8606EA58D1FF72D2FBD7C9A0C7290 (void);
// 0x000001C5 System.Void LTDescr::<setRotateZ>b__90_1()
extern void LTDescr_U3CsetRotateZU3Eb__90_1_mA55CC8D92F2601E04F21F45337134FBE437A20B6 (void);
// 0x000001C6 System.Void LTDescr::<setRotateAround>b__91_0()
extern void LTDescr_U3CsetRotateAroundU3Eb__91_0_m90372B54B5D631FC5E1FF67FCE64F69A1E0E7E63 (void);
// 0x000001C7 System.Void LTDescr::<setRotateAround>b__91_1()
extern void LTDescr_U3CsetRotateAroundU3Eb__91_1_m21A791E3630653D459153347755624070DC38965 (void);
// 0x000001C8 System.Void LTDescr::<setRotateAroundLocal>b__92_0()
extern void LTDescr_U3CsetRotateAroundLocalU3Eb__92_0_m49A28FAA34B36D7AA09A322D63974997CB31BF43 (void);
// 0x000001C9 System.Void LTDescr::<setRotateAroundLocal>b__92_1()
extern void LTDescr_U3CsetRotateAroundLocalU3Eb__92_1_mCB4C19C5A763BBF640E5BE6785D5AB03859AD606 (void);
// 0x000001CA System.Void LTDescr::<setAlpha>b__93_0()
extern void LTDescr_U3CsetAlphaU3Eb__93_0_mADB8DE8A613762F896CC7754539BFFCA3A23B4E9 (void);
// 0x000001CB System.Void LTDescr::<setAlpha>b__93_2()
extern void LTDescr_U3CsetAlphaU3Eb__93_2_m5318309F91340E1C143196D9ADA659C53B3C918C (void);
// 0x000001CC System.Void LTDescr::<setAlpha>b__93_1()
extern void LTDescr_U3CsetAlphaU3Eb__93_1_m44E2198D69A4F502CE6F5B1532AFD8F600D84717 (void);
// 0x000001CD System.Void LTDescr::<setTextAlpha>b__94_0()
extern void LTDescr_U3CsetTextAlphaU3Eb__94_0_mE551CB2CBB6114CA72656A033CAC42EAB2EFCC8B (void);
// 0x000001CE System.Void LTDescr::<setTextAlpha>b__94_1()
extern void LTDescr_U3CsetTextAlphaU3Eb__94_1_m809E895E8E9ED4049E44ACAEEFE222EBA2E6A259 (void);
// 0x000001CF System.Void LTDescr::<setAlphaVertex>b__95_0()
extern void LTDescr_U3CsetAlphaVertexU3Eb__95_0_m53AB148A8D470C29DACB58274436B9535F5CAC37 (void);
// 0x000001D0 System.Void LTDescr::<setAlphaVertex>b__95_1()
extern void LTDescr_U3CsetAlphaVertexU3Eb__95_1_m197F71CB84DF1265DDDEC92AB60CFAEC2B2E8BE9 (void);
// 0x000001D1 System.Void LTDescr::<setColor>b__96_0()
extern void LTDescr_U3CsetColorU3Eb__96_0_mF63A06E57C3B0A1B5B8FE3E7EF131BD862457B19 (void);
// 0x000001D2 System.Void LTDescr::<setColor>b__96_1()
extern void LTDescr_U3CsetColorU3Eb__96_1_m8EA4C07F32943F895BE0CB6909C63263827CEBF8 (void);
// 0x000001D3 System.Void LTDescr::<setCallbackColor>b__97_0()
extern void LTDescr_U3CsetCallbackColorU3Eb__97_0_mAD825635FB3FD1DE688BD692F394C91BD701659F (void);
// 0x000001D4 System.Void LTDescr::<setCallbackColor>b__97_1()
extern void LTDescr_U3CsetCallbackColorU3Eb__97_1_m3635B1AEC7852BD361B57DAC85568D84BB050C38 (void);
// 0x000001D5 System.Void LTDescr::<setTextColor>b__98_0()
extern void LTDescr_U3CsetTextColorU3Eb__98_0_m1AC4C496FB90FA9C994A7D8772EE75A96D8C7499 (void);
// 0x000001D6 System.Void LTDescr::<setTextColor>b__98_1()
extern void LTDescr_U3CsetTextColorU3Eb__98_1_mF8C5941A01CC6AB44A3C1A9E4E0D42DE0019B1B5 (void);
// 0x000001D7 System.Void LTDescr::<setCanvasAlpha>b__99_0()
extern void LTDescr_U3CsetCanvasAlphaU3Eb__99_0_m03D22099CB93845FEE24F439C52C194CF424307B (void);
// 0x000001D8 System.Void LTDescr::<setCanvasAlpha>b__99_1()
extern void LTDescr_U3CsetCanvasAlphaU3Eb__99_1_mC255CF9153C3E078BCC1E35C16E786C8662EBFD8 (void);
// 0x000001D9 System.Void LTDescr::<setCanvasGroupAlpha>b__100_0()
extern void LTDescr_U3CsetCanvasGroupAlphaU3Eb__100_0_mCD1535F45FF98C078658D56A058E4D44D7CD47F2 (void);
// 0x000001DA System.Void LTDescr::<setCanvasGroupAlpha>b__100_1()
extern void LTDescr_U3CsetCanvasGroupAlphaU3Eb__100_1_m6EBBBBA42BCC9B435DB26932FCCD7DF904E3D31E (void);
// 0x000001DB System.Void LTDescr::<setCanvasColor>b__101_0()
extern void LTDescr_U3CsetCanvasColorU3Eb__101_0_mC7AB0F758514D92910F073FB38A41C395E6764E3 (void);
// 0x000001DC System.Void LTDescr::<setCanvasColor>b__101_1()
extern void LTDescr_U3CsetCanvasColorU3Eb__101_1_m0DC35761F636C62A4B9A9D9B7C8EEFD9B30F3CA4 (void);
// 0x000001DD System.Void LTDescr::<setCanvasMoveX>b__102_0()
extern void LTDescr_U3CsetCanvasMoveXU3Eb__102_0_m0E72A6C3F16FC694B6D54907490ECA5D6D359776 (void);
// 0x000001DE System.Void LTDescr::<setCanvasMoveX>b__102_1()
extern void LTDescr_U3CsetCanvasMoveXU3Eb__102_1_m7FF531D0062E0803860A0CD456E6DD6AE30BD00D (void);
// 0x000001DF System.Void LTDescr::<setCanvasMoveY>b__103_0()
extern void LTDescr_U3CsetCanvasMoveYU3Eb__103_0_m4730D57FCDEA041D3F4111E45E34C15149C4BD4C (void);
// 0x000001E0 System.Void LTDescr::<setCanvasMoveY>b__103_1()
extern void LTDescr_U3CsetCanvasMoveYU3Eb__103_1_m9CF415CA444CEBB9B5B6C01B337E1C4598B74A32 (void);
// 0x000001E1 System.Void LTDescr::<setCanvasMoveZ>b__104_0()
extern void LTDescr_U3CsetCanvasMoveZU3Eb__104_0_mDCD6897BB64C355669F8EE9CFD8F2B386F830CA2 (void);
// 0x000001E2 System.Void LTDescr::<setCanvasMoveZ>b__104_1()
extern void LTDescr_U3CsetCanvasMoveZU3Eb__104_1_m5B12F7E220DA96B6F05025A4BE1AFABDEEAFF853 (void);
// 0x000001E3 System.Void LTDescr::<setCanvasRotateAround>b__106_0()
extern void LTDescr_U3CsetCanvasRotateAroundU3Eb__106_0_m7254F615E75865226968EA098B90FE904EE5B38B (void);
// 0x000001E4 System.Void LTDescr::<setCanvasRotateAroundLocal>b__107_0()
extern void LTDescr_U3CsetCanvasRotateAroundLocalU3Eb__107_0_m2C7BE76A27280304C47F1787199EA2790E10D6D6 (void);
// 0x000001E5 System.Void LTDescr::<setCanvasPlaySprite>b__108_0()
extern void LTDescr_U3CsetCanvasPlaySpriteU3Eb__108_0_m60117A734E7707143FD04D2FBEE6B75618B258DF (void);
// 0x000001E6 System.Void LTDescr::<setCanvasPlaySprite>b__108_1()
extern void LTDescr_U3CsetCanvasPlaySpriteU3Eb__108_1_mAA1E46E79560810DA46AE8EA0BFA91EDAE3D2DA5 (void);
// 0x000001E7 System.Void LTDescr::<setCanvasMove>b__109_0()
extern void LTDescr_U3CsetCanvasMoveU3Eb__109_0_mABE1F4262412CA7E93AA4B71881FAE5B817FCF8D (void);
// 0x000001E8 System.Void LTDescr::<setCanvasMove>b__109_1()
extern void LTDescr_U3CsetCanvasMoveU3Eb__109_1_mD190E0829649FEA47BAB95DED30B6EAB95C03ECF (void);
// 0x000001E9 System.Void LTDescr::<setCanvasScale>b__110_0()
extern void LTDescr_U3CsetCanvasScaleU3Eb__110_0_mF68FF782BFAD2E93A65661953E683EADC9A52DA0 (void);
// 0x000001EA System.Void LTDescr::<setCanvasScale>b__110_1()
extern void LTDescr_U3CsetCanvasScaleU3Eb__110_1_mCF2CCB10E46FC5F00061B020C857E7A35EF14E0D (void);
// 0x000001EB System.Void LTDescr::<setCanvasSizeDelta>b__111_0()
extern void LTDescr_U3CsetCanvasSizeDeltaU3Eb__111_0_m021163E83008DE7874346B03AE065037CDC34D3A (void);
// 0x000001EC System.Void LTDescr::<setCanvasSizeDelta>b__111_1()
extern void LTDescr_U3CsetCanvasSizeDeltaU3Eb__111_1_m9AA7511D4E1BF277C282F3E30388E82D0A9E1C0B (void);
// 0x000001ED System.Void LTDescr::<setMove>b__115_0()
extern void LTDescr_U3CsetMoveU3Eb__115_0_m5DDD8D43FEBB5099535893334FD76E1F0DAFD21B (void);
// 0x000001EE System.Void LTDescr::<setMove>b__115_1()
extern void LTDescr_U3CsetMoveU3Eb__115_1_m6B66D8C95AE011E8C641FA4AEF0245809F334399 (void);
// 0x000001EF System.Void LTDescr::<setMoveLocal>b__116_0()
extern void LTDescr_U3CsetMoveLocalU3Eb__116_0_m7FE28EDDA5832CD6FF6BB2370DA6059DF23B2A1F (void);
// 0x000001F0 System.Void LTDescr::<setMoveLocal>b__116_1()
extern void LTDescr_U3CsetMoveLocalU3Eb__116_1_m8D5A11FA82E103327A9ABD0CA5BB397952CD260C (void);
// 0x000001F1 System.Void LTDescr::<setMoveToTransform>b__117_0()
extern void LTDescr_U3CsetMoveToTransformU3Eb__117_0_mCE51DD0DF5028FDE555344C8FF602CD98716C33B (void);
// 0x000001F2 System.Void LTDescr::<setMoveToTransform>b__117_1()
extern void LTDescr_U3CsetMoveToTransformU3Eb__117_1_mACD7BEE7BA65E5DCB5B234EB3A31D9D8C3ED7FA8 (void);
// 0x000001F3 System.Void LTDescr::<setRotate>b__118_0()
extern void LTDescr_U3CsetRotateU3Eb__118_0_m3ECAD3ACBA191CD5E6FBB67BB3E3AAEDA454B4D3 (void);
// 0x000001F4 System.Void LTDescr::<setRotate>b__118_1()
extern void LTDescr_U3CsetRotateU3Eb__118_1_mEB2249D09D0E69826AE6036FD448AF92D77B47D7 (void);
// 0x000001F5 System.Void LTDescr::<setRotateLocal>b__119_0()
extern void LTDescr_U3CsetRotateLocalU3Eb__119_0_m2AA0D5D9B5334CEC12D6E8670332C4BEEB5CD7DB (void);
// 0x000001F6 System.Void LTDescr::<setRotateLocal>b__119_1()
extern void LTDescr_U3CsetRotateLocalU3Eb__119_1_m64E3FC9E52EE3EB30AA9260408C3D02F24A34064 (void);
// 0x000001F7 System.Void LTDescr::<setScale>b__120_0()
extern void LTDescr_U3CsetScaleU3Eb__120_0_m4528B485CAF7246E73A7E602504675D4B11A44B7 (void);
// 0x000001F8 System.Void LTDescr::<setScale>b__120_1()
extern void LTDescr_U3CsetScaleU3Eb__120_1_m4A1D3D78C772991EC422F7D2D29EF4D069E39C78 (void);
// 0x000001F9 System.Void LTDescr::<setGUIMove>b__121_0()
extern void LTDescr_U3CsetGUIMoveU3Eb__121_0_m4AD272AF144CD468B048F733B93D1DFC08527804 (void);
// 0x000001FA System.Void LTDescr::<setGUIMove>b__121_1()
extern void LTDescr_U3CsetGUIMoveU3Eb__121_1_mDBDABB8CB0F1D7CC05BF861826BD23B1A5CF68B3 (void);
// 0x000001FB System.Void LTDescr::<setGUIMoveMargin>b__122_0()
extern void LTDescr_U3CsetGUIMoveMarginU3Eb__122_0_m92AE989BFBF68D262DDFD12EB9DBBE05BDCE7AB1 (void);
// 0x000001FC System.Void LTDescr::<setGUIMoveMargin>b__122_1()
extern void LTDescr_U3CsetGUIMoveMarginU3Eb__122_1_mA60DFD7F27C11D0B55761F7A0936D8056ED09382 (void);
// 0x000001FD System.Void LTDescr::<setGUIScale>b__123_0()
extern void LTDescr_U3CsetGUIScaleU3Eb__123_0_m5A0FC254BDC374AACB5FED5922852DD294CBA77C (void);
// 0x000001FE System.Void LTDescr::<setGUIScale>b__123_1()
extern void LTDescr_U3CsetGUIScaleU3Eb__123_1_m01CD6A1BE962B34E940B420723AE967C9AF4B248 (void);
// 0x000001FF System.Void LTDescr::<setGUIAlpha>b__124_0()
extern void LTDescr_U3CsetGUIAlphaU3Eb__124_0_m584C608C8B326C3B0401CAA8C696F8ABCF95DD38 (void);
// 0x00000200 System.Void LTDescr::<setGUIAlpha>b__124_1()
extern void LTDescr_U3CsetGUIAlphaU3Eb__124_1_mF0CAD09EF53473D82DF70172C27DA602272E3361 (void);
// 0x00000201 System.Void LTDescr::<setGUIRotate>b__125_0()
extern void LTDescr_U3CsetGUIRotateU3Eb__125_0_m8F9A74633240B6083A3C1DB7EA24F762D40589C9 (void);
// 0x00000202 System.Void LTDescr::<setGUIRotate>b__125_1()
extern void LTDescr_U3CsetGUIRotateU3Eb__125_1_m43CF134D7443CAA354F69A7031CE9F2574CE4DA5 (void);
// 0x00000203 System.Void LTDescr::<setDelayedSound>b__126_0()
extern void LTDescr_U3CsetDelayedSoundU3Eb__126_0_mEF9FE367DCCC3DDE76DA47246D17F221C0F4D93F (void);
// 0x00000204 System.Void LTDescr/EaseTypeDelegate::.ctor(System.Object,System.IntPtr)
extern void EaseTypeDelegate__ctor_mCD18183DC7DD02010D779AB3969F9D2B6BDAEA28 (void);
// 0x00000205 UnityEngine.Vector3 LTDescr/EaseTypeDelegate::Invoke()
extern void EaseTypeDelegate_Invoke_mC562BC4CD7B5786B26A9D7F780BC43F1B2F601AB (void);
// 0x00000206 System.IAsyncResult LTDescr/EaseTypeDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void EaseTypeDelegate_BeginInvoke_m8B1407D35ABAD4BA8A49AA4CE44736F90B5D834F (void);
// 0x00000207 UnityEngine.Vector3 LTDescr/EaseTypeDelegate::EndInvoke(System.IAsyncResult)
extern void EaseTypeDelegate_EndInvoke_m275A58EADF116EBDD21996509E57703CCC10C75A (void);
// 0x00000208 System.Void LTDescr/ActionMethodDelegate::.ctor(System.Object,System.IntPtr)
extern void ActionMethodDelegate__ctor_m86D0105115F69AEC95F9DB9542A3CB52854649E6 (void);
// 0x00000209 System.Void LTDescr/ActionMethodDelegate::Invoke()
extern void ActionMethodDelegate_Invoke_m4C86E95A515A25BE5ADB85D4889AAEDAA157173A (void);
// 0x0000020A System.IAsyncResult LTDescr/ActionMethodDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void ActionMethodDelegate_BeginInvoke_m1B3C88E85A1CF2B1CE645AC9594565046D26BE0B (void);
// 0x0000020B System.Void LTDescr/ActionMethodDelegate::EndInvoke(System.IAsyncResult)
extern void ActionMethodDelegate_EndInvoke_m7B0A147CA95FB6A16463DFA4C30DE37053C5D72F (void);
// 0x0000020C System.Void LTDescr/<>c::.cctor()
extern void U3CU3Ec__cctor_mACE0E27AEF258161C83D9EA95E78DBD6B559A5EB (void);
// 0x0000020D System.Void LTDescr/<>c::.ctor()
extern void U3CU3Ec__ctor_mB934C02FF6DB889EA42A5581DCA0B5CEDBCC5903 (void);
// 0x0000020E System.Void LTDescr/<>c::<setCallback>b__113_0()
extern void U3CU3Ec_U3CsetCallbackU3Eb__113_0_m7FC51BAA4B25A66AAAE19427E427E31C01E7EF0D (void);
// 0x0000020F System.Void LTDescr/<>c::<setValue3>b__114_0()
extern void U3CU3Ec_U3CsetValue3U3Eb__114_0_mF93F5C6F0B095D8B1EE239BE2A475A16E713A012 (void);
// 0x00000210 UnityEngine.Transform LTDescrOptional::get_toTrans()
extern void LTDescrOptional_get_toTrans_mB4912C34FDE4BB3CE94B86FDE3D7D8C0DE951773 (void);
// 0x00000211 System.Void LTDescrOptional::set_toTrans(UnityEngine.Transform)
extern void LTDescrOptional_set_toTrans_m4C747BFF6573BF8C2484AF01C3FCCC65E8A54FB9 (void);
// 0x00000212 UnityEngine.Vector3 LTDescrOptional::get_point()
extern void LTDescrOptional_get_point_mB79A8D97B381FE3122B1549DBB3527F9691A1156 (void);
// 0x00000213 System.Void LTDescrOptional::set_point(UnityEngine.Vector3)
extern void LTDescrOptional_set_point_mD7AE42D3184ED0D66545DC3A98B5CB5759DF8627 (void);
// 0x00000214 UnityEngine.Vector3 LTDescrOptional::get_axis()
extern void LTDescrOptional_get_axis_m546D8ADD4003D9B69F7EC4A243008064AC42DB20 (void);
// 0x00000215 System.Void LTDescrOptional::set_axis(UnityEngine.Vector3)
extern void LTDescrOptional_set_axis_m17A070B698AFE6D066C10EEF1A86DDEABC0A13B0 (void);
// 0x00000216 System.Single LTDescrOptional::get_lastVal()
extern void LTDescrOptional_get_lastVal_mD84B4C7F0D87E853D6EB9786261068DA74031DF8 (void);
// 0x00000217 System.Void LTDescrOptional::set_lastVal(System.Single)
extern void LTDescrOptional_set_lastVal_m1B2DC6C4CE540BA60BD467F4394CEF18409B820C (void);
// 0x00000218 UnityEngine.Quaternion LTDescrOptional::get_origRotation()
extern void LTDescrOptional_get_origRotation_m1B98DB0AACC7AF940C43DC29DF7A61FE6E49736E (void);
// 0x00000219 System.Void LTDescrOptional::set_origRotation(UnityEngine.Quaternion)
extern void LTDescrOptional_set_origRotation_m6E9E885A6259D7DE7FD1D398CABD7F7CB1751DE4 (void);
// 0x0000021A LTBezierPath LTDescrOptional::get_path()
extern void LTDescrOptional_get_path_m20A38F266BEEDD6370F8F54B2FA58C7262808AC5 (void);
// 0x0000021B System.Void LTDescrOptional::set_path(LTBezierPath)
extern void LTDescrOptional_set_path_m10D4A506CACD00E0951E6E5E41003DE99C72DA64 (void);
// 0x0000021C LTSpline LTDescrOptional::get_spline()
extern void LTDescrOptional_get_spline_m32BE3A98427C2A4FBE40E1BAEDEC2246ABD3D4F2 (void);
// 0x0000021D System.Void LTDescrOptional::set_spline(LTSpline)
extern void LTDescrOptional_set_spline_m5FF9921610A1F8C38E004CF9129F185BC7FDD708 (void);
// 0x0000021E LTRect LTDescrOptional::get_ltRect()
extern void LTDescrOptional_get_ltRect_mDBE34D781428B8DD07A8F25AC32B9B0932B62BE7 (void);
// 0x0000021F System.Void LTDescrOptional::set_ltRect(LTRect)
extern void LTDescrOptional_set_ltRect_m273BF61CF793266842167003657D7083E62B655A (void);
// 0x00000220 System.Action`1<System.Single> LTDescrOptional::get_onUpdateFloat()
extern void LTDescrOptional_get_onUpdateFloat_m8573CE5DFEFBA6E350AF056485B265FF59E8072B (void);
// 0x00000221 System.Void LTDescrOptional::set_onUpdateFloat(System.Action`1<System.Single>)
extern void LTDescrOptional_set_onUpdateFloat_mC6B9DD9721D66C62B4F196BFB0AC6EAFFD98A10F (void);
// 0x00000222 System.Action`2<System.Single,System.Single> LTDescrOptional::get_onUpdateFloatRatio()
extern void LTDescrOptional_get_onUpdateFloatRatio_m6757E471D3ADDB9202062289E6EFAD3B61B2C7F4 (void);
// 0x00000223 System.Void LTDescrOptional::set_onUpdateFloatRatio(System.Action`2<System.Single,System.Single>)
extern void LTDescrOptional_set_onUpdateFloatRatio_m0FBA91E49D4D592E2CAA88BF2963B2B5737ACEF7 (void);
// 0x00000224 System.Action`2<System.Single,System.Object> LTDescrOptional::get_onUpdateFloatObject()
extern void LTDescrOptional_get_onUpdateFloatObject_m8A282C0573D32731C92CA8A4E352382CE73E4839 (void);
// 0x00000225 System.Void LTDescrOptional::set_onUpdateFloatObject(System.Action`2<System.Single,System.Object>)
extern void LTDescrOptional_set_onUpdateFloatObject_mF14622BEBB5CCF990BC0DC9622CE2EE037F364C5 (void);
// 0x00000226 System.Action`1<UnityEngine.Vector2> LTDescrOptional::get_onUpdateVector2()
extern void LTDescrOptional_get_onUpdateVector2_m2692F9A7698099312B1956EF89ADC677E156325A (void);
// 0x00000227 System.Void LTDescrOptional::set_onUpdateVector2(System.Action`1<UnityEngine.Vector2>)
extern void LTDescrOptional_set_onUpdateVector2_mC9D4DD0709714E0D052A2A6E475099B146375331 (void);
// 0x00000228 System.Action`1<UnityEngine.Vector3> LTDescrOptional::get_onUpdateVector3()
extern void LTDescrOptional_get_onUpdateVector3_m1A96786092458EFAEAB22467DC6AA01F2F34247A (void);
// 0x00000229 System.Void LTDescrOptional::set_onUpdateVector3(System.Action`1<UnityEngine.Vector3>)
extern void LTDescrOptional_set_onUpdateVector3_m91295D4F19AFF3080F6B44A071118CCDB52F288B (void);
// 0x0000022A System.Action`2<UnityEngine.Vector3,System.Object> LTDescrOptional::get_onUpdateVector3Object()
extern void LTDescrOptional_get_onUpdateVector3Object_m2CAC540C86A54F7744AE39443FB4054066E3DE02 (void);
// 0x0000022B System.Void LTDescrOptional::set_onUpdateVector3Object(System.Action`2<UnityEngine.Vector3,System.Object>)
extern void LTDescrOptional_set_onUpdateVector3Object_m2ECE95CB5D7DFE5E9DEDB9F3B03BEF39D4000B77 (void);
// 0x0000022C System.Action`1<UnityEngine.Color> LTDescrOptional::get_onUpdateColor()
extern void LTDescrOptional_get_onUpdateColor_m78FBDE4D9B8856EBE7EBF42709CFF81E127B9A43 (void);
// 0x0000022D System.Void LTDescrOptional::set_onUpdateColor(System.Action`1<UnityEngine.Color>)
extern void LTDescrOptional_set_onUpdateColor_m7D77082D84B3831810A825A0628CE556012AAA2B (void);
// 0x0000022E System.Action`2<UnityEngine.Color,System.Object> LTDescrOptional::get_onUpdateColorObject()
extern void LTDescrOptional_get_onUpdateColorObject_m45EF15CE7E1F771A04B6E9374323759A7A021C62 (void);
// 0x0000022F System.Void LTDescrOptional::set_onUpdateColorObject(System.Action`2<UnityEngine.Color,System.Object>)
extern void LTDescrOptional_set_onUpdateColorObject_m25F9B89775D1630387BEDF4DC8B2F88F8E4842A0 (void);
// 0x00000230 System.Action LTDescrOptional::get_onComplete()
extern void LTDescrOptional_get_onComplete_m89C63A691247B03AF894749D3F32987E7994CFA8 (void);
// 0x00000231 System.Void LTDescrOptional::set_onComplete(System.Action)
extern void LTDescrOptional_set_onComplete_m71A16E08537D67C75C706DD0EB88B7954A0CFCAF (void);
// 0x00000232 System.Action`1<System.Object> LTDescrOptional::get_onCompleteObject()
extern void LTDescrOptional_get_onCompleteObject_m50BB3E137E79340957CEE096A24A3635ACD91DEA (void);
// 0x00000233 System.Void LTDescrOptional::set_onCompleteObject(System.Action`1<System.Object>)
extern void LTDescrOptional_set_onCompleteObject_m70FA4651F7E2CA76267B09C3EFBEA8AFE62F95E3 (void);
// 0x00000234 System.Object LTDescrOptional::get_onCompleteParam()
extern void LTDescrOptional_get_onCompleteParam_mD546B9E0E31E70CEF0514DFB486D49E4BBE9D120 (void);
// 0x00000235 System.Void LTDescrOptional::set_onCompleteParam(System.Object)
extern void LTDescrOptional_set_onCompleteParam_mE4F5C590CFA4FF8204FE0C4722BF7EEBC1F4D7C7 (void);
// 0x00000236 System.Object LTDescrOptional::get_onUpdateParam()
extern void LTDescrOptional_get_onUpdateParam_m8568043E9421D14172B62712FF0519738F0E8D88 (void);
// 0x00000237 System.Void LTDescrOptional::set_onUpdateParam(System.Object)
extern void LTDescrOptional_set_onUpdateParam_m415D9E73546338141E97EBEB8587C8A6F00C18E6 (void);
// 0x00000238 System.Action LTDescrOptional::get_onStart()
extern void LTDescrOptional_get_onStart_m67196E7D6B56A68A3D7A5BAC7201CB5D79575EEF (void);
// 0x00000239 System.Void LTDescrOptional::set_onStart(System.Action)
extern void LTDescrOptional_set_onStart_mFC780FBC4B2363070A9395BAA7693ACBFA3C75F8 (void);
// 0x0000023A System.Void LTDescrOptional::reset()
extern void LTDescrOptional_reset_mBC39AAD50AF833763A2547D2BB295CCE04F8CDBB (void);
// 0x0000023B System.Void LTDescrOptional::callOnUpdate(System.Single,System.Single)
extern void LTDescrOptional_callOnUpdate_mCD2B766E5358969C288B360A409B741962E90A9E (void);
// 0x0000023C System.Void LTDescrOptional::.ctor()
extern void LTDescrOptional__ctor_m31327367F73AF26F1F0513529F56B4D4527F1CD7 (void);
// 0x0000023D System.Int32 LTSeq::get_id()
extern void LTSeq_get_id_m7BB843F84403ED4D5D7816EAB92F64D64AA16528 (void);
// 0x0000023E System.Void LTSeq::reset()
extern void LTSeq_reset_m81F08444C11BCB0DA11DA2655C967A31B0F48E43 (void);
// 0x0000023F System.Void LTSeq::init(System.UInt32,System.UInt32)
extern void LTSeq_init_m7666AE5720766D32C80F45C08B15F29BDF926FCA (void);
// 0x00000240 LTSeq LTSeq::addOn()
extern void LTSeq_addOn_m92322FBE7CE53DC475ECF67BD47ED0A0FB37471A (void);
// 0x00000241 System.Single LTSeq::addPreviousDelays()
extern void LTSeq_addPreviousDelays_mA44FF52A17DF8B8F70385938E809B922AE96D914 (void);
// 0x00000242 LTSeq LTSeq::append(System.Single)
extern void LTSeq_append_m9CCA2E2A2FEC8BB783E6973C5259009DE1F2CA93 (void);
// 0x00000243 LTSeq LTSeq::append(System.Action)
extern void LTSeq_append_m918D5B42DF7E78F1D632BDFB559566153BC03151 (void);
// 0x00000244 LTSeq LTSeq::append(System.Action`1<System.Object>,System.Object)
extern void LTSeq_append_m771276B4D19FF6E029D920552CA35785C06166DC (void);
// 0x00000245 LTSeq LTSeq::append(UnityEngine.GameObject,System.Action)
extern void LTSeq_append_m7B1CF4D2CEE5FEC4FD64BE97123E3466981969C9 (void);
// 0x00000246 LTSeq LTSeq::append(UnityEngine.GameObject,System.Action`1<System.Object>,System.Object)
extern void LTSeq_append_m38B101D631E233A3EA115D8102FECE3A9F8501D0 (void);
// 0x00000247 LTSeq LTSeq::append(LTDescr)
extern void LTSeq_append_m16B953CD94139BC1C75791A5D461C744EA4ECA64 (void);
// 0x00000248 LTSeq LTSeq::insert(LTDescr)
extern void LTSeq_insert_m557E13DE79F5FDF0DCC9746AA15307FED3DA9F0A (void);
// 0x00000249 LTSeq LTSeq::setScale(System.Single)
extern void LTSeq_setScale_m01A80A5EF1549289CCFE8F7E7176A5B669781699 (void);
// 0x0000024A System.Void LTSeq::setScaleRecursive(LTSeq,System.Single,System.Int32)
extern void LTSeq_setScaleRecursive_mA815906D527D73895B508205E6DF519C26560C7F (void);
// 0x0000024B LTSeq LTSeq::reverse()
extern void LTSeq_reverse_m626178075D66F45F66D451A95BCFFDBCAD95E04C (void);
// 0x0000024C System.Void LTSeq::.ctor()
extern void LTSeq__ctor_m0C853ED35E39038B9153A5E212A11D5366E3EB01 (void);
// 0x0000024D System.Void LeanAudioStream::.ctor(System.Single[])
extern void LeanAudioStream__ctor_m767420411C73925D7260141B6AC141C332340F57 (void);
// 0x0000024E System.Void LeanAudioStream::OnAudioRead(System.Single[])
extern void LeanAudioStream_OnAudioRead_mB0B822C29AF57CD64964495BC47B2D552C9A61EE (void);
// 0x0000024F System.Void LeanAudioStream::OnAudioSetPosition(System.Int32)
extern void LeanAudioStream_OnAudioSetPosition_m84D20E5E2E90A883DA74466C176BD12EF300CFD7 (void);
// 0x00000250 LeanAudioOptions LeanAudio::options()
extern void LeanAudio_options_m24F4466E9FF88143F014E1341ECAA03A0BD1DE16 (void);
// 0x00000251 LeanAudioStream LeanAudio::createAudioStream(UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,LeanAudioOptions)
extern void LeanAudio_createAudioStream_mF0B2E4AE61E77E5E4F5BB229BB17EA0010EED167 (void);
// 0x00000252 UnityEngine.AudioClip LeanAudio::createAudio(UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,LeanAudioOptions)
extern void LeanAudio_createAudio_m741D84D297AF8413E9A6890FA81CD7F288A303C7 (void);
// 0x00000253 System.Int32 LeanAudio::createAudioWave(UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,LeanAudioOptions)
extern void LeanAudio_createAudioWave_m4368517E2416AF360A06777597C9A3142BA5FC00 (void);
// 0x00000254 UnityEngine.AudioClip LeanAudio::createAudioFromWave(System.Int32,LeanAudioOptions)
extern void LeanAudio_createAudioFromWave_m1D0C1A2142307BF2378F25C76C42F61265D5ED21 (void);
// 0x00000255 System.Void LeanAudio::OnAudioSetPosition(System.Int32)
extern void LeanAudio_OnAudioSetPosition_m7FBC95FBFCA93F4A878761356FFD1B4FB716331C (void);
// 0x00000256 UnityEngine.AudioClip LeanAudio::generateAudioFromCurve(UnityEngine.AnimationCurve,System.Int32)
extern void LeanAudio_generateAudioFromCurve_mD5AE758FCAB5F326FECD85B37A313678B9D8929B (void);
// 0x00000257 UnityEngine.AudioSource LeanAudio::play(UnityEngine.AudioClip,System.Single)
extern void LeanAudio_play_m96190E0C8E00011FC6A98EFEBDFAEE2E3C06AB9E (void);
// 0x00000258 UnityEngine.AudioSource LeanAudio::play(UnityEngine.AudioClip)
extern void LeanAudio_play_m7F543AE840832C2D4FC88AA184C605A892F1C030 (void);
// 0x00000259 UnityEngine.AudioSource LeanAudio::play(UnityEngine.AudioClip,UnityEngine.Vector3)
extern void LeanAudio_play_m6CD5A5DC70FD339BDD5BEDC9243FACA5C3186451 (void);
// 0x0000025A UnityEngine.AudioSource LeanAudio::play(UnityEngine.AudioClip,UnityEngine.Vector3,System.Single)
extern void LeanAudio_play_mBC8A61F39D8ECDCA09F458FB1028945AC35DB0D2 (void);
// 0x0000025B UnityEngine.AudioSource LeanAudio::playClipAt(UnityEngine.AudioClip,UnityEngine.Vector3)
extern void LeanAudio_playClipAt_mC86BC94850F21DBC4B9C831CC666FEBC86A2C510 (void);
// 0x0000025C System.Void LeanAudio::printOutAudioClip(UnityEngine.AudioClip,UnityEngine.AnimationCurve&,System.Single)
extern void LeanAudio_printOutAudioClip_m22003A70913081D802CBF76538C92F929CD8D857 (void);
// 0x0000025D System.Void LeanAudio::.ctor()
extern void LeanAudio__ctor_m492D6C2E9108D90D229F0811C3B185DFD891907A (void);
// 0x0000025E System.Void LeanAudio::.cctor()
extern void LeanAudio__cctor_m31067C511A043987DBFED412C6486A1D4D2CBF53 (void);
// 0x0000025F System.Void LeanAudioOptions::.ctor()
extern void LeanAudioOptions__ctor_m29A189A1499462B631EBD724FF961389527A8EA2 (void);
// 0x00000260 LeanAudioOptions LeanAudioOptions::setFrequency(System.Int32)
extern void LeanAudioOptions_setFrequency_m85E50359A5904A370955AFF52AC3D88CC01EBBC5 (void);
// 0x00000261 LeanAudioOptions LeanAudioOptions::setVibrato(UnityEngine.Vector3[])
extern void LeanAudioOptions_setVibrato_mF64FB8E4DD576D90776F76B01B753247D99B49DB (void);
// 0x00000262 LeanAudioOptions LeanAudioOptions::setWaveSine()
extern void LeanAudioOptions_setWaveSine_m76F10085FDCE754A60760BB1542537F9DA46034F (void);
// 0x00000263 LeanAudioOptions LeanAudioOptions::setWaveSquare()
extern void LeanAudioOptions_setWaveSquare_mF8F08EACD82E37E15137F5F1C785DD0D671AF42E (void);
// 0x00000264 LeanAudioOptions LeanAudioOptions::setWaveSawtooth()
extern void LeanAudioOptions_setWaveSawtooth_m27624D2920970C41AD3CE47DE408FE9C7EEA530E (void);
// 0x00000265 LeanAudioOptions LeanAudioOptions::setWaveNoise()
extern void LeanAudioOptions_setWaveNoise_m55FF493A787F18D8692DF85866D055354AE1A2B0 (void);
// 0x00000266 LeanAudioOptions LeanAudioOptions::setWaveStyle(LeanAudioOptions/LeanAudioWaveStyle)
extern void LeanAudioOptions_setWaveStyle_mEB0CA095133249C1947E51FA9FBE9657F7A784CF (void);
// 0x00000267 LeanAudioOptions LeanAudioOptions::setWaveNoiseScale(System.Single)
extern void LeanAudioOptions_setWaveNoiseScale_m68B9C76F8DD866150B060285CF705D4ECB5C533C (void);
// 0x00000268 LeanAudioOptions LeanAudioOptions::setWaveNoiseInfluence(System.Single)
extern void LeanAudioOptions_setWaveNoiseInfluence_m7CBBEC51092269C15133C3B53D1828ACC874AF56 (void);
// 0x00000269 System.Single LeanSmooth::damp(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single)
extern void LeanSmooth_damp_mAFCFAA57FEE313048C9AF8C5F16FC2F5E723D0FF (void);
// 0x0000026A UnityEngine.Vector3 LeanSmooth::damp(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&,System.Single,System.Single,System.Single)
extern void LeanSmooth_damp_m7CCE1AB623BF22468A59B97CC2D161682476168E (void);
// 0x0000026B UnityEngine.Color LeanSmooth::damp(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color&,System.Single,System.Single,System.Single)
extern void LeanSmooth_damp_m9AA68F6F1D20FF61F338BA122A374CB0F10D6EFE (void);
// 0x0000026C System.Single LeanSmooth::spring(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_spring_m314389AD151BF197F11CB50DFEC3F05396E189AD (void);
// 0x0000026D UnityEngine.Vector3 LeanSmooth::spring(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_spring_mAC93690E733A39072CB37D5BFF1239414F98E1EF (void);
// 0x0000026E UnityEngine.Color LeanSmooth::spring(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color&,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_spring_mC13A607AA221D50EB1AB72CE0C5D314F2748E946 (void);
// 0x0000026F System.Single LeanSmooth::linear(System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_linear_mCAA760AD54BCCFD8A3C67631437C528F84806A11 (void);
// 0x00000270 UnityEngine.Vector3 LeanSmooth::linear(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanSmooth_linear_mE9F99D1FD99A7D58A0F81A3A39EA4D994D55BB6D (void);
// 0x00000271 UnityEngine.Color LeanSmooth::linear(UnityEngine.Color,UnityEngine.Color,System.Single)
extern void LeanSmooth_linear_mD4AAD42ECD1AFD102F576A52116C57CDEEFB73E7 (void);
// 0x00000272 System.Single LeanSmooth::bounceOut(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_bounceOut_mD1B8F20CB910CD8609D9F5988472CCC83B98DE6A (void);
// 0x00000273 UnityEngine.Vector3 LeanSmooth::bounceOut(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_bounceOut_m39C1F55CD36843F594D1C58D067696AD294C4812 (void);
// 0x00000274 UnityEngine.Color LeanSmooth::bounceOut(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color&,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_bounceOut_mF3E43703A80AB9D705575A9D803AD2E8AF3722F1 (void);
// 0x00000275 System.Void LeanSmooth::.ctor()
extern void LeanSmooth__ctor_mA939A643B1463547170CECB7FCC6AF013FF0DCAA (void);
// 0x00000276 System.Void LeanTester::Start()
extern void LeanTester_Start_m9EEED491D1FBBF5A677178E6477E7B68491F1517 (void);
// 0x00000277 System.Collections.IEnumerator LeanTester::timeoutCheck()
extern void LeanTester_timeoutCheck_m59067A5600F0D2434CE9B7EDAC062829C85D5C00 (void);
// 0x00000278 System.Void LeanTester::.ctor()
extern void LeanTester__ctor_mBB9C14D1DDB7AA3F591D6B0040EEA74F032EB1F7 (void);
// 0x00000279 System.Void LeanTester/<timeoutCheck>d__2::.ctor(System.Int32)
extern void U3CtimeoutCheckU3Ed__2__ctor_mACFA1EA79EDBEA235C7474D1BA3E71804A23FEB8 (void);
// 0x0000027A System.Void LeanTester/<timeoutCheck>d__2::System.IDisposable.Dispose()
extern void U3CtimeoutCheckU3Ed__2_System_IDisposable_Dispose_m0AF241B0BA496E508C41A00A92ACCB507F764F3B (void);
// 0x0000027B System.Boolean LeanTester/<timeoutCheck>d__2::MoveNext()
extern void U3CtimeoutCheckU3Ed__2_MoveNext_mF2666B91C53937A9B6CFBCDB7E111B6E74685643 (void);
// 0x0000027C System.Object LeanTester/<timeoutCheck>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CtimeoutCheckU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF1EF5FF8E038CD04BB438F282E437032A52DF973 (void);
// 0x0000027D System.Void LeanTester/<timeoutCheck>d__2::System.Collections.IEnumerator.Reset()
extern void U3CtimeoutCheckU3Ed__2_System_Collections_IEnumerator_Reset_m377EF3F9C3E2B3A09D749A0A347B92595F0D756D (void);
// 0x0000027E System.Object LeanTester/<timeoutCheck>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CtimeoutCheckU3Ed__2_System_Collections_IEnumerator_get_Current_m275B7C4BFE741FFBA1D7CE3E422460E5BD15EFF6 (void);
// 0x0000027F System.Void LeanTest::debug(System.String,System.Boolean,System.String)
extern void LeanTest_debug_m185769356E43C51F1A057B12DC7F0E6970CA05D2 (void);
// 0x00000280 System.Void LeanTest::expect(System.Boolean,System.String,System.String)
extern void LeanTest_expect_m697408A02255410088277BB6C41F73FFB789458B (void);
// 0x00000281 System.String LeanTest::padRight(System.Int32)
extern void LeanTest_padRight_m6DEC7C60D840B9D4F35BDFD3A27411551F182CF2 (void);
// 0x00000282 System.Single LeanTest::printOutLength(System.String)
extern void LeanTest_printOutLength_mE439776B28BB9737553D43E95254DD2A79676EFF (void);
// 0x00000283 System.String LeanTest::formatBC(System.String,System.String)
extern void LeanTest_formatBC_mCE1B1751EF0B35A008C32A43AEBACA76B2FF0272 (void);
// 0x00000284 System.String LeanTest::formatB(System.String)
extern void LeanTest_formatB_mDF309FD724B541A97D931A28FE32B9F3245DA966 (void);
// 0x00000285 System.String LeanTest::formatC(System.String,System.String)
extern void LeanTest_formatC_m10095A4C3AEB8A45468A8251101CE25BE1643F34 (void);
// 0x00000286 System.Void LeanTest::overview()
extern void LeanTest_overview_mFBDD0E748CC3DF470B668293185D2637F2ACF17F (void);
// 0x00000287 System.Void LeanTest::.ctor()
extern void LeanTest__ctor_m2534B2F8BA29F28EF5D26482735FE6D93B229F6E (void);
// 0x00000288 System.Void LeanTest::.cctor()
extern void LeanTest__cctor_mE75EB5D6AF852A6D0B480FCC70D1F9958AFE0943 (void);
// 0x00000289 System.Void LeanTween::init()
extern void LeanTween_init_m2257D056F7462641D1316804B27DB2EF03C54B0B (void);
// 0x0000028A System.Int32 LeanTween::get_maxSearch()
extern void LeanTween_get_maxSearch_m76F5A3551B333FEAD6918DC0795238CEA51E8764 (void);
// 0x0000028B System.Int32 LeanTween::get_maxSimulataneousTweens()
extern void LeanTween_get_maxSimulataneousTweens_m38608BBD6E41F3B66FF348DE652F2EA6F1BEBF45 (void);
// 0x0000028C System.Int32 LeanTween::get_tweensRunning()
extern void LeanTween_get_tweensRunning_mDD39747E471B1CCDF83F6F79B5CFA224E47D3842 (void);
// 0x0000028D System.Void LeanTween::init(System.Int32)
extern void LeanTween_init_m4CE7872C3DBD46FF7252C60DA96FD99FE9A071C8 (void);
// 0x0000028E System.Void LeanTween::init(System.Int32,System.Int32)
extern void LeanTween_init_m641164BCF025E634EFBD06151E55753939DB6F13 (void);
// 0x0000028F System.Void LeanTween::reset()
extern void LeanTween_reset_mC778C2B33DA945C2D7FC099D6B0EDE24A98A78AD (void);
// 0x00000290 System.Void LeanTween::Update()
extern void LeanTween_Update_m4E96DD5D854DBD4CDF76759B63786DDD8739BF08 (void);
// 0x00000291 System.Void LeanTween::onLevelWasLoaded54(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void LeanTween_onLevelWasLoaded54_mE6FF28F42BD62CC30963F9D80C2B26FFF0B3C5B0 (void);
// 0x00000292 System.Void LeanTween::internalOnLevelWasLoaded(System.Int32)
extern void LeanTween_internalOnLevelWasLoaded_m14ED1FA1C1214624DC4A780E737D2FFE90A3B0AB (void);
// 0x00000293 System.Void LeanTween::update()
extern void LeanTween_update_m2BF953CB1B4904B57BC6A43058FDA4BE52B57688 (void);
// 0x00000294 System.Void LeanTween::removeTween(System.Int32,System.Int32)
extern void LeanTween_removeTween_mD1216980A45C032C76FBD7D66BD3ECD81F992D84 (void);
// 0x00000295 System.Void LeanTween::removeTween(System.Int32)
extern void LeanTween_removeTween_mEF159BEAAC6DC0EEAA016966749C762D3EE88A33 (void);
// 0x00000296 UnityEngine.Vector3[] LeanTween::add(UnityEngine.Vector3[],UnityEngine.Vector3)
extern void LeanTween_add_mF9434A6D817FB5B901ED97889690158C6E99AD4E (void);
// 0x00000297 System.Single LeanTween::closestRot(System.Single,System.Single)
extern void LeanTween_closestRot_m45089460836416CF978CED05F04748378298675C (void);
// 0x00000298 System.Void LeanTween::cancelAll()
extern void LeanTween_cancelAll_m017FC44A7C2778837326A7518192A881D4F39FD2 (void);
// 0x00000299 System.Void LeanTween::cancelAll(System.Boolean)
extern void LeanTween_cancelAll_mC78FDFE4A1045C23D35C9447D3D71FFBDA2837CE (void);
// 0x0000029A System.Void LeanTween::cancel(UnityEngine.GameObject)
extern void LeanTween_cancel_m8F49EAB6908B4BE0715C0D4160C51516DCDD2823 (void);
// 0x0000029B System.Void LeanTween::cancel(UnityEngine.GameObject,System.Boolean)
extern void LeanTween_cancel_m28B082D5434EBF590F33D552278E4969614E35E6 (void);
// 0x0000029C System.Void LeanTween::cancel(UnityEngine.RectTransform)
extern void LeanTween_cancel_mE6AF5A1A673FCA3C9EAFBAE4C15EBE5ED42B7710 (void);
// 0x0000029D System.Void LeanTween::cancel(UnityEngine.GameObject,System.Int32,System.Boolean)
extern void LeanTween_cancel_mF3E2B961A0AB5C23CB2EB64B557AC718BF931F9C (void);
// 0x0000029E System.Void LeanTween::cancel(LTRect,System.Int32)
extern void LeanTween_cancel_mE5CB54E734F2E1DD02E9979D64E163400677AA7B (void);
// 0x0000029F System.Void LeanTween::cancel(System.Int32)
extern void LeanTween_cancel_m8F235169322AEA85E23C774745EED07BD83D344E (void);
// 0x000002A0 System.Void LeanTween::cancel(System.Int32,System.Boolean)
extern void LeanTween_cancel_m54BD97AD0C8C50A9EE5645108B28F7E25104EA01 (void);
// 0x000002A1 LTDescr LeanTween::descr(System.Int32)
extern void LeanTween_descr_m0686304AC8834008456087DDB221577D87246C90 (void);
// 0x000002A2 LTDescr LeanTween::description(System.Int32)
extern void LeanTween_description_mCF024DD0005701DAC6CFDDFCAC0FCBF66C613B38 (void);
// 0x000002A3 LTDescr[] LeanTween::descriptions(UnityEngine.GameObject)
extern void LeanTween_descriptions_m2ADDBCE0DFC78640128B7224DD86FE6C3938D76C (void);
// 0x000002A4 System.Void LeanTween::pause(UnityEngine.GameObject,System.Int32)
extern void LeanTween_pause_mAF75D7E636CB0D02F8D515FF76C23D99B8B28FB1 (void);
// 0x000002A5 System.Void LeanTween::pause(System.Int32)
extern void LeanTween_pause_mEE3D7E0190BB1879C5E4E40A34E22146B867B2B0 (void);
// 0x000002A6 System.Void LeanTween::pause(UnityEngine.GameObject)
extern void LeanTween_pause_m28FA361A74060F3CF6CFF07EA470DEB5871ABFFF (void);
// 0x000002A7 System.Void LeanTween::pauseAll()
extern void LeanTween_pauseAll_mBC633AE2F834B3E89A97848EBE21C2CF24E04214 (void);
// 0x000002A8 System.Void LeanTween::resumeAll()
extern void LeanTween_resumeAll_m933FBC788EED5D67979F160369D98BEF44CCB435 (void);
// 0x000002A9 System.Void LeanTween::resume(UnityEngine.GameObject,System.Int32)
extern void LeanTween_resume_m1940BE978DB7EC3606D92C181FFE10A4570043E2 (void);
// 0x000002AA System.Void LeanTween::resume(System.Int32)
extern void LeanTween_resume_mB671FD12433254652D7A6F1F2E8B5A924EE8665C (void);
// 0x000002AB System.Void LeanTween::resume(UnityEngine.GameObject)
extern void LeanTween_resume_mB910424A52CA71E88F376EBC8BD7DAC8BA638F3A (void);
// 0x000002AC System.Boolean LeanTween::isPaused(UnityEngine.GameObject)
extern void LeanTween_isPaused_m36EF7ACA58D5CF19D261ECC2740D3BABE292AE17 (void);
// 0x000002AD System.Boolean LeanTween::isPaused(UnityEngine.RectTransform)
extern void LeanTween_isPaused_m8433AB793191572FC80B287A05951CCA46D60415 (void);
// 0x000002AE System.Boolean LeanTween::isPaused(System.Int32)
extern void LeanTween_isPaused_mCBFAAB19270CC1E95412B9DB3F5FCE12F56B64C2 (void);
// 0x000002AF System.Boolean LeanTween::isTweening(UnityEngine.GameObject)
extern void LeanTween_isTweening_m7D2702558144653FEF2A2994BFFDB95BB547D76E (void);
// 0x000002B0 System.Boolean LeanTween::isTweening(UnityEngine.RectTransform)
extern void LeanTween_isTweening_m0DD0D935F7AEC656A1397DD8DFFADA8509ACCBD1 (void);
// 0x000002B1 System.Boolean LeanTween::isTweening(System.Int32)
extern void LeanTween_isTweening_m8A9E15F4297CE4D50C54540646F91DEE0767071C (void);
// 0x000002B2 System.Boolean LeanTween::isTweening(LTRect)
extern void LeanTween_isTweening_mD1EC7E088742B5EB1A698E9C82DDC9F4AF2F831A (void);
// 0x000002B3 System.Void LeanTween::drawBezierPath(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Transform)
extern void LeanTween_drawBezierPath_m140E21617AD80CA0F665DA5A74E8AB641A5C3A41 (void);
// 0x000002B4 System.Object LeanTween::logError(System.String)
extern void LeanTween_logError_m941ECF9F4DA921ACE1845B97FE0C814F9E26B1CA (void);
// 0x000002B5 LTDescr LeanTween::options(LTDescr)
extern void LeanTween_options_mC569B03E642CE27936B5ADF511ACE67C40E3EFD6 (void);
// 0x000002B6 LTDescr LeanTween::options()
extern void LeanTween_options_m859EFA1A4C3BE0E937966B8F5D902F798B60B2C1 (void);
// 0x000002B7 UnityEngine.GameObject LeanTween::get_tweenEmpty()
extern void LeanTween_get_tweenEmpty_mB1509CF6AE8FAB5591F914CBC1CC1DD5C7447F08 (void);
// 0x000002B8 LTDescr LeanTween::pushNewTween(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,LTDescr)
extern void LeanTween_pushNewTween_m4C8A3AFE33EDDBCEF0FAFB6D9DE68BE6D930014D (void);
// 0x000002B9 LTDescr LeanTween::play(UnityEngine.RectTransform,UnityEngine.Sprite[])
extern void LeanTween_play_m8F84314D3491001A540FA92F7A4F0B1A66ABC010 (void);
// 0x000002BA LTSeq LeanTween::sequence(System.Boolean)
extern void LeanTween_sequence_m8FE5B82BEF69D9AF66D54E849587B4CF933E34A5 (void);
// 0x000002BB LTDescr LeanTween::alpha(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_alpha_m9D8A4A0F46CBF56AA0008446BCE391A124BDE913 (void);
// 0x000002BC LTDescr LeanTween::alpha(LTRect,System.Single,System.Single)
extern void LeanTween_alpha_mCE5E7D21CF2E2687DD1CFB80A9EE40E1E2EBCC0C (void);
// 0x000002BD LTDescr LeanTween::textAlpha(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_textAlpha_m31154DC1D5392385E4318CE77677AC4E29CF99B0 (void);
// 0x000002BE LTDescr LeanTween::alphaText(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_alphaText_mFD7A2FD730E1BA7638F985A93CE91B5EA652C126 (void);
// 0x000002BF LTDescr LeanTween::alphaCanvas(UnityEngine.CanvasGroup,System.Single,System.Single)
extern void LeanTween_alphaCanvas_m8BCDB0DB0CAE28F75A7DDF79FA6D62FC1B3AD513 (void);
// 0x000002C0 LTDescr LeanTween::alphaVertex(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_alphaVertex_m94B9F32105B2C406F41E8D014FD6E5E235EFA506 (void);
// 0x000002C1 LTDescr LeanTween::color(UnityEngine.GameObject,UnityEngine.Color,System.Single)
extern void LeanTween_color_mB591490EB11BF6AAB3D9C823844CD51A54CB54AF (void);
// 0x000002C2 LTDescr LeanTween::textColor(UnityEngine.RectTransform,UnityEngine.Color,System.Single)
extern void LeanTween_textColor_m4BCEDC9A9BAB880E6C4440833CF36C2ED3101343 (void);
// 0x000002C3 LTDescr LeanTween::colorText(UnityEngine.RectTransform,UnityEngine.Color,System.Single)
extern void LeanTween_colorText_m34F5C1169E22FE097A19CC6B85377C738D73B937 (void);
// 0x000002C4 LTDescr LeanTween::delayedCall(System.Single,System.Action)
extern void LeanTween_delayedCall_mF74ECC6AC349466F55E56C4625DB67892553A59A (void);
// 0x000002C5 LTDescr LeanTween::delayedCall(System.Single,System.Action`1<System.Object>)
extern void LeanTween_delayedCall_mF0264F6DCD013BFA7483D1399C04DA37502DC729 (void);
// 0x000002C6 LTDescr LeanTween::delayedCall(UnityEngine.GameObject,System.Single,System.Action)
extern void LeanTween_delayedCall_m537234980306375FFF55F428FE0F5AF5F35AD1CB (void);
// 0x000002C7 LTDescr LeanTween::delayedCall(UnityEngine.GameObject,System.Single,System.Action`1<System.Object>)
extern void LeanTween_delayedCall_m15A55B8039CD7A86856CE10FF15AC9F99D8D8EE4 (void);
// 0x000002C8 LTDescr LeanTween::destroyAfter(LTRect,System.Single)
extern void LeanTween_destroyAfter_m8EBFD0632BEE8F6EB0280D36C85CE24414C0FF15 (void);
// 0x000002C9 LTDescr LeanTween::move(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTween_move_m78D08B26807623C822B81CFA60DAC960DDBAC377 (void);
// 0x000002CA LTDescr LeanTween::move(UnityEngine.GameObject,UnityEngine.Vector2,System.Single)
extern void LeanTween_move_m1E331CB112535FB9EA90E55461A2D77D5260B530 (void);
// 0x000002CB LTDescr LeanTween::move(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTween_move_m44271CB95A4B41D5EB76C4D83B419135A6EF33EC (void);
// 0x000002CC LTDescr LeanTween::move(UnityEngine.GameObject,LTBezierPath,System.Single)
extern void LeanTween_move_mFEDFA180C09AB9A50941E5F63CD8670356C67ED3 (void);
// 0x000002CD LTDescr LeanTween::move(UnityEngine.GameObject,LTSpline,System.Single)
extern void LeanTween_move_mAE5F83E88AABED48945E61924B0427C1A9D2D2B7 (void);
// 0x000002CE LTDescr LeanTween::moveSpline(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTween_moveSpline_m5935DB5E37D36935FF3FBDA2C498F927FB7F5570 (void);
// 0x000002CF LTDescr LeanTween::moveSpline(UnityEngine.GameObject,LTSpline,System.Single)
extern void LeanTween_moveSpline_mB64C328098A521A9E7BB2EEAF0258238DF2DF712 (void);
// 0x000002D0 LTDescr LeanTween::moveSplineLocal(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTween_moveSplineLocal_m280D26BA7DE193E5F3EBB523ACEE6E44FF0978B7 (void);
// 0x000002D1 LTDescr LeanTween::move(LTRect,UnityEngine.Vector2,System.Single)
extern void LeanTween_move_m5E61C62A0F22777D370EB7CF2E6C0AC2EF3EB8B5 (void);
// 0x000002D2 LTDescr LeanTween::moveMargin(LTRect,UnityEngine.Vector2,System.Single)
extern void LeanTween_moveMargin_mCD8BA91FB1CB5762858AB694C374525B40570658 (void);
// 0x000002D3 LTDescr LeanTween::moveX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_moveX_m3345DDA9D080FF581C38E0B92B7B2A0389A61DB3 (void);
// 0x000002D4 LTDescr LeanTween::moveY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_moveY_mF3562A335C9D8A19192A1D05BBBB062D5696E01F (void);
// 0x000002D5 LTDescr LeanTween::moveZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_moveZ_m1D2828EEFBA366465AE916017B04BD730DB02918 (void);
// 0x000002D6 LTDescr LeanTween::moveLocal(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTween_moveLocal_m5EA4899950A5A6E5244A472D5B0095A456A3E828 (void);
// 0x000002D7 LTDescr LeanTween::moveLocal(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTween_moveLocal_m5EFEFD16EBF5E4EFC333418953F72DA504E191A0 (void);
// 0x000002D8 LTDescr LeanTween::moveLocalX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_moveLocalX_m6CE239F4E86EE5439B2D86FB0163306EC152DAD4 (void);
// 0x000002D9 LTDescr LeanTween::moveLocalY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_moveLocalY_m2EBE679F58F63B2D3BDA557474CD0C99B6AB8C81 (void);
// 0x000002DA LTDescr LeanTween::moveLocalZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_moveLocalZ_mCCAE8DEB09113FABEE3D66031E1B25C2B8BD318B (void);
// 0x000002DB LTDescr LeanTween::moveLocal(UnityEngine.GameObject,LTBezierPath,System.Single)
extern void LeanTween_moveLocal_m4D6E9C2DFFB9D9BF77A70A417CEDEB22D7B0689C (void);
// 0x000002DC LTDescr LeanTween::moveLocal(UnityEngine.GameObject,LTSpline,System.Single)
extern void LeanTween_moveLocal_m3AB20706D4DAFD80EB42795B594331820B1C41E2 (void);
// 0x000002DD LTDescr LeanTween::move(UnityEngine.GameObject,UnityEngine.Transform,System.Single)
extern void LeanTween_move_mAF8F6C1C95F4A9E38C99E9471889EF3D6F26B21B (void);
// 0x000002DE LTDescr LeanTween::rotate(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTween_rotate_mFE6F0AFD90CF08BC42B97AB676D30F5D6B685BB1 (void);
// 0x000002DF LTDescr LeanTween::rotate(LTRect,System.Single,System.Single)
extern void LeanTween_rotate_mCDB520E4B0ACB36B9EE004C8CF8B3C0A5EC0D9E2 (void);
// 0x000002E0 LTDescr LeanTween::rotateLocal(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTween_rotateLocal_m5B5E5981164221594D9D4FF15C84D7B9FB81C067 (void);
// 0x000002E1 LTDescr LeanTween::rotateX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_rotateX_mCBA79F277E3512C992772E15E9298BEAFAA046D0 (void);
// 0x000002E2 LTDescr LeanTween::rotateY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_rotateY_m14E1525768E32FDF7A2378B1C8AF023D1C0B1C24 (void);
// 0x000002E3 LTDescr LeanTween::rotateZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_rotateZ_mA0AD8A6E7771AB656243CD3669EBDB0E38CA2408 (void);
// 0x000002E4 LTDescr LeanTween::rotateAround(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTween_rotateAround_m588F9185E90CC14CAB0B0EB6C3D575CF7E7C285E (void);
// 0x000002E5 LTDescr LeanTween::rotateAroundLocal(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTween_rotateAroundLocal_mC869325E94AD6A0B95314D081F29B976CA91A81B (void);
// 0x000002E6 LTDescr LeanTween::scale(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTween_scale_m684CBC818ED1F1ED8C50D1BD0F49495CACC0067C (void);
// 0x000002E7 LTDescr LeanTween::scale(LTRect,UnityEngine.Vector2,System.Single)
extern void LeanTween_scale_m16AF41D6C5EB894246FDE21B3B9B90EADE1949AC (void);
// 0x000002E8 LTDescr LeanTween::scaleX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_scaleX_m4DCF8A388D1FE2AF9C40F67DDB77B958DE1F0220 (void);
// 0x000002E9 LTDescr LeanTween::scaleY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_scaleY_m011130E1548F777DC187AEF2A88291CDE924851B (void);
// 0x000002EA LTDescr LeanTween::scaleZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_scaleZ_m2CF069E5E4EC0E4BC040DBA5C759E1A401A67481 (void);
// 0x000002EB LTDescr LeanTween::value(UnityEngine.GameObject,System.Single,System.Single,System.Single)
extern void LeanTween_value_m16722DEDC681F28C188D0F4557F0BF9B6A1488AF (void);
// 0x000002EC LTDescr LeanTween::value(System.Single,System.Single,System.Single)
extern void LeanTween_value_m85D0A108DCD42E6A1576B1CC53D2F9D00B0E47FC (void);
// 0x000002ED LTDescr LeanTween::value(UnityEngine.GameObject,UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void LeanTween_value_m6B91F8AA266FFF0C593FF90E7A6BE3D1960B84E9 (void);
// 0x000002EE LTDescr LeanTween::value(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void LeanTween_value_m45BC8CC34DB8AE45C89048BAAAA016A4C6F0D526 (void);
// 0x000002EF LTDescr LeanTween::value(UnityEngine.GameObject,UnityEngine.Color,UnityEngine.Color,System.Single)
extern void LeanTween_value_m91EB5E56B4A910CDE7D3F1CD5EFA1E352FEC04D8 (void);
// 0x000002F0 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`1<System.Single>,System.Single,System.Single,System.Single)
extern void LeanTween_value_mAC251E42C465A03544154ADD2059E8808E208F4A (void);
// 0x000002F1 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`2<System.Single,System.Single>,System.Single,System.Single,System.Single)
extern void LeanTween_value_mDA02C25B8315D819999BA319C0E183E3534BC0D9 (void);
// 0x000002F2 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`1<UnityEngine.Color>,UnityEngine.Color,UnityEngine.Color,System.Single)
extern void LeanTween_value_mEA929F405968830C7B82A604B40F5810DA0274B8 (void);
// 0x000002F3 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`2<UnityEngine.Color,System.Object>,UnityEngine.Color,UnityEngine.Color,System.Single)
extern void LeanTween_value_m3F5B71A925784CD187198B3AB14CDB08351AE715 (void);
// 0x000002F4 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`1<UnityEngine.Vector2>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void LeanTween_value_m214501A86FB206A4A18BE7C373F67B1E3BF90E17 (void);
// 0x000002F5 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`1<UnityEngine.Vector3>,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void LeanTween_value_mEE3F62AA3FED7E7FFAF54C8CB689045550F31E2B (void);
// 0x000002F6 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`2<System.Single,System.Object>,System.Single,System.Single,System.Single)
extern void LeanTween_value_m062FD40F3682DD46D70A1FD5740CE32C421BE536 (void);
// 0x000002F7 LTDescr LeanTween::delayedSound(UnityEngine.AudioClip,UnityEngine.Vector3,System.Single)
extern void LeanTween_delayedSound_mEE95C5C431F6AE155BC4B5CE22679353E5BC5BCA (void);
// 0x000002F8 LTDescr LeanTween::delayedSound(UnityEngine.GameObject,UnityEngine.AudioClip,UnityEngine.Vector3,System.Single)
extern void LeanTween_delayedSound_mD975E34F6E1D2545468F716845C850F049D1C430 (void);
// 0x000002F9 LTDescr LeanTween::move(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single)
extern void LeanTween_move_m255964EF3EF82F6F371AFCAF934CDBE555E31CA4 (void);
// 0x000002FA LTDescr LeanTween::moveX(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_moveX_m968EB608281BDCD184F6B71F22A36B1BD0B32FF5 (void);
// 0x000002FB LTDescr LeanTween::moveY(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_moveY_m8B0C0BD32AECB7A9F1FE4287A1303F8A5BAA225A (void);
// 0x000002FC LTDescr LeanTween::moveZ(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_moveZ_m2E0856F50D20B11039FB477A7F28124713884DC3 (void);
// 0x000002FD LTDescr LeanTween::rotate(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_rotate_m2D1DFCC15465567BB121704213FAA70FD5C336B6 (void);
// 0x000002FE LTDescr LeanTween::rotate(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single)
extern void LeanTween_rotate_m76084F742248BE37F7A6B458BB34CA78DCFC36F5 (void);
// 0x000002FF LTDescr LeanTween::rotateAround(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTween_rotateAround_m3577D6688BDC54193F15C71894E6E55A69006630 (void);
// 0x00000300 LTDescr LeanTween::rotateAroundLocal(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTween_rotateAroundLocal_mD02923DFFD7FAD7DCBF034734D058E7C99EB8D44 (void);
// 0x00000301 LTDescr LeanTween::scale(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single)
extern void LeanTween_scale_mC4B678FE9E438AD23893A27F09E3C007F2EBEABA (void);
// 0x00000302 LTDescr LeanTween::size(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single)
extern void LeanTween_size_m28011A80D7FC887385A015C3F499A3F33C339E08 (void);
// 0x00000303 LTDescr LeanTween::alpha(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_alpha_mC673E74E2397289FEC358CEFC8E691FF25680886 (void);
// 0x00000304 LTDescr LeanTween::color(UnityEngine.RectTransform,UnityEngine.Color,System.Single)
extern void LeanTween_color_m173A290E75DEDA181F8F8BDEDF62C1B12550830B (void);
// 0x00000305 System.Single LeanTween::tweenOnCurve(LTDescr,System.Single)
extern void LeanTween_tweenOnCurve_mCF1C2DBF18A825484F5213591A74D0406108BE78 (void);
// 0x00000306 UnityEngine.Vector3 LeanTween::tweenOnCurveVector(LTDescr,System.Single)
extern void LeanTween_tweenOnCurveVector_m2F3889ED1C5209DCB8DF39AAAE310848E3AEB882 (void);
// 0x00000307 System.Single LeanTween::easeOutQuadOpt(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutQuadOpt_mA9D54EA2DFA885DCA918FF829C7EAFED0567F0C1 (void);
// 0x00000308 System.Single LeanTween::easeInQuadOpt(System.Single,System.Single,System.Single)
extern void LeanTween_easeInQuadOpt_mA07BD22C4B17A4FE3B44BC927D18C6E14036811C (void);
// 0x00000309 System.Single LeanTween::easeInOutQuadOpt(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutQuadOpt_m5CB73FF539B12D06CD85E6DC09AF281E3FEC2D86 (void);
// 0x0000030A UnityEngine.Vector3 LeanTween::easeInOutQuadOpt(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void LeanTween_easeInOutQuadOpt_mE09DD740EAEC7F818C4AC10178F11B6587DDAFF0 (void);
// 0x0000030B System.Single LeanTween::linear(System.Single,System.Single,System.Single)
extern void LeanTween_linear_mC13897A14BF6CAD2E64BD36644CF8D9AF8EE2F0C (void);
// 0x0000030C System.Single LeanTween::clerp(System.Single,System.Single,System.Single)
extern void LeanTween_clerp_mF38CC191CF1F93FBA5FA125C1BBD362A8F492574 (void);
// 0x0000030D System.Single LeanTween::spring(System.Single,System.Single,System.Single)
extern void LeanTween_spring_mE01745BEC87C32BDF7EA6A91D0B321BADF5884AB (void);
// 0x0000030E System.Single LeanTween::easeInQuad(System.Single,System.Single,System.Single)
extern void LeanTween_easeInQuad_mFADDBAF1404502B07D54D73A5E3E6988D8FA3D24 (void);
// 0x0000030F System.Single LeanTween::easeOutQuad(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutQuad_mAC0C6A6D659FCDE5636A2AFC5D622B99B61808C7 (void);
// 0x00000310 System.Single LeanTween::easeInOutQuad(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutQuad_m71AA1E657CE443D018036A98715420BF5925E7E6 (void);
// 0x00000311 System.Single LeanTween::easeInOutQuadOpt2(System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutQuadOpt2_m292D5F817A9125E0415C3526F931AA81432A3B09 (void);
// 0x00000312 System.Single LeanTween::easeInCubic(System.Single,System.Single,System.Single)
extern void LeanTween_easeInCubic_m3D273E2955A78F68A0F2A9032C2A67E0BAEB6F87 (void);
// 0x00000313 System.Single LeanTween::easeOutCubic(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutCubic_m2DB944476129350B6D0700C846E3063346F07090 (void);
// 0x00000314 System.Single LeanTween::easeInOutCubic(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutCubic_m74507BCA5B56F0F295BA3C93D744D332B4B5CC53 (void);
// 0x00000315 System.Single LeanTween::easeInQuart(System.Single,System.Single,System.Single)
extern void LeanTween_easeInQuart_m48DE80F5CC0478F91AD6C3F63879747A0C75D1D9 (void);
// 0x00000316 System.Single LeanTween::easeOutQuart(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutQuart_m609DCDE1826DB79CB2B9985CD60D1B239A984772 (void);
// 0x00000317 System.Single LeanTween::easeInOutQuart(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutQuart_m0F5D0D7285A20C6D860CF36F25D5D99D59900B33 (void);
// 0x00000318 System.Single LeanTween::easeInQuint(System.Single,System.Single,System.Single)
extern void LeanTween_easeInQuint_mDB55F438F15637CA989A645BE894AAF9A004B647 (void);
// 0x00000319 System.Single LeanTween::easeOutQuint(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutQuint_mBB803F45CF6EE94AC581337373184BC955468FB2 (void);
// 0x0000031A System.Single LeanTween::easeInOutQuint(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutQuint_m63E5EFF41668EB890D1A86E424189FCEF9F7D4E2 (void);
// 0x0000031B System.Single LeanTween::easeInSine(System.Single,System.Single,System.Single)
extern void LeanTween_easeInSine_m04E033CDA7DEDAD91402A67441771684E9DF9585 (void);
// 0x0000031C System.Single LeanTween::easeOutSine(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutSine_mAD08688F562F7B020B432B9F176BFC4E7246E9CA (void);
// 0x0000031D System.Single LeanTween::easeInOutSine(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutSine_m19A665CD11C2430CE4D6B2EE7E49D5E4D69B255B (void);
// 0x0000031E System.Single LeanTween::easeInExpo(System.Single,System.Single,System.Single)
extern void LeanTween_easeInExpo_m5849B8DA222D18020D5DF8CF7CB21EFF6CAF918A (void);
// 0x0000031F System.Single LeanTween::easeOutExpo(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutExpo_m8047C00ED545EBB9A49539302273CBD72B99D0DB (void);
// 0x00000320 System.Single LeanTween::easeInOutExpo(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutExpo_mC75B771143EC08818C36CD0F1F7A3E5C94ED27DE (void);
// 0x00000321 System.Single LeanTween::easeInCirc(System.Single,System.Single,System.Single)
extern void LeanTween_easeInCirc_m1B31C81F16B5E9297D21C701D372DBD548319883 (void);
// 0x00000322 System.Single LeanTween::easeOutCirc(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutCirc_m68F5077C75EA1933F562B96FA0EAF3A114D87EE6 (void);
// 0x00000323 System.Single LeanTween::easeInOutCirc(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutCirc_m76125DC5F83F40B9931F8114B87A44962EADC567 (void);
// 0x00000324 System.Single LeanTween::easeInBounce(System.Single,System.Single,System.Single)
extern void LeanTween_easeInBounce_mE3DF8975A300367183C2C926938E6405B33112A5 (void);
// 0x00000325 System.Single LeanTween::easeOutBounce(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutBounce_m27698CDAABF99F8EDC7D6F6D6586433A9D12970E (void);
// 0x00000326 System.Single LeanTween::easeInOutBounce(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutBounce_m9450C71BE4DC36B9E7C7337E89D1BB131E5E0BFF (void);
// 0x00000327 System.Single LeanTween::easeInBack(System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeInBack_m0D8EFBAB1984C81F9A26AC9936DF26444E8CFDCD (void);
// 0x00000328 System.Single LeanTween::easeOutBack(System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeOutBack_m10B1A82299EE98AEF7C601D14A4D74F613849520 (void);
// 0x00000329 System.Single LeanTween::easeInOutBack(System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutBack_mA896015E78E55F1FBB1F29D8AA0359A74AC1E284 (void);
// 0x0000032A System.Single LeanTween::easeInElastic(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeInElastic_m0A27227BD0603697DA973D517017937FF956C483 (void);
// 0x0000032B System.Single LeanTween::easeOutElastic(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeOutElastic_mC63F462458A6CBE208E739E5F92B4D21A16E2ED4 (void);
// 0x0000032C System.Single LeanTween::easeInOutElastic(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutElastic_m62A56706E9907A81A5459B47709F75C1947E6473 (void);
// 0x0000032D LTDescr LeanTween::followDamp(UnityEngine.Transform,UnityEngine.Transform,LeanProp,System.Single,System.Single)
extern void LeanTween_followDamp_m5C9F35D1479612D8A617DA5EF9698BBA34BCFB5B (void);
// 0x0000032E LTDescr LeanTween::followSpring(UnityEngine.Transform,UnityEngine.Transform,LeanProp,System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_followSpring_m0897DA70DDD37F467E99B8E095157FCD30E02519 (void);
// 0x0000032F LTDescr LeanTween::followBounceOut(UnityEngine.Transform,UnityEngine.Transform,LeanProp,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_followBounceOut_m65AA5DB3E225B699DEF3111EF3FC5F33043D5F08 (void);
// 0x00000330 LTDescr LeanTween::followLinear(UnityEngine.Transform,UnityEngine.Transform,LeanProp,System.Single)
extern void LeanTween_followLinear_mE9D8D28E7216EB21260AEF49DC344EB5075E36E0 (void);
// 0x00000331 System.Void LeanTween::addListener(System.Int32,System.Action`1<LTEvent>)
extern void LeanTween_addListener_m3F4A9293AEBEC7AB6896D092CCD808317C3DCD97 (void);
// 0x00000332 System.Void LeanTween::addListener(UnityEngine.GameObject,System.Int32,System.Action`1<LTEvent>)
extern void LeanTween_addListener_mD93323361477B8F8671D3E0C2261A56967E4BBED (void);
// 0x00000333 System.Boolean LeanTween::removeListener(System.Int32,System.Action`1<LTEvent>)
extern void LeanTween_removeListener_mB3A2221839B0C781D1597674FC5E0674D8C189FA (void);
// 0x00000334 System.Boolean LeanTween::removeListener(System.Int32)
extern void LeanTween_removeListener_mC6FD437AF58BC94743A0A978FAD970886CEF0CAE (void);
// 0x00000335 System.Boolean LeanTween::removeListener(UnityEngine.GameObject,System.Int32,System.Action`1<LTEvent>)
extern void LeanTween_removeListener_m2DECCE672B558063CCED79464E5401FFC922DCE4 (void);
// 0x00000336 System.Void LeanTween::dispatchEvent(System.Int32)
extern void LeanTween_dispatchEvent_m1B94CB1AE76D30A0629498DC3CF920C16C3C5B80 (void);
// 0x00000337 System.Void LeanTween::dispatchEvent(System.Int32,System.Object)
extern void LeanTween_dispatchEvent_mA17A21F65DC5DFEA2312C5F1446BCD2A13D03C98 (void);
// 0x00000338 System.Void LeanTween::.ctor()
extern void LeanTween__ctor_m0E530009EEDFEFE39A618092205A560791154327 (void);
// 0x00000339 System.Void LeanTween::.cctor()
extern void LeanTween__cctor_m7C31F6E8304BED5D2568407206013A918B566DB8 (void);
// 0x0000033A System.Void LeanTween/<>c__DisplayClass193_0::.ctor()
extern void U3CU3Ec__DisplayClass193_0__ctor_m42DC73265951A655C0448FF60C880316D9DE1A43 (void);
// 0x0000033B System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__0()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__0_m191266906DD2277CDC3B46FF109CF35D23BBDAC7 (void);
// 0x0000033C System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__1()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__1_m704758A390EF812C25A3A65988859C02AFBED1C4 (void);
// 0x0000033D System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__2()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__2_mAA06748BE5AB364E8F927A9EE39224B64FD24398 (void);
// 0x0000033E System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__3()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__3_mD56CFC8DF59426863F6189980E747D8413AA5CA0 (void);
// 0x0000033F System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__4()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__4_m6D340E392A6FE3E0389103E0DDB77C9ED361D9ED (void);
// 0x00000340 System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__5()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__5_m0220EC49C37178D39C26609650CE24AAB28621FE (void);
// 0x00000341 System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__6()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__6_m2DC42525AE6AA25C9692CA049C80F6958918DB42 (void);
// 0x00000342 System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__7()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__7_mE7F490B90125F2A8B3C5B91FF517537313A0FCE0 (void);
// 0x00000343 System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__8()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__8_m007DC11D196C3911171092A96EFF8588C9CBF947 (void);
// 0x00000344 System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__9()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__9_m26FE2854DC255FCB17A99976AF3B1C9674AF3DA5 (void);
// 0x00000345 System.Void LeanTween/<>c__DisplayClass194_0::.ctor()
extern void U3CU3Ec__DisplayClass194_0__ctor_m7BE723A2115ACC48F87D807B5AD61D097D7B80CA (void);
// 0x00000346 System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__0()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__0_m96E779A4A9A701F3B635CFB3EBCC96458BD3FDC3 (void);
// 0x00000347 System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__1()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__1_mACD73E450FC2E3CE551EDDAC93A2F52A45C5054C (void);
// 0x00000348 System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__2()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__2_m56BDFF78D7668526967A93EA96C4F427DC600683 (void);
// 0x00000349 System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__3()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__3_m8F19785EA57FC703DDADF403EE7321F94343C995 (void);
// 0x0000034A System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__4()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__4_mD7590F09F10CE901B14B9E7C2D59281C17BF6D45 (void);
// 0x0000034B System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__5()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__5_m0173F460A2260824B38994CCB06B17F86A43BC00 (void);
// 0x0000034C System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__6()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__6_mDF693219BF56597185AC7A86A83B41CCAE50995D (void);
// 0x0000034D System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__7()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__7_m02E8CAD9D7320D317F1291024AEC66A7BA1DF8BC (void);
// 0x0000034E System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__8()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__8_m2815FB0A57623C0A7694BEE29292D36748AF2FEF (void);
// 0x0000034F System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__9()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__9_m2249E4B791535178AF9B4005460FC55DC089E8CA (void);
// 0x00000350 System.Void LeanTween/<>c__DisplayClass195_0::.ctor()
extern void U3CU3Ec__DisplayClass195_0__ctor_mBBB552F5EA7C92143790A2E28ECAAFA48EC7B4D7 (void);
// 0x00000351 System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__0()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__0_m85BF6FBF1E6DDC848ED2747320A820E6A16F69A9 (void);
// 0x00000352 System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__1()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__1_m8C32BD1F9A6489144955B3C032D8EFB4884F2ED6 (void);
// 0x00000353 System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__2()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__2_m3ADBC5765839ADC33E591B0E3F86D3BCEDD966D3 (void);
// 0x00000354 System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__3()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__3_m7F6DABA8926F943AB02A6C527D916B95331FD3C9 (void);
// 0x00000355 System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__4()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__4_mB8C14DF64A83794EB497687CD62EB8246F2F3039 (void);
// 0x00000356 System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__5()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__5_mA48C1D48F29E8F11F15E727577E658F0CAC0DD46 (void);
// 0x00000357 System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__6()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__6_m6915716A4F822A502028FEABC63C125E057402CD (void);
// 0x00000358 System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__7()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__7_mDDD6AF7E2634F4081FD3849FF6C18B87A3235250 (void);
// 0x00000359 System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__8()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__8_m90947880CD99C1E7247F57A1AC77692D6FFBFE69 (void);
// 0x0000035A System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__9()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__9_mB5FEE58DA881C5915A000A3B5988C3D58B578E29 (void);
// 0x0000035B System.Void LeanTween/<>c__DisplayClass196_0::.ctor()
extern void U3CU3Ec__DisplayClass196_0__ctor_m53A3A91F675B0FEBE70D8A183F4AEE80BB562132 (void);
// 0x0000035C System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__0()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__0_mFA5A42D63EE280D45A87226343CAED5E72B4259F (void);
// 0x0000035D System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__1()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__1_mA72FB34A781A70DE472941BE6213DAD4056A4156 (void);
// 0x0000035E System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__2()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__2_m3FFAD56268BA1B8BA721A870C39B2EB3FED3AFFF (void);
// 0x0000035F System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__3()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__3_m95959A86F6834CCBCE7080772F0C215459296987 (void);
// 0x00000360 System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__4()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__4_m17D9CC99AA674FE28A2596BAB8D7AF7EA0AC74F5 (void);
// 0x00000361 System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__5()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__5_m8CAABA8DEEB326B0DFBA45714E4DE40FA69D2FB9 (void);
// 0x00000362 System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__6()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__6_mDF3FFFFEFDD9AFFCA4883B15B5BA1C2C568B5238 (void);
// 0x00000363 System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__7()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__7_mF835E156C518889CC37B7B18196CB66817995693 (void);
// 0x00000364 System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__8()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__8_m20A928A64BB76244A147659349B4952FB2AEF119 (void);
// 0x00000365 System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__9()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__9_m485F0716E3A701BA24C7BE51462D4081218137BC (void);
// 0x00000366 UnityEngine.Vector3[] LTUtility::reverse(UnityEngine.Vector3[])
extern void LTUtility_reverse_m792C6EFC380420D2FD31BF47B331CBB50B650824 (void);
// 0x00000367 System.Void LTUtility::.ctor()
extern void LTUtility__ctor_m480117B4F53FC8EF08F8FA640618FDFEB2DA5730 (void);
// 0x00000368 System.Void LTBezier::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void LTBezier__ctor_mDE430564C41DA6E441FF61F9075DA00688964D8D (void);
// 0x00000369 System.Single LTBezier::map(System.Single)
extern void LTBezier_map_mC8981CF95BB7EE3FACDC51DC93EE0A638EB7FCD4 (void);
// 0x0000036A UnityEngine.Vector3 LTBezier::bezierPoint(System.Single)
extern void LTBezier_bezierPoint_mB8ECD0DB38F58EEFB1FCEFA03F96C4F6A04E6B9B (void);
// 0x0000036B UnityEngine.Vector3 LTBezier::point(System.Single)
extern void LTBezier_point_m11756123BEE5DB1D4A3D97B8920CB6434DE229FC (void);
// 0x0000036C System.Void LTBezierPath::.ctor()
extern void LTBezierPath__ctor_mC14B0C13FB3A48E608456696A8CA14DB6A6B803A (void);
// 0x0000036D System.Void LTBezierPath::.ctor(UnityEngine.Vector3[])
extern void LTBezierPath__ctor_m761CE6701D4AF322CDF34898E4A75EE3996EEF32 (void);
// 0x0000036E System.Void LTBezierPath::setPoints(UnityEngine.Vector3[])
extern void LTBezierPath_setPoints_m0326ECE7DAAF6A7FF84203D7C4DF88207765D100 (void);
// 0x0000036F System.Single LTBezierPath::get_distance()
extern void LTBezierPath_get_distance_mCC5AFBA165F1DE82DD616DCCD6A1DF46D6DC2268 (void);
// 0x00000370 UnityEngine.Vector3 LTBezierPath::point(System.Single)
extern void LTBezierPath_point_mE9E25CE7B83E1F6A9509A12EB82792D0F2AD08CF (void);
// 0x00000371 System.Void LTBezierPath::place2d(UnityEngine.Transform,System.Single)
extern void LTBezierPath_place2d_m86914A18D26070FDB3601E3F4B0A9F337C71D57D (void);
// 0x00000372 System.Void LTBezierPath::placeLocal2d(UnityEngine.Transform,System.Single)
extern void LTBezierPath_placeLocal2d_m3B554AF9CDDCAD494F07CFEAD024F82501474089 (void);
// 0x00000373 System.Void LTBezierPath::place(UnityEngine.Transform,System.Single)
extern void LTBezierPath_place_m92A56AFF394BB4A2B4997C09DC87CF712D67FB62 (void);
// 0x00000374 System.Void LTBezierPath::place(UnityEngine.Transform,System.Single,UnityEngine.Vector3)
extern void LTBezierPath_place_m089484993924A7E12BCFE5418BD9DA5AB802E664 (void);
// 0x00000375 System.Void LTBezierPath::placeLocal(UnityEngine.Transform,System.Single)
extern void LTBezierPath_placeLocal_m84ECB572C31B7DC9420B9E22745AF8E553EA8EBE (void);
// 0x00000376 System.Void LTBezierPath::placeLocal(UnityEngine.Transform,System.Single,UnityEngine.Vector3)
extern void LTBezierPath_placeLocal_mD55D6F5C237A9A3E3CF585A38697083AD608A6C9 (void);
// 0x00000377 System.Void LTBezierPath::gizmoDraw(System.Single)
extern void LTBezierPath_gizmoDraw_mF83D68461DAAD27D153D7C368B9A17CE33FFBA2C (void);
// 0x00000378 System.Single LTBezierPath::ratioAtPoint(UnityEngine.Vector3,System.Single)
extern void LTBezierPath_ratioAtPoint_m143BC2A593074BDC7858B6861351A0D044EC38C3 (void);
// 0x00000379 System.Void LTSpline::.ctor(UnityEngine.Vector3[])
extern void LTSpline__ctor_m9551907DED1FDB0E2E0530EC1773BEB94364C936 (void);
// 0x0000037A System.Void LTSpline::.ctor(UnityEngine.Vector3[],System.Boolean)
extern void LTSpline__ctor_m2F73C5AFFBC6626390D7B2FA507B202DD52225E2 (void);
// 0x0000037B System.Void LTSpline::init(UnityEngine.Vector3[],System.Boolean)
extern void LTSpline_init_m25191659D43D582146D1C29714BD5B7B01D6B5F8 (void);
// 0x0000037C UnityEngine.Vector3 LTSpline::map(System.Single)
extern void LTSpline_map_mDCB7147639A49699ED42F125EF6A5D27CA9E1485 (void);
// 0x0000037D UnityEngine.Vector3 LTSpline::interp(System.Single)
extern void LTSpline_interp_m75A500E14151D59BC9F7240B4AB73190AAB1E598 (void);
// 0x0000037E System.Single LTSpline::ratioAtPoint(UnityEngine.Vector3)
extern void LTSpline_ratioAtPoint_mC3196D78FBCD206F34E51D528EEB6D9DE5093B76 (void);
// 0x0000037F UnityEngine.Vector3 LTSpline::point(System.Single)
extern void LTSpline_point_m84BC62A0BE5F6F90E7AE7632BF88AD5BB332E75D (void);
// 0x00000380 System.Void LTSpline::place2d(UnityEngine.Transform,System.Single)
extern void LTSpline_place2d_mF79DEE3F7C5F54541047D267A2E88C681E9A0C52 (void);
// 0x00000381 System.Void LTSpline::placeLocal2d(UnityEngine.Transform,System.Single)
extern void LTSpline_placeLocal2d_m6BA2FF841EADD05FA6CFBAC808B8E7D6641D154B (void);
// 0x00000382 System.Void LTSpline::place(UnityEngine.Transform,System.Single)
extern void LTSpline_place_mEC9C1AB39B6FAFC9D8F41CEE63391BCB2A577DD9 (void);
// 0x00000383 System.Void LTSpline::place(UnityEngine.Transform,System.Single,UnityEngine.Vector3)
extern void LTSpline_place_m2F73FDFFE8520633664738EA4D3E769154EC645E (void);
// 0x00000384 System.Void LTSpline::placeLocal(UnityEngine.Transform,System.Single)
extern void LTSpline_placeLocal_m861F5C7F91F4A15BD04D1F2C66E196219AC2B10F (void);
// 0x00000385 System.Void LTSpline::placeLocal(UnityEngine.Transform,System.Single,UnityEngine.Vector3)
extern void LTSpline_placeLocal_m8A2D1C6DA0C7BC6E9B61E6577998C6397000F388 (void);
// 0x00000386 System.Void LTSpline::gizmoDraw(System.Single)
extern void LTSpline_gizmoDraw_mAADED3A63F49D610C035F71DC725D214895189B9 (void);
// 0x00000387 System.Void LTSpline::drawGizmo(UnityEngine.Color)
extern void LTSpline_drawGizmo_mE14FCC2AE443D4F66CB45A64754557476051945A (void);
// 0x00000388 System.Void LTSpline::drawGizmo(UnityEngine.Transform[],UnityEngine.Color)
extern void LTSpline_drawGizmo_mFC2AB1F7539EB9239D2AC471B276CC5A380A6A5C (void);
// 0x00000389 System.Void LTSpline::drawLine(UnityEngine.Transform[],System.Single,UnityEngine.Color)
extern void LTSpline_drawLine_m4CEECA0A218EA8067F860F48FAC4E6CDD27EB7E7 (void);
// 0x0000038A System.Void LTSpline::drawLinesGLLines(UnityEngine.Material,UnityEngine.Color,System.Single)
extern void LTSpline_drawLinesGLLines_m248D051DAF686041049E3994F3F3FCA9434DB823 (void);
// 0x0000038B UnityEngine.Vector3[] LTSpline::generateVectors()
extern void LTSpline_generateVectors_mECF369B072F107D79E9AD06DD4D7BBBF5E5716D3 (void);
// 0x0000038C System.Void LTSpline::.cctor()
extern void LTSpline__cctor_mEB46040AD009E27CB5CF44A9F39148191983681E (void);
// 0x0000038D System.Void LTRect::.ctor()
extern void LTRect__ctor_m99E599C0ECB67B47DEE7F91377ACB404E81FDD9C (void);
// 0x0000038E System.Void LTRect::.ctor(UnityEngine.Rect)
extern void LTRect__ctor_mA9DDB568C78410BF943CEC80A10FCC9FE8E7E0CB (void);
// 0x0000038F System.Void LTRect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void LTRect__ctor_m3894896A04C8DE5CF36FEB67B77A9D51471AA5EE (void);
// 0x00000390 System.Void LTRect::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LTRect__ctor_m3FE04D973AF8662CF993C5C7A99A8190AF660DFA (void);
// 0x00000391 System.Void LTRect::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LTRect__ctor_m1FE9274A3C18C4E81E0E68C91DDC727712CC0005 (void);
// 0x00000392 System.Boolean LTRect::get_hasInitiliazed()
extern void LTRect_get_hasInitiliazed_mB71593427DCCF0404DEB52065AABFF1762158555 (void);
// 0x00000393 System.Int32 LTRect::get_id()
extern void LTRect_get_id_mD85F4AFF11D30E3CFB7F39142F98EB798EBC2950 (void);
// 0x00000394 System.Void LTRect::setId(System.Int32,System.Int32)
extern void LTRect_setId_mFE916182B6874208861CD518BEE9F0C89EF7EF8D (void);
// 0x00000395 System.Void LTRect::reset()
extern void LTRect_reset_m79849D25F50652D6BCE8CA8F3F329EE9511852DA (void);
// 0x00000396 System.Void LTRect::resetForRotation()
extern void LTRect_resetForRotation_mADBFF61280065301AADF66876334779462287788 (void);
// 0x00000397 System.Single LTRect::get_x()
extern void LTRect_get_x_m515C6A230B6B72CB215A27535B967428BD6DC3A7 (void);
// 0x00000398 System.Void LTRect::set_x(System.Single)
extern void LTRect_set_x_m2F416C9557DD5B8F51C00D0710A634CD1B25FEEE (void);
// 0x00000399 System.Single LTRect::get_y()
extern void LTRect_get_y_mA1414A3C465D24CCA90A9C73D6EEC35F7411C957 (void);
// 0x0000039A System.Void LTRect::set_y(System.Single)
extern void LTRect_set_y_m40A2235B9E00614E61213F394430C86962E9CA42 (void);
// 0x0000039B System.Single LTRect::get_width()
extern void LTRect_get_width_m04A3F854E9585909A7948905CFFCC4C3AA726B60 (void);
// 0x0000039C System.Void LTRect::set_width(System.Single)
extern void LTRect_set_width_mC8D40CA7ECCEA4B3E2E3B0C0DFC573CD6A461888 (void);
// 0x0000039D System.Single LTRect::get_height()
extern void LTRect_get_height_mB4F9445DF5CF2F82979B009EEF48A4F22634CE44 (void);
// 0x0000039E System.Void LTRect::set_height(System.Single)
extern void LTRect_set_height_m52D3E7ACF7372560DD7412E524171FBF34B2FBE2 (void);
// 0x0000039F UnityEngine.Rect LTRect::get_rect()
extern void LTRect_get_rect_m775DA6B41DFC9720EDBAA6ECD285058C4BE41505 (void);
// 0x000003A0 System.Void LTRect::set_rect(UnityEngine.Rect)
extern void LTRect_set_rect_m09C60F3DDB77909404D570E60060AEDD73BFD969 (void);
// 0x000003A1 LTRect LTRect::setStyle(UnityEngine.GUIStyle)
extern void LTRect_setStyle_m5E5B4BC5ACD18F9ACC62171F440460CBB4B549F1 (void);
// 0x000003A2 LTRect LTRect::setFontScaleToFit(System.Boolean)
extern void LTRect_setFontScaleToFit_mA9F65B0883725878887560F549C2F4820445CF3B (void);
// 0x000003A3 LTRect LTRect::setColor(UnityEngine.Color)
extern void LTRect_setColor_mD5BD0C65451DDD14F2927F5BFA617DB93B9E2CC6 (void);
// 0x000003A4 LTRect LTRect::setAlpha(System.Single)
extern void LTRect_setAlpha_m5F785B6E9D8C6DFD7CC751417A86959041F5D845 (void);
// 0x000003A5 LTRect LTRect::setLabel(System.String)
extern void LTRect_setLabel_m87C389A819F76DBCEABE8968C2C357F43EEAC6DC (void);
// 0x000003A6 LTRect LTRect::setUseSimpleScale(System.Boolean,UnityEngine.Rect)
extern void LTRect_setUseSimpleScale_m9C2401689E18E8B93BC6C9A95E781226E765E81E (void);
// 0x000003A7 LTRect LTRect::setUseSimpleScale(System.Boolean)
extern void LTRect_setUseSimpleScale_mF4509500DEBCBD33A6CBFE6F4B7C2BC4D75D845C (void);
// 0x000003A8 LTRect LTRect::setSizeByHeight(System.Boolean)
extern void LTRect_setSizeByHeight_m002DF767AC9C9ACCF561B65C2809C32FE07D8759 (void);
// 0x000003A9 System.String LTRect::ToString()
extern void LTRect_ToString_m1034234D679BFDD3FAEB6E69E031652F13022AA8 (void);
// 0x000003AA System.Void LTEvent::.ctor(System.Int32,System.Object)
extern void LTEvent__ctor_m6ABB897986AE37C647A526BA35CD577196C80053 (void);
// 0x000003AB System.Void LTGUI::init()
extern void LTGUI_init_m90D0DAAF20EA870C10FC394D34AE654788D6E9E3 (void);
// 0x000003AC System.Void LTGUI::initRectCheck()
extern void LTGUI_initRectCheck_m01927ECCEAA6B39214CAEC2DA11C59C7D7A417DC (void);
// 0x000003AD System.Void LTGUI::reset()
extern void LTGUI_reset_m4AF33FEE1C4FFF585B61E00329512D26D9348405 (void);
// 0x000003AE System.Void LTGUI::update(System.Int32)
extern void LTGUI_update_m346A6915B0DA4CAE2E17FE892D39122992599AE0 (void);
// 0x000003AF System.Boolean LTGUI::checkOnScreen(UnityEngine.Rect)
extern void LTGUI_checkOnScreen_m018B856681A029B796023FFC001610DB7C87497A (void);
// 0x000003B0 System.Void LTGUI::destroy(System.Int32)
extern void LTGUI_destroy_mF6818BEF1B1A6510F9F38B20B4D4ACA1E4BAECE4 (void);
// 0x000003B1 System.Void LTGUI::destroyAll(System.Int32)
extern void LTGUI_destroyAll_m6C70120370B727440C087BC73ED1F62914431B5E (void);
// 0x000003B2 LTRect LTGUI::label(UnityEngine.Rect,System.String,System.Int32)
extern void LTGUI_label_m568FA2530F19BB9D082825B7406336A90CA7BF19 (void);
// 0x000003B3 LTRect LTGUI::label(LTRect,System.String,System.Int32)
extern void LTGUI_label_mF0A3E4EA3BF86BFFAFF265B1E848418ADF91B4CA (void);
// 0x000003B4 LTRect LTGUI::texture(UnityEngine.Rect,UnityEngine.Texture,System.Int32)
extern void LTGUI_texture_m18FCD865307F279E7E6A0CF29D3E68DA0125F09E (void);
// 0x000003B5 LTRect LTGUI::texture(LTRect,UnityEngine.Texture,System.Int32)
extern void LTGUI_texture_mD8320410FA4A576CAC639511DAD9271A8C2D32EA (void);
// 0x000003B6 LTRect LTGUI::element(LTRect,System.Int32)
extern void LTGUI_element_mB7414EFDA599B3627466CD84A505842398B3BDAD (void);
// 0x000003B7 System.Boolean LTGUI::hasNoOverlap(UnityEngine.Rect,System.Int32)
extern void LTGUI_hasNoOverlap_m8F6CBA73181394C020334257C98D15D73222A21B (void);
// 0x000003B8 System.Boolean LTGUI::pressedWithinRect(UnityEngine.Rect)
extern void LTGUI_pressedWithinRect_m6955BEF35822977E4C488F640E982C05624D2C87 (void);
// 0x000003B9 System.Boolean LTGUI::checkWithinRect(UnityEngine.Vector2,UnityEngine.Rect)
extern void LTGUI_checkWithinRect_m7BD673AE0509E6E36A939C2A15547FB1B35425DE (void);
// 0x000003BA UnityEngine.Vector2 LTGUI::firstTouch()
extern void LTGUI_firstTouch_m59A232DAB90147A8D67F2636CF126E3868B749A3 (void);
// 0x000003BB System.Void LTGUI::.ctor()
extern void LTGUI__ctor_m3DF0E374662C139E92CE25BD00208671EA425500 (void);
// 0x000003BC System.Void LTGUI::.cctor()
extern void LTGUI__cctor_m4A1C16EF1B32B267816AE59E983E024BDF672F06 (void);
// 0x000003BD LTDescr LeanTweenExt::LeanAlpha(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanAlpha_m622BA3E2E95AA0C3ED769D14693452C71B464202 (void);
// 0x000003BE LTDescr LeanTweenExt::LeanAlphaVertex(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanAlphaVertex_m276A64D30C7DBC2962D952F39164E74266A80805 (void);
// 0x000003BF LTDescr LeanTweenExt::LeanAlpha(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTweenExt_LeanAlpha_m5D4F86AB57DEAB1788098157DD3858155B41A026 (void);
// 0x000003C0 LTDescr LeanTweenExt::LeanAlpha(UnityEngine.CanvasGroup,System.Single,System.Single)
extern void LeanTweenExt_LeanAlpha_m80C6BB78F56CBCBAF562EA721D66F568AEB253E5 (void);
// 0x000003C1 LTDescr LeanTweenExt::LeanAlphaText(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTweenExt_LeanAlphaText_m2E879D95FD0A499D43742BC7E19C70FC00DB3C2F (void);
// 0x000003C2 System.Void LeanTweenExt::LeanCancel(UnityEngine.GameObject)
extern void LeanTweenExt_LeanCancel_mF59992A82C61655FB9C3C02CAA26A659B9DE3646 (void);
// 0x000003C3 System.Void LeanTweenExt::LeanCancel(UnityEngine.GameObject,System.Boolean)
extern void LeanTweenExt_LeanCancel_mD017D8DDC025544954D2E68BC260FC83C9469661 (void);
// 0x000003C4 System.Void LeanTweenExt::LeanCancel(UnityEngine.GameObject,System.Int32,System.Boolean)
extern void LeanTweenExt_LeanCancel_mC35E34FD1DF38362DA3970677A1A3745F02873E5 (void);
// 0x000003C5 System.Void LeanTweenExt::LeanCancel(UnityEngine.RectTransform)
extern void LeanTweenExt_LeanCancel_m17794FCEC25F233AC350724707F017F476BEA6B1 (void);
// 0x000003C6 LTDescr LeanTweenExt::LeanColor(UnityEngine.GameObject,UnityEngine.Color,System.Single)
extern void LeanTweenExt_LeanColor_m444318E458A875BB4CB2A0DD294D992CD93EBEFD (void);
// 0x000003C7 LTDescr LeanTweenExt::LeanColorText(UnityEngine.RectTransform,UnityEngine.Color,System.Single)
extern void LeanTweenExt_LeanColorText_m400922DC92DE54BE07F329E4949145CF60571731 (void);
// 0x000003C8 LTDescr LeanTweenExt::LeanDelayedCall(UnityEngine.GameObject,System.Single,System.Action)
extern void LeanTweenExt_LeanDelayedCall_m2377F6B8E51CB5CDEA727E0ECB5719E78E1E6DC8 (void);
// 0x000003C9 LTDescr LeanTweenExt::LeanDelayedCall(UnityEngine.GameObject,System.Single,System.Action`1<System.Object>)
extern void LeanTweenExt_LeanDelayedCall_m813456BD5A6C3BEF150219E1FB7FFECF3CFDB8FB (void);
// 0x000003CA System.Boolean LeanTweenExt::LeanIsPaused(UnityEngine.GameObject)
extern void LeanTweenExt_LeanIsPaused_m004A9E480599A969A094BBF5EAB8298D25F72877 (void);
// 0x000003CB System.Boolean LeanTweenExt::LeanIsPaused(UnityEngine.RectTransform)
extern void LeanTweenExt_LeanIsPaused_mAB912B24C71D148315C8E71232F9B54C2FADB5A4 (void);
// 0x000003CC System.Boolean LeanTweenExt::LeanIsTweening(UnityEngine.GameObject)
extern void LeanTweenExt_LeanIsTweening_mECC74A5F933AEEEC71D8EBE7E977639DDD124F59 (void);
// 0x000003CD LTDescr LeanTweenExt::LeanMove(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanMove_m054FBB59F420198B550A6069D9F9E6ABAE6AB43A (void);
// 0x000003CE LTDescr LeanTweenExt::LeanMove(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanMove_m615AA30F73F0ADF408BCE5B3EB2E7B0107F613DF (void);
// 0x000003CF LTDescr LeanTweenExt::LeanMove(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanMove_mE09C2744CD6F66260CCB2AE85EEC189D1739A95C (void);
// 0x000003D0 LTDescr LeanTweenExt::LeanMove(UnityEngine.GameObject,UnityEngine.Vector2,System.Single)
extern void LeanTweenExt_LeanMove_m213A2BEDDDE4818C47346D971A3D99CCE02F1E51 (void);
// 0x000003D1 LTDescr LeanTweenExt::LeanMove(UnityEngine.Transform,UnityEngine.Vector2,System.Single)
extern void LeanTweenExt_LeanMove_m0A44978D179428622357E68D3AE31FCD5597B014 (void);
// 0x000003D2 LTDescr LeanTweenExt::LeanMove(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTweenExt_LeanMove_m47C512A7C68F548F0C20D61A9EBC21444A89F47F (void);
// 0x000003D3 LTDescr LeanTweenExt::LeanMove(UnityEngine.GameObject,LTBezierPath,System.Single)
extern void LeanTweenExt_LeanMove_m28CCB67D4CD2B89B5A3F25E9D70842C949AD0990 (void);
// 0x000003D4 LTDescr LeanTweenExt::LeanMove(UnityEngine.GameObject,LTSpline,System.Single)
extern void LeanTweenExt_LeanMove_mB1E50A4734E3C3CC223FEB98A394DBDBA9614850 (void);
// 0x000003D5 LTDescr LeanTweenExt::LeanMove(UnityEngine.Transform,UnityEngine.Vector3[],System.Single)
extern void LeanTweenExt_LeanMove_m0BF63EB3A8D4B614817EA402EB1CC4B5F2C83328 (void);
// 0x000003D6 LTDescr LeanTweenExt::LeanMove(UnityEngine.Transform,LTBezierPath,System.Single)
extern void LeanTweenExt_LeanMove_m184D419AE52BA8FFCD237883D4118434DA2F40BB (void);
// 0x000003D7 LTDescr LeanTweenExt::LeanMove(UnityEngine.Transform,LTSpline,System.Single)
extern void LeanTweenExt_LeanMove_m31A80E7BD7568CEFF3D7B4073D2BA176A9ADF3DF (void);
// 0x000003D8 LTDescr LeanTweenExt::LeanMoveLocal(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanMoveLocal_m15178FB562F6F85C35B682AF97FCA004BDA1946F (void);
// 0x000003D9 LTDescr LeanTweenExt::LeanMoveLocal(UnityEngine.GameObject,LTBezierPath,System.Single)
extern void LeanTweenExt_LeanMoveLocal_m91600FA81E8D5DB2D9C69E4F7C5FC09CC40D2DC1 (void);
// 0x000003DA LTDescr LeanTweenExt::LeanMoveLocal(UnityEngine.GameObject,LTSpline,System.Single)
extern void LeanTweenExt_LeanMoveLocal_m98D7F1A2052D0F25DEE7B58DE411EE62A8B27CF2 (void);
// 0x000003DB LTDescr LeanTweenExt::LeanMoveLocal(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanMoveLocal_m920B50D3FBD4DC072118E879AFC151D682AE64B0 (void);
// 0x000003DC LTDescr LeanTweenExt::LeanMoveLocal(UnityEngine.Transform,LTBezierPath,System.Single)
extern void LeanTweenExt_LeanMoveLocal_mE4CF4E64AC16B510FB904060DDC2AE8BB231AA51 (void);
// 0x000003DD LTDescr LeanTweenExt::LeanMoveLocal(UnityEngine.Transform,LTSpline,System.Single)
extern void LeanTweenExt_LeanMoveLocal_mA49B57CD94C59A10CA8AF9D53F71C48286EAFFD0 (void);
// 0x000003DE LTDescr LeanTweenExt::LeanMoveLocalX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveLocalX_m8847296A8B3401D2D054027ABB659B296FF73760 (void);
// 0x000003DF LTDescr LeanTweenExt::LeanMoveLocalY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveLocalY_m86889CA4B214309EEAC239C3F175937E24310FD6 (void);
// 0x000003E0 LTDescr LeanTweenExt::LeanMoveLocalZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveLocalZ_m248064CFFDF7E42AC612D7F72C1E0F2DA37DCFB4 (void);
// 0x000003E1 LTDescr LeanTweenExt::LeanMoveLocalX(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveLocalX_mEAB28137FE65A74C1F4963ADCFF044CD5DB7CF69 (void);
// 0x000003E2 LTDescr LeanTweenExt::LeanMoveLocalY(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveLocalY_m727F1ED96B0D8FBF32EBB69787F82561886A07FA (void);
// 0x000003E3 LTDescr LeanTweenExt::LeanMoveLocalZ(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveLocalZ_m731E4A396200E9C165B47A327CB59CCDB9817707 (void);
// 0x000003E4 LTDescr LeanTweenExt::LeanMoveSpline(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTweenExt_LeanMoveSpline_m0E5CBB6704835E27EDA9B25D2915E8D2360E11A4 (void);
// 0x000003E5 LTDescr LeanTweenExt::LeanMoveSpline(UnityEngine.GameObject,LTSpline,System.Single)
extern void LeanTweenExt_LeanMoveSpline_mF640B6FFF8B58004A5C0A4475FE36E2C5BBAC607 (void);
// 0x000003E6 LTDescr LeanTweenExt::LeanMoveSpline(UnityEngine.Transform,UnityEngine.Vector3[],System.Single)
extern void LeanTweenExt_LeanMoveSpline_m3EE259E4A0061A1497D195E7910EAE9CD0B984C5 (void);
// 0x000003E7 LTDescr LeanTweenExt::LeanMoveSpline(UnityEngine.Transform,LTSpline,System.Single)
extern void LeanTweenExt_LeanMoveSpline_m59EF921EB07DAFBB715DAB0BBCFAC8003E67BD6C (void);
// 0x000003E8 LTDescr LeanTweenExt::LeanMoveSplineLocal(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTweenExt_LeanMoveSplineLocal_m667397DE03EFD1DBE0CE5F6CC2287807C3A56761 (void);
// 0x000003E9 LTDescr LeanTweenExt::LeanMoveSplineLocal(UnityEngine.Transform,UnityEngine.Vector3[],System.Single)
extern void LeanTweenExt_LeanMoveSplineLocal_mB8706C86995411A7D6408A04AB6B3545922C192D (void);
// 0x000003EA LTDescr LeanTweenExt::LeanMoveX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveX_m95E776B4AFF3B3472159C21ACCA7A074FD6250E1 (void);
// 0x000003EB LTDescr LeanTweenExt::LeanMoveX(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveX_m5EA136BF257503853FF984AB882BADCEDD22F864 (void);
// 0x000003EC LTDescr LeanTweenExt::LeanMoveX(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveX_m2E387E7F079BC81CCBE68B4D705C31E581639352 (void);
// 0x000003ED LTDescr LeanTweenExt::LeanMoveY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveY_mD3811BDA209686DCA906AB52B2CDD29F337FD6F4 (void);
// 0x000003EE LTDescr LeanTweenExt::LeanMoveY(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveY_m65DEFE466DE737FAB4B1DEF0BD8DC8ED22D03433 (void);
// 0x000003EF LTDescr LeanTweenExt::LeanMoveY(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveY_m846E64F6EED274DD02405DF113895FD366BA0FBB (void);
// 0x000003F0 LTDescr LeanTweenExt::LeanMoveZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveZ_mC2E89CF6F065D18A1F456935AF7EFA51091F0D9B (void);
// 0x000003F1 LTDescr LeanTweenExt::LeanMoveZ(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveZ_m85D8863F40BBF5A99203396C72B421E022FAA417 (void);
// 0x000003F2 LTDescr LeanTweenExt::LeanMoveZ(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveZ_mD47791E76E05328EAE363F9A3D0B8DAE6713902A (void);
// 0x000003F3 System.Void LeanTweenExt::LeanPause(UnityEngine.GameObject)
extern void LeanTweenExt_LeanPause_m3B5948BE9F86AAB0C837383D547DF2B291B8EA43 (void);
// 0x000003F4 LTDescr LeanTweenExt::LeanPlay(UnityEngine.RectTransform,UnityEngine.Sprite[])
extern void LeanTweenExt_LeanPlay_m6DCF6A71031221B880B66660EFF17720C6F1A42D (void);
// 0x000003F5 System.Void LeanTweenExt::LeanResume(UnityEngine.GameObject)
extern void LeanTweenExt_LeanResume_m0C9B09411CA1C367B2E2F1E74ECC4860BB400BF4 (void);
// 0x000003F6 LTDescr LeanTweenExt::LeanRotate(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanRotate_m9A3EA584BE26EADB024F0E5EC57B0862F3B0BEAE (void);
// 0x000003F7 LTDescr LeanTweenExt::LeanRotate(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanRotate_m46EDCCD0F931B4488BF8EDE72F0B1BA6BD65261F (void);
// 0x000003F8 LTDescr LeanTweenExt::LeanRotate(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanRotate_mF34054132FFA550762A3C04473FEF3FB54931CA0 (void);
// 0x000003F9 LTDescr LeanTweenExt::LeanRotateAround(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateAround_m97EA89B0C3FDE7B29B2A5F2FE3BE10F27B0C9993 (void);
// 0x000003FA LTDescr LeanTweenExt::LeanRotateAround(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateAround_m9922AD8260E8767A2D33963F2C9BA18ADDF22D61 (void);
// 0x000003FB LTDescr LeanTweenExt::LeanRotateAround(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateAround_m7F47B45F2074B30B22D75DC117BCF0A1351710E3 (void);
// 0x000003FC LTDescr LeanTweenExt::LeanRotateAroundLocal(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateAroundLocal_m43F940634C80C6D459B9DACF22F06E1E42F29116 (void);
// 0x000003FD LTDescr LeanTweenExt::LeanRotateAroundLocal(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateAroundLocal_m81397BB2C883AC5B3D62716FC3C93F43AE4EE5EB (void);
// 0x000003FE LTDescr LeanTweenExt::LeanRotateAroundLocal(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateAroundLocal_mE1E669F2BCAAEA47AB4785D6BBFAE413CC74F053 (void);
// 0x000003FF LTDescr LeanTweenExt::LeanRotateX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateX_m0C6142BF35F74AC9A912509713E53E9F1B3F1A20 (void);
// 0x00000400 LTDescr LeanTweenExt::LeanRotateX(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateX_mD09A3A174615A5479F81DFF31192673DADBF3324 (void);
// 0x00000401 LTDescr LeanTweenExt::LeanRotateY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateY_mE334362113CE7DA83AB29EFA1AAB8BFEB8C326E0 (void);
// 0x00000402 LTDescr LeanTweenExt::LeanRotateY(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateY_m823DD2E4B248E09CA4E2CDDFD2D791DF8527FCD2 (void);
// 0x00000403 LTDescr LeanTweenExt::LeanRotateZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateZ_m587B987933373ED9B32F6D319F158899A25AF9C3 (void);
// 0x00000404 LTDescr LeanTweenExt::LeanRotateZ(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateZ_m25AA9563CCFC5754ABE0CDE84FE8E05F09B0357F (void);
// 0x00000405 LTDescr LeanTweenExt::LeanScale(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanScale_m883D32607662D32DAB92DAF865CF110680BF18B3 (void);
// 0x00000406 LTDescr LeanTweenExt::LeanScale(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanScale_m572C2CB624F08B589CA3F7CEDA18D0521D396DAF (void);
// 0x00000407 LTDescr LeanTweenExt::LeanScale(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanScale_mFDA8A475A889665712B39717572167439241C6D5 (void);
// 0x00000408 LTDescr LeanTweenExt::LeanScaleX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanScaleX_m9AC6DED15F097E50134DF4213F86799F3A3F370B (void);
// 0x00000409 LTDescr LeanTweenExt::LeanScaleX(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanScaleX_m469A49E471B4D706CF5859D368CD3F13DD4FBA67 (void);
// 0x0000040A LTDescr LeanTweenExt::LeanScaleY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanScaleY_mEF7B8B2B411B70E312A061227CC9EF7496F1A720 (void);
// 0x0000040B LTDescr LeanTweenExt::LeanScaleY(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanScaleY_mC152F2D04B0CB5770AF0157B8BA217DDC0881D61 (void);
// 0x0000040C LTDescr LeanTweenExt::LeanScaleZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanScaleZ_m23CAE903347B1DFEA59FFFB127419620967227F1 (void);
// 0x0000040D LTDescr LeanTweenExt::LeanScaleZ(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanScaleZ_m2C148ADC5EA94A54CC00F349B1DF331079B13B16 (void);
// 0x0000040E LTDescr LeanTweenExt::LeanSize(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single)
extern void LeanTweenExt_LeanSize_m410361AD3B78FE2A69D65C21570490F82E660241 (void);
// 0x0000040F LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,UnityEngine.Color,UnityEngine.Color,System.Single)
extern void LeanTweenExt_LeanValue_m1FB1FFD9157FA40B5854744EA15111178AEDE1C1 (void);
// 0x00000410 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Single,System.Single,System.Single)
extern void LeanTweenExt_LeanValue_mE9099E0F64A010AD2BEA7030C5FEFEDBE78A1571 (void);
// 0x00000411 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void LeanTweenExt_LeanValue_mF09B8E5D72CBADA7E7335A1CCE8397B6BEE0B286 (void);
// 0x00000412 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanValue_m165CCF09A13FA398105E06E6B441D0C5BEF33E20 (void);
// 0x00000413 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Action`1<System.Single>,System.Single,System.Single,System.Single)
extern void LeanTweenExt_LeanValue_m2D0676A4B078FE10C1819ECF5520374CA2E39E8D (void);
// 0x00000414 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Action`2<System.Single,System.Single>,System.Single,System.Single,System.Single)
extern void LeanTweenExt_LeanValue_m626C6C95ABDA16428DAD1399B0862363589AC9F9 (void);
// 0x00000415 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Action`2<System.Single,System.Object>,System.Single,System.Single,System.Single)
extern void LeanTweenExt_LeanValue_mBB6A5387301F308B4C42B4DF5B70C17AE29F5533 (void);
// 0x00000416 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Action`1<UnityEngine.Color>,UnityEngine.Color,UnityEngine.Color,System.Single)
extern void LeanTweenExt_LeanValue_m93C212023658F9EF6A27683DEBAA852EE4670411 (void);
// 0x00000417 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Action`1<UnityEngine.Vector2>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void LeanTweenExt_LeanValue_m5B60256ABE5F8B7E28F389AAE1696EB656633398 (void);
// 0x00000418 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Action`1<UnityEngine.Vector3>,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanValue_mBA982D980197033944CF17D7C7F236A6CED885DF (void);
// 0x00000419 System.Void LeanTweenExt::LeanSetPosX(UnityEngine.Transform,System.Single)
extern void LeanTweenExt_LeanSetPosX_m82201BB5C9781FA41FB69DDA05471D693E59DF2E (void);
// 0x0000041A System.Void LeanTweenExt::LeanSetPosY(UnityEngine.Transform,System.Single)
extern void LeanTweenExt_LeanSetPosY_mD9DAE6B462055110382822D16B5551854CB68DD7 (void);
// 0x0000041B System.Void LeanTweenExt::LeanSetPosZ(UnityEngine.Transform,System.Single)
extern void LeanTweenExt_LeanSetPosZ_m1E17DB053DD69876E163757D053A85B172CAC027 (void);
// 0x0000041C System.Void LeanTweenExt::LeanSetLocalPosX(UnityEngine.Transform,System.Single)
extern void LeanTweenExt_LeanSetLocalPosX_m2325658730387BE391CB8FE674FD3FD2D8D3ECDE (void);
// 0x0000041D System.Void LeanTweenExt::LeanSetLocalPosY(UnityEngine.Transform,System.Single)
extern void LeanTweenExt_LeanSetLocalPosY_mF14BA20EFA312158AB0FD456EB8762457DB03AE5 (void);
// 0x0000041E System.Void LeanTweenExt::LeanSetLocalPosZ(UnityEngine.Transform,System.Single)
extern void LeanTweenExt_LeanSetLocalPosZ_m685180D2C04DF7E3417E9EECA9BEBE60F0850790 (void);
// 0x0000041F UnityEngine.Color LeanTweenExt::LeanColor(UnityEngine.Transform)
extern void LeanTweenExt_LeanColor_m2027EB778950A5E71F3DCF398F8A6A77AE32EF35 (void);
// 0x00000420 System.Void ImageRecognition::Awake()
extern void ImageRecognition_Awake_mE172637B23DD1B72F4CE31B40B7BF0CE56E642DB (void);
// 0x00000421 System.Void ImageRecognition::OnEnable()
extern void ImageRecognition_OnEnable_mB261FCAE7E314E1B9D537C60D8F075EA76FFC47C (void);
// 0x00000422 System.Void ImageRecognition::OnDisable()
extern void ImageRecognition_OnDisable_m2B722701BC3F3627F4A9ECBA138B31368CFCC0A9 (void);
// 0x00000423 System.Void ImageRecognition::OnChanged(UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs)
extern void ImageRecognition_OnChanged_mFB283597AECA8A146430A1A06321DE1A171C1DAE (void);
// 0x00000424 System.Void ImageRecognition::UpdateImage(UnityEngine.XR.ARFoundation.ARTrackedImage)
extern void ImageRecognition_UpdateImage_m26D7D72CEA73CBCAEA9FD74832029472A742D138 (void);
// 0x00000425 System.Void ImageRecognition::.ctor()
extern void ImageRecognition__ctor_mDDF9CAFE6A1FA239354591A0A6AED16DF84E5591 (void);
// 0x00000426 System.Void CamFacing::Start()
extern void CamFacing_Start_m14EB9F61DA6165AE57AD42E22D7D23398F919BD1 (void);
// 0x00000427 System.Void CamFacing::Update()
extern void CamFacing_Update_mDDEA5160B367B17DA264B87428D724324DAAA21E (void);
// 0x00000428 System.Void CamFacing::.ctor()
extern void CamFacing__ctor_mCA0D2B74599C0746D5051F64B0FD834119019A47 (void);
// 0x00000429 System.Void CoinCollector::OnEnable()
extern void CoinCollector_OnEnable_mC21EC7F6EDFCAF431F295ED2A876CFFD538DF9CF (void);
// 0x0000042A System.Void CoinCollector::OnDisable()
extern void CoinCollector_OnDisable_m133841F690E3F25FF11FE0264E37FC145773CF61 (void);
// 0x0000042B System.Void CoinCollector::OnCoinsCollected(System.Int32)
extern void CoinCollector_OnCoinsCollected_m3C9A78F020F8A84608681E3A39BBF39BE3D53D4E (void);
// 0x0000042C System.Void CoinCollector::.ctor()
extern void CoinCollector__ctor_m209CC6D1E9378D8878B0625FEB01ABCAF7A77BA0 (void);
// 0x0000042D System.Void CoinSpin::Start()
extern void CoinSpin_Start_m4775228F7A36BB1AB26C3D834ADFB5479EC4C262 (void);
// 0x0000042E System.Void CoinSpin::OnEnable()
extern void CoinSpin_OnEnable_mFFBFE5AFA1833307258741B7104C907FF7DF3D54 (void);
// 0x0000042F System.Void CoinSpin::OnDisable()
extern void CoinSpin_OnDisable_m3B6E36881E2E8A3CCCE705255FDA47F9F4FAF11A (void);
// 0x00000430 System.Void CoinSpin::OnCoinsCollected(System.Int32)
extern void CoinSpin_OnCoinsCollected_mAAC796BB25FC68AC76DACF9AF79610517D6EED45 (void);
// 0x00000431 System.Void CoinSpin::Update()
extern void CoinSpin_Update_m7B845F5D805C0747C71A7C99772BD27386AB4D32 (void);
// 0x00000432 System.Void CoinSpin::.ctor()
extern void CoinSpin__ctor_m05750F090B6509A6133F34F5DB3399DE04E27118 (void);
// 0x00000433 System.Void CoinSpin/<>c::.cctor()
extern void U3CU3Ec__cctor_m1443B5BFB16125461557F58BB5CF344FC106795C (void);
// 0x00000434 System.Void CoinSpin/<>c::.ctor()
extern void U3CU3Ec__ctor_m0A494CDF7EB83C504F6DEDDF7785A1E51FF37A90 (void);
// 0x00000435 System.Void CoinSpin/<>c::<OnCoinsCollected>b__4_0()
extern void U3CU3Ec_U3COnCoinsCollectedU3Eb__4_0_m981C64B43B1937A8AC0AEFD5F3E774708DD8D2C7 (void);
// 0x00000436 System.Void Elevate::Start()
extern void Elevate_Start_m2C3BCE670D42ECD29D05D2A8623C4A85AA40F4F7 (void);
// 0x00000437 System.Void Elevate::Update()
extern void Elevate_Update_m7901F7E643727AB5AA6ECE9B31D5811BBAB557AC (void);
// 0x00000438 System.Void Elevate::.ctor()
extern void Elevate__ctor_m12A3D4B2C533756054EF5CD7D1582BFAE28C87C8 (void);
// 0x00000439 System.Void Float::Start()
extern void Float_Start_mBBF047E2FB9FE4F6EBA52C211264418CA98D016F (void);
// 0x0000043A System.Void Float::Update()
extern void Float_Update_m7D3509C96925953822E1CB021F022E51B215A02B (void);
// 0x0000043B System.Void Float::.ctor()
extern void Float__ctor_mBB0E290A40E91A1DCEFA692420EA6462129EEB2F (void);
// 0x0000043C System.Void GameEvents::.ctor()
extern void GameEvents__ctor_mA46EC570904E1217B6ED97ED040A6A86BCF4BB5E (void);
// 0x0000043D System.Void GameEvents/OnARCameraPoseChangedDelegate::.ctor(System.Object,System.IntPtr)
extern void OnARCameraPoseChangedDelegate__ctor_mD7D74EC559637DC63A77139FE1B12E9D458C7DFC (void);
// 0x0000043E System.Void GameEvents/OnARCameraPoseChangedDelegate::Invoke(UnityEngine.Pose,UnityEngine.Pose)
extern void OnARCameraPoseChangedDelegate_Invoke_m54DA5C240672398B8518E7981EA7B3B1E7CB8819 (void);
// 0x0000043F System.IAsyncResult GameEvents/OnARCameraPoseChangedDelegate::BeginInvoke(UnityEngine.Pose,UnityEngine.Pose,System.AsyncCallback,System.Object)
extern void OnARCameraPoseChangedDelegate_BeginInvoke_mD5A219AD2791717D25C425350D3E3B738A31708C (void);
// 0x00000440 System.Void GameEvents/OnARCameraPoseChangedDelegate::EndInvoke(System.IAsyncResult)
extern void OnARCameraPoseChangedDelegate_EndInvoke_m6CAC0AE240A54794BA350E15C086A39F7D75AFB1 (void);
// 0x00000441 System.Void GameEvents/OnPageGameStatusChangedDelegate::.ctor(System.Object,System.IntPtr)
extern void OnPageGameStatusChangedDelegate__ctor_mC6D1BE56C668B3ACED8FE3755B42B9836E569DAE (void);
// 0x00000442 System.Void GameEvents/OnPageGameStatusChangedDelegate::Invoke(PageGame/PageStatus,PageGame)
extern void OnPageGameStatusChangedDelegate_Invoke_m8A82520C191BAE52D685BC15121BA93E2E2EFBFE (void);
// 0x00000443 System.IAsyncResult GameEvents/OnPageGameStatusChangedDelegate::BeginInvoke(PageGame/PageStatus,PageGame,System.AsyncCallback,System.Object)
extern void OnPageGameStatusChangedDelegate_BeginInvoke_m55A49F76F508A220146EE19140025F279E40B41A (void);
// 0x00000444 System.Void GameEvents/OnPageGameStatusChangedDelegate::EndInvoke(System.IAsyncResult)
extern void OnPageGameStatusChangedDelegate_EndInvoke_m4907F39B00241794F88E59CA8DB0957FC9D28C76 (void);
// 0x00000445 System.Void GameEvents/OnPageGameCompletedDelegate::.ctor(System.Object,System.IntPtr)
extern void OnPageGameCompletedDelegate__ctor_mF9509A74617EB7CA5ECEE91D4DDFADE30ED55F27 (void);
// 0x00000446 System.Void GameEvents/OnPageGameCompletedDelegate::Invoke(PageGame,System.Int32)
extern void OnPageGameCompletedDelegate_Invoke_m292ADB5DCB2BA7E6F3D80F22C6A0291628DE5705 (void);
// 0x00000447 System.IAsyncResult GameEvents/OnPageGameCompletedDelegate::BeginInvoke(PageGame,System.Int32,System.AsyncCallback,System.Object)
extern void OnPageGameCompletedDelegate_BeginInvoke_m1567599C61DB410CF7AFA8D134E1593528EDBEC3 (void);
// 0x00000448 System.Void GameEvents/OnPageGameCompletedDelegate::EndInvoke(System.IAsyncResult)
extern void OnPageGameCompletedDelegate_EndInvoke_m21D6ED8D450273E6A7133925B4837BCC9A3AD087 (void);
// 0x00000449 System.Void GameEvents/OnPageGameBlockedDelegate::.ctor(System.Object,System.IntPtr)
extern void OnPageGameBlockedDelegate__ctor_mC52A412077FAD65DC525C6DB4F4D4C0AF7EABC93 (void);
// 0x0000044A System.Void GameEvents/OnPageGameBlockedDelegate::Invoke(PageGame,System.String)
extern void OnPageGameBlockedDelegate_Invoke_mD714F908E78ADD72B406DC537A83E0512821C078 (void);
// 0x0000044B System.IAsyncResult GameEvents/OnPageGameBlockedDelegate::BeginInvoke(PageGame,System.String,System.AsyncCallback,System.Object)
extern void OnPageGameBlockedDelegate_BeginInvoke_m0B69E6EE0E4B7D7418A0730343EC3138E2285E0C (void);
// 0x0000044C System.Void GameEvents/OnPageGameBlockedDelegate::EndInvoke(System.IAsyncResult)
extern void OnPageGameBlockedDelegate_EndInvoke_mE4EB82596E84E5A858529F0A22DD3FB35F5590F1 (void);
// 0x0000044D System.Void GameEvents/OnPageGameLookAtRaycastDelegate::.ctor(System.Object,System.IntPtr)
extern void OnPageGameLookAtRaycastDelegate__ctor_m4E8EAAAD726154985CDBCC8B6B4651B12DFCB6A9 (void);
// 0x0000044E System.Void GameEvents/OnPageGameLookAtRaycastDelegate::Invoke(UnityEngine.Transform)
extern void OnPageGameLookAtRaycastDelegate_Invoke_m5202EF9E40E982E2DC52E14EF2038081050D5105 (void);
// 0x0000044F System.IAsyncResult GameEvents/OnPageGameLookAtRaycastDelegate::BeginInvoke(UnityEngine.Transform,System.AsyncCallback,System.Object)
extern void OnPageGameLookAtRaycastDelegate_BeginInvoke_m24C6FCD011A757A4D3294EE8400D2E2A81DB2B31 (void);
// 0x00000450 System.Void GameEvents/OnPageGameLookAtRaycastDelegate::EndInvoke(System.IAsyncResult)
extern void OnPageGameLookAtRaycastDelegate_EndInvoke_mEBEBFA7F474EFD910D1B4A04C2232E69B923147B (void);
// 0x00000451 System.Void GameEvents/OnPageGameTouchRaycastDelegate::.ctor(System.Object,System.IntPtr)
extern void OnPageGameTouchRaycastDelegate__ctor_mDE3D6A07333F96DB184316D578A3197434D1B03B (void);
// 0x00000452 System.Void GameEvents/OnPageGameTouchRaycastDelegate::Invoke(UnityEngine.Transform,UnityEngine.Vector3)
extern void OnPageGameTouchRaycastDelegate_Invoke_m97E2C9BAC94482E67D5AAF54F6E9D91CE08BC572 (void);
// 0x00000453 System.IAsyncResult GameEvents/OnPageGameTouchRaycastDelegate::BeginInvoke(UnityEngine.Transform,UnityEngine.Vector3,System.AsyncCallback,System.Object)
extern void OnPageGameTouchRaycastDelegate_BeginInvoke_m86CB14F2B404DF44CEACF7D25C1946D85894063D (void);
// 0x00000454 System.Void GameEvents/OnPageGameTouchRaycastDelegate::EndInvoke(System.IAsyncResult)
extern void OnPageGameTouchRaycastDelegate_EndInvoke_m5F8537CC860A8D01A7168E848A51ABEAC6223A02 (void);
// 0x00000455 System.Void GameEvents/OnSplashWorldEnterDelegate::.ctor(System.Object,System.IntPtr)
extern void OnSplashWorldEnterDelegate__ctor_m63FF101453D43A9C71BD65243B7D80BD332C86E7 (void);
// 0x00000456 System.Void GameEvents/OnSplashWorldEnterDelegate::Invoke(SplashWorld,PageGame)
extern void OnSplashWorldEnterDelegate_Invoke_m54A1C472FB41A97C1FE172D3B6F0535FB0C79253 (void);
// 0x00000457 System.IAsyncResult GameEvents/OnSplashWorldEnterDelegate::BeginInvoke(SplashWorld,PageGame,System.AsyncCallback,System.Object)
extern void OnSplashWorldEnterDelegate_BeginInvoke_m8C40327E560A3841D7C5DF4F38F7097031E6875A (void);
// 0x00000458 System.Void GameEvents/OnSplashWorldEnterDelegate::EndInvoke(System.IAsyncResult)
extern void OnSplashWorldEnterDelegate_EndInvoke_m0469135B1A23DA1339AD8EB99A7DF9A607165D65 (void);
// 0x00000459 System.Void GameEvents/OnSplashWorldExitDelegate::.ctor(System.Object,System.IntPtr)
extern void OnSplashWorldExitDelegate__ctor_mA58FED7B590BBD6311CC70906D31523786DD5E07 (void);
// 0x0000045A System.Void GameEvents/OnSplashWorldExitDelegate::Invoke(SplashWorld,PageGame)
extern void OnSplashWorldExitDelegate_Invoke_mA17FEFC4DFA1E44C75C1359C261D16F4D8D93C82 (void);
// 0x0000045B System.IAsyncResult GameEvents/OnSplashWorldExitDelegate::BeginInvoke(SplashWorld,PageGame,System.AsyncCallback,System.Object)
extern void OnSplashWorldExitDelegate_BeginInvoke_m16FEE0E59104AC8702EA4364CB055D3E39935007 (void);
// 0x0000045C System.Void GameEvents/OnSplashWorldExitDelegate::EndInvoke(System.IAsyncResult)
extern void OnSplashWorldExitDelegate_EndInvoke_m8D6C3D49BAEA5180A9812A618EC807DB6C69BB23 (void);
// 0x0000045D System.Void GameEvents/OnSplashWorldLookAtRaycastDelegate::.ctor(System.Object,System.IntPtr)
extern void OnSplashWorldLookAtRaycastDelegate__ctor_m324F366FF3FBA14D69EC749D5D9C33F8D31FF25F (void);
// 0x0000045E System.Void GameEvents/OnSplashWorldLookAtRaycastDelegate::Invoke(UnityEngine.Transform)
extern void OnSplashWorldLookAtRaycastDelegate_Invoke_m23DA083ED354545670F3E37F602183A8EB8D554C (void);
// 0x0000045F System.IAsyncResult GameEvents/OnSplashWorldLookAtRaycastDelegate::BeginInvoke(UnityEngine.Transform,System.AsyncCallback,System.Object)
extern void OnSplashWorldLookAtRaycastDelegate_BeginInvoke_mD1087C28732FEEB669C31975FFFDE518AAC8A5CC (void);
// 0x00000460 System.Void GameEvents/OnSplashWorldLookAtRaycastDelegate::EndInvoke(System.IAsyncResult)
extern void OnSplashWorldLookAtRaycastDelegate_EndInvoke_mED01CD57E18A01D0A92D80D00AAB947F6CD56972 (void);
// 0x00000461 System.Void GameEvents/OnSplashWorldTouchRaycastDelegate::.ctor(System.Object,System.IntPtr)
extern void OnSplashWorldTouchRaycastDelegate__ctor_m9D794529B53F13112BFAD233DBCB2D0C6EEB67F0 (void);
// 0x00000462 System.Void GameEvents/OnSplashWorldTouchRaycastDelegate::Invoke(UnityEngine.Transform,System.Boolean)
extern void OnSplashWorldTouchRaycastDelegate_Invoke_m902BAB09A737CF45244566632FF7F3861712F5A6 (void);
// 0x00000463 System.IAsyncResult GameEvents/OnSplashWorldTouchRaycastDelegate::BeginInvoke(UnityEngine.Transform,System.Boolean,System.AsyncCallback,System.Object)
extern void OnSplashWorldTouchRaycastDelegate_BeginInvoke_m9C8F92D5651C65C4DB6848BEBE1A8EC8FD0BD807 (void);
// 0x00000464 System.Void GameEvents/OnSplashWorldTouchRaycastDelegate::EndInvoke(System.IAsyncResult)
extern void OnSplashWorldTouchRaycastDelegate_EndInvoke_m04F6F70DCABD301C4AB4C2CC74CB6CE943CC9AA7 (void);
// 0x00000465 System.Void GameEvents/OnTapOnDelegate::.ctor(System.Object,System.IntPtr)
extern void OnTapOnDelegate__ctor_m4E74094DC85BE96A510EDB77BCA49A3A0C254C5D (void);
// 0x00000466 System.Void GameEvents/OnTapOnDelegate::Invoke(UnityEngine.Vector2,System.Int32)
extern void OnTapOnDelegate_Invoke_m0B6E47CF342B976FF15123B09C844764E730362F (void);
// 0x00000467 System.IAsyncResult GameEvents/OnTapOnDelegate::BeginInvoke(UnityEngine.Vector2,System.Int32,System.AsyncCallback,System.Object)
extern void OnTapOnDelegate_BeginInvoke_m51E569CA9206297E464B27CCECDDF3169987250B (void);
// 0x00000468 System.Void GameEvents/OnTapOnDelegate::EndInvoke(System.IAsyncResult)
extern void OnTapOnDelegate_EndInvoke_mF107157A47D6C33F9BF9EC3BCFF8516BE4951A5B (void);
// 0x00000469 System.Void GameEvents/OnTapMoveDelegate::.ctor(System.Object,System.IntPtr)
extern void OnTapMoveDelegate__ctor_mB23EFB5884121D3696EF96AC4C63D15C868BB21C (void);
// 0x0000046A System.Void GameEvents/OnTapMoveDelegate::Invoke(UnityEngine.Vector2,System.Int32,UnityEngine.Vector2,System.TimeSpan)
extern void OnTapMoveDelegate_Invoke_m82EAB5BC9B7953E64D5FD9A134E1C96854FBE053 (void);
// 0x0000046B System.IAsyncResult GameEvents/OnTapMoveDelegate::BeginInvoke(UnityEngine.Vector2,System.Int32,UnityEngine.Vector2,System.TimeSpan,System.AsyncCallback,System.Object)
extern void OnTapMoveDelegate_BeginInvoke_m6B71072069CE1E8E847C5774E48BB23092A3C1C6 (void);
// 0x0000046C System.Void GameEvents/OnTapMoveDelegate::EndInvoke(System.IAsyncResult)
extern void OnTapMoveDelegate_EndInvoke_mF87B674B7C31D97E8D4B97F30EE4ABAF702F94EF (void);
// 0x0000046D System.Void GameEvents/OnTapOffDelegate::.ctor(System.Object,System.IntPtr)
extern void OnTapOffDelegate__ctor_m8AE822D97CDC73ABB86EC6F5735AFF8E02A3B16F (void);
// 0x0000046E System.Void GameEvents/OnTapOffDelegate::Invoke(UnityEngine.Vector2,System.Int32,UnityEngine.Vector2,System.TimeSpan)
extern void OnTapOffDelegate_Invoke_m56638CD9FC5533926E7C5507AF6E30ABDE7B6D90 (void);
// 0x0000046F System.IAsyncResult GameEvents/OnTapOffDelegate::BeginInvoke(UnityEngine.Vector2,System.Int32,UnityEngine.Vector2,System.TimeSpan,System.AsyncCallback,System.Object)
extern void OnTapOffDelegate_BeginInvoke_mF7C6873C714D66B27139D805B834ED9C62AB1C7D (void);
// 0x00000470 System.Void GameEvents/OnTapOffDelegate::EndInvoke(System.IAsyncResult)
extern void OnTapOffDelegate_EndInvoke_mEF4560AB49588DAA834521F2914C6609F37436F5 (void);
// 0x00000471 System.Void GameEvents/OnFirstPageTrackedDelegate::.ctor(System.Object,System.IntPtr)
extern void OnFirstPageTrackedDelegate__ctor_mB75FDEC54830FB287C4A0EEB0D49B31D58C77AB0 (void);
// 0x00000472 System.Void GameEvents/OnFirstPageTrackedDelegate::Invoke(PageTracker,Idle)
extern void OnFirstPageTrackedDelegate_Invoke_m8C612429E7B45367EE79B426B3A51F6DFB909901 (void);
// 0x00000473 System.IAsyncResult GameEvents/OnFirstPageTrackedDelegate::BeginInvoke(PageTracker,Idle,System.AsyncCallback,System.Object)
extern void OnFirstPageTrackedDelegate_BeginInvoke_mB81EC53B8B5BFAF5623DE314AA82146FE974BBFD (void);
// 0x00000474 System.Void GameEvents/OnFirstPageTrackedDelegate::EndInvoke(System.IAsyncResult)
extern void OnFirstPageTrackedDelegate_EndInvoke_m6951D2FAEB889959B571436BA3A01DB990ABFC89 (void);
// 0x00000475 System.Void GameEvents/OnStarfishTappedDelegate::.ctor(System.Object,System.IntPtr)
extern void OnStarfishTappedDelegate__ctor_m87D899695FE02D720D73E864BE94CD32D54705D4 (void);
// 0x00000476 System.Void GameEvents/OnStarfishTappedDelegate::Invoke(StarfishTap)
extern void OnStarfishTappedDelegate_Invoke_m2CBB20F4BF0BE52B144302FCE1E612A9B36A0747 (void);
// 0x00000477 System.IAsyncResult GameEvents/OnStarfishTappedDelegate::BeginInvoke(StarfishTap,System.AsyncCallback,System.Object)
extern void OnStarfishTappedDelegate_BeginInvoke_mFD9D6A9A6F87B6BBE09B62C8A99F288265DD32E0 (void);
// 0x00000478 System.Void GameEvents/OnStarfishTappedDelegate::EndInvoke(System.IAsyncResult)
extern void OnStarfishTappedDelegate_EndInvoke_mF281E0ACD6914F63A69A5AADA68A28CD0B9D9A4F (void);
// 0x00000479 System.Void GameEvents/OnFlickableTappedDelegate::.ctor(System.Object,System.IntPtr)
extern void OnFlickableTappedDelegate__ctor_m78A64B884A92E45427B06445D5CB9AF2C1158C77 (void);
// 0x0000047A System.Void GameEvents/OnFlickableTappedDelegate::Invoke(SplashWorldTapThrow)
extern void OnFlickableTappedDelegate_Invoke_m2337934002592A0F31D526A8C61283DA427745DD (void);
// 0x0000047B System.IAsyncResult GameEvents/OnFlickableTappedDelegate::BeginInvoke(SplashWorldTapThrow,System.AsyncCallback,System.Object)
extern void OnFlickableTappedDelegate_BeginInvoke_mC493D4B521739B0230A91F031A6225CFDB0376E2 (void);
// 0x0000047C System.Void GameEvents/OnFlickableTappedDelegate::EndInvoke(System.IAsyncResult)
extern void OnFlickableTappedDelegate_EndInvoke_mA16AFB8C9E5EA5F9B5213FDAAEE89C7E57AB89FE (void);
// 0x0000047D System.Void GameEvents/OnFlickableDraggedDelegate::.ctor(System.Object,System.IntPtr)
extern void OnFlickableDraggedDelegate__ctor_mD6BF605C10B35F1950A0C65009F22814586F0005 (void);
// 0x0000047E System.Void GameEvents/OnFlickableDraggedDelegate::Invoke(SplashWorldTapThrow)
extern void OnFlickableDraggedDelegate_Invoke_m755C9E5C87FD48673496B485278ACFD7DCE22EC8 (void);
// 0x0000047F System.IAsyncResult GameEvents/OnFlickableDraggedDelegate::BeginInvoke(SplashWorldTapThrow,System.AsyncCallback,System.Object)
extern void OnFlickableDraggedDelegate_BeginInvoke_mA289DD1825A4A277458F56EFD5B274158AFC37AA (void);
// 0x00000480 System.Void GameEvents/OnFlickableDraggedDelegate::EndInvoke(System.IAsyncResult)
extern void OnFlickableDraggedDelegate_EndInvoke_m736D66982A4EBB21AA45DBBEAEECE12B5E75C930 (void);
// 0x00000481 System.Void GameEvents/OnFlickableThrownDelegate::.ctor(System.Object,System.IntPtr)
extern void OnFlickableThrownDelegate__ctor_m7F872563A119EB783080EF20A11445ED990E28B6 (void);
// 0x00000482 System.Void GameEvents/OnFlickableThrownDelegate::Invoke(SplashWorldTapThrow)
extern void OnFlickableThrownDelegate_Invoke_m377686AE2223CDAC297886D3E8AAD16678707EB9 (void);
// 0x00000483 System.IAsyncResult GameEvents/OnFlickableThrownDelegate::BeginInvoke(SplashWorldTapThrow,System.AsyncCallback,System.Object)
extern void OnFlickableThrownDelegate_BeginInvoke_mA549322810396DF0EF1986E5C197B3ED9A4686A5 (void);
// 0x00000484 System.Void GameEvents/OnFlickableThrownDelegate::EndInvoke(System.IAsyncResult)
extern void OnFlickableThrownDelegate_EndInvoke_m95BFC469F524C199CE32025B350DB934406B3774 (void);
// 0x00000485 System.Void GameEvents/OnFadeToBlackDelegate::.ctor(System.Object,System.IntPtr)
extern void OnFadeToBlackDelegate__ctor_m568D84467B3DA85258F1CD019B35A8575D565074 (void);
// 0x00000486 System.Void GameEvents/OnFadeToBlackDelegate::Invoke(System.Action,System.String)
extern void OnFadeToBlackDelegate_Invoke_m1DF87DE2454B68CEB51C885EFC35968A06FDDE9D (void);
// 0x00000487 System.IAsyncResult GameEvents/OnFadeToBlackDelegate::BeginInvoke(System.Action,System.String,System.AsyncCallback,System.Object)
extern void OnFadeToBlackDelegate_BeginInvoke_m5A04C197ACF0AC84D66236C046C1D23A7645AAF5 (void);
// 0x00000488 System.Void GameEvents/OnFadeToBlackDelegate::EndInvoke(System.IAsyncResult)
extern void OnFadeToBlackDelegate_EndInvoke_m55C74CAF046C9C576A46812A2D2D48E58AB726B9 (void);
// 0x00000489 System.Void GameEvents/OnCoinsCollectedDelegate::.ctor(System.Object,System.IntPtr)
extern void OnCoinsCollectedDelegate__ctor_m7B1543D4E912C7B7F56A05902E77AFBCC8DB97E9 (void);
// 0x0000048A System.Void GameEvents/OnCoinsCollectedDelegate::Invoke(System.Int32)
extern void OnCoinsCollectedDelegate_Invoke_m2B603892435DCA37B952DB3AE079A498979BB7BD (void);
// 0x0000048B System.IAsyncResult GameEvents/OnCoinsCollectedDelegate::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void OnCoinsCollectedDelegate_BeginInvoke_m05F93FD54891E0BCDF61252EEC9ECBDD639D5386 (void);
// 0x0000048C System.Void GameEvents/OnCoinsCollectedDelegate::EndInvoke(System.IAsyncResult)
extern void OnCoinsCollectedDelegate_EndInvoke_mE48D93B3AD098DA9A6E7A42B006009F8157A6492 (void);
// 0x0000048D System.Void GameEvents/OnTotalCoinsUpdatedDelegate::.ctor(System.Object,System.IntPtr)
extern void OnTotalCoinsUpdatedDelegate__ctor_m350CF5E17BF1715CC628D878A67C6B86B1296A62 (void);
// 0x0000048E System.Void GameEvents/OnTotalCoinsUpdatedDelegate::Invoke(System.Int32)
extern void OnTotalCoinsUpdatedDelegate_Invoke_mF68ACF8669149F71E43FC3BB8E63D1DB4C21499A (void);
// 0x0000048F System.IAsyncResult GameEvents/OnTotalCoinsUpdatedDelegate::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void OnTotalCoinsUpdatedDelegate_BeginInvoke_mB27D8C0BF706F1023C3267F89487D371342A514E (void);
// 0x00000490 System.Void GameEvents/OnTotalCoinsUpdatedDelegate::EndInvoke(System.IAsyncResult)
extern void OnTotalCoinsUpdatedDelegate_EndInvoke_m90EB63D65A1B2883EEC7D1B6B0662459EA05AF46 (void);
// 0x00000491 PageGame GameManager::get_CurrentPageGame()
extern void GameManager_get_CurrentPageGame_mBB7545C1F39191029C356CE8A0CF621C05A0466D (void);
// 0x00000492 System.Void GameManager::set_CurrentPageGame(PageGame)
extern void GameManager_set_CurrentPageGame_m9D471B21E93F777A181EC198961365863F981851 (void);
// 0x00000493 System.Void GameManager::Start()
extern void GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E (void);
// 0x00000494 System.Void GameManager::OnEnable()
extern void GameManager_OnEnable_mD5C3947351399CD9D94EAABE360192091328B2E2 (void);
// 0x00000495 System.Void GameManager::OnDisable()
extern void GameManager_OnDisable_m5A032AE11D2E2BD8808702576A5DF606E0828003 (void);
// 0x00000496 System.Void GameManager::Update()
extern void GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1 (void);
// 0x00000497 System.Void GameManager::OnPageGameStatusChanged(PageGame/PageStatus,PageGame)
extern void GameManager_OnPageGameStatusChanged_m23AF3B11627C8C1F3E46023100A5F7F33F4A7669 (void);
// 0x00000498 System.Void GameManager::InitCamera()
extern void GameManager_InitCamera_m59AC8612E323A872BBAF5774991B40969A774E4F (void);
// 0x00000499 System.Void GameManager::.ctor()
extern void GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D (void);
// 0x0000049A GameScale/ScaleStatus GameScale::get_CurrentStatus()
extern void GameScale_get_CurrentStatus_m38D0B805AB43526506AAFAF0BCFB3280942720AC (void);
// 0x0000049B System.Void GameScale::set_CurrentStatus(GameScale/ScaleStatus)
extern void GameScale_set_CurrentStatus_m15A0A482C1931D16C7AA8CAB0CE5A03E0C45B03D (void);
// 0x0000049C System.Void GameScale::Start()
extern void GameScale_Start_m8B23A2D484BCA918F863887B70B4EDA22293A634 (void);
// 0x0000049D System.Collections.IEnumerator GameScale::ScaleObject()
extern void GameScale_ScaleObject_m0585571AAE75600F571926D1F9F72348F82407BB (void);
// 0x0000049E System.Void GameScale::Shrink()
extern void GameScale_Shrink_mAB3753ABFE65694641E2844AFDF37EAEBF677D05 (void);
// 0x0000049F System.Void GameScale::Grow()
extern void GameScale_Grow_m8D0F488BDB5AC478A96DCFDE28876F520C0EFD0B (void);
// 0x000004A0 System.Void GameScale::.ctor()
extern void GameScale__ctor_m86ABD3B568565132CE2F9637FEA31A26602BDC66 (void);
// 0x000004A1 System.Void GameScale::<Shrink>b__12_0()
extern void GameScale_U3CShrinkU3Eb__12_0_m2F0F00081290A6FC6F7AC0290DE44009D3B01527 (void);
// 0x000004A2 System.Void GameScale::<Grow>b__13_0()
extern void GameScale_U3CGrowU3Eb__13_0_mD9600484A0A19E931BAE06A6BCB1EEF0A96AF277 (void);
// 0x000004A3 System.Void GameScale/<ScaleObject>d__11::.ctor(System.Int32)
extern void U3CScaleObjectU3Ed__11__ctor_m9F9912F1FE2061B80649C3E2D3EE03F0C81A2ED2 (void);
// 0x000004A4 System.Void GameScale/<ScaleObject>d__11::System.IDisposable.Dispose()
extern void U3CScaleObjectU3Ed__11_System_IDisposable_Dispose_m1F6F9E741217D4A31F59B0AB382F7EA581962124 (void);
// 0x000004A5 System.Boolean GameScale/<ScaleObject>d__11::MoveNext()
extern void U3CScaleObjectU3Ed__11_MoveNext_m4049CE4C30DE148292B5E29E2353D62BDD9AFC66 (void);
// 0x000004A6 System.Object GameScale/<ScaleObject>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CScaleObjectU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA18277648E8ACA4C90D1053F66BB3A45B40A6504 (void);
// 0x000004A7 System.Void GameScale/<ScaleObject>d__11::System.Collections.IEnumerator.Reset()
extern void U3CScaleObjectU3Ed__11_System_Collections_IEnumerator_Reset_m03133167C210B463F8914107D14565D5EF30F003 (void);
// 0x000004A8 System.Object GameScale/<ScaleObject>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CScaleObjectU3Ed__11_System_Collections_IEnumerator_get_Current_m3D9D148281A0AE0E2A411244F4636E11A8AC4541 (void);
// 0x000004A9 System.Void Idle::Start()
extern void Idle_Start_m1071EBB94553CF0B91C9AB1933D3EF218FB1154E (void);
// 0x000004AA System.Collections.IEnumerator Idle::WaitAndActivate(System.Single)
extern void Idle_WaitAndActivate_m70D559158D8C1EDF0B9B44BD9823D4611AB2290B (void);
// 0x000004AB System.Collections.IEnumerator Idle::WaitAndDeactivate(System.Single)
extern void Idle_WaitAndDeactivate_m59520F96D5799F5496ADC573057933A6555CD144 (void);
// 0x000004AC System.Void Idle::.ctor()
extern void Idle__ctor_mF407A49B8DF091C857F7A11B9D8095F40526D11D (void);
// 0x000004AD System.Void Idle/<WaitAndActivate>d__6::.ctor(System.Int32)
extern void U3CWaitAndActivateU3Ed__6__ctor_m372FCD69AC54F9B6F417E419D1CBE41AFEBAF14A (void);
// 0x000004AE System.Void Idle/<WaitAndActivate>d__6::System.IDisposable.Dispose()
extern void U3CWaitAndActivateU3Ed__6_System_IDisposable_Dispose_m0D3874C983C3F7471BE43376345B2AA14B702A36 (void);
// 0x000004AF System.Boolean Idle/<WaitAndActivate>d__6::MoveNext()
extern void U3CWaitAndActivateU3Ed__6_MoveNext_mD91A64656968EE7793A445E0479E98CD9DC137F8 (void);
// 0x000004B0 System.Object Idle/<WaitAndActivate>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitAndActivateU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m41C234EFF7D21326815858026BE32FE067EF619F (void);
// 0x000004B1 System.Void Idle/<WaitAndActivate>d__6::System.Collections.IEnumerator.Reset()
extern void U3CWaitAndActivateU3Ed__6_System_Collections_IEnumerator_Reset_m5C8CE0D8F95DDAB7A3EFB667513B448F26BCE043 (void);
// 0x000004B2 System.Object Idle/<WaitAndActivate>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CWaitAndActivateU3Ed__6_System_Collections_IEnumerator_get_Current_m1D86BB210D7EA8F9BE13DD49A67154BD11E2BFAB (void);
// 0x000004B3 System.Void Idle/<WaitAndDeactivate>d__7::.ctor(System.Int32)
extern void U3CWaitAndDeactivateU3Ed__7__ctor_m3557474DAA9C5DB0CD5BD4032344C0A53452B250 (void);
// 0x000004B4 System.Void Idle/<WaitAndDeactivate>d__7::System.IDisposable.Dispose()
extern void U3CWaitAndDeactivateU3Ed__7_System_IDisposable_Dispose_m9FEB66AAD863C5A40D5E5420E53BFB2E7586E4D8 (void);
// 0x000004B5 System.Boolean Idle/<WaitAndDeactivate>d__7::MoveNext()
extern void U3CWaitAndDeactivateU3Ed__7_MoveNext_m8E0193259ACC6560820A9DE2EE116E0344F92AC4 (void);
// 0x000004B6 System.Object Idle/<WaitAndDeactivate>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitAndDeactivateU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m554F125318195969EAA9504CBBDB3D77CE7CB917 (void);
// 0x000004B7 System.Void Idle/<WaitAndDeactivate>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWaitAndDeactivateU3Ed__7_System_Collections_IEnumerator_Reset_m8B28B60638773A42F14FBE776C0E896D4F2AE0C4 (void);
// 0x000004B8 System.Object Idle/<WaitAndDeactivate>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWaitAndDeactivateU3Ed__7_System_Collections_IEnumerator_get_Current_m4AA71565E06F69AFC1CE8BCED114941F756B919B (void);
// 0x000004B9 System.Void InputManager::Update()
extern void InputManager_Update_mE17FD2A03E0E1BE94DFE0B0AB4B5B9C5F3EA285B (void);
// 0x000004BA System.Void InputManager::.ctor()
extern void InputManager__ctor_mB533F16325A793C9274F6CA3804EBCE27AD700A7 (void);
// 0x000004BB System.Void MV::Awake()
extern void MV_Awake_m008F0D0ACC5C1115624FD084641E66843C1416D1 (void);
// 0x000004BC System.Void MV::Start()
extern void MV_Start_m86C4237712EC695DDCD1B3B46CB269C1D7633DFD (void);
// 0x000004BD System.Void MV::Update()
extern void MV_Update_m5BD715E81EC2902864B64E72CAA0C4092E0BE927 (void);
// 0x000004BE System.Void MV::.ctor()
extern void MV__ctor_mCDC5A8302C9A0106B442F234DEC75CB1E4515661 (void);
// 0x000004BF System.Void OrbTap::Start()
extern void OrbTap_Start_m6E0BCC7996A47CFE7E1D37337EE386B62620D04E (void);
// 0x000004C0 System.Void OrbTap::Update()
extern void OrbTap_Update_m0DFA86B6FFD270FA49D1BA05B0542749C685DCB2 (void);
// 0x000004C1 System.Void OrbTap::Tap()
extern void OrbTap_Tap_m9A92D23E3843F6B5CFBBDA7100661D3CBC4A8479 (void);
// 0x000004C2 System.Void OrbTap::.ctor()
extern void OrbTap__ctor_m741D0805658EB70CF00EAD07A70DE78AAFE435A8 (void);
// 0x000004C3 System.Void PageCompletion::Start()
extern void PageCompletion_Start_mDBDE60FA1FB2EA54089A30FE18E832F53C269376 (void);
// 0x000004C4 System.Void PageCompletion::Update()
extern void PageCompletion_Update_m9D41C20F891A6D1BE2819F36C2816AADD102F65E (void);
// 0x000004C5 System.Void PageCompletion::OnEnable()
extern void PageCompletion_OnEnable_mC7E0A6D8B018FC8EF0B5C3EF0E66B417A73FA8DE (void);
// 0x000004C6 System.Void PageCompletion::OnDisable()
extern void PageCompletion_OnDisable_m439C8CD84035C690C165EB9F3ECC4F6FBF9A442F (void);
// 0x000004C7 System.Void PageCompletion::OnPageGameTapRaycast(UnityEngine.Transform,UnityEngine.Vector3)
extern void PageCompletion_OnPageGameTapRaycast_m048127FA790A0AFB0BABEEF82753CED49267553E (void);
// 0x000004C8 System.Boolean PageCompletion::IsAllTapped()
extern void PageCompletion_IsAllTapped_mA814F495F6AE2F691C302D2E49E44E8F589A4AC5 (void);
// 0x000004C9 System.Void PageCompletion::PageComplete()
extern void PageCompletion_PageComplete_mC87F1E1F909464791F41B0B86AFD25D84C76170D (void);
// 0x000004CA System.Void PageCompletion::.ctor()
extern void PageCompletion__ctor_mCBAAEB51A750E50191A35D107BD3E4FEC863401A (void);
// 0x000004CB System.Boolean PageGame::get_HasSplashWorld()
extern void PageGame_get_HasSplashWorld_m1E2194DE66FC47CC228E7450DB856653F57C6629 (void);
// 0x000004CC PageGame/PageStatus PageGame::get_CurrentStatus()
extern void PageGame_get_CurrentStatus_m62216C85BE6E5B3889395214E48C988D10E4ADAC (void);
// 0x000004CD System.Void PageGame::set_CurrentStatus(PageGame/PageStatus)
extern void PageGame_set_CurrentStatus_m80C5F4ACC165A3A47F04F47DD7C04D97E914D5C5 (void);
// 0x000004CE System.Int32 PageGame::get_CompletionCount()
extern void PageGame_get_CompletionCount_m163C431B336779F854B28006C445D9ACFBE41E3C (void);
// 0x000004CF System.Void PageGame::set_CompletionCount(System.Int32)
extern void PageGame_set_CompletionCount_m773C823765A76AA176ACE8A5D7CC238B07797EA2 (void);
// 0x000004D0 System.Void PageGame::Awake()
extern void PageGame_Awake_m798006381EEF3029AA15ED0EFDCA8E1C83BF8C46 (void);
// 0x000004D1 System.Void PageGame::OnEnable()
extern void PageGame_OnEnable_m08CCF29AFE68ADA36CCB77E974AB453A4A3EAF65 (void);
// 0x000004D2 System.Void PageGame::OnDisable()
extern void PageGame_OnDisable_mE98487A04E575C55CD2DAF44397214C60674A88E (void);
// 0x000004D3 System.Void PageGame::StartListeningForTouch()
extern void PageGame_StartListeningForTouch_mFB45A14D729075D31515F786D29E7D475E1FDBE5 (void);
// 0x000004D4 System.Void PageGame::StopListeningForTouch()
extern void PageGame_StopListeningForTouch_m266F0504AC3678F0B0068504E8DB46F50D900C7C (void);
// 0x000004D5 System.Void PageGame::EnableImageTargetting(System.Boolean)
extern void PageGame_EnableImageTargetting_m83F6BE8E0989B3605BD9AE159B974D13669F76C7 (void);
// 0x000004D6 System.Void PageGame::OnPageGameTapRaycast(UnityEngine.Transform,UnityEngine.Vector3)
extern void PageGame_OnPageGameTapRaycast_m0E9BDDE3058EBDEAC517C0E63B8DAF3CA50C386E (void);
// 0x000004D7 System.Void PageGame::OnSplashWorldTapRaycast(UnityEngine.Transform,System.Boolean)
extern void PageGame_OnSplashWorldTapRaycast_m4CA9A20365943007423B08E62F022345DFEE9292 (void);
// 0x000004D8 System.Void PageGame::OnTargetFound()
extern void PageGame_OnTargetFound_mA8082170277B6AA4447CE3EB4518B36842091980 (void);
// 0x000004D9 System.Void PageGame::OnTargetLost()
extern void PageGame_OnTargetLost_mE2103835326A93F467DA5CB5DED0EB5C84D75FAE (void);
// 0x000004DA System.Void PageGame::SetStatus(PageGame/PageStatus)
extern void PageGame_SetStatus_m135727AF9BA54570F961B1B85C80703781F022DC (void);
// 0x000004DB System.Void PageGame::EnterSplashWorld()
extern void PageGame_EnterSplashWorld_m6CC9D52AA2548E613C72ECCBA13D190B9760957C (void);
// 0x000004DC System.Void PageGame::.ctor()
extern void PageGame__ctor_mE30B9FF15964608458BF17C75DBA8340B8880D48 (void);
// 0x000004DD System.Void PageManager::OnEnable()
extern void PageManager_OnEnable_mD97A62753725BD8315D744C838EE2073AE61B6F3 (void);
// 0x000004DE System.Void PageManager::OnDisable()
extern void PageManager_OnDisable_m7E562F9E5528ABE45AB74AE7F4E7C5E2CCDAF346 (void);
// 0x000004DF System.Void PageManager::OnPageGameStatusChanged(PageGame/PageStatus,PageGame)
extern void PageManager_OnPageGameStatusChanged_m37698A0D6310FD920DC2B8BB831F7B8FC38B0FAC (void);
// 0x000004E0 System.Void PageManager::PageTracked(PageTracker,Idle)
extern void PageManager_PageTracked_mE3DE47EA8D795515C3CECD1B74871971A819C397 (void);
// 0x000004E1 System.Void PageManager::.ctor()
extern void PageManager__ctor_m88AF16E94DF020C84F832C3E34ECCB4DDB4EBBCE (void);
// 0x000004E2 System.Void PageTracker::Awake()
extern void PageTracker_Awake_mFF8F16468AE8FFD253955764C38F7964222634FD (void);
// 0x000004E3 System.Void PageTracker::OnEnable()
extern void PageTracker_OnEnable_mC929997C7E675482A678736EFDD43D2E7BFF1D4B (void);
// 0x000004E4 System.Void PageTracker::OnImageTracked()
extern void PageTracker_OnImageTracked_mB6D9FB10F747598ABE2990AABFEC5C041B6189AA (void);
// 0x000004E5 System.Void PageTracker::OnDisable()
extern void PageTracker_OnDisable_m457F0C7263FBC6EC38EBB381688B3082551DC08B (void);
// 0x000004E6 System.Void PageTracker::.ctor()
extern void PageTracker__ctor_mFB006E94A796C6F6E506741CFA7084C7CB5A423E (void);
// 0x000004E7 System.Void Raycaster::OnEnable()
extern void Raycaster_OnEnable_m3821F1CC53335174A4DDD3C90A76497215332F93 (void);
// 0x000004E8 System.Void Raycaster::OnDisable()
extern void Raycaster_OnDisable_mC6762911CBA887F439EB0B0558488AC874A06A07 (void);
// 0x000004E9 System.Void Raycaster::Start()
extern void Raycaster_Start_mBE50D4A510FFF56FED5CFA84D12798E16A40FCEB (void);
// 0x000004EA System.Void Raycaster::OnTapOn(UnityEngine.Vector2,System.Int32)
extern void Raycaster_OnTapOn_m2386CAB3CCD74B3232CC6A89F75205FF31872D57 (void);
// 0x000004EB System.Void Raycaster::OnTapMove(UnityEngine.Vector2,System.Int32,UnityEngine.Vector2,System.TimeSpan)
extern void Raycaster_OnTapMove_m68E87D42B49AD820F82BA6A9D5A05C25A15BEABE (void);
// 0x000004EC System.Void Raycaster::OnTapOff(UnityEngine.Vector2,System.Int32,UnityEngine.Vector2,System.TimeSpan)
extern void Raycaster_OnTapOff_m4D7A90BB4FDA3E8994CA8E810668767D324D50BB (void);
// 0x000004ED System.Void Raycaster::RaycastToTap(UnityEngine.Vector2)
extern void Raycaster_RaycastToTap_m192F84A11C3E1088AD672BDDA3A47B1131295D44 (void);
// 0x000004EE System.Void Raycaster::RaycastToDrag(UnityEngine.Vector2)
extern void Raycaster_RaycastToDrag_m5EAA0CE8A99105FD2C8523E486F99162FFCC946E (void);
// 0x000004EF System.Void Raycaster::RaycastToThrowOrIdle(UnityEngine.Vector2)
extern void Raycaster_RaycastToThrowOrIdle_m4EE3F01C3CE43C9FD08FA4BB0F5D988DFFF66378 (void);
// 0x000004F0 UnityEngine.RaycastHit Raycaster::DoRaycast(UnityEngine.Vector2,UnityEngine.LayerMask)
extern void Raycaster_DoRaycast_m4C8E1C12AFC41527E1A718114C5B3AC14F4CDEBA (void);
// 0x000004F1 System.Void Raycaster::.ctor()
extern void Raycaster__ctor_mCE9420B0FF812611DA3918D527E761D5075F7404 (void);
// 0x000004F2 Spawn/ScaleStatus Spawn::get_CurrentStatus()
extern void Spawn_get_CurrentStatus_m5E2624F5D15120BAF25669DFC891BB10BF2FE067 (void);
// 0x000004F3 System.Void Spawn::set_CurrentStatus(Spawn/ScaleStatus)
extern void Spawn_set_CurrentStatus_mA3512D04052416AB2B5D7167D64D39DE8C41C084 (void);
// 0x000004F4 System.Void Spawn::Start()
extern void Spawn_Start_m99E70C46F85C6318276009783089BE4FEB2F3DCE (void);
// 0x000004F5 System.Collections.IEnumerator Spawn::ScaleObject()
extern void Spawn_ScaleObject_mA7A227A8BE23DEEF647D38A89FDC136C1CA68A82 (void);
// 0x000004F6 System.Void Spawn::Grow()
extern void Spawn_Grow_m425F2432DBBDF90D5AEA3A96CA32A59D17E84C00 (void);
// 0x000004F7 System.Void Spawn::.ctor()
extern void Spawn__ctor_m29DA62C57DCB06B5990FB843EE2F8533D4FCEC1F (void);
// 0x000004F8 System.Void Spawn::<Grow>b__12_0()
extern void Spawn_U3CGrowU3Eb__12_0_mCACEB31935EBC09019449E1370A1225EFE1140CD (void);
// 0x000004F9 System.Void Spawn/<ScaleObject>d__11::.ctor(System.Int32)
extern void U3CScaleObjectU3Ed__11__ctor_m8D9F6C2638A2271E6525C7972EDFC7CAB742B0DD (void);
// 0x000004FA System.Void Spawn/<ScaleObject>d__11::System.IDisposable.Dispose()
extern void U3CScaleObjectU3Ed__11_System_IDisposable_Dispose_m40B8BFE624A73F108E46FFFA98B2AE4D6787CB5E (void);
// 0x000004FB System.Boolean Spawn/<ScaleObject>d__11::MoveNext()
extern void U3CScaleObjectU3Ed__11_MoveNext_mC31883E5BCC98BCE6524A2E263A7F992DD5BAC0B (void);
// 0x000004FC System.Object Spawn/<ScaleObject>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CScaleObjectU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m30FB78064DC61798F0BFA19E5D2F12A85975F0F5 (void);
// 0x000004FD System.Void Spawn/<ScaleObject>d__11::System.Collections.IEnumerator.Reset()
extern void U3CScaleObjectU3Ed__11_System_Collections_IEnumerator_Reset_m10257A8A5A594206D1B8A584528BFC9A24615121 (void);
// 0x000004FE System.Object Spawn/<ScaleObject>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CScaleObjectU3Ed__11_System_Collections_IEnumerator_get_Current_m116291CCC3A2D4AAB33B5F5683271A7799F9B80D (void);
// 0x000004FF System.Void SplashWorld::Start()
extern void SplashWorld_Start_mA0C62D66AE9960C317B369D12DF11D70CC72EFE6 (void);
// 0x00000500 System.Void SplashWorld::OnEnable()
extern void SplashWorld_OnEnable_m7C1C68FCB62DCE7013BB1D8A9F7FDB69418497E4 (void);
// 0x00000501 System.Void SplashWorld::OnDisable()
extern void SplashWorld_OnDisable_mCF7872F8B7B4AD2527158554BC36BE3B4E8F4E4F (void);
// 0x00000502 System.Void SplashWorld::Update()
extern void SplashWorld_Update_m8D77D9516591560E332B49042E667B0863704541 (void);
// 0x00000503 System.Void SplashWorld::CameraRaycast()
extern void SplashWorld_CameraRaycast_m2077251F485F55107FD1EEFC65C605660CAFC5E0 (void);
// 0x00000504 System.Void SplashWorld::OnARCameraPoseChanged(UnityEngine.Pose,UnityEngine.Pose)
extern void SplashWorld_OnARCameraPoseChanged_m76AC3D829A0CA59D5DA83C1BC7D3D4A22C3A7B68 (void);
// 0x00000505 System.Void SplashWorld::OnTouchOn(UnityEngine.Vector2,System.Int32)
extern void SplashWorld_OnTouchOn_mEB0B32B58ADCC1DFD2F5F73C1541D56C1F10051A (void);
// 0x00000506 System.Void SplashWorld::PlayMusic()
extern void SplashWorld_PlayMusic_mFC384AF0FC751C403770C5424DB630A4C682777A (void);
// 0x00000507 System.Void SplashWorld::StopMusic()
extern void SplashWorld_StopMusic_m6B6F9FD1277A7CC2423CFF915E04C2F6C56E200F (void);
// 0x00000508 System.Void SplashWorld::PlaySFX(UnityEngine.AudioClip)
extern void SplashWorld_PlaySFX_m46D2ECCCBB18D8D112F727E90BF01313391F69E9 (void);
// 0x00000509 System.Void SplashWorld::.ctor()
extern void SplashWorld__ctor_mCD892468D492B38EAEFCC243FC058DF6211825C0 (void);
// 0x0000050A System.Void SplashWorldManager::OnEnable()
extern void SplashWorldManager_OnEnable_m2D71F9709788941DB115E3E909AECB41F561F3C0 (void);
// 0x0000050B System.Void SplashWorldManager::OnDisable()
extern void SplashWorldManager_OnDisable_mBB418D9C6DE0E34B389D762B22248AB6250E26CF (void);
// 0x0000050C System.Void SplashWorldManager::OnSplashWorldEnter(SplashWorld,PageGame)
extern void SplashWorldManager_OnSplashWorldEnter_m8229A96B068EEBBF59AF1E8B1BE4409965975761 (void);
// 0x0000050D System.Void SplashWorldManager::OnSplashWorldExit(SplashWorld,PageGame)
extern void SplashWorldManager_OnSplashWorldExit_m4016B5DBB3678A156A8440A511A18F6CFC79595E (void);
// 0x0000050E System.Void SplashWorldManager::ActivateSplashWorld()
extern void SplashWorldManager_ActivateSplashWorld_m7F0EC5F9BB33B28ACA48CEC85A3D3FCE882196C5 (void);
// 0x0000050F System.Void SplashWorldManager::DeactivateSplashWorld()
extern void SplashWorldManager_DeactivateSplashWorld_m2254E024226FCF558E305FA360D20DC2317F19D1 (void);
// 0x00000510 System.Void SplashWorldManager::.ctor()
extern void SplashWorldManager__ctor_m9ECB8D2E115C583D949609EBE587E8FF90116ECF (void);
// 0x00000511 System.Void SplashWorldTapThrow::Start()
extern void SplashWorldTapThrow_Start_m71AE50CD88E255AFE3ABAC648BCA18501CB3D710 (void);
// 0x00000512 System.Void SplashWorldTapThrow::FixedUpdate()
extern void SplashWorldTapThrow_FixedUpdate_m1BF3BCC236C4AD5FC08724867564CDD1FD3747A9 (void);
// 0x00000513 System.Void SplashWorldTapThrow::Tap()
extern void SplashWorldTapThrow_Tap_mFA80CC1A04761C4CE1BD33E3EE9DC15C7A8D2BE0 (void);
// 0x00000514 System.Void SplashWorldTapThrow::Drag(UnityEngine.Vector3,System.Boolean)
extern void SplashWorldTapThrow_Drag_mA3F4BB999FB95BD64E975A77BBAB61D6304895D9 (void);
// 0x00000515 System.Void SplashWorldTapThrow::Throw(UnityEngine.Vector3)
extern void SplashWorldTapThrow_Throw_mD9885C3914F04FB7FE4AC249AABCF50226424B58 (void);
// 0x00000516 System.Void SplashWorldTapThrow::StopThrow()
extern void SplashWorldTapThrow_StopThrow_m1C8B860D53D89E391CFAEA7F2996B04C54E53C03 (void);
// 0x00000517 System.Void SplashWorldTapThrow::DropToStartY()
extern void SplashWorldTapThrow_DropToStartY_m7957DD0A0E9F4E3258262AD2916D8023BF6E134D (void);
// 0x00000518 System.Void SplashWorldTapThrow::Disappear()
extern void SplashWorldTapThrow_Disappear_m6C60EBEC528C76520AFCD19FCF0E32DA3300DDF5 (void);
// 0x00000519 System.Void SplashWorldTapThrow::SnapBackToStart()
extern void SplashWorldTapThrow_SnapBackToStart_m1E65D46190B9E177D191B1B53361649A44005EEC (void);
// 0x0000051A System.Void SplashWorldTapThrow::OnCollisionEnter(UnityEngine.Collision)
extern void SplashWorldTapThrow_OnCollisionEnter_m471A08D6C895A814740F3ECA61D258EEA9E3BBC6 (void);
// 0x0000051B System.Void SplashWorldTapThrow::OnTriggerEnter(UnityEngine.Collider)
extern void SplashWorldTapThrow_OnTriggerEnter_m831AA983BDB87D15D2F5F49AB88E6B6069EA349C (void);
// 0x0000051C System.Void SplashWorldTapThrow::OnTriggerExit(UnityEngine.Collider)
extern void SplashWorldTapThrow_OnTriggerExit_mE9F03FF5D82476691B5785D4C28A485DCCDA8052 (void);
// 0x0000051D System.Void SplashWorldTapThrow::Release()
extern void SplashWorldTapThrow_Release_m21C11A6B9A97FB1DEF3C04040118646425F983EE (void);
// 0x0000051E System.Void SplashWorldTapThrow::.ctor()
extern void SplashWorldTapThrow__ctor_m5F4AA3076F4416710B09D21B1AE6271F635B9EE0 (void);
// 0x0000051F System.Void SplashWorldTapThrow::<Throw>b__30_0()
extern void SplashWorldTapThrow_U3CThrowU3Eb__30_0_m0D5CC01A9E3F3BBE5BBC74F21EFB1AA7F962E30A (void);
// 0x00000520 System.Void SplashWorldTapThrow::<Throw>b__30_1()
extern void SplashWorldTapThrow_U3CThrowU3Eb__30_1_m646A85613E11A7EC17CAFB9A2140FA9E49E07A67 (void);
// 0x00000521 System.Void StarfishTap::Start()
extern void StarfishTap_Start_mBBB0C1E2F1120724FA3374B47C5707DCC2AE26DE (void);
// 0x00000522 System.Void StarfishTap::Tap()
extern void StarfishTap_Tap_m913B3F3C11973AB8F25696007A6ADB3F6F07F61C (void);
// 0x00000523 System.Void StarfishTap::.ctor()
extern void StarfishTap__ctor_mA6A4C51C7A0E7EC404ACBCAFD56E6E8C7278605D (void);
// 0x00000524 System.Collections.IEnumerator StarfishTap::<Tap>g__WaitAndActivate|20_2(System.Single)
extern void StarfishTap_U3CTapU3Eg__WaitAndActivateU7C20_2_m45303C54140A66C6A1C3AA9FB164850D9B9E0605 (void);
// 0x00000525 System.Void StarfishTap/<>c::.cctor()
extern void U3CU3Ec__cctor_m7E7B343B256D846881EE6D9805A8CF9589F32557 (void);
// 0x00000526 System.Void StarfishTap/<>c::.ctor()
extern void U3CU3Ec__ctor_mFEBA80A164FB9BA80162B341FE2DC5B95723B1A7 (void);
// 0x00000527 System.Void StarfishTap/<>c::<Tap>b__20_0()
extern void U3CU3Ec_U3CTapU3Eb__20_0_m8EFDB804F91709071566BB6224B050A7E830DF9F (void);
// 0x00000528 System.Void StarfishTap/<>c::<Tap>b__20_1()
extern void U3CU3Ec_U3CTapU3Eb__20_1_m322A25DB3D5CC75BE8E6058378AB17FADA407EEE (void);
// 0x00000529 System.Void StarfishTap/<>c::<Tap>b__20_3()
extern void U3CU3Ec_U3CTapU3Eb__20_3_mDBE5CA2B9D6ECB2DD1F3146662098592F3748E96 (void);
// 0x0000052A System.Void StarfishTap/<<Tap>g__WaitAndActivate|20_2>d::.ctor(System.Int32)
extern void U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed__ctor_mC1FAC773B7C389314756A2BC1C0B935D629E5CB8 (void);
// 0x0000052B System.Void StarfishTap/<<Tap>g__WaitAndActivate|20_2>d::System.IDisposable.Dispose()
extern void U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_System_IDisposable_Dispose_mD1831FF6B8869F465B692C7BC3440334E3C277AE (void);
// 0x0000052C System.Boolean StarfishTap/<<Tap>g__WaitAndActivate|20_2>d::MoveNext()
extern void U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_MoveNext_m16445490866976247BDF43DD8CAAC133C1045C9F (void);
// 0x0000052D System.Object StarfishTap/<<Tap>g__WaitAndActivate|20_2>d::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9764191B0083E73AC10D2DD64B724AF7BFE30151 (void);
// 0x0000052E System.Void StarfishTap/<<Tap>g__WaitAndActivate|20_2>d::System.Collections.IEnumerator.Reset()
extern void U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_System_Collections_IEnumerator_Reset_m2A883B4CA0FA17D2368B73A8DD265F095C3662D5 (void);
// 0x0000052F System.Object StarfishTap/<<Tap>g__WaitAndActivate|20_2>d::System.Collections.IEnumerator.get_Current()
extern void U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_System_Collections_IEnumerator_get_Current_m15B9F318820B9184FA7C97E17B84E9AA142D375C (void);
// 0x00000530 System.Void ThrowSuccess::OnTriggerEnter(UnityEngine.Collider)
extern void ThrowSuccess_OnTriggerEnter_mB5E4FEB2DB739F204D37D31798572A64B9F3331B (void);
// 0x00000531 System.Void ThrowSuccess::Start()
extern void ThrowSuccess_Start_m542004C57C61C4E09741315FB3125187C52D27F4 (void);
// 0x00000532 System.Void ThrowSuccess::.ctor()
extern void ThrowSuccess__ctor_mC3B1E94A2889FBE7B2E87A233058CD7D4983AC41 (void);
// 0x00000533 System.Void TitleReveal::Start()
extern void TitleReveal_Start_mAB5858E71F305E5E27EDEBC34E71BEDB32189F63 (void);
// 0x00000534 System.Void TitleReveal::Update()
extern void TitleReveal_Update_m9A44B25E35C2D358E8F5C1C05442395CFCF3590E (void);
// 0x00000535 System.Void TitleReveal::.ctor()
extern void TitleReveal__ctor_m59A1A622E3451498874A8448B755BCE4B54F627E (void);
// 0x00000536 System.Void UIManager::Start()
extern void UIManager_Start_mAA4B371DC406146E84A9D8803B9C861939B2E04E (void);
// 0x00000537 System.Void UIManager::OnEnable()
extern void UIManager_OnEnable_mAFE10D24F19B379487455D635E4A3C65D4B64240 (void);
// 0x00000538 System.Void UIManager::OnDisable()
extern void UIManager_OnDisable_mBAED7246344396BA861ADE4603C663A9ECFBCDEF (void);
// 0x00000539 System.Void UIManager::OnPageGameStatusChanged(PageGame/PageStatus,PageGame)
extern void UIManager_OnPageGameStatusChanged_mDBCBA6610A305A26A73CA24FB72DA3CAF1B521FE (void);
// 0x0000053A System.Void UIManager::OnTotalCoinsUpdated(System.Int32)
extern void UIManager_OnTotalCoinsUpdated_m0ECA7EBBF06E503B0E27263FB847C8D735E2BDD8 (void);
// 0x0000053B System.Void UIManager::.ctor()
extern void UIManager__ctor_mDADE1D724D40AF63AE78D51FC1CF1FE4784B4D4B (void);
// 0x0000053C UIScale/ScaleStatus UIScale::get_CurrentStatus()
extern void UIScale_get_CurrentStatus_m59B74B309E7DEDD304B5975587C597227D35AAF7 (void);
// 0x0000053D System.Void UIScale::set_CurrentStatus(UIScale/ScaleStatus)
extern void UIScale_set_CurrentStatus_m3DDDB961B7587E70B838BEAFB6B78E2D9C577004 (void);
// 0x0000053E System.Void UIScale::Start()
extern void UIScale_Start_mB103E0582EADF2571F03AD1E95C8960531C2A678 (void);
// 0x0000053F System.Collections.IEnumerator UIScale::ScaleObject()
extern void UIScale_ScaleObject_m2D6ACA6D185145F4DC427EF2D42A49EAC233369D (void);
// 0x00000540 System.Void UIScale::Shrink()
extern void UIScale_Shrink_mD23357992FADEBB542F6D4769D5585664CA93248 (void);
// 0x00000541 System.Void UIScale::Grow()
extern void UIScale_Grow_mA576B57E720E08B0CAABD8C9BFE56A20700FF04B (void);
// 0x00000542 System.Void UIScale::.ctor()
extern void UIScale__ctor_mCAA6B6E247394903A335B751CBB1C6F3F4381586 (void);
// 0x00000543 System.Void UIScale::<Shrink>b__13_0()
extern void UIScale_U3CShrinkU3Eb__13_0_mFFDD536FF19492248C5AF5931C4DF93F7AB58411 (void);
// 0x00000544 System.Void UIScale::<Grow>b__14_0()
extern void UIScale_U3CGrowU3Eb__14_0_m3BB4510B628C633F140F8A7EE6AC08E08170E52A (void);
// 0x00000545 System.Void UIScale/<ScaleObject>d__12::.ctor(System.Int32)
extern void U3CScaleObjectU3Ed__12__ctor_mDC451B5F819E9A4AE8516332FB6EB086259AB187 (void);
// 0x00000546 System.Void UIScale/<ScaleObject>d__12::System.IDisposable.Dispose()
extern void U3CScaleObjectU3Ed__12_System_IDisposable_Dispose_m18311F276F727390C6BB8F45547A1055AD46B687 (void);
// 0x00000547 System.Boolean UIScale/<ScaleObject>d__12::MoveNext()
extern void U3CScaleObjectU3Ed__12_MoveNext_mFBEA76F7419EFADB3A3E3A9D747DD36A8A2FCDEC (void);
// 0x00000548 System.Object UIScale/<ScaleObject>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CScaleObjectU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m208AC32EA8F4EFFA396F863484497848182EE124 (void);
// 0x00000549 System.Void UIScale/<ScaleObject>d__12::System.Collections.IEnumerator.Reset()
extern void U3CScaleObjectU3Ed__12_System_Collections_IEnumerator_Reset_mDCE696778B7C746AF2B21B9EDBF0EEDD9FA7C644 (void);
// 0x0000054A System.Object UIScale/<ScaleObject>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CScaleObjectU3Ed__12_System_Collections_IEnumerator_get_Current_m919B949113F60B7761002096FB3E96CDF935EDD5 (void);
// 0x0000054B System.Void CanvasSlider::Awake()
extern void CanvasSlider_Awake_mE32BF63E2F695EA0E9F568D21719573561962111 (void);
// 0x0000054C System.Void CanvasSlider::Start()
extern void CanvasSlider_Start_mB39BE74CE1BB17577ED12744AF909F90370D4F33 (void);
// 0x0000054D System.Void CanvasSlider::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void CanvasSlider_OnPointerDown_m2943AB5A386A0162CCD4ACCA204CEC1B64A22394 (void);
// 0x0000054E System.Void CanvasSlider::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void CanvasSlider_OnPointerUp_mE4F3D66FAB7F10F07C5938722147858D9BD56F4A (void);
// 0x0000054F System.Void CanvasSlider::Update()
extern void CanvasSlider_Update_mE3B0594C72B26CEC4C0E6629ACF1460F3199F2D2 (void);
// 0x00000550 System.Void CanvasSlider::.ctor()
extern void CanvasSlider__ctor_m38912C1E7DADCC31320E462922DDC08088AF2058 (void);
// 0x00000551 System.Void OpenerVideo::Awake()
extern void OpenerVideo_Awake_mAEF909354080E50381A40E6664333E037AFB4445 (void);
// 0x00000552 System.Void OpenerVideo::Start()
extern void OpenerVideo_Start_mD1AEADB2B11A00400CDF66F1F0BB5B3F536E0FB9 (void);
// 0x00000553 System.Void OpenerVideo::Update()
extern void OpenerVideo_Update_m0B35BFB4C9BD78C4CCE64E47BD56DEF88F942081 (void);
// 0x00000554 System.Void OpenerVideo::.ctor()
extern void OpenerVideo__ctor_mBE80C503758B7A43ECF45B57D5726C90A8D3F50E (void);
// 0x00000555 System.Void PlayPause::Awake()
extern void PlayPause_Awake_mF0E0387D6976BAE582D136B853CA1825756E9362 (void);
// 0x00000556 System.Void PlayPause::Start()
extern void PlayPause_Start_m932E78DA3BCE735856E3610251BD41330A1961B0 (void);
// 0x00000557 System.Void PlayPause::Update()
extern void PlayPause_Update_m54C6CA681D9B2AEEFCE0F223A915558867C5A0D0 (void);
// 0x00000558 System.Void PlayPause::videoPlayPause()
extern void PlayPause_videoPlayPause_m4327377C3DC61DF4D6B76CD980310ECE42A9C312 (void);
// 0x00000559 System.Void PlayPause::.ctor()
extern void PlayPause__ctor_m6EE8B948F0668C505EAFABC70C3676575B2C0729 (void);
// 0x0000055A System.Void Readme::.ctor()
extern void Readme__ctor_mF465410C5B2E598F2685E82CFCE1F42186AFF448 (void);
// 0x0000055B System.Void Readme/Section::.ctor()
extern void Section__ctor_mBAD5262A353BC071C61B8DB462A3D4D5AB5C7C4E (void);
// 0x0000055C System.Void UnityTemplateProjects.SimpleCameraController::OnEnable()
extern void SimpleCameraController_OnEnable_m2BA1F31BEDE84695933E86CF731059D6FAC2111C (void);
// 0x0000055D UnityEngine.Vector3 UnityTemplateProjects.SimpleCameraController::GetInputTranslationDirection()
extern void SimpleCameraController_GetInputTranslationDirection_m924AB4CEA66ADC4F1C63EAC21B660E0495765F4A (void);
// 0x0000055E System.Void UnityTemplateProjects.SimpleCameraController::Update()
extern void SimpleCameraController_Update_m0E587F33074BE72C54BA835B2217B0E8EC94F24A (void);
// 0x0000055F System.Single UnityTemplateProjects.SimpleCameraController::GetBoostFactor()
extern void SimpleCameraController_GetBoostFactor_m42B667ADEACC6F9CF0E4B6549A501609A15716AE (void);
// 0x00000560 UnityEngine.Vector2 UnityTemplateProjects.SimpleCameraController::GetInputLookRotation()
extern void SimpleCameraController_GetInputLookRotation_m4F945850C9036FAF5C3436AA42A1C3CB918E1BB9 (void);
// 0x00000561 System.Boolean UnityTemplateProjects.SimpleCameraController::IsBoostPressed()
extern void SimpleCameraController_IsBoostPressed_m26120B18155EBB68597EA52F392AD1E59F9A71E0 (void);
// 0x00000562 System.Boolean UnityTemplateProjects.SimpleCameraController::IsEscapePressed()
extern void SimpleCameraController_IsEscapePressed_mC694179281D2F4B20E831995751C70A26806488D (void);
// 0x00000563 System.Boolean UnityTemplateProjects.SimpleCameraController::IsCameraRotationAllowed()
extern void SimpleCameraController_IsCameraRotationAllowed_mABB9E26C1CC5C5643B305685F046B4B8493C7AFA (void);
// 0x00000564 System.Boolean UnityTemplateProjects.SimpleCameraController::IsRightMouseButtonDown()
extern void SimpleCameraController_IsRightMouseButtonDown_m0DE4F75C0A0AE963F333712F226000E0AE2DC32A (void);
// 0x00000565 System.Boolean UnityTemplateProjects.SimpleCameraController::IsRightMouseButtonUp()
extern void SimpleCameraController_IsRightMouseButtonUp_mEDE93C76DE7120043B15F390865340D1C5165CE5 (void);
// 0x00000566 System.Void UnityTemplateProjects.SimpleCameraController::.ctor()
extern void SimpleCameraController__ctor_mC6AE2509DDF461856450EC7DE0058A1687AB5C87 (void);
// 0x00000567 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::SetFromTransform(UnityEngine.Transform)
extern void CameraState_SetFromTransform_mAF13C515CFB1085295C01A870D93375E98F16647 (void);
// 0x00000568 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::Translate(UnityEngine.Vector3)
extern void CameraState_Translate_mB8F7239BD9DB70190E59D47D75DD125AD9AF3A96 (void);
// 0x00000569 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::LerpTowards(UnityTemplateProjects.SimpleCameraController/CameraState,System.Single,System.Single)
extern void CameraState_LerpTowards_mF2D4B962A677B281ED2F539A2FFF8A693FB9A326 (void);
// 0x0000056A System.Void UnityTemplateProjects.SimpleCameraController/CameraState::UpdateTransform(UnityEngine.Transform)
extern void CameraState_UpdateTransform_mE653356FD34828D19ECB6793439A14C38F372410 (void);
// 0x0000056B System.Void UnityTemplateProjects.SimpleCameraController/CameraState::.ctor()
extern void CameraState__ctor_m9C5338CABE70B8C73F8A4A08C1AFA1B33417DE9D (void);
// 0x0000056C System.Void DentedPixel.LeanDummy::.ctor()
extern void LeanDummy__ctor_mD0BA3A71589D10708BAF2C24160EC0305D014DBE (void);
// 0x0000056D System.Void DentedPixel.LTExamples.PathBezier::OnEnable()
extern void PathBezier_OnEnable_m5E057D877378F11B064309894D1AE8BB524F4771 (void);
// 0x0000056E System.Void DentedPixel.LTExamples.PathBezier::Start()
extern void PathBezier_Start_m17B69DE04B368B76932DC991342B633A7F0A9334 (void);
// 0x0000056F System.Void DentedPixel.LTExamples.PathBezier::Update()
extern void PathBezier_Update_m3827BD355D4006FA740F2B9F1EDD8C426CB43A91 (void);
// 0x00000570 System.Void DentedPixel.LTExamples.PathBezier::OnDrawGizmos()
extern void PathBezier_OnDrawGizmos_mBC48C0867056FB925C45FF97187F8AB4C4D6F13C (void);
// 0x00000571 System.Void DentedPixel.LTExamples.PathBezier::.ctor()
extern void PathBezier__ctor_m64C0684464ED26D96A5AE82DEA44C6ADF0F2085A (void);
// 0x00000572 System.Void DentedPixel.LTExamples.TestingUnitTests::Awake()
extern void TestingUnitTests_Awake_m93E88F38DA88C64CC12ABAE80900B948E5BC30E1 (void);
// 0x00000573 System.Void DentedPixel.LTExamples.TestingUnitTests::Start()
extern void TestingUnitTests_Start_m8C9EF37C5BC5461665D44564D0FE7FB3A6C69A66 (void);
// 0x00000574 UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cubeNamed(System.String)
extern void TestingUnitTests_cubeNamed_m3948700DD2B6420F25DFC461AB2A2A5357E792D6 (void);
// 0x00000575 System.Collections.IEnumerator DentedPixel.LTExamples.TestingUnitTests::timeBasedTesting()
extern void TestingUnitTests_timeBasedTesting_mBD8B52CEA7286C9F4FAAA419C773B78255E55F4D (void);
// 0x00000576 System.Collections.IEnumerator DentedPixel.LTExamples.TestingUnitTests::lotsOfCancels()
extern void TestingUnitTests_lotsOfCancels_mD21D0D0D0B8BE316E58C412A19ADE6658DAA5134 (void);
// 0x00000577 System.Collections.IEnumerator DentedPixel.LTExamples.TestingUnitTests::pauseTimeNow()
extern void TestingUnitTests_pauseTimeNow_m3293FFC3DDEC01D507093F2D2B8E34B40FC0DD7F (void);
// 0x00000578 System.Void DentedPixel.LTExamples.TestingUnitTests::rotateRepeatFinished()
extern void TestingUnitTests_rotateRepeatFinished_m3ED0D0A9DF982711F7D8D670535F1136CB7E3BA0 (void);
// 0x00000579 System.Void DentedPixel.LTExamples.TestingUnitTests::rotateRepeatAllFinished()
extern void TestingUnitTests_rotateRepeatAllFinished_mA343A62B2E6F1D84DAD61BF7DAD6DDC4F24A5077 (void);
// 0x0000057A System.Void DentedPixel.LTExamples.TestingUnitTests::eventGameObjectCalled(LTEvent)
extern void TestingUnitTests_eventGameObjectCalled_mAFA0C3AF3614A013DC2BB8B1F82E6FB121FE56AF (void);
// 0x0000057B System.Void DentedPixel.LTExamples.TestingUnitTests::eventGeneralCalled(LTEvent)
extern void TestingUnitTests_eventGeneralCalled_m9054A75A4B6E1A3BE3E393F88168FAEA8B34D65E (void);
// 0x0000057C System.Void DentedPixel.LTExamples.TestingUnitTests::.ctor()
extern void TestingUnitTests__ctor_m7108B4718BC8D59EC59A4EF627657842243EBB27 (void);
// 0x0000057D System.Void DentedPixel.LTExamples.TestingUnitTests::<lotsOfCancels>b__25_0()
extern void TestingUnitTests_U3ClotsOfCancelsU3Eb__25_0_m79612DDE2FFAD2A0986AA0D91A768BA96269D4F6 (void);
// 0x0000057E System.Void DentedPixel.LTExamples.TestingUnitTests::<pauseTimeNow>b__26_1()
extern void TestingUnitTests_U3CpauseTimeNowU3Eb__26_1_mB0C630E660D38F819734F11E2998CDE43590D503 (void);
// 0x0000057F System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_m85E81FE7FB95A8484DB589E78AB8F1B684592DDF (void);
// 0x00000580 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__0()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__0_m0E29967BE8C8986E397C440673E2793E2569089B (void);
// 0x00000581 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__1()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__1_m887D60815BE91C503B1B557775A0633F1B60E86E (void);
// 0x00000582 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__21()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__21_mD56FCF9AA4FAE66DF497E1779D9334E757A222E5 (void);
// 0x00000583 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__2()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__2_m25A626FC5B5176333408538B200230F7A012FCF1 (void);
// 0x00000584 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__4()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__4_mC5E35367A4E3D78731DCB8D56BC172E263F40648 (void);
// 0x00000585 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__5()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__5_m31888B2277D8EEE835811D9FF81FA68312AA086B (void);
// 0x00000586 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__6()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__6_mE7251F18767B392C7011C9893101E7498D8B0C29 (void);
// 0x00000587 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__8()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__8_mCAE5BA94A4F65FFE4C9F531C9AA0249D6F7B9218 (void);
// 0x00000588 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__9()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__9_m4AA88BFFB46D1E0E6212B98A76E7A0F6AA5C0039 (void);
// 0x00000589 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__10()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__10_m561C045E5C1DC104420555CF0BA62F1DF6B671E8 (void);
// 0x0000058A System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__11()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__11_m81593E9FC9183EC0E37356C733A02C3266B87796 (void);
// 0x0000058B System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__13(System.Object)
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__13_mF54BD21F02ED31763F697C67DB121D4646CC7522 (void);
// 0x0000058C System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__14()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__14_m5A91E5886E31BA74327DDC19A47BB2E14DB7E810 (void);
// 0x0000058D System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__15()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__15_mF1644C8D1D721BD4651678F2F71ACCA470CA60D9 (void);
// 0x0000058E System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__16()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__16_m347E0C3ED3E8390844A251CE7CDAE009166D0358 (void);
// 0x0000058F System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__17()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__17_m5404D4E64F471E4BB3E95B2DBF8C78AEDCD6C4FC (void);
// 0x00000590 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__19(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__19_mFCD1C9E7E22F2F6710CB151C14E7C6094B6F0EF2 (void);
// 0x00000591 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__20()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__20_m07D0BA90D12217971B1F53FF20AA9B02391A4877 (void);
// 0x00000592 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_1::.ctor()
extern void U3CU3Ec__DisplayClass22_1__ctor_m42740028F7DBAC0910C2B0D8FE48AE424104B01F (void);
// 0x00000593 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_1::<Start>b__23()
extern void U3CU3Ec__DisplayClass22_1_U3CStartU3Eb__23_m71B69B2DE70762D5765B748E1B062290289D9195 (void);
// 0x00000594 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_2::.ctor()
extern void U3CU3Ec__DisplayClass22_2__ctor_m92BFBC8093B249E40CB64FABE4D548B594B0A5A0 (void);
// 0x00000595 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_2::<Start>b__24(System.Object)
extern void U3CU3Ec__DisplayClass22_2_U3CStartU3Eb__24_mF27E3CE748DB0EEE7037C4DD812DD75BAE86B46A (void);
// 0x00000596 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c::.cctor()
extern void U3CU3Ec__cctor_m6F153F2D37E38349116C0879DD2F597DEC855ADD (void);
// 0x00000597 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c::.ctor()
extern void U3CU3Ec__ctor_mF4403EE165173DC522103616E6645018625DE513 (void);
// 0x00000598 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c::<Start>b__22_3()
extern void U3CU3Ec_U3CStartU3Eb__22_3_mB3B2B3F460A87856365CD811630D58EA7A704357 (void);
// 0x00000599 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c::<Start>b__22_22()
extern void U3CU3Ec_U3CStartU3Eb__22_22_mCFEF8C7D30A25EAB5E114C737BF1D2A30D599CA9 (void);
// 0x0000059A System.Void DentedPixel.LTExamples.TestingUnitTests/<>c::<Start>b__22_7()
extern void U3CU3Ec_U3CStartU3Eb__22_7_mAC888F5211B48BEDE3B76735AF1835BC125992CE (void);
// 0x0000059B System.Void DentedPixel.LTExamples.TestingUnitTests/<>c::<Start>b__22_12(System.Single)
extern void U3CU3Ec_U3CStartU3Eb__22_12_m56B9B268A61C3FB036CCE2D844B45C27C0B201AD (void);
// 0x0000059C System.Void DentedPixel.LTExamples.TestingUnitTests/<>c::<Start>b__22_18()
extern void U3CU3Ec_U3CStartU3Eb__22_18_m210C935C0307C41651403480E5EF0856246C8C3D (void);
// 0x0000059D System.Void DentedPixel.LTExamples.TestingUnitTests/<>c::<pauseTimeNow>b__26_0()
extern void U3CU3Ec_U3CpauseTimeNowU3Eb__26_0_mE763C76C7D6AEEAAB8A0C84892C1D42F508144EB (void);
// 0x0000059E System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_m8818C4F306B80B7FEE062793CBEBEAB352E53F22 (void);
// 0x0000059F System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__0()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__0_mB82E5FEE09CC712C778F6A007B14D3D455256ECB (void);
// 0x000005A0 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__1()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__1_m8D5BED91D3EE4A8C0C0DF77D2B62F903C3E20D0C (void);
// 0x000005A1 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__2(System.Single)
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__2_mB677486A14C58435757062820302D43BCC02D144 (void);
// 0x000005A2 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__3()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__3_m3987A57ECD0F50AB39FEF707C42D9710AA3CFB2D (void);
// 0x000005A3 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__4()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__4_m87D8E7758DA9C355EE513F9775D9F4B4C387BB56 (void);
// 0x000005A4 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__5()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__5_m2B18F0F9B914A675CEEA356DCFF478B4F5611D8F (void);
// 0x000005A5 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__6(System.Single)
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__6_mE7EA3413E1F81C42AAFE03C96A7C6C430EAFAF81 (void);
// 0x000005A6 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__7()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__7_mF6A7BD6551F5A9F146C1B47E86AFF650A2B0005D (void);
// 0x000005A7 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__13()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__13_mFFA8F8E7E5F79A608E1C2216AA8D1B734A758093 (void);
// 0x000005A8 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__14(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__14_m0D4E422B37F43F7D3ACECDE81ADD5A18D4C97EE8 (void);
// 0x000005A9 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__15(System.Object)
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__15_mE41B0CD588F01B0A76360EBD332CFAF425E20931 (void);
// 0x000005AA System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__16()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__16_mEA9C490B5AEA717E999FFF3DD2CB25B23966277E (void);
// 0x000005AB System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__8()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__8_m63E7CF9513E4624B5B101B1C46941DC34982AC33 (void);
// 0x000005AC System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__9(System.Single)
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__9_m7B1419838E647715BC8200AD6D56DAF1A8F04F81 (void);
// 0x000005AD System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__10()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__10_m91860D812373862DCCD33D1D479A52867ED3CC02 (void);
// 0x000005AE System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__11(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__11_m9D776D70F7B40CB7DB069BE5D095C281D59F2713 (void);
// 0x000005AF System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__12()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__12_mB40AAAC5766C52276E4F9A375DB379DBE62012FA (void);
// 0x000005B0 System.Void DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::.ctor(System.Int32)
extern void U3CtimeBasedTestingU3Ed__24__ctor_mBB473D0F9FCF74C17EE67A02377DBAC113B0ECE6 (void);
// 0x000005B1 System.Void DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::System.IDisposable.Dispose()
extern void U3CtimeBasedTestingU3Ed__24_System_IDisposable_Dispose_m2240ABE280E91D3110F4956F4DDAE8A6F211340A (void);
// 0x000005B2 System.Boolean DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::MoveNext()
extern void U3CtimeBasedTestingU3Ed__24_MoveNext_mF87066714174A943FE18B6A562B291DFA766B72E (void);
// 0x000005B3 System.Object DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CtimeBasedTestingU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mACB1A3175D122506B71BCDB9EBF4AA9F0383E74B (void);
// 0x000005B4 System.Void DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::System.Collections.IEnumerator.Reset()
extern void U3CtimeBasedTestingU3Ed__24_System_Collections_IEnumerator_Reset_m96D9D8DFA5B24A08E71FA9EA4595288F06E9837B (void);
// 0x000005B5 System.Object DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CtimeBasedTestingU3Ed__24_System_Collections_IEnumerator_get_Current_mA059C8602E0A9BCCC1849108A55B19E09E4818CA (void);
// 0x000005B6 System.Void DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::.ctor(System.Int32)
extern void U3ClotsOfCancelsU3Ed__25__ctor_m9B88818966D8A321B3F23FB53C461C28A354F1C0 (void);
// 0x000005B7 System.Void DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::System.IDisposable.Dispose()
extern void U3ClotsOfCancelsU3Ed__25_System_IDisposable_Dispose_m40E5F2D2626AB1E624CA39E3740CDFBE5B4F1B30 (void);
// 0x000005B8 System.Boolean DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::MoveNext()
extern void U3ClotsOfCancelsU3Ed__25_MoveNext_m9AA75163A6CE98F58622048E0971E438D1E2AFEE (void);
// 0x000005B9 System.Object DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3ClotsOfCancelsU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2F111DF2BFDD6B3974DFA490B271DC9FCCBFC1DB (void);
// 0x000005BA System.Void DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::System.Collections.IEnumerator.Reset()
extern void U3ClotsOfCancelsU3Ed__25_System_Collections_IEnumerator_Reset_m440F860762B28408E3B9AB6D72144B5E6A1C6F8C (void);
// 0x000005BB System.Object DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::System.Collections.IEnumerator.get_Current()
extern void U3ClotsOfCancelsU3Ed__25_System_Collections_IEnumerator_get_Current_mA7D1CE85A7D50D60D73C5914F5EC60B5E33A344E (void);
// 0x000005BC System.Void DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::.ctor(System.Int32)
extern void U3CpauseTimeNowU3Ed__26__ctor_mD1AE96EC04B7EB3C54830BBCA910ACEC6A857D84 (void);
// 0x000005BD System.Void DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::System.IDisposable.Dispose()
extern void U3CpauseTimeNowU3Ed__26_System_IDisposable_Dispose_mE8F0437ED1B062C967A1FFCA460708BD42BC5142 (void);
// 0x000005BE System.Boolean DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::MoveNext()
extern void U3CpauseTimeNowU3Ed__26_MoveNext_m0750DC2D5E587C420188232EDCAD54FEE7ABF659 (void);
// 0x000005BF System.Object DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CpauseTimeNowU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD12A76EAE3CD0B1B9C938030B3E4D2FAA6456011 (void);
// 0x000005C0 System.Void DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::System.Collections.IEnumerator.Reset()
extern void U3CpauseTimeNowU3Ed__26_System_Collections_IEnumerator_Reset_m88957DE3F231D3C4F8E64C6DADA45876FABB7FD0 (void);
// 0x000005C1 System.Object DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CpauseTimeNowU3Ed__26_System_Collections_IEnumerator_get_Current_m04EA51DB175A4043B51D9B3CFDD410D924B422A0 (void);
static Il2CppMethodPointer s_methodPointers[1473] = 
{
	OldGUIExamplesCS_Start_mC33821BD3ADFD74B0B1BDD411FE35F29173AAE16,
	OldGUIExamplesCS_catMoved_m0AFE2D767EEFFE2B23F700545EDCADBA4FDE4495,
	OldGUIExamplesCS_OnGUI_mFA5EA2B56824C4712729521C4545139AB167C4AA,
	OldGUIExamplesCS__ctor_m805206E5EF03EF6452AC2A3979FD378F1CB86998,
	TestingPunch_Start_m51A034719AB8587292861C585C70970EDEBE8877,
	TestingPunch_Update_m953417D5B76254AF9BF4E2BA996801A997F9D6D6,
	TestingPunch_tweenStatically_m0E13525CEE807E5FA77B94242B182868B1D129A0,
	TestingPunch_enterMiniGameStart_m017AD4178C7F47FB535AFCA0B39AB97A58F0D9BE,
	TestingPunch_updateColor_mB0688DDA58E8C0CBA76525C4B31BD18EB3A2E4FC,
	TestingPunch_delayedMethod_mC7F5E34BF17C44BC3B311081B8ED1897F1CC08C3,
	TestingPunch_destroyOnComp_mB95F605847D2D64B1F4BF239600FAF788917BF44,
	TestingPunch_curveToString_mA840114D184FEA8169EE6B379C4513E282E09511,
	TestingPunch__ctor_m8AA41432E6E7079F6B683958FECD329615A21169,
	TestingPunch_U3CUpdateU3Eb__4_0_m573E0207272AF5B03F77EBE674FC9B7C0B49791D,
	TestingPunch_U3CUpdateU3Eb__4_3_m4CC34D2970098316C32EBE67B368AB199A20A2E1,
	TestingPunch_U3CUpdateU3Eb__4_7_m2E2AD49C20FF476ED389803BE00C0C87495D0DFC,
	U3CU3Ec__DisplayClass4_0__ctor_m6EF45FEFD275B14E2DFC1358F9AF89B57EF810AC,
	U3CU3Ec__DisplayClass4_0_U3CUpdateU3Eb__2_m59E4E1443D5262BE2A23D2C28AB670EA4071ABF4,
	U3CU3Ec__DisplayClass4_1__ctor_mB4665A6E7864E352E8943BE5EFEDF0E53D1685AC,
	U3CU3Ec__DisplayClass4_1_U3CUpdateU3Eb__6_m954F24C94FA12A5815D1FED30C2506F75E8628F2,
	U3CU3Ec__DisplayClass4_2__ctor_m1584CFD1510446CA5FCCF444CC4C3A8416419C58,
	U3CU3Ec__DisplayClass4_2_U3CUpdateU3Eb__8_m06A77409E2EEAED1E858D86BDDD2CC58DE2CE83A,
	U3CU3Ec__cctor_m66A08467936DA18B932008F1E6DC72EEB39F16A1,
	U3CU3Ec__ctor_mF00B82FD7B30077FBDE19AD680A208A4C3BA70F9,
	U3CU3Ec_U3CUpdateU3Eb__4_1_mC4766FA978E4EE87A4519997A0A18B9FF21F128A,
	U3CU3Ec_U3CUpdateU3Eb__4_4_mB29A35054C7AD0CC10D5E31667EDF5249A05D507,
	U3CU3Ec_U3CUpdateU3Eb__4_5_mDC2E23D6DCE6159A01A4E5965CA5654E521A6203,
	U3CU3Ec_U3CtweenStaticallyU3Eb__5_0_m412A7565AC15AED058D5BD67A3BFF97E6450F355,
	TestingRigidbodyCS_Start_m995B801673B71100B5483E039BFCF522BA624548,
	TestingRigidbodyCS_Update_m8DD8457212175A81186F50921B999350FA9DEC0F,
	TestingRigidbodyCS__ctor_mDE99A4FEAFB79D731E586C9AF3656849987B741B,
	Following_Start_m5CD86C24B70F949375792EFAEF58F657F06DBA18,
	Following_Update_mEB56FD4D3CE1029AB038A8DA8CE6C6704A9541DC,
	Following_moveArrow_mF5878CA6A071D36230DD6FA4209FFA765BAC640F,
	Following__ctor_mA110CFAE2A03042B9F1FE53338D377A005CBAB13,
	GeneralAdvancedTechniques_Start_m132CD028BB491EE7C59B5F5F1D1AB47EB7DAE306,
	GeneralAdvancedTechniques__ctor_mB828FC801FA5649B544E8FF31A6E1FF8916563D8,
	GeneralAdvancedTechniques_U3CStartU3Eb__10_0_mBBEFB6358D20EC1EFC4D6B51139D0DEEDFF86AD2,
	GeneralBasic_Start_m159ABB4F96835B58FEB058E24A576462A119E871,
	GeneralBasic_advancedExamples_mB7CE50C2F062F66FD7D0E1BD403DFDACE1D099AF,
	GeneralBasic__ctor_mF2DBA4586E298C9F216C4680C14837E6B5E095AD,
	GeneralBasic_U3CadvancedExamplesU3Eb__2_0_m706BD8DE22C36E9A7EF6326EF1B7EAB6C780BF3D,
	U3CU3Ec__DisplayClass2_0__ctor_mAE48CD4D687E71BADBB3C871A2194F0803D1EDC0,
	U3CU3Ec__DisplayClass2_0_U3CadvancedExamplesU3Eb__1_m9425EEE45EC19E72004ADE4D0B203F5448C21B27,
	U3CU3Ec__DisplayClass2_0_U3CadvancedExamplesU3Eb__2_mA4BA962A162AB1425F97630A3FB0FD395D77444C,
	GeneralBasics2d_Start_mD2ED7FA046D4AD2C347896AFF54984E3BDC73876,
	GeneralBasics2d_createSpriteDude_m5D7A1E41EEAADFE587F9AB375AA4C3B7617DF7B3,
	GeneralBasics2d_advancedExamples_m7A48E45BB9836372021B76A523A2594A1E66F1EA,
	GeneralBasics2d__ctor_m5BBA8B80514CB8E8098C55AF472FADC2BA164C2A,
	GeneralBasics2d_U3CadvancedExamplesU3Eb__4_0_mFB34B228D53D66D2DA60CBE36C863AAC135AAE41,
	U3CU3Ec__DisplayClass4_0__ctor_mF77915960E9F4C762CB8473CB99645EB20C2BB06,
	U3CU3Ec__DisplayClass4_0_U3CadvancedExamplesU3Eb__1_m34824CEE9AF028A0C15E9EF50B69F24B63AD82C2,
	U3CU3Ec__DisplayClass4_0_U3CadvancedExamplesU3Eb__2_m9486B659998B862E7D8D7F60D70F7E60023F33C1,
	GeneralCameraShake_Start_mF7CB74F9B674B375BDCB29EC25FF2B568431E543,
	GeneralCameraShake_bigGuyJump_m502DE5D59ED9FEAD8F13549A9A2105E86D1F90E3,
	GeneralCameraShake__ctor_m6B09C9BCA8DDFD913791AD123731FFC86E274FAD,
	U3CU3Ec__DisplayClass4_0__ctor_m3C8CF484C54E2EE225CC0D8A11A5BAEB2686C557,
	U3CU3Ec__DisplayClass4_0_U3CbigGuyJumpU3Eb__0_mA78BEF951754FC194ABA798EF9E551E30CD1DF91,
	U3CU3Ec__DisplayClass4_0_U3CbigGuyJumpU3Eb__1_m4A2B4F54066C9921E28893C3580BB18C47C8E99F,
	U3CU3Ec__DisplayClass4_1__ctor_mE5D0BDBB5AC685B299E3E124147549F48D5360FD,
	U3CU3Ec__DisplayClass4_1_U3CbigGuyJumpU3Eb__2_mE50BF2533D5A8E4215CA056E6C5A7B707C31615B,
	GeneralEasingTypes_Start_m9918519E1FE8506C66C09A06FC0973544C9B6CFE,
	GeneralEasingTypes_demoEaseTypes_mFAB0A2AA4E562BA44E4D99BB36EFC8BCC92CBD4E,
	GeneralEasingTypes_resetLines_mF6F826BE8342FEBEBB97FE69AEDCD749AD38E290,
	GeneralEasingTypes__ctor_m826931FD7A0F6840EC3920EF742AE00023053424,
	U3CU3Ec__DisplayClass4_0__ctor_m67D836D2CA426B728355DCE885351EF9BA44FB17,
	U3CU3Ec__DisplayClass4_0_U3CdemoEaseTypesU3Eb__0_mDA8F2DD5BE6F9B20420A94430FE2C634CC07EED3,
	GeneralEventsListeners_Awake_m929CA13301F3FA1A5C1AA230681377F30E0E78C4,
	GeneralEventsListeners_Start_m1C63BB1DBC231496398533A9D365C0B2EAD4BE80,
	GeneralEventsListeners_jumpUp_m2CBD7F9072D9EB312F608BFC683E63D3DFF1DB28,
	GeneralEventsListeners_changeColor_mF0F3863B481DE533D2DA0CCF3424BD1B231268BE,
	GeneralEventsListeners_OnCollisionEnter_m296C0AD01860A6C98C1E293DD67D880BB0590E91,
	GeneralEventsListeners_OnCollisionStay_m290268F1B803DC1E306F52423DA7A365442C7C31,
	GeneralEventsListeners_FixedUpdate_m903B40B7AC9A96613D21A365E8BC005E87AA8439,
	GeneralEventsListeners_OnMouseDown_m6B7190177327186B2D363437F5B96595367C7454,
	GeneralEventsListeners__ctor_m7B76E509C73273AA56871D0D0E9772F6FC2A3056,
	GeneralEventsListeners_U3CchangeColorU3Eb__8_0_m8D567E7710ED42E4ED9DF3074F1F9E4AF19AF1A1,
	GeneralSequencer_Start_m8B0E62A3511525F37EF49CF5BA670A8FDC005FBC,
	GeneralSequencer__ctor_mFD32ABD60398858ED0D676B843D61702B7FB1F75,
	GeneralSequencer_U3CStartU3Eb__4_0_m01F09B25E4652D0AD12402145A95897D0E65C36F,
	GeneralSimpleUI_Start_mE89FC53D2284D832DC21032BAB637D0C4075AC6D,
	GeneralSimpleUI__ctor_mC03751F3B550ACC5742FCB2C9B381E1EA841C085,
	GeneralSimpleUI_U3CStartU3Eb__1_0_mCC061524D85ACD8BAF6FE7D8CE1F2277735B3FC8,
	GeneralSimpleUI_U3CStartU3Eb__1_2_m32632A4DB6420C70DED2CA0F5D16A52ABED4D202,
	GeneralSimpleUI_U3CStartU3Eb__1_3_m81A54F1B0397D24C3ABC0367E5F3A96CD8938D25,
	U3CU3Ec__cctor_m2F58606DC92BEEAFADDAB84BDE6B467660090F3A,
	U3CU3Ec__ctor_mB27080DC10CB152A62D2B7D7F2BFFA689E952C73,
	U3CU3Ec_U3CStartU3Eb__1_1_m3121B206ADF49779458AA5961E16BC6F6DFC8277,
	GeneralUISpace_Start_m5EE834954E03550B87B33D931A593651703C2223,
	GeneralUISpace__ctor_m007D52C81E8F67F6C5FDBD433EBDD1F82281CE87,
	U3CU3Ec__DisplayClass15_0__ctor_mBE556FCA9CC905E8AFDA137DCA924101E479875C,
	U3CU3Ec__DisplayClass15_0_U3CStartU3Eb__0_m6F18D230FF8599CCA7A14221973A08E0447954B1,
	LogoCinematic_Awake_mC405113467B061C77307CFD90E064F5A651B3D22,
	LogoCinematic_Start_m3E319B3DEC812349F8C37171CA5D4081276F7826,
	LogoCinematic_playBoom_m787C8F6D4E17645300701C39C99D4E37893FEFAF,
	LogoCinematic__ctor_m4106D7C04CED9DBA0EA597C3A8961A01C01B88AD,
	PathBezier2d_Start_m6F6F8E4B60B7D179AE36BEE9EC0A264ED03D6849,
	PathBezier2d_OnDrawGizmos_m69311F42A05B822B9FCBD8CC4F0F4C4804AC9354,
	PathBezier2d__ctor_m28D0B7A13B4DA0EDA3A009A105016F4A7E143055,
	ExampleSpline_Start_mA17AF5D470B3BCA08101E4A82E8EB7A0BAABEAB6,
	ExampleSpline_Update_m356EAE7A488D899AAA63B3F9CC96E19743AE7EC6,
	ExampleSpline_OnDrawGizmos_m1479029D4155936B69C362144D347542E05CC0DF,
	ExampleSpline__ctor_m9FCDF08DBF16C46F63F1AF63EF143E3DAC295EE1,
	PathSpline2d_Start_m1EBB4A3F12FFD7F33EC3D10C0955B5D421DE5B5D,
	PathSpline2d_OnDrawGizmos_mF49ABC37A9C2C3B83BB5BF1419067E2FD8AB1B77,
	PathSpline2d__ctor_mD3756639777CCF9D251F157BCF32647C9F0AB7C6,
	PathSplineEndless_Start_mAB94E60786A238B143854F91E30BB877D5730800,
	PathSplineEndless_Update_m5C2C0D01E2D3AF4FFD2AAF23756C62733DB1509A,
	PathSplineEndless_objectQueue_m4F43CF1A70A9897A13A3B385DCE841DA5141C524,
	PathSplineEndless_addRandomTrackPoint_m3A914064336F71FAC30C4919147216682994A961,
	PathSplineEndless_refreshSpline_m8B64174788E0276D3C61E2F66C85EF7378EA30C2,
	PathSplineEndless_playSwish_m968F1F1AED0F8421A2834347C318076A3CE747BD,
	PathSplineEndless__ctor_m411B70A0AC412E9518BBBDC687C25CD1B388E6C9,
	PathSplineEndless_U3CStartU3Eb__17_0_m89EDA38D298348E747F008DED4C1CF7D4F815D60,
	PathSplinePerformance_Start_m6A0FB9A0FCA5D3B1AFDF6C03C8B8CD3DCFC57040,
	PathSplinePerformance_Update_m5416A63FFB450EABE065F6C5C3312B7C048EF5A7,
	PathSplinePerformance_OnDrawGizmos_m490615FEB9C7E53A19C0DA922E9A7077A1A66274,
	PathSplinePerformance_playSwish_mDCC9C70227FFF9F7969D9D8330E5B485B954DC7A,
	PathSplinePerformance__ctor_m3918B4846029379DBF03985747088A8F4E734CBB,
	PathSplineTrack_Start_m63CB9A3917A58E8791AA4DED6921EB32280FFF7F,
	PathSplineTrack_Update_m00439912F9ADA883049E7B01A7453419BDB6AE79,
	PathSplineTrack_OnDrawGizmos_mD7F0DCDB7C1B0CB6ED0F1A4A20E2389A14127D5F,
	PathSplineTrack_playSwish_mEADD52F2DF6153507245FCEF25CCE5AAC216627A,
	PathSplineTrack__ctor_m7F933342B4D6E1B74306EF85FC59C7F70E5EF982,
	PathSplines_OnEnable_mBAC54412DA75F41A2961B5D2A8D33971CA42EF78,
	PathSplines_Start_m7E196F5CDC4C203D9187A30B90FC02414FBDF186,
	PathSplines_Update_m2D049CD05DCD8B298477D7A6F6EEF97F16A140EA,
	PathSplines_OnDrawGizmos_m2099220E2C8911102EA2BE1EFC5153CDE8FF6283,
	PathSplines__ctor_m4BAB7527375E54EAD3B636B8AB1F5AEBF88AB565,
	PathSplines_U3CStartU3Eb__4_0_mC304E252D7336E181BBEF0F27B79311DEDF4BB0B,
	TestingZLegacy_Awake_m6ACDE967AF5970766B8EFFCC196B677E07DB0C10,
	TestingZLegacy_Start_mE43112422475CBDC67D4CE4680D0B64C38D23BA6,
	TestingZLegacy_pauseNow_mCC617C16BCE35E2D901183CFAEF9F51D7805DA61,
	TestingZLegacy_OnGUI_mAF981918615B59787EED9DC6EB2F232D6A4F6E5F,
	TestingZLegacy_endlessCallback_m96FFBCC19A52A227D879CA4C9A4D784078F0FB65,
	TestingZLegacy_cycleThroughExamples_mE124EAAF3FE6EDE035F8D620E0FF877916FFCA2A,
	TestingZLegacy_updateValue3Example_m690CC88D964055BB090736A4D7D080E043E15D82,
	TestingZLegacy_updateValue3ExampleUpdate_m28A23A4EFF68505C400DC9F5BBBD6ADA3ACAEE62,
	TestingZLegacy_updateValue3ExampleCallback_m25E92C1FD9658ABBED9080D237D95E6BCFC57E12,
	TestingZLegacy_loopTestClamp_m1EC272930E9852F0D980AC98F1633B4F2841C450,
	TestingZLegacy_loopTestPingPong_mC5B0F54C34F86BD154F6A0BB43C88FC7913344B9,
	TestingZLegacy_colorExample_m9C06FD132681F8FF64F1498BFC2811EFD82E4D28,
	TestingZLegacy_moveOnACurveExample_m89E0D3F9175DBDEAB39A915BCCD5AF06FCD2357F,
	TestingZLegacy_customTweenExample_mF418B80DCF007D21FBD0663BB9F002CF18EAEB0A,
	TestingZLegacy_moveExample_m5A0E16D835C2A10B7042F64D1B6CD0C1320B1F78,
	TestingZLegacy_rotateExample_m61CF7E2DC245EBF480DCCD0369ABA0F332658917,
	TestingZLegacy_rotateOnUpdate_mE9D7799638F85C32FB7715059ACBDF657C89B7CE,
	TestingZLegacy_rotateFinished_mE2E799232C3119A15368CBABBF594C57799D5683,
	TestingZLegacy_scaleExample_mD080FABF7510CFB95F324A1B4BB1C4277937EFF9,
	TestingZLegacy_updateValueExample_m9DBE93DA0EC50F7E6764A27DE1E87F12ACF8DA2A,
	TestingZLegacy_updateValueExampleCallback_mEB667645443DDFD352588C65ACBBE1C500E97C4C,
	TestingZLegacy_delayedCallExample_m51E456D34EDF2598C8EF549CDDB2376F607C3CC1,
	TestingZLegacy_delayedCallExampleCallback_m15CE90D43A85804204B22169D73FDE3EA46F609A,
	TestingZLegacy_alphaExample_mA4ED4E57F148B4EF6E2B1B608BAEE0EEF5AE7143,
	TestingZLegacy_moveLocalExample_mE6E3C5CD62456482BADA24B0CA93C0962125B80F,
	TestingZLegacy_rotateAroundExample_mB3F00FEDCDD63C7BFD293C187BA847BF15F7ED3C,
	TestingZLegacy_loopPause_mC83ACD076E19354DC2CFF3D3E409DA2901CE229E,
	TestingZLegacy_loopResume_mBA3E805FE386DFC40F4F795223744619E9048A7E,
	TestingZLegacy_punchTest_m1266ADD2035D0E9971ADA805B0A1636D35ABF2B2,
	TestingZLegacy__ctor_m1CA1AEE9AA84B7551AFA5A034BF4E648AE662D9E,
	NextFunc__ctor_mD13871807D65A0BB2C5E0F567E5FB021AF248AED,
	NextFunc_Invoke_m8D7A63BD3F35D7D22BC9DAF5241C637889044719,
	NextFunc_BeginInvoke_mB38D1030FE53CEF432C2E6D1569F0EBAD0F1B5F0,
	NextFunc_EndInvoke_mFE2435A3542567B6D23B6E63B50BF9EC6D55609B,
	U3CU3Ec__cctor_mBD80E302F3A69C0C61FDB9987E223DF4A13097D9,
	U3CU3Ec__ctor_mD7A097986D81133EA1341850CD4003072DC490BB,
	U3CU3Ec_U3CcycleThroughExamplesU3Eb__20_0_m99E554155125675C01E3E61098DA676F0B69E649,
	TestingZLegacyExt_Awake_m7D6F79923E270BE07F7B475DBFE0A7CA94534FCB,
	TestingZLegacyExt_Start_m86BD1A532532B1F08B6ECF7457358F6CB1410E5E,
	TestingZLegacyExt_pauseNow_m0A9E3639505AB9DDE82956D7FB34C37758982180,
	TestingZLegacyExt_OnGUI_m25777A6897DF3B16F25546C5CAFBF80FB60923AA,
	TestingZLegacyExt_endlessCallback_m73A1A18DA9DD7642231DE5BBBF35F45DB785A077,
	TestingZLegacyExt_cycleThroughExamples_m15FB09793AAA4E36F905F6D81C254E0B763F391D,
	TestingZLegacyExt_updateValue3Example_m15F0FA3A4491DBB842544D0A878B2C533640B4AE,
	TestingZLegacyExt_updateValue3ExampleUpdate_m46BCB3BF2A85D38DE7CE46720DF912FDA7DB84FE,
	TestingZLegacyExt_updateValue3ExampleCallback_m7B2BA08D3878E1672F358452FA11C48F8D686B76,
	TestingZLegacyExt_loopTestClamp_mA4A7563CB2448E2D1EC1394494E21C79399BAC06,
	TestingZLegacyExt_loopTestPingPong_m6523FCB9E3A1AC5035882C67F6EF5B8A00AAC5D3,
	TestingZLegacyExt_colorExample_mCF5A6A5FAA5CC4EC32FFD99062DA09D220B776A2,
	TestingZLegacyExt_moveOnACurveExample_m8E7EA9EADCB22EB3DC2DDC7FD27BD05050C2FB44,
	TestingZLegacyExt_customTweenExample_mE32AD842F2ACF6BB96A100E29B784B942004386F,
	TestingZLegacyExt_moveExample_mB59B97379A841F1AB17E8E9CE8BC7AFF0DB9F3C8,
	TestingZLegacyExt_rotateExample_m15D1742A434CC70072A4C96DFEDB623B1BD8C02F,
	TestingZLegacyExt_rotateOnUpdate_m854E95A564B027980BE0FFF08EF1A20CA3EF5EC7,
	TestingZLegacyExt_rotateFinished_m78E88F2F51C8018BBA472122955C89DCD3C13B53,
	TestingZLegacyExt_scaleExample_mDDF64026F75968C35562CEE5C1B2F84874C76C1A,
	TestingZLegacyExt_updateValueExample_mD778D12570BC222BB68613D27BD4B5B0DCD9E76C,
	TestingZLegacyExt_updateValueExampleCallback_m273FBEFFA8289DCB71813A6C4FF5C66ABD2C5003,
	TestingZLegacyExt_delayedCallExample_m92DECA48CA80DF5FF46B873EF87AC2A564AA6452,
	TestingZLegacyExt_delayedCallExampleCallback_m2F2F9A40BA43F410DABE187FEA3861FCF719058B,
	TestingZLegacyExt_alphaExample_m0875576982CCE840BF262C71817239AB7EA6C8C2,
	TestingZLegacyExt_moveLocalExample_m81967DF8243173690E01BCD0C9DEDF141C080813,
	TestingZLegacyExt_rotateAroundExample_mA9188C5A54C644D2B78F8956DAB3B245B975BCA1,
	TestingZLegacyExt_loopPause_m04999A727B7B5D4D7C04BA4D24C78FD7C35FCB42,
	TestingZLegacyExt_loopResume_mAD5DEA4AB4F83437FE25A8C8BBE141FB885CB6CF,
	TestingZLegacyExt_punchTest_mEE9802D38D39AB2BE2BE50BD88F901A16383B87B,
	TestingZLegacyExt__ctor_m5E300E444E4496DA32F5F4A1F8B48051126D8B61,
	NextFunc__ctor_m0008349A4886ECE27F576CFE82C9A888798F60BA,
	NextFunc_Invoke_mFB6931473FA9F6AD02BE8B752A4D051ECB660ECF,
	NextFunc_BeginInvoke_mD91FEF6AFFC5695F493E6950FAAA8A6B72DB37CD,
	NextFunc_EndInvoke_m96DB2010CA80A82BDCD35903E0CE02CE8AC78296,
	U3CU3Ec__cctor_mD11D91D44BCDECB51A3CF966B686782B4F5330A7,
	U3CU3Ec__ctor_mC490B74A87268C68E2428BE981C75A67BE4460A7,
	U3CU3Ec_U3CcycleThroughExamplesU3Eb__20_0_m232F589CF9029278662FD1D5BB31DB7E6A2973F5,
	LTDescr_get_from_m5C46DBE96932E1F21CB71C1174B3294CF8F80E4A,
	LTDescr_set_from_mCEA64BFED572C9327D1186A8DB46FFB56836E4F5,
	LTDescr_get_to_m3887E20BF193FF607A48502338A46C90948F5FA5,
	LTDescr_set_to_m12DADAD8999E97D8D46F4219632BFAB4FCA41796,
	LTDescr_get_easeInternal_m263F859E9225DF85EFD75C079CD5073AE647B284,
	LTDescr_set_easeInternal_m40A3FEDCAA0EE7EB86E804641367E50E34C0CDB9,
	LTDescr_get_initInternal_m3E9E4486FBFCFFDB8A3A91EB00DBEBA29916C943,
	LTDescr_set_initInternal_m21E7CD474330836550F6298EF7F0F2902BC3EE60,
	LTDescr_get_toTrans_m03763D518E68ED5106B72B7DEBBD29765285CB78,
	LTDescr_ToString_m8D2047DA16E09587E6709505E2328386ED0BADA7,
	LTDescr__ctor_mD2905F6F8358B3AD8C5784EEA14DE3C05769538B,
	LTDescr_cancel_mC12FD3EE1E42AFCC7AA536A6D344BE3AEA86AE70,
	LTDescr_get_uniqueId_mCCC4A89D6863332459B9771B4E636F951B2AAD3B,
	LTDescr_get_id_m71271377DB5ECF8CF10782531395CCB0B5478E09,
	LTDescr_get_optional_m0475EBA98D33B8F970E159516CC6BBEC331BE807,
	LTDescr_set_optional_m17425AF517082967050C04F2C3B3D2D574DDB2B0,
	LTDescr_reset_mDC3B5AEC9CB77A12D954B5BB283B062A950BC8DE,
	LTDescr_setFollow_m984BCBD0F7B73FB1E6ABAF2C1B4CA1EE506546DC,
	LTDescr_setMoveX_m76CE7ACEF44A88ED9BC3E747549EE2CFCB2013A2,
	LTDescr_setMoveY_m09BC6472FAD45D9AB081F3D63C49C09E1369205D,
	LTDescr_setMoveZ_m421A6F5AFA9E4627D93946E2797FE1C4DB561DF4,
	LTDescr_setMoveLocalX_m75617B5FB504CAF9FF78274042314A7A20388ED6,
	LTDescr_setMoveLocalY_m22DCFF89FDE31EBEB557B9964E55DFC264110243,
	LTDescr_setMoveLocalZ_m5B79B96D94FF6B39A2CFEFF240E3C88D6388F6A2,
	LTDescr_initFromInternal_m639FAC099FE64CC165535CCE0719472B44AC7F22,
	LTDescr_setOffset_mB2DAAF3FEB7717194BBA1C04741D4EF68C8F952D,
	LTDescr_setMoveCurved_m0DBB6E2C2BAA1806B5027FFA8DC167215CC9425E,
	LTDescr_setMoveCurvedLocal_m81E717B0084EB49305FF083C0CADA94DCD974319,
	LTDescr_setMoveSpline_m0927D09871DEF890A033AC185FD26BCDBE1911C7,
	LTDescr_setMoveSplineLocal_mC0E62F0B0E9597DAAE5027FAB2F649EAF2E13034,
	LTDescr_setScaleX_m7CFF1D6BE843E9721DB183EB147279B195953B7B,
	LTDescr_setScaleY_mBC9B04832806ACB660D3E326621711B15CF7F791,
	LTDescr_setScaleZ_m0401AF8B4C25CB3AA2CDCBF4C2DBD75166897C0D,
	LTDescr_setRotateX_m26F91A22B505D763B444E6523057008EAD6FD7B3,
	LTDescr_setRotateY_m52016439FDA153BB871679C34D65946434862E66,
	LTDescr_setRotateZ_mBC6D82441A461E415CB255195BD66FBC4AF132D9,
	LTDescr_setRotateAround_m346F3BDC7A38E146F5EF8FACC1DBA07BD3684918,
	LTDescr_setRotateAroundLocal_mBE18B57E33C1118A01FBDB41A99DE69746C9C865,
	LTDescr_setAlpha_m291AFD44926FB68DD36823B9D142AA276D072962,
	LTDescr_setTextAlpha_m569C24FD4701EFE34149E4803E189518B383B0B1,
	LTDescr_setAlphaVertex_mEF508AADA9444AAB3946ED1C2A3506094A541E4A,
	LTDescr_setColor_mA47DB19E9808DD9AAA142AB9CC3D33C4A0EFAB04,
	LTDescr_setCallbackColor_m9EA1B5111A4241C55BC10C0705147A33BA07AC33,
	LTDescr_setTextColor_m3A20272C52CF7141B376DCFFB58C4BA642DD4A83,
	LTDescr_setCanvasAlpha_mD05551595E5C4D79C3242EEEEA99AD01274C0BF1,
	LTDescr_setCanvasGroupAlpha_mF556BF3EE226BAC974CC36C638CECD86BD29E2B3,
	LTDescr_setCanvasColor_m52C8833AC9DCC36676E9795E24F31D7955CE17C2,
	LTDescr_setCanvasMoveX_m7B0F43CC9D9B78A933E1B5EE3CF4A9944129B11F,
	LTDescr_setCanvasMoveY_m5A9AAC03AB65F7B5E14CA663EE591F9FF8FDE5A9,
	LTDescr_setCanvasMoveZ_mEBDF9362DB395299B0E6C4EF45A8E5DF8FCA5CE0,
	LTDescr_initCanvasRotateAround_mDF5155DE268AB0DB414A892643342B04C3C80430,
	LTDescr_setCanvasRotateAround_mD875CE01D408277380BA7E1A90855FAB176930E5,
	LTDescr_setCanvasRotateAroundLocal_m0FB16DFF151E8479B394929E477812B908E71811,
	LTDescr_setCanvasPlaySprite_m5507D16A5CA3D57966B06FE22CF280B3635D272D,
	LTDescr_setCanvasMove_mBA28E1B08C4CDD0493F4AEE9032DB47D8DE96A57,
	LTDescr_setCanvasScale_m25D2078C0E8BC068AB18C945177B424FD8BDF35D,
	LTDescr_setCanvasSizeDelta_mD3C9CBB1C8F686F2E253C22DB1A2C841C39D00F3,
	LTDescr_callback_mCC3CF88649A2DB3AADEA6D010D9E1D6F8F0B50A1,
	LTDescr_setCallback_mD0C03116D293C89C840A0EB7D7276987E5704AE4,
	LTDescr_setValue3_mDCA24E0028F363246355199865E0E9C795919C87,
	LTDescr_setMove_m0A39B796C492B5F74EAC7A88B614DABEEC83E31D,
	LTDescr_setMoveLocal_m7979A95A8C3E83116630A1972484ACB778E042B0,
	LTDescr_setMoveToTransform_m44267D9670BA0AA3F282AF4654C65465EBE562CD,
	LTDescr_setRotate_m481E57B448F3773EB7E8FEDB4D54061FC454EE79,
	LTDescr_setRotateLocal_m2C363D0AC041E31D2D2ED4143CFFBA71E6625EB8,
	LTDescr_setScale_m0E480B54D7D9C2B8268023050E9231052B87BCA9,
	LTDescr_setGUIMove_mCEF7E5A5AABE334A23DE8CDBE26EDD4658B06CA7,
	LTDescr_setGUIMoveMargin_mFA7CF549318E4EA41AEF0A72DE52926C88AE3D28,
	LTDescr_setGUIScale_m428EBDD7206CF4353C456CDD2554ACAD5989457A,
	LTDescr_setGUIAlpha_m316B0C88A1C8D63CA862FEF28A6AC263D043B4A5,
	LTDescr_setGUIRotate_m5C0C9A675F084647D909DA88099F8A056CC18DD0,
	LTDescr_setDelayedSound_m1D0040CD3D5090096914684D47D8076A21A0D732,
	LTDescr_setTarget_m5F34436BE6EF6A6E6804AA39FD773562BF873647,
	LTDescr_init_m9C699F46C58C3BEE7F19E14E4B737FBC34685B60,
	LTDescr_initSpeed_m4F4E1F0B62028C76C78B000403BDDFBABCB30754,
	LTDescr_updateNow_m7A17C67327BF1CA89C94257396A0B64F916FD330,
	LTDescr_updateInternal_m6F69EAADDCA9F3FC16D96351A97EB4D0DA935A76,
	LTDescr_callOnCompletes_m5F22E85C54CBAEA43EB1E2972B309DE885B6BC72,
	LTDescr_setFromColor_m93EB8D7B171E28CC66A866DFE42C4637D17BF9B7,
	LTDescr_alphaRecursive_m9862620727DE002BA5334449EE51E47FB9C35BDE,
	LTDescr_colorRecursive_m66AF77EFA7536C295518504A00B20968E3F773AD,
	LTDescr_alphaRecursive_mDE10D7A23D1710400E453C787924FB83F89EFDDD,
	LTDescr_alphaRecursiveSprite_mD677C77EFB7426B437BF89A60BE8D29B6E3ED014,
	LTDescr_colorRecursiveSprite_m2BFB41154D3FBB7D70FF5F2BBB8C3B0F4FC02699,
	LTDescr_colorRecursive_mDFBBC244E1A546B872218F43CE4D3E230C07DC35,
	LTDescr_textAlphaChildrenRecursive_m30329E963CF1363F6841959DD91EED9FF7EA320B,
	LTDescr_textAlphaRecursive_m12B0431A56E4A23B20528CD399BFA2CE80658E3F,
	LTDescr_textColorRecursive_mFA16970EB10C436367105DAEF64A22B7C584CCF0,
	LTDescr_tweenColor_m3020E4C1CC76332688E53BF95D72BD1B3CB461A9,
	LTDescr_pause_m831C5988C87832DF30D6566086DD174849A8712A,
	LTDescr_resume_mBE4F94BDD8338970F8DA5167345E87C4B58B30F8,
	LTDescr_setAxis_m786C5D48517CA736F5B704D876E4E69234E087E3,
	LTDescr_setDelay_m55BD6D8AB740123B2EE42BA1721C7E6E29504110,
	LTDescr_setEase_mDE953D5A1E2D1234C5CFD2F0CDB6F32B787ACD0C,
	LTDescr_setEaseLinear_m02CEA15BDCDC76F3FFE4AF8C8C7D4C4BE696A2E0,
	LTDescr_setEaseSpring_mB49258E88824A2EF22CA9CFA751754D59C00646D,
	LTDescr_setEaseInQuad_m1BD7E1556B02807399E80DB203B9553AA766B35E,
	LTDescr_setEaseOutQuad_m9FD9511C826BB2309FB7ADA82620AC454C207960,
	LTDescr_setEaseInOutQuad_mAD680A08BE87DD7F92202EEEA941DBD71A78B92A,
	LTDescr_setEaseInCubic_mDB7AC25B20EDCDA9371911EFEA0D073548C13681,
	LTDescr_setEaseOutCubic_m405E1A83E795CC30CA245DA33133F21D9FCD03A9,
	LTDescr_setEaseInOutCubic_mD69B9E20BD56C26709938CE025470378595D804A,
	LTDescr_setEaseInQuart_m8B52F62F717ED83CB7CD00E869E548FC5C60E0FD,
	LTDescr_setEaseOutQuart_m926A771E75DCEF5025BA544D0D4814D802658967,
	LTDescr_setEaseInOutQuart_m36299EB57FE5C7B772B07760AD79A4758A407EAB,
	LTDescr_setEaseInQuint_mC37282FD3528ABDE0B9ABBECD0EE4158EB78BB34,
	LTDescr_setEaseOutQuint_mB5DF94AAAA9986D805ABD7B8705957F00C9FA298,
	LTDescr_setEaseInOutQuint_mD107E1CB8DA7D291E2507DE864FE3BA7690D3667,
	LTDescr_setEaseInSine_m558572F5D792761C77311F93E823AA522ABFA821,
	LTDescr_setEaseOutSine_m04078632ABF4A13E48FFC566DADAE650E3531D8B,
	LTDescr_setEaseInOutSine_mCBF1B47223AB0E40EC462E99311B5BAC92CAF0F4,
	LTDescr_setEaseInExpo_m031D49646619C11E4CD9124E33ACDE7455963D61,
	LTDescr_setEaseOutExpo_mD8C3A31B2E8E473F34744C5BD4855A28385777F5,
	LTDescr_setEaseInOutExpo_mC6247CF20D6C40011D6A31C5AFB4E93C2E27D83B,
	LTDescr_setEaseInCirc_m64F3ED13ADA1CF100ADAD67A01A5C57FC073ECFF,
	LTDescr_setEaseOutCirc_m3FA5A19F0A8F63DE7F2FA3AE57A1E0DF371451DF,
	LTDescr_setEaseInOutCirc_m82DC1F56BD70CAAA7CFC0786FC31565D4B7D1F8C,
	LTDescr_setEaseInBounce_m0A8189AD4174381A97F215E75C5D40485395A549,
	LTDescr_setEaseOutBounce_m3E4A3DBF4712B6F566D5FFC5B8AFD96DB06FA374,
	LTDescr_setEaseInOutBounce_mEC4B3DC43EE8829DB40045AE072895AB782B9FDC,
	LTDescr_setEaseInBack_m7A4BBFA0DA727FA51678D7B4561F588CFB1454E6,
	LTDescr_setEaseOutBack_m53F3499FE76CAEA5F462EC96113A265E15438418,
	LTDescr_setEaseInOutBack_m1970B0DA7214B401D9894AD2BDC75A96016FDBBE,
	LTDescr_setEaseInElastic_m9D356852FCC3BEB9002EC375047192B257CDE9A6,
	LTDescr_setEaseOutElastic_m97DCC808E07D990199DD09AE961EF7CFEB401698,
	LTDescr_setEaseInOutElastic_m1CC9921334D9A64FF1F4C2E5285F3E5712C6D401,
	LTDescr_setEasePunch_m1DF3D2CFEE5E2CB6EEAF5F485DF326F8B2A08B66,
	LTDescr_setEaseShake_mAC2E54A2A115410297235E85E9567AD5D96CC86E,
	LTDescr_tweenOnCurve_mAAE98016598CDD3C0E19EC9016CDAAC27290FE40,
	LTDescr_easeInOutQuad_mA1B3167A272546E8F4AD51BA4456D911E7A97CAA,
	LTDescr_easeInQuad_m8B8A3D80C2483095A2E9319E0E560A16A5D45E03,
	LTDescr_easeOutQuad_mA14EA059114AEE2C71D891DA1D723554387FB0FC,
	LTDescr_easeLinear_mAFF573D79A9494A37F171FDCFB1A843E4A099BBD,
	LTDescr_easeSpring_mBCC692E63F1CA607883E42FD5EF44799798CF544,
	LTDescr_easeInCubic_mA1F3409DFC4FAB01F0E1D7D5C13AF3EF533A592C,
	LTDescr_easeOutCubic_mD3D32B7F8A774C06F28C78EFD8B35F82ABE0B2BD,
	LTDescr_easeInOutCubic_m10079E2EE4532DD46450DE324ACA7091D4082449,
	LTDescr_easeInQuart_m7F3F07BAF2296FE3EF14D9315A45B101B4A88C48,
	LTDescr_easeOutQuart_m19C687BA558F3962E3D94A9224DFB21AEB8DB6B4,
	LTDescr_easeInOutQuart_m4A83DAC8CFD32AC6A3F5BE9847FAB5B397567EA0,
	LTDescr_easeInQuint_m8DA64234B559AEBF6F300EF6FB4A168247F54DD3,
	LTDescr_easeOutQuint_m72712C773DCFA24B83AF2013806A795B3874B4E2,
	LTDescr_easeInOutQuint_m860D58335F979A03E4BDA78BA9D37BF3F9CAE3C0,
	LTDescr_easeInSine_mCF9996B096BEB3C47C523DB7DC3DDE5EEEB362CF,
	LTDescr_easeOutSine_m56F54AD9F205357269FB50FC3DB13551BBDE7AA8,
	LTDescr_easeInOutSine_m4ACFEF2C6BDF120DAA6C76E563B6AE6CECD5380F,
	LTDescr_easeInExpo_m07C8C2CF2B118A52CBDC361310B8D8C3DF366A52,
	LTDescr_easeOutExpo_mA866024A98FD2A2CC8808917B983BE6D3BF4156A,
	LTDescr_easeInOutExpo_mBEC5D63CA1D9C4E0F6EB2C0935E6D2550E8FBBC9,
	LTDescr_easeInCirc_m5DB7FCD1C107646BCBF7BACAE93191864E5A7BDA,
	LTDescr_easeOutCirc_mEA8558C7773468D1860A2BDC636FA5EC2C8791D8,
	LTDescr_easeInOutCirc_m49B59C6FB90730D195CFE5910C1A1596F18385C3,
	LTDescr_easeInBounce_mA53E96538907F54DF3E668026D7F62D8A346B325,
	LTDescr_easeOutBounce_mB2732EE22499FB0ED83AFEF85FD4896E629A19C8,
	LTDescr_easeInOutBounce_m5F2B5E4F09C66FA720583B13EE7DEBF46E4B4250,
	LTDescr_easeInBack_m1AD66A3F3E0513ACF1D06C01909DB989D263ACFC,
	LTDescr_easeOutBack_m971AF92F07A84D5E01B10F1CED6F43F02217CA0B,
	LTDescr_easeInOutBack_m9AEAAB6088AA50FAE1F839D39D70B79250467DC1,
	LTDescr_easeInElastic_mA980A8C764DFBC3A2DCE4CC566A3AE5D8DBB4F15,
	LTDescr_easeOutElastic_m4E6536A1BEA97546DDB61146FD6A78BBF31E1207,
	LTDescr_easeInOutElastic_mA4E464B18D106514E4092266A28D8EA4138858CA,
	LTDescr_setOvershoot_m47B052637792C78A0D5195306CCA86CB9E7A96A8,
	LTDescr_setPeriod_mC03D996C9712715E08646FDBD5421719CFB8F3DE,
	LTDescr_setScale_m20ABE114A3769DF22F968A3BAE3B969D7405C84D,
	LTDescr_setEase_m85FF6788C6DD67CB57F667E80E456C7CE86C9E3F,
	LTDescr_setTo_mC614978FF31785D8135380F7BF1FEF8ADD2F365D,
	LTDescr_setTo_mFE5077089EAAEED3A0B4E00827BF4867FA5E4E76,
	LTDescr_setFrom_mBFD923D83E18FB53AAFB9293C1F13175DCE15D34,
	LTDescr_setFrom_m82D22FCE616A6826A3007BCF148DA437F16BC0F8,
	LTDescr_setDiff_mAF9E8982F70181B987022FA76170A8DD7C61FA5E,
	LTDescr_setHasInitialized_mC54A9AAD341DE33BC04734061E6EFE42E89402F6,
	LTDescr_setId_mB492FBFB9D2FE6ED7CDD7EEA9F3A547A4BC596D0,
	LTDescr_setPassed_mC3016CADF8995A54ABF2D5D95908EE63D3298E65,
	LTDescr_setTime_m0DE4EDC9DA5F9D6B5255BCA73A198E18F0A320A4,
	LTDescr_setSpeed_mF8F0031D23893E09F0E258367FEA33FB175CC269,
	LTDescr_setRepeat_m7D66B000EF60DCC943D848C1BF53BD7C7BEF8313,
	LTDescr_setLoopType_mF2FC7F646DDB9AD46FE9FB3972C4B2EAA5634C77,
	LTDescr_setUseEstimatedTime_m1882A739CA3F9CE2F6215C61E92D4D5108F352EE,
	LTDescr_setIgnoreTimeScale_mDD2DF875144DFBA017697139ECD83164182DD6C9,
	LTDescr_setUseFrames_m356FC10CEEBD2F74F93F14EDB27B58A87C67901E,
	LTDescr_setUseManualTime_mAC4545BA99BA9C7117151D155C4664F6D7E933D2,
	LTDescr_setLoopCount_m658F924D41197D6DDDEAC18C37849D2341C1C993,
	LTDescr_setLoopOnce_mBF4016C6E09D73B6C4210585FEC20D250BBEBE60,
	LTDescr_setLoopClamp_m596650592727CC3F3C5513F54432D93CEA45666E,
	LTDescr_setLoopClamp_mEA5F7A4F29DFEC798D1DBF7CACC4EA95E5681733,
	LTDescr_setLoopPingPong_mB14D4AD3878CB744ADBB3F0B933E1C50CACEDC18,
	LTDescr_setLoopPingPong_mDBEA094A372AACBB02EFFA679DBF2C551928140F,
	LTDescr_setOnComplete_mBD0B6BAC2B05C7AE12E93F478BC6F0F41A33C44F,
	LTDescr_setOnComplete_m61B62A9BB1DE6DC69EB97F111139AB379A2AB33C,
	LTDescr_setOnComplete_mD8043AC13DAB80A85715B3B5D2E273754A93157D,
	LTDescr_setOnCompleteParam_m0DF2C3997947B6F72DFD4AA1BAEC841B8B84DD63,
	LTDescr_setOnUpdate_m55FBE2275AB200DCFB3793FA745A238491E7072B,
	LTDescr_setOnUpdateRatio_m52F3EB3754460E5DA2EC48CDD69FAEF7953CC1EB,
	LTDescr_setOnUpdateObject_m5B410690D035323DF02535DBFDD06EFE602E4F45,
	LTDescr_setOnUpdateVector2_m0FB697AF241FBDB5958D2FA52C5BF964FDC9F3A8,
	LTDescr_setOnUpdateVector3_mFFF939DCA282CC77307788DDE78DAC3FE8801920,
	LTDescr_setOnUpdateColor_m162951739730174C9B12F55091940A39981C1BF7,
	LTDescr_setOnUpdateColor_m3CE8B6B35EA308221FF7739A6DDFFE8B308DAAAB,
	LTDescr_setOnUpdate_mB7154EAF06F1366C24E3E8EABFDF53169A1EC1B5,
	LTDescr_setOnUpdate_m487CA9638173DAF55C9DF3B81F7D97232222D88B,
	LTDescr_setOnUpdate_m8A27C956F125846A8713234EDB95FCA045C3BF50,
	LTDescr_setOnUpdate_mF6CF9481ADCA4D3270B5E21634B8D924D1FD7DD2,
	LTDescr_setOnUpdate_m5404E20426F6927BD37BD1EA3B0BBBF1642CE37F,
	LTDescr_setOnUpdate_m5356685ADFD38D45881211740E990617C0F2A80D,
	LTDescr_setOnUpdateParam_mE61939C5A8C86D2603FD2F9ED73ACF3CF1FB1D69,
	LTDescr_setOrientToPath_mECE38AB1BC1313F39FD2FE3D582AB18A849826EE,
	LTDescr_setOrientToPath2d_m08D9F50871013C10D1961AFF0A7759A8D92B1294,
	LTDescr_setRect_m1E626946F828ADB3D7E7A70D4E43B1CA7B2009F3,
	LTDescr_setRect_m98165DCDF51B68BCB9CAEC7EAC5C364D2E26758A,
	LTDescr_setPath_m9BFB77CBAE51ED839078FD49C7611ED8323AC154,
	LTDescr_setPoint_mB0744AAA4A452543F68A3126C05CD8487BEF7F6E,
	LTDescr_setDestroyOnComplete_m697253D565C9D2CDA484A82E1005660595CEDA06,
	LTDescr_setAudio_m86759A48D1B7D26395A5B28AB5450E84AE46AECE,
	LTDescr_setOnCompleteOnRepeat_m28F4EE66B0CA50AC6FA5A9391EEAD0B62E85A3C1,
	LTDescr_setOnCompleteOnStart_m2FB448E21905E33DCE1F35EBBD8E39097AC35938,
	LTDescr_setRect_m0CDE3901FF03E6216DD7E6DEC6E65E7DEAF39AB6,
	LTDescr_setSprites_mC83C58D4DC03CDB0E8529A2DCDE37270089B0A41,
	LTDescr_setFrameRate_m0A569EDD28DDFD74BD71E0E5CF4730F178220AF9,
	LTDescr_setOnStart_m37383DCD917312D49E2291F3A47F8F9361155A1A,
	LTDescr_setDirection_m3B76149226B433591EC98766C8B14D63BDB7F193,
	LTDescr_setRecursive_mC2895C6EEB59EF79044495FA8BB6ACE914CAB402,
	LTDescr_U3CsetMoveXU3Eb__73_0_m39FBF744669510650F08E0F2B4E87CDEECA7440A,
	LTDescr_U3CsetMoveXU3Eb__73_1_mB4AD58106C3D205E5D4763F3F1CCA63B2F99EB78,
	LTDescr_U3CsetMoveYU3Eb__74_0_mA6309FBB2AF9AEEB859024E69AC8ABD6D102AD87,
	LTDescr_U3CsetMoveYU3Eb__74_1_m7A2A40165003571F84F057D3F39BD77A23B2F0E9,
	LTDescr_U3CsetMoveZU3Eb__75_0_mFE473C92996CA9EBA69397DA460F4B16EDE7D03C,
	LTDescr_U3CsetMoveZU3Eb__75_1_mF78C868A90B38D04ADAD2AD93D536B1791B3C671,
	LTDescr_U3CsetMoveLocalXU3Eb__76_0_m8A895E545E84F266EA0E0963C68274D9545D17BE,
	LTDescr_U3CsetMoveLocalXU3Eb__76_1_m17F56B25DDE0E4E6D6CC186BF9A9A617957C2FD6,
	LTDescr_U3CsetMoveLocalYU3Eb__77_0_m8D00D189D0ADCF6C778EFF39DCA1EA55C03E34F4,
	LTDescr_U3CsetMoveLocalYU3Eb__77_1_m67ADD5EAA303DB53DA4B896F2080F3E6975C76A6,
	LTDescr_U3CsetMoveLocalZU3Eb__78_0_mFD9EBD1D9EF08CB93FDCCE06D88DC940852C03DF,
	LTDescr_U3CsetMoveLocalZU3Eb__78_1_mF3D05D5EA84618939D8F8C0F9F06EE5D6294D77D,
	LTDescr_U3CsetMoveCurvedU3Eb__81_0_m8BD948AD31BE03BCEF9D75091AECC3DF8B4A50E3,
	LTDescr_U3CsetMoveCurvedLocalU3Eb__82_0_mE35CF92421F9DBB5687F1F1608AFBE9F86C80378,
	LTDescr_U3CsetMoveSplineU3Eb__83_0_mD9ACDB850FD978554B5465139A4E1EDD844CA1BB,
	LTDescr_U3CsetMoveSplineLocalU3Eb__84_0_m3219BB023C406080046DE37EBCF0CEDB8E323D1A,
	LTDescr_U3CsetScaleXU3Eb__85_0_mDF33F215E49CCD02AE02FDD79827006CB41DF18F,
	LTDescr_U3CsetScaleXU3Eb__85_1_m332A3ADAFDC200BF9AC11279593B5968DC537623,
	LTDescr_U3CsetScaleYU3Eb__86_0_m6A34E540DCDF955D835FCBD7FEF708257E07FE31,
	LTDescr_U3CsetScaleYU3Eb__86_1_mE1BCEE0DADE70D6CA3873140143141D9B51DBDB5,
	LTDescr_U3CsetScaleZU3Eb__87_0_mAEDB3F2903B250EC9885F64B147EDBDD4881E812,
	LTDescr_U3CsetScaleZU3Eb__87_1_mEFAE5A48C329D33CAF5F7003D48DD0B9C581AB85,
	LTDescr_U3CsetRotateXU3Eb__88_0_mD0976C92A94804F53718DEE26B131989A067EF7A,
	LTDescr_U3CsetRotateXU3Eb__88_1_m7C171EAF890568C630E3BE1EC70C97A0C55F0984,
	LTDescr_U3CsetRotateYU3Eb__89_0_m1B2E4D04C35A72D3957FEB294B306BF741228F1B,
	LTDescr_U3CsetRotateYU3Eb__89_1_m67AC1C0BCE4902B58FA999FC9475BF42D391F167,
	LTDescr_U3CsetRotateZU3Eb__90_0_m92CFF50F82E8606EA58D1FF72D2FBD7C9A0C7290,
	LTDescr_U3CsetRotateZU3Eb__90_1_mA55CC8D92F2601E04F21F45337134FBE437A20B6,
	LTDescr_U3CsetRotateAroundU3Eb__91_0_m90372B54B5D631FC5E1FF67FCE64F69A1E0E7E63,
	LTDescr_U3CsetRotateAroundU3Eb__91_1_m21A791E3630653D459153347755624070DC38965,
	LTDescr_U3CsetRotateAroundLocalU3Eb__92_0_m49A28FAA34B36D7AA09A322D63974997CB31BF43,
	LTDescr_U3CsetRotateAroundLocalU3Eb__92_1_mCB4C19C5A763BBF640E5BE6785D5AB03859AD606,
	LTDescr_U3CsetAlphaU3Eb__93_0_mADB8DE8A613762F896CC7754539BFFCA3A23B4E9,
	LTDescr_U3CsetAlphaU3Eb__93_2_m5318309F91340E1C143196D9ADA659C53B3C918C,
	LTDescr_U3CsetAlphaU3Eb__93_1_m44E2198D69A4F502CE6F5B1532AFD8F600D84717,
	LTDescr_U3CsetTextAlphaU3Eb__94_0_mE551CB2CBB6114CA72656A033CAC42EAB2EFCC8B,
	LTDescr_U3CsetTextAlphaU3Eb__94_1_m809E895E8E9ED4049E44ACAEEFE222EBA2E6A259,
	LTDescr_U3CsetAlphaVertexU3Eb__95_0_m53AB148A8D470C29DACB58274436B9535F5CAC37,
	LTDescr_U3CsetAlphaVertexU3Eb__95_1_m197F71CB84DF1265DDDEC92AB60CFAEC2B2E8BE9,
	LTDescr_U3CsetColorU3Eb__96_0_mF63A06E57C3B0A1B5B8FE3E7EF131BD862457B19,
	LTDescr_U3CsetColorU3Eb__96_1_m8EA4C07F32943F895BE0CB6909C63263827CEBF8,
	LTDescr_U3CsetCallbackColorU3Eb__97_0_mAD825635FB3FD1DE688BD692F394C91BD701659F,
	LTDescr_U3CsetCallbackColorU3Eb__97_1_m3635B1AEC7852BD361B57DAC85568D84BB050C38,
	LTDescr_U3CsetTextColorU3Eb__98_0_m1AC4C496FB90FA9C994A7D8772EE75A96D8C7499,
	LTDescr_U3CsetTextColorU3Eb__98_1_mF8C5941A01CC6AB44A3C1A9E4E0D42DE0019B1B5,
	LTDescr_U3CsetCanvasAlphaU3Eb__99_0_m03D22099CB93845FEE24F439C52C194CF424307B,
	LTDescr_U3CsetCanvasAlphaU3Eb__99_1_mC255CF9153C3E078BCC1E35C16E786C8662EBFD8,
	LTDescr_U3CsetCanvasGroupAlphaU3Eb__100_0_mCD1535F45FF98C078658D56A058E4D44D7CD47F2,
	LTDescr_U3CsetCanvasGroupAlphaU3Eb__100_1_m6EBBBBA42BCC9B435DB26932FCCD7DF904E3D31E,
	LTDescr_U3CsetCanvasColorU3Eb__101_0_mC7AB0F758514D92910F073FB38A41C395E6764E3,
	LTDescr_U3CsetCanvasColorU3Eb__101_1_m0DC35761F636C62A4B9A9D9B7C8EEFD9B30F3CA4,
	LTDescr_U3CsetCanvasMoveXU3Eb__102_0_m0E72A6C3F16FC694B6D54907490ECA5D6D359776,
	LTDescr_U3CsetCanvasMoveXU3Eb__102_1_m7FF531D0062E0803860A0CD456E6DD6AE30BD00D,
	LTDescr_U3CsetCanvasMoveYU3Eb__103_0_m4730D57FCDEA041D3F4111E45E34C15149C4BD4C,
	LTDescr_U3CsetCanvasMoveYU3Eb__103_1_m9CF415CA444CEBB9B5B6C01B337E1C4598B74A32,
	LTDescr_U3CsetCanvasMoveZU3Eb__104_0_mDCD6897BB64C355669F8EE9CFD8F2B386F830CA2,
	LTDescr_U3CsetCanvasMoveZU3Eb__104_1_m5B12F7E220DA96B6F05025A4BE1AFABDEEAFF853,
	LTDescr_U3CsetCanvasRotateAroundU3Eb__106_0_m7254F615E75865226968EA098B90FE904EE5B38B,
	LTDescr_U3CsetCanvasRotateAroundLocalU3Eb__107_0_m2C7BE76A27280304C47F1787199EA2790E10D6D6,
	LTDescr_U3CsetCanvasPlaySpriteU3Eb__108_0_m60117A734E7707143FD04D2FBEE6B75618B258DF,
	LTDescr_U3CsetCanvasPlaySpriteU3Eb__108_1_mAA1E46E79560810DA46AE8EA0BFA91EDAE3D2DA5,
	LTDescr_U3CsetCanvasMoveU3Eb__109_0_mABE1F4262412CA7E93AA4B71881FAE5B817FCF8D,
	LTDescr_U3CsetCanvasMoveU3Eb__109_1_mD190E0829649FEA47BAB95DED30B6EAB95C03ECF,
	LTDescr_U3CsetCanvasScaleU3Eb__110_0_mF68FF782BFAD2E93A65661953E683EADC9A52DA0,
	LTDescr_U3CsetCanvasScaleU3Eb__110_1_mCF2CCB10E46FC5F00061B020C857E7A35EF14E0D,
	LTDescr_U3CsetCanvasSizeDeltaU3Eb__111_0_m021163E83008DE7874346B03AE065037CDC34D3A,
	LTDescr_U3CsetCanvasSizeDeltaU3Eb__111_1_m9AA7511D4E1BF277C282F3E30388E82D0A9E1C0B,
	LTDescr_U3CsetMoveU3Eb__115_0_m5DDD8D43FEBB5099535893334FD76E1F0DAFD21B,
	LTDescr_U3CsetMoveU3Eb__115_1_m6B66D8C95AE011E8C641FA4AEF0245809F334399,
	LTDescr_U3CsetMoveLocalU3Eb__116_0_m7FE28EDDA5832CD6FF6BB2370DA6059DF23B2A1F,
	LTDescr_U3CsetMoveLocalU3Eb__116_1_m8D5A11FA82E103327A9ABD0CA5BB397952CD260C,
	LTDescr_U3CsetMoveToTransformU3Eb__117_0_mCE51DD0DF5028FDE555344C8FF602CD98716C33B,
	LTDescr_U3CsetMoveToTransformU3Eb__117_1_mACD7BEE7BA65E5DCB5B234EB3A31D9D8C3ED7FA8,
	LTDescr_U3CsetRotateU3Eb__118_0_m3ECAD3ACBA191CD5E6FBB67BB3E3AAEDA454B4D3,
	LTDescr_U3CsetRotateU3Eb__118_1_mEB2249D09D0E69826AE6036FD448AF92D77B47D7,
	LTDescr_U3CsetRotateLocalU3Eb__119_0_m2AA0D5D9B5334CEC12D6E8670332C4BEEB5CD7DB,
	LTDescr_U3CsetRotateLocalU3Eb__119_1_m64E3FC9E52EE3EB30AA9260408C3D02F24A34064,
	LTDescr_U3CsetScaleU3Eb__120_0_m4528B485CAF7246E73A7E602504675D4B11A44B7,
	LTDescr_U3CsetScaleU3Eb__120_1_m4A1D3D78C772991EC422F7D2D29EF4D069E39C78,
	LTDescr_U3CsetGUIMoveU3Eb__121_0_m4AD272AF144CD468B048F733B93D1DFC08527804,
	LTDescr_U3CsetGUIMoveU3Eb__121_1_mDBDABB8CB0F1D7CC05BF861826BD23B1A5CF68B3,
	LTDescr_U3CsetGUIMoveMarginU3Eb__122_0_m92AE989BFBF68D262DDFD12EB9DBBE05BDCE7AB1,
	LTDescr_U3CsetGUIMoveMarginU3Eb__122_1_mA60DFD7F27C11D0B55761F7A0936D8056ED09382,
	LTDescr_U3CsetGUIScaleU3Eb__123_0_m5A0FC254BDC374AACB5FED5922852DD294CBA77C,
	LTDescr_U3CsetGUIScaleU3Eb__123_1_m01CD6A1BE962B34E940B420723AE967C9AF4B248,
	LTDescr_U3CsetGUIAlphaU3Eb__124_0_m584C608C8B326C3B0401CAA8C696F8ABCF95DD38,
	LTDescr_U3CsetGUIAlphaU3Eb__124_1_mF0CAD09EF53473D82DF70172C27DA602272E3361,
	LTDescr_U3CsetGUIRotateU3Eb__125_0_m8F9A74633240B6083A3C1DB7EA24F762D40589C9,
	LTDescr_U3CsetGUIRotateU3Eb__125_1_m43CF134D7443CAA354F69A7031CE9F2574CE4DA5,
	LTDescr_U3CsetDelayedSoundU3Eb__126_0_mEF9FE367DCCC3DDE76DA47246D17F221C0F4D93F,
	EaseTypeDelegate__ctor_mCD18183DC7DD02010D779AB3969F9D2B6BDAEA28,
	EaseTypeDelegate_Invoke_mC562BC4CD7B5786B26A9D7F780BC43F1B2F601AB,
	EaseTypeDelegate_BeginInvoke_m8B1407D35ABAD4BA8A49AA4CE44736F90B5D834F,
	EaseTypeDelegate_EndInvoke_m275A58EADF116EBDD21996509E57703CCC10C75A,
	ActionMethodDelegate__ctor_m86D0105115F69AEC95F9DB9542A3CB52854649E6,
	ActionMethodDelegate_Invoke_m4C86E95A515A25BE5ADB85D4889AAEDAA157173A,
	ActionMethodDelegate_BeginInvoke_m1B3C88E85A1CF2B1CE645AC9594565046D26BE0B,
	ActionMethodDelegate_EndInvoke_m7B0A147CA95FB6A16463DFA4C30DE37053C5D72F,
	U3CU3Ec__cctor_mACE0E27AEF258161C83D9EA95E78DBD6B559A5EB,
	U3CU3Ec__ctor_mB934C02FF6DB889EA42A5581DCA0B5CEDBCC5903,
	U3CU3Ec_U3CsetCallbackU3Eb__113_0_m7FC51BAA4B25A66AAAE19427E427E31C01E7EF0D,
	U3CU3Ec_U3CsetValue3U3Eb__114_0_mF93F5C6F0B095D8B1EE239BE2A475A16E713A012,
	LTDescrOptional_get_toTrans_mB4912C34FDE4BB3CE94B86FDE3D7D8C0DE951773,
	LTDescrOptional_set_toTrans_m4C747BFF6573BF8C2484AF01C3FCCC65E8A54FB9,
	LTDescrOptional_get_point_mB79A8D97B381FE3122B1549DBB3527F9691A1156,
	LTDescrOptional_set_point_mD7AE42D3184ED0D66545DC3A98B5CB5759DF8627,
	LTDescrOptional_get_axis_m546D8ADD4003D9B69F7EC4A243008064AC42DB20,
	LTDescrOptional_set_axis_m17A070B698AFE6D066C10EEF1A86DDEABC0A13B0,
	LTDescrOptional_get_lastVal_mD84B4C7F0D87E853D6EB9786261068DA74031DF8,
	LTDescrOptional_set_lastVal_m1B2DC6C4CE540BA60BD467F4394CEF18409B820C,
	LTDescrOptional_get_origRotation_m1B98DB0AACC7AF940C43DC29DF7A61FE6E49736E,
	LTDescrOptional_set_origRotation_m6E9E885A6259D7DE7FD1D398CABD7F7CB1751DE4,
	LTDescrOptional_get_path_m20A38F266BEEDD6370F8F54B2FA58C7262808AC5,
	LTDescrOptional_set_path_m10D4A506CACD00E0951E6E5E41003DE99C72DA64,
	LTDescrOptional_get_spline_m32BE3A98427C2A4FBE40E1BAEDEC2246ABD3D4F2,
	LTDescrOptional_set_spline_m5FF9921610A1F8C38E004CF9129F185BC7FDD708,
	LTDescrOptional_get_ltRect_mDBE34D781428B8DD07A8F25AC32B9B0932B62BE7,
	LTDescrOptional_set_ltRect_m273BF61CF793266842167003657D7083E62B655A,
	LTDescrOptional_get_onUpdateFloat_m8573CE5DFEFBA6E350AF056485B265FF59E8072B,
	LTDescrOptional_set_onUpdateFloat_mC6B9DD9721D66C62B4F196BFB0AC6EAFFD98A10F,
	LTDescrOptional_get_onUpdateFloatRatio_m6757E471D3ADDB9202062289E6EFAD3B61B2C7F4,
	LTDescrOptional_set_onUpdateFloatRatio_m0FBA91E49D4D592E2CAA88BF2963B2B5737ACEF7,
	LTDescrOptional_get_onUpdateFloatObject_m8A282C0573D32731C92CA8A4E352382CE73E4839,
	LTDescrOptional_set_onUpdateFloatObject_mF14622BEBB5CCF990BC0DC9622CE2EE037F364C5,
	LTDescrOptional_get_onUpdateVector2_m2692F9A7698099312B1956EF89ADC677E156325A,
	LTDescrOptional_set_onUpdateVector2_mC9D4DD0709714E0D052A2A6E475099B146375331,
	LTDescrOptional_get_onUpdateVector3_m1A96786092458EFAEAB22467DC6AA01F2F34247A,
	LTDescrOptional_set_onUpdateVector3_m91295D4F19AFF3080F6B44A071118CCDB52F288B,
	LTDescrOptional_get_onUpdateVector3Object_m2CAC540C86A54F7744AE39443FB4054066E3DE02,
	LTDescrOptional_set_onUpdateVector3Object_m2ECE95CB5D7DFE5E9DEDB9F3B03BEF39D4000B77,
	LTDescrOptional_get_onUpdateColor_m78FBDE4D9B8856EBE7EBF42709CFF81E127B9A43,
	LTDescrOptional_set_onUpdateColor_m7D77082D84B3831810A825A0628CE556012AAA2B,
	LTDescrOptional_get_onUpdateColorObject_m45EF15CE7E1F771A04B6E9374323759A7A021C62,
	LTDescrOptional_set_onUpdateColorObject_m25F9B89775D1630387BEDF4DC8B2F88F8E4842A0,
	LTDescrOptional_get_onComplete_m89C63A691247B03AF894749D3F32987E7994CFA8,
	LTDescrOptional_set_onComplete_m71A16E08537D67C75C706DD0EB88B7954A0CFCAF,
	LTDescrOptional_get_onCompleteObject_m50BB3E137E79340957CEE096A24A3635ACD91DEA,
	LTDescrOptional_set_onCompleteObject_m70FA4651F7E2CA76267B09C3EFBEA8AFE62F95E3,
	LTDescrOptional_get_onCompleteParam_mD546B9E0E31E70CEF0514DFB486D49E4BBE9D120,
	LTDescrOptional_set_onCompleteParam_mE4F5C590CFA4FF8204FE0C4722BF7EEBC1F4D7C7,
	LTDescrOptional_get_onUpdateParam_m8568043E9421D14172B62712FF0519738F0E8D88,
	LTDescrOptional_set_onUpdateParam_m415D9E73546338141E97EBEB8587C8A6F00C18E6,
	LTDescrOptional_get_onStart_m67196E7D6B56A68A3D7A5BAC7201CB5D79575EEF,
	LTDescrOptional_set_onStart_mFC780FBC4B2363070A9395BAA7693ACBFA3C75F8,
	LTDescrOptional_reset_mBC39AAD50AF833763A2547D2BB295CCE04F8CDBB,
	LTDescrOptional_callOnUpdate_mCD2B766E5358969C288B360A409B741962E90A9E,
	LTDescrOptional__ctor_m31327367F73AF26F1F0513529F56B4D4527F1CD7,
	LTSeq_get_id_m7BB843F84403ED4D5D7816EAB92F64D64AA16528,
	LTSeq_reset_m81F08444C11BCB0DA11DA2655C967A31B0F48E43,
	LTSeq_init_m7666AE5720766D32C80F45C08B15F29BDF926FCA,
	LTSeq_addOn_m92322FBE7CE53DC475ECF67BD47ED0A0FB37471A,
	LTSeq_addPreviousDelays_mA44FF52A17DF8B8F70385938E809B922AE96D914,
	LTSeq_append_m9CCA2E2A2FEC8BB783E6973C5259009DE1F2CA93,
	LTSeq_append_m918D5B42DF7E78F1D632BDFB559566153BC03151,
	LTSeq_append_m771276B4D19FF6E029D920552CA35785C06166DC,
	LTSeq_append_m7B1CF4D2CEE5FEC4FD64BE97123E3466981969C9,
	LTSeq_append_m38B101D631E233A3EA115D8102FECE3A9F8501D0,
	LTSeq_append_m16B953CD94139BC1C75791A5D461C744EA4ECA64,
	LTSeq_insert_m557E13DE79F5FDF0DCC9746AA15307FED3DA9F0A,
	LTSeq_setScale_m01A80A5EF1549289CCFE8F7E7176A5B669781699,
	LTSeq_setScaleRecursive_mA815906D527D73895B508205E6DF519C26560C7F,
	LTSeq_reverse_m626178075D66F45F66D451A95BCFFDBCAD95E04C,
	LTSeq__ctor_m0C853ED35E39038B9153A5E212A11D5366E3EB01,
	LeanAudioStream__ctor_m767420411C73925D7260141B6AC141C332340F57,
	LeanAudioStream_OnAudioRead_mB0B822C29AF57CD64964495BC47B2D552C9A61EE,
	LeanAudioStream_OnAudioSetPosition_m84D20E5E2E90A883DA74466C176BD12EF300CFD7,
	LeanAudio_options_m24F4466E9FF88143F014E1341ECAA03A0BD1DE16,
	LeanAudio_createAudioStream_mF0B2E4AE61E77E5E4F5BB229BB17EA0010EED167,
	LeanAudio_createAudio_m741D84D297AF8413E9A6890FA81CD7F288A303C7,
	LeanAudio_createAudioWave_m4368517E2416AF360A06777597C9A3142BA5FC00,
	LeanAudio_createAudioFromWave_m1D0C1A2142307BF2378F25C76C42F61265D5ED21,
	LeanAudio_OnAudioSetPosition_m7FBC95FBFCA93F4A878761356FFD1B4FB716331C,
	LeanAudio_generateAudioFromCurve_mD5AE758FCAB5F326FECD85B37A313678B9D8929B,
	LeanAudio_play_m96190E0C8E00011FC6A98EFEBDFAEE2E3C06AB9E,
	LeanAudio_play_m7F543AE840832C2D4FC88AA184C605A892F1C030,
	LeanAudio_play_m6CD5A5DC70FD339BDD5BEDC9243FACA5C3186451,
	LeanAudio_play_mBC8A61F39D8ECDCA09F458FB1028945AC35DB0D2,
	LeanAudio_playClipAt_mC86BC94850F21DBC4B9C831CC666FEBC86A2C510,
	LeanAudio_printOutAudioClip_m22003A70913081D802CBF76538C92F929CD8D857,
	LeanAudio__ctor_m492D6C2E9108D90D229F0811C3B185DFD891907A,
	LeanAudio__cctor_m31067C511A043987DBFED412C6486A1D4D2CBF53,
	LeanAudioOptions__ctor_m29A189A1499462B631EBD724FF961389527A8EA2,
	LeanAudioOptions_setFrequency_m85E50359A5904A370955AFF52AC3D88CC01EBBC5,
	LeanAudioOptions_setVibrato_mF64FB8E4DD576D90776F76B01B753247D99B49DB,
	LeanAudioOptions_setWaveSine_m76F10085FDCE754A60760BB1542537F9DA46034F,
	LeanAudioOptions_setWaveSquare_mF8F08EACD82E37E15137F5F1C785DD0D671AF42E,
	LeanAudioOptions_setWaveSawtooth_m27624D2920970C41AD3CE47DE408FE9C7EEA530E,
	LeanAudioOptions_setWaveNoise_m55FF493A787F18D8692DF85866D055354AE1A2B0,
	LeanAudioOptions_setWaveStyle_mEB0CA095133249C1947E51FA9FBE9657F7A784CF,
	LeanAudioOptions_setWaveNoiseScale_m68B9C76F8DD866150B060285CF705D4ECB5C533C,
	LeanAudioOptions_setWaveNoiseInfluence_m7CBBEC51092269C15133C3B53D1828ACC874AF56,
	LeanSmooth_damp_mAFCFAA57FEE313048C9AF8C5F16FC2F5E723D0FF,
	LeanSmooth_damp_m7CCE1AB623BF22468A59B97CC2D161682476168E,
	LeanSmooth_damp_m9AA68F6F1D20FF61F338BA122A374CB0F10D6EFE,
	LeanSmooth_spring_m314389AD151BF197F11CB50DFEC3F05396E189AD,
	LeanSmooth_spring_mAC93690E733A39072CB37D5BFF1239414F98E1EF,
	LeanSmooth_spring_mC13A607AA221D50EB1AB72CE0C5D314F2748E946,
	LeanSmooth_linear_mCAA760AD54BCCFD8A3C67631437C528F84806A11,
	LeanSmooth_linear_mE9F99D1FD99A7D58A0F81A3A39EA4D994D55BB6D,
	LeanSmooth_linear_mD4AAD42ECD1AFD102F576A52116C57CDEEFB73E7,
	LeanSmooth_bounceOut_mD1B8F20CB910CD8609D9F5988472CCC83B98DE6A,
	LeanSmooth_bounceOut_m39C1F55CD36843F594D1C58D067696AD294C4812,
	LeanSmooth_bounceOut_mF3E43703A80AB9D705575A9D803AD2E8AF3722F1,
	LeanSmooth__ctor_mA939A643B1463547170CECB7FCC6AF013FF0DCAA,
	LeanTester_Start_m9EEED491D1FBBF5A677178E6477E7B68491F1517,
	LeanTester_timeoutCheck_m59067A5600F0D2434CE9B7EDAC062829C85D5C00,
	LeanTester__ctor_mBB9C14D1DDB7AA3F591D6B0040EEA74F032EB1F7,
	U3CtimeoutCheckU3Ed__2__ctor_mACFA1EA79EDBEA235C7474D1BA3E71804A23FEB8,
	U3CtimeoutCheckU3Ed__2_System_IDisposable_Dispose_m0AF241B0BA496E508C41A00A92ACCB507F764F3B,
	U3CtimeoutCheckU3Ed__2_MoveNext_mF2666B91C53937A9B6CFBCDB7E111B6E74685643,
	U3CtimeoutCheckU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF1EF5FF8E038CD04BB438F282E437032A52DF973,
	U3CtimeoutCheckU3Ed__2_System_Collections_IEnumerator_Reset_m377EF3F9C3E2B3A09D749A0A347B92595F0D756D,
	U3CtimeoutCheckU3Ed__2_System_Collections_IEnumerator_get_Current_m275B7C4BFE741FFBA1D7CE3E422460E5BD15EFF6,
	LeanTest_debug_m185769356E43C51F1A057B12DC7F0E6970CA05D2,
	LeanTest_expect_m697408A02255410088277BB6C41F73FFB789458B,
	LeanTest_padRight_m6DEC7C60D840B9D4F35BDFD3A27411551F182CF2,
	LeanTest_printOutLength_mE439776B28BB9737553D43E95254DD2A79676EFF,
	LeanTest_formatBC_mCE1B1751EF0B35A008C32A43AEBACA76B2FF0272,
	LeanTest_formatB_mDF309FD724B541A97D931A28FE32B9F3245DA966,
	LeanTest_formatC_m10095A4C3AEB8A45468A8251101CE25BE1643F34,
	LeanTest_overview_mFBDD0E748CC3DF470B668293185D2637F2ACF17F,
	LeanTest__ctor_m2534B2F8BA29F28EF5D26482735FE6D93B229F6E,
	LeanTest__cctor_mE75EB5D6AF852A6D0B480FCC70D1F9958AFE0943,
	LeanTween_init_m2257D056F7462641D1316804B27DB2EF03C54B0B,
	LeanTween_get_maxSearch_m76F5A3551B333FEAD6918DC0795238CEA51E8764,
	LeanTween_get_maxSimulataneousTweens_m38608BBD6E41F3B66FF348DE652F2EA6F1BEBF45,
	LeanTween_get_tweensRunning_mDD39747E471B1CCDF83F6F79B5CFA224E47D3842,
	LeanTween_init_m4CE7872C3DBD46FF7252C60DA96FD99FE9A071C8,
	LeanTween_init_m641164BCF025E634EFBD06151E55753939DB6F13,
	LeanTween_reset_mC778C2B33DA945C2D7FC099D6B0EDE24A98A78AD,
	LeanTween_Update_m4E96DD5D854DBD4CDF76759B63786DDD8739BF08,
	LeanTween_onLevelWasLoaded54_mE6FF28F42BD62CC30963F9D80C2B26FFF0B3C5B0,
	LeanTween_internalOnLevelWasLoaded_m14ED1FA1C1214624DC4A780E737D2FFE90A3B0AB,
	LeanTween_update_m2BF953CB1B4904B57BC6A43058FDA4BE52B57688,
	LeanTween_removeTween_mD1216980A45C032C76FBD7D66BD3ECD81F992D84,
	LeanTween_removeTween_mEF159BEAAC6DC0EEAA016966749C762D3EE88A33,
	LeanTween_add_mF9434A6D817FB5B901ED97889690158C6E99AD4E,
	LeanTween_closestRot_m45089460836416CF978CED05F04748378298675C,
	LeanTween_cancelAll_m017FC44A7C2778837326A7518192A881D4F39FD2,
	LeanTween_cancelAll_mC78FDFE4A1045C23D35C9447D3D71FFBDA2837CE,
	LeanTween_cancel_m8F49EAB6908B4BE0715C0D4160C51516DCDD2823,
	LeanTween_cancel_m28B082D5434EBF590F33D552278E4969614E35E6,
	LeanTween_cancel_mE6AF5A1A673FCA3C9EAFBAE4C15EBE5ED42B7710,
	LeanTween_cancel_mF3E2B961A0AB5C23CB2EB64B557AC718BF931F9C,
	LeanTween_cancel_mE5CB54E734F2E1DD02E9979D64E163400677AA7B,
	LeanTween_cancel_m8F235169322AEA85E23C774745EED07BD83D344E,
	LeanTween_cancel_m54BD97AD0C8C50A9EE5645108B28F7E25104EA01,
	LeanTween_descr_m0686304AC8834008456087DDB221577D87246C90,
	LeanTween_description_mCF024DD0005701DAC6CFDDFCAC0FCBF66C613B38,
	LeanTween_descriptions_m2ADDBCE0DFC78640128B7224DD86FE6C3938D76C,
	LeanTween_pause_mAF75D7E636CB0D02F8D515FF76C23D99B8B28FB1,
	LeanTween_pause_mEE3D7E0190BB1879C5E4E40A34E22146B867B2B0,
	LeanTween_pause_m28FA361A74060F3CF6CFF07EA470DEB5871ABFFF,
	LeanTween_pauseAll_mBC633AE2F834B3E89A97848EBE21C2CF24E04214,
	LeanTween_resumeAll_m933FBC788EED5D67979F160369D98BEF44CCB435,
	LeanTween_resume_m1940BE978DB7EC3606D92C181FFE10A4570043E2,
	LeanTween_resume_mB671FD12433254652D7A6F1F2E8B5A924EE8665C,
	LeanTween_resume_mB910424A52CA71E88F376EBC8BD7DAC8BA638F3A,
	LeanTween_isPaused_m36EF7ACA58D5CF19D261ECC2740D3BABE292AE17,
	LeanTween_isPaused_m8433AB793191572FC80B287A05951CCA46D60415,
	LeanTween_isPaused_mCBFAAB19270CC1E95412B9DB3F5FCE12F56B64C2,
	LeanTween_isTweening_m7D2702558144653FEF2A2994BFFDB95BB547D76E,
	LeanTween_isTweening_m0DD0D935F7AEC656A1397DD8DFFADA8509ACCBD1,
	LeanTween_isTweening_m8A9E15F4297CE4D50C54540646F91DEE0767071C,
	LeanTween_isTweening_mD1EC7E088742B5EB1A698E9C82DDC9F4AF2F831A,
	LeanTween_drawBezierPath_m140E21617AD80CA0F665DA5A74E8AB641A5C3A41,
	LeanTween_logError_m941ECF9F4DA921ACE1845B97FE0C814F9E26B1CA,
	LeanTween_options_mC569B03E642CE27936B5ADF511ACE67C40E3EFD6,
	LeanTween_options_m859EFA1A4C3BE0E937966B8F5D902F798B60B2C1,
	LeanTween_get_tweenEmpty_mB1509CF6AE8FAB5591F914CBC1CC1DD5C7447F08,
	LeanTween_pushNewTween_m4C8A3AFE33EDDBCEF0FAFB6D9DE68BE6D930014D,
	LeanTween_play_m8F84314D3491001A540FA92F7A4F0B1A66ABC010,
	LeanTween_sequence_m8FE5B82BEF69D9AF66D54E849587B4CF933E34A5,
	LeanTween_alpha_m9D8A4A0F46CBF56AA0008446BCE391A124BDE913,
	LeanTween_alpha_mCE5E7D21CF2E2687DD1CFB80A9EE40E1E2EBCC0C,
	LeanTween_textAlpha_m31154DC1D5392385E4318CE77677AC4E29CF99B0,
	LeanTween_alphaText_mFD7A2FD730E1BA7638F985A93CE91B5EA652C126,
	LeanTween_alphaCanvas_m8BCDB0DB0CAE28F75A7DDF79FA6D62FC1B3AD513,
	LeanTween_alphaVertex_m94B9F32105B2C406F41E8D014FD6E5E235EFA506,
	LeanTween_color_mB591490EB11BF6AAB3D9C823844CD51A54CB54AF,
	LeanTween_textColor_m4BCEDC9A9BAB880E6C4440833CF36C2ED3101343,
	LeanTween_colorText_m34F5C1169E22FE097A19CC6B85377C738D73B937,
	LeanTween_delayedCall_mF74ECC6AC349466F55E56C4625DB67892553A59A,
	LeanTween_delayedCall_mF0264F6DCD013BFA7483D1399C04DA37502DC729,
	LeanTween_delayedCall_m537234980306375FFF55F428FE0F5AF5F35AD1CB,
	LeanTween_delayedCall_m15A55B8039CD7A86856CE10FF15AC9F99D8D8EE4,
	LeanTween_destroyAfter_m8EBFD0632BEE8F6EB0280D36C85CE24414C0FF15,
	LeanTween_move_m78D08B26807623C822B81CFA60DAC960DDBAC377,
	LeanTween_move_m1E331CB112535FB9EA90E55461A2D77D5260B530,
	LeanTween_move_m44271CB95A4B41D5EB76C4D83B419135A6EF33EC,
	LeanTween_move_mFEDFA180C09AB9A50941E5F63CD8670356C67ED3,
	LeanTween_move_mAE5F83E88AABED48945E61924B0427C1A9D2D2B7,
	LeanTween_moveSpline_m5935DB5E37D36935FF3FBDA2C498F927FB7F5570,
	LeanTween_moveSpline_mB64C328098A521A9E7BB2EEAF0258238DF2DF712,
	LeanTween_moveSplineLocal_m280D26BA7DE193E5F3EBB523ACEE6E44FF0978B7,
	LeanTween_move_m5E61C62A0F22777D370EB7CF2E6C0AC2EF3EB8B5,
	LeanTween_moveMargin_mCD8BA91FB1CB5762858AB694C374525B40570658,
	LeanTween_moveX_m3345DDA9D080FF581C38E0B92B7B2A0389A61DB3,
	LeanTween_moveY_mF3562A335C9D8A19192A1D05BBBB062D5696E01F,
	LeanTween_moveZ_m1D2828EEFBA366465AE916017B04BD730DB02918,
	LeanTween_moveLocal_m5EA4899950A5A6E5244A472D5B0095A456A3E828,
	LeanTween_moveLocal_m5EFEFD16EBF5E4EFC333418953F72DA504E191A0,
	LeanTween_moveLocalX_m6CE239F4E86EE5439B2D86FB0163306EC152DAD4,
	LeanTween_moveLocalY_m2EBE679F58F63B2D3BDA557474CD0C99B6AB8C81,
	LeanTween_moveLocalZ_mCCAE8DEB09113FABEE3D66031E1B25C2B8BD318B,
	LeanTween_moveLocal_m4D6E9C2DFFB9D9BF77A70A417CEDEB22D7B0689C,
	LeanTween_moveLocal_m3AB20706D4DAFD80EB42795B594331820B1C41E2,
	LeanTween_move_mAF8F6C1C95F4A9E38C99E9471889EF3D6F26B21B,
	LeanTween_rotate_mFE6F0AFD90CF08BC42B97AB676D30F5D6B685BB1,
	LeanTween_rotate_mCDB520E4B0ACB36B9EE004C8CF8B3C0A5EC0D9E2,
	LeanTween_rotateLocal_m5B5E5981164221594D9D4FF15C84D7B9FB81C067,
	LeanTween_rotateX_mCBA79F277E3512C992772E15E9298BEAFAA046D0,
	LeanTween_rotateY_m14E1525768E32FDF7A2378B1C8AF023D1C0B1C24,
	LeanTween_rotateZ_mA0AD8A6E7771AB656243CD3669EBDB0E38CA2408,
	LeanTween_rotateAround_m588F9185E90CC14CAB0B0EB6C3D575CF7E7C285E,
	LeanTween_rotateAroundLocal_mC869325E94AD6A0B95314D081F29B976CA91A81B,
	LeanTween_scale_m684CBC818ED1F1ED8C50D1BD0F49495CACC0067C,
	LeanTween_scale_m16AF41D6C5EB894246FDE21B3B9B90EADE1949AC,
	LeanTween_scaleX_m4DCF8A388D1FE2AF9C40F67DDB77B958DE1F0220,
	LeanTween_scaleY_m011130E1548F777DC187AEF2A88291CDE924851B,
	LeanTween_scaleZ_m2CF069E5E4EC0E4BC040DBA5C759E1A401A67481,
	LeanTween_value_m16722DEDC681F28C188D0F4557F0BF9B6A1488AF,
	LeanTween_value_m85D0A108DCD42E6A1576B1CC53D2F9D00B0E47FC,
	LeanTween_value_m6B91F8AA266FFF0C593FF90E7A6BE3D1960B84E9,
	LeanTween_value_m45BC8CC34DB8AE45C89048BAAAA016A4C6F0D526,
	LeanTween_value_m91EB5E56B4A910CDE7D3F1CD5EFA1E352FEC04D8,
	LeanTween_value_mAC251E42C465A03544154ADD2059E8808E208F4A,
	LeanTween_value_mDA02C25B8315D819999BA319C0E183E3534BC0D9,
	LeanTween_value_mEA929F405968830C7B82A604B40F5810DA0274B8,
	LeanTween_value_m3F5B71A925784CD187198B3AB14CDB08351AE715,
	LeanTween_value_m214501A86FB206A4A18BE7C373F67B1E3BF90E17,
	LeanTween_value_mEE3F62AA3FED7E7FFAF54C8CB689045550F31E2B,
	LeanTween_value_m062FD40F3682DD46D70A1FD5740CE32C421BE536,
	LeanTween_delayedSound_mEE95C5C431F6AE155BC4B5CE22679353E5BC5BCA,
	LeanTween_delayedSound_mD975E34F6E1D2545468F716845C850F049D1C430,
	LeanTween_move_m255964EF3EF82F6F371AFCAF934CDBE555E31CA4,
	LeanTween_moveX_m968EB608281BDCD184F6B71F22A36B1BD0B32FF5,
	LeanTween_moveY_m8B0C0BD32AECB7A9F1FE4287A1303F8A5BAA225A,
	LeanTween_moveZ_m2E0856F50D20B11039FB477A7F28124713884DC3,
	LeanTween_rotate_m2D1DFCC15465567BB121704213FAA70FD5C336B6,
	LeanTween_rotate_m76084F742248BE37F7A6B458BB34CA78DCFC36F5,
	LeanTween_rotateAround_m3577D6688BDC54193F15C71894E6E55A69006630,
	LeanTween_rotateAroundLocal_mD02923DFFD7FAD7DCBF034734D058E7C99EB8D44,
	LeanTween_scale_mC4B678FE9E438AD23893A27F09E3C007F2EBEABA,
	LeanTween_size_m28011A80D7FC887385A015C3F499A3F33C339E08,
	LeanTween_alpha_mC673E74E2397289FEC358CEFC8E691FF25680886,
	LeanTween_color_m173A290E75DEDA181F8F8BDEDF62C1B12550830B,
	LeanTween_tweenOnCurve_mCF1C2DBF18A825484F5213591A74D0406108BE78,
	LeanTween_tweenOnCurveVector_m2F3889ED1C5209DCB8DF39AAAE310848E3AEB882,
	LeanTween_easeOutQuadOpt_mA9D54EA2DFA885DCA918FF829C7EAFED0567F0C1,
	LeanTween_easeInQuadOpt_mA07BD22C4B17A4FE3B44BC927D18C6E14036811C,
	LeanTween_easeInOutQuadOpt_m5CB73FF539B12D06CD85E6DC09AF281E3FEC2D86,
	LeanTween_easeInOutQuadOpt_mE09DD740EAEC7F818C4AC10178F11B6587DDAFF0,
	LeanTween_linear_mC13897A14BF6CAD2E64BD36644CF8D9AF8EE2F0C,
	LeanTween_clerp_mF38CC191CF1F93FBA5FA125C1BBD362A8F492574,
	LeanTween_spring_mE01745BEC87C32BDF7EA6A91D0B321BADF5884AB,
	LeanTween_easeInQuad_mFADDBAF1404502B07D54D73A5E3E6988D8FA3D24,
	LeanTween_easeOutQuad_mAC0C6A6D659FCDE5636A2AFC5D622B99B61808C7,
	LeanTween_easeInOutQuad_m71AA1E657CE443D018036A98715420BF5925E7E6,
	LeanTween_easeInOutQuadOpt2_m292D5F817A9125E0415C3526F931AA81432A3B09,
	LeanTween_easeInCubic_m3D273E2955A78F68A0F2A9032C2A67E0BAEB6F87,
	LeanTween_easeOutCubic_m2DB944476129350B6D0700C846E3063346F07090,
	LeanTween_easeInOutCubic_m74507BCA5B56F0F295BA3C93D744D332B4B5CC53,
	LeanTween_easeInQuart_m48DE80F5CC0478F91AD6C3F63879747A0C75D1D9,
	LeanTween_easeOutQuart_m609DCDE1826DB79CB2B9985CD60D1B239A984772,
	LeanTween_easeInOutQuart_m0F5D0D7285A20C6D860CF36F25D5D99D59900B33,
	LeanTween_easeInQuint_mDB55F438F15637CA989A645BE894AAF9A004B647,
	LeanTween_easeOutQuint_mBB803F45CF6EE94AC581337373184BC955468FB2,
	LeanTween_easeInOutQuint_m63E5EFF41668EB890D1A86E424189FCEF9F7D4E2,
	LeanTween_easeInSine_m04E033CDA7DEDAD91402A67441771684E9DF9585,
	LeanTween_easeOutSine_mAD08688F562F7B020B432B9F176BFC4E7246E9CA,
	LeanTween_easeInOutSine_m19A665CD11C2430CE4D6B2EE7E49D5E4D69B255B,
	LeanTween_easeInExpo_m5849B8DA222D18020D5DF8CF7CB21EFF6CAF918A,
	LeanTween_easeOutExpo_m8047C00ED545EBB9A49539302273CBD72B99D0DB,
	LeanTween_easeInOutExpo_mC75B771143EC08818C36CD0F1F7A3E5C94ED27DE,
	LeanTween_easeInCirc_m1B31C81F16B5E9297D21C701D372DBD548319883,
	LeanTween_easeOutCirc_m68F5077C75EA1933F562B96FA0EAF3A114D87EE6,
	LeanTween_easeInOutCirc_m76125DC5F83F40B9931F8114B87A44962EADC567,
	LeanTween_easeInBounce_mE3DF8975A300367183C2C926938E6405B33112A5,
	LeanTween_easeOutBounce_m27698CDAABF99F8EDC7D6F6D6586433A9D12970E,
	LeanTween_easeInOutBounce_m9450C71BE4DC36B9E7C7337E89D1BB131E5E0BFF,
	LeanTween_easeInBack_m0D8EFBAB1984C81F9A26AC9936DF26444E8CFDCD,
	LeanTween_easeOutBack_m10B1A82299EE98AEF7C601D14A4D74F613849520,
	LeanTween_easeInOutBack_mA896015E78E55F1FBB1F29D8AA0359A74AC1E284,
	LeanTween_easeInElastic_m0A27227BD0603697DA973D517017937FF956C483,
	LeanTween_easeOutElastic_mC63F462458A6CBE208E739E5F92B4D21A16E2ED4,
	LeanTween_easeInOutElastic_m62A56706E9907A81A5459B47709F75C1947E6473,
	LeanTween_followDamp_m5C9F35D1479612D8A617DA5EF9698BBA34BCFB5B,
	LeanTween_followSpring_m0897DA70DDD37F467E99B8E095157FCD30E02519,
	LeanTween_followBounceOut_m65AA5DB3E225B699DEF3111EF3FC5F33043D5F08,
	LeanTween_followLinear_mE9D8D28E7216EB21260AEF49DC344EB5075E36E0,
	LeanTween_addListener_m3F4A9293AEBEC7AB6896D092CCD808317C3DCD97,
	LeanTween_addListener_mD93323361477B8F8671D3E0C2261A56967E4BBED,
	LeanTween_removeListener_mB3A2221839B0C781D1597674FC5E0674D8C189FA,
	LeanTween_removeListener_mC6FD437AF58BC94743A0A978FAD970886CEF0CAE,
	LeanTween_removeListener_m2DECCE672B558063CCED79464E5401FFC922DCE4,
	LeanTween_dispatchEvent_m1B94CB1AE76D30A0629498DC3CF920C16C3C5B80,
	LeanTween_dispatchEvent_mA17A21F65DC5DFEA2312C5F1446BCD2A13D03C98,
	LeanTween__ctor_m0E530009EEDFEFE39A618092205A560791154327,
	LeanTween__cctor_m7C31F6E8304BED5D2568407206013A918B566DB8,
	U3CU3Ec__DisplayClass193_0__ctor_m42DC73265951A655C0448FF60C880316D9DE1A43,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__0_m191266906DD2277CDC3B46FF109CF35D23BBDAC7,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__1_m704758A390EF812C25A3A65988859C02AFBED1C4,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__2_mAA06748BE5AB364E8F927A9EE39224B64FD24398,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__3_mD56CFC8DF59426863F6189980E747D8413AA5CA0,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__4_m6D340E392A6FE3E0389103E0DDB77C9ED361D9ED,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__5_m0220EC49C37178D39C26609650CE24AAB28621FE,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__6_m2DC42525AE6AA25C9692CA049C80F6958918DB42,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__7_mE7F490B90125F2A8B3C5B91FF517537313A0FCE0,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__8_m007DC11D196C3911171092A96EFF8588C9CBF947,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__9_m26FE2854DC255FCB17A99976AF3B1C9674AF3DA5,
	U3CU3Ec__DisplayClass194_0__ctor_m7BE723A2115ACC48F87D807B5AD61D097D7B80CA,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__0_m96E779A4A9A701F3B635CFB3EBCC96458BD3FDC3,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__1_mACD73E450FC2E3CE551EDDAC93A2F52A45C5054C,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__2_m56BDFF78D7668526967A93EA96C4F427DC600683,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__3_m8F19785EA57FC703DDADF403EE7321F94343C995,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__4_mD7590F09F10CE901B14B9E7C2D59281C17BF6D45,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__5_m0173F460A2260824B38994CCB06B17F86A43BC00,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__6_mDF693219BF56597185AC7A86A83B41CCAE50995D,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__7_m02E8CAD9D7320D317F1291024AEC66A7BA1DF8BC,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__8_m2815FB0A57623C0A7694BEE29292D36748AF2FEF,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__9_m2249E4B791535178AF9B4005460FC55DC089E8CA,
	U3CU3Ec__DisplayClass195_0__ctor_mBBB552F5EA7C92143790A2E28ECAAFA48EC7B4D7,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__0_m85BF6FBF1E6DDC848ED2747320A820E6A16F69A9,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__1_m8C32BD1F9A6489144955B3C032D8EFB4884F2ED6,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__2_m3ADBC5765839ADC33E591B0E3F86D3BCEDD966D3,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__3_m7F6DABA8926F943AB02A6C527D916B95331FD3C9,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__4_mB8C14DF64A83794EB497687CD62EB8246F2F3039,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__5_mA48C1D48F29E8F11F15E727577E658F0CAC0DD46,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__6_m6915716A4F822A502028FEABC63C125E057402CD,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__7_mDDD6AF7E2634F4081FD3849FF6C18B87A3235250,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__8_m90947880CD99C1E7247F57A1AC77692D6FFBFE69,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__9_mB5FEE58DA881C5915A000A3B5988C3D58B578E29,
	U3CU3Ec__DisplayClass196_0__ctor_m53A3A91F675B0FEBE70D8A183F4AEE80BB562132,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__0_mFA5A42D63EE280D45A87226343CAED5E72B4259F,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__1_mA72FB34A781A70DE472941BE6213DAD4056A4156,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__2_m3FFAD56268BA1B8BA721A870C39B2EB3FED3AFFF,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__3_m95959A86F6834CCBCE7080772F0C215459296987,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__4_m17D9CC99AA674FE28A2596BAB8D7AF7EA0AC74F5,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__5_m8CAABA8DEEB326B0DFBA45714E4DE40FA69D2FB9,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__6_mDF3FFFFEFDD9AFFCA4883B15B5BA1C2C568B5238,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__7_mF835E156C518889CC37B7B18196CB66817995693,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__8_m20A928A64BB76244A147659349B4952FB2AEF119,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__9_m485F0716E3A701BA24C7BE51462D4081218137BC,
	LTUtility_reverse_m792C6EFC380420D2FD31BF47B331CBB50B650824,
	LTUtility__ctor_m480117B4F53FC8EF08F8FA640618FDFEB2DA5730,
	LTBezier__ctor_mDE430564C41DA6E441FF61F9075DA00688964D8D,
	LTBezier_map_mC8981CF95BB7EE3FACDC51DC93EE0A638EB7FCD4,
	LTBezier_bezierPoint_mB8ECD0DB38F58EEFB1FCEFA03F96C4F6A04E6B9B,
	LTBezier_point_m11756123BEE5DB1D4A3D97B8920CB6434DE229FC,
	LTBezierPath__ctor_mC14B0C13FB3A48E608456696A8CA14DB6A6B803A,
	LTBezierPath__ctor_m761CE6701D4AF322CDF34898E4A75EE3996EEF32,
	LTBezierPath_setPoints_m0326ECE7DAAF6A7FF84203D7C4DF88207765D100,
	LTBezierPath_get_distance_mCC5AFBA165F1DE82DD616DCCD6A1DF46D6DC2268,
	LTBezierPath_point_mE9E25CE7B83E1F6A9509A12EB82792D0F2AD08CF,
	LTBezierPath_place2d_m86914A18D26070FDB3601E3F4B0A9F337C71D57D,
	LTBezierPath_placeLocal2d_m3B554AF9CDDCAD494F07CFEAD024F82501474089,
	LTBezierPath_place_m92A56AFF394BB4A2B4997C09DC87CF712D67FB62,
	LTBezierPath_place_m089484993924A7E12BCFE5418BD9DA5AB802E664,
	LTBezierPath_placeLocal_m84ECB572C31B7DC9420B9E22745AF8E553EA8EBE,
	LTBezierPath_placeLocal_mD55D6F5C237A9A3E3CF585A38697083AD608A6C9,
	LTBezierPath_gizmoDraw_mF83D68461DAAD27D153D7C368B9A17CE33FFBA2C,
	LTBezierPath_ratioAtPoint_m143BC2A593074BDC7858B6861351A0D044EC38C3,
	LTSpline__ctor_m9551907DED1FDB0E2E0530EC1773BEB94364C936,
	LTSpline__ctor_m2F73C5AFFBC6626390D7B2FA507B202DD52225E2,
	LTSpline_init_m25191659D43D582146D1C29714BD5B7B01D6B5F8,
	LTSpline_map_mDCB7147639A49699ED42F125EF6A5D27CA9E1485,
	LTSpline_interp_m75A500E14151D59BC9F7240B4AB73190AAB1E598,
	LTSpline_ratioAtPoint_mC3196D78FBCD206F34E51D528EEB6D9DE5093B76,
	LTSpline_point_m84BC62A0BE5F6F90E7AE7632BF88AD5BB332E75D,
	LTSpline_place2d_mF79DEE3F7C5F54541047D267A2E88C681E9A0C52,
	LTSpline_placeLocal2d_m6BA2FF841EADD05FA6CFBAC808B8E7D6641D154B,
	LTSpline_place_mEC9C1AB39B6FAFC9D8F41CEE63391BCB2A577DD9,
	LTSpline_place_m2F73FDFFE8520633664738EA4D3E769154EC645E,
	LTSpline_placeLocal_m861F5C7F91F4A15BD04D1F2C66E196219AC2B10F,
	LTSpline_placeLocal_m8A2D1C6DA0C7BC6E9B61E6577998C6397000F388,
	LTSpline_gizmoDraw_mAADED3A63F49D610C035F71DC725D214895189B9,
	LTSpline_drawGizmo_mE14FCC2AE443D4F66CB45A64754557476051945A,
	LTSpline_drawGizmo_mFC2AB1F7539EB9239D2AC471B276CC5A380A6A5C,
	LTSpline_drawLine_m4CEECA0A218EA8067F860F48FAC4E6CDD27EB7E7,
	LTSpline_drawLinesGLLines_m248D051DAF686041049E3994F3F3FCA9434DB823,
	LTSpline_generateVectors_mECF369B072F107D79E9AD06DD4D7BBBF5E5716D3,
	LTSpline__cctor_mEB46040AD009E27CB5CF44A9F39148191983681E,
	LTRect__ctor_m99E599C0ECB67B47DEE7F91377ACB404E81FDD9C,
	LTRect__ctor_mA9DDB568C78410BF943CEC80A10FCC9FE8E7E0CB,
	LTRect__ctor_m3894896A04C8DE5CF36FEB67B77A9D51471AA5EE,
	LTRect__ctor_m3FE04D973AF8662CF993C5C7A99A8190AF660DFA,
	LTRect__ctor_m1FE9274A3C18C4E81E0E68C91DDC727712CC0005,
	LTRect_get_hasInitiliazed_mB71593427DCCF0404DEB52065AABFF1762158555,
	LTRect_get_id_mD85F4AFF11D30E3CFB7F39142F98EB798EBC2950,
	LTRect_setId_mFE916182B6874208861CD518BEE9F0C89EF7EF8D,
	LTRect_reset_m79849D25F50652D6BCE8CA8F3F329EE9511852DA,
	LTRect_resetForRotation_mADBFF61280065301AADF66876334779462287788,
	LTRect_get_x_m515C6A230B6B72CB215A27535B967428BD6DC3A7,
	LTRect_set_x_m2F416C9557DD5B8F51C00D0710A634CD1B25FEEE,
	LTRect_get_y_mA1414A3C465D24CCA90A9C73D6EEC35F7411C957,
	LTRect_set_y_m40A2235B9E00614E61213F394430C86962E9CA42,
	LTRect_get_width_m04A3F854E9585909A7948905CFFCC4C3AA726B60,
	LTRect_set_width_mC8D40CA7ECCEA4B3E2E3B0C0DFC573CD6A461888,
	LTRect_get_height_mB4F9445DF5CF2F82979B009EEF48A4F22634CE44,
	LTRect_set_height_m52D3E7ACF7372560DD7412E524171FBF34B2FBE2,
	LTRect_get_rect_m775DA6B41DFC9720EDBAA6ECD285058C4BE41505,
	LTRect_set_rect_m09C60F3DDB77909404D570E60060AEDD73BFD969,
	LTRect_setStyle_m5E5B4BC5ACD18F9ACC62171F440460CBB4B549F1,
	LTRect_setFontScaleToFit_mA9F65B0883725878887560F549C2F4820445CF3B,
	LTRect_setColor_mD5BD0C65451DDD14F2927F5BFA617DB93B9E2CC6,
	LTRect_setAlpha_m5F785B6E9D8C6DFD7CC751417A86959041F5D845,
	LTRect_setLabel_m87C389A819F76DBCEABE8968C2C357F43EEAC6DC,
	LTRect_setUseSimpleScale_m9C2401689E18E8B93BC6C9A95E781226E765E81E,
	LTRect_setUseSimpleScale_mF4509500DEBCBD33A6CBFE6F4B7C2BC4D75D845C,
	LTRect_setSizeByHeight_m002DF767AC9C9ACCF561B65C2809C32FE07D8759,
	LTRect_ToString_m1034234D679BFDD3FAEB6E69E031652F13022AA8,
	LTEvent__ctor_m6ABB897986AE37C647A526BA35CD577196C80053,
	LTGUI_init_m90D0DAAF20EA870C10FC394D34AE654788D6E9E3,
	LTGUI_initRectCheck_m01927ECCEAA6B39214CAEC2DA11C59C7D7A417DC,
	LTGUI_reset_m4AF33FEE1C4FFF585B61E00329512D26D9348405,
	LTGUI_update_m346A6915B0DA4CAE2E17FE892D39122992599AE0,
	LTGUI_checkOnScreen_m018B856681A029B796023FFC001610DB7C87497A,
	LTGUI_destroy_mF6818BEF1B1A6510F9F38B20B4D4ACA1E4BAECE4,
	LTGUI_destroyAll_m6C70120370B727440C087BC73ED1F62914431B5E,
	LTGUI_label_m568FA2530F19BB9D082825B7406336A90CA7BF19,
	LTGUI_label_mF0A3E4EA3BF86BFFAFF265B1E848418ADF91B4CA,
	LTGUI_texture_m18FCD865307F279E7E6A0CF29D3E68DA0125F09E,
	LTGUI_texture_mD8320410FA4A576CAC639511DAD9271A8C2D32EA,
	LTGUI_element_mB7414EFDA599B3627466CD84A505842398B3BDAD,
	LTGUI_hasNoOverlap_m8F6CBA73181394C020334257C98D15D73222A21B,
	LTGUI_pressedWithinRect_m6955BEF35822977E4C488F640E982C05624D2C87,
	LTGUI_checkWithinRect_m7BD673AE0509E6E36A939C2A15547FB1B35425DE,
	LTGUI_firstTouch_m59A232DAB90147A8D67F2636CF126E3868B749A3,
	LTGUI__ctor_m3DF0E374662C139E92CE25BD00208671EA425500,
	LTGUI__cctor_m4A1C16EF1B32B267816AE59E983E024BDF672F06,
	LeanTweenExt_LeanAlpha_m622BA3E2E95AA0C3ED769D14693452C71B464202,
	LeanTweenExt_LeanAlphaVertex_m276A64D30C7DBC2962D952F39164E74266A80805,
	LeanTweenExt_LeanAlpha_m5D4F86AB57DEAB1788098157DD3858155B41A026,
	LeanTweenExt_LeanAlpha_m80C6BB78F56CBCBAF562EA721D66F568AEB253E5,
	LeanTweenExt_LeanAlphaText_m2E879D95FD0A499D43742BC7E19C70FC00DB3C2F,
	LeanTweenExt_LeanCancel_mF59992A82C61655FB9C3C02CAA26A659B9DE3646,
	LeanTweenExt_LeanCancel_mD017D8DDC025544954D2E68BC260FC83C9469661,
	LeanTweenExt_LeanCancel_mC35E34FD1DF38362DA3970677A1A3745F02873E5,
	LeanTweenExt_LeanCancel_m17794FCEC25F233AC350724707F017F476BEA6B1,
	LeanTweenExt_LeanColor_m444318E458A875BB4CB2A0DD294D992CD93EBEFD,
	LeanTweenExt_LeanColorText_m400922DC92DE54BE07F329E4949145CF60571731,
	LeanTweenExt_LeanDelayedCall_m2377F6B8E51CB5CDEA727E0ECB5719E78E1E6DC8,
	LeanTweenExt_LeanDelayedCall_m813456BD5A6C3BEF150219E1FB7FFECF3CFDB8FB,
	LeanTweenExt_LeanIsPaused_m004A9E480599A969A094BBF5EAB8298D25F72877,
	LeanTweenExt_LeanIsPaused_mAB912B24C71D148315C8E71232F9B54C2FADB5A4,
	LeanTweenExt_LeanIsTweening_mECC74A5F933AEEEC71D8EBE7E977639DDD124F59,
	LeanTweenExt_LeanMove_m054FBB59F420198B550A6069D9F9E6ABAE6AB43A,
	LeanTweenExt_LeanMove_m615AA30F73F0ADF408BCE5B3EB2E7B0107F613DF,
	LeanTweenExt_LeanMove_mE09C2744CD6F66260CCB2AE85EEC189D1739A95C,
	LeanTweenExt_LeanMove_m213A2BEDDDE4818C47346D971A3D99CCE02F1E51,
	LeanTweenExt_LeanMove_m0A44978D179428622357E68D3AE31FCD5597B014,
	LeanTweenExt_LeanMove_m47C512A7C68F548F0C20D61A9EBC21444A89F47F,
	LeanTweenExt_LeanMove_m28CCB67D4CD2B89B5A3F25E9D70842C949AD0990,
	LeanTweenExt_LeanMove_mB1E50A4734E3C3CC223FEB98A394DBDBA9614850,
	LeanTweenExt_LeanMove_m0BF63EB3A8D4B614817EA402EB1CC4B5F2C83328,
	LeanTweenExt_LeanMove_m184D419AE52BA8FFCD237883D4118434DA2F40BB,
	LeanTweenExt_LeanMove_m31A80E7BD7568CEFF3D7B4073D2BA176A9ADF3DF,
	LeanTweenExt_LeanMoveLocal_m15178FB562F6F85C35B682AF97FCA004BDA1946F,
	LeanTweenExt_LeanMoveLocal_m91600FA81E8D5DB2D9C69E4F7C5FC09CC40D2DC1,
	LeanTweenExt_LeanMoveLocal_m98D7F1A2052D0F25DEE7B58DE411EE62A8B27CF2,
	LeanTweenExt_LeanMoveLocal_m920B50D3FBD4DC072118E879AFC151D682AE64B0,
	LeanTweenExt_LeanMoveLocal_mE4CF4E64AC16B510FB904060DDC2AE8BB231AA51,
	LeanTweenExt_LeanMoveLocal_mA49B57CD94C59A10CA8AF9D53F71C48286EAFFD0,
	LeanTweenExt_LeanMoveLocalX_m8847296A8B3401D2D054027ABB659B296FF73760,
	LeanTweenExt_LeanMoveLocalY_m86889CA4B214309EEAC239C3F175937E24310FD6,
	LeanTweenExt_LeanMoveLocalZ_m248064CFFDF7E42AC612D7F72C1E0F2DA37DCFB4,
	LeanTweenExt_LeanMoveLocalX_mEAB28137FE65A74C1F4963ADCFF044CD5DB7CF69,
	LeanTweenExt_LeanMoveLocalY_m727F1ED96B0D8FBF32EBB69787F82561886A07FA,
	LeanTweenExt_LeanMoveLocalZ_m731E4A396200E9C165B47A327CB59CCDB9817707,
	LeanTweenExt_LeanMoveSpline_m0E5CBB6704835E27EDA9B25D2915E8D2360E11A4,
	LeanTweenExt_LeanMoveSpline_mF640B6FFF8B58004A5C0A4475FE36E2C5BBAC607,
	LeanTweenExt_LeanMoveSpline_m3EE259E4A0061A1497D195E7910EAE9CD0B984C5,
	LeanTweenExt_LeanMoveSpline_m59EF921EB07DAFBB715DAB0BBCFAC8003E67BD6C,
	LeanTweenExt_LeanMoveSplineLocal_m667397DE03EFD1DBE0CE5F6CC2287807C3A56761,
	LeanTweenExt_LeanMoveSplineLocal_mB8706C86995411A7D6408A04AB6B3545922C192D,
	LeanTweenExt_LeanMoveX_m95E776B4AFF3B3472159C21ACCA7A074FD6250E1,
	LeanTweenExt_LeanMoveX_m5EA136BF257503853FF984AB882BADCEDD22F864,
	LeanTweenExt_LeanMoveX_m2E387E7F079BC81CCBE68B4D705C31E581639352,
	LeanTweenExt_LeanMoveY_mD3811BDA209686DCA906AB52B2CDD29F337FD6F4,
	LeanTweenExt_LeanMoveY_m65DEFE466DE737FAB4B1DEF0BD8DC8ED22D03433,
	LeanTweenExt_LeanMoveY_m846E64F6EED274DD02405DF113895FD366BA0FBB,
	LeanTweenExt_LeanMoveZ_mC2E89CF6F065D18A1F456935AF7EFA51091F0D9B,
	LeanTweenExt_LeanMoveZ_m85D8863F40BBF5A99203396C72B421E022FAA417,
	LeanTweenExt_LeanMoveZ_mD47791E76E05328EAE363F9A3D0B8DAE6713902A,
	LeanTweenExt_LeanPause_m3B5948BE9F86AAB0C837383D547DF2B291B8EA43,
	LeanTweenExt_LeanPlay_m6DCF6A71031221B880B66660EFF17720C6F1A42D,
	LeanTweenExt_LeanResume_m0C9B09411CA1C367B2E2F1E74ECC4860BB400BF4,
	LeanTweenExt_LeanRotate_m9A3EA584BE26EADB024F0E5EC57B0862F3B0BEAE,
	LeanTweenExt_LeanRotate_m46EDCCD0F931B4488BF8EDE72F0B1BA6BD65261F,
	LeanTweenExt_LeanRotate_mF34054132FFA550762A3C04473FEF3FB54931CA0,
	LeanTweenExt_LeanRotateAround_m97EA89B0C3FDE7B29B2A5F2FE3BE10F27B0C9993,
	LeanTweenExt_LeanRotateAround_m9922AD8260E8767A2D33963F2C9BA18ADDF22D61,
	LeanTweenExt_LeanRotateAround_m7F47B45F2074B30B22D75DC117BCF0A1351710E3,
	LeanTweenExt_LeanRotateAroundLocal_m43F940634C80C6D459B9DACF22F06E1E42F29116,
	LeanTweenExt_LeanRotateAroundLocal_m81397BB2C883AC5B3D62716FC3C93F43AE4EE5EB,
	LeanTweenExt_LeanRotateAroundLocal_mE1E669F2BCAAEA47AB4785D6BBFAE413CC74F053,
	LeanTweenExt_LeanRotateX_m0C6142BF35F74AC9A912509713E53E9F1B3F1A20,
	LeanTweenExt_LeanRotateX_mD09A3A174615A5479F81DFF31192673DADBF3324,
	LeanTweenExt_LeanRotateY_mE334362113CE7DA83AB29EFA1AAB8BFEB8C326E0,
	LeanTweenExt_LeanRotateY_m823DD2E4B248E09CA4E2CDDFD2D791DF8527FCD2,
	LeanTweenExt_LeanRotateZ_m587B987933373ED9B32F6D319F158899A25AF9C3,
	LeanTweenExt_LeanRotateZ_m25AA9563CCFC5754ABE0CDE84FE8E05F09B0357F,
	LeanTweenExt_LeanScale_m883D32607662D32DAB92DAF865CF110680BF18B3,
	LeanTweenExt_LeanScale_m572C2CB624F08B589CA3F7CEDA18D0521D396DAF,
	LeanTweenExt_LeanScale_mFDA8A475A889665712B39717572167439241C6D5,
	LeanTweenExt_LeanScaleX_m9AC6DED15F097E50134DF4213F86799F3A3F370B,
	LeanTweenExt_LeanScaleX_m469A49E471B4D706CF5859D368CD3F13DD4FBA67,
	LeanTweenExt_LeanScaleY_mEF7B8B2B411B70E312A061227CC9EF7496F1A720,
	LeanTweenExt_LeanScaleY_mC152F2D04B0CB5770AF0157B8BA217DDC0881D61,
	LeanTweenExt_LeanScaleZ_m23CAE903347B1DFEA59FFFB127419620967227F1,
	LeanTweenExt_LeanScaleZ_m2C148ADC5EA94A54CC00F349B1DF331079B13B16,
	LeanTweenExt_LeanSize_m410361AD3B78FE2A69D65C21570490F82E660241,
	LeanTweenExt_LeanValue_m1FB1FFD9157FA40B5854744EA15111178AEDE1C1,
	LeanTweenExt_LeanValue_mE9099E0F64A010AD2BEA7030C5FEFEDBE78A1571,
	LeanTweenExt_LeanValue_mF09B8E5D72CBADA7E7335A1CCE8397B6BEE0B286,
	LeanTweenExt_LeanValue_m165CCF09A13FA398105E06E6B441D0C5BEF33E20,
	LeanTweenExt_LeanValue_m2D0676A4B078FE10C1819ECF5520374CA2E39E8D,
	LeanTweenExt_LeanValue_m626C6C95ABDA16428DAD1399B0862363589AC9F9,
	LeanTweenExt_LeanValue_mBB6A5387301F308B4C42B4DF5B70C17AE29F5533,
	LeanTweenExt_LeanValue_m93C212023658F9EF6A27683DEBAA852EE4670411,
	LeanTweenExt_LeanValue_m5B60256ABE5F8B7E28F389AAE1696EB656633398,
	LeanTweenExt_LeanValue_mBA982D980197033944CF17D7C7F236A6CED885DF,
	LeanTweenExt_LeanSetPosX_m82201BB5C9781FA41FB69DDA05471D693E59DF2E,
	LeanTweenExt_LeanSetPosY_mD9DAE6B462055110382822D16B5551854CB68DD7,
	LeanTweenExt_LeanSetPosZ_m1E17DB053DD69876E163757D053A85B172CAC027,
	LeanTweenExt_LeanSetLocalPosX_m2325658730387BE391CB8FE674FD3FD2D8D3ECDE,
	LeanTweenExt_LeanSetLocalPosY_mF14BA20EFA312158AB0FD456EB8762457DB03AE5,
	LeanTweenExt_LeanSetLocalPosZ_m685180D2C04DF7E3417E9EECA9BEBE60F0850790,
	LeanTweenExt_LeanColor_m2027EB778950A5E71F3DCF398F8A6A77AE32EF35,
	ImageRecognition_Awake_mE172637B23DD1B72F4CE31B40B7BF0CE56E642DB,
	ImageRecognition_OnEnable_mB261FCAE7E314E1B9D537C60D8F075EA76FFC47C,
	ImageRecognition_OnDisable_m2B722701BC3F3627F4A9ECBA138B31368CFCC0A9,
	ImageRecognition_OnChanged_mFB283597AECA8A146430A1A06321DE1A171C1DAE,
	ImageRecognition_UpdateImage_m26D7D72CEA73CBCAEA9FD74832029472A742D138,
	ImageRecognition__ctor_mDDF9CAFE6A1FA239354591A0A6AED16DF84E5591,
	CamFacing_Start_m14EB9F61DA6165AE57AD42E22D7D23398F919BD1,
	CamFacing_Update_mDDEA5160B367B17DA264B87428D724324DAAA21E,
	CamFacing__ctor_mCA0D2B74599C0746D5051F64B0FD834119019A47,
	CoinCollector_OnEnable_mC21EC7F6EDFCAF431F295ED2A876CFFD538DF9CF,
	CoinCollector_OnDisable_m133841F690E3F25FF11FE0264E37FC145773CF61,
	CoinCollector_OnCoinsCollected_m3C9A78F020F8A84608681E3A39BBF39BE3D53D4E,
	CoinCollector__ctor_m209CC6D1E9378D8878B0625FEB01ABCAF7A77BA0,
	CoinSpin_Start_m4775228F7A36BB1AB26C3D834ADFB5479EC4C262,
	CoinSpin_OnEnable_mFFBFE5AFA1833307258741B7104C907FF7DF3D54,
	CoinSpin_OnDisable_m3B6E36881E2E8A3CCCE705255FDA47F9F4FAF11A,
	CoinSpin_OnCoinsCollected_mAAC796BB25FC68AC76DACF9AF79610517D6EED45,
	CoinSpin_Update_m7B845F5D805C0747C71A7C99772BD27386AB4D32,
	CoinSpin__ctor_m05750F090B6509A6133F34F5DB3399DE04E27118,
	U3CU3Ec__cctor_m1443B5BFB16125461557F58BB5CF344FC106795C,
	U3CU3Ec__ctor_m0A494CDF7EB83C504F6DEDDF7785A1E51FF37A90,
	U3CU3Ec_U3COnCoinsCollectedU3Eb__4_0_m981C64B43B1937A8AC0AEFD5F3E774708DD8D2C7,
	Elevate_Start_m2C3BCE670D42ECD29D05D2A8623C4A85AA40F4F7,
	Elevate_Update_m7901F7E643727AB5AA6ECE9B31D5811BBAB557AC,
	Elevate__ctor_m12A3D4B2C533756054EF5CD7D1582BFAE28C87C8,
	Float_Start_mBBF047E2FB9FE4F6EBA52C211264418CA98D016F,
	Float_Update_m7D3509C96925953822E1CB021F022E51B215A02B,
	Float__ctor_mBB0E290A40E91A1DCEFA692420EA6462129EEB2F,
	GameEvents__ctor_mA46EC570904E1217B6ED97ED040A6A86BCF4BB5E,
	OnARCameraPoseChangedDelegate__ctor_mD7D74EC559637DC63A77139FE1B12E9D458C7DFC,
	OnARCameraPoseChangedDelegate_Invoke_m54DA5C240672398B8518E7981EA7B3B1E7CB8819,
	OnARCameraPoseChangedDelegate_BeginInvoke_mD5A219AD2791717D25C425350D3E3B738A31708C,
	OnARCameraPoseChangedDelegate_EndInvoke_m6CAC0AE240A54794BA350E15C086A39F7D75AFB1,
	OnPageGameStatusChangedDelegate__ctor_mC6D1BE56C668B3ACED8FE3755B42B9836E569DAE,
	OnPageGameStatusChangedDelegate_Invoke_m8A82520C191BAE52D685BC15121BA93E2E2EFBFE,
	OnPageGameStatusChangedDelegate_BeginInvoke_m55A49F76F508A220146EE19140025F279E40B41A,
	OnPageGameStatusChangedDelegate_EndInvoke_m4907F39B00241794F88E59CA8DB0957FC9D28C76,
	OnPageGameCompletedDelegate__ctor_mF9509A74617EB7CA5ECEE91D4DDFADE30ED55F27,
	OnPageGameCompletedDelegate_Invoke_m292ADB5DCB2BA7E6F3D80F22C6A0291628DE5705,
	OnPageGameCompletedDelegate_BeginInvoke_m1567599C61DB410CF7AFA8D134E1593528EDBEC3,
	OnPageGameCompletedDelegate_EndInvoke_m21D6ED8D450273E6A7133925B4837BCC9A3AD087,
	OnPageGameBlockedDelegate__ctor_mC52A412077FAD65DC525C6DB4F4D4C0AF7EABC93,
	OnPageGameBlockedDelegate_Invoke_mD714F908E78ADD72B406DC537A83E0512821C078,
	OnPageGameBlockedDelegate_BeginInvoke_m0B69E6EE0E4B7D7418A0730343EC3138E2285E0C,
	OnPageGameBlockedDelegate_EndInvoke_mE4EB82596E84E5A858529F0A22DD3FB35F5590F1,
	OnPageGameLookAtRaycastDelegate__ctor_m4E8EAAAD726154985CDBCC8B6B4651B12DFCB6A9,
	OnPageGameLookAtRaycastDelegate_Invoke_m5202EF9E40E982E2DC52E14EF2038081050D5105,
	OnPageGameLookAtRaycastDelegate_BeginInvoke_m24C6FCD011A757A4D3294EE8400D2E2A81DB2B31,
	OnPageGameLookAtRaycastDelegate_EndInvoke_mEBEBFA7F474EFD910D1B4A04C2232E69B923147B,
	OnPageGameTouchRaycastDelegate__ctor_mDE3D6A07333F96DB184316D578A3197434D1B03B,
	OnPageGameTouchRaycastDelegate_Invoke_m97E2C9BAC94482E67D5AAF54F6E9D91CE08BC572,
	OnPageGameTouchRaycastDelegate_BeginInvoke_m86CB14F2B404DF44CEACF7D25C1946D85894063D,
	OnPageGameTouchRaycastDelegate_EndInvoke_m5F8537CC860A8D01A7168E848A51ABEAC6223A02,
	OnSplashWorldEnterDelegate__ctor_m63FF101453D43A9C71BD65243B7D80BD332C86E7,
	OnSplashWorldEnterDelegate_Invoke_m54A1C472FB41A97C1FE172D3B6F0535FB0C79253,
	OnSplashWorldEnterDelegate_BeginInvoke_m8C40327E560A3841D7C5DF4F38F7097031E6875A,
	OnSplashWorldEnterDelegate_EndInvoke_m0469135B1A23DA1339AD8EB99A7DF9A607165D65,
	OnSplashWorldExitDelegate__ctor_mA58FED7B590BBD6311CC70906D31523786DD5E07,
	OnSplashWorldExitDelegate_Invoke_mA17FEFC4DFA1E44C75C1359C261D16F4D8D93C82,
	OnSplashWorldExitDelegate_BeginInvoke_m16FEE0E59104AC8702EA4364CB055D3E39935007,
	OnSplashWorldExitDelegate_EndInvoke_m8D6C3D49BAEA5180A9812A618EC807DB6C69BB23,
	OnSplashWorldLookAtRaycastDelegate__ctor_m324F366FF3FBA14D69EC749D5D9C33F8D31FF25F,
	OnSplashWorldLookAtRaycastDelegate_Invoke_m23DA083ED354545670F3E37F602183A8EB8D554C,
	OnSplashWorldLookAtRaycastDelegate_BeginInvoke_mD1087C28732FEEB669C31975FFFDE518AAC8A5CC,
	OnSplashWorldLookAtRaycastDelegate_EndInvoke_mED01CD57E18A01D0A92D80D00AAB947F6CD56972,
	OnSplashWorldTouchRaycastDelegate__ctor_m9D794529B53F13112BFAD233DBCB2D0C6EEB67F0,
	OnSplashWorldTouchRaycastDelegate_Invoke_m902BAB09A737CF45244566632FF7F3861712F5A6,
	OnSplashWorldTouchRaycastDelegate_BeginInvoke_m9C8F92D5651C65C4DB6848BEBE1A8EC8FD0BD807,
	OnSplashWorldTouchRaycastDelegate_EndInvoke_m04F6F70DCABD301C4AB4C2CC74CB6CE943CC9AA7,
	OnTapOnDelegate__ctor_m4E74094DC85BE96A510EDB77BCA49A3A0C254C5D,
	OnTapOnDelegate_Invoke_m0B6E47CF342B976FF15123B09C844764E730362F,
	OnTapOnDelegate_BeginInvoke_m51E569CA9206297E464B27CCECDDF3169987250B,
	OnTapOnDelegate_EndInvoke_mF107157A47D6C33F9BF9EC3BCFF8516BE4951A5B,
	OnTapMoveDelegate__ctor_mB23EFB5884121D3696EF96AC4C63D15C868BB21C,
	OnTapMoveDelegate_Invoke_m82EAB5BC9B7953E64D5FD9A134E1C96854FBE053,
	OnTapMoveDelegate_BeginInvoke_m6B71072069CE1E8E847C5774E48BB23092A3C1C6,
	OnTapMoveDelegate_EndInvoke_mF87B674B7C31D97E8D4B97F30EE4ABAF702F94EF,
	OnTapOffDelegate__ctor_m8AE822D97CDC73ABB86EC6F5735AFF8E02A3B16F,
	OnTapOffDelegate_Invoke_m56638CD9FC5533926E7C5507AF6E30ABDE7B6D90,
	OnTapOffDelegate_BeginInvoke_mF7C6873C714D66B27139D805B834ED9C62AB1C7D,
	OnTapOffDelegate_EndInvoke_mEF4560AB49588DAA834521F2914C6609F37436F5,
	OnFirstPageTrackedDelegate__ctor_mB75FDEC54830FB287C4A0EEB0D49B31D58C77AB0,
	OnFirstPageTrackedDelegate_Invoke_m8C612429E7B45367EE79B426B3A51F6DFB909901,
	OnFirstPageTrackedDelegate_BeginInvoke_mB81EC53B8B5BFAF5623DE314AA82146FE974BBFD,
	OnFirstPageTrackedDelegate_EndInvoke_m6951D2FAEB889959B571436BA3A01DB990ABFC89,
	OnStarfishTappedDelegate__ctor_m87D899695FE02D720D73E864BE94CD32D54705D4,
	OnStarfishTappedDelegate_Invoke_m2CBB20F4BF0BE52B144302FCE1E612A9B36A0747,
	OnStarfishTappedDelegate_BeginInvoke_mFD9D6A9A6F87B6BBE09B62C8A99F288265DD32E0,
	OnStarfishTappedDelegate_EndInvoke_mF281E0ACD6914F63A69A5AADA68A28CD0B9D9A4F,
	OnFlickableTappedDelegate__ctor_m78A64B884A92E45427B06445D5CB9AF2C1158C77,
	OnFlickableTappedDelegate_Invoke_m2337934002592A0F31D526A8C61283DA427745DD,
	OnFlickableTappedDelegate_BeginInvoke_mC493D4B521739B0230A91F031A6225CFDB0376E2,
	OnFlickableTappedDelegate_EndInvoke_mA16AFB8C9E5EA5F9B5213FDAAEE89C7E57AB89FE,
	OnFlickableDraggedDelegate__ctor_mD6BF605C10B35F1950A0C65009F22814586F0005,
	OnFlickableDraggedDelegate_Invoke_m755C9E5C87FD48673496B485278ACFD7DCE22EC8,
	OnFlickableDraggedDelegate_BeginInvoke_mA289DD1825A4A277458F56EFD5B274158AFC37AA,
	OnFlickableDraggedDelegate_EndInvoke_m736D66982A4EBB21AA45DBBEAEECE12B5E75C930,
	OnFlickableThrownDelegate__ctor_m7F872563A119EB783080EF20A11445ED990E28B6,
	OnFlickableThrownDelegate_Invoke_m377686AE2223CDAC297886D3E8AAD16678707EB9,
	OnFlickableThrownDelegate_BeginInvoke_mA549322810396DF0EF1986E5C197B3ED9A4686A5,
	OnFlickableThrownDelegate_EndInvoke_m95BFC469F524C199CE32025B350DB934406B3774,
	OnFadeToBlackDelegate__ctor_m568D84467B3DA85258F1CD019B35A8575D565074,
	OnFadeToBlackDelegate_Invoke_m1DF87DE2454B68CEB51C885EFC35968A06FDDE9D,
	OnFadeToBlackDelegate_BeginInvoke_m5A04C197ACF0AC84D66236C046C1D23A7645AAF5,
	OnFadeToBlackDelegate_EndInvoke_m55C74CAF046C9C576A46812A2D2D48E58AB726B9,
	OnCoinsCollectedDelegate__ctor_m7B1543D4E912C7B7F56A05902E77AFBCC8DB97E9,
	OnCoinsCollectedDelegate_Invoke_m2B603892435DCA37B952DB3AE079A498979BB7BD,
	OnCoinsCollectedDelegate_BeginInvoke_m05F93FD54891E0BCDF61252EEC9ECBDD639D5386,
	OnCoinsCollectedDelegate_EndInvoke_mE48D93B3AD098DA9A6E7A42B006009F8157A6492,
	OnTotalCoinsUpdatedDelegate__ctor_m350CF5E17BF1715CC628D878A67C6B86B1296A62,
	OnTotalCoinsUpdatedDelegate_Invoke_mF68ACF8669149F71E43FC3BB8E63D1DB4C21499A,
	OnTotalCoinsUpdatedDelegate_BeginInvoke_mB27D8C0BF706F1023C3267F89487D371342A514E,
	OnTotalCoinsUpdatedDelegate_EndInvoke_m90EB63D65A1B2883EEC7D1B6B0662459EA05AF46,
	GameManager_get_CurrentPageGame_mBB7545C1F39191029C356CE8A0CF621C05A0466D,
	GameManager_set_CurrentPageGame_m9D471B21E93F777A181EC198961365863F981851,
	GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E,
	GameManager_OnEnable_mD5C3947351399CD9D94EAABE360192091328B2E2,
	GameManager_OnDisable_m5A032AE11D2E2BD8808702576A5DF606E0828003,
	GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1,
	GameManager_OnPageGameStatusChanged_m23AF3B11627C8C1F3E46023100A5F7F33F4A7669,
	GameManager_InitCamera_m59AC8612E323A872BBAF5774991B40969A774E4F,
	GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D,
	GameScale_get_CurrentStatus_m38D0B805AB43526506AAFAF0BCFB3280942720AC,
	GameScale_set_CurrentStatus_m15A0A482C1931D16C7AA8CAB0CE5A03E0C45B03D,
	GameScale_Start_m8B23A2D484BCA918F863887B70B4EDA22293A634,
	GameScale_ScaleObject_m0585571AAE75600F571926D1F9F72348F82407BB,
	GameScale_Shrink_mAB3753ABFE65694641E2844AFDF37EAEBF677D05,
	GameScale_Grow_m8D0F488BDB5AC478A96DCFDE28876F520C0EFD0B,
	GameScale__ctor_m86ABD3B568565132CE2F9637FEA31A26602BDC66,
	GameScale_U3CShrinkU3Eb__12_0_m2F0F00081290A6FC6F7AC0290DE44009D3B01527,
	GameScale_U3CGrowU3Eb__13_0_mD9600484A0A19E931BAE06A6BCB1EEF0A96AF277,
	U3CScaleObjectU3Ed__11__ctor_m9F9912F1FE2061B80649C3E2D3EE03F0C81A2ED2,
	U3CScaleObjectU3Ed__11_System_IDisposable_Dispose_m1F6F9E741217D4A31F59B0AB382F7EA581962124,
	U3CScaleObjectU3Ed__11_MoveNext_m4049CE4C30DE148292B5E29E2353D62BDD9AFC66,
	U3CScaleObjectU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA18277648E8ACA4C90D1053F66BB3A45B40A6504,
	U3CScaleObjectU3Ed__11_System_Collections_IEnumerator_Reset_m03133167C210B463F8914107D14565D5EF30F003,
	U3CScaleObjectU3Ed__11_System_Collections_IEnumerator_get_Current_m3D9D148281A0AE0E2A411244F4636E11A8AC4541,
	Idle_Start_m1071EBB94553CF0B91C9AB1933D3EF218FB1154E,
	Idle_WaitAndActivate_m70D559158D8C1EDF0B9B44BD9823D4611AB2290B,
	Idle_WaitAndDeactivate_m59520F96D5799F5496ADC573057933A6555CD144,
	Idle__ctor_mF407A49B8DF091C857F7A11B9D8095F40526D11D,
	U3CWaitAndActivateU3Ed__6__ctor_m372FCD69AC54F9B6F417E419D1CBE41AFEBAF14A,
	U3CWaitAndActivateU3Ed__6_System_IDisposable_Dispose_m0D3874C983C3F7471BE43376345B2AA14B702A36,
	U3CWaitAndActivateU3Ed__6_MoveNext_mD91A64656968EE7793A445E0479E98CD9DC137F8,
	U3CWaitAndActivateU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m41C234EFF7D21326815858026BE32FE067EF619F,
	U3CWaitAndActivateU3Ed__6_System_Collections_IEnumerator_Reset_m5C8CE0D8F95DDAB7A3EFB667513B448F26BCE043,
	U3CWaitAndActivateU3Ed__6_System_Collections_IEnumerator_get_Current_m1D86BB210D7EA8F9BE13DD49A67154BD11E2BFAB,
	U3CWaitAndDeactivateU3Ed__7__ctor_m3557474DAA9C5DB0CD5BD4032344C0A53452B250,
	U3CWaitAndDeactivateU3Ed__7_System_IDisposable_Dispose_m9FEB66AAD863C5A40D5E5420E53BFB2E7586E4D8,
	U3CWaitAndDeactivateU3Ed__7_MoveNext_m8E0193259ACC6560820A9DE2EE116E0344F92AC4,
	U3CWaitAndDeactivateU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m554F125318195969EAA9504CBBDB3D77CE7CB917,
	U3CWaitAndDeactivateU3Ed__7_System_Collections_IEnumerator_Reset_m8B28B60638773A42F14FBE776C0E896D4F2AE0C4,
	U3CWaitAndDeactivateU3Ed__7_System_Collections_IEnumerator_get_Current_m4AA71565E06F69AFC1CE8BCED114941F756B919B,
	InputManager_Update_mE17FD2A03E0E1BE94DFE0B0AB4B5B9C5F3EA285B,
	InputManager__ctor_mB533F16325A793C9274F6CA3804EBCE27AD700A7,
	MV_Awake_m008F0D0ACC5C1115624FD084641E66843C1416D1,
	MV_Start_m86C4237712EC695DDCD1B3B46CB269C1D7633DFD,
	MV_Update_m5BD715E81EC2902864B64E72CAA0C4092E0BE927,
	MV__ctor_mCDC5A8302C9A0106B442F234DEC75CB1E4515661,
	OrbTap_Start_m6E0BCC7996A47CFE7E1D37337EE386B62620D04E,
	OrbTap_Update_m0DFA86B6FFD270FA49D1BA05B0542749C685DCB2,
	OrbTap_Tap_m9A92D23E3843F6B5CFBBDA7100661D3CBC4A8479,
	OrbTap__ctor_m741D0805658EB70CF00EAD07A70DE78AAFE435A8,
	PageCompletion_Start_mDBDE60FA1FB2EA54089A30FE18E832F53C269376,
	PageCompletion_Update_m9D41C20F891A6D1BE2819F36C2816AADD102F65E,
	PageCompletion_OnEnable_mC7E0A6D8B018FC8EF0B5C3EF0E66B417A73FA8DE,
	PageCompletion_OnDisable_m439C8CD84035C690C165EB9F3ECC4F6FBF9A442F,
	PageCompletion_OnPageGameTapRaycast_m048127FA790A0AFB0BABEEF82753CED49267553E,
	PageCompletion_IsAllTapped_mA814F495F6AE2F691C302D2E49E44E8F589A4AC5,
	PageCompletion_PageComplete_mC87F1E1F909464791F41B0B86AFD25D84C76170D,
	PageCompletion__ctor_mCBAAEB51A750E50191A35D107BD3E4FEC863401A,
	PageGame_get_HasSplashWorld_m1E2194DE66FC47CC228E7450DB856653F57C6629,
	PageGame_get_CurrentStatus_m62216C85BE6E5B3889395214E48C988D10E4ADAC,
	PageGame_set_CurrentStatus_m80C5F4ACC165A3A47F04F47DD7C04D97E914D5C5,
	PageGame_get_CompletionCount_m163C431B336779F854B28006C445D9ACFBE41E3C,
	PageGame_set_CompletionCount_m773C823765A76AA176ACE8A5D7CC238B07797EA2,
	PageGame_Awake_m798006381EEF3029AA15ED0EFDCA8E1C83BF8C46,
	PageGame_OnEnable_m08CCF29AFE68ADA36CCB77E974AB453A4A3EAF65,
	PageGame_OnDisable_mE98487A04E575C55CD2DAF44397214C60674A88E,
	PageGame_StartListeningForTouch_mFB45A14D729075D31515F786D29E7D475E1FDBE5,
	PageGame_StopListeningForTouch_m266F0504AC3678F0B0068504E8DB46F50D900C7C,
	PageGame_EnableImageTargetting_m83F6BE8E0989B3605BD9AE159B974D13669F76C7,
	PageGame_OnPageGameTapRaycast_m0E9BDDE3058EBDEAC517C0E63B8DAF3CA50C386E,
	PageGame_OnSplashWorldTapRaycast_m4CA9A20365943007423B08E62F022345DFEE9292,
	PageGame_OnTargetFound_mA8082170277B6AA4447CE3EB4518B36842091980,
	PageGame_OnTargetLost_mE2103835326A93F467DA5CB5DED0EB5C84D75FAE,
	PageGame_SetStatus_m135727AF9BA54570F961B1B85C80703781F022DC,
	PageGame_EnterSplashWorld_m6CC9D52AA2548E613C72ECCBA13D190B9760957C,
	PageGame__ctor_mE30B9FF15964608458BF17C75DBA8340B8880D48,
	PageManager_OnEnable_mD97A62753725BD8315D744C838EE2073AE61B6F3,
	PageManager_OnDisable_m7E562F9E5528ABE45AB74AE7F4E7C5E2CCDAF346,
	PageManager_OnPageGameStatusChanged_m37698A0D6310FD920DC2B8BB831F7B8FC38B0FAC,
	PageManager_PageTracked_mE3DE47EA8D795515C3CECD1B74871971A819C397,
	PageManager__ctor_m88AF16E94DF020C84F832C3E34ECCB4DDB4EBBCE,
	PageTracker_Awake_mFF8F16468AE8FFD253955764C38F7964222634FD,
	PageTracker_OnEnable_mC929997C7E675482A678736EFDD43D2E7BFF1D4B,
	PageTracker_OnImageTracked_mB6D9FB10F747598ABE2990AABFEC5C041B6189AA,
	PageTracker_OnDisable_m457F0C7263FBC6EC38EBB381688B3082551DC08B,
	PageTracker__ctor_mFB006E94A796C6F6E506741CFA7084C7CB5A423E,
	Raycaster_OnEnable_m3821F1CC53335174A4DDD3C90A76497215332F93,
	Raycaster_OnDisable_mC6762911CBA887F439EB0B0558488AC874A06A07,
	Raycaster_Start_mBE50D4A510FFF56FED5CFA84D12798E16A40FCEB,
	Raycaster_OnTapOn_m2386CAB3CCD74B3232CC6A89F75205FF31872D57,
	Raycaster_OnTapMove_m68E87D42B49AD820F82BA6A9D5A05C25A15BEABE,
	Raycaster_OnTapOff_m4D7A90BB4FDA3E8994CA8E810668767D324D50BB,
	Raycaster_RaycastToTap_m192F84A11C3E1088AD672BDDA3A47B1131295D44,
	Raycaster_RaycastToDrag_m5EAA0CE8A99105FD2C8523E486F99162FFCC946E,
	Raycaster_RaycastToThrowOrIdle_m4EE3F01C3CE43C9FD08FA4BB0F5D988DFFF66378,
	Raycaster_DoRaycast_m4C8E1C12AFC41527E1A718114C5B3AC14F4CDEBA,
	Raycaster__ctor_mCE9420B0FF812611DA3918D527E761D5075F7404,
	Spawn_get_CurrentStatus_m5E2624F5D15120BAF25669DFC891BB10BF2FE067,
	Spawn_set_CurrentStatus_mA3512D04052416AB2B5D7167D64D39DE8C41C084,
	Spawn_Start_m99E70C46F85C6318276009783089BE4FEB2F3DCE,
	Spawn_ScaleObject_mA7A227A8BE23DEEF647D38A89FDC136C1CA68A82,
	Spawn_Grow_m425F2432DBBDF90D5AEA3A96CA32A59D17E84C00,
	Spawn__ctor_m29DA62C57DCB06B5990FB843EE2F8533D4FCEC1F,
	Spawn_U3CGrowU3Eb__12_0_mCACEB31935EBC09019449E1370A1225EFE1140CD,
	U3CScaleObjectU3Ed__11__ctor_m8D9F6C2638A2271E6525C7972EDFC7CAB742B0DD,
	U3CScaleObjectU3Ed__11_System_IDisposable_Dispose_m40B8BFE624A73F108E46FFFA98B2AE4D6787CB5E,
	U3CScaleObjectU3Ed__11_MoveNext_mC31883E5BCC98BCE6524A2E263A7F992DD5BAC0B,
	U3CScaleObjectU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m30FB78064DC61798F0BFA19E5D2F12A85975F0F5,
	U3CScaleObjectU3Ed__11_System_Collections_IEnumerator_Reset_m10257A8A5A594206D1B8A584528BFC9A24615121,
	U3CScaleObjectU3Ed__11_System_Collections_IEnumerator_get_Current_m116291CCC3A2D4AAB33B5F5683271A7799F9B80D,
	SplashWorld_Start_mA0C62D66AE9960C317B369D12DF11D70CC72EFE6,
	SplashWorld_OnEnable_m7C1C68FCB62DCE7013BB1D8A9F7FDB69418497E4,
	SplashWorld_OnDisable_mCF7872F8B7B4AD2527158554BC36BE3B4E8F4E4F,
	SplashWorld_Update_m8D77D9516591560E332B49042E667B0863704541,
	SplashWorld_CameraRaycast_m2077251F485F55107FD1EEFC65C605660CAFC5E0,
	SplashWorld_OnARCameraPoseChanged_m76AC3D829A0CA59D5DA83C1BC7D3D4A22C3A7B68,
	SplashWorld_OnTouchOn_mEB0B32B58ADCC1DFD2F5F73C1541D56C1F10051A,
	SplashWorld_PlayMusic_mFC384AF0FC751C403770C5424DB630A4C682777A,
	SplashWorld_StopMusic_m6B6F9FD1277A7CC2423CFF915E04C2F6C56E200F,
	SplashWorld_PlaySFX_m46D2ECCCBB18D8D112F727E90BF01313391F69E9,
	SplashWorld__ctor_mCD892468D492B38EAEFCC243FC058DF6211825C0,
	SplashWorldManager_OnEnable_m2D71F9709788941DB115E3E909AECB41F561F3C0,
	SplashWorldManager_OnDisable_mBB418D9C6DE0E34B389D762B22248AB6250E26CF,
	SplashWorldManager_OnSplashWorldEnter_m8229A96B068EEBBF59AF1E8B1BE4409965975761,
	SplashWorldManager_OnSplashWorldExit_m4016B5DBB3678A156A8440A511A18F6CFC79595E,
	SplashWorldManager_ActivateSplashWorld_m7F0EC5F9BB33B28ACA48CEC85A3D3FCE882196C5,
	SplashWorldManager_DeactivateSplashWorld_m2254E024226FCF558E305FA360D20DC2317F19D1,
	SplashWorldManager__ctor_m9ECB8D2E115C583D949609EBE587E8FF90116ECF,
	SplashWorldTapThrow_Start_m71AE50CD88E255AFE3ABAC648BCA18501CB3D710,
	SplashWorldTapThrow_FixedUpdate_m1BF3BCC236C4AD5FC08724867564CDD1FD3747A9,
	SplashWorldTapThrow_Tap_mFA80CC1A04761C4CE1BD33E3EE9DC15C7A8D2BE0,
	SplashWorldTapThrow_Drag_mA3F4BB999FB95BD64E975A77BBAB61D6304895D9,
	SplashWorldTapThrow_Throw_mD9885C3914F04FB7FE4AC249AABCF50226424B58,
	SplashWorldTapThrow_StopThrow_m1C8B860D53D89E391CFAEA7F2996B04C54E53C03,
	SplashWorldTapThrow_DropToStartY_m7957DD0A0E9F4E3258262AD2916D8023BF6E134D,
	SplashWorldTapThrow_Disappear_m6C60EBEC528C76520AFCD19FCF0E32DA3300DDF5,
	SplashWorldTapThrow_SnapBackToStart_m1E65D46190B9E177D191B1B53361649A44005EEC,
	SplashWorldTapThrow_OnCollisionEnter_m471A08D6C895A814740F3ECA61D258EEA9E3BBC6,
	SplashWorldTapThrow_OnTriggerEnter_m831AA983BDB87D15D2F5F49AB88E6B6069EA349C,
	SplashWorldTapThrow_OnTriggerExit_mE9F03FF5D82476691B5785D4C28A485DCCDA8052,
	SplashWorldTapThrow_Release_m21C11A6B9A97FB1DEF3C04040118646425F983EE,
	SplashWorldTapThrow__ctor_m5F4AA3076F4416710B09D21B1AE6271F635B9EE0,
	SplashWorldTapThrow_U3CThrowU3Eb__30_0_m0D5CC01A9E3F3BBE5BBC74F21EFB1AA7F962E30A,
	SplashWorldTapThrow_U3CThrowU3Eb__30_1_m646A85613E11A7EC17CAFB9A2140FA9E49E07A67,
	StarfishTap_Start_mBBB0C1E2F1120724FA3374B47C5707DCC2AE26DE,
	StarfishTap_Tap_m913B3F3C11973AB8F25696007A6ADB3F6F07F61C,
	StarfishTap__ctor_mA6A4C51C7A0E7EC404ACBCAFD56E6E8C7278605D,
	StarfishTap_U3CTapU3Eg__WaitAndActivateU7C20_2_m45303C54140A66C6A1C3AA9FB164850D9B9E0605,
	U3CU3Ec__cctor_m7E7B343B256D846881EE6D9805A8CF9589F32557,
	U3CU3Ec__ctor_mFEBA80A164FB9BA80162B341FE2DC5B95723B1A7,
	U3CU3Ec_U3CTapU3Eb__20_0_m8EFDB804F91709071566BB6224B050A7E830DF9F,
	U3CU3Ec_U3CTapU3Eb__20_1_m322A25DB3D5CC75BE8E6058378AB17FADA407EEE,
	U3CU3Ec_U3CTapU3Eb__20_3_mDBE5CA2B9D6ECB2DD1F3146662098592F3748E96,
	U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed__ctor_mC1FAC773B7C389314756A2BC1C0B935D629E5CB8,
	U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_System_IDisposable_Dispose_mD1831FF6B8869F465B692C7BC3440334E3C277AE,
	U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_MoveNext_m16445490866976247BDF43DD8CAAC133C1045C9F,
	U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9764191B0083E73AC10D2DD64B724AF7BFE30151,
	U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_System_Collections_IEnumerator_Reset_m2A883B4CA0FA17D2368B73A8DD265F095C3662D5,
	U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_System_Collections_IEnumerator_get_Current_m15B9F318820B9184FA7C97E17B84E9AA142D375C,
	ThrowSuccess_OnTriggerEnter_mB5E4FEB2DB739F204D37D31798572A64B9F3331B,
	ThrowSuccess_Start_m542004C57C61C4E09741315FB3125187C52D27F4,
	ThrowSuccess__ctor_mC3B1E94A2889FBE7B2E87A233058CD7D4983AC41,
	TitleReveal_Start_mAB5858E71F305E5E27EDEBC34E71BEDB32189F63,
	TitleReveal_Update_m9A44B25E35C2D358E8F5C1C05442395CFCF3590E,
	TitleReveal__ctor_m59A1A622E3451498874A8448B755BCE4B54F627E,
	UIManager_Start_mAA4B371DC406146E84A9D8803B9C861939B2E04E,
	UIManager_OnEnable_mAFE10D24F19B379487455D635E4A3C65D4B64240,
	UIManager_OnDisable_mBAED7246344396BA861ADE4603C663A9ECFBCDEF,
	UIManager_OnPageGameStatusChanged_mDBCBA6610A305A26A73CA24FB72DA3CAF1B521FE,
	UIManager_OnTotalCoinsUpdated_m0ECA7EBBF06E503B0E27263FB847C8D735E2BDD8,
	UIManager__ctor_mDADE1D724D40AF63AE78D51FC1CF1FE4784B4D4B,
	UIScale_get_CurrentStatus_m59B74B309E7DEDD304B5975587C597227D35AAF7,
	UIScale_set_CurrentStatus_m3DDDB961B7587E70B838BEAFB6B78E2D9C577004,
	UIScale_Start_mB103E0582EADF2571F03AD1E95C8960531C2A678,
	UIScale_ScaleObject_m2D6ACA6D185145F4DC427EF2D42A49EAC233369D,
	UIScale_Shrink_mD23357992FADEBB542F6D4769D5585664CA93248,
	UIScale_Grow_mA576B57E720E08B0CAABD8C9BFE56A20700FF04B,
	UIScale__ctor_mCAA6B6E247394903A335B751CBB1C6F3F4381586,
	UIScale_U3CShrinkU3Eb__13_0_mFFDD536FF19492248C5AF5931C4DF93F7AB58411,
	UIScale_U3CGrowU3Eb__14_0_m3BB4510B628C633F140F8A7EE6AC08E08170E52A,
	U3CScaleObjectU3Ed__12__ctor_mDC451B5F819E9A4AE8516332FB6EB086259AB187,
	U3CScaleObjectU3Ed__12_System_IDisposable_Dispose_m18311F276F727390C6BB8F45547A1055AD46B687,
	U3CScaleObjectU3Ed__12_MoveNext_mFBEA76F7419EFADB3A3E3A9D747DD36A8A2FCDEC,
	U3CScaleObjectU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m208AC32EA8F4EFFA396F863484497848182EE124,
	U3CScaleObjectU3Ed__12_System_Collections_IEnumerator_Reset_mDCE696778B7C746AF2B21B9EDBF0EEDD9FA7C644,
	U3CScaleObjectU3Ed__12_System_Collections_IEnumerator_get_Current_m919B949113F60B7761002096FB3E96CDF935EDD5,
	CanvasSlider_Awake_mE32BF63E2F695EA0E9F568D21719573561962111,
	CanvasSlider_Start_mB39BE74CE1BB17577ED12744AF909F90370D4F33,
	CanvasSlider_OnPointerDown_m2943AB5A386A0162CCD4ACCA204CEC1B64A22394,
	CanvasSlider_OnPointerUp_mE4F3D66FAB7F10F07C5938722147858D9BD56F4A,
	CanvasSlider_Update_mE3B0594C72B26CEC4C0E6629ACF1460F3199F2D2,
	CanvasSlider__ctor_m38912C1E7DADCC31320E462922DDC08088AF2058,
	OpenerVideo_Awake_mAEF909354080E50381A40E6664333E037AFB4445,
	OpenerVideo_Start_mD1AEADB2B11A00400CDF66F1F0BB5B3F536E0FB9,
	OpenerVideo_Update_m0B35BFB4C9BD78C4CCE64E47BD56DEF88F942081,
	OpenerVideo__ctor_mBE80C503758B7A43ECF45B57D5726C90A8D3F50E,
	PlayPause_Awake_mF0E0387D6976BAE582D136B853CA1825756E9362,
	PlayPause_Start_m932E78DA3BCE735856E3610251BD41330A1961B0,
	PlayPause_Update_m54C6CA681D9B2AEEFCE0F223A915558867C5A0D0,
	PlayPause_videoPlayPause_m4327377C3DC61DF4D6B76CD980310ECE42A9C312,
	PlayPause__ctor_m6EE8B948F0668C505EAFABC70C3676575B2C0729,
	Readme__ctor_mF465410C5B2E598F2685E82CFCE1F42186AFF448,
	Section__ctor_mBAD5262A353BC071C61B8DB462A3D4D5AB5C7C4E,
	SimpleCameraController_OnEnable_m2BA1F31BEDE84695933E86CF731059D6FAC2111C,
	SimpleCameraController_GetInputTranslationDirection_m924AB4CEA66ADC4F1C63EAC21B660E0495765F4A,
	SimpleCameraController_Update_m0E587F33074BE72C54BA835B2217B0E8EC94F24A,
	SimpleCameraController_GetBoostFactor_m42B667ADEACC6F9CF0E4B6549A501609A15716AE,
	SimpleCameraController_GetInputLookRotation_m4F945850C9036FAF5C3436AA42A1C3CB918E1BB9,
	SimpleCameraController_IsBoostPressed_m26120B18155EBB68597EA52F392AD1E59F9A71E0,
	SimpleCameraController_IsEscapePressed_mC694179281D2F4B20E831995751C70A26806488D,
	SimpleCameraController_IsCameraRotationAllowed_mABB9E26C1CC5C5643B305685F046B4B8493C7AFA,
	SimpleCameraController_IsRightMouseButtonDown_m0DE4F75C0A0AE963F333712F226000E0AE2DC32A,
	SimpleCameraController_IsRightMouseButtonUp_mEDE93C76DE7120043B15F390865340D1C5165CE5,
	SimpleCameraController__ctor_mC6AE2509DDF461856450EC7DE0058A1687AB5C87,
	CameraState_SetFromTransform_mAF13C515CFB1085295C01A870D93375E98F16647,
	CameraState_Translate_mB8F7239BD9DB70190E59D47D75DD125AD9AF3A96,
	CameraState_LerpTowards_mF2D4B962A677B281ED2F539A2FFF8A693FB9A326,
	CameraState_UpdateTransform_mE653356FD34828D19ECB6793439A14C38F372410,
	CameraState__ctor_m9C5338CABE70B8C73F8A4A08C1AFA1B33417DE9D,
	LeanDummy__ctor_mD0BA3A71589D10708BAF2C24160EC0305D014DBE,
	PathBezier_OnEnable_m5E057D877378F11B064309894D1AE8BB524F4771,
	PathBezier_Start_m17B69DE04B368B76932DC991342B633A7F0A9334,
	PathBezier_Update_m3827BD355D4006FA740F2B9F1EDD8C426CB43A91,
	PathBezier_OnDrawGizmos_mBC48C0867056FB925C45FF97187F8AB4C4D6F13C,
	PathBezier__ctor_m64C0684464ED26D96A5AE82DEA44C6ADF0F2085A,
	TestingUnitTests_Awake_m93E88F38DA88C64CC12ABAE80900B948E5BC30E1,
	TestingUnitTests_Start_m8C9EF37C5BC5461665D44564D0FE7FB3A6C69A66,
	TestingUnitTests_cubeNamed_m3948700DD2B6420F25DFC461AB2A2A5357E792D6,
	TestingUnitTests_timeBasedTesting_mBD8B52CEA7286C9F4FAAA419C773B78255E55F4D,
	TestingUnitTests_lotsOfCancels_mD21D0D0D0B8BE316E58C412A19ADE6658DAA5134,
	TestingUnitTests_pauseTimeNow_m3293FFC3DDEC01D507093F2D2B8E34B40FC0DD7F,
	TestingUnitTests_rotateRepeatFinished_m3ED0D0A9DF982711F7D8D670535F1136CB7E3BA0,
	TestingUnitTests_rotateRepeatAllFinished_mA343A62B2E6F1D84DAD61BF7DAD6DDC4F24A5077,
	TestingUnitTests_eventGameObjectCalled_mAFA0C3AF3614A013DC2BB8B1F82E6FB121FE56AF,
	TestingUnitTests_eventGeneralCalled_m9054A75A4B6E1A3BE3E393F88168FAEA8B34D65E,
	TestingUnitTests__ctor_m7108B4718BC8D59EC59A4EF627657842243EBB27,
	TestingUnitTests_U3ClotsOfCancelsU3Eb__25_0_m79612DDE2FFAD2A0986AA0D91A768BA96269D4F6,
	TestingUnitTests_U3CpauseTimeNowU3Eb__26_1_mB0C630E660D38F819734F11E2998CDE43590D503,
	U3CU3Ec__DisplayClass22_0__ctor_m85E81FE7FB95A8484DB589E78AB8F1B684592DDF,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__0_m0E29967BE8C8986E397C440673E2793E2569089B,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__1_m887D60815BE91C503B1B557775A0633F1B60E86E,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__21_mD56FCF9AA4FAE66DF497E1779D9334E757A222E5,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__2_m25A626FC5B5176333408538B200230F7A012FCF1,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__4_mC5E35367A4E3D78731DCB8D56BC172E263F40648,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__5_m31888B2277D8EEE835811D9FF81FA68312AA086B,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__6_mE7251F18767B392C7011C9893101E7498D8B0C29,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__8_mCAE5BA94A4F65FFE4C9F531C9AA0249D6F7B9218,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__9_m4AA88BFFB46D1E0E6212B98A76E7A0F6AA5C0039,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__10_m561C045E5C1DC104420555CF0BA62F1DF6B671E8,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__11_m81593E9FC9183EC0E37356C733A02C3266B87796,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__13_mF54BD21F02ED31763F697C67DB121D4646CC7522,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__14_m5A91E5886E31BA74327DDC19A47BB2E14DB7E810,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__15_mF1644C8D1D721BD4651678F2F71ACCA470CA60D9,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__16_m347E0C3ED3E8390844A251CE7CDAE009166D0358,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__17_m5404D4E64F471E4BB3E95B2DBF8C78AEDCD6C4FC,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__19_mFCD1C9E7E22F2F6710CB151C14E7C6094B6F0EF2,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__20_m07D0BA90D12217971B1F53FF20AA9B02391A4877,
	U3CU3Ec__DisplayClass22_1__ctor_m42740028F7DBAC0910C2B0D8FE48AE424104B01F,
	U3CU3Ec__DisplayClass22_1_U3CStartU3Eb__23_m71B69B2DE70762D5765B748E1B062290289D9195,
	U3CU3Ec__DisplayClass22_2__ctor_m92BFBC8093B249E40CB64FABE4D548B594B0A5A0,
	U3CU3Ec__DisplayClass22_2_U3CStartU3Eb__24_mF27E3CE748DB0EEE7037C4DD812DD75BAE86B46A,
	U3CU3Ec__cctor_m6F153F2D37E38349116C0879DD2F597DEC855ADD,
	U3CU3Ec__ctor_mF4403EE165173DC522103616E6645018625DE513,
	U3CU3Ec_U3CStartU3Eb__22_3_mB3B2B3F460A87856365CD811630D58EA7A704357,
	U3CU3Ec_U3CStartU3Eb__22_22_mCFEF8C7D30A25EAB5E114C737BF1D2A30D599CA9,
	U3CU3Ec_U3CStartU3Eb__22_7_mAC888F5211B48BEDE3B76735AF1835BC125992CE,
	U3CU3Ec_U3CStartU3Eb__22_12_m56B9B268A61C3FB036CCE2D844B45C27C0B201AD,
	U3CU3Ec_U3CStartU3Eb__22_18_m210C935C0307C41651403480E5EF0856246C8C3D,
	U3CU3Ec_U3CpauseTimeNowU3Eb__26_0_mE763C76C7D6AEEAAB8A0C84892C1D42F508144EB,
	U3CU3Ec__DisplayClass24_0__ctor_m8818C4F306B80B7FEE062793CBEBEAB352E53F22,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__0_mB82E5FEE09CC712C778F6A007B14D3D455256ECB,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__1_m8D5BED91D3EE4A8C0C0DF77D2B62F903C3E20D0C,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__2_mB677486A14C58435757062820302D43BCC02D144,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__3_m3987A57ECD0F50AB39FEF707C42D9710AA3CFB2D,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__4_m87D8E7758DA9C355EE513F9775D9F4B4C387BB56,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__5_m2B18F0F9B914A675CEEA356DCFF478B4F5611D8F,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__6_mE7EA3413E1F81C42AAFE03C96A7C6C430EAFAF81,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__7_mF6A7BD6551F5A9F146C1B47E86AFF650A2B0005D,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__13_mFFA8F8E7E5F79A608E1C2216AA8D1B734A758093,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__14_m0D4E422B37F43F7D3ACECDE81ADD5A18D4C97EE8,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__15_mE41B0CD588F01B0A76360EBD332CFAF425E20931,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__16_mEA9C490B5AEA717E999FFF3DD2CB25B23966277E,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__8_m63E7CF9513E4624B5B101B1C46941DC34982AC33,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__9_m7B1419838E647715BC8200AD6D56DAF1A8F04F81,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__10_m91860D812373862DCCD33D1D479A52867ED3CC02,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__11_m9D776D70F7B40CB7DB069BE5D095C281D59F2713,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__12_mB40AAAC5766C52276E4F9A375DB379DBE62012FA,
	U3CtimeBasedTestingU3Ed__24__ctor_mBB473D0F9FCF74C17EE67A02377DBAC113B0ECE6,
	U3CtimeBasedTestingU3Ed__24_System_IDisposable_Dispose_m2240ABE280E91D3110F4956F4DDAE8A6F211340A,
	U3CtimeBasedTestingU3Ed__24_MoveNext_mF87066714174A943FE18B6A562B291DFA766B72E,
	U3CtimeBasedTestingU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mACB1A3175D122506B71BCDB9EBF4AA9F0383E74B,
	U3CtimeBasedTestingU3Ed__24_System_Collections_IEnumerator_Reset_m96D9D8DFA5B24A08E71FA9EA4595288F06E9837B,
	U3CtimeBasedTestingU3Ed__24_System_Collections_IEnumerator_get_Current_mA059C8602E0A9BCCC1849108A55B19E09E4818CA,
	U3ClotsOfCancelsU3Ed__25__ctor_m9B88818966D8A321B3F23FB53C461C28A354F1C0,
	U3ClotsOfCancelsU3Ed__25_System_IDisposable_Dispose_m40E5F2D2626AB1E624CA39E3740CDFBE5B4F1B30,
	U3ClotsOfCancelsU3Ed__25_MoveNext_m9AA75163A6CE98F58622048E0971E438D1E2AFEE,
	U3ClotsOfCancelsU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2F111DF2BFDD6B3974DFA490B271DC9FCCBFC1DB,
	U3ClotsOfCancelsU3Ed__25_System_Collections_IEnumerator_Reset_m440F860762B28408E3B9AB6D72144B5E6A1C6F8C,
	U3ClotsOfCancelsU3Ed__25_System_Collections_IEnumerator_get_Current_mA7D1CE85A7D50D60D73C5914F5EC60B5E33A344E,
	U3CpauseTimeNowU3Ed__26__ctor_mD1AE96EC04B7EB3C54830BBCA910ACEC6A857D84,
	U3CpauseTimeNowU3Ed__26_System_IDisposable_Dispose_mE8F0437ED1B062C967A1FFCA460708BD42BC5142,
	U3CpauseTimeNowU3Ed__26_MoveNext_m0750DC2D5E587C420188232EDCAD54FEE7ABF659,
	U3CpauseTimeNowU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD12A76EAE3CD0B1B9C938030B3E4D2FAA6456011,
	U3CpauseTimeNowU3Ed__26_System_Collections_IEnumerator_Reset_m88957DE3F231D3C4F8E64C6DADA45876FABB7FD0,
	U3CpauseTimeNowU3Ed__26_System_Collections_IEnumerator_get_Current_m04EA51DB175A4043B51D9B3CFDD410D924B422A0,
};
static const int32_t s_InvokerIndices[1473] = 
{
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	6702,
	3397,
	3330,
	3397,
	3397,
	2516,
	4270,
	4270,
	4270,
	3465,
	4270,
	3438,
	4270,
	4270,
	4270,
	3465,
	6825,
	4270,
	4270,
	3438,
	4270,
	3438,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	3438,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	947,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	3438,
	4270,
	4270,
	4270,
	4270,
	4270,
	3438,
	4270,
	4270,
	3397,
	3397,
	3397,
	3397,
	4270,
	4270,
	4270,
	3330,
	4270,
	4270,
	4270,
	4270,
	4270,
	3465,
	3467,
	3330,
	6825,
	4270,
	3438,
	4270,
	4270,
	4270,
	3438,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	1364,
	4270,
	4270,
	4270,
	4270,
	3438,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	3467,
	3467,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	3438,
	3397,
	4270,
	4270,
	1910,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	1865,
	4270,
	1368,
	3397,
	6825,
	4270,
	3438,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	3467,
	3467,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	3438,
	3397,
	4270,
	4270,
	1910,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	1865,
	4270,
	1368,
	3397,
	6825,
	4270,
	3438,
	4264,
	3467,
	4264,
	3467,
	4189,
	3397,
	4189,
	3397,
	4189,
	4189,
	4270,
	2516,
	4168,
	4168,
	4189,
	3397,
	4270,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4270,
	2525,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4270,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4270,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	2516,
	4270,
	4270,
	4189,
	4227,
	4270,
	2504,
	5667,
	5639,
	5666,
	6206,
	6191,
	6191,
	5667,
	5667,
	6191,
	5720,
	4189,
	4189,
	2525,
	2522,
	2512,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4189,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	4264,
	2522,
	2522,
	2522,
	2516,
	2525,
	2516,
	2525,
	2522,
	2525,
	2521,
	1357,
	2522,
	2522,
	2522,
	2512,
	2512,
	2521,
	2521,
	2521,
	2521,
	2512,
	4189,
	4189,
	2512,
	4189,
	2512,
	2516,
	2516,
	1368,
	2516,
	2516,
	2516,
	2516,
	2516,
	2516,
	2516,
	2516,
	2516,
	2516,
	1368,
	1368,
	1368,
	1368,
	2516,
	2521,
	2521,
	2516,
	2518,
	2516,
	2525,
	2521,
	2516,
	2521,
	2521,
	2516,
	2516,
	2522,
	2516,
	2522,
	2521,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	1865,
	4264,
	1368,
	3134,
	1865,
	4270,
	1368,
	3397,
	6825,
	4270,
	4270,
	4270,
	4189,
	3397,
	4264,
	3467,
	4264,
	3467,
	4236,
	3438,
	4200,
	3410,
	4189,
	3397,
	4189,
	3397,
	4189,
	3397,
	4189,
	3397,
	4189,
	3397,
	4189,
	3397,
	4189,
	3397,
	4189,
	3397,
	4189,
	3397,
	4189,
	3397,
	4189,
	3397,
	4189,
	3397,
	4189,
	3397,
	4189,
	3397,
	4189,
	3397,
	4189,
	3397,
	4270,
	1912,
	4270,
	4168,
	4270,
	1697,
	4189,
	4236,
	2522,
	2516,
	1368,
	1368,
	936,
	2516,
	2516,
	2522,
	1176,
	4189,
	4270,
	3397,
	3397,
	3378,
	6796,
	5379,
	5379,
	5322,
	5836,
	6698,
	5848,
	5854,
	6588,
	5857,
	5392,
	5857,
	5637,
	4270,
	6825,
	4270,
	2512,
	2516,
	4189,
	4189,
	4189,
	4189,
	2512,
	2522,
	2522,
	4552,
	4554,
	4499,
	4435,
	4436,
	4413,
	5157,
	5160,
	5272,
	4403,
	4404,
	4398,
	4270,
	4270,
	4189,
	4270,
	3378,
	4270,
	4227,
	4189,
	4270,
	4189,
	5663,
	5673,
	6584,
	6661,
	5852,
	6588,
	5852,
	6825,
	4270,
	6825,
	6825,
	6789,
	6789,
	6789,
	6698,
	6129,
	6825,
	4270,
	6214,
	6698,
	6825,
	6129,
	6698,
	5857,
	6014,
	6825,
	6704,
	6702,
	6205,
	6702,
	5645,
	6196,
	6698,
	6133,
	6584,
	6584,
	6588,
	6196,
	6698,
	6702,
	6825,
	6825,
	6196,
	6698,
	6702,
	6640,
	6640,
	6637,
	6640,
	6640,
	6637,
	6640,
	4581,
	6588,
	6588,
	6796,
	6796,
	5091,
	5852,
	6595,
	5388,
	5388,
	5388,
	5388,
	5388,
	5388,
	5368,
	5368,
	5368,
	5865,
	5865,
	5387,
	5387,
	5854,
	5392,
	5390,
	5382,
	5382,
	5382,
	5382,
	5382,
	5382,
	5390,
	5390,
	5388,
	5388,
	5388,
	5392,
	5382,
	5388,
	5388,
	5388,
	5382,
	5382,
	5382,
	5392,
	5388,
	5392,
	5388,
	5388,
	5388,
	5092,
	5092,
	5392,
	5390,
	5388,
	5388,
	5388,
	5088,
	5398,
	5090,
	5093,
	5063,
	4708,
	4708,
	4697,
	4697,
	4709,
	4710,
	4708,
	5392,
	5082,
	5392,
	5388,
	5388,
	5388,
	5388,
	5392,
	5092,
	5092,
	5392,
	5390,
	5388,
	5368,
	6012,
	6045,
	5457,
	5457,
	5457,
	5463,
	5457,
	5457,
	5457,
	5457,
	5457,
	5457,
	5157,
	5457,
	5457,
	5457,
	5457,
	5457,
	5457,
	5457,
	5457,
	5457,
	5457,
	5457,
	5457,
	5457,
	5457,
	5457,
	5457,
	5457,
	5457,
	5457,
	5457,
	5457,
	5157,
	5157,
	5157,
	4748,
	4748,
	4748,
	4700,
	4468,
	4423,
	5075,
	6131,
	5644,
	5935,
	6637,
	5429,
	6698,
	6131,
	4270,
	6825,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	6588,
	4270,
	424,
	3089,
	3135,
	3135,
	4270,
	3397,
	3397,
	4236,
	3135,
	1875,
	1875,
	1875,
	1178,
	1875,
	1178,
	3438,
	1539,
	3397,
	1874,
	1874,
	3135,
	3135,
	3091,
	3135,
	1875,
	1875,
	1875,
	1178,
	1875,
	1178,
	3438,
	3330,
	6191,
	5665,
	1136,
	4189,
	6825,
	4270,
	3417,
	820,
	422,
	273,
	4227,
	4168,
	1697,
	4270,
	4270,
	4236,
	3438,
	4236,
	3438,
	4236,
	3438,
	4236,
	3438,
	4208,
	3417,
	2516,
	2521,
	2504,
	2522,
	2516,
	1375,
	2521,
	2521,
	4189,
	1714,
	6825,
	6825,
	6825,
	6698,
	6642,
	6698,
	6698,
	5396,
	5378,
	5396,
	5378,
	5848,
	5971,
	6642,
	5987,
	6821,
	4270,
	6825,
	5388,
	5388,
	5388,
	5388,
	5388,
	6702,
	6205,
	5645,
	6702,
	5368,
	5368,
	5387,
	5387,
	6640,
	6640,
	6640,
	5392,
	5392,
	5392,
	5390,
	5390,
	5382,
	5382,
	5382,
	5382,
	5382,
	5382,
	5392,
	5382,
	5382,
	5392,
	5382,
	5382,
	5388,
	5388,
	5388,
	5388,
	5388,
	5388,
	5382,
	5382,
	5382,
	5382,
	5382,
	5382,
	5388,
	5388,
	5388,
	5388,
	5388,
	5388,
	5388,
	5388,
	5388,
	6702,
	5852,
	6702,
	5392,
	5392,
	5392,
	5092,
	5092,
	5092,
	5092,
	5092,
	5092,
	5388,
	5388,
	5388,
	5388,
	5388,
	5388,
	5392,
	5392,
	5392,
	5388,
	5388,
	5388,
	5388,
	5388,
	5388,
	5390,
	5063,
	5088,
	5090,
	5093,
	4708,
	4708,
	4708,
	4697,
	4709,
	4710,
	6206,
	6206,
	6206,
	6206,
	6206,
	6206,
	6397,
	4270,
	4270,
	4270,
	3321,
	3397,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	3378,
	4270,
	4270,
	4270,
	4270,
	3378,
	4270,
	4270,
	6825,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	1865,
	1888,
	607,
	3397,
	1865,
	1714,
	559,
	3397,
	1865,
	1863,
	578,
	3397,
	1865,
	1869,
	588,
	3397,
	1865,
	3397,
	936,
	3397,
	1865,
	1879,
	602,
	3397,
	1865,
	1869,
	588,
	3397,
	1865,
	1869,
	588,
	3397,
	1865,
	3397,
	936,
	3397,
	1865,
	1874,
	594,
	3397,
	1865,
	1921,
	628,
	3397,
	1865,
	822,
	203,
	3397,
	1865,
	822,
	203,
	3397,
	1865,
	1869,
	588,
	3397,
	1865,
	3397,
	936,
	3397,
	1865,
	3397,
	936,
	3397,
	1865,
	3397,
	936,
	3397,
	1865,
	3397,
	936,
	3397,
	1865,
	1869,
	588,
	3397,
	1865,
	3378,
	919,
	3397,
	1865,
	3378,
	919,
	3397,
	4189,
	3397,
	4270,
	4270,
	4270,
	4270,
	1714,
	4270,
	4270,
	4168,
	3378,
	4270,
	4189,
	4270,
	4270,
	4270,
	4270,
	4270,
	3378,
	4270,
	4227,
	4189,
	4270,
	4189,
	4270,
	2522,
	2522,
	4270,
	3378,
	4270,
	4227,
	4189,
	4270,
	4189,
	3378,
	4270,
	4227,
	4189,
	4270,
	4189,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	1879,
	4227,
	4270,
	4270,
	4227,
	4168,
	3378,
	4168,
	3378,
	4270,
	4270,
	4270,
	4270,
	4270,
	3431,
	1879,
	1874,
	4270,
	4270,
	3378,
	4270,
	4270,
	4270,
	4270,
	1714,
	1869,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	1921,
	822,
	822,
	3465,
	3465,
	3465,
	1386,
	4270,
	4168,
	3378,
	4270,
	4189,
	4270,
	4270,
	4270,
	3378,
	4270,
	4227,
	4189,
	4270,
	4189,
	4270,
	4270,
	4270,
	4270,
	4270,
	1888,
	1921,
	4270,
	4270,
	3397,
	4270,
	4270,
	4270,
	1869,
	1869,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	1928,
	3467,
	4270,
	4270,
	4270,
	4270,
	3397,
	3397,
	3397,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	2522,
	6825,
	4270,
	4270,
	4270,
	4270,
	3378,
	4270,
	4227,
	4189,
	4270,
	4189,
	3397,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	1714,
	3378,
	4270,
	4168,
	3378,
	4270,
	4189,
	4270,
	4270,
	4270,
	4270,
	4270,
	3378,
	4270,
	4227,
	4189,
	4270,
	4189,
	4270,
	4270,
	3397,
	3397,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4264,
	4270,
	4236,
	4262,
	4227,
	4227,
	4227,
	4227,
	4227,
	4270,
	3397,
	3467,
	1177,
	3397,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	2516,
	4189,
	4189,
	4189,
	4270,
	4270,
	3397,
	3397,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	4270,
	3397,
	4270,
	4270,
	4270,
	4270,
	3465,
	4270,
	4270,
	4270,
	4270,
	3397,
	6825,
	4270,
	4270,
	4270,
	4270,
	3438,
	4270,
	4270,
	4270,
	4270,
	4270,
	3438,
	4270,
	4270,
	4270,
	3438,
	4270,
	4270,
	3467,
	3397,
	4270,
	4270,
	3438,
	4270,
	3467,
	4270,
	3378,
	4270,
	4227,
	4189,
	4270,
	4189,
	3378,
	4270,
	4227,
	4189,
	4270,
	4189,
	3378,
	4270,
	4227,
	4189,
	4270,
	4189,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	1473,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
