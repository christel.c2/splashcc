﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* ARAnchorManager_t969330AB785F0DC41EF9F4390D77F7ABA30F7D0F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ARTrackedImageManager_tB916E34D053E6712190F2BAE46E21D76A0882FF2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* DefaultObserverEventHandler_t25FF9CE0FF0ED822CE21D6AE20C6A6E77105C02B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CScaleObjectU3Ed__11_t461C201F3015C43C04CB97A6D8D45A7B73627A6D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CScaleObjectU3Ed__11_t4E9DC2B458A4FA829BDAE1A00561A16F1A8A40F5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CScaleObjectU3Ed__12_t8DC31192573F6DFEF7FBA86C992E3587EAA46457_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_t610B7E25C1F7E862A6CB4B61E84B0023BAA56FA9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitAndActivateU3Ed__6_t503E05974A5610E1E9848BFB9321AE1CE472AE6D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitAndDeactivateU3Ed__7_t0B9854B91CC5946899CC4024F6D32F274A81643C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.ObsoleteAttribute::_message
	String_t* ____message_0;
	// System.Boolean System.ObsoleteAttribute::_error
	bool ____error_1;

public:
	inline static int32_t get_offset_of__message_0() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____message_0)); }
	inline String_t* get__message_0() const { return ____message_0; }
	inline String_t** get_address_of__message_0() { return &____message_0; }
	inline void set__message_0(String_t* value)
	{
		____message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_0), (void*)value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____error_1)); }
	inline bool get__error_1() const { return ____error_1; }
	inline bool* get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(bool value)
	{
		____error_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.SpaceAttribute::height
	float ___height_0;

public:
	inline static int32_t get_offset_of_height_0() { return static_cast<int32_t>(offsetof(SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8, ___height_0)); }
	inline float get_height_0() const { return ___height_0; }
	inline float* get_address_of_height_0() { return &___height_0; }
	inline void set_height_0(float value)
	{
		___height_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.ObsoleteAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868 (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.SpaceAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[2];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[3];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void TestingPunch_tD3C862FA45AA792897C92A24D8EB232568FB4232_CustomAttributesCacheGenerator_TestingPunch_U3CUpdateU3Eb__4_0_m573E0207272AF5B03F77EBE674FC9B7C0B49791D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TestingPunch_tD3C862FA45AA792897C92A24D8EB232568FB4232_CustomAttributesCacheGenerator_TestingPunch_U3CUpdateU3Eb__4_3_m4CC34D2970098316C32EBE67B368AB199A20A2E1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TestingPunch_tD3C862FA45AA792897C92A24D8EB232568FB4232_CustomAttributesCacheGenerator_TestingPunch_U3CUpdateU3Eb__4_7_m2E2AD49C20FF476ED389803BE00C0C87495D0DFC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_t038683C2E93B5073D484142AB01DE825CEEDBEBD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_1_t60F636DD05421DE520F34389F79491ABBEC7296A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_2_t657E0C1A8D3477F99ABEE7DD54418FC518C44D05_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tB1B120168A6E7BA6AD56D21520A093EEE79C30FB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GeneralAdvancedTechniques_t9CC1EDE627191C21C904074F94D92D26F45AA70D_CustomAttributesCacheGenerator_GeneralAdvancedTechniques_U3CStartU3Eb__10_0_mBBEFB6358D20EC1EFC4D6B51139D0DEEDFF86AD2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GeneralBasic_t67AF63F77C4D821EE283A4220DAC04A9008315E4_CustomAttributesCacheGenerator_GeneralBasic_U3CadvancedExamplesU3Eb__2_0_m706BD8DE22C36E9A7EF6326EF1B7EAB6C780BF3D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_tB541BE7A9909628CA87B0D4AB005DDB3FFAB3A6C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GeneralBasics2d_t2BC39F63283747E77CD6462197411F81BD9C59AB_CustomAttributesCacheGenerator_GeneralBasics2d_U3CadvancedExamplesU3Eb__4_0_mFB34B228D53D66D2DA60CBE36C863AAC135AAE41(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_t58B048B073D31EB8F5A9ED27B5DE2730EFCFB3BB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_tEFDABD6CC1CBBFC6AC7A112C3717524ECAADB421_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_1_t0F31BD07E1BCC48A7ED53ACEEB2FB3105D312E6F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_tEF56FCF80D4C652E8CC501F5CF0D88D34D01D386_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GeneralEventsListeners_tB79AB185DA5D044126B7C32FB156D3B4DFE3F733_CustomAttributesCacheGenerator_GeneralEventsListeners_U3CchangeColorU3Eb__8_0_m8D567E7710ED42E4ED9DF3074F1F9E4AF19AF1A1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GeneralSequencer_t6D018FEB941E2BBA2F3BB9DB0EF288F40C8C1D92_CustomAttributesCacheGenerator_GeneralSequencer_U3CStartU3Eb__4_0_m01F09B25E4652D0AD12402145A95897D0E65C36F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GeneralSimpleUI_t15D77C2AB58F89C178F7B20E1590A36E240E5FA8_CustomAttributesCacheGenerator_GeneralSimpleUI_U3CStartU3Eb__1_0_mCC061524D85ACD8BAF6FE7D8CE1F2277735B3FC8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GeneralSimpleUI_t15D77C2AB58F89C178F7B20E1590A36E240E5FA8_CustomAttributesCacheGenerator_GeneralSimpleUI_U3CStartU3Eb__1_2_m32632A4DB6420C70DED2CA0F5D16A52ABED4D202(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GeneralSimpleUI_t15D77C2AB58F89C178F7B20E1590A36E240E5FA8_CustomAttributesCacheGenerator_GeneralSimpleUI_U3CStartU3Eb__1_3_m81A54F1B0397D24C3ABC0367E5F3A96CD8938D25(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t0FFE348F3AECAC4BFE5D4196F3BBD8CC15874A96_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass15_0_t30EA498BE1CED5C189EDC1015FB8AC828E008426_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28_CustomAttributesCacheGenerator_PathSplineEndless_U3CStartU3Eb__17_0_m89EDA38D298348E747F008DED4C1CF7D4F815D60(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PathSplines_t5E3D3A23634F2898542C5C6F650D03565C9C2D6A_CustomAttributesCacheGenerator_PathSplines_U3CStartU3Eb__4_0_mC304E252D7336E181BBEF0F27B79311DEDF4BB0B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t8ABF57D71EAC0C260D48F0C6C5CCC9C3D247FBEC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tD7C586F26F27190F29F8B90744BCE69E24E2B0BB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_U3CeaseInternalU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_U3CinitInternalU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_get_easeInternal_m263F859E9225DF85EFD75C079CD5073AE647B284(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_set_easeInternal_m40A3FEDCAA0EE7EB86E804641367E50E34C0CDB9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_get_initInternal_m3E9E4486FBFCFFDB8A3A91EB00DBEBA29916C943(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_set_initInternal_m21E7CD474330836550F6298EF7F0F2902BC3EE60(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_cancel_mC12FD3EE1E42AFCC7AA536A6D344BE3AEA86AE70(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x27\x4C\x65\x61\x6E\x54\x77\x65\x65\x6E\x2E\x63\x61\x6E\x63\x65\x6C\x28\x20\x69\x64\x20\x29\x27\x20\x69\x6E\x73\x74\x65\x61\x64"), NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveXU3Eb__73_0_m39FBF744669510650F08E0F2B4E87CDEECA7440A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveXU3Eb__73_1_mB4AD58106C3D205E5D4763F3F1CCA63B2F99EB78(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveYU3Eb__74_0_mA6309FBB2AF9AEEB859024E69AC8ABD6D102AD87(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveYU3Eb__74_1_m7A2A40165003571F84F057D3F39BD77A23B2F0E9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveZU3Eb__75_0_mFE473C92996CA9EBA69397DA460F4B16EDE7D03C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveZU3Eb__75_1_mF78C868A90B38D04ADAD2AD93D536B1791B3C671(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveLocalXU3Eb__76_0_m8A895E545E84F266EA0E0963C68274D9545D17BE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveLocalXU3Eb__76_1_m17F56B25DDE0E4E6D6CC186BF9A9A617957C2FD6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveLocalYU3Eb__77_0_m8D00D189D0ADCF6C778EFF39DCA1EA55C03E34F4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveLocalYU3Eb__77_1_m67ADD5EAA303DB53DA4B896F2080F3E6975C76A6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveLocalZU3Eb__78_0_mFD9EBD1D9EF08CB93FDCCE06D88DC940852C03DF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveLocalZU3Eb__78_1_mF3D05D5EA84618939D8F8C0F9F06EE5D6294D77D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveCurvedU3Eb__81_0_m8BD948AD31BE03BCEF9D75091AECC3DF8B4A50E3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveCurvedLocalU3Eb__82_0_mE35CF92421F9DBB5687F1F1608AFBE9F86C80378(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveSplineU3Eb__83_0_mD9ACDB850FD978554B5465139A4E1EDD844CA1BB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveSplineLocalU3Eb__84_0_m3219BB023C406080046DE37EBCF0CEDB8E323D1A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetScaleXU3Eb__85_0_mDF33F215E49CCD02AE02FDD79827006CB41DF18F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetScaleXU3Eb__85_1_m332A3ADAFDC200BF9AC11279593B5968DC537623(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetScaleYU3Eb__86_0_m6A34E540DCDF955D835FCBD7FEF708257E07FE31(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetScaleYU3Eb__86_1_mE1BCEE0DADE70D6CA3873140143141D9B51DBDB5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetScaleZU3Eb__87_0_mAEDB3F2903B250EC9885F64B147EDBDD4881E812(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetScaleZU3Eb__87_1_mEFAE5A48C329D33CAF5F7003D48DD0B9C581AB85(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateXU3Eb__88_0_mD0976C92A94804F53718DEE26B131989A067EF7A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateXU3Eb__88_1_m7C171EAF890568C630E3BE1EC70C97A0C55F0984(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateYU3Eb__89_0_m1B2E4D04C35A72D3957FEB294B306BF741228F1B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateYU3Eb__89_1_m67AC1C0BCE4902B58FA999FC9475BF42D391F167(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateZU3Eb__90_0_m92CFF50F82E8606EA58D1FF72D2FBD7C9A0C7290(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateZU3Eb__90_1_mA55CC8D92F2601E04F21F45337134FBE437A20B6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateAroundU3Eb__91_0_m90372B54B5D631FC5E1FF67FCE64F69A1E0E7E63(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateAroundU3Eb__91_1_m21A791E3630653D459153347755624070DC38965(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateAroundLocalU3Eb__92_0_m49A28FAA34B36D7AA09A322D63974997CB31BF43(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateAroundLocalU3Eb__92_1_mCB4C19C5A763BBF640E5BE6785D5AB03859AD606(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetAlphaU3Eb__93_0_mADB8DE8A613762F896CC7754539BFFCA3A23B4E9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetAlphaU3Eb__93_2_m5318309F91340E1C143196D9ADA659C53B3C918C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetAlphaU3Eb__93_1_m44E2198D69A4F502CE6F5B1532AFD8F600D84717(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetTextAlphaU3Eb__94_0_mE551CB2CBB6114CA72656A033CAC42EAB2EFCC8B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetTextAlphaU3Eb__94_1_m809E895E8E9ED4049E44ACAEEFE222EBA2E6A259(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetAlphaVertexU3Eb__95_0_m53AB148A8D470C29DACB58274436B9535F5CAC37(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetAlphaVertexU3Eb__95_1_m197F71CB84DF1265DDDEC92AB60CFAEC2B2E8BE9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetColorU3Eb__96_0_mF63A06E57C3B0A1B5B8FE3E7EF131BD862457B19(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetColorU3Eb__96_1_m8EA4C07F32943F895BE0CB6909C63263827CEBF8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCallbackColorU3Eb__97_0_mAD825635FB3FD1DE688BD692F394C91BD701659F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCallbackColorU3Eb__97_1_m3635B1AEC7852BD361B57DAC85568D84BB050C38(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetTextColorU3Eb__98_0_m1AC4C496FB90FA9C994A7D8772EE75A96D8C7499(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetTextColorU3Eb__98_1_mF8C5941A01CC6AB44A3C1A9E4E0D42DE0019B1B5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasAlphaU3Eb__99_0_m03D22099CB93845FEE24F439C52C194CF424307B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasAlphaU3Eb__99_1_mC255CF9153C3E078BCC1E35C16E786C8662EBFD8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasGroupAlphaU3Eb__100_0_mCD1535F45FF98C078658D56A058E4D44D7CD47F2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasGroupAlphaU3Eb__100_1_m6EBBBBA42BCC9B435DB26932FCCD7DF904E3D31E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasColorU3Eb__101_0_mC7AB0F758514D92910F073FB38A41C395E6764E3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasColorU3Eb__101_1_m0DC35761F636C62A4B9A9D9B7C8EEFD9B30F3CA4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasMoveXU3Eb__102_0_m0E72A6C3F16FC694B6D54907490ECA5D6D359776(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasMoveXU3Eb__102_1_m7FF531D0062E0803860A0CD456E6DD6AE30BD00D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasMoveYU3Eb__103_0_m4730D57FCDEA041D3F4111E45E34C15149C4BD4C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasMoveYU3Eb__103_1_m9CF415CA444CEBB9B5B6C01B337E1C4598B74A32(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasMoveZU3Eb__104_0_mDCD6897BB64C355669F8EE9CFD8F2B386F830CA2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasMoveZU3Eb__104_1_m5B12F7E220DA96B6F05025A4BE1AFABDEEAFF853(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasRotateAroundU3Eb__106_0_m7254F615E75865226968EA098B90FE904EE5B38B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasRotateAroundLocalU3Eb__107_0_m2C7BE76A27280304C47F1787199EA2790E10D6D6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasPlaySpriteU3Eb__108_0_m60117A734E7707143FD04D2FBEE6B75618B258DF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasPlaySpriteU3Eb__108_1_mAA1E46E79560810DA46AE8EA0BFA91EDAE3D2DA5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasMoveU3Eb__109_0_mABE1F4262412CA7E93AA4B71881FAE5B817FCF8D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasMoveU3Eb__109_1_mD190E0829649FEA47BAB95DED30B6EAB95C03ECF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasScaleU3Eb__110_0_mF68FF782BFAD2E93A65661953E683EADC9A52DA0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasScaleU3Eb__110_1_mCF2CCB10E46FC5F00061B020C857E7A35EF14E0D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasSizeDeltaU3Eb__111_0_m021163E83008DE7874346B03AE065037CDC34D3A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasSizeDeltaU3Eb__111_1_m9AA7511D4E1BF277C282F3E30388E82D0A9E1C0B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveU3Eb__115_0_m5DDD8D43FEBB5099535893334FD76E1F0DAFD21B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveU3Eb__115_1_m6B66D8C95AE011E8C641FA4AEF0245809F334399(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveLocalU3Eb__116_0_m7FE28EDDA5832CD6FF6BB2370DA6059DF23B2A1F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveLocalU3Eb__116_1_m8D5A11FA82E103327A9ABD0CA5BB397952CD260C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveToTransformU3Eb__117_0_mCE51DD0DF5028FDE555344C8FF602CD98716C33B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveToTransformU3Eb__117_1_mACD7BEE7BA65E5DCB5B234EB3A31D9D8C3ED7FA8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateU3Eb__118_0_m3ECAD3ACBA191CD5E6FBB67BB3E3AAEDA454B4D3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateU3Eb__118_1_mEB2249D09D0E69826AE6036FD448AF92D77B47D7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateLocalU3Eb__119_0_m2AA0D5D9B5334CEC12D6E8670332C4BEEB5CD7DB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateLocalU3Eb__119_1_m64E3FC9E52EE3EB30AA9260408C3D02F24A34064(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetScaleU3Eb__120_0_m4528B485CAF7246E73A7E602504675D4B11A44B7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetScaleU3Eb__120_1_m4A1D3D78C772991EC422F7D2D29EF4D069E39C78(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetGUIMoveU3Eb__121_0_m4AD272AF144CD468B048F733B93D1DFC08527804(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetGUIMoveU3Eb__121_1_mDBDABB8CB0F1D7CC05BF861826BD23B1A5CF68B3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetGUIMoveMarginU3Eb__122_0_m92AE989BFBF68D262DDFD12EB9DBBE05BDCE7AB1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetGUIMoveMarginU3Eb__122_1_mA60DFD7F27C11D0B55761F7A0936D8056ED09382(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetGUIScaleU3Eb__123_0_m5A0FC254BDC374AACB5FED5922852DD294CBA77C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetGUIScaleU3Eb__123_1_m01CD6A1BE962B34E940B420723AE967C9AF4B248(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetGUIAlphaU3Eb__124_0_m584C608C8B326C3B0401CAA8C696F8ABCF95DD38(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetGUIAlphaU3Eb__124_1_mF0CAD09EF53473D82DF70172C27DA602272E3361(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetGUIRotateU3Eb__125_0_m8F9A74633240B6083A3C1DB7EA24F762D40589C9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetGUIRotateU3Eb__125_1_m43CF134D7443CAA354F69A7031CE9F2574CE4DA5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetDelayedSoundU3Eb__126_0_mEF9FE367DCCC3DDE76DA47246D17F221C0F4D93F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3CtoTransU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3CpointU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3CaxisU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ClastValU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3CorigRotationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3CpathU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3CsplineU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3CltRectU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConUpdateFloatU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConUpdateFloatRatioU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConUpdateFloatObjectU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConUpdateVector2U3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConUpdateVector3U3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConUpdateVector3ObjectU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConUpdateColorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConUpdateColorObjectU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConCompleteU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConCompleteObjectU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConCompleteParamU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConUpdateParamU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConStartU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_toTrans_mB4912C34FDE4BB3CE94B86FDE3D7D8C0DE951773(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_toTrans_m4C747BFF6573BF8C2484AF01C3FCCC65E8A54FB9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_point_mB79A8D97B381FE3122B1549DBB3527F9691A1156(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_point_mD7AE42D3184ED0D66545DC3A98B5CB5759DF8627(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_axis_m546D8ADD4003D9B69F7EC4A243008064AC42DB20(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_axis_m17A070B698AFE6D066C10EEF1A86DDEABC0A13B0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_lastVal_mD84B4C7F0D87E853D6EB9786261068DA74031DF8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_lastVal_m1B2DC6C4CE540BA60BD467F4394CEF18409B820C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_origRotation_m1B98DB0AACC7AF940C43DC29DF7A61FE6E49736E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_origRotation_m6E9E885A6259D7DE7FD1D398CABD7F7CB1751DE4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_path_m20A38F266BEEDD6370F8F54B2FA58C7262808AC5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_path_m10D4A506CACD00E0951E6E5E41003DE99C72DA64(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_spline_m32BE3A98427C2A4FBE40E1BAEDEC2246ABD3D4F2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_spline_m5FF9921610A1F8C38E004CF9129F185BC7FDD708(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_ltRect_mDBE34D781428B8DD07A8F25AC32B9B0932B62BE7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_ltRect_m273BF61CF793266842167003657D7083E62B655A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onUpdateFloat_m8573CE5DFEFBA6E350AF056485B265FF59E8072B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onUpdateFloat_mC6B9DD9721D66C62B4F196BFB0AC6EAFFD98A10F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onUpdateFloatRatio_m6757E471D3ADDB9202062289E6EFAD3B61B2C7F4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onUpdateFloatRatio_m0FBA91E49D4D592E2CAA88BF2963B2B5737ACEF7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onUpdateFloatObject_m8A282C0573D32731C92CA8A4E352382CE73E4839(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onUpdateFloatObject_mF14622BEBB5CCF990BC0DC9622CE2EE037F364C5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onUpdateVector2_m2692F9A7698099312B1956EF89ADC677E156325A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onUpdateVector2_mC9D4DD0709714E0D052A2A6E475099B146375331(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onUpdateVector3_m1A96786092458EFAEAB22467DC6AA01F2F34247A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onUpdateVector3_m91295D4F19AFF3080F6B44A071118CCDB52F288B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onUpdateVector3Object_m2CAC540C86A54F7744AE39443FB4054066E3DE02(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onUpdateVector3Object_m2ECE95CB5D7DFE5E9DEDB9F3B03BEF39D4000B77(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onUpdateColor_m78FBDE4D9B8856EBE7EBF42709CFF81E127B9A43(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onUpdateColor_m7D77082D84B3831810A825A0628CE556012AAA2B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onUpdateColorObject_m45EF15CE7E1F771A04B6E9374323759A7A021C62(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onUpdateColorObject_m25F9B89775D1630387BEDF4DC8B2F88F8E4842A0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onComplete_m89C63A691247B03AF894749D3F32987E7994CFA8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onComplete_m71A16E08537D67C75C706DD0EB88B7954A0CFCAF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onCompleteObject_m50BB3E137E79340957CEE096A24A3635ACD91DEA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onCompleteObject_m70FA4651F7E2CA76267B09C3EFBEA8AFE62F95E3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onCompleteParam_mD546B9E0E31E70CEF0514DFB486D49E4BBE9D120(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onCompleteParam_mE4F5C590CFA4FF8204FE0C4722BF7EEBC1F4D7C7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onUpdateParam_m8568043E9421D14172B62712FF0519738F0E8D88(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onUpdateParam_m415D9E73546338141E97EBEB8587C8A6F00C18E6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onStart_m67196E7D6B56A68A3D7A5BAC7201CB5D79575EEF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onStart_mFC780FBC4B2363070A9395BAA7693ACBFA3C75F8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTester_t537C3FE41ED244252241E5B02F6B1BA6DA437297_CustomAttributesCacheGenerator_LeanTester_timeoutCheck_m59067A5600F0D2434CE9B7EDAC062829C85D5C00(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6_0_0_0_var), NULL);
	}
}
static void U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6_CustomAttributesCacheGenerator_U3CtimeoutCheckU3Ed__2__ctor_mACFA1EA79EDBEA235C7474D1BA3E71804A23FEB8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6_CustomAttributesCacheGenerator_U3CtimeoutCheckU3Ed__2_System_IDisposable_Dispose_m0AF241B0BA496E508C41A00A92ACCB507F764F3B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6_CustomAttributesCacheGenerator_U3CtimeoutCheckU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF1EF5FF8E038CD04BB438F282E437032A52DF973(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6_CustomAttributesCacheGenerator_U3CtimeoutCheckU3Ed__2_System_Collections_IEnumerator_Reset_m377EF3F9C3E2B3A09D749A0A347B92595F0D756D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6_CustomAttributesCacheGenerator_U3CtimeoutCheckU3Ed__2_System_Collections_IEnumerator_get_Current_m275B7C4BFE741FFBA1D7CE3E422460E5BD15EFF6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_CustomAttributesCacheGenerator_LeanTween_pause_mAF75D7E636CB0D02F8D515FF76C23D99B8B28FB1(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x27\x70\x61\x75\x73\x65\x28\x20\x69\x64\x20\x29\x27\x20\x69\x6E\x73\x74\x65\x61\x64"), NULL);
	}
}
static void LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_CustomAttributesCacheGenerator_LeanTween_resume_m1940BE978DB7EC3606D92C181FFE10A4570043E2(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x27\x72\x65\x73\x75\x6D\x65\x28\x20\x69\x64\x20\x29\x27\x20\x69\x6E\x73\x74\x65\x61\x64"), NULL);
	}
}
static void U3CU3Ec__DisplayClass193_0_tAD697F408BB575C622FD5E018C1F75AB28CBAF48_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass194_0_t6FE45F50B4F93FED778E7D54F0EF582EA8252814_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass196_0_t4FA263E9CE532093F4513F5750D54960760E3F78_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD_CustomAttributesCacheGenerator_rotateFinished(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD_CustomAttributesCacheGenerator_counter(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanAlpha_m622BA3E2E95AA0C3ED769D14693452C71B464202(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanAlphaVertex_m276A64D30C7DBC2962D952F39164E74266A80805(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanAlpha_m5D4F86AB57DEAB1788098157DD3858155B41A026(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanAlpha_m80C6BB78F56CBCBAF562EA721D66F568AEB253E5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanAlphaText_m2E879D95FD0A499D43742BC7E19C70FC00DB3C2F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanCancel_mF59992A82C61655FB9C3C02CAA26A659B9DE3646(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanCancel_mD017D8DDC025544954D2E68BC260FC83C9469661(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanCancel_mC35E34FD1DF38362DA3970677A1A3745F02873E5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanCancel_m17794FCEC25F233AC350724707F017F476BEA6B1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanColor_m444318E458A875BB4CB2A0DD294D992CD93EBEFD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanColorText_m400922DC92DE54BE07F329E4949145CF60571731(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanDelayedCall_m2377F6B8E51CB5CDEA727E0ECB5719E78E1E6DC8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanDelayedCall_m813456BD5A6C3BEF150219E1FB7FFECF3CFDB8FB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanIsPaused_m004A9E480599A969A094BBF5EAB8298D25F72877(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanIsPaused_mAB912B24C71D148315C8E71232F9B54C2FADB5A4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanIsTweening_mECC74A5F933AEEEC71D8EBE7E977639DDD124F59(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMove_m054FBB59F420198B550A6069D9F9E6ABAE6AB43A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMove_m615AA30F73F0ADF408BCE5B3EB2E7B0107F613DF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMove_mE09C2744CD6F66260CCB2AE85EEC189D1739A95C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMove_m213A2BEDDDE4818C47346D971A3D99CCE02F1E51(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMove_m0A44978D179428622357E68D3AE31FCD5597B014(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMove_m47C512A7C68F548F0C20D61A9EBC21444A89F47F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMove_m28CCB67D4CD2B89B5A3F25E9D70842C949AD0990(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMove_mB1E50A4734E3C3CC223FEB98A394DBDBA9614850(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMove_m0BF63EB3A8D4B614817EA402EB1CC4B5F2C83328(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMove_m184D419AE52BA8FFCD237883D4118434DA2F40BB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMove_m31A80E7BD7568CEFF3D7B4073D2BA176A9ADF3DF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveLocal_m15178FB562F6F85C35B682AF97FCA004BDA1946F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveLocal_m91600FA81E8D5DB2D9C69E4F7C5FC09CC40D2DC1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveLocal_m98D7F1A2052D0F25DEE7B58DE411EE62A8B27CF2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveLocal_m920B50D3FBD4DC072118E879AFC151D682AE64B0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveLocal_mE4CF4E64AC16B510FB904060DDC2AE8BB231AA51(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveLocal_mA49B57CD94C59A10CA8AF9D53F71C48286EAFFD0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveLocalX_m8847296A8B3401D2D054027ABB659B296FF73760(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveLocalY_m86889CA4B214309EEAC239C3F175937E24310FD6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveLocalZ_m248064CFFDF7E42AC612D7F72C1E0F2DA37DCFB4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveLocalX_mEAB28137FE65A74C1F4963ADCFF044CD5DB7CF69(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveLocalY_m727F1ED96B0D8FBF32EBB69787F82561886A07FA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveLocalZ_m731E4A396200E9C165B47A327CB59CCDB9817707(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveSpline_m0E5CBB6704835E27EDA9B25D2915E8D2360E11A4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveSpline_mF640B6FFF8B58004A5C0A4475FE36E2C5BBAC607(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveSpline_m3EE259E4A0061A1497D195E7910EAE9CD0B984C5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveSpline_m59EF921EB07DAFBB715DAB0BBCFAC8003E67BD6C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveSplineLocal_m667397DE03EFD1DBE0CE5F6CC2287807C3A56761(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveSplineLocal_mB8706C86995411A7D6408A04AB6B3545922C192D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveX_m95E776B4AFF3B3472159C21ACCA7A074FD6250E1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveX_m5EA136BF257503853FF984AB882BADCEDD22F864(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveX_m2E387E7F079BC81CCBE68B4D705C31E581639352(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveY_mD3811BDA209686DCA906AB52B2CDD29F337FD6F4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveY_m65DEFE466DE737FAB4B1DEF0BD8DC8ED22D03433(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveY_m846E64F6EED274DD02405DF113895FD366BA0FBB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveZ_mC2E89CF6F065D18A1F456935AF7EFA51091F0D9B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveZ_m85D8863F40BBF5A99203396C72B421E022FAA417(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveZ_mD47791E76E05328EAE363F9A3D0B8DAE6713902A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanPause_m3B5948BE9F86AAB0C837383D547DF2B291B8EA43(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanPlay_m6DCF6A71031221B880B66660EFF17720C6F1A42D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanResume_m0C9B09411CA1C367B2E2F1E74ECC4860BB400BF4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotate_m9A3EA584BE26EADB024F0E5EC57B0862F3B0BEAE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotate_m46EDCCD0F931B4488BF8EDE72F0B1BA6BD65261F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotate_mF34054132FFA550762A3C04473FEF3FB54931CA0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotateAround_m97EA89B0C3FDE7B29B2A5F2FE3BE10F27B0C9993(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotateAround_m9922AD8260E8767A2D33963F2C9BA18ADDF22D61(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotateAround_m7F47B45F2074B30B22D75DC117BCF0A1351710E3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotateAroundLocal_m43F940634C80C6D459B9DACF22F06E1E42F29116(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotateAroundLocal_m81397BB2C883AC5B3D62716FC3C93F43AE4EE5EB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotateAroundLocal_mE1E669F2BCAAEA47AB4785D6BBFAE413CC74F053(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotateX_m0C6142BF35F74AC9A912509713E53E9F1B3F1A20(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotateX_mD09A3A174615A5479F81DFF31192673DADBF3324(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotateY_mE334362113CE7DA83AB29EFA1AAB8BFEB8C326E0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotateY_m823DD2E4B248E09CA4E2CDDFD2D791DF8527FCD2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotateZ_m587B987933373ED9B32F6D319F158899A25AF9C3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotateZ_m25AA9563CCFC5754ABE0CDE84FE8E05F09B0357F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanScale_m883D32607662D32DAB92DAF865CF110680BF18B3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanScale_m572C2CB624F08B589CA3F7CEDA18D0521D396DAF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanScale_mFDA8A475A889665712B39717572167439241C6D5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanScaleX_m9AC6DED15F097E50134DF4213F86799F3A3F370B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanScaleX_m469A49E471B4D706CF5859D368CD3F13DD4FBA67(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanScaleY_mEF7B8B2B411B70E312A061227CC9EF7496F1A720(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanScaleY_mC152F2D04B0CB5770AF0157B8BA217DDC0881D61(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanScaleZ_m23CAE903347B1DFEA59FFFB127419620967227F1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanScaleZ_m2C148ADC5EA94A54CC00F349B1DF331079B13B16(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanSize_m410361AD3B78FE2A69D65C21570490F82E660241(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanValue_m1FB1FFD9157FA40B5854744EA15111178AEDE1C1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanValue_mE9099E0F64A010AD2BEA7030C5FEFEDBE78A1571(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanValue_mF09B8E5D72CBADA7E7335A1CCE8397B6BEE0B286(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanValue_m165CCF09A13FA398105E06E6B441D0C5BEF33E20(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanValue_m2D0676A4B078FE10C1819ECF5520374CA2E39E8D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanValue_m626C6C95ABDA16428DAD1399B0862363589AC9F9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanValue_mBB6A5387301F308B4C42B4DF5B70C17AE29F5533(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanValue_m93C212023658F9EF6A27683DEBAA852EE4670411(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanValue_m5B60256ABE5F8B7E28F389AAE1696EB656633398(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanValue_mBA982D980197033944CF17D7C7F236A6CED885DF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanSetPosX_m82201BB5C9781FA41FB69DDA05471D693E59DF2E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanSetPosY_mD9DAE6B462055110382822D16B5551854CB68DD7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanSetPosZ_m1E17DB053DD69876E163757D053A85B172CAC027(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanSetLocalPosX_m2325658730387BE391CB8FE674FD3FD2D8D3ECDE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanSetLocalPosY_mF14BA20EFA312158AB0FD456EB8762457DB03AE5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanSetLocalPosZ_m685180D2C04DF7E3417E9EECA9BEBE60F0850790(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanColor_m2027EB778950A5E71F3DCF398F8A6A77AE32EF35(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ImageRecognition_t3FA1D134672565B626D02FABA8BCB9732ACFAC23_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARAnchorManager_t969330AB785F0DC41EF9F4390D77F7ABA30F7D0F_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARTrackedImageManager_tB916E34D053E6712190F2BAE46E21D76A0882FF2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARAnchorManager_t969330AB785F0DC41EF9F4390D77F7ABA30F7D0F_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[2];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARTrackedImageManager_tB916E34D053E6712190F2BAE46E21D76A0882FF2_0_0_0_var), NULL);
	}
}
static void ImageRecognition_t3FA1D134672565B626D02FABA8BCB9732ACFAC23_CustomAttributesCacheGenerator_bookPages(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec_t5B5DA66907FD15891D64D11538E77E3537A4AB4C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Float_t28B5D79DA5B24D79FA3F8F8DC65BCC9791E16307_CustomAttributesCacheGenerator_speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_arCamera(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_gameLight(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_arAudioListener(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_U3CCurrentPageGameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_GameManager_get_CurrentPageGame_mBB7545C1F39191029C356CE8A0CF621C05A0466D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_GameManager_set_CurrentPageGame_m9D471B21E93F777A181EC198961365863F981851(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607_CustomAttributesCacheGenerator_minTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607_CustomAttributesCacheGenerator_maxTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607_CustomAttributesCacheGenerator_scaleObject(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607_CustomAttributesCacheGenerator_U3CCurrentStatusU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607_CustomAttributesCacheGenerator_GameScale_get_CurrentStatus_m38D0B805AB43526506AAFAF0BCFB3280942720AC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607_CustomAttributesCacheGenerator_GameScale_set_CurrentStatus_m15A0A482C1931D16C7AA8CAB0CE5A03E0C45B03D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607_CustomAttributesCacheGenerator_GameScale_ScaleObject_m0585571AAE75600F571926D1F9F72348F82407BB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CScaleObjectU3Ed__11_t461C201F3015C43C04CB97A6D8D45A7B73627A6D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CScaleObjectU3Ed__11_t461C201F3015C43C04CB97A6D8D45A7B73627A6D_0_0_0_var), NULL);
	}
}
static void GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607_CustomAttributesCacheGenerator_GameScale_U3CShrinkU3Eb__12_0_m2F0F00081290A6FC6F7AC0290DE44009D3B01527(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607_CustomAttributesCacheGenerator_GameScale_U3CGrowU3Eb__13_0_mD9600484A0A19E931BAE06A6BCB1EEF0A96AF277(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CScaleObjectU3Ed__11_t461C201F3015C43C04CB97A6D8D45A7B73627A6D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CScaleObjectU3Ed__11_t461C201F3015C43C04CB97A6D8D45A7B73627A6D_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__11__ctor_m9F9912F1FE2061B80649C3E2D3EE03F0C81A2ED2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScaleObjectU3Ed__11_t461C201F3015C43C04CB97A6D8D45A7B73627A6D_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__11_System_IDisposable_Dispose_m1F6F9E741217D4A31F59B0AB382F7EA581962124(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScaleObjectU3Ed__11_t461C201F3015C43C04CB97A6D8D45A7B73627A6D_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA18277648E8ACA4C90D1053F66BB3A45B40A6504(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScaleObjectU3Ed__11_t461C201F3015C43C04CB97A6D8D45A7B73627A6D_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__11_System_Collections_IEnumerator_Reset_m03133167C210B463F8914107D14565D5EF30F003(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScaleObjectU3Ed__11_t461C201F3015C43C04CB97A6D8D45A7B73627A6D_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__11_System_Collections_IEnumerator_get_Current_m3D9D148281A0AE0E2A411244F4636E11A8AC4541(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867_CustomAttributesCacheGenerator_particle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867_CustomAttributesCacheGenerator_pointer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867_CustomAttributesCacheGenerator_Idle_WaitAndActivate_m70D559158D8C1EDF0B9B44BD9823D4611AB2290B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitAndActivateU3Ed__6_t503E05974A5610E1E9848BFB9321AE1CE472AE6D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitAndActivateU3Ed__6_t503E05974A5610E1E9848BFB9321AE1CE472AE6D_0_0_0_var), NULL);
	}
}
static void Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867_CustomAttributesCacheGenerator_Idle_WaitAndDeactivate_m59520F96D5799F5496ADC573057933A6555CD144(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitAndDeactivateU3Ed__7_t0B9854B91CC5946899CC4024F6D32F274A81643C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitAndDeactivateU3Ed__7_t0B9854B91CC5946899CC4024F6D32F274A81643C_0_0_0_var), NULL);
	}
}
static void U3CWaitAndActivateU3Ed__6_t503E05974A5610E1E9848BFB9321AE1CE472AE6D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitAndActivateU3Ed__6_t503E05974A5610E1E9848BFB9321AE1CE472AE6D_CustomAttributesCacheGenerator_U3CWaitAndActivateU3Ed__6__ctor_m372FCD69AC54F9B6F417E419D1CBE41AFEBAF14A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndActivateU3Ed__6_t503E05974A5610E1E9848BFB9321AE1CE472AE6D_CustomAttributesCacheGenerator_U3CWaitAndActivateU3Ed__6_System_IDisposable_Dispose_m0D3874C983C3F7471BE43376345B2AA14B702A36(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndActivateU3Ed__6_t503E05974A5610E1E9848BFB9321AE1CE472AE6D_CustomAttributesCacheGenerator_U3CWaitAndActivateU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m41C234EFF7D21326815858026BE32FE067EF619F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndActivateU3Ed__6_t503E05974A5610E1E9848BFB9321AE1CE472AE6D_CustomAttributesCacheGenerator_U3CWaitAndActivateU3Ed__6_System_Collections_IEnumerator_Reset_m5C8CE0D8F95DDAB7A3EFB667513B448F26BCE043(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndActivateU3Ed__6_t503E05974A5610E1E9848BFB9321AE1CE472AE6D_CustomAttributesCacheGenerator_U3CWaitAndActivateU3Ed__6_System_Collections_IEnumerator_get_Current_m1D86BB210D7EA8F9BE13DD49A67154BD11E2BFAB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndDeactivateU3Ed__7_t0B9854B91CC5946899CC4024F6D32F274A81643C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitAndDeactivateU3Ed__7_t0B9854B91CC5946899CC4024F6D32F274A81643C_CustomAttributesCacheGenerator_U3CWaitAndDeactivateU3Ed__7__ctor_m3557474DAA9C5DB0CD5BD4032344C0A53452B250(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndDeactivateU3Ed__7_t0B9854B91CC5946899CC4024F6D32F274A81643C_CustomAttributesCacheGenerator_U3CWaitAndDeactivateU3Ed__7_System_IDisposable_Dispose_m9FEB66AAD863C5A40D5E5420E53BFB2E7586E4D8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndDeactivateU3Ed__7_t0B9854B91CC5946899CC4024F6D32F274A81643C_CustomAttributesCacheGenerator_U3CWaitAndDeactivateU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m554F125318195969EAA9504CBBDB3D77CE7CB917(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndDeactivateU3Ed__7_t0B9854B91CC5946899CC4024F6D32F274A81643C_CustomAttributesCacheGenerator_U3CWaitAndDeactivateU3Ed__7_System_Collections_IEnumerator_Reset_m8B28B60638773A42F14FBE776C0E896D4F2AE0C4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndDeactivateU3Ed__7_t0B9854B91CC5946899CC4024F6D32F274A81643C_CustomAttributesCacheGenerator_U3CWaitAndDeactivateU3Ed__7_System_Collections_IEnumerator_get_Current_m4AA71565E06F69AFC1CE8BCED114941F756B919B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MV_t50F4510A4AF6B71BA52F27D2ECFB44FBAD8A37B7_CustomAttributesCacheGenerator_videoMV(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MV_t50F4510A4AF6B71BA52F27D2ECFB44FBAD8A37B7_CustomAttributesCacheGenerator_toEnable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MV_t50F4510A4AF6B71BA52F27D2ECFB44FBAD8A37B7_CustomAttributesCacheGenerator_toEnable2(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MV_t50F4510A4AF6B71BA52F27D2ECFB44FBAD8A37B7_CustomAttributesCacheGenerator_toDisable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MV_t50F4510A4AF6B71BA52F27D2ECFB44FBAD8A37B7_CustomAttributesCacheGenerator_gameSFX(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MV_t50F4510A4AF6B71BA52F27D2ECFB44FBAD8A37B7_CustomAttributesCacheGenerator_gameMusic(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E_CustomAttributesCacheGenerator_coinCollectedOnTap(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E_CustomAttributesCacheGenerator_orbTapVO(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E_CustomAttributesCacheGenerator_colliderName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E_CustomAttributesCacheGenerator_storyAnimation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E_CustomAttributesCacheGenerator_splashAnimation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E_CustomAttributesCacheGenerator_storyTapVO(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E_CustomAttributesCacheGenerator_splashTapVO(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PageCompletion_tC5898F7BAD8C861FFA45295E4EE89BA178AB8B54_CustomAttributesCacheGenerator_starfishToTap(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PageCompletion_tC5898F7BAD8C861FFA45295E4EE89BA178AB8B54_CustomAttributesCacheGenerator_toEnable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PageCompletion_tC5898F7BAD8C861FFA45295E4EE89BA178AB8B54_CustomAttributesCacheGenerator_toEnable2(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DefaultObserverEventHandler_t25FF9CE0FF0ED822CE21D6AE20C6A6E77105C02B_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(DefaultObserverEventHandler_t25FF9CE0FF0ED822CE21D6AE20C6A6E77105C02B_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[2];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34_0_0_0_var), NULL);
	}
}
static void PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator_videoMV(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator_videoDuration(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator_hasOnPageGame(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator_splashWorld(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator_completeGameAudio(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator_exitObject(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator_U3CCurrentStatusU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator_U3CCompletionCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator_PageGame_get_CurrentStatus_m62216C85BE6E5B3889395214E48C988D10E4ADAC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator_PageGame_set_CurrentStatus_m80C5F4ACC165A3A47F04F47DD7C04D97E914D5C5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator_PageGame_get_CompletionCount_m163C431B336779F854B28006C445D9ACFBE41E3C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator_PageGame_set_CompletionCount_m773C823765A76AA176ACE8A5D7CC238B07797EA2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PageManager_t2117D39B2F2C5C8A4EE38E159E54B69771568945_CustomAttributesCacheGenerator_pages(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139_CustomAttributesCacheGenerator_raycastTapLayers(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139_CustomAttributesCacheGenerator_raycastOrbTapLayers(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139_CustomAttributesCacheGenerator_raycastDragLayers(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139_CustomAttributesCacheGenerator_raycastThrowLayers(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139_CustomAttributesCacheGenerator_throwParticles(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2_CustomAttributesCacheGenerator_minTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2_CustomAttributesCacheGenerator_maxTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2_CustomAttributesCacheGenerator_scaleObject(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2_CustomAttributesCacheGenerator_U3CCurrentStatusU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2_CustomAttributesCacheGenerator_Spawn_get_CurrentStatus_m5E2624F5D15120BAF25669DFC891BB10BF2FE067(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2_CustomAttributesCacheGenerator_Spawn_set_CurrentStatus_mA3512D04052416AB2B5D7167D64D39DE8C41C084(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2_CustomAttributesCacheGenerator_Spawn_ScaleObject_mA7A227A8BE23DEEF647D38A89FDC136C1CA68A82(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CScaleObjectU3Ed__11_t4E9DC2B458A4FA829BDAE1A00561A16F1A8A40F5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CScaleObjectU3Ed__11_t4E9DC2B458A4FA829BDAE1A00561A16F1A8A40F5_0_0_0_var), NULL);
	}
}
static void Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2_CustomAttributesCacheGenerator_Spawn_U3CGrowU3Eb__12_0_mCACEB31935EBC09019449E1370A1225EFE1140CD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CScaleObjectU3Ed__11_t4E9DC2B458A4FA829BDAE1A00561A16F1A8A40F5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CScaleObjectU3Ed__11_t4E9DC2B458A4FA829BDAE1A00561A16F1A8A40F5_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__11__ctor_m8D9F6C2638A2271E6525C7972EDFC7CAB742B0DD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScaleObjectU3Ed__11_t4E9DC2B458A4FA829BDAE1A00561A16F1A8A40F5_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__11_System_IDisposable_Dispose_m40B8BFE624A73F108E46FFFA98B2AE4D6787CB5E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScaleObjectU3Ed__11_t4E9DC2B458A4FA829BDAE1A00561A16F1A8A40F5_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m30FB78064DC61798F0BFA19E5D2F12A85975F0F5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScaleObjectU3Ed__11_t4E9DC2B458A4FA829BDAE1A00561A16F1A8A40F5_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__11_System_Collections_IEnumerator_Reset_m10257A8A5A594206D1B8A584528BFC9A24615121(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScaleObjectU3Ed__11_t4E9DC2B458A4FA829BDAE1A00561A16F1A8A40F5_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__11_System_Collections_IEnumerator_get_Current_m116291CCC3A2D4AAB33B5F5683271A7799F9B80D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC_CustomAttributesCacheGenerator_pageCamera(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC_CustomAttributesCacheGenerator_raycastLayers(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC_CustomAttributesCacheGenerator_mirrorARCamera(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC_CustomAttributesCacheGenerator_ARCameraMoveFactor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC_CustomAttributesCacheGenerator_pageLight(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC_CustomAttributesCacheGenerator_musicSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F"), NULL);
	}
}
static void SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC_CustomAttributesCacheGenerator_sfxSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC_CustomAttributesCacheGenerator_gameMusic(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC_CustomAttributesCacheGenerator_startGameAudio(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC_CustomAttributesCacheGenerator_completeGameAudio(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC_CustomAttributesCacheGenerator_exitObject(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var), NULL);
	}
}
static void SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F_CustomAttributesCacheGenerator_usePhysicsForThrow(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F_CustomAttributesCacheGenerator_throwSpeedY(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F_CustomAttributesCacheGenerator_throwForceFactor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F_CustomAttributesCacheGenerator_coinCollectedOnTap(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F_CustomAttributesCacheGenerator_coinCollectedOnThrow(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F_CustomAttributesCacheGenerator_starfishTapVO(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F_CustomAttributesCacheGenerator_starfishThrowVO(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F_CustomAttributesCacheGenerator_Animation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F_CustomAttributesCacheGenerator_colliderName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F_CustomAttributesCacheGenerator_SplashWorldTapThrow_U3CThrowU3Eb__30_0_m0D5CC01A9E3F3BBE5BBC74F21EFB1AA7F962E30A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F_CustomAttributesCacheGenerator_SplashWorldTapThrow_U3CThrowU3Eb__30_1_m646A85613E11A7EC17CAFB9A2140FA9E49E07A67(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056_CustomAttributesCacheGenerator_coinCollectedOnTap(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056_CustomAttributesCacheGenerator_coinCollected(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056_CustomAttributesCacheGenerator_uiSound(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056_CustomAttributesCacheGenerator_Animation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056_CustomAttributesCacheGenerator_colliderName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056_CustomAttributesCacheGenerator_throwTarget(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056_CustomAttributesCacheGenerator_waterSplash(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056_CustomAttributesCacheGenerator_throwTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056_CustomAttributesCacheGenerator_throwLiftY(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056_CustomAttributesCacheGenerator_toSpawn(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056_CustomAttributesCacheGenerator_toDisable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056_CustomAttributesCacheGenerator_StarfishTap_U3CTapU3Eg__WaitAndActivateU7C20_2_m45303C54140A66C6A1C3AA9FB164850D9B9E0605(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_t610B7E25C1F7E862A6CB4B61E84B0023BAA56FA9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_t610B7E25C1F7E862A6CB4B61E84B0023BAA56FA9_0_0_0_var), NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tDADB0F86F4A25004FFE6463D967246A9D23C87D4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_t610B7E25C1F7E862A6CB4B61E84B0023BAA56FA9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_t610B7E25C1F7E862A6CB4B61E84B0023BAA56FA9_CustomAttributesCacheGenerator_U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed__ctor_mC1FAC773B7C389314756A2BC1C0B935D629E5CB8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_t610B7E25C1F7E862A6CB4B61E84B0023BAA56FA9_CustomAttributesCacheGenerator_U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_System_IDisposable_Dispose_mD1831FF6B8869F465B692C7BC3440334E3C277AE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_t610B7E25C1F7E862A6CB4B61E84B0023BAA56FA9_CustomAttributesCacheGenerator_U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9764191B0083E73AC10D2DD64B724AF7BFE30151(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_t610B7E25C1F7E862A6CB4B61E84B0023BAA56FA9_CustomAttributesCacheGenerator_U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_System_Collections_IEnumerator_Reset_m2A883B4CA0FA17D2368B73A8DD265F095C3662D5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_t610B7E25C1F7E862A6CB4B61E84B0023BAA56FA9_CustomAttributesCacheGenerator_U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_System_Collections_IEnumerator_get_Current_m15B9F318820B9184FA7C97E17B84E9AA142D375C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_pageCompleted(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_coinTotal(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_coinsCollected(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55_CustomAttributesCacheGenerator_minTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55_CustomAttributesCacheGenerator_maxTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55_CustomAttributesCacheGenerator_scaleObject(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55_CustomAttributesCacheGenerator_U3CCurrentStatusU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55_CustomAttributesCacheGenerator_UIScale_get_CurrentStatus_m59B74B309E7DEDD304B5975587C597227D35AAF7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55_CustomAttributesCacheGenerator_UIScale_set_CurrentStatus_m3DDDB961B7587E70B838BEAFB6B78E2D9C577004(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55_CustomAttributesCacheGenerator_UIScale_ScaleObject_m2D6ACA6D185145F4DC427EF2D42A49EAC233369D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CScaleObjectU3Ed__12_t8DC31192573F6DFEF7FBA86C992E3587EAA46457_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CScaleObjectU3Ed__12_t8DC31192573F6DFEF7FBA86C992E3587EAA46457_0_0_0_var), NULL);
	}
}
static void UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55_CustomAttributesCacheGenerator_UIScale_U3CShrinkU3Eb__13_0_mFFDD536FF19492248C5AF5931C4DF93F7AB58411(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55_CustomAttributesCacheGenerator_UIScale_U3CGrowU3Eb__14_0_m3BB4510B628C633F140F8A7EE6AC08E08170E52A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CScaleObjectU3Ed__12_t8DC31192573F6DFEF7FBA86C992E3587EAA46457_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CScaleObjectU3Ed__12_t8DC31192573F6DFEF7FBA86C992E3587EAA46457_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__12__ctor_mDC451B5F819E9A4AE8516332FB6EB086259AB187(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScaleObjectU3Ed__12_t8DC31192573F6DFEF7FBA86C992E3587EAA46457_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__12_System_IDisposable_Dispose_m18311F276F727390C6BB8F45547A1055AD46B687(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScaleObjectU3Ed__12_t8DC31192573F6DFEF7FBA86C992E3587EAA46457_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m208AC32EA8F4EFFA396F863484497848182EE124(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScaleObjectU3Ed__12_t8DC31192573F6DFEF7FBA86C992E3587EAA46457_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__12_System_Collections_IEnumerator_Reset_mDCE696778B7C746AF2B21B9EDBF0EEDD9FA7C644(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScaleObjectU3Ed__12_t8DC31192573F6DFEF7FBA86C992E3587EAA46457_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__12_System_Collections_IEnumerator_get_Current_m919B949113F60B7761002096FB3E96CDF935EDD5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7_CustomAttributesCacheGenerator_canvasVideo(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7_CustomAttributesCacheGenerator_toDisable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7_CustomAttributesCacheGenerator_toEnable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7_CustomAttributesCacheGenerator_getSlider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7_CustomAttributesCacheGenerator_stopMusic(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7_CustomAttributesCacheGenerator_playMusic(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayPause_tE57D2A634C24A66387C3115F3AF1B6663BC49BEA_CustomAttributesCacheGenerator_videoPlayer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayPause_tE57D2A634C24A66387C3115F3AF1B6663BC49BEA_CustomAttributesCacheGenerator_toDisable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayPause_tE57D2A634C24A66387C3115F3AF1B6663BC49BEA_CustomAttributesCacheGenerator_toEnable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayPause_tE57D2A634C24A66387C3115F3AF1B6663BC49BEA_CustomAttributesCacheGenerator_toEnable2(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2_CustomAttributesCacheGenerator_boost(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x76\x65\x6D\x65\x6E\x74\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x78\x70\x6F\x6E\x65\x6E\x74\x69\x61\x6C\x20\x62\x6F\x6F\x73\x74\x20\x66\x61\x63\x74\x6F\x72\x20\x6F\x6E\x20\x74\x72\x61\x6E\x73\x6C\x61\x74\x69\x6F\x6E\x2C\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x6C\x61\x62\x6C\x65\x20\x62\x79\x20\x6D\x6F\x75\x73\x65\x20\x77\x68\x65\x65\x6C\x2E"), NULL);
	}
}
static void SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2_CustomAttributesCacheGenerator_positionLerpTime(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x69\x6D\x65\x20\x69\x74\x20\x74\x61\x6B\x65\x73\x20\x74\x6F\x20\x69\x6E\x74\x65\x72\x70\x6F\x6C\x61\x74\x65\x20\x63\x61\x6D\x65\x72\x61\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E\x20\x39\x39\x25\x20\x6F\x66\x20\x74\x68\x65\x20\x77\x61\x79\x20\x74\x6F\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.00100000005f, 1.0f, NULL);
	}
}
static void SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2_CustomAttributesCacheGenerator_mouseSensitivity(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x6F\x74\x61\x74\x69\x6F\x6E\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x75\x6C\x74\x69\x70\x6C\x69\x65\x72\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x73\x65\x6E\x73\x69\x74\x69\x76\x69\x74\x79\x20\x6F\x66\x20\x74\x68\x65\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x2E"), NULL);
	}
}
static void SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2_CustomAttributesCacheGenerator_mouseSensitivityCurve(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x58\x20\x3D\x20\x43\x68\x61\x6E\x67\x65\x20\x69\x6E\x20\x6D\x6F\x75\x73\x65\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E\x2E\xA\x59\x20\x3D\x20\x4D\x75\x6C\x74\x69\x70\x6C\x69\x63\x61\x74\x69\x76\x65\x20\x66\x61\x63\x74\x6F\x72\x20\x66\x6F\x72\x20\x63\x61\x6D\x65\x72\x61\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x2E"), NULL);
	}
}
static void SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2_CustomAttributesCacheGenerator_rotationLerpTime(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x69\x6D\x65\x20\x69\x74\x20\x74\x61\x6B\x65\x73\x20\x74\x6F\x20\x69\x6E\x74\x65\x72\x70\x6F\x6C\x61\x74\x65\x20\x63\x61\x6D\x65\x72\x61\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x39\x39\x25\x20\x6F\x66\x20\x74\x68\x65\x20\x77\x61\x79\x20\x74\x6F\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.00100000005f, 1.0f, NULL);
	}
}
static void SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2_CustomAttributesCacheGenerator_invertY(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x69\x6E\x76\x65\x72\x74\x20\x6F\x75\x72\x20\x59\x20\x61\x78\x69\x73\x20\x66\x6F\x72\x20\x6D\x6F\x75\x73\x65\x20\x69\x6E\x70\x75\x74\x20\x74\x6F\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x2E"), NULL);
	}
}
static void TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757_CustomAttributesCacheGenerator_TestingUnitTests_timeBasedTesting_mBD8B52CEA7286C9F4FAAA419C773B78255E55F4D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65_0_0_0_var), NULL);
	}
}
static void TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757_CustomAttributesCacheGenerator_TestingUnitTests_lotsOfCancels_mD21D0D0D0B8BE316E58C412A19ADE6658DAA5134(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523_0_0_0_var), NULL);
	}
}
static void TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757_CustomAttributesCacheGenerator_TestingUnitTests_pauseTimeNow_m3293FFC3DDEC01D507093F2D2B8E34B40FC0DD7F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB_0_0_0_var), NULL);
	}
}
static void TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757_CustomAttributesCacheGenerator_TestingUnitTests_U3ClotsOfCancelsU3Eb__25_0_m79612DDE2FFAD2A0986AA0D91A768BA96269D4F6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757_CustomAttributesCacheGenerator_TestingUnitTests_U3CpauseTimeNowU3Eb__26_1_mB0C630E660D38F819734F11E2998CDE43590D503(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass22_1_t041360FB309BAE73F44DAC9B4689082AC2FF5973_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass22_2_tF5B9A247AE90B21641EE6BA256550BF6E5CBC1E8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65_CustomAttributesCacheGenerator_U3CtimeBasedTestingU3Ed__24__ctor_mBB473D0F9FCF74C17EE67A02377DBAC113B0ECE6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65_CustomAttributesCacheGenerator_U3CtimeBasedTestingU3Ed__24_System_IDisposable_Dispose_m2240ABE280E91D3110F4956F4DDAE8A6F211340A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65_CustomAttributesCacheGenerator_U3CtimeBasedTestingU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mACB1A3175D122506B71BCDB9EBF4AA9F0383E74B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65_CustomAttributesCacheGenerator_U3CtimeBasedTestingU3Ed__24_System_Collections_IEnumerator_Reset_m96D9D8DFA5B24A08E71FA9EA4595288F06E9837B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65_CustomAttributesCacheGenerator_U3CtimeBasedTestingU3Ed__24_System_Collections_IEnumerator_get_Current_mA059C8602E0A9BCCC1849108A55B19E09E4818CA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523_CustomAttributesCacheGenerator_U3ClotsOfCancelsU3Ed__25__ctor_m9B88818966D8A321B3F23FB53C461C28A354F1C0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523_CustomAttributesCacheGenerator_U3ClotsOfCancelsU3Ed__25_System_IDisposable_Dispose_m40E5F2D2626AB1E624CA39E3740CDFBE5B4F1B30(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523_CustomAttributesCacheGenerator_U3ClotsOfCancelsU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2F111DF2BFDD6B3974DFA490B271DC9FCCBFC1DB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523_CustomAttributesCacheGenerator_U3ClotsOfCancelsU3Ed__25_System_Collections_IEnumerator_Reset_m440F860762B28408E3B9AB6D72144B5E6A1C6F8C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523_CustomAttributesCacheGenerator_U3ClotsOfCancelsU3Ed__25_System_Collections_IEnumerator_get_Current_mA7D1CE85A7D50D60D73C5914F5EC60B5E33A344E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB_CustomAttributesCacheGenerator_U3CpauseTimeNowU3Ed__26__ctor_mD1AE96EC04B7EB3C54830BBCA910ACEC6A857D84(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB_CustomAttributesCacheGenerator_U3CpauseTimeNowU3Ed__26_System_IDisposable_Dispose_mE8F0437ED1B062C967A1FFCA460708BD42BC5142(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB_CustomAttributesCacheGenerator_U3CpauseTimeNowU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD12A76EAE3CD0B1B9C938030B3E4D2FAA6456011(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB_CustomAttributesCacheGenerator_U3CpauseTimeNowU3Ed__26_System_Collections_IEnumerator_Reset_m88957DE3F231D3C4F8E64C6DADA45876FABB7FD0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB_CustomAttributesCacheGenerator_U3CpauseTimeNowU3Ed__26_System_Collections_IEnumerator_get_Current_m04EA51DB175A4043B51D9B3CFDD410D924B422A0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[497] = 
{
	U3CU3Ec__DisplayClass4_0_t038683C2E93B5073D484142AB01DE825CEEDBEBD_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_1_t60F636DD05421DE520F34389F79491ABBEC7296A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_2_t657E0C1A8D3477F99ABEE7DD54418FC518C44D05_CustomAttributesCacheGenerator,
	U3CU3Ec_tB1B120168A6E7BA6AD56D21520A093EEE79C30FB_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_tB541BE7A9909628CA87B0D4AB005DDB3FFAB3A6C_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_t58B048B073D31EB8F5A9ED27B5DE2730EFCFB3BB_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_tEFDABD6CC1CBBFC6AC7A112C3717524ECAADB421_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_1_t0F31BD07E1BCC48A7ED53ACEEB2FB3105D312E6F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_tEF56FCF80D4C652E8CC501F5CF0D88D34D01D386_CustomAttributesCacheGenerator,
	U3CU3Ec_t0FFE348F3AECAC4BFE5D4196F3BBD8CC15874A96_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass15_0_t30EA498BE1CED5C189EDC1015FB8AC828E008426_CustomAttributesCacheGenerator,
	U3CU3Ec_t8ABF57D71EAC0C260D48F0C6C5CCC9C3D247FBEC_CustomAttributesCacheGenerator,
	U3CU3Ec_tD7C586F26F27190F29F8B90744BCE69E24E2B0BB_CustomAttributesCacheGenerator,
	U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58_CustomAttributesCacheGenerator,
	U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass193_0_tAD697F408BB575C622FD5E018C1F75AB28CBAF48_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass194_0_t6FE45F50B4F93FED778E7D54F0EF582EA8252814_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass196_0_t4FA263E9CE532093F4513F5750D54960760E3F78_CustomAttributesCacheGenerator,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator,
	ImageRecognition_t3FA1D134672565B626D02FABA8BCB9732ACFAC23_CustomAttributesCacheGenerator,
	U3CU3Ec_t5B5DA66907FD15891D64D11538E77E3537A4AB4C_CustomAttributesCacheGenerator,
	U3CScaleObjectU3Ed__11_t461C201F3015C43C04CB97A6D8D45A7B73627A6D_CustomAttributesCacheGenerator,
	U3CWaitAndActivateU3Ed__6_t503E05974A5610E1E9848BFB9321AE1CE472AE6D_CustomAttributesCacheGenerator,
	U3CWaitAndDeactivateU3Ed__7_t0B9854B91CC5946899CC4024F6D32F274A81643C_CustomAttributesCacheGenerator,
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator,
	U3CScaleObjectU3Ed__11_t4E9DC2B458A4FA829BDAE1A00561A16F1A8A40F5_CustomAttributesCacheGenerator,
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F_CustomAttributesCacheGenerator,
	U3CU3Ec_tDADB0F86F4A25004FFE6463D967246A9D23C87D4_CustomAttributesCacheGenerator,
	U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_t610B7E25C1F7E862A6CB4B61E84B0023BAA56FA9_CustomAttributesCacheGenerator,
	U3CScaleObjectU3Ed__12_t8DC31192573F6DFEF7FBA86C992E3587EAA46457_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass22_1_t041360FB309BAE73F44DAC9B4689082AC2FF5973_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass22_2_tF5B9A247AE90B21641EE6BA256550BF6E5CBC1E8_CustomAttributesCacheGenerator,
	U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F_CustomAttributesCacheGenerator,
	U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65_CustomAttributesCacheGenerator,
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523_CustomAttributesCacheGenerator,
	U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB_CustomAttributesCacheGenerator,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_U3CeaseInternalU3Ek__BackingField,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_U3CinitInternalU3Ek__BackingField,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3CtoTransU3Ek__BackingField,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3CpointU3Ek__BackingField,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3CaxisU3Ek__BackingField,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ClastValU3Ek__BackingField,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3CorigRotationU3Ek__BackingField,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3CpathU3Ek__BackingField,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3CsplineU3Ek__BackingField,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3CltRectU3Ek__BackingField,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConUpdateFloatU3Ek__BackingField,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConUpdateFloatRatioU3Ek__BackingField,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConUpdateFloatObjectU3Ek__BackingField,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConUpdateVector2U3Ek__BackingField,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConUpdateVector3U3Ek__BackingField,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConUpdateVector3ObjectU3Ek__BackingField,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConUpdateColorU3Ek__BackingField,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConUpdateColorObjectU3Ek__BackingField,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConCompleteU3Ek__BackingField,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConCompleteObjectU3Ek__BackingField,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConCompleteParamU3Ek__BackingField,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConUpdateParamU3Ek__BackingField,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_U3ConStartU3Ek__BackingField,
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD_CustomAttributesCacheGenerator_rotateFinished,
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD_CustomAttributesCacheGenerator_counter,
	ImageRecognition_t3FA1D134672565B626D02FABA8BCB9732ACFAC23_CustomAttributesCacheGenerator_bookPages,
	Float_t28B5D79DA5B24D79FA3F8F8DC65BCC9791E16307_CustomAttributesCacheGenerator_speed,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_arCamera,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_gameLight,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_arAudioListener,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_U3CCurrentPageGameU3Ek__BackingField,
	GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607_CustomAttributesCacheGenerator_minTime,
	GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607_CustomAttributesCacheGenerator_maxTime,
	GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607_CustomAttributesCacheGenerator_scaleObject,
	GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607_CustomAttributesCacheGenerator_U3CCurrentStatusU3Ek__BackingField,
	Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867_CustomAttributesCacheGenerator_particle,
	Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867_CustomAttributesCacheGenerator_pointer,
	MV_t50F4510A4AF6B71BA52F27D2ECFB44FBAD8A37B7_CustomAttributesCacheGenerator_videoMV,
	MV_t50F4510A4AF6B71BA52F27D2ECFB44FBAD8A37B7_CustomAttributesCacheGenerator_toEnable,
	MV_t50F4510A4AF6B71BA52F27D2ECFB44FBAD8A37B7_CustomAttributesCacheGenerator_toEnable2,
	MV_t50F4510A4AF6B71BA52F27D2ECFB44FBAD8A37B7_CustomAttributesCacheGenerator_toDisable,
	MV_t50F4510A4AF6B71BA52F27D2ECFB44FBAD8A37B7_CustomAttributesCacheGenerator_gameSFX,
	MV_t50F4510A4AF6B71BA52F27D2ECFB44FBAD8A37B7_CustomAttributesCacheGenerator_gameMusic,
	OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E_CustomAttributesCacheGenerator_coinCollectedOnTap,
	OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E_CustomAttributesCacheGenerator_orbTapVO,
	OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E_CustomAttributesCacheGenerator_colliderName,
	OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E_CustomAttributesCacheGenerator_storyAnimation,
	OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E_CustomAttributesCacheGenerator_splashAnimation,
	OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E_CustomAttributesCacheGenerator_storyTapVO,
	OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E_CustomAttributesCacheGenerator_splashTapVO,
	PageCompletion_tC5898F7BAD8C861FFA45295E4EE89BA178AB8B54_CustomAttributesCacheGenerator_starfishToTap,
	PageCompletion_tC5898F7BAD8C861FFA45295E4EE89BA178AB8B54_CustomAttributesCacheGenerator_toEnable,
	PageCompletion_tC5898F7BAD8C861FFA45295E4EE89BA178AB8B54_CustomAttributesCacheGenerator_toEnable2,
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator_videoMV,
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator_videoDuration,
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator_hasOnPageGame,
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator_splashWorld,
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator_completeGameAudio,
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator_exitObject,
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator_U3CCurrentStatusU3Ek__BackingField,
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator_U3CCompletionCountU3Ek__BackingField,
	PageManager_t2117D39B2F2C5C8A4EE38E159E54B69771568945_CustomAttributesCacheGenerator_pages,
	Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139_CustomAttributesCacheGenerator_raycastTapLayers,
	Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139_CustomAttributesCacheGenerator_raycastOrbTapLayers,
	Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139_CustomAttributesCacheGenerator_raycastDragLayers,
	Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139_CustomAttributesCacheGenerator_raycastThrowLayers,
	Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139_CustomAttributesCacheGenerator_throwParticles,
	Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2_CustomAttributesCacheGenerator_minTime,
	Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2_CustomAttributesCacheGenerator_maxTime,
	Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2_CustomAttributesCacheGenerator_scaleObject,
	Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2_CustomAttributesCacheGenerator_U3CCurrentStatusU3Ek__BackingField,
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC_CustomAttributesCacheGenerator_pageCamera,
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC_CustomAttributesCacheGenerator_raycastLayers,
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC_CustomAttributesCacheGenerator_mirrorARCamera,
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC_CustomAttributesCacheGenerator_ARCameraMoveFactor,
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC_CustomAttributesCacheGenerator_pageLight,
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC_CustomAttributesCacheGenerator_musicSource,
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC_CustomAttributesCacheGenerator_sfxSource,
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC_CustomAttributesCacheGenerator_gameMusic,
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC_CustomAttributesCacheGenerator_startGameAudio,
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC_CustomAttributesCacheGenerator_completeGameAudio,
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC_CustomAttributesCacheGenerator_exitObject,
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F_CustomAttributesCacheGenerator_usePhysicsForThrow,
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F_CustomAttributesCacheGenerator_throwSpeedY,
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F_CustomAttributesCacheGenerator_throwForceFactor,
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F_CustomAttributesCacheGenerator_coinCollectedOnTap,
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F_CustomAttributesCacheGenerator_coinCollectedOnThrow,
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F_CustomAttributesCacheGenerator_starfishTapVO,
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F_CustomAttributesCacheGenerator_starfishThrowVO,
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F_CustomAttributesCacheGenerator_Animation,
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F_CustomAttributesCacheGenerator_colliderName,
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056_CustomAttributesCacheGenerator_coinCollectedOnTap,
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056_CustomAttributesCacheGenerator_coinCollected,
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056_CustomAttributesCacheGenerator_uiSound,
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056_CustomAttributesCacheGenerator_Animation,
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056_CustomAttributesCacheGenerator_colliderName,
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056_CustomAttributesCacheGenerator_throwTarget,
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056_CustomAttributesCacheGenerator_waterSplash,
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056_CustomAttributesCacheGenerator_throwTime,
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056_CustomAttributesCacheGenerator_throwLiftY,
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056_CustomAttributesCacheGenerator_toSpawn,
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056_CustomAttributesCacheGenerator_toDisable,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_pageCompleted,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_coinTotal,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_coinsCollected,
	UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55_CustomAttributesCacheGenerator_minTime,
	UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55_CustomAttributesCacheGenerator_maxTime,
	UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55_CustomAttributesCacheGenerator_scaleObject,
	UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55_CustomAttributesCacheGenerator_U3CCurrentStatusU3Ek__BackingField,
	OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7_CustomAttributesCacheGenerator_canvasVideo,
	OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7_CustomAttributesCacheGenerator_toDisable,
	OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7_CustomAttributesCacheGenerator_toEnable,
	OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7_CustomAttributesCacheGenerator_getSlider,
	OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7_CustomAttributesCacheGenerator_stopMusic,
	OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7_CustomAttributesCacheGenerator_playMusic,
	PlayPause_tE57D2A634C24A66387C3115F3AF1B6663BC49BEA_CustomAttributesCacheGenerator_videoPlayer,
	PlayPause_tE57D2A634C24A66387C3115F3AF1B6663BC49BEA_CustomAttributesCacheGenerator_toDisable,
	PlayPause_tE57D2A634C24A66387C3115F3AF1B6663BC49BEA_CustomAttributesCacheGenerator_toEnable,
	PlayPause_tE57D2A634C24A66387C3115F3AF1B6663BC49BEA_CustomAttributesCacheGenerator_toEnable2,
	SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2_CustomAttributesCacheGenerator_boost,
	SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2_CustomAttributesCacheGenerator_positionLerpTime,
	SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2_CustomAttributesCacheGenerator_mouseSensitivity,
	SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2_CustomAttributesCacheGenerator_mouseSensitivityCurve,
	SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2_CustomAttributesCacheGenerator_rotationLerpTime,
	SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2_CustomAttributesCacheGenerator_invertY,
	TestingPunch_tD3C862FA45AA792897C92A24D8EB232568FB4232_CustomAttributesCacheGenerator_TestingPunch_U3CUpdateU3Eb__4_0_m573E0207272AF5B03F77EBE674FC9B7C0B49791D,
	TestingPunch_tD3C862FA45AA792897C92A24D8EB232568FB4232_CustomAttributesCacheGenerator_TestingPunch_U3CUpdateU3Eb__4_3_m4CC34D2970098316C32EBE67B368AB199A20A2E1,
	TestingPunch_tD3C862FA45AA792897C92A24D8EB232568FB4232_CustomAttributesCacheGenerator_TestingPunch_U3CUpdateU3Eb__4_7_m2E2AD49C20FF476ED389803BE00C0C87495D0DFC,
	GeneralAdvancedTechniques_t9CC1EDE627191C21C904074F94D92D26F45AA70D_CustomAttributesCacheGenerator_GeneralAdvancedTechniques_U3CStartU3Eb__10_0_mBBEFB6358D20EC1EFC4D6B51139D0DEEDFF86AD2,
	GeneralBasic_t67AF63F77C4D821EE283A4220DAC04A9008315E4_CustomAttributesCacheGenerator_GeneralBasic_U3CadvancedExamplesU3Eb__2_0_m706BD8DE22C36E9A7EF6326EF1B7EAB6C780BF3D,
	GeneralBasics2d_t2BC39F63283747E77CD6462197411F81BD9C59AB_CustomAttributesCacheGenerator_GeneralBasics2d_U3CadvancedExamplesU3Eb__4_0_mFB34B228D53D66D2DA60CBE36C863AAC135AAE41,
	GeneralEventsListeners_tB79AB185DA5D044126B7C32FB156D3B4DFE3F733_CustomAttributesCacheGenerator_GeneralEventsListeners_U3CchangeColorU3Eb__8_0_m8D567E7710ED42E4ED9DF3074F1F9E4AF19AF1A1,
	GeneralSequencer_t6D018FEB941E2BBA2F3BB9DB0EF288F40C8C1D92_CustomAttributesCacheGenerator_GeneralSequencer_U3CStartU3Eb__4_0_m01F09B25E4652D0AD12402145A95897D0E65C36F,
	GeneralSimpleUI_t15D77C2AB58F89C178F7B20E1590A36E240E5FA8_CustomAttributesCacheGenerator_GeneralSimpleUI_U3CStartU3Eb__1_0_mCC061524D85ACD8BAF6FE7D8CE1F2277735B3FC8,
	GeneralSimpleUI_t15D77C2AB58F89C178F7B20E1590A36E240E5FA8_CustomAttributesCacheGenerator_GeneralSimpleUI_U3CStartU3Eb__1_2_m32632A4DB6420C70DED2CA0F5D16A52ABED4D202,
	GeneralSimpleUI_t15D77C2AB58F89C178F7B20E1590A36E240E5FA8_CustomAttributesCacheGenerator_GeneralSimpleUI_U3CStartU3Eb__1_3_m81A54F1B0397D24C3ABC0367E5F3A96CD8938D25,
	PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28_CustomAttributesCacheGenerator_PathSplineEndless_U3CStartU3Eb__17_0_m89EDA38D298348E747F008DED4C1CF7D4F815D60,
	PathSplines_t5E3D3A23634F2898542C5C6F650D03565C9C2D6A_CustomAttributesCacheGenerator_PathSplines_U3CStartU3Eb__4_0_mC304E252D7336E181BBEF0F27B79311DEDF4BB0B,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_get_easeInternal_m263F859E9225DF85EFD75C079CD5073AE647B284,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_set_easeInternal_m40A3FEDCAA0EE7EB86E804641367E50E34C0CDB9,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_get_initInternal_m3E9E4486FBFCFFDB8A3A91EB00DBEBA29916C943,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_set_initInternal_m21E7CD474330836550F6298EF7F0F2902BC3EE60,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_cancel_mC12FD3EE1E42AFCC7AA536A6D344BE3AEA86AE70,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveXU3Eb__73_0_m39FBF744669510650F08E0F2B4E87CDEECA7440A,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveXU3Eb__73_1_mB4AD58106C3D205E5D4763F3F1CCA63B2F99EB78,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveYU3Eb__74_0_mA6309FBB2AF9AEEB859024E69AC8ABD6D102AD87,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveYU3Eb__74_1_m7A2A40165003571F84F057D3F39BD77A23B2F0E9,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveZU3Eb__75_0_mFE473C92996CA9EBA69397DA460F4B16EDE7D03C,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveZU3Eb__75_1_mF78C868A90B38D04ADAD2AD93D536B1791B3C671,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveLocalXU3Eb__76_0_m8A895E545E84F266EA0E0963C68274D9545D17BE,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveLocalXU3Eb__76_1_m17F56B25DDE0E4E6D6CC186BF9A9A617957C2FD6,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveLocalYU3Eb__77_0_m8D00D189D0ADCF6C778EFF39DCA1EA55C03E34F4,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveLocalYU3Eb__77_1_m67ADD5EAA303DB53DA4B896F2080F3E6975C76A6,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveLocalZU3Eb__78_0_mFD9EBD1D9EF08CB93FDCCE06D88DC940852C03DF,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveLocalZU3Eb__78_1_mF3D05D5EA84618939D8F8C0F9F06EE5D6294D77D,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveCurvedU3Eb__81_0_m8BD948AD31BE03BCEF9D75091AECC3DF8B4A50E3,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveCurvedLocalU3Eb__82_0_mE35CF92421F9DBB5687F1F1608AFBE9F86C80378,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveSplineU3Eb__83_0_mD9ACDB850FD978554B5465139A4E1EDD844CA1BB,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveSplineLocalU3Eb__84_0_m3219BB023C406080046DE37EBCF0CEDB8E323D1A,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetScaleXU3Eb__85_0_mDF33F215E49CCD02AE02FDD79827006CB41DF18F,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetScaleXU3Eb__85_1_m332A3ADAFDC200BF9AC11279593B5968DC537623,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetScaleYU3Eb__86_0_m6A34E540DCDF955D835FCBD7FEF708257E07FE31,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetScaleYU3Eb__86_1_mE1BCEE0DADE70D6CA3873140143141D9B51DBDB5,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetScaleZU3Eb__87_0_mAEDB3F2903B250EC9885F64B147EDBDD4881E812,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetScaleZU3Eb__87_1_mEFAE5A48C329D33CAF5F7003D48DD0B9C581AB85,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateXU3Eb__88_0_mD0976C92A94804F53718DEE26B131989A067EF7A,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateXU3Eb__88_1_m7C171EAF890568C630E3BE1EC70C97A0C55F0984,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateYU3Eb__89_0_m1B2E4D04C35A72D3957FEB294B306BF741228F1B,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateYU3Eb__89_1_m67AC1C0BCE4902B58FA999FC9475BF42D391F167,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateZU3Eb__90_0_m92CFF50F82E8606EA58D1FF72D2FBD7C9A0C7290,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateZU3Eb__90_1_mA55CC8D92F2601E04F21F45337134FBE437A20B6,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateAroundU3Eb__91_0_m90372B54B5D631FC5E1FF67FCE64F69A1E0E7E63,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateAroundU3Eb__91_1_m21A791E3630653D459153347755624070DC38965,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateAroundLocalU3Eb__92_0_m49A28FAA34B36D7AA09A322D63974997CB31BF43,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateAroundLocalU3Eb__92_1_mCB4C19C5A763BBF640E5BE6785D5AB03859AD606,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetAlphaU3Eb__93_0_mADB8DE8A613762F896CC7754539BFFCA3A23B4E9,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetAlphaU3Eb__93_2_m5318309F91340E1C143196D9ADA659C53B3C918C,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetAlphaU3Eb__93_1_m44E2198D69A4F502CE6F5B1532AFD8F600D84717,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetTextAlphaU3Eb__94_0_mE551CB2CBB6114CA72656A033CAC42EAB2EFCC8B,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetTextAlphaU3Eb__94_1_m809E895E8E9ED4049E44ACAEEFE222EBA2E6A259,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetAlphaVertexU3Eb__95_0_m53AB148A8D470C29DACB58274436B9535F5CAC37,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetAlphaVertexU3Eb__95_1_m197F71CB84DF1265DDDEC92AB60CFAEC2B2E8BE9,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetColorU3Eb__96_0_mF63A06E57C3B0A1B5B8FE3E7EF131BD862457B19,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetColorU3Eb__96_1_m8EA4C07F32943F895BE0CB6909C63263827CEBF8,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCallbackColorU3Eb__97_0_mAD825635FB3FD1DE688BD692F394C91BD701659F,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCallbackColorU3Eb__97_1_m3635B1AEC7852BD361B57DAC85568D84BB050C38,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetTextColorU3Eb__98_0_m1AC4C496FB90FA9C994A7D8772EE75A96D8C7499,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetTextColorU3Eb__98_1_mF8C5941A01CC6AB44A3C1A9E4E0D42DE0019B1B5,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasAlphaU3Eb__99_0_m03D22099CB93845FEE24F439C52C194CF424307B,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasAlphaU3Eb__99_1_mC255CF9153C3E078BCC1E35C16E786C8662EBFD8,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasGroupAlphaU3Eb__100_0_mCD1535F45FF98C078658D56A058E4D44D7CD47F2,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasGroupAlphaU3Eb__100_1_m6EBBBBA42BCC9B435DB26932FCCD7DF904E3D31E,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasColorU3Eb__101_0_mC7AB0F758514D92910F073FB38A41C395E6764E3,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasColorU3Eb__101_1_m0DC35761F636C62A4B9A9D9B7C8EEFD9B30F3CA4,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasMoveXU3Eb__102_0_m0E72A6C3F16FC694B6D54907490ECA5D6D359776,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasMoveXU3Eb__102_1_m7FF531D0062E0803860A0CD456E6DD6AE30BD00D,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasMoveYU3Eb__103_0_m4730D57FCDEA041D3F4111E45E34C15149C4BD4C,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasMoveYU3Eb__103_1_m9CF415CA444CEBB9B5B6C01B337E1C4598B74A32,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasMoveZU3Eb__104_0_mDCD6897BB64C355669F8EE9CFD8F2B386F830CA2,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasMoveZU3Eb__104_1_m5B12F7E220DA96B6F05025A4BE1AFABDEEAFF853,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasRotateAroundU3Eb__106_0_m7254F615E75865226968EA098B90FE904EE5B38B,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasRotateAroundLocalU3Eb__107_0_m2C7BE76A27280304C47F1787199EA2790E10D6D6,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasPlaySpriteU3Eb__108_0_m60117A734E7707143FD04D2FBEE6B75618B258DF,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasPlaySpriteU3Eb__108_1_mAA1E46E79560810DA46AE8EA0BFA91EDAE3D2DA5,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasMoveU3Eb__109_0_mABE1F4262412CA7E93AA4B71881FAE5B817FCF8D,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasMoveU3Eb__109_1_mD190E0829649FEA47BAB95DED30B6EAB95C03ECF,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasScaleU3Eb__110_0_mF68FF782BFAD2E93A65661953E683EADC9A52DA0,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasScaleU3Eb__110_1_mCF2CCB10E46FC5F00061B020C857E7A35EF14E0D,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasSizeDeltaU3Eb__111_0_m021163E83008DE7874346B03AE065037CDC34D3A,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetCanvasSizeDeltaU3Eb__111_1_m9AA7511D4E1BF277C282F3E30388E82D0A9E1C0B,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveU3Eb__115_0_m5DDD8D43FEBB5099535893334FD76E1F0DAFD21B,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveU3Eb__115_1_m6B66D8C95AE011E8C641FA4AEF0245809F334399,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveLocalU3Eb__116_0_m7FE28EDDA5832CD6FF6BB2370DA6059DF23B2A1F,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveLocalU3Eb__116_1_m8D5A11FA82E103327A9ABD0CA5BB397952CD260C,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveToTransformU3Eb__117_0_mCE51DD0DF5028FDE555344C8FF602CD98716C33B,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetMoveToTransformU3Eb__117_1_mACD7BEE7BA65E5DCB5B234EB3A31D9D8C3ED7FA8,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateU3Eb__118_0_m3ECAD3ACBA191CD5E6FBB67BB3E3AAEDA454B4D3,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateU3Eb__118_1_mEB2249D09D0E69826AE6036FD448AF92D77B47D7,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateLocalU3Eb__119_0_m2AA0D5D9B5334CEC12D6E8670332C4BEEB5CD7DB,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetRotateLocalU3Eb__119_1_m64E3FC9E52EE3EB30AA9260408C3D02F24A34064,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetScaleU3Eb__120_0_m4528B485CAF7246E73A7E602504675D4B11A44B7,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetScaleU3Eb__120_1_m4A1D3D78C772991EC422F7D2D29EF4D069E39C78,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetGUIMoveU3Eb__121_0_m4AD272AF144CD468B048F733B93D1DFC08527804,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetGUIMoveU3Eb__121_1_mDBDABB8CB0F1D7CC05BF861826BD23B1A5CF68B3,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetGUIMoveMarginU3Eb__122_0_m92AE989BFBF68D262DDFD12EB9DBBE05BDCE7AB1,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetGUIMoveMarginU3Eb__122_1_mA60DFD7F27C11D0B55761F7A0936D8056ED09382,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetGUIScaleU3Eb__123_0_m5A0FC254BDC374AACB5FED5922852DD294CBA77C,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetGUIScaleU3Eb__123_1_m01CD6A1BE962B34E940B420723AE967C9AF4B248,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetGUIAlphaU3Eb__124_0_m584C608C8B326C3B0401CAA8C696F8ABCF95DD38,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetGUIAlphaU3Eb__124_1_mF0CAD09EF53473D82DF70172C27DA602272E3361,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetGUIRotateU3Eb__125_0_m8F9A74633240B6083A3C1DB7EA24F762D40589C9,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetGUIRotateU3Eb__125_1_m43CF134D7443CAA354F69A7031CE9F2574CE4DA5,
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_CustomAttributesCacheGenerator_LTDescr_U3CsetDelayedSoundU3Eb__126_0_mEF9FE367DCCC3DDE76DA47246D17F221C0F4D93F,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_toTrans_mB4912C34FDE4BB3CE94B86FDE3D7D8C0DE951773,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_toTrans_m4C747BFF6573BF8C2484AF01C3FCCC65E8A54FB9,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_point_mB79A8D97B381FE3122B1549DBB3527F9691A1156,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_point_mD7AE42D3184ED0D66545DC3A98B5CB5759DF8627,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_axis_m546D8ADD4003D9B69F7EC4A243008064AC42DB20,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_axis_m17A070B698AFE6D066C10EEF1A86DDEABC0A13B0,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_lastVal_mD84B4C7F0D87E853D6EB9786261068DA74031DF8,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_lastVal_m1B2DC6C4CE540BA60BD467F4394CEF18409B820C,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_origRotation_m1B98DB0AACC7AF940C43DC29DF7A61FE6E49736E,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_origRotation_m6E9E885A6259D7DE7FD1D398CABD7F7CB1751DE4,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_path_m20A38F266BEEDD6370F8F54B2FA58C7262808AC5,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_path_m10D4A506CACD00E0951E6E5E41003DE99C72DA64,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_spline_m32BE3A98427C2A4FBE40E1BAEDEC2246ABD3D4F2,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_spline_m5FF9921610A1F8C38E004CF9129F185BC7FDD708,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_ltRect_mDBE34D781428B8DD07A8F25AC32B9B0932B62BE7,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_ltRect_m273BF61CF793266842167003657D7083E62B655A,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onUpdateFloat_m8573CE5DFEFBA6E350AF056485B265FF59E8072B,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onUpdateFloat_mC6B9DD9721D66C62B4F196BFB0AC6EAFFD98A10F,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onUpdateFloatRatio_m6757E471D3ADDB9202062289E6EFAD3B61B2C7F4,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onUpdateFloatRatio_m0FBA91E49D4D592E2CAA88BF2963B2B5737ACEF7,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onUpdateFloatObject_m8A282C0573D32731C92CA8A4E352382CE73E4839,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onUpdateFloatObject_mF14622BEBB5CCF990BC0DC9622CE2EE037F364C5,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onUpdateVector2_m2692F9A7698099312B1956EF89ADC677E156325A,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onUpdateVector2_mC9D4DD0709714E0D052A2A6E475099B146375331,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onUpdateVector3_m1A96786092458EFAEAB22467DC6AA01F2F34247A,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onUpdateVector3_m91295D4F19AFF3080F6B44A071118CCDB52F288B,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onUpdateVector3Object_m2CAC540C86A54F7744AE39443FB4054066E3DE02,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onUpdateVector3Object_m2ECE95CB5D7DFE5E9DEDB9F3B03BEF39D4000B77,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onUpdateColor_m78FBDE4D9B8856EBE7EBF42709CFF81E127B9A43,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onUpdateColor_m7D77082D84B3831810A825A0628CE556012AAA2B,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onUpdateColorObject_m45EF15CE7E1F771A04B6E9374323759A7A021C62,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onUpdateColorObject_m25F9B89775D1630387BEDF4DC8B2F88F8E4842A0,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onComplete_m89C63A691247B03AF894749D3F32987E7994CFA8,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onComplete_m71A16E08537D67C75C706DD0EB88B7954A0CFCAF,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onCompleteObject_m50BB3E137E79340957CEE096A24A3635ACD91DEA,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onCompleteObject_m70FA4651F7E2CA76267B09C3EFBEA8AFE62F95E3,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onCompleteParam_mD546B9E0E31E70CEF0514DFB486D49E4BBE9D120,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onCompleteParam_mE4F5C590CFA4FF8204FE0C4722BF7EEBC1F4D7C7,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onUpdateParam_m8568043E9421D14172B62712FF0519738F0E8D88,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onUpdateParam_m415D9E73546338141E97EBEB8587C8A6F00C18E6,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_get_onStart_m67196E7D6B56A68A3D7A5BAC7201CB5D79575EEF,
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2_CustomAttributesCacheGenerator_LTDescrOptional_set_onStart_mFC780FBC4B2363070A9395BAA7693ACBFA3C75F8,
	LeanTester_t537C3FE41ED244252241E5B02F6B1BA6DA437297_CustomAttributesCacheGenerator_LeanTester_timeoutCheck_m59067A5600F0D2434CE9B7EDAC062829C85D5C00,
	U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6_CustomAttributesCacheGenerator_U3CtimeoutCheckU3Ed__2__ctor_mACFA1EA79EDBEA235C7474D1BA3E71804A23FEB8,
	U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6_CustomAttributesCacheGenerator_U3CtimeoutCheckU3Ed__2_System_IDisposable_Dispose_m0AF241B0BA496E508C41A00A92ACCB507F764F3B,
	U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6_CustomAttributesCacheGenerator_U3CtimeoutCheckU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF1EF5FF8E038CD04BB438F282E437032A52DF973,
	U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6_CustomAttributesCacheGenerator_U3CtimeoutCheckU3Ed__2_System_Collections_IEnumerator_Reset_m377EF3F9C3E2B3A09D749A0A347B92595F0D756D,
	U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6_CustomAttributesCacheGenerator_U3CtimeoutCheckU3Ed__2_System_Collections_IEnumerator_get_Current_m275B7C4BFE741FFBA1D7CE3E422460E5BD15EFF6,
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_CustomAttributesCacheGenerator_LeanTween_pause_mAF75D7E636CB0D02F8D515FF76C23D99B8B28FB1,
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_CustomAttributesCacheGenerator_LeanTween_resume_m1940BE978DB7EC3606D92C181FFE10A4570043E2,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanAlpha_m622BA3E2E95AA0C3ED769D14693452C71B464202,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanAlphaVertex_m276A64D30C7DBC2962D952F39164E74266A80805,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanAlpha_m5D4F86AB57DEAB1788098157DD3858155B41A026,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanAlpha_m80C6BB78F56CBCBAF562EA721D66F568AEB253E5,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanAlphaText_m2E879D95FD0A499D43742BC7E19C70FC00DB3C2F,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanCancel_mF59992A82C61655FB9C3C02CAA26A659B9DE3646,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanCancel_mD017D8DDC025544954D2E68BC260FC83C9469661,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanCancel_mC35E34FD1DF38362DA3970677A1A3745F02873E5,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanCancel_m17794FCEC25F233AC350724707F017F476BEA6B1,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanColor_m444318E458A875BB4CB2A0DD294D992CD93EBEFD,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanColorText_m400922DC92DE54BE07F329E4949145CF60571731,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanDelayedCall_m2377F6B8E51CB5CDEA727E0ECB5719E78E1E6DC8,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanDelayedCall_m813456BD5A6C3BEF150219E1FB7FFECF3CFDB8FB,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanIsPaused_m004A9E480599A969A094BBF5EAB8298D25F72877,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanIsPaused_mAB912B24C71D148315C8E71232F9B54C2FADB5A4,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanIsTweening_mECC74A5F933AEEEC71D8EBE7E977639DDD124F59,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMove_m054FBB59F420198B550A6069D9F9E6ABAE6AB43A,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMove_m615AA30F73F0ADF408BCE5B3EB2E7B0107F613DF,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMove_mE09C2744CD6F66260CCB2AE85EEC189D1739A95C,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMove_m213A2BEDDDE4818C47346D971A3D99CCE02F1E51,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMove_m0A44978D179428622357E68D3AE31FCD5597B014,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMove_m47C512A7C68F548F0C20D61A9EBC21444A89F47F,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMove_m28CCB67D4CD2B89B5A3F25E9D70842C949AD0990,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMove_mB1E50A4734E3C3CC223FEB98A394DBDBA9614850,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMove_m0BF63EB3A8D4B614817EA402EB1CC4B5F2C83328,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMove_m184D419AE52BA8FFCD237883D4118434DA2F40BB,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMove_m31A80E7BD7568CEFF3D7B4073D2BA176A9ADF3DF,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveLocal_m15178FB562F6F85C35B682AF97FCA004BDA1946F,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveLocal_m91600FA81E8D5DB2D9C69E4F7C5FC09CC40D2DC1,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveLocal_m98D7F1A2052D0F25DEE7B58DE411EE62A8B27CF2,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveLocal_m920B50D3FBD4DC072118E879AFC151D682AE64B0,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveLocal_mE4CF4E64AC16B510FB904060DDC2AE8BB231AA51,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveLocal_mA49B57CD94C59A10CA8AF9D53F71C48286EAFFD0,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveLocalX_m8847296A8B3401D2D054027ABB659B296FF73760,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveLocalY_m86889CA4B214309EEAC239C3F175937E24310FD6,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveLocalZ_m248064CFFDF7E42AC612D7F72C1E0F2DA37DCFB4,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveLocalX_mEAB28137FE65A74C1F4963ADCFF044CD5DB7CF69,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveLocalY_m727F1ED96B0D8FBF32EBB69787F82561886A07FA,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveLocalZ_m731E4A396200E9C165B47A327CB59CCDB9817707,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveSpline_m0E5CBB6704835E27EDA9B25D2915E8D2360E11A4,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveSpline_mF640B6FFF8B58004A5C0A4475FE36E2C5BBAC607,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveSpline_m3EE259E4A0061A1497D195E7910EAE9CD0B984C5,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveSpline_m59EF921EB07DAFBB715DAB0BBCFAC8003E67BD6C,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveSplineLocal_m667397DE03EFD1DBE0CE5F6CC2287807C3A56761,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveSplineLocal_mB8706C86995411A7D6408A04AB6B3545922C192D,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveX_m95E776B4AFF3B3472159C21ACCA7A074FD6250E1,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveX_m5EA136BF257503853FF984AB882BADCEDD22F864,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveX_m2E387E7F079BC81CCBE68B4D705C31E581639352,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveY_mD3811BDA209686DCA906AB52B2CDD29F337FD6F4,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveY_m65DEFE466DE737FAB4B1DEF0BD8DC8ED22D03433,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveY_m846E64F6EED274DD02405DF113895FD366BA0FBB,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveZ_mC2E89CF6F065D18A1F456935AF7EFA51091F0D9B,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveZ_m85D8863F40BBF5A99203396C72B421E022FAA417,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanMoveZ_mD47791E76E05328EAE363F9A3D0B8DAE6713902A,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanPause_m3B5948BE9F86AAB0C837383D547DF2B291B8EA43,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanPlay_m6DCF6A71031221B880B66660EFF17720C6F1A42D,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanResume_m0C9B09411CA1C367B2E2F1E74ECC4860BB400BF4,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotate_m9A3EA584BE26EADB024F0E5EC57B0862F3B0BEAE,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotate_m46EDCCD0F931B4488BF8EDE72F0B1BA6BD65261F,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotate_mF34054132FFA550762A3C04473FEF3FB54931CA0,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotateAround_m97EA89B0C3FDE7B29B2A5F2FE3BE10F27B0C9993,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotateAround_m9922AD8260E8767A2D33963F2C9BA18ADDF22D61,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotateAround_m7F47B45F2074B30B22D75DC117BCF0A1351710E3,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotateAroundLocal_m43F940634C80C6D459B9DACF22F06E1E42F29116,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotateAroundLocal_m81397BB2C883AC5B3D62716FC3C93F43AE4EE5EB,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotateAroundLocal_mE1E669F2BCAAEA47AB4785D6BBFAE413CC74F053,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotateX_m0C6142BF35F74AC9A912509713E53E9F1B3F1A20,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotateX_mD09A3A174615A5479F81DFF31192673DADBF3324,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotateY_mE334362113CE7DA83AB29EFA1AAB8BFEB8C326E0,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotateY_m823DD2E4B248E09CA4E2CDDFD2D791DF8527FCD2,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotateZ_m587B987933373ED9B32F6D319F158899A25AF9C3,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanRotateZ_m25AA9563CCFC5754ABE0CDE84FE8E05F09B0357F,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanScale_m883D32607662D32DAB92DAF865CF110680BF18B3,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanScale_m572C2CB624F08B589CA3F7CEDA18D0521D396DAF,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanScale_mFDA8A475A889665712B39717572167439241C6D5,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanScaleX_m9AC6DED15F097E50134DF4213F86799F3A3F370B,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanScaleX_m469A49E471B4D706CF5859D368CD3F13DD4FBA67,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanScaleY_mEF7B8B2B411B70E312A061227CC9EF7496F1A720,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanScaleY_mC152F2D04B0CB5770AF0157B8BA217DDC0881D61,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanScaleZ_m23CAE903347B1DFEA59FFFB127419620967227F1,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanScaleZ_m2C148ADC5EA94A54CC00F349B1DF331079B13B16,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanSize_m410361AD3B78FE2A69D65C21570490F82E660241,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanValue_m1FB1FFD9157FA40B5854744EA15111178AEDE1C1,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanValue_mE9099E0F64A010AD2BEA7030C5FEFEDBE78A1571,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanValue_mF09B8E5D72CBADA7E7335A1CCE8397B6BEE0B286,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanValue_m165CCF09A13FA398105E06E6B441D0C5BEF33E20,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanValue_m2D0676A4B078FE10C1819ECF5520374CA2E39E8D,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanValue_m626C6C95ABDA16428DAD1399B0862363589AC9F9,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanValue_mBB6A5387301F308B4C42B4DF5B70C17AE29F5533,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanValue_m93C212023658F9EF6A27683DEBAA852EE4670411,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanValue_m5B60256ABE5F8B7E28F389AAE1696EB656633398,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanValue_mBA982D980197033944CF17D7C7F236A6CED885DF,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanSetPosX_m82201BB5C9781FA41FB69DDA05471D693E59DF2E,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanSetPosY_mD9DAE6B462055110382822D16B5551854CB68DD7,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanSetPosZ_m1E17DB053DD69876E163757D053A85B172CAC027,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanSetLocalPosX_m2325658730387BE391CB8FE674FD3FD2D8D3ECDE,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanSetLocalPosY_mF14BA20EFA312158AB0FD456EB8762457DB03AE5,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanSetLocalPosZ_m685180D2C04DF7E3417E9EECA9BEBE60F0850790,
	LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414_CustomAttributesCacheGenerator_LeanTweenExt_LeanColor_m2027EB778950A5E71F3DCF398F8A6A77AE32EF35,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_GameManager_get_CurrentPageGame_mBB7545C1F39191029C356CE8A0CF621C05A0466D,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_GameManager_set_CurrentPageGame_m9D471B21E93F777A181EC198961365863F981851,
	GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607_CustomAttributesCacheGenerator_GameScale_get_CurrentStatus_m38D0B805AB43526506AAFAF0BCFB3280942720AC,
	GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607_CustomAttributesCacheGenerator_GameScale_set_CurrentStatus_m15A0A482C1931D16C7AA8CAB0CE5A03E0C45B03D,
	GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607_CustomAttributesCacheGenerator_GameScale_ScaleObject_m0585571AAE75600F571926D1F9F72348F82407BB,
	GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607_CustomAttributesCacheGenerator_GameScale_U3CShrinkU3Eb__12_0_m2F0F00081290A6FC6F7AC0290DE44009D3B01527,
	GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607_CustomAttributesCacheGenerator_GameScale_U3CGrowU3Eb__13_0_mD9600484A0A19E931BAE06A6BCB1EEF0A96AF277,
	U3CScaleObjectU3Ed__11_t461C201F3015C43C04CB97A6D8D45A7B73627A6D_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__11__ctor_m9F9912F1FE2061B80649C3E2D3EE03F0C81A2ED2,
	U3CScaleObjectU3Ed__11_t461C201F3015C43C04CB97A6D8D45A7B73627A6D_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__11_System_IDisposable_Dispose_m1F6F9E741217D4A31F59B0AB382F7EA581962124,
	U3CScaleObjectU3Ed__11_t461C201F3015C43C04CB97A6D8D45A7B73627A6D_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA18277648E8ACA4C90D1053F66BB3A45B40A6504,
	U3CScaleObjectU3Ed__11_t461C201F3015C43C04CB97A6D8D45A7B73627A6D_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__11_System_Collections_IEnumerator_Reset_m03133167C210B463F8914107D14565D5EF30F003,
	U3CScaleObjectU3Ed__11_t461C201F3015C43C04CB97A6D8D45A7B73627A6D_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__11_System_Collections_IEnumerator_get_Current_m3D9D148281A0AE0E2A411244F4636E11A8AC4541,
	Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867_CustomAttributesCacheGenerator_Idle_WaitAndActivate_m70D559158D8C1EDF0B9B44BD9823D4611AB2290B,
	Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867_CustomAttributesCacheGenerator_Idle_WaitAndDeactivate_m59520F96D5799F5496ADC573057933A6555CD144,
	U3CWaitAndActivateU3Ed__6_t503E05974A5610E1E9848BFB9321AE1CE472AE6D_CustomAttributesCacheGenerator_U3CWaitAndActivateU3Ed__6__ctor_m372FCD69AC54F9B6F417E419D1CBE41AFEBAF14A,
	U3CWaitAndActivateU3Ed__6_t503E05974A5610E1E9848BFB9321AE1CE472AE6D_CustomAttributesCacheGenerator_U3CWaitAndActivateU3Ed__6_System_IDisposable_Dispose_m0D3874C983C3F7471BE43376345B2AA14B702A36,
	U3CWaitAndActivateU3Ed__6_t503E05974A5610E1E9848BFB9321AE1CE472AE6D_CustomAttributesCacheGenerator_U3CWaitAndActivateU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m41C234EFF7D21326815858026BE32FE067EF619F,
	U3CWaitAndActivateU3Ed__6_t503E05974A5610E1E9848BFB9321AE1CE472AE6D_CustomAttributesCacheGenerator_U3CWaitAndActivateU3Ed__6_System_Collections_IEnumerator_Reset_m5C8CE0D8F95DDAB7A3EFB667513B448F26BCE043,
	U3CWaitAndActivateU3Ed__6_t503E05974A5610E1E9848BFB9321AE1CE472AE6D_CustomAttributesCacheGenerator_U3CWaitAndActivateU3Ed__6_System_Collections_IEnumerator_get_Current_m1D86BB210D7EA8F9BE13DD49A67154BD11E2BFAB,
	U3CWaitAndDeactivateU3Ed__7_t0B9854B91CC5946899CC4024F6D32F274A81643C_CustomAttributesCacheGenerator_U3CWaitAndDeactivateU3Ed__7__ctor_m3557474DAA9C5DB0CD5BD4032344C0A53452B250,
	U3CWaitAndDeactivateU3Ed__7_t0B9854B91CC5946899CC4024F6D32F274A81643C_CustomAttributesCacheGenerator_U3CWaitAndDeactivateU3Ed__7_System_IDisposable_Dispose_m9FEB66AAD863C5A40D5E5420E53BFB2E7586E4D8,
	U3CWaitAndDeactivateU3Ed__7_t0B9854B91CC5946899CC4024F6D32F274A81643C_CustomAttributesCacheGenerator_U3CWaitAndDeactivateU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m554F125318195969EAA9504CBBDB3D77CE7CB917,
	U3CWaitAndDeactivateU3Ed__7_t0B9854B91CC5946899CC4024F6D32F274A81643C_CustomAttributesCacheGenerator_U3CWaitAndDeactivateU3Ed__7_System_Collections_IEnumerator_Reset_m8B28B60638773A42F14FBE776C0E896D4F2AE0C4,
	U3CWaitAndDeactivateU3Ed__7_t0B9854B91CC5946899CC4024F6D32F274A81643C_CustomAttributesCacheGenerator_U3CWaitAndDeactivateU3Ed__7_System_Collections_IEnumerator_get_Current_m4AA71565E06F69AFC1CE8BCED114941F756B919B,
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator_PageGame_get_CurrentStatus_m62216C85BE6E5B3889395214E48C988D10E4ADAC,
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator_PageGame_set_CurrentStatus_m80C5F4ACC165A3A47F04F47DD7C04D97E914D5C5,
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator_PageGame_get_CompletionCount_m163C431B336779F854B28006C445D9ACFBE41E3C,
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5_CustomAttributesCacheGenerator_PageGame_set_CompletionCount_m773C823765A76AA176ACE8A5D7CC238B07797EA2,
	Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2_CustomAttributesCacheGenerator_Spawn_get_CurrentStatus_m5E2624F5D15120BAF25669DFC891BB10BF2FE067,
	Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2_CustomAttributesCacheGenerator_Spawn_set_CurrentStatus_mA3512D04052416AB2B5D7167D64D39DE8C41C084,
	Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2_CustomAttributesCacheGenerator_Spawn_ScaleObject_mA7A227A8BE23DEEF647D38A89FDC136C1CA68A82,
	Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2_CustomAttributesCacheGenerator_Spawn_U3CGrowU3Eb__12_0_mCACEB31935EBC09019449E1370A1225EFE1140CD,
	U3CScaleObjectU3Ed__11_t4E9DC2B458A4FA829BDAE1A00561A16F1A8A40F5_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__11__ctor_m8D9F6C2638A2271E6525C7972EDFC7CAB742B0DD,
	U3CScaleObjectU3Ed__11_t4E9DC2B458A4FA829BDAE1A00561A16F1A8A40F5_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__11_System_IDisposable_Dispose_m40B8BFE624A73F108E46FFFA98B2AE4D6787CB5E,
	U3CScaleObjectU3Ed__11_t4E9DC2B458A4FA829BDAE1A00561A16F1A8A40F5_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m30FB78064DC61798F0BFA19E5D2F12A85975F0F5,
	U3CScaleObjectU3Ed__11_t4E9DC2B458A4FA829BDAE1A00561A16F1A8A40F5_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__11_System_Collections_IEnumerator_Reset_m10257A8A5A594206D1B8A584528BFC9A24615121,
	U3CScaleObjectU3Ed__11_t4E9DC2B458A4FA829BDAE1A00561A16F1A8A40F5_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__11_System_Collections_IEnumerator_get_Current_m116291CCC3A2D4AAB33B5F5683271A7799F9B80D,
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F_CustomAttributesCacheGenerator_SplashWorldTapThrow_U3CThrowU3Eb__30_0_m0D5CC01A9E3F3BBE5BBC74F21EFB1AA7F962E30A,
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F_CustomAttributesCacheGenerator_SplashWorldTapThrow_U3CThrowU3Eb__30_1_m646A85613E11A7EC17CAFB9A2140FA9E49E07A67,
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056_CustomAttributesCacheGenerator_StarfishTap_U3CTapU3Eg__WaitAndActivateU7C20_2_m45303C54140A66C6A1C3AA9FB164850D9B9E0605,
	U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_t610B7E25C1F7E862A6CB4B61E84B0023BAA56FA9_CustomAttributesCacheGenerator_U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed__ctor_mC1FAC773B7C389314756A2BC1C0B935D629E5CB8,
	U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_t610B7E25C1F7E862A6CB4B61E84B0023BAA56FA9_CustomAttributesCacheGenerator_U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_System_IDisposable_Dispose_mD1831FF6B8869F465B692C7BC3440334E3C277AE,
	U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_t610B7E25C1F7E862A6CB4B61E84B0023BAA56FA9_CustomAttributesCacheGenerator_U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9764191B0083E73AC10D2DD64B724AF7BFE30151,
	U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_t610B7E25C1F7E862A6CB4B61E84B0023BAA56FA9_CustomAttributesCacheGenerator_U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_System_Collections_IEnumerator_Reset_m2A883B4CA0FA17D2368B73A8DD265F095C3662D5,
	U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_t610B7E25C1F7E862A6CB4B61E84B0023BAA56FA9_CustomAttributesCacheGenerator_U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_System_Collections_IEnumerator_get_Current_m15B9F318820B9184FA7C97E17B84E9AA142D375C,
	UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55_CustomAttributesCacheGenerator_UIScale_get_CurrentStatus_m59B74B309E7DEDD304B5975587C597227D35AAF7,
	UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55_CustomAttributesCacheGenerator_UIScale_set_CurrentStatus_m3DDDB961B7587E70B838BEAFB6B78E2D9C577004,
	UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55_CustomAttributesCacheGenerator_UIScale_ScaleObject_m2D6ACA6D185145F4DC427EF2D42A49EAC233369D,
	UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55_CustomAttributesCacheGenerator_UIScale_U3CShrinkU3Eb__13_0_mFFDD536FF19492248C5AF5931C4DF93F7AB58411,
	UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55_CustomAttributesCacheGenerator_UIScale_U3CGrowU3Eb__14_0_m3BB4510B628C633F140F8A7EE6AC08E08170E52A,
	U3CScaleObjectU3Ed__12_t8DC31192573F6DFEF7FBA86C992E3587EAA46457_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__12__ctor_mDC451B5F819E9A4AE8516332FB6EB086259AB187,
	U3CScaleObjectU3Ed__12_t8DC31192573F6DFEF7FBA86C992E3587EAA46457_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__12_System_IDisposable_Dispose_m18311F276F727390C6BB8F45547A1055AD46B687,
	U3CScaleObjectU3Ed__12_t8DC31192573F6DFEF7FBA86C992E3587EAA46457_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m208AC32EA8F4EFFA396F863484497848182EE124,
	U3CScaleObjectU3Ed__12_t8DC31192573F6DFEF7FBA86C992E3587EAA46457_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__12_System_Collections_IEnumerator_Reset_mDCE696778B7C746AF2B21B9EDBF0EEDD9FA7C644,
	U3CScaleObjectU3Ed__12_t8DC31192573F6DFEF7FBA86C992E3587EAA46457_CustomAttributesCacheGenerator_U3CScaleObjectU3Ed__12_System_Collections_IEnumerator_get_Current_m919B949113F60B7761002096FB3E96CDF935EDD5,
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757_CustomAttributesCacheGenerator_TestingUnitTests_timeBasedTesting_mBD8B52CEA7286C9F4FAAA419C773B78255E55F4D,
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757_CustomAttributesCacheGenerator_TestingUnitTests_lotsOfCancels_mD21D0D0D0B8BE316E58C412A19ADE6658DAA5134,
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757_CustomAttributesCacheGenerator_TestingUnitTests_pauseTimeNow_m3293FFC3DDEC01D507093F2D2B8E34B40FC0DD7F,
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757_CustomAttributesCacheGenerator_TestingUnitTests_U3ClotsOfCancelsU3Eb__25_0_m79612DDE2FFAD2A0986AA0D91A768BA96269D4F6,
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757_CustomAttributesCacheGenerator_TestingUnitTests_U3CpauseTimeNowU3Eb__26_1_mB0C630E660D38F819734F11E2998CDE43590D503,
	U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65_CustomAttributesCacheGenerator_U3CtimeBasedTestingU3Ed__24__ctor_mBB473D0F9FCF74C17EE67A02377DBAC113B0ECE6,
	U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65_CustomAttributesCacheGenerator_U3CtimeBasedTestingU3Ed__24_System_IDisposable_Dispose_m2240ABE280E91D3110F4956F4DDAE8A6F211340A,
	U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65_CustomAttributesCacheGenerator_U3CtimeBasedTestingU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mACB1A3175D122506B71BCDB9EBF4AA9F0383E74B,
	U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65_CustomAttributesCacheGenerator_U3CtimeBasedTestingU3Ed__24_System_Collections_IEnumerator_Reset_m96D9D8DFA5B24A08E71FA9EA4595288F06E9837B,
	U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65_CustomAttributesCacheGenerator_U3CtimeBasedTestingU3Ed__24_System_Collections_IEnumerator_get_Current_mA059C8602E0A9BCCC1849108A55B19E09E4818CA,
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523_CustomAttributesCacheGenerator_U3ClotsOfCancelsU3Ed__25__ctor_m9B88818966D8A321B3F23FB53C461C28A354F1C0,
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523_CustomAttributesCacheGenerator_U3ClotsOfCancelsU3Ed__25_System_IDisposable_Dispose_m40E5F2D2626AB1E624CA39E3740CDFBE5B4F1B30,
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523_CustomAttributesCacheGenerator_U3ClotsOfCancelsU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2F111DF2BFDD6B3974DFA490B271DC9FCCBFC1DB,
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523_CustomAttributesCacheGenerator_U3ClotsOfCancelsU3Ed__25_System_Collections_IEnumerator_Reset_m440F860762B28408E3B9AB6D72144B5E6A1C6F8C,
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523_CustomAttributesCacheGenerator_U3ClotsOfCancelsU3Ed__25_System_Collections_IEnumerator_get_Current_mA7D1CE85A7D50D60D73C5914F5EC60B5E33A344E,
	U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB_CustomAttributesCacheGenerator_U3CpauseTimeNowU3Ed__26__ctor_mD1AE96EC04B7EB3C54830BBCA910ACEC6A857D84,
	U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB_CustomAttributesCacheGenerator_U3CpauseTimeNowU3Ed__26_System_IDisposable_Dispose_mE8F0437ED1B062C967A1FFCA460708BD42BC5142,
	U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB_CustomAttributesCacheGenerator_U3CpauseTimeNowU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD12A76EAE3CD0B1B9C938030B3E4D2FAA6456011,
	U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB_CustomAttributesCacheGenerator_U3CpauseTimeNowU3Ed__26_System_Collections_IEnumerator_Reset_m88957DE3F231D3C4F8E64C6DADA45876FABB7FD0,
	U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB_CustomAttributesCacheGenerator_U3CpauseTimeNowU3Ed__26_System_Collections_IEnumerator_get_Current_m04EA51DB175A4043B51D9B3CFDD410D924B422A0,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
