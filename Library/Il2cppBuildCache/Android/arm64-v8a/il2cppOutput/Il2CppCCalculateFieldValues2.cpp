﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>
#include <limits>



// System.Action`1<UnityEngine.Color>
struct Action_1_tC747810051096FEC47140D886015F5428DEDDAE2;
// System.Action`1<System.Object>
struct Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC;
// System.Action`1<System.Single>
struct Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9;
// System.Action`1<UnityEngine.Vector2>
struct Action_1_t0C1F417511439CBAA03909A69138FAF0BD19FA30;
// System.Action`1<UnityEngine.Vector3>
struct Action_1_tC2B4AB26EC30C6FC4AD8C9172FE509B3B4E1C26B;
// System.Action`2<UnityEngine.Color,System.Object>
struct Action_2_t05923E9D06D867E1AC6A90E2B1B2FE28CA9F910A;
// System.Action`2<System.Single,System.Object>
struct Action_2_tC5EA1BA82797894656DD89A0F3E7B791EC8BD050;
// System.Action`2<System.Single,System.Single>
struct Action_2_tBA82D2430D3596594262E1ACAD640A8EEB5DB2F2;
// System.Action`2<UnityEngine.Vector3,System.Object>
struct Action_2_tCA8676CFBCEAA1EE28321875815FD1767B25E4CB;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_tADDBF4C67A82C92CD16099CD166D7F72E8DCB13C;
// System.Collections.Generic.List`1<PageGame>
struct List_1_t4A2EDA437C1CBDE74B20DD2C43CE752D0469B0A0;
// System.Collections.Generic.List`1<StarfishTap>
struct List_1_tA1ADFC988916A9C8B9DE6B0B0E277C1F7EEF099B;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181;
// System.Action`1<LTEvent>[]
struct Action_1U5BU5D_t930658157BBD9409C35E85E5A34B23FC4EEF0772;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// LTBezier[]
struct LTBezierU5BU5D_t4DDB3352316B2B476AEEE5AB4F760513856FC153;
// LTDescr[]
struct LTDescrU5BU5D_t731554F94C09A2A31CB19EA8663E934537DF797F;
// LTRect[]
struct LTRectU5BU5D_t764F00C0F4846E8D85477368EC19E34DF5D0DCBA;
// LTSeq[]
struct LTSeqU5BU5D_t5C740B5E327381CE2E6FD41B011EA9A72355FD51;
// UnityEngine.Rect[]
struct RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE;
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// UnityEngine.Transform[]
struct TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// Readme/Section[]
struct SectionU5BU5D_t013C5C59593F9D61D69A33E91B8C1450419ED659;
// UnityEngine.XR.ARFoundation.ARAnchorManager
struct ARAnchorManager_t969330AB785F0DC41EF9F4390D77F7ABA30F7D0F;
// UnityEngine.XR.ARFoundation.ARRaycastManager
struct ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F;
// UnityEngine.XR.ARFoundation.ARTrackedImageManager
struct ARTrackedImageManager_tB916E34D053E6712190F2BAE46E21D76A0882FF2;
// System.Action
struct Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6;
// UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03;
// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// UnityEngine.AudioClip
struct AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE;
// UnityEngine.AudioListener
struct AudioListener_t03B51B434A263F9AFD07AC8AA5CB4FE6402252A3;
// UnityEngine.AudioSource
struct AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// DefaultObserverEventHandler
struct DefaultObserverEventHandler_t25FF9CE0FF0ED822CE21D6AE20C6A6E77105C02B;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// UnityEngine.GUIStyle
struct GUIStyle_t29C59470ACD0A35C81EB0615653FD38C455A4726;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// GameScale
struct GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// Idle
struct Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// Vuforia.ImageTargetBehaviour
struct ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34;
// LTBezierPath
struct LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48;
// LTDescr
struct LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F;
// LTDescrOptional
struct LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2;
// LTRect
struct LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD;
// LTSpline
struct LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701;
// LeanAudioStream
struct LeanAudioStream_tDA1B6363B48B65291F4C928A5A706C159FD13712;
// LeanTester
struct LeanTester_t537C3FE41ED244252241E5B02F6B1BA6DA437297;
// UnityEngine.Light
struct Light_tA2F349FE839781469A0344CF6039B51512394275;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// PageGame
struct PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5;
// PageManager
struct PageManager_t2117D39B2F2C5C8A4EE38E159E54B69771568945;
// PageTracker
struct PageTracker_tF742F2BC772F15FC123563EFFEA7932454A688A2;
// UnityEngine.ParticleSystem
struct ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E;
// UnityEngine.UI.RawImage
struct RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// UnityEngine.Rigidbody
struct Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A;
// UnityEngine.UI.Slider
struct Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A;
// Spawn
struct Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2;
// SplashWorld
struct SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC;
// SplashWorldTapThrow
struct SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF;
// StarfishTap
struct StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056;
// System.String
struct String_t;
// DentedPixel.LTExamples.TestingUnitTests
struct TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1;
// UnityEngine.Texture
struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.Time
struct Time_tCE5C6E624BDC86B30112C860F5622AFA25F1EC9F;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UIScale
struct UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55;
// UnityEngine.Video.VideoPlayer
struct VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// GameEvents/OnARCameraPoseChangedDelegate
struct OnARCameraPoseChangedDelegate_t1E51F102F4DED797A7CA358A3A0D7D92BB5BE292;
// GameEvents/OnCoinsCollectedDelegate
struct OnCoinsCollectedDelegate_t73E03EBD90D903F0CEEEB4C5E86860BEF7290B94;
// GameEvents/OnFadeToBlackDelegate
struct OnFadeToBlackDelegate_tD0C3CF3048E7FD45B6BA5332BDE5BB05FD59FE3F;
// GameEvents/OnFirstPageTrackedDelegate
struct OnFirstPageTrackedDelegate_tB10EA0A4FC2EDB8A727310557768DA2D7E3E942B;
// GameEvents/OnFlickableDraggedDelegate
struct OnFlickableDraggedDelegate_tBAF20D4FC47CDFC5EEFDF658277B86EBBFD03E1B;
// GameEvents/OnFlickableTappedDelegate
struct OnFlickableTappedDelegate_t1C5F3C2CB74A16B63E9997D152CDDFA196465908;
// GameEvents/OnFlickableThrownDelegate
struct OnFlickableThrownDelegate_tE88AB9432A7582F497E8E34507D71BF606B1C272;
// GameEvents/OnPageGameBlockedDelegate
struct OnPageGameBlockedDelegate_t3C6BA7A1E6F919151BC27121DD48978603508B60;
// GameEvents/OnPageGameCompletedDelegate
struct OnPageGameCompletedDelegate_tEC600C09E877109BC0B80DFECFD4F95ABE0F29CB;
// GameEvents/OnPageGameLookAtRaycastDelegate
struct OnPageGameLookAtRaycastDelegate_t0A2901F0757A4782C71B8715980C933FC6ACEF35;
// GameEvents/OnPageGameStatusChangedDelegate
struct OnPageGameStatusChangedDelegate_t51DDB305321C9999BC37D3FB2AD067F4D0E5AE8C;
// GameEvents/OnPageGameTouchRaycastDelegate
struct OnPageGameTouchRaycastDelegate_t79ACF0C496B3D911F73287E3821D3D9DB70F9387;
// GameEvents/OnSplashWorldEnterDelegate
struct OnSplashWorldEnterDelegate_tE10056481597DD80FAD0E3FF0E0E174EAEE5D7CF;
// GameEvents/OnSplashWorldExitDelegate
struct OnSplashWorldExitDelegate_tD93D4940EAF1FF4E23A372D4D88439D4273F4557;
// GameEvents/OnSplashWorldLookAtRaycastDelegate
struct OnSplashWorldLookAtRaycastDelegate_tA4690438A19C04948CA6609796DCAC2EE70BF3B0;
// GameEvents/OnSplashWorldTouchRaycastDelegate
struct OnSplashWorldTouchRaycastDelegate_t52F1E630AFC268B9D26C5738F63805FF40B0D027;
// GameEvents/OnStarfishTappedDelegate
struct OnStarfishTappedDelegate_tDEDDAA2572797A17474E1AF26CF4DD06B0C60317;
// GameEvents/OnTapMoveDelegate
struct OnTapMoveDelegate_t723F9E3AED1681A747EFCEE8EF576177F038188D;
// GameEvents/OnTapOffDelegate
struct OnTapOffDelegate_t40D5D499048112C9CCF95E6FF8CB13323679CF81;
// GameEvents/OnTapOnDelegate
struct OnTapOnDelegate_tB00348CD112FB24724A207D1A265741539F5C9A1;
// GameEvents/OnTotalCoinsUpdatedDelegate
struct OnTotalCoinsUpdatedDelegate_tB29199907D790A984C111DF7FB8A6669C9C4C0A5;
// LTDescr/ActionMethodDelegate
struct ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05;
// LTDescr/EaseTypeDelegate
struct EaseTypeDelegate_t0285AB55B28F5B5A6688E966B51FBA837B2E84AF;
// UnityTemplateProjects.SimpleCameraController/CameraState
struct CameraState_t3CFB67357E39129414AC5C9D628B9B47015CCE8C;
// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0
struct U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F;
// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0
struct U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// GameEvents
struct GameEvents_t304F2D5249A807B067595CF817CA809E1051942C  : public RuntimeObject
{
public:

public:
};

struct GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields
{
public:
	// GameEvents/OnARCameraPoseChangedDelegate GameEvents::OnARCameraPoseChanged
	OnARCameraPoseChangedDelegate_t1E51F102F4DED797A7CA358A3A0D7D92BB5BE292 * ___OnARCameraPoseChanged_0;
	// GameEvents/OnPageGameStatusChangedDelegate GameEvents::OnPageGameStatusChanged
	OnPageGameStatusChangedDelegate_t51DDB305321C9999BC37D3FB2AD067F4D0E5AE8C * ___OnPageGameStatusChanged_1;
	// GameEvents/OnPageGameCompletedDelegate GameEvents::OnPageGameCompleted
	OnPageGameCompletedDelegate_tEC600C09E877109BC0B80DFECFD4F95ABE0F29CB * ___OnPageGameCompleted_2;
	// GameEvents/OnPageGameBlockedDelegate GameEvents::OnPageGameBlocked
	OnPageGameBlockedDelegate_t3C6BA7A1E6F919151BC27121DD48978603508B60 * ___OnPageGameBlocked_3;
	// GameEvents/OnPageGameLookAtRaycastDelegate GameEvents::OnPageGameLookAtRaycast
	OnPageGameLookAtRaycastDelegate_t0A2901F0757A4782C71B8715980C933FC6ACEF35 * ___OnPageGameLookAtRaycast_4;
	// GameEvents/OnPageGameTouchRaycastDelegate GameEvents::OnPageGameTapRaycast
	OnPageGameTouchRaycastDelegate_t79ACF0C496B3D911F73287E3821D3D9DB70F9387 * ___OnPageGameTapRaycast_5;
	// GameEvents/OnSplashWorldEnterDelegate GameEvents::OnSplashWorldEnter
	OnSplashWorldEnterDelegate_tE10056481597DD80FAD0E3FF0E0E174EAEE5D7CF * ___OnSplashWorldEnter_6;
	// GameEvents/OnSplashWorldExitDelegate GameEvents::OnSplashWorldExit
	OnSplashWorldExitDelegate_tD93D4940EAF1FF4E23A372D4D88439D4273F4557 * ___OnSplashWorldExit_7;
	// GameEvents/OnSplashWorldLookAtRaycastDelegate GameEvents::OnSplashWorldLookAtRaycast
	OnSplashWorldLookAtRaycastDelegate_tA4690438A19C04948CA6609796DCAC2EE70BF3B0 * ___OnSplashWorldLookAtRaycast_8;
	// GameEvents/OnSplashWorldTouchRaycastDelegate GameEvents::OnSplashWorldTapRaycast
	OnSplashWorldTouchRaycastDelegate_t52F1E630AFC268B9D26C5738F63805FF40B0D027 * ___OnSplashWorldTapRaycast_9;
	// GameEvents/OnTapOnDelegate GameEvents::OnTapOn
	OnTapOnDelegate_tB00348CD112FB24724A207D1A265741539F5C9A1 * ___OnTapOn_10;
	// GameEvents/OnTapMoveDelegate GameEvents::OnTapMove
	OnTapMoveDelegate_t723F9E3AED1681A747EFCEE8EF576177F038188D * ___OnTapMove_11;
	// GameEvents/OnTapOffDelegate GameEvents::OnTapOff
	OnTapOffDelegate_t40D5D499048112C9CCF95E6FF8CB13323679CF81 * ___OnTapOff_12;
	// GameEvents/OnFirstPageTrackedDelegate GameEvents::OnFirstPageTracked
	OnFirstPageTrackedDelegate_tB10EA0A4FC2EDB8A727310557768DA2D7E3E942B * ___OnFirstPageTracked_13;
	// GameEvents/OnStarfishTappedDelegate GameEvents::OnStarfishTapped
	OnStarfishTappedDelegate_tDEDDAA2572797A17474E1AF26CF4DD06B0C60317 * ___OnStarfishTapped_14;
	// GameEvents/OnFlickableTappedDelegate GameEvents::OnFlickableTapped
	OnFlickableTappedDelegate_t1C5F3C2CB74A16B63E9997D152CDDFA196465908 * ___OnFlickableTapped_15;
	// GameEvents/OnFlickableDraggedDelegate GameEvents::OnFlickableDragged
	OnFlickableDraggedDelegate_tBAF20D4FC47CDFC5EEFDF658277B86EBBFD03E1B * ___OnFlickableDragged_16;
	// GameEvents/OnFlickableThrownDelegate GameEvents::OnFlickableThrown
	OnFlickableThrownDelegate_tE88AB9432A7582F497E8E34507D71BF606B1C272 * ___OnFlickableThrown_17;
	// GameEvents/OnFadeToBlackDelegate GameEvents::OnFadeToBlack
	OnFadeToBlackDelegate_tD0C3CF3048E7FD45B6BA5332BDE5BB05FD59FE3F * ___OnFadeToBlack_18;
	// GameEvents/OnCoinsCollectedDelegate GameEvents::OnCoinsCollected
	OnCoinsCollectedDelegate_t73E03EBD90D903F0CEEEB4C5E86860BEF7290B94 * ___OnCoinsCollected_19;
	// GameEvents/OnTotalCoinsUpdatedDelegate GameEvents::OnTotalCoinsUpdated
	OnTotalCoinsUpdatedDelegate_tB29199907D790A984C111DF7FB8A6669C9C4C0A5 * ___OnTotalCoinsUpdated_20;

public:
	inline static int32_t get_offset_of_OnARCameraPoseChanged_0() { return static_cast<int32_t>(offsetof(GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields, ___OnARCameraPoseChanged_0)); }
	inline OnARCameraPoseChangedDelegate_t1E51F102F4DED797A7CA358A3A0D7D92BB5BE292 * get_OnARCameraPoseChanged_0() const { return ___OnARCameraPoseChanged_0; }
	inline OnARCameraPoseChangedDelegate_t1E51F102F4DED797A7CA358A3A0D7D92BB5BE292 ** get_address_of_OnARCameraPoseChanged_0() { return &___OnARCameraPoseChanged_0; }
	inline void set_OnARCameraPoseChanged_0(OnARCameraPoseChangedDelegate_t1E51F102F4DED797A7CA358A3A0D7D92BB5BE292 * value)
	{
		___OnARCameraPoseChanged_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnARCameraPoseChanged_0), (void*)value);
	}

	inline static int32_t get_offset_of_OnPageGameStatusChanged_1() { return static_cast<int32_t>(offsetof(GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields, ___OnPageGameStatusChanged_1)); }
	inline OnPageGameStatusChangedDelegate_t51DDB305321C9999BC37D3FB2AD067F4D0E5AE8C * get_OnPageGameStatusChanged_1() const { return ___OnPageGameStatusChanged_1; }
	inline OnPageGameStatusChangedDelegate_t51DDB305321C9999BC37D3FB2AD067F4D0E5AE8C ** get_address_of_OnPageGameStatusChanged_1() { return &___OnPageGameStatusChanged_1; }
	inline void set_OnPageGameStatusChanged_1(OnPageGameStatusChangedDelegate_t51DDB305321C9999BC37D3FB2AD067F4D0E5AE8C * value)
	{
		___OnPageGameStatusChanged_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPageGameStatusChanged_1), (void*)value);
	}

	inline static int32_t get_offset_of_OnPageGameCompleted_2() { return static_cast<int32_t>(offsetof(GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields, ___OnPageGameCompleted_2)); }
	inline OnPageGameCompletedDelegate_tEC600C09E877109BC0B80DFECFD4F95ABE0F29CB * get_OnPageGameCompleted_2() const { return ___OnPageGameCompleted_2; }
	inline OnPageGameCompletedDelegate_tEC600C09E877109BC0B80DFECFD4F95ABE0F29CB ** get_address_of_OnPageGameCompleted_2() { return &___OnPageGameCompleted_2; }
	inline void set_OnPageGameCompleted_2(OnPageGameCompletedDelegate_tEC600C09E877109BC0B80DFECFD4F95ABE0F29CB * value)
	{
		___OnPageGameCompleted_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPageGameCompleted_2), (void*)value);
	}

	inline static int32_t get_offset_of_OnPageGameBlocked_3() { return static_cast<int32_t>(offsetof(GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields, ___OnPageGameBlocked_3)); }
	inline OnPageGameBlockedDelegate_t3C6BA7A1E6F919151BC27121DD48978603508B60 * get_OnPageGameBlocked_3() const { return ___OnPageGameBlocked_3; }
	inline OnPageGameBlockedDelegate_t3C6BA7A1E6F919151BC27121DD48978603508B60 ** get_address_of_OnPageGameBlocked_3() { return &___OnPageGameBlocked_3; }
	inline void set_OnPageGameBlocked_3(OnPageGameBlockedDelegate_t3C6BA7A1E6F919151BC27121DD48978603508B60 * value)
	{
		___OnPageGameBlocked_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPageGameBlocked_3), (void*)value);
	}

	inline static int32_t get_offset_of_OnPageGameLookAtRaycast_4() { return static_cast<int32_t>(offsetof(GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields, ___OnPageGameLookAtRaycast_4)); }
	inline OnPageGameLookAtRaycastDelegate_t0A2901F0757A4782C71B8715980C933FC6ACEF35 * get_OnPageGameLookAtRaycast_4() const { return ___OnPageGameLookAtRaycast_4; }
	inline OnPageGameLookAtRaycastDelegate_t0A2901F0757A4782C71B8715980C933FC6ACEF35 ** get_address_of_OnPageGameLookAtRaycast_4() { return &___OnPageGameLookAtRaycast_4; }
	inline void set_OnPageGameLookAtRaycast_4(OnPageGameLookAtRaycastDelegate_t0A2901F0757A4782C71B8715980C933FC6ACEF35 * value)
	{
		___OnPageGameLookAtRaycast_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPageGameLookAtRaycast_4), (void*)value);
	}

	inline static int32_t get_offset_of_OnPageGameTapRaycast_5() { return static_cast<int32_t>(offsetof(GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields, ___OnPageGameTapRaycast_5)); }
	inline OnPageGameTouchRaycastDelegate_t79ACF0C496B3D911F73287E3821D3D9DB70F9387 * get_OnPageGameTapRaycast_5() const { return ___OnPageGameTapRaycast_5; }
	inline OnPageGameTouchRaycastDelegate_t79ACF0C496B3D911F73287E3821D3D9DB70F9387 ** get_address_of_OnPageGameTapRaycast_5() { return &___OnPageGameTapRaycast_5; }
	inline void set_OnPageGameTapRaycast_5(OnPageGameTouchRaycastDelegate_t79ACF0C496B3D911F73287E3821D3D9DB70F9387 * value)
	{
		___OnPageGameTapRaycast_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPageGameTapRaycast_5), (void*)value);
	}

	inline static int32_t get_offset_of_OnSplashWorldEnter_6() { return static_cast<int32_t>(offsetof(GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields, ___OnSplashWorldEnter_6)); }
	inline OnSplashWorldEnterDelegate_tE10056481597DD80FAD0E3FF0E0E174EAEE5D7CF * get_OnSplashWorldEnter_6() const { return ___OnSplashWorldEnter_6; }
	inline OnSplashWorldEnterDelegate_tE10056481597DD80FAD0E3FF0E0E174EAEE5D7CF ** get_address_of_OnSplashWorldEnter_6() { return &___OnSplashWorldEnter_6; }
	inline void set_OnSplashWorldEnter_6(OnSplashWorldEnterDelegate_tE10056481597DD80FAD0E3FF0E0E174EAEE5D7CF * value)
	{
		___OnSplashWorldEnter_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnSplashWorldEnter_6), (void*)value);
	}

	inline static int32_t get_offset_of_OnSplashWorldExit_7() { return static_cast<int32_t>(offsetof(GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields, ___OnSplashWorldExit_7)); }
	inline OnSplashWorldExitDelegate_tD93D4940EAF1FF4E23A372D4D88439D4273F4557 * get_OnSplashWorldExit_7() const { return ___OnSplashWorldExit_7; }
	inline OnSplashWorldExitDelegate_tD93D4940EAF1FF4E23A372D4D88439D4273F4557 ** get_address_of_OnSplashWorldExit_7() { return &___OnSplashWorldExit_7; }
	inline void set_OnSplashWorldExit_7(OnSplashWorldExitDelegate_tD93D4940EAF1FF4E23A372D4D88439D4273F4557 * value)
	{
		___OnSplashWorldExit_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnSplashWorldExit_7), (void*)value);
	}

	inline static int32_t get_offset_of_OnSplashWorldLookAtRaycast_8() { return static_cast<int32_t>(offsetof(GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields, ___OnSplashWorldLookAtRaycast_8)); }
	inline OnSplashWorldLookAtRaycastDelegate_tA4690438A19C04948CA6609796DCAC2EE70BF3B0 * get_OnSplashWorldLookAtRaycast_8() const { return ___OnSplashWorldLookAtRaycast_8; }
	inline OnSplashWorldLookAtRaycastDelegate_tA4690438A19C04948CA6609796DCAC2EE70BF3B0 ** get_address_of_OnSplashWorldLookAtRaycast_8() { return &___OnSplashWorldLookAtRaycast_8; }
	inline void set_OnSplashWorldLookAtRaycast_8(OnSplashWorldLookAtRaycastDelegate_tA4690438A19C04948CA6609796DCAC2EE70BF3B0 * value)
	{
		___OnSplashWorldLookAtRaycast_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnSplashWorldLookAtRaycast_8), (void*)value);
	}

	inline static int32_t get_offset_of_OnSplashWorldTapRaycast_9() { return static_cast<int32_t>(offsetof(GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields, ___OnSplashWorldTapRaycast_9)); }
	inline OnSplashWorldTouchRaycastDelegate_t52F1E630AFC268B9D26C5738F63805FF40B0D027 * get_OnSplashWorldTapRaycast_9() const { return ___OnSplashWorldTapRaycast_9; }
	inline OnSplashWorldTouchRaycastDelegate_t52F1E630AFC268B9D26C5738F63805FF40B0D027 ** get_address_of_OnSplashWorldTapRaycast_9() { return &___OnSplashWorldTapRaycast_9; }
	inline void set_OnSplashWorldTapRaycast_9(OnSplashWorldTouchRaycastDelegate_t52F1E630AFC268B9D26C5738F63805FF40B0D027 * value)
	{
		___OnSplashWorldTapRaycast_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnSplashWorldTapRaycast_9), (void*)value);
	}

	inline static int32_t get_offset_of_OnTapOn_10() { return static_cast<int32_t>(offsetof(GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields, ___OnTapOn_10)); }
	inline OnTapOnDelegate_tB00348CD112FB24724A207D1A265741539F5C9A1 * get_OnTapOn_10() const { return ___OnTapOn_10; }
	inline OnTapOnDelegate_tB00348CD112FB24724A207D1A265741539F5C9A1 ** get_address_of_OnTapOn_10() { return &___OnTapOn_10; }
	inline void set_OnTapOn_10(OnTapOnDelegate_tB00348CD112FB24724A207D1A265741539F5C9A1 * value)
	{
		___OnTapOn_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnTapOn_10), (void*)value);
	}

	inline static int32_t get_offset_of_OnTapMove_11() { return static_cast<int32_t>(offsetof(GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields, ___OnTapMove_11)); }
	inline OnTapMoveDelegate_t723F9E3AED1681A747EFCEE8EF576177F038188D * get_OnTapMove_11() const { return ___OnTapMove_11; }
	inline OnTapMoveDelegate_t723F9E3AED1681A747EFCEE8EF576177F038188D ** get_address_of_OnTapMove_11() { return &___OnTapMove_11; }
	inline void set_OnTapMove_11(OnTapMoveDelegate_t723F9E3AED1681A747EFCEE8EF576177F038188D * value)
	{
		___OnTapMove_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnTapMove_11), (void*)value);
	}

	inline static int32_t get_offset_of_OnTapOff_12() { return static_cast<int32_t>(offsetof(GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields, ___OnTapOff_12)); }
	inline OnTapOffDelegate_t40D5D499048112C9CCF95E6FF8CB13323679CF81 * get_OnTapOff_12() const { return ___OnTapOff_12; }
	inline OnTapOffDelegate_t40D5D499048112C9CCF95E6FF8CB13323679CF81 ** get_address_of_OnTapOff_12() { return &___OnTapOff_12; }
	inline void set_OnTapOff_12(OnTapOffDelegate_t40D5D499048112C9CCF95E6FF8CB13323679CF81 * value)
	{
		___OnTapOff_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnTapOff_12), (void*)value);
	}

	inline static int32_t get_offset_of_OnFirstPageTracked_13() { return static_cast<int32_t>(offsetof(GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields, ___OnFirstPageTracked_13)); }
	inline OnFirstPageTrackedDelegate_tB10EA0A4FC2EDB8A727310557768DA2D7E3E942B * get_OnFirstPageTracked_13() const { return ___OnFirstPageTracked_13; }
	inline OnFirstPageTrackedDelegate_tB10EA0A4FC2EDB8A727310557768DA2D7E3E942B ** get_address_of_OnFirstPageTracked_13() { return &___OnFirstPageTracked_13; }
	inline void set_OnFirstPageTracked_13(OnFirstPageTrackedDelegate_tB10EA0A4FC2EDB8A727310557768DA2D7E3E942B * value)
	{
		___OnFirstPageTracked_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnFirstPageTracked_13), (void*)value);
	}

	inline static int32_t get_offset_of_OnStarfishTapped_14() { return static_cast<int32_t>(offsetof(GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields, ___OnStarfishTapped_14)); }
	inline OnStarfishTappedDelegate_tDEDDAA2572797A17474E1AF26CF4DD06B0C60317 * get_OnStarfishTapped_14() const { return ___OnStarfishTapped_14; }
	inline OnStarfishTappedDelegate_tDEDDAA2572797A17474E1AF26CF4DD06B0C60317 ** get_address_of_OnStarfishTapped_14() { return &___OnStarfishTapped_14; }
	inline void set_OnStarfishTapped_14(OnStarfishTappedDelegate_tDEDDAA2572797A17474E1AF26CF4DD06B0C60317 * value)
	{
		___OnStarfishTapped_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnStarfishTapped_14), (void*)value);
	}

	inline static int32_t get_offset_of_OnFlickableTapped_15() { return static_cast<int32_t>(offsetof(GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields, ___OnFlickableTapped_15)); }
	inline OnFlickableTappedDelegate_t1C5F3C2CB74A16B63E9997D152CDDFA196465908 * get_OnFlickableTapped_15() const { return ___OnFlickableTapped_15; }
	inline OnFlickableTappedDelegate_t1C5F3C2CB74A16B63E9997D152CDDFA196465908 ** get_address_of_OnFlickableTapped_15() { return &___OnFlickableTapped_15; }
	inline void set_OnFlickableTapped_15(OnFlickableTappedDelegate_t1C5F3C2CB74A16B63E9997D152CDDFA196465908 * value)
	{
		___OnFlickableTapped_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnFlickableTapped_15), (void*)value);
	}

	inline static int32_t get_offset_of_OnFlickableDragged_16() { return static_cast<int32_t>(offsetof(GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields, ___OnFlickableDragged_16)); }
	inline OnFlickableDraggedDelegate_tBAF20D4FC47CDFC5EEFDF658277B86EBBFD03E1B * get_OnFlickableDragged_16() const { return ___OnFlickableDragged_16; }
	inline OnFlickableDraggedDelegate_tBAF20D4FC47CDFC5EEFDF658277B86EBBFD03E1B ** get_address_of_OnFlickableDragged_16() { return &___OnFlickableDragged_16; }
	inline void set_OnFlickableDragged_16(OnFlickableDraggedDelegate_tBAF20D4FC47CDFC5EEFDF658277B86EBBFD03E1B * value)
	{
		___OnFlickableDragged_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnFlickableDragged_16), (void*)value);
	}

	inline static int32_t get_offset_of_OnFlickableThrown_17() { return static_cast<int32_t>(offsetof(GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields, ___OnFlickableThrown_17)); }
	inline OnFlickableThrownDelegate_tE88AB9432A7582F497E8E34507D71BF606B1C272 * get_OnFlickableThrown_17() const { return ___OnFlickableThrown_17; }
	inline OnFlickableThrownDelegate_tE88AB9432A7582F497E8E34507D71BF606B1C272 ** get_address_of_OnFlickableThrown_17() { return &___OnFlickableThrown_17; }
	inline void set_OnFlickableThrown_17(OnFlickableThrownDelegate_tE88AB9432A7582F497E8E34507D71BF606B1C272 * value)
	{
		___OnFlickableThrown_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnFlickableThrown_17), (void*)value);
	}

	inline static int32_t get_offset_of_OnFadeToBlack_18() { return static_cast<int32_t>(offsetof(GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields, ___OnFadeToBlack_18)); }
	inline OnFadeToBlackDelegate_tD0C3CF3048E7FD45B6BA5332BDE5BB05FD59FE3F * get_OnFadeToBlack_18() const { return ___OnFadeToBlack_18; }
	inline OnFadeToBlackDelegate_tD0C3CF3048E7FD45B6BA5332BDE5BB05FD59FE3F ** get_address_of_OnFadeToBlack_18() { return &___OnFadeToBlack_18; }
	inline void set_OnFadeToBlack_18(OnFadeToBlackDelegate_tD0C3CF3048E7FD45B6BA5332BDE5BB05FD59FE3F * value)
	{
		___OnFadeToBlack_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnFadeToBlack_18), (void*)value);
	}

	inline static int32_t get_offset_of_OnCoinsCollected_19() { return static_cast<int32_t>(offsetof(GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields, ___OnCoinsCollected_19)); }
	inline OnCoinsCollectedDelegate_t73E03EBD90D903F0CEEEB4C5E86860BEF7290B94 * get_OnCoinsCollected_19() const { return ___OnCoinsCollected_19; }
	inline OnCoinsCollectedDelegate_t73E03EBD90D903F0CEEEB4C5E86860BEF7290B94 ** get_address_of_OnCoinsCollected_19() { return &___OnCoinsCollected_19; }
	inline void set_OnCoinsCollected_19(OnCoinsCollectedDelegate_t73E03EBD90D903F0CEEEB4C5E86860BEF7290B94 * value)
	{
		___OnCoinsCollected_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnCoinsCollected_19), (void*)value);
	}

	inline static int32_t get_offset_of_OnTotalCoinsUpdated_20() { return static_cast<int32_t>(offsetof(GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields, ___OnTotalCoinsUpdated_20)); }
	inline OnTotalCoinsUpdatedDelegate_tB29199907D790A984C111DF7FB8A6669C9C4C0A5 * get_OnTotalCoinsUpdated_20() const { return ___OnTotalCoinsUpdated_20; }
	inline OnTotalCoinsUpdatedDelegate_tB29199907D790A984C111DF7FB8A6669C9C4C0A5 ** get_address_of_OnTotalCoinsUpdated_20() { return &___OnTotalCoinsUpdated_20; }
	inline void set_OnTotalCoinsUpdated_20(OnTotalCoinsUpdatedDelegate_tB29199907D790A984C111DF7FB8A6669C9C4C0A5 * value)
	{
		___OnTotalCoinsUpdated_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnTotalCoinsUpdated_20), (void*)value);
	}
};


// LTBezierPath
struct LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] LTBezierPath::pts
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___pts_0;
	// System.Single LTBezierPath::length
	float ___length_1;
	// System.Boolean LTBezierPath::orientToPath
	bool ___orientToPath_2;
	// System.Boolean LTBezierPath::orientToPath2d
	bool ___orientToPath2d_3;
	// LTBezier[] LTBezierPath::beziers
	LTBezierU5BU5D_t4DDB3352316B2B476AEEE5AB4F760513856FC153* ___beziers_4;
	// System.Single[] LTBezierPath::lengthRatio
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___lengthRatio_5;
	// System.Int32 LTBezierPath::currentBezier
	int32_t ___currentBezier_6;
	// System.Int32 LTBezierPath::previousBezier
	int32_t ___previousBezier_7;

public:
	inline static int32_t get_offset_of_pts_0() { return static_cast<int32_t>(offsetof(LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48, ___pts_0)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_pts_0() const { return ___pts_0; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_pts_0() { return &___pts_0; }
	inline void set_pts_0(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___pts_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pts_0), (void*)value);
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48, ___length_1)); }
	inline float get_length_1() const { return ___length_1; }
	inline float* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(float value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_orientToPath_2() { return static_cast<int32_t>(offsetof(LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48, ___orientToPath_2)); }
	inline bool get_orientToPath_2() const { return ___orientToPath_2; }
	inline bool* get_address_of_orientToPath_2() { return &___orientToPath_2; }
	inline void set_orientToPath_2(bool value)
	{
		___orientToPath_2 = value;
	}

	inline static int32_t get_offset_of_orientToPath2d_3() { return static_cast<int32_t>(offsetof(LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48, ___orientToPath2d_3)); }
	inline bool get_orientToPath2d_3() const { return ___orientToPath2d_3; }
	inline bool* get_address_of_orientToPath2d_3() { return &___orientToPath2d_3; }
	inline void set_orientToPath2d_3(bool value)
	{
		___orientToPath2d_3 = value;
	}

	inline static int32_t get_offset_of_beziers_4() { return static_cast<int32_t>(offsetof(LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48, ___beziers_4)); }
	inline LTBezierU5BU5D_t4DDB3352316B2B476AEEE5AB4F760513856FC153* get_beziers_4() const { return ___beziers_4; }
	inline LTBezierU5BU5D_t4DDB3352316B2B476AEEE5AB4F760513856FC153** get_address_of_beziers_4() { return &___beziers_4; }
	inline void set_beziers_4(LTBezierU5BU5D_t4DDB3352316B2B476AEEE5AB4F760513856FC153* value)
	{
		___beziers_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___beziers_4), (void*)value);
	}

	inline static int32_t get_offset_of_lengthRatio_5() { return static_cast<int32_t>(offsetof(LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48, ___lengthRatio_5)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_lengthRatio_5() const { return ___lengthRatio_5; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_lengthRatio_5() { return &___lengthRatio_5; }
	inline void set_lengthRatio_5(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___lengthRatio_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lengthRatio_5), (void*)value);
	}

	inline static int32_t get_offset_of_currentBezier_6() { return static_cast<int32_t>(offsetof(LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48, ___currentBezier_6)); }
	inline int32_t get_currentBezier_6() const { return ___currentBezier_6; }
	inline int32_t* get_address_of_currentBezier_6() { return &___currentBezier_6; }
	inline void set_currentBezier_6(int32_t value)
	{
		___currentBezier_6 = value;
	}

	inline static int32_t get_offset_of_previousBezier_7() { return static_cast<int32_t>(offsetof(LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48, ___previousBezier_7)); }
	inline int32_t get_previousBezier_7() const { return ___previousBezier_7; }
	inline int32_t* get_address_of_previousBezier_7() { return &___previousBezier_7; }
	inline void set_previousBezier_7(int32_t value)
	{
		___previousBezier_7 = value;
	}
};


// LTEvent
struct LTEvent_tFD435CD71D73BB5C8DBB11E20B4E3EF3ECC428EA  : public RuntimeObject
{
public:
	// System.Int32 LTEvent::id
	int32_t ___id_0;
	// System.Object LTEvent::data
	RuntimeObject * ___data_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(LTEvent_tFD435CD71D73BB5C8DBB11E20B4E3EF3ECC428EA, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(LTEvent_tFD435CD71D73BB5C8DBB11E20B4E3EF3ECC428EA, ___data_1)); }
	inline RuntimeObject * get_data_1() const { return ___data_1; }
	inline RuntimeObject ** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(RuntimeObject * value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_1), (void*)value);
	}
};


// LTSeq
struct LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222  : public RuntimeObject
{
public:
	// LTSeq LTSeq::previous
	LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222 * ___previous_0;
	// LTSeq LTSeq::current
	LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222 * ___current_1;
	// LTDescr LTSeq::tween
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * ___tween_2;
	// System.Single LTSeq::totalDelay
	float ___totalDelay_3;
	// System.Single LTSeq::timeScale
	float ___timeScale_4;
	// System.Int32 LTSeq::debugIter
	int32_t ___debugIter_5;
	// System.UInt32 LTSeq::counter
	uint32_t ___counter_6;
	// System.Boolean LTSeq::toggle
	bool ___toggle_7;
	// System.UInt32 LTSeq::_id
	uint32_t ____id_8;

public:
	inline static int32_t get_offset_of_previous_0() { return static_cast<int32_t>(offsetof(LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222, ___previous_0)); }
	inline LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222 * get_previous_0() const { return ___previous_0; }
	inline LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222 ** get_address_of_previous_0() { return &___previous_0; }
	inline void set_previous_0(LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222 * value)
	{
		___previous_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___previous_0), (void*)value);
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222, ___current_1)); }
	inline LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222 * get_current_1() const { return ___current_1; }
	inline LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222 ** get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222 * value)
	{
		___current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_1), (void*)value);
	}

	inline static int32_t get_offset_of_tween_2() { return static_cast<int32_t>(offsetof(LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222, ___tween_2)); }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * get_tween_2() const { return ___tween_2; }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F ** get_address_of_tween_2() { return &___tween_2; }
	inline void set_tween_2(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * value)
	{
		___tween_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tween_2), (void*)value);
	}

	inline static int32_t get_offset_of_totalDelay_3() { return static_cast<int32_t>(offsetof(LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222, ___totalDelay_3)); }
	inline float get_totalDelay_3() const { return ___totalDelay_3; }
	inline float* get_address_of_totalDelay_3() { return &___totalDelay_3; }
	inline void set_totalDelay_3(float value)
	{
		___totalDelay_3 = value;
	}

	inline static int32_t get_offset_of_timeScale_4() { return static_cast<int32_t>(offsetof(LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222, ___timeScale_4)); }
	inline float get_timeScale_4() const { return ___timeScale_4; }
	inline float* get_address_of_timeScale_4() { return &___timeScale_4; }
	inline void set_timeScale_4(float value)
	{
		___timeScale_4 = value;
	}

	inline static int32_t get_offset_of_debugIter_5() { return static_cast<int32_t>(offsetof(LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222, ___debugIter_5)); }
	inline int32_t get_debugIter_5() const { return ___debugIter_5; }
	inline int32_t* get_address_of_debugIter_5() { return &___debugIter_5; }
	inline void set_debugIter_5(int32_t value)
	{
		___debugIter_5 = value;
	}

	inline static int32_t get_offset_of_counter_6() { return static_cast<int32_t>(offsetof(LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222, ___counter_6)); }
	inline uint32_t get_counter_6() const { return ___counter_6; }
	inline uint32_t* get_address_of_counter_6() { return &___counter_6; }
	inline void set_counter_6(uint32_t value)
	{
		___counter_6 = value;
	}

	inline static int32_t get_offset_of_toggle_7() { return static_cast<int32_t>(offsetof(LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222, ___toggle_7)); }
	inline bool get_toggle_7() const { return ___toggle_7; }
	inline bool* get_address_of_toggle_7() { return &___toggle_7; }
	inline void set_toggle_7(bool value)
	{
		___toggle_7 = value;
	}

	inline static int32_t get_offset_of__id_8() { return static_cast<int32_t>(offsetof(LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222, ____id_8)); }
	inline uint32_t get__id_8() const { return ____id_8; }
	inline uint32_t* get_address_of__id_8() { return &____id_8; }
	inline void set__id_8(uint32_t value)
	{
		____id_8 = value;
	}
};


// LTSpline
struct LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701  : public RuntimeObject
{
public:
	// System.Single LTSpline::distance
	float ___distance_2;
	// System.Boolean LTSpline::constantSpeed
	bool ___constantSpeed_3;
	// UnityEngine.Vector3[] LTSpline::pts
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___pts_4;
	// UnityEngine.Vector3[] LTSpline::ptsAdj
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___ptsAdj_5;
	// System.Int32 LTSpline::ptsAdjLength
	int32_t ___ptsAdjLength_6;
	// System.Boolean LTSpline::orientToPath
	bool ___orientToPath_7;
	// System.Boolean LTSpline::orientToPath2d
	bool ___orientToPath2d_8;
	// System.Int32 LTSpline::numSections
	int32_t ___numSections_9;
	// System.Int32 LTSpline::currPt
	int32_t ___currPt_10;

public:
	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_constantSpeed_3() { return static_cast<int32_t>(offsetof(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701, ___constantSpeed_3)); }
	inline bool get_constantSpeed_3() const { return ___constantSpeed_3; }
	inline bool* get_address_of_constantSpeed_3() { return &___constantSpeed_3; }
	inline void set_constantSpeed_3(bool value)
	{
		___constantSpeed_3 = value;
	}

	inline static int32_t get_offset_of_pts_4() { return static_cast<int32_t>(offsetof(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701, ___pts_4)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_pts_4() const { return ___pts_4; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_pts_4() { return &___pts_4; }
	inline void set_pts_4(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___pts_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pts_4), (void*)value);
	}

	inline static int32_t get_offset_of_ptsAdj_5() { return static_cast<int32_t>(offsetof(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701, ___ptsAdj_5)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_ptsAdj_5() const { return ___ptsAdj_5; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_ptsAdj_5() { return &___ptsAdj_5; }
	inline void set_ptsAdj_5(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___ptsAdj_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ptsAdj_5), (void*)value);
	}

	inline static int32_t get_offset_of_ptsAdjLength_6() { return static_cast<int32_t>(offsetof(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701, ___ptsAdjLength_6)); }
	inline int32_t get_ptsAdjLength_6() const { return ___ptsAdjLength_6; }
	inline int32_t* get_address_of_ptsAdjLength_6() { return &___ptsAdjLength_6; }
	inline void set_ptsAdjLength_6(int32_t value)
	{
		___ptsAdjLength_6 = value;
	}

	inline static int32_t get_offset_of_orientToPath_7() { return static_cast<int32_t>(offsetof(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701, ___orientToPath_7)); }
	inline bool get_orientToPath_7() const { return ___orientToPath_7; }
	inline bool* get_address_of_orientToPath_7() { return &___orientToPath_7; }
	inline void set_orientToPath_7(bool value)
	{
		___orientToPath_7 = value;
	}

	inline static int32_t get_offset_of_orientToPath2d_8() { return static_cast<int32_t>(offsetof(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701, ___orientToPath2d_8)); }
	inline bool get_orientToPath2d_8() const { return ___orientToPath2d_8; }
	inline bool* get_address_of_orientToPath2d_8() { return &___orientToPath2d_8; }
	inline void set_orientToPath2d_8(bool value)
	{
		___orientToPath2d_8 = value;
	}

	inline static int32_t get_offset_of_numSections_9() { return static_cast<int32_t>(offsetof(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701, ___numSections_9)); }
	inline int32_t get_numSections_9() const { return ___numSections_9; }
	inline int32_t* get_address_of_numSections_9() { return &___numSections_9; }
	inline void set_numSections_9(int32_t value)
	{
		___numSections_9 = value;
	}

	inline static int32_t get_offset_of_currPt_10() { return static_cast<int32_t>(offsetof(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701, ___currPt_10)); }
	inline int32_t get_currPt_10() const { return ___currPt_10; }
	inline int32_t* get_address_of_currPt_10() { return &___currPt_10; }
	inline void set_currPt_10(int32_t value)
	{
		___currPt_10 = value;
	}
};

struct LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701_StaticFields
{
public:
	// System.Int32 LTSpline::DISTANCE_COUNT
	int32_t ___DISTANCE_COUNT_0;
	// System.Int32 LTSpline::SUBLINE_COUNT
	int32_t ___SUBLINE_COUNT_1;

public:
	inline static int32_t get_offset_of_DISTANCE_COUNT_0() { return static_cast<int32_t>(offsetof(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701_StaticFields, ___DISTANCE_COUNT_0)); }
	inline int32_t get_DISTANCE_COUNT_0() const { return ___DISTANCE_COUNT_0; }
	inline int32_t* get_address_of_DISTANCE_COUNT_0() { return &___DISTANCE_COUNT_0; }
	inline void set_DISTANCE_COUNT_0(int32_t value)
	{
		___DISTANCE_COUNT_0 = value;
	}

	inline static int32_t get_offset_of_SUBLINE_COUNT_1() { return static_cast<int32_t>(offsetof(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701_StaticFields, ___SUBLINE_COUNT_1)); }
	inline int32_t get_SUBLINE_COUNT_1() const { return ___SUBLINE_COUNT_1; }
	inline int32_t* get_address_of_SUBLINE_COUNT_1() { return &___SUBLINE_COUNT_1; }
	inline void set_SUBLINE_COUNT_1(int32_t value)
	{
		___SUBLINE_COUNT_1 = value;
	}
};


// LTUtility
struct LTUtility_tD1A4A8590D2B9A3EB56F29821CACD7B41905503B  : public RuntimeObject
{
public:

public:
};


// LeanAudio
struct LeanAudio_t880217F92A02C8780710C41135754D5BF8BD14FE  : public RuntimeObject
{
public:

public:
};

struct LeanAudio_t880217F92A02C8780710C41135754D5BF8BD14FE_StaticFields
{
public:
	// System.Single LeanAudio::MIN_FREQEUNCY_PERIOD
	float ___MIN_FREQEUNCY_PERIOD_0;
	// System.Int32 LeanAudio::PROCESSING_ITERATIONS_MAX
	int32_t ___PROCESSING_ITERATIONS_MAX_1;
	// System.Single[] LeanAudio::generatedWaveDistances
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___generatedWaveDistances_2;
	// System.Int32 LeanAudio::generatedWaveDistancesCount
	int32_t ___generatedWaveDistancesCount_3;
	// System.Single[] LeanAudio::longList
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___longList_4;

public:
	inline static int32_t get_offset_of_MIN_FREQEUNCY_PERIOD_0() { return static_cast<int32_t>(offsetof(LeanAudio_t880217F92A02C8780710C41135754D5BF8BD14FE_StaticFields, ___MIN_FREQEUNCY_PERIOD_0)); }
	inline float get_MIN_FREQEUNCY_PERIOD_0() const { return ___MIN_FREQEUNCY_PERIOD_0; }
	inline float* get_address_of_MIN_FREQEUNCY_PERIOD_0() { return &___MIN_FREQEUNCY_PERIOD_0; }
	inline void set_MIN_FREQEUNCY_PERIOD_0(float value)
	{
		___MIN_FREQEUNCY_PERIOD_0 = value;
	}

	inline static int32_t get_offset_of_PROCESSING_ITERATIONS_MAX_1() { return static_cast<int32_t>(offsetof(LeanAudio_t880217F92A02C8780710C41135754D5BF8BD14FE_StaticFields, ___PROCESSING_ITERATIONS_MAX_1)); }
	inline int32_t get_PROCESSING_ITERATIONS_MAX_1() const { return ___PROCESSING_ITERATIONS_MAX_1; }
	inline int32_t* get_address_of_PROCESSING_ITERATIONS_MAX_1() { return &___PROCESSING_ITERATIONS_MAX_1; }
	inline void set_PROCESSING_ITERATIONS_MAX_1(int32_t value)
	{
		___PROCESSING_ITERATIONS_MAX_1 = value;
	}

	inline static int32_t get_offset_of_generatedWaveDistances_2() { return static_cast<int32_t>(offsetof(LeanAudio_t880217F92A02C8780710C41135754D5BF8BD14FE_StaticFields, ___generatedWaveDistances_2)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_generatedWaveDistances_2() const { return ___generatedWaveDistances_2; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_generatedWaveDistances_2() { return &___generatedWaveDistances_2; }
	inline void set_generatedWaveDistances_2(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___generatedWaveDistances_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___generatedWaveDistances_2), (void*)value);
	}

	inline static int32_t get_offset_of_generatedWaveDistancesCount_3() { return static_cast<int32_t>(offsetof(LeanAudio_t880217F92A02C8780710C41135754D5BF8BD14FE_StaticFields, ___generatedWaveDistancesCount_3)); }
	inline int32_t get_generatedWaveDistancesCount_3() const { return ___generatedWaveDistancesCount_3; }
	inline int32_t* get_address_of_generatedWaveDistancesCount_3() { return &___generatedWaveDistancesCount_3; }
	inline void set_generatedWaveDistancesCount_3(int32_t value)
	{
		___generatedWaveDistancesCount_3 = value;
	}

	inline static int32_t get_offset_of_longList_4() { return static_cast<int32_t>(offsetof(LeanAudio_t880217F92A02C8780710C41135754D5BF8BD14FE_StaticFields, ___longList_4)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_longList_4() const { return ___longList_4; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_longList_4() { return &___longList_4; }
	inline void set_longList_4(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___longList_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___longList_4), (void*)value);
	}
};


// LeanAudioStream
struct LeanAudioStream_tDA1B6363B48B65291F4C928A5A706C159FD13712  : public RuntimeObject
{
public:
	// System.Int32 LeanAudioStream::position
	int32_t ___position_0;
	// UnityEngine.AudioClip LeanAudioStream::audioClip
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___audioClip_1;
	// System.Single[] LeanAudioStream::audioArr
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___audioArr_2;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(LeanAudioStream_tDA1B6363B48B65291F4C928A5A706C159FD13712, ___position_0)); }
	inline int32_t get_position_0() const { return ___position_0; }
	inline int32_t* get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(int32_t value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_audioClip_1() { return static_cast<int32_t>(offsetof(LeanAudioStream_tDA1B6363B48B65291F4C928A5A706C159FD13712, ___audioClip_1)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get_audioClip_1() const { return ___audioClip_1; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of_audioClip_1() { return &___audioClip_1; }
	inline void set_audioClip_1(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		___audioClip_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audioClip_1), (void*)value);
	}

	inline static int32_t get_offset_of_audioArr_2() { return static_cast<int32_t>(offsetof(LeanAudioStream_tDA1B6363B48B65291F4C928A5A706C159FD13712, ___audioArr_2)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_audioArr_2() const { return ___audioArr_2; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_audioArr_2() { return &___audioArr_2; }
	inline void set_audioArr_2(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___audioArr_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audioArr_2), (void*)value);
	}
};


// DentedPixel.LeanDummy
struct LeanDummy_tB8BEBC768133908A0553CDD8C0E763705722BC5E  : public RuntimeObject
{
public:

public:
};


// LeanSmooth
struct LeanSmooth_t4E1B1AAA58DAE2327DB5E72D257C5E325E252A09  : public RuntimeObject
{
public:

public:
};


// LeanTest
struct LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F  : public RuntimeObject
{
public:

public:
};

struct LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields
{
public:
	// System.Int32 LeanTest::expected
	int32_t ___expected_0;
	// System.Int32 LeanTest::tests
	int32_t ___tests_1;
	// System.Int32 LeanTest::passes
	int32_t ___passes_2;
	// System.Single LeanTest::timeout
	float ___timeout_3;
	// System.Boolean LeanTest::timeoutStarted
	bool ___timeoutStarted_4;
	// System.Boolean LeanTest::testsFinished
	bool ___testsFinished_5;

public:
	inline static int32_t get_offset_of_expected_0() { return static_cast<int32_t>(offsetof(LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields, ___expected_0)); }
	inline int32_t get_expected_0() const { return ___expected_0; }
	inline int32_t* get_address_of_expected_0() { return &___expected_0; }
	inline void set_expected_0(int32_t value)
	{
		___expected_0 = value;
	}

	inline static int32_t get_offset_of_tests_1() { return static_cast<int32_t>(offsetof(LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields, ___tests_1)); }
	inline int32_t get_tests_1() const { return ___tests_1; }
	inline int32_t* get_address_of_tests_1() { return &___tests_1; }
	inline void set_tests_1(int32_t value)
	{
		___tests_1 = value;
	}

	inline static int32_t get_offset_of_passes_2() { return static_cast<int32_t>(offsetof(LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields, ___passes_2)); }
	inline int32_t get_passes_2() const { return ___passes_2; }
	inline int32_t* get_address_of_passes_2() { return &___passes_2; }
	inline void set_passes_2(int32_t value)
	{
		___passes_2 = value;
	}

	inline static int32_t get_offset_of_timeout_3() { return static_cast<int32_t>(offsetof(LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields, ___timeout_3)); }
	inline float get_timeout_3() const { return ___timeout_3; }
	inline float* get_address_of_timeout_3() { return &___timeout_3; }
	inline void set_timeout_3(float value)
	{
		___timeout_3 = value;
	}

	inline static int32_t get_offset_of_timeoutStarted_4() { return static_cast<int32_t>(offsetof(LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields, ___timeoutStarted_4)); }
	inline bool get_timeoutStarted_4() const { return ___timeoutStarted_4; }
	inline bool* get_address_of_timeoutStarted_4() { return &___timeoutStarted_4; }
	inline void set_timeoutStarted_4(bool value)
	{
		___timeoutStarted_4 = value;
	}

	inline static int32_t get_offset_of_testsFinished_5() { return static_cast<int32_t>(offsetof(LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields, ___testsFinished_5)); }
	inline bool get_testsFinished_5() const { return ___testsFinished_5; }
	inline bool* get_address_of_testsFinished_5() { return &___testsFinished_5; }
	inline void set_testsFinished_5(bool value)
	{
		___testsFinished_5 = value;
	}
};


// LeanTweenExt
struct LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414  : public RuntimeObject
{
public:

public:
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// CoinSpin/<>c
struct U3CU3Ec_t5B5DA66907FD15891D64D11538E77E3537A4AB4C  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t5B5DA66907FD15891D64D11538E77E3537A4AB4C_StaticFields
{
public:
	// CoinSpin/<>c CoinSpin/<>c::<>9
	U3CU3Ec_t5B5DA66907FD15891D64D11538E77E3537A4AB4C * ___U3CU3E9_0;
	// System.Action CoinSpin/<>c::<>9__4_0
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3CU3E9__4_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5B5DA66907FD15891D64D11538E77E3537A4AB4C_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t5B5DA66907FD15891D64D11538E77E3537A4AB4C * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t5B5DA66907FD15891D64D11538E77E3537A4AB4C ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t5B5DA66907FD15891D64D11538E77E3537A4AB4C * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5B5DA66907FD15891D64D11538E77E3537A4AB4C_StaticFields, ___U3CU3E9__4_0_1)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3CU3E9__4_0_1() const { return ___U3CU3E9__4_0_1; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3CU3E9__4_0_1() { return &___U3CU3E9__4_0_1; }
	inline void set_U3CU3E9__4_0_1(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3CU3E9__4_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__4_0_1), (void*)value);
	}
};


// GameScale/<ScaleObject>d__11
struct U3CScaleObjectU3Ed__11_t461C201F3015C43C04CB97A6D8D45A7B73627A6D  : public RuntimeObject
{
public:
	// System.Int32 GameScale/<ScaleObject>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object GameScale/<ScaleObject>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// GameScale GameScale/<ScaleObject>d__11::<>4__this
	GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CScaleObjectU3Ed__11_t461C201F3015C43C04CB97A6D8D45A7B73627A6D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CScaleObjectU3Ed__11_t461C201F3015C43C04CB97A6D8D45A7B73627A6D, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CScaleObjectU3Ed__11_t461C201F3015C43C04CB97A6D8D45A7B73627A6D, ___U3CU3E4__this_2)); }
	inline GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// Idle/<WaitAndActivate>d__6
struct U3CWaitAndActivateU3Ed__6_t503E05974A5610E1E9848BFB9321AE1CE472AE6D  : public RuntimeObject
{
public:
	// System.Int32 Idle/<WaitAndActivate>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Idle/<WaitAndActivate>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single Idle/<WaitAndActivate>d__6::waitTime
	float ___waitTime_2;
	// Idle Idle/<WaitAndActivate>d__6::<>4__this
	Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitAndActivateU3Ed__6_t503E05974A5610E1E9848BFB9321AE1CE472AE6D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitAndActivateU3Ed__6_t503E05974A5610E1E9848BFB9321AE1CE472AE6D, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_waitTime_2() { return static_cast<int32_t>(offsetof(U3CWaitAndActivateU3Ed__6_t503E05974A5610E1E9848BFB9321AE1CE472AE6D, ___waitTime_2)); }
	inline float get_waitTime_2() const { return ___waitTime_2; }
	inline float* get_address_of_waitTime_2() { return &___waitTime_2; }
	inline void set_waitTime_2(float value)
	{
		___waitTime_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CWaitAndActivateU3Ed__6_t503E05974A5610E1E9848BFB9321AE1CE472AE6D, ___U3CU3E4__this_3)); }
	inline Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}
};


// Idle/<WaitAndDeactivate>d__7
struct U3CWaitAndDeactivateU3Ed__7_t0B9854B91CC5946899CC4024F6D32F274A81643C  : public RuntimeObject
{
public:
	// System.Int32 Idle/<WaitAndDeactivate>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Idle/<WaitAndDeactivate>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single Idle/<WaitAndDeactivate>d__7::waitTime
	float ___waitTime_2;
	// Idle Idle/<WaitAndDeactivate>d__7::<>4__this
	Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitAndDeactivateU3Ed__7_t0B9854B91CC5946899CC4024F6D32F274A81643C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitAndDeactivateU3Ed__7_t0B9854B91CC5946899CC4024F6D32F274A81643C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_waitTime_2() { return static_cast<int32_t>(offsetof(U3CWaitAndDeactivateU3Ed__7_t0B9854B91CC5946899CC4024F6D32F274A81643C, ___waitTime_2)); }
	inline float get_waitTime_2() const { return ___waitTime_2; }
	inline float* get_address_of_waitTime_2() { return &___waitTime_2; }
	inline void set_waitTime_2(float value)
	{
		___waitTime_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CWaitAndDeactivateU3Ed__7_t0B9854B91CC5946899CC4024F6D32F274A81643C, ___U3CU3E4__this_3)); }
	inline Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}
};


// LTDescr/<>c
struct U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58_StaticFields
{
public:
	// LTDescr/<>c LTDescr/<>c::<>9
	U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58 * ___U3CU3E9_0;
	// LTDescr/ActionMethodDelegate LTDescr/<>c::<>9__113_0
	ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 * ___U3CU3E9__113_0_1;
	// LTDescr/ActionMethodDelegate LTDescr/<>c::<>9__114_0
	ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 * ___U3CU3E9__114_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__113_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58_StaticFields, ___U3CU3E9__113_0_1)); }
	inline ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 * get_U3CU3E9__113_0_1() const { return ___U3CU3E9__113_0_1; }
	inline ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 ** get_address_of_U3CU3E9__113_0_1() { return &___U3CU3E9__113_0_1; }
	inline void set_U3CU3E9__113_0_1(ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 * value)
	{
		___U3CU3E9__113_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__113_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__114_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58_StaticFields, ___U3CU3E9__114_0_2)); }
	inline ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 * get_U3CU3E9__114_0_2() const { return ___U3CU3E9__114_0_2; }
	inline ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 ** get_address_of_U3CU3E9__114_0_2() { return &___U3CU3E9__114_0_2; }
	inline void set_U3CU3E9__114_0_2(ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 * value)
	{
		___U3CU3E9__114_0_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__114_0_2), (void*)value);
	}
};


// LeanTester/<timeoutCheck>d__2
struct U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6  : public RuntimeObject
{
public:
	// System.Int32 LeanTester/<timeoutCheck>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LeanTester/<timeoutCheck>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// LeanTester LeanTester/<timeoutCheck>d__2::<>4__this
	LeanTester_t537C3FE41ED244252241E5B02F6B1BA6DA437297 * ___U3CU3E4__this_2;
	// System.Single LeanTester/<timeoutCheck>d__2::<pauseEndTime>5__2
	float ___U3CpauseEndTimeU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6, ___U3CU3E4__this_2)); }
	inline LeanTester_t537C3FE41ED244252241E5B02F6B1BA6DA437297 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LeanTester_t537C3FE41ED244252241E5B02F6B1BA6DA437297 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LeanTester_t537C3FE41ED244252241E5B02F6B1BA6DA437297 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpauseEndTimeU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6, ___U3CpauseEndTimeU3E5__2_3)); }
	inline float get_U3CpauseEndTimeU3E5__2_3() const { return ___U3CpauseEndTimeU3E5__2_3; }
	inline float* get_address_of_U3CpauseEndTimeU3E5__2_3() { return &___U3CpauseEndTimeU3E5__2_3; }
	inline void set_U3CpauseEndTimeU3E5__2_3(float value)
	{
		___U3CpauseEndTimeU3E5__2_3 = value;
	}
};


// LeanTween/<>c__DisplayClass193_0
struct U3CU3Ec__DisplayClass193_0_tAD697F408BB575C622FD5E018C1F75AB28CBAF48  : public RuntimeObject
{
public:
	// LTDescr LeanTween/<>c__DisplayClass193_0::d
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * ___d_0;
	// System.Single LeanTween/<>c__DisplayClass193_0::smoothTime
	float ___smoothTime_1;
	// System.Single LeanTween/<>c__DisplayClass193_0::maxSpeed
	float ___maxSpeed_2;

public:
	inline static int32_t get_offset_of_d_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass193_0_tAD697F408BB575C622FD5E018C1F75AB28CBAF48, ___d_0)); }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * get_d_0() const { return ___d_0; }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F ** get_address_of_d_0() { return &___d_0; }
	inline void set_d_0(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * value)
	{
		___d_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___d_0), (void*)value);
	}

	inline static int32_t get_offset_of_smoothTime_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass193_0_tAD697F408BB575C622FD5E018C1F75AB28CBAF48, ___smoothTime_1)); }
	inline float get_smoothTime_1() const { return ___smoothTime_1; }
	inline float* get_address_of_smoothTime_1() { return &___smoothTime_1; }
	inline void set_smoothTime_1(float value)
	{
		___smoothTime_1 = value;
	}

	inline static int32_t get_offset_of_maxSpeed_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass193_0_tAD697F408BB575C622FD5E018C1F75AB28CBAF48, ___maxSpeed_2)); }
	inline float get_maxSpeed_2() const { return ___maxSpeed_2; }
	inline float* get_address_of_maxSpeed_2() { return &___maxSpeed_2; }
	inline void set_maxSpeed_2(float value)
	{
		___maxSpeed_2 = value;
	}
};


// LeanTween/<>c__DisplayClass194_0
struct U3CU3Ec__DisplayClass194_0_t6FE45F50B4F93FED778E7D54F0EF582EA8252814  : public RuntimeObject
{
public:
	// LTDescr LeanTween/<>c__DisplayClass194_0::d
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * ___d_0;
	// System.Single LeanTween/<>c__DisplayClass194_0::smoothTime
	float ___smoothTime_1;
	// System.Single LeanTween/<>c__DisplayClass194_0::maxSpeed
	float ___maxSpeed_2;
	// System.Single LeanTween/<>c__DisplayClass194_0::friction
	float ___friction_3;
	// System.Single LeanTween/<>c__DisplayClass194_0::accelRate
	float ___accelRate_4;

public:
	inline static int32_t get_offset_of_d_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass194_0_t6FE45F50B4F93FED778E7D54F0EF582EA8252814, ___d_0)); }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * get_d_0() const { return ___d_0; }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F ** get_address_of_d_0() { return &___d_0; }
	inline void set_d_0(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * value)
	{
		___d_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___d_0), (void*)value);
	}

	inline static int32_t get_offset_of_smoothTime_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass194_0_t6FE45F50B4F93FED778E7D54F0EF582EA8252814, ___smoothTime_1)); }
	inline float get_smoothTime_1() const { return ___smoothTime_1; }
	inline float* get_address_of_smoothTime_1() { return &___smoothTime_1; }
	inline void set_smoothTime_1(float value)
	{
		___smoothTime_1 = value;
	}

	inline static int32_t get_offset_of_maxSpeed_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass194_0_t6FE45F50B4F93FED778E7D54F0EF582EA8252814, ___maxSpeed_2)); }
	inline float get_maxSpeed_2() const { return ___maxSpeed_2; }
	inline float* get_address_of_maxSpeed_2() { return &___maxSpeed_2; }
	inline void set_maxSpeed_2(float value)
	{
		___maxSpeed_2 = value;
	}

	inline static int32_t get_offset_of_friction_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass194_0_t6FE45F50B4F93FED778E7D54F0EF582EA8252814, ___friction_3)); }
	inline float get_friction_3() const { return ___friction_3; }
	inline float* get_address_of_friction_3() { return &___friction_3; }
	inline void set_friction_3(float value)
	{
		___friction_3 = value;
	}

	inline static int32_t get_offset_of_accelRate_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass194_0_t6FE45F50B4F93FED778E7D54F0EF582EA8252814, ___accelRate_4)); }
	inline float get_accelRate_4() const { return ___accelRate_4; }
	inline float* get_address_of_accelRate_4() { return &___accelRate_4; }
	inline void set_accelRate_4(float value)
	{
		___accelRate_4 = value;
	}
};


// LeanTween/<>c__DisplayClass195_0
struct U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4  : public RuntimeObject
{
public:
	// LTDescr LeanTween/<>c__DisplayClass195_0::d
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * ___d_0;
	// System.Single LeanTween/<>c__DisplayClass195_0::smoothTime
	float ___smoothTime_1;
	// System.Single LeanTween/<>c__DisplayClass195_0::maxSpeed
	float ___maxSpeed_2;
	// System.Single LeanTween/<>c__DisplayClass195_0::friction
	float ___friction_3;
	// System.Single LeanTween/<>c__DisplayClass195_0::accelRate
	float ___accelRate_4;
	// System.Single LeanTween/<>c__DisplayClass195_0::hitDamping
	float ___hitDamping_5;

public:
	inline static int32_t get_offset_of_d_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4, ___d_0)); }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * get_d_0() const { return ___d_0; }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F ** get_address_of_d_0() { return &___d_0; }
	inline void set_d_0(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * value)
	{
		___d_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___d_0), (void*)value);
	}

	inline static int32_t get_offset_of_smoothTime_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4, ___smoothTime_1)); }
	inline float get_smoothTime_1() const { return ___smoothTime_1; }
	inline float* get_address_of_smoothTime_1() { return &___smoothTime_1; }
	inline void set_smoothTime_1(float value)
	{
		___smoothTime_1 = value;
	}

	inline static int32_t get_offset_of_maxSpeed_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4, ___maxSpeed_2)); }
	inline float get_maxSpeed_2() const { return ___maxSpeed_2; }
	inline float* get_address_of_maxSpeed_2() { return &___maxSpeed_2; }
	inline void set_maxSpeed_2(float value)
	{
		___maxSpeed_2 = value;
	}

	inline static int32_t get_offset_of_friction_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4, ___friction_3)); }
	inline float get_friction_3() const { return ___friction_3; }
	inline float* get_address_of_friction_3() { return &___friction_3; }
	inline void set_friction_3(float value)
	{
		___friction_3 = value;
	}

	inline static int32_t get_offset_of_accelRate_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4, ___accelRate_4)); }
	inline float get_accelRate_4() const { return ___accelRate_4; }
	inline float* get_address_of_accelRate_4() { return &___accelRate_4; }
	inline void set_accelRate_4(float value)
	{
		___accelRate_4 = value;
	}

	inline static int32_t get_offset_of_hitDamping_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4, ___hitDamping_5)); }
	inline float get_hitDamping_5() const { return ___hitDamping_5; }
	inline float* get_address_of_hitDamping_5() { return &___hitDamping_5; }
	inline void set_hitDamping_5(float value)
	{
		___hitDamping_5 = value;
	}
};


// LeanTween/<>c__DisplayClass196_0
struct U3CU3Ec__DisplayClass196_0_t4FA263E9CE532093F4513F5750D54960760E3F78  : public RuntimeObject
{
public:
	// LTDescr LeanTween/<>c__DisplayClass196_0::d
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * ___d_0;
	// System.Single LeanTween/<>c__DisplayClass196_0::moveSpeed
	float ___moveSpeed_1;

public:
	inline static int32_t get_offset_of_d_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass196_0_t4FA263E9CE532093F4513F5750D54960760E3F78, ___d_0)); }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * get_d_0() const { return ___d_0; }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F ** get_address_of_d_0() { return &___d_0; }
	inline void set_d_0(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * value)
	{
		___d_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___d_0), (void*)value);
	}

	inline static int32_t get_offset_of_moveSpeed_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass196_0_t4FA263E9CE532093F4513F5750D54960760E3F78, ___moveSpeed_1)); }
	inline float get_moveSpeed_1() const { return ___moveSpeed_1; }
	inline float* get_address_of_moveSpeed_1() { return &___moveSpeed_1; }
	inline void set_moveSpeed_1(float value)
	{
		___moveSpeed_1 = value;
	}
};


// Readme/Section
struct Section_t9F25FADC74C202674AFDB11AE2AC4D332DE6CA1D  : public RuntimeObject
{
public:
	// System.String Readme/Section::heading
	String_t* ___heading_0;
	// System.String Readme/Section::text
	String_t* ___text_1;
	// System.String Readme/Section::linkText
	String_t* ___linkText_2;
	// System.String Readme/Section::url
	String_t* ___url_3;

public:
	inline static int32_t get_offset_of_heading_0() { return static_cast<int32_t>(offsetof(Section_t9F25FADC74C202674AFDB11AE2AC4D332DE6CA1D, ___heading_0)); }
	inline String_t* get_heading_0() const { return ___heading_0; }
	inline String_t** get_address_of_heading_0() { return &___heading_0; }
	inline void set_heading_0(String_t* value)
	{
		___heading_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___heading_0), (void*)value);
	}

	inline static int32_t get_offset_of_text_1() { return static_cast<int32_t>(offsetof(Section_t9F25FADC74C202674AFDB11AE2AC4D332DE6CA1D, ___text_1)); }
	inline String_t* get_text_1() const { return ___text_1; }
	inline String_t** get_address_of_text_1() { return &___text_1; }
	inline void set_text_1(String_t* value)
	{
		___text_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___text_1), (void*)value);
	}

	inline static int32_t get_offset_of_linkText_2() { return static_cast<int32_t>(offsetof(Section_t9F25FADC74C202674AFDB11AE2AC4D332DE6CA1D, ___linkText_2)); }
	inline String_t* get_linkText_2() const { return ___linkText_2; }
	inline String_t** get_address_of_linkText_2() { return &___linkText_2; }
	inline void set_linkText_2(String_t* value)
	{
		___linkText_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___linkText_2), (void*)value);
	}

	inline static int32_t get_offset_of_url_3() { return static_cast<int32_t>(offsetof(Section_t9F25FADC74C202674AFDB11AE2AC4D332DE6CA1D, ___url_3)); }
	inline String_t* get_url_3() const { return ___url_3; }
	inline String_t** get_address_of_url_3() { return &___url_3; }
	inline void set_url_3(String_t* value)
	{
		___url_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___url_3), (void*)value);
	}
};


// UnityTemplateProjects.SimpleCameraController/CameraState
struct CameraState_t3CFB67357E39129414AC5C9D628B9B47015CCE8C  : public RuntimeObject
{
public:
	// System.Single UnityTemplateProjects.SimpleCameraController/CameraState::yaw
	float ___yaw_0;
	// System.Single UnityTemplateProjects.SimpleCameraController/CameraState::pitch
	float ___pitch_1;
	// System.Single UnityTemplateProjects.SimpleCameraController/CameraState::roll
	float ___roll_2;
	// System.Single UnityTemplateProjects.SimpleCameraController/CameraState::x
	float ___x_3;
	// System.Single UnityTemplateProjects.SimpleCameraController/CameraState::y
	float ___y_4;
	// System.Single UnityTemplateProjects.SimpleCameraController/CameraState::z
	float ___z_5;

public:
	inline static int32_t get_offset_of_yaw_0() { return static_cast<int32_t>(offsetof(CameraState_t3CFB67357E39129414AC5C9D628B9B47015CCE8C, ___yaw_0)); }
	inline float get_yaw_0() const { return ___yaw_0; }
	inline float* get_address_of_yaw_0() { return &___yaw_0; }
	inline void set_yaw_0(float value)
	{
		___yaw_0 = value;
	}

	inline static int32_t get_offset_of_pitch_1() { return static_cast<int32_t>(offsetof(CameraState_t3CFB67357E39129414AC5C9D628B9B47015CCE8C, ___pitch_1)); }
	inline float get_pitch_1() const { return ___pitch_1; }
	inline float* get_address_of_pitch_1() { return &___pitch_1; }
	inline void set_pitch_1(float value)
	{
		___pitch_1 = value;
	}

	inline static int32_t get_offset_of_roll_2() { return static_cast<int32_t>(offsetof(CameraState_t3CFB67357E39129414AC5C9D628B9B47015CCE8C, ___roll_2)); }
	inline float get_roll_2() const { return ___roll_2; }
	inline float* get_address_of_roll_2() { return &___roll_2; }
	inline void set_roll_2(float value)
	{
		___roll_2 = value;
	}

	inline static int32_t get_offset_of_x_3() { return static_cast<int32_t>(offsetof(CameraState_t3CFB67357E39129414AC5C9D628B9B47015CCE8C, ___x_3)); }
	inline float get_x_3() const { return ___x_3; }
	inline float* get_address_of_x_3() { return &___x_3; }
	inline void set_x_3(float value)
	{
		___x_3 = value;
	}

	inline static int32_t get_offset_of_y_4() { return static_cast<int32_t>(offsetof(CameraState_t3CFB67357E39129414AC5C9D628B9B47015CCE8C, ___y_4)); }
	inline float get_y_4() const { return ___y_4; }
	inline float* get_address_of_y_4() { return &___y_4; }
	inline void set_y_4(float value)
	{
		___y_4 = value;
	}

	inline static int32_t get_offset_of_z_5() { return static_cast<int32_t>(offsetof(CameraState_t3CFB67357E39129414AC5C9D628B9B47015CCE8C, ___z_5)); }
	inline float get_z_5() const { return ___z_5; }
	inline float* get_address_of_z_5() { return &___z_5; }
	inline void set_z_5(float value)
	{
		___z_5 = value;
	}
};


// Spawn/<ScaleObject>d__11
struct U3CScaleObjectU3Ed__11_t4E9DC2B458A4FA829BDAE1A00561A16F1A8A40F5  : public RuntimeObject
{
public:
	// System.Int32 Spawn/<ScaleObject>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Spawn/<ScaleObject>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Spawn Spawn/<ScaleObject>d__11::<>4__this
	Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CScaleObjectU3Ed__11_t4E9DC2B458A4FA829BDAE1A00561A16F1A8A40F5, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CScaleObjectU3Ed__11_t4E9DC2B458A4FA829BDAE1A00561A16F1A8A40F5, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CScaleObjectU3Ed__11_t4E9DC2B458A4FA829BDAE1A00561A16F1A8A40F5, ___U3CU3E4__this_2)); }
	inline Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// StarfishTap/<<Tap>g__WaitAndActivate|20_2>d
struct U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_t610B7E25C1F7E862A6CB4B61E84B0023BAA56FA9  : public RuntimeObject
{
public:
	// System.Int32 StarfishTap/<<Tap>g__WaitAndActivate|20_2>d::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object StarfishTap/<<Tap>g__WaitAndActivate|20_2>d::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single StarfishTap/<<Tap>g__WaitAndActivate|20_2>d::waitTime
	float ___waitTime_2;
	// StarfishTap StarfishTap/<<Tap>g__WaitAndActivate|20_2>d::<>4__this
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_t610B7E25C1F7E862A6CB4B61E84B0023BAA56FA9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_t610B7E25C1F7E862A6CB4B61E84B0023BAA56FA9, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_waitTime_2() { return static_cast<int32_t>(offsetof(U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_t610B7E25C1F7E862A6CB4B61E84B0023BAA56FA9, ___waitTime_2)); }
	inline float get_waitTime_2() const { return ___waitTime_2; }
	inline float* get_address_of_waitTime_2() { return &___waitTime_2; }
	inline void set_waitTime_2(float value)
	{
		___waitTime_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_t610B7E25C1F7E862A6CB4B61E84B0023BAA56FA9, ___U3CU3E4__this_3)); }
	inline StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}
};


// StarfishTap/<>c
struct U3CU3Ec_tDADB0F86F4A25004FFE6463D967246A9D23C87D4  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tDADB0F86F4A25004FFE6463D967246A9D23C87D4_StaticFields
{
public:
	// StarfishTap/<>c StarfishTap/<>c::<>9
	U3CU3Ec_tDADB0F86F4A25004FFE6463D967246A9D23C87D4 * ___U3CU3E9_0;
	// System.Action StarfishTap/<>c::<>9__20_0
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3CU3E9__20_0_1;
	// System.Action StarfishTap/<>c::<>9__20_1
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3CU3E9__20_1_2;
	// System.Action StarfishTap/<>c::<>9__20_3
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3CU3E9__20_3_3;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tDADB0F86F4A25004FFE6463D967246A9D23C87D4_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tDADB0F86F4A25004FFE6463D967246A9D23C87D4 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tDADB0F86F4A25004FFE6463D967246A9D23C87D4 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tDADB0F86F4A25004FFE6463D967246A9D23C87D4 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__20_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tDADB0F86F4A25004FFE6463D967246A9D23C87D4_StaticFields, ___U3CU3E9__20_0_1)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3CU3E9__20_0_1() const { return ___U3CU3E9__20_0_1; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3CU3E9__20_0_1() { return &___U3CU3E9__20_0_1; }
	inline void set_U3CU3E9__20_0_1(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3CU3E9__20_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__20_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__20_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tDADB0F86F4A25004FFE6463D967246A9D23C87D4_StaticFields, ___U3CU3E9__20_1_2)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3CU3E9__20_1_2() const { return ___U3CU3E9__20_1_2; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3CU3E9__20_1_2() { return &___U3CU3E9__20_1_2; }
	inline void set_U3CU3E9__20_1_2(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3CU3E9__20_1_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__20_1_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__20_3_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tDADB0F86F4A25004FFE6463D967246A9D23C87D4_StaticFields, ___U3CU3E9__20_3_3)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3CU3E9__20_3_3() const { return ___U3CU3E9__20_3_3; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3CU3E9__20_3_3() { return &___U3CU3E9__20_3_3; }
	inline void set_U3CU3E9__20_3_3(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3CU3E9__20_3_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__20_3_3), (void*)value);
	}
};


// DentedPixel.LTExamples.TestingUnitTests/<>c
struct U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields
{
public:
	// DentedPixel.LTExamples.TestingUnitTests/<>c DentedPixel.LTExamples.TestingUnitTests/<>c::<>9
	U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104 * ___U3CU3E9_0;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c::<>9__22_3
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3CU3E9__22_3_1;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c::<>9__22_22
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3CU3E9__22_22_2;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c::<>9__22_7
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3CU3E9__22_7_3;
	// System.Action`1<System.Single> DentedPixel.LTExamples.TestingUnitTests/<>c::<>9__22_12
	Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * ___U3CU3E9__22_12_4;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c::<>9__22_18
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3CU3E9__22_18_5;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c::<>9__26_0
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3CU3E9__26_0_6;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_3_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields, ___U3CU3E9__22_3_1)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3CU3E9__22_3_1() const { return ___U3CU3E9__22_3_1; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3CU3E9__22_3_1() { return &___U3CU3E9__22_3_1; }
	inline void set_U3CU3E9__22_3_1(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3CU3E9__22_3_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__22_3_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_22_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields, ___U3CU3E9__22_22_2)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3CU3E9__22_22_2() const { return ___U3CU3E9__22_22_2; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3CU3E9__22_22_2() { return &___U3CU3E9__22_22_2; }
	inline void set_U3CU3E9__22_22_2(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3CU3E9__22_22_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__22_22_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_7_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields, ___U3CU3E9__22_7_3)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3CU3E9__22_7_3() const { return ___U3CU3E9__22_7_3; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3CU3E9__22_7_3() { return &___U3CU3E9__22_7_3; }
	inline void set_U3CU3E9__22_7_3(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3CU3E9__22_7_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__22_7_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_12_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields, ___U3CU3E9__22_12_4)); }
	inline Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * get_U3CU3E9__22_12_4() const { return ___U3CU3E9__22_12_4; }
	inline Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 ** get_address_of_U3CU3E9__22_12_4() { return &___U3CU3E9__22_12_4; }
	inline void set_U3CU3E9__22_12_4(Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * value)
	{
		___U3CU3E9__22_12_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__22_12_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_18_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields, ___U3CU3E9__22_18_5)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3CU3E9__22_18_5() const { return ___U3CU3E9__22_18_5; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3CU3E9__22_18_5() { return &___U3CU3E9__22_18_5; }
	inline void set_U3CU3E9__22_18_5(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3CU3E9__22_18_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__22_18_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__26_0_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields, ___U3CU3E9__26_0_6)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3CU3E9__26_0_6() const { return ___U3CU3E9__26_0_6; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3CU3E9__26_0_6() { return &___U3CU3E9__26_0_6; }
	inline void set_U3CU3E9__26_0_6(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3CU3E9__26_0_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__26_0_6), (void*)value);
	}
};


// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_1
struct U3CU3Ec__DisplayClass22_1_t041360FB309BAE73F44DAC9B4689082AC2FF5973  : public RuntimeObject
{
public:
	// System.Single DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_1::beforeX
	float ___beforeX_0;
	// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_beforeX_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_1_t041360FB309BAE73F44DAC9B4689082AC2FF5973, ___beforeX_0)); }
	inline float get_beforeX_0() const { return ___beforeX_0; }
	inline float* get_address_of_beforeX_0() { return &___beforeX_0; }
	inline void set_beforeX_0(float value)
	{
		___beforeX_0 = value;
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_1_t041360FB309BAE73F44DAC9B4689082AC2FF5973, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CSU24U3CU3E8__locals1_1), (void*)value);
	}
};


// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_2
struct U3CU3Ec__DisplayClass22_2_tF5B9A247AE90B21641EE6BA256550BF6E5CBC1E8  : public RuntimeObject
{
public:
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_2::totalTweenTypeLength
	int32_t ___totalTweenTypeLength_0;
	// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_2::CS$<>8__locals2
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F * ___CSU24U3CU3E8__locals2_1;
	// System.Action`1<System.Object> DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_2::<>9__24
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ___U3CU3E9__24_2;

public:
	inline static int32_t get_offset_of_totalTweenTypeLength_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_2_tF5B9A247AE90B21641EE6BA256550BF6E5CBC1E8, ___totalTweenTypeLength_0)); }
	inline int32_t get_totalTweenTypeLength_0() const { return ___totalTweenTypeLength_0; }
	inline int32_t* get_address_of_totalTweenTypeLength_0() { return &___totalTweenTypeLength_0; }
	inline void set_totalTweenTypeLength_0(int32_t value)
	{
		___totalTweenTypeLength_0 = value;
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals2_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_2_tF5B9A247AE90B21641EE6BA256550BF6E5CBC1E8, ___CSU24U3CU3E8__locals2_1)); }
	inline U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F * get_CSU24U3CU3E8__locals2_1() const { return ___CSU24U3CU3E8__locals2_1; }
	inline U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F ** get_address_of_CSU24U3CU3E8__locals2_1() { return &___CSU24U3CU3E8__locals2_1; }
	inline void set_CSU24U3CU3E8__locals2_1(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F * value)
	{
		___CSU24U3CU3E8__locals2_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CSU24U3CU3E8__locals2_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_2_tF5B9A247AE90B21641EE6BA256550BF6E5CBC1E8, ___U3CU3E9__24_2)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get_U3CU3E9__24_2() const { return ___U3CU3E9__24_2; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of_U3CU3E9__24_2() { return &___U3CU3E9__24_2; }
	inline void set_U3CU3E9__24_2(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		___U3CU3E9__24_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__24_2), (void*)value);
	}
};


// DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25
struct U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523  : public RuntimeObject
{
public:
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DentedPixel.LTExamples.TestingUnitTests DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<>4__this
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * ___U3CU3E4__this_2;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<cubeCount>5__2
	int32_t ___U3CcubeCountU3E5__2_3;
	// System.Int32[] DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<tweensA>5__3
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___U3CtweensAU3E5__3_4;
	// UnityEngine.GameObject[] DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<aGOs>5__4
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___U3CaGOsU3E5__4_5;
	// System.Int32[] DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<tweensB>5__5
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___U3CtweensBU3E5__5_6;
	// UnityEngine.GameObject[] DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<bGOs>5__6
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___U3CbGOsU3E5__6_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523, ___U3CU3E4__this_2)); }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcubeCountU3E5__2_3() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523, ___U3CcubeCountU3E5__2_3)); }
	inline int32_t get_U3CcubeCountU3E5__2_3() const { return ___U3CcubeCountU3E5__2_3; }
	inline int32_t* get_address_of_U3CcubeCountU3E5__2_3() { return &___U3CcubeCountU3E5__2_3; }
	inline void set_U3CcubeCountU3E5__2_3(int32_t value)
	{
		___U3CcubeCountU3E5__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CtweensAU3E5__3_4() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523, ___U3CtweensAU3E5__3_4)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_U3CtweensAU3E5__3_4() const { return ___U3CtweensAU3E5__3_4; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_U3CtweensAU3E5__3_4() { return &___U3CtweensAU3E5__3_4; }
	inline void set_U3CtweensAU3E5__3_4(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___U3CtweensAU3E5__3_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtweensAU3E5__3_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CaGOsU3E5__4_5() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523, ___U3CaGOsU3E5__4_5)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_U3CaGOsU3E5__4_5() const { return ___U3CaGOsU3E5__4_5; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_U3CaGOsU3E5__4_5() { return &___U3CaGOsU3E5__4_5; }
	inline void set_U3CaGOsU3E5__4_5(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___U3CaGOsU3E5__4_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CaGOsU3E5__4_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtweensBU3E5__5_6() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523, ___U3CtweensBU3E5__5_6)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_U3CtweensBU3E5__5_6() const { return ___U3CtweensBU3E5__5_6; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_U3CtweensBU3E5__5_6() { return &___U3CtweensBU3E5__5_6; }
	inline void set_U3CtweensBU3E5__5_6(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___U3CtweensBU3E5__5_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtweensBU3E5__5_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CbGOsU3E5__6_7() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523, ___U3CbGOsU3E5__6_7)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_U3CbGOsU3E5__6_7() const { return ___U3CbGOsU3E5__6_7; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_U3CbGOsU3E5__6_7() { return &___U3CbGOsU3E5__6_7; }
	inline void set_U3CbGOsU3E5__6_7(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___U3CbGOsU3E5__6_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CbGOsU3E5__6_7), (void*)value);
	}
};


// DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26
struct U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB  : public RuntimeObject
{
public:
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DentedPixel.LTExamples.TestingUnitTests DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::<>4__this
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB, ___U3CU3E4__this_2)); }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24
struct U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65  : public RuntimeObject
{
public:
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DentedPixel.LTExamples.TestingUnitTests DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::<>4__this
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * ___U3CU3E4__this_2;
	// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0 DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::<>8__1
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F * ___U3CU3E8__1_3;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::<descriptionMatchCount>5__2
	int32_t ___U3CdescriptionMatchCountU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65, ___U3CU3E4__this_2)); }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E8__1_3() { return static_cast<int32_t>(offsetof(U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65, ___U3CU3E8__1_3)); }
	inline U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F * get_U3CU3E8__1_3() const { return ___U3CU3E8__1_3; }
	inline U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F ** get_address_of_U3CU3E8__1_3() { return &___U3CU3E8__1_3; }
	inline void set_U3CU3E8__1_3(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F * value)
	{
		___U3CU3E8__1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E8__1_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdescriptionMatchCountU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65, ___U3CdescriptionMatchCountU3E5__2_4)); }
	inline int32_t get_U3CdescriptionMatchCountU3E5__2_4() const { return ___U3CdescriptionMatchCountU3E5__2_4; }
	inline int32_t* get_address_of_U3CdescriptionMatchCountU3E5__2_4() { return &___U3CdescriptionMatchCountU3E5__2_4; }
	inline void set_U3CdescriptionMatchCountU3E5__2_4(int32_t value)
	{
		___U3CdescriptionMatchCountU3E5__2_4 = value;
	}
};


// TestingZLegacy/<>c
struct U3CU3Ec_t8ABF57D71EAC0C260D48F0C6C5CCC9C3D247FBEC  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t8ABF57D71EAC0C260D48F0C6C5CCC9C3D247FBEC_StaticFields
{
public:
	// TestingZLegacy/<>c TestingZLegacy/<>c::<>9
	U3CU3Ec_t8ABF57D71EAC0C260D48F0C6C5CCC9C3D247FBEC * ___U3CU3E9_0;
	// System.Action`1<System.Single> TestingZLegacy/<>c::<>9__20_0
	Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * ___U3CU3E9__20_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8ABF57D71EAC0C260D48F0C6C5CCC9C3D247FBEC_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t8ABF57D71EAC0C260D48F0C6C5CCC9C3D247FBEC * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t8ABF57D71EAC0C260D48F0C6C5CCC9C3D247FBEC ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t8ABF57D71EAC0C260D48F0C6C5CCC9C3D247FBEC * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__20_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8ABF57D71EAC0C260D48F0C6C5CCC9C3D247FBEC_StaticFields, ___U3CU3E9__20_0_1)); }
	inline Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * get_U3CU3E9__20_0_1() const { return ___U3CU3E9__20_0_1; }
	inline Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 ** get_address_of_U3CU3E9__20_0_1() { return &___U3CU3E9__20_0_1; }
	inline void set_U3CU3E9__20_0_1(Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * value)
	{
		___U3CU3E9__20_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__20_0_1), (void*)value);
	}
};


// TestingZLegacyExt/<>c
struct U3CU3Ec_tD7C586F26F27190F29F8B90744BCE69E24E2B0BB  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tD7C586F26F27190F29F8B90744BCE69E24E2B0BB_StaticFields
{
public:
	// TestingZLegacyExt/<>c TestingZLegacyExt/<>c::<>9
	U3CU3Ec_tD7C586F26F27190F29F8B90744BCE69E24E2B0BB * ___U3CU3E9_0;
	// System.Action`1<System.Single> TestingZLegacyExt/<>c::<>9__20_0
	Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * ___U3CU3E9__20_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD7C586F26F27190F29F8B90744BCE69E24E2B0BB_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tD7C586F26F27190F29F8B90744BCE69E24E2B0BB * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tD7C586F26F27190F29F8B90744BCE69E24E2B0BB ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tD7C586F26F27190F29F8B90744BCE69E24E2B0BB * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__20_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD7C586F26F27190F29F8B90744BCE69E24E2B0BB_StaticFields, ___U3CU3E9__20_0_1)); }
	inline Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * get_U3CU3E9__20_0_1() const { return ___U3CU3E9__20_0_1; }
	inline Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 ** get_address_of_U3CU3E9__20_0_1() { return &___U3CU3E9__20_0_1; }
	inline void set_U3CU3E9__20_0_1(Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * value)
	{
		___U3CU3E9__20_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__20_0_1), (void*)value);
	}
};


// UIScale/<ScaleObject>d__12
struct U3CScaleObjectU3Ed__12_t8DC31192573F6DFEF7FBA86C992E3587EAA46457  : public RuntimeObject
{
public:
	// System.Int32 UIScale/<ScaleObject>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UIScale/<ScaleObject>d__12::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UIScale UIScale/<ScaleObject>d__12::<>4__this
	UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CScaleObjectU3Ed__12_t8DC31192573F6DFEF7FBA86C992E3587EAA46457, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CScaleObjectU3Ed__12_t8DC31192573F6DFEF7FBA86C992E3587EAA46457, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CScaleObjectU3Ed__12_t8DC31192573F6DFEF7FBA86C992E3587EAA46457, ___U3CU3E4__this_2)); }
	inline UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.DateTime
struct DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth365_29), (void*)value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth366_30), (void*)value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___MinValue_31)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___MaxValue_32)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___MaxValue_32 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.LayerMask
struct LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.Rect
struct Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};


// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// LTBezier
struct LTBezier_tD081588F24017B409080ADD7EC92332888511C41  : public RuntimeObject
{
public:
	// System.Single LTBezier::length
	float ___length_0;
	// UnityEngine.Vector3 LTBezier::a
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a_1;
	// UnityEngine.Vector3 LTBezier::aa
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___aa_2;
	// UnityEngine.Vector3 LTBezier::bb
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___bb_3;
	// UnityEngine.Vector3 LTBezier::cc
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___cc_4;
	// System.Single LTBezier::len
	float ___len_5;
	// System.Single[] LTBezier::arcLengths
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___arcLengths_6;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(LTBezier_tD081588F24017B409080ADD7EC92332888511C41, ___length_0)); }
	inline float get_length_0() const { return ___length_0; }
	inline float* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(float value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_a_1() { return static_cast<int32_t>(offsetof(LTBezier_tD081588F24017B409080ADD7EC92332888511C41, ___a_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_a_1() const { return ___a_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_a_1() { return &___a_1; }
	inline void set_a_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___a_1 = value;
	}

	inline static int32_t get_offset_of_aa_2() { return static_cast<int32_t>(offsetof(LTBezier_tD081588F24017B409080ADD7EC92332888511C41, ___aa_2)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_aa_2() const { return ___aa_2; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_aa_2() { return &___aa_2; }
	inline void set_aa_2(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___aa_2 = value;
	}

	inline static int32_t get_offset_of_bb_3() { return static_cast<int32_t>(offsetof(LTBezier_tD081588F24017B409080ADD7EC92332888511C41, ___bb_3)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_bb_3() const { return ___bb_3; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_bb_3() { return &___bb_3; }
	inline void set_bb_3(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___bb_3 = value;
	}

	inline static int32_t get_offset_of_cc_4() { return static_cast<int32_t>(offsetof(LTBezier_tD081588F24017B409080ADD7EC92332888511C41, ___cc_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_cc_4() const { return ___cc_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_cc_4() { return &___cc_4; }
	inline void set_cc_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___cc_4 = value;
	}

	inline static int32_t get_offset_of_len_5() { return static_cast<int32_t>(offsetof(LTBezier_tD081588F24017B409080ADD7EC92332888511C41, ___len_5)); }
	inline float get_len_5() const { return ___len_5; }
	inline float* get_address_of_len_5() { return &___len_5; }
	inline void set_len_5(float value)
	{
		___len_5 = value;
	}

	inline static int32_t get_offset_of_arcLengths_6() { return static_cast<int32_t>(offsetof(LTBezier_tD081588F24017B409080ADD7EC92332888511C41, ___arcLengths_6)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_arcLengths_6() const { return ___arcLengths_6; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_arcLengths_6() { return &___arcLengths_6; }
	inline void set_arcLengths_6(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___arcLengths_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arcLengths_6), (void*)value);
	}
};


// LTDescrOptional
struct LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2  : public RuntimeObject
{
public:
	// UnityEngine.Transform LTDescrOptional::<toTrans>k__BackingField
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___U3CtoTransU3Ek__BackingField_0;
	// UnityEngine.Vector3 LTDescrOptional::<point>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CpointU3Ek__BackingField_1;
	// UnityEngine.Vector3 LTDescrOptional::<axis>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CaxisU3Ek__BackingField_2;
	// System.Single LTDescrOptional::<lastVal>k__BackingField
	float ___U3ClastValU3Ek__BackingField_3;
	// UnityEngine.Quaternion LTDescrOptional::<origRotation>k__BackingField
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___U3CorigRotationU3Ek__BackingField_4;
	// LTBezierPath LTDescrOptional::<path>k__BackingField
	LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48 * ___U3CpathU3Ek__BackingField_5;
	// LTSpline LTDescrOptional::<spline>k__BackingField
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 * ___U3CsplineU3Ek__BackingField_6;
	// UnityEngine.AnimationCurve LTDescrOptional::animationCurve
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___animationCurve_7;
	// System.Int32 LTDescrOptional::initFrameCount
	int32_t ___initFrameCount_8;
	// UnityEngine.Color LTDescrOptional::color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color_9;
	// LTRect LTDescrOptional::<ltRect>k__BackingField
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD * ___U3CltRectU3Ek__BackingField_10;
	// System.Action`1<System.Single> LTDescrOptional::<onUpdateFloat>k__BackingField
	Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * ___U3ConUpdateFloatU3Ek__BackingField_11;
	// System.Action`2<System.Single,System.Single> LTDescrOptional::<onUpdateFloatRatio>k__BackingField
	Action_2_tBA82D2430D3596594262E1ACAD640A8EEB5DB2F2 * ___U3ConUpdateFloatRatioU3Ek__BackingField_12;
	// System.Action`2<System.Single,System.Object> LTDescrOptional::<onUpdateFloatObject>k__BackingField
	Action_2_tC5EA1BA82797894656DD89A0F3E7B791EC8BD050 * ___U3ConUpdateFloatObjectU3Ek__BackingField_13;
	// System.Action`1<UnityEngine.Vector2> LTDescrOptional::<onUpdateVector2>k__BackingField
	Action_1_t0C1F417511439CBAA03909A69138FAF0BD19FA30 * ___U3ConUpdateVector2U3Ek__BackingField_14;
	// System.Action`1<UnityEngine.Vector3> LTDescrOptional::<onUpdateVector3>k__BackingField
	Action_1_tC2B4AB26EC30C6FC4AD8C9172FE509B3B4E1C26B * ___U3ConUpdateVector3U3Ek__BackingField_15;
	// System.Action`2<UnityEngine.Vector3,System.Object> LTDescrOptional::<onUpdateVector3Object>k__BackingField
	Action_2_tCA8676CFBCEAA1EE28321875815FD1767B25E4CB * ___U3ConUpdateVector3ObjectU3Ek__BackingField_16;
	// System.Action`1<UnityEngine.Color> LTDescrOptional::<onUpdateColor>k__BackingField
	Action_1_tC747810051096FEC47140D886015F5428DEDDAE2 * ___U3ConUpdateColorU3Ek__BackingField_17;
	// System.Action`2<UnityEngine.Color,System.Object> LTDescrOptional::<onUpdateColorObject>k__BackingField
	Action_2_t05923E9D06D867E1AC6A90E2B1B2FE28CA9F910A * ___U3ConUpdateColorObjectU3Ek__BackingField_18;
	// System.Action LTDescrOptional::<onComplete>k__BackingField
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3ConCompleteU3Ek__BackingField_19;
	// System.Action`1<System.Object> LTDescrOptional::<onCompleteObject>k__BackingField
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ___U3ConCompleteObjectU3Ek__BackingField_20;
	// System.Object LTDescrOptional::<onCompleteParam>k__BackingField
	RuntimeObject * ___U3ConCompleteParamU3Ek__BackingField_21;
	// System.Object LTDescrOptional::<onUpdateParam>k__BackingField
	RuntimeObject * ___U3ConUpdateParamU3Ek__BackingField_22;
	// System.Action LTDescrOptional::<onStart>k__BackingField
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3ConStartU3Ek__BackingField_23;

public:
	inline static int32_t get_offset_of_U3CtoTransU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3CtoTransU3Ek__BackingField_0)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_U3CtoTransU3Ek__BackingField_0() const { return ___U3CtoTransU3Ek__BackingField_0; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_U3CtoTransU3Ek__BackingField_0() { return &___U3CtoTransU3Ek__BackingField_0; }
	inline void set_U3CtoTransU3Ek__BackingField_0(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___U3CtoTransU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtoTransU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpointU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3CpointU3Ek__BackingField_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CpointU3Ek__BackingField_1() const { return ___U3CpointU3Ek__BackingField_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CpointU3Ek__BackingField_1() { return &___U3CpointU3Ek__BackingField_1; }
	inline void set_U3CpointU3Ek__BackingField_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CpointU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CaxisU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3CaxisU3Ek__BackingField_2)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CaxisU3Ek__BackingField_2() const { return ___U3CaxisU3Ek__BackingField_2; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CaxisU3Ek__BackingField_2() { return &___U3CaxisU3Ek__BackingField_2; }
	inline void set_U3CaxisU3Ek__BackingField_2(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CaxisU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3ClastValU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ClastValU3Ek__BackingField_3)); }
	inline float get_U3ClastValU3Ek__BackingField_3() const { return ___U3ClastValU3Ek__BackingField_3; }
	inline float* get_address_of_U3ClastValU3Ek__BackingField_3() { return &___U3ClastValU3Ek__BackingField_3; }
	inline void set_U3ClastValU3Ek__BackingField_3(float value)
	{
		___U3ClastValU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CorigRotationU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3CorigRotationU3Ek__BackingField_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_U3CorigRotationU3Ek__BackingField_4() const { return ___U3CorigRotationU3Ek__BackingField_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_U3CorigRotationU3Ek__BackingField_4() { return &___U3CorigRotationU3Ek__BackingField_4; }
	inline void set_U3CorigRotationU3Ek__BackingField_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___U3CorigRotationU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CpathU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3CpathU3Ek__BackingField_5)); }
	inline LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48 * get_U3CpathU3Ek__BackingField_5() const { return ___U3CpathU3Ek__BackingField_5; }
	inline LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48 ** get_address_of_U3CpathU3Ek__BackingField_5() { return &___U3CpathU3Ek__BackingField_5; }
	inline void set_U3CpathU3Ek__BackingField_5(LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48 * value)
	{
		___U3CpathU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpathU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CsplineU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3CsplineU3Ek__BackingField_6)); }
	inline LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 * get_U3CsplineU3Ek__BackingField_6() const { return ___U3CsplineU3Ek__BackingField_6; }
	inline LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 ** get_address_of_U3CsplineU3Ek__BackingField_6() { return &___U3CsplineU3Ek__BackingField_6; }
	inline void set_U3CsplineU3Ek__BackingField_6(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 * value)
	{
		___U3CsplineU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsplineU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_animationCurve_7() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___animationCurve_7)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_animationCurve_7() const { return ___animationCurve_7; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_animationCurve_7() { return &___animationCurve_7; }
	inline void set_animationCurve_7(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___animationCurve_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animationCurve_7), (void*)value);
	}

	inline static int32_t get_offset_of_initFrameCount_8() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___initFrameCount_8)); }
	inline int32_t get_initFrameCount_8() const { return ___initFrameCount_8; }
	inline int32_t* get_address_of_initFrameCount_8() { return &___initFrameCount_8; }
	inline void set_initFrameCount_8(int32_t value)
	{
		___initFrameCount_8 = value;
	}

	inline static int32_t get_offset_of_color_9() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___color_9)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_color_9() const { return ___color_9; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_color_9() { return &___color_9; }
	inline void set_color_9(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___color_9 = value;
	}

	inline static int32_t get_offset_of_U3CltRectU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3CltRectU3Ek__BackingField_10)); }
	inline LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD * get_U3CltRectU3Ek__BackingField_10() const { return ___U3CltRectU3Ek__BackingField_10; }
	inline LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD ** get_address_of_U3CltRectU3Ek__BackingField_10() { return &___U3CltRectU3Ek__BackingField_10; }
	inline void set_U3CltRectU3Ek__BackingField_10(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD * value)
	{
		___U3CltRectU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CltRectU3Ek__BackingField_10), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConUpdateFloatU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConUpdateFloatU3Ek__BackingField_11)); }
	inline Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * get_U3ConUpdateFloatU3Ek__BackingField_11() const { return ___U3ConUpdateFloatU3Ek__BackingField_11; }
	inline Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 ** get_address_of_U3ConUpdateFloatU3Ek__BackingField_11() { return &___U3ConUpdateFloatU3Ek__BackingField_11; }
	inline void set_U3ConUpdateFloatU3Ek__BackingField_11(Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * value)
	{
		___U3ConUpdateFloatU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConUpdateFloatU3Ek__BackingField_11), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConUpdateFloatRatioU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConUpdateFloatRatioU3Ek__BackingField_12)); }
	inline Action_2_tBA82D2430D3596594262E1ACAD640A8EEB5DB2F2 * get_U3ConUpdateFloatRatioU3Ek__BackingField_12() const { return ___U3ConUpdateFloatRatioU3Ek__BackingField_12; }
	inline Action_2_tBA82D2430D3596594262E1ACAD640A8EEB5DB2F2 ** get_address_of_U3ConUpdateFloatRatioU3Ek__BackingField_12() { return &___U3ConUpdateFloatRatioU3Ek__BackingField_12; }
	inline void set_U3ConUpdateFloatRatioU3Ek__BackingField_12(Action_2_tBA82D2430D3596594262E1ACAD640A8EEB5DB2F2 * value)
	{
		___U3ConUpdateFloatRatioU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConUpdateFloatRatioU3Ek__BackingField_12), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConUpdateFloatObjectU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConUpdateFloatObjectU3Ek__BackingField_13)); }
	inline Action_2_tC5EA1BA82797894656DD89A0F3E7B791EC8BD050 * get_U3ConUpdateFloatObjectU3Ek__BackingField_13() const { return ___U3ConUpdateFloatObjectU3Ek__BackingField_13; }
	inline Action_2_tC5EA1BA82797894656DD89A0F3E7B791EC8BD050 ** get_address_of_U3ConUpdateFloatObjectU3Ek__BackingField_13() { return &___U3ConUpdateFloatObjectU3Ek__BackingField_13; }
	inline void set_U3ConUpdateFloatObjectU3Ek__BackingField_13(Action_2_tC5EA1BA82797894656DD89A0F3E7B791EC8BD050 * value)
	{
		___U3ConUpdateFloatObjectU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConUpdateFloatObjectU3Ek__BackingField_13), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConUpdateVector2U3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConUpdateVector2U3Ek__BackingField_14)); }
	inline Action_1_t0C1F417511439CBAA03909A69138FAF0BD19FA30 * get_U3ConUpdateVector2U3Ek__BackingField_14() const { return ___U3ConUpdateVector2U3Ek__BackingField_14; }
	inline Action_1_t0C1F417511439CBAA03909A69138FAF0BD19FA30 ** get_address_of_U3ConUpdateVector2U3Ek__BackingField_14() { return &___U3ConUpdateVector2U3Ek__BackingField_14; }
	inline void set_U3ConUpdateVector2U3Ek__BackingField_14(Action_1_t0C1F417511439CBAA03909A69138FAF0BD19FA30 * value)
	{
		___U3ConUpdateVector2U3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConUpdateVector2U3Ek__BackingField_14), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConUpdateVector3U3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConUpdateVector3U3Ek__BackingField_15)); }
	inline Action_1_tC2B4AB26EC30C6FC4AD8C9172FE509B3B4E1C26B * get_U3ConUpdateVector3U3Ek__BackingField_15() const { return ___U3ConUpdateVector3U3Ek__BackingField_15; }
	inline Action_1_tC2B4AB26EC30C6FC4AD8C9172FE509B3B4E1C26B ** get_address_of_U3ConUpdateVector3U3Ek__BackingField_15() { return &___U3ConUpdateVector3U3Ek__BackingField_15; }
	inline void set_U3ConUpdateVector3U3Ek__BackingField_15(Action_1_tC2B4AB26EC30C6FC4AD8C9172FE509B3B4E1C26B * value)
	{
		___U3ConUpdateVector3U3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConUpdateVector3U3Ek__BackingField_15), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConUpdateVector3ObjectU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConUpdateVector3ObjectU3Ek__BackingField_16)); }
	inline Action_2_tCA8676CFBCEAA1EE28321875815FD1767B25E4CB * get_U3ConUpdateVector3ObjectU3Ek__BackingField_16() const { return ___U3ConUpdateVector3ObjectU3Ek__BackingField_16; }
	inline Action_2_tCA8676CFBCEAA1EE28321875815FD1767B25E4CB ** get_address_of_U3ConUpdateVector3ObjectU3Ek__BackingField_16() { return &___U3ConUpdateVector3ObjectU3Ek__BackingField_16; }
	inline void set_U3ConUpdateVector3ObjectU3Ek__BackingField_16(Action_2_tCA8676CFBCEAA1EE28321875815FD1767B25E4CB * value)
	{
		___U3ConUpdateVector3ObjectU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConUpdateVector3ObjectU3Ek__BackingField_16), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConUpdateColorU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConUpdateColorU3Ek__BackingField_17)); }
	inline Action_1_tC747810051096FEC47140D886015F5428DEDDAE2 * get_U3ConUpdateColorU3Ek__BackingField_17() const { return ___U3ConUpdateColorU3Ek__BackingField_17; }
	inline Action_1_tC747810051096FEC47140D886015F5428DEDDAE2 ** get_address_of_U3ConUpdateColorU3Ek__BackingField_17() { return &___U3ConUpdateColorU3Ek__BackingField_17; }
	inline void set_U3ConUpdateColorU3Ek__BackingField_17(Action_1_tC747810051096FEC47140D886015F5428DEDDAE2 * value)
	{
		___U3ConUpdateColorU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConUpdateColorU3Ek__BackingField_17), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConUpdateColorObjectU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConUpdateColorObjectU3Ek__BackingField_18)); }
	inline Action_2_t05923E9D06D867E1AC6A90E2B1B2FE28CA9F910A * get_U3ConUpdateColorObjectU3Ek__BackingField_18() const { return ___U3ConUpdateColorObjectU3Ek__BackingField_18; }
	inline Action_2_t05923E9D06D867E1AC6A90E2B1B2FE28CA9F910A ** get_address_of_U3ConUpdateColorObjectU3Ek__BackingField_18() { return &___U3ConUpdateColorObjectU3Ek__BackingField_18; }
	inline void set_U3ConUpdateColorObjectU3Ek__BackingField_18(Action_2_t05923E9D06D867E1AC6A90E2B1B2FE28CA9F910A * value)
	{
		___U3ConUpdateColorObjectU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConUpdateColorObjectU3Ek__BackingField_18), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConCompleteU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConCompleteU3Ek__BackingField_19)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3ConCompleteU3Ek__BackingField_19() const { return ___U3ConCompleteU3Ek__BackingField_19; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3ConCompleteU3Ek__BackingField_19() { return &___U3ConCompleteU3Ek__BackingField_19; }
	inline void set_U3ConCompleteU3Ek__BackingField_19(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3ConCompleteU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConCompleteU3Ek__BackingField_19), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConCompleteObjectU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConCompleteObjectU3Ek__BackingField_20)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get_U3ConCompleteObjectU3Ek__BackingField_20() const { return ___U3ConCompleteObjectU3Ek__BackingField_20; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of_U3ConCompleteObjectU3Ek__BackingField_20() { return &___U3ConCompleteObjectU3Ek__BackingField_20; }
	inline void set_U3ConCompleteObjectU3Ek__BackingField_20(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		___U3ConCompleteObjectU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConCompleteObjectU3Ek__BackingField_20), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConCompleteParamU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConCompleteParamU3Ek__BackingField_21)); }
	inline RuntimeObject * get_U3ConCompleteParamU3Ek__BackingField_21() const { return ___U3ConCompleteParamU3Ek__BackingField_21; }
	inline RuntimeObject ** get_address_of_U3ConCompleteParamU3Ek__BackingField_21() { return &___U3ConCompleteParamU3Ek__BackingField_21; }
	inline void set_U3ConCompleteParamU3Ek__BackingField_21(RuntimeObject * value)
	{
		___U3ConCompleteParamU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConCompleteParamU3Ek__BackingField_21), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConUpdateParamU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConUpdateParamU3Ek__BackingField_22)); }
	inline RuntimeObject * get_U3ConUpdateParamU3Ek__BackingField_22() const { return ___U3ConUpdateParamU3Ek__BackingField_22; }
	inline RuntimeObject ** get_address_of_U3ConUpdateParamU3Ek__BackingField_22() { return &___U3ConUpdateParamU3Ek__BackingField_22; }
	inline void set_U3ConUpdateParamU3Ek__BackingField_22(RuntimeObject * value)
	{
		___U3ConUpdateParamU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConUpdateParamU3Ek__BackingField_22), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConStartU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConStartU3Ek__BackingField_23)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3ConStartU3Ek__BackingField_23() const { return ___U3ConStartU3Ek__BackingField_23; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3ConStartU3Ek__BackingField_23() { return &___U3ConStartU3Ek__BackingField_23; }
	inline void set_U3ConStartU3Ek__BackingField_23(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3ConStartU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConStartU3Ek__BackingField_23), (void*)value);
	}
};


// LTGUI
struct LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816  : public RuntimeObject
{
public:

public:
};

struct LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields
{
public:
	// System.Int32 LTGUI::RECT_LEVELS
	int32_t ___RECT_LEVELS_0;
	// System.Int32 LTGUI::RECTS_PER_LEVEL
	int32_t ___RECTS_PER_LEVEL_1;
	// System.Int32 LTGUI::BUTTONS_MAX
	int32_t ___BUTTONS_MAX_2;
	// LTRect[] LTGUI::levels
	LTRectU5BU5D_t764F00C0F4846E8D85477368EC19E34DF5D0DCBA* ___levels_3;
	// System.Int32[] LTGUI::levelDepths
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___levelDepths_4;
	// UnityEngine.Rect[] LTGUI::buttons
	RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* ___buttons_5;
	// System.Int32[] LTGUI::buttonLevels
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buttonLevels_6;
	// System.Int32[] LTGUI::buttonLastFrame
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buttonLastFrame_7;
	// LTRect LTGUI::r
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD * ___r_8;
	// UnityEngine.Color LTGUI::color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color_9;
	// System.Boolean LTGUI::isGUIEnabled
	bool ___isGUIEnabled_10;
	// System.Int32 LTGUI::global_counter
	int32_t ___global_counter_11;

public:
	inline static int32_t get_offset_of_RECT_LEVELS_0() { return static_cast<int32_t>(offsetof(LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields, ___RECT_LEVELS_0)); }
	inline int32_t get_RECT_LEVELS_0() const { return ___RECT_LEVELS_0; }
	inline int32_t* get_address_of_RECT_LEVELS_0() { return &___RECT_LEVELS_0; }
	inline void set_RECT_LEVELS_0(int32_t value)
	{
		___RECT_LEVELS_0 = value;
	}

	inline static int32_t get_offset_of_RECTS_PER_LEVEL_1() { return static_cast<int32_t>(offsetof(LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields, ___RECTS_PER_LEVEL_1)); }
	inline int32_t get_RECTS_PER_LEVEL_1() const { return ___RECTS_PER_LEVEL_1; }
	inline int32_t* get_address_of_RECTS_PER_LEVEL_1() { return &___RECTS_PER_LEVEL_1; }
	inline void set_RECTS_PER_LEVEL_1(int32_t value)
	{
		___RECTS_PER_LEVEL_1 = value;
	}

	inline static int32_t get_offset_of_BUTTONS_MAX_2() { return static_cast<int32_t>(offsetof(LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields, ___BUTTONS_MAX_2)); }
	inline int32_t get_BUTTONS_MAX_2() const { return ___BUTTONS_MAX_2; }
	inline int32_t* get_address_of_BUTTONS_MAX_2() { return &___BUTTONS_MAX_2; }
	inline void set_BUTTONS_MAX_2(int32_t value)
	{
		___BUTTONS_MAX_2 = value;
	}

	inline static int32_t get_offset_of_levels_3() { return static_cast<int32_t>(offsetof(LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields, ___levels_3)); }
	inline LTRectU5BU5D_t764F00C0F4846E8D85477368EC19E34DF5D0DCBA* get_levels_3() const { return ___levels_3; }
	inline LTRectU5BU5D_t764F00C0F4846E8D85477368EC19E34DF5D0DCBA** get_address_of_levels_3() { return &___levels_3; }
	inline void set_levels_3(LTRectU5BU5D_t764F00C0F4846E8D85477368EC19E34DF5D0DCBA* value)
	{
		___levels_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___levels_3), (void*)value);
	}

	inline static int32_t get_offset_of_levelDepths_4() { return static_cast<int32_t>(offsetof(LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields, ___levelDepths_4)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_levelDepths_4() const { return ___levelDepths_4; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_levelDepths_4() { return &___levelDepths_4; }
	inline void set_levelDepths_4(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___levelDepths_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___levelDepths_4), (void*)value);
	}

	inline static int32_t get_offset_of_buttons_5() { return static_cast<int32_t>(offsetof(LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields, ___buttons_5)); }
	inline RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* get_buttons_5() const { return ___buttons_5; }
	inline RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE** get_address_of_buttons_5() { return &___buttons_5; }
	inline void set_buttons_5(RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* value)
	{
		___buttons_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buttons_5), (void*)value);
	}

	inline static int32_t get_offset_of_buttonLevels_6() { return static_cast<int32_t>(offsetof(LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields, ___buttonLevels_6)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buttonLevels_6() const { return ___buttonLevels_6; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buttonLevels_6() { return &___buttonLevels_6; }
	inline void set_buttonLevels_6(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buttonLevels_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buttonLevels_6), (void*)value);
	}

	inline static int32_t get_offset_of_buttonLastFrame_7() { return static_cast<int32_t>(offsetof(LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields, ___buttonLastFrame_7)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buttonLastFrame_7() const { return ___buttonLastFrame_7; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buttonLastFrame_7() { return &___buttonLastFrame_7; }
	inline void set_buttonLastFrame_7(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buttonLastFrame_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buttonLastFrame_7), (void*)value);
	}

	inline static int32_t get_offset_of_r_8() { return static_cast<int32_t>(offsetof(LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields, ___r_8)); }
	inline LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD * get_r_8() const { return ___r_8; }
	inline LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD ** get_address_of_r_8() { return &___r_8; }
	inline void set_r_8(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD * value)
	{
		___r_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___r_8), (void*)value);
	}

	inline static int32_t get_offset_of_color_9() { return static_cast<int32_t>(offsetof(LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields, ___color_9)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_color_9() const { return ___color_9; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_color_9() { return &___color_9; }
	inline void set_color_9(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___color_9 = value;
	}

	inline static int32_t get_offset_of_isGUIEnabled_10() { return static_cast<int32_t>(offsetof(LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields, ___isGUIEnabled_10)); }
	inline bool get_isGUIEnabled_10() const { return ___isGUIEnabled_10; }
	inline bool* get_address_of_isGUIEnabled_10() { return &___isGUIEnabled_10; }
	inline void set_isGUIEnabled_10(bool value)
	{
		___isGUIEnabled_10 = value;
	}

	inline static int32_t get_offset_of_global_counter_11() { return static_cast<int32_t>(offsetof(LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields, ___global_counter_11)); }
	inline int32_t get_global_counter_11() const { return ___global_counter_11; }
	inline int32_t* get_address_of_global_counter_11() { return &___global_counter_11; }
	inline void set_global_counter_11(int32_t value)
	{
		___global_counter_11 = value;
	}
};


// LeanProp
struct LeanProp_tEF273BF6EB0960483D03A097A40313D700DE3538 
{
public:
	// System.Int32 LeanProp::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LeanProp_tEF273BF6EB0960483D03A097A40313D700DE3538, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// LeanTweenType
struct LeanTweenType_tAE51C34373F1326AC0BB9DB0F7EF1883603D55A9 
{
public:
	// System.Int32 LeanTweenType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LeanTweenType_tAE51C34373F1326AC0BB9DB0F7EF1883603D55A9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.Pose
struct Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A 
{
public:
	// UnityEngine.Vector3 UnityEngine.Pose::position
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position_0;
	// UnityEngine.Quaternion UnityEngine.Pose::rotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A, ___position_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_position_0() const { return ___position_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_rotation_1() { return static_cast<int32_t>(offsetof(Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A, ___rotation_1)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_rotation_1() const { return ___rotation_1; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_rotation_1() { return &___rotation_1; }
	inline void set_rotation_1(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___rotation_1 = value;
	}
};

struct Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A_StaticFields
{
public:
	// UnityEngine.Pose UnityEngine.Pose::k_Identity
	Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  ___k_Identity_2;

public:
	inline static int32_t get_offset_of_k_Identity_2() { return static_cast<int32_t>(offsetof(Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A_StaticFields, ___k_Identity_2)); }
	inline Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  get_k_Identity_2() const { return ___k_Identity_2; }
	inline Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A * get_address_of_k_Identity_2() { return &___k_Identity_2; }
	inline void set_k_Identity_2(Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  value)
	{
		___k_Identity_2 = value;
	}
};


// System.TimeSpan
struct TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ___Zero_19)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ___MinValue_21)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};


// TweenAction
struct TweenAction_tB7B9473B02F7CC04ED4083C3934285274119AE0B 
{
public:
	// System.Int32 TweenAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TweenAction_tB7B9473B02F7CC04ED4083C3934285274119AE0B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GameScale/ScaleStatus
struct ScaleStatus_t689BFBF9A323CB919946D3D3ACFF4FC81CE9345C 
{
public:
	// System.Int32 GameScale/ScaleStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScaleStatus_t689BFBF9A323CB919946D3D3ACFF4FC81CE9345C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// LTGUI/Element_Type
struct Element_Type_tCB9236487D710B365968D3F055C4CCCA555A6BFE 
{
public:
	// System.Int32 LTGUI/Element_Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Element_Type_tCB9236487D710B365968D3F055C4CCCA555A6BFE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// LeanAudioOptions/LeanAudioWaveStyle
struct LeanAudioWaveStyle_tCFD7C1D135072B754C7CBA8422AE8DA6BE8D6CC2 
{
public:
	// System.Int32 LeanAudioOptions/LeanAudioWaveStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LeanAudioWaveStyle_tCFD7C1D135072B754C7CBA8422AE8DA6BE8D6CC2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// PageGame/PageStatus
struct PageStatus_t019890187BA0881F3266F253DBDDE533BB3C8738 
{
public:
	// System.Int32 PageGame/PageStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PageStatus_t019890187BA0881F3266F253DBDDE533BB3C8738, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Spawn/ScaleStatus
struct ScaleStatus_tB2E9FCEE91E30E92C70DFDFC1EE30BA4FB6764F0 
{
public:
	// System.Int32 Spawn/ScaleStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScaleStatus_tB2E9FCEE91E30E92C70DFDFC1EE30BA4FB6764F0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0
struct U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F  : public RuntimeObject
{
public:
	// DentedPixel.LTExamples.TestingUnitTests DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<>4__this
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * ___U3CU3E4__this_0;
	// UnityEngine.GameObject[] DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::cubes
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___cubes_1;
	// System.Int32[] DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::tweenIds
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___tweenIds_2;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::onCompleteCount
	int32_t ___onCompleteCount_3;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::cubeToTrans
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeToTrans_4;
	// UnityEngine.Vector3 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::cubeDestEnd
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___cubeDestEnd_5;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::cubeSpline
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeSpline_6;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::jumpTimeId
	int32_t ___jumpTimeId_7;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::jumpCube
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___jumpCube_8;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::zeroCube
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___zeroCube_9;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::cubeScale
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeScale_10;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::cubeRotate
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeRotate_11;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::cubeRotateA
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeRotateA_12;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::cubeRotateB
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeRotateB_13;
	// System.Single DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::onStartTime
	float ___onStartTime_14;
	// UnityEngine.Vector3 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::beforePos
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___beforePos_15;
	// UnityEngine.Vector3 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::beforePos2
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___beforePos2_16;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::totalEasingCheck
	int32_t ___totalEasingCheck_17;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::totalEasingCheckSuccess
	int32_t ___totalEasingCheckSuccess_18;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::value2UpdateCalled
	bool ___value2UpdateCalled_19;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<>9__21
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3CU3E9__21_20;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___U3CU3E4__this_0)); }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_cubes_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___cubes_1)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_cubes_1() const { return ___cubes_1; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_cubes_1() { return &___cubes_1; }
	inline void set_cubes_1(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___cubes_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubes_1), (void*)value);
	}

	inline static int32_t get_offset_of_tweenIds_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___tweenIds_2)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_tweenIds_2() const { return ___tweenIds_2; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_tweenIds_2() { return &___tweenIds_2; }
	inline void set_tweenIds_2(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___tweenIds_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tweenIds_2), (void*)value);
	}

	inline static int32_t get_offset_of_onCompleteCount_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___onCompleteCount_3)); }
	inline int32_t get_onCompleteCount_3() const { return ___onCompleteCount_3; }
	inline int32_t* get_address_of_onCompleteCount_3() { return &___onCompleteCount_3; }
	inline void set_onCompleteCount_3(int32_t value)
	{
		___onCompleteCount_3 = value;
	}

	inline static int32_t get_offset_of_cubeToTrans_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___cubeToTrans_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeToTrans_4() const { return ___cubeToTrans_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeToTrans_4() { return &___cubeToTrans_4; }
	inline void set_cubeToTrans_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeToTrans_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeToTrans_4), (void*)value);
	}

	inline static int32_t get_offset_of_cubeDestEnd_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___cubeDestEnd_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_cubeDestEnd_5() const { return ___cubeDestEnd_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_cubeDestEnd_5() { return &___cubeDestEnd_5; }
	inline void set_cubeDestEnd_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___cubeDestEnd_5 = value;
	}

	inline static int32_t get_offset_of_cubeSpline_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___cubeSpline_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeSpline_6() const { return ___cubeSpline_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeSpline_6() { return &___cubeSpline_6; }
	inline void set_cubeSpline_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeSpline_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeSpline_6), (void*)value);
	}

	inline static int32_t get_offset_of_jumpTimeId_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___jumpTimeId_7)); }
	inline int32_t get_jumpTimeId_7() const { return ___jumpTimeId_7; }
	inline int32_t* get_address_of_jumpTimeId_7() { return &___jumpTimeId_7; }
	inline void set_jumpTimeId_7(int32_t value)
	{
		___jumpTimeId_7 = value;
	}

	inline static int32_t get_offset_of_jumpCube_8() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___jumpCube_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_jumpCube_8() const { return ___jumpCube_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_jumpCube_8() { return &___jumpCube_8; }
	inline void set_jumpCube_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___jumpCube_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___jumpCube_8), (void*)value);
	}

	inline static int32_t get_offset_of_zeroCube_9() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___zeroCube_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_zeroCube_9() const { return ___zeroCube_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_zeroCube_9() { return &___zeroCube_9; }
	inline void set_zeroCube_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___zeroCube_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___zeroCube_9), (void*)value);
	}

	inline static int32_t get_offset_of_cubeScale_10() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___cubeScale_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeScale_10() const { return ___cubeScale_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeScale_10() { return &___cubeScale_10; }
	inline void set_cubeScale_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeScale_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeScale_10), (void*)value);
	}

	inline static int32_t get_offset_of_cubeRotate_11() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___cubeRotate_11)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeRotate_11() const { return ___cubeRotate_11; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeRotate_11() { return &___cubeRotate_11; }
	inline void set_cubeRotate_11(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeRotate_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeRotate_11), (void*)value);
	}

	inline static int32_t get_offset_of_cubeRotateA_12() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___cubeRotateA_12)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeRotateA_12() const { return ___cubeRotateA_12; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeRotateA_12() { return &___cubeRotateA_12; }
	inline void set_cubeRotateA_12(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeRotateA_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeRotateA_12), (void*)value);
	}

	inline static int32_t get_offset_of_cubeRotateB_13() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___cubeRotateB_13)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeRotateB_13() const { return ___cubeRotateB_13; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeRotateB_13() { return &___cubeRotateB_13; }
	inline void set_cubeRotateB_13(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeRotateB_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeRotateB_13), (void*)value);
	}

	inline static int32_t get_offset_of_onStartTime_14() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___onStartTime_14)); }
	inline float get_onStartTime_14() const { return ___onStartTime_14; }
	inline float* get_address_of_onStartTime_14() { return &___onStartTime_14; }
	inline void set_onStartTime_14(float value)
	{
		___onStartTime_14 = value;
	}

	inline static int32_t get_offset_of_beforePos_15() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___beforePos_15)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_beforePos_15() const { return ___beforePos_15; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_beforePos_15() { return &___beforePos_15; }
	inline void set_beforePos_15(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___beforePos_15 = value;
	}

	inline static int32_t get_offset_of_beforePos2_16() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___beforePos2_16)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_beforePos2_16() const { return ___beforePos2_16; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_beforePos2_16() { return &___beforePos2_16; }
	inline void set_beforePos2_16(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___beforePos2_16 = value;
	}

	inline static int32_t get_offset_of_totalEasingCheck_17() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___totalEasingCheck_17)); }
	inline int32_t get_totalEasingCheck_17() const { return ___totalEasingCheck_17; }
	inline int32_t* get_address_of_totalEasingCheck_17() { return &___totalEasingCheck_17; }
	inline void set_totalEasingCheck_17(int32_t value)
	{
		___totalEasingCheck_17 = value;
	}

	inline static int32_t get_offset_of_totalEasingCheckSuccess_18() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___totalEasingCheckSuccess_18)); }
	inline int32_t get_totalEasingCheckSuccess_18() const { return ___totalEasingCheckSuccess_18; }
	inline int32_t* get_address_of_totalEasingCheckSuccess_18() { return &___totalEasingCheckSuccess_18; }
	inline void set_totalEasingCheckSuccess_18(int32_t value)
	{
		___totalEasingCheckSuccess_18 = value;
	}

	inline static int32_t get_offset_of_value2UpdateCalled_19() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___value2UpdateCalled_19)); }
	inline bool get_value2UpdateCalled_19() const { return ___value2UpdateCalled_19; }
	inline bool* get_address_of_value2UpdateCalled_19() { return &___value2UpdateCalled_19; }
	inline void set_value2UpdateCalled_19(bool value)
	{
		___value2UpdateCalled_19 = value;
	}

	inline static int32_t get_offset_of_U3CU3E9__21_20() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___U3CU3E9__21_20)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3CU3E9__21_20() const { return ___U3CU3E9__21_20; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3CU3E9__21_20() { return &___U3CU3E9__21_20; }
	inline void set_U3CU3E9__21_20(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3CU3E9__21_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__21_20), (void*)value);
	}
};


// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0
struct U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F  : public RuntimeObject
{
public:
	// DentedPixel.LTExamples.TestingUnitTests DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<>4__this
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * ___U3CU3E4__this_0;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::pauseCount
	int32_t ___pauseCount_1;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::cubeRound
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeRound_2;
	// UnityEngine.Vector3 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::onStartPos
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___onStartPos_3;
	// UnityEngine.Vector3 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::onStartPosSpline
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___onStartPosSpline_4;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::cubeSpline
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeSpline_5;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::cubeSeq
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeSeq_6;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::cubeBounds
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeBounds_7;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::didPassBounds
	bool ___didPassBounds_8;
	// UnityEngine.Vector3 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::failPoint
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___failPoint_9;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::setOnStartNum
	int32_t ___setOnStartNum_10;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::setPosOnUpdate
	bool ___setPosOnUpdate_11;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::setPosNum
	int32_t ___setPosNum_12;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::hasGroupTweensCheckStarted
	bool ___hasGroupTweensCheckStarted_13;
	// System.Single DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::previousXlt4
	float ___previousXlt4_14;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::onUpdateWasCalled
	bool ___onUpdateWasCalled_15;
	// System.Single DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::start
	float ___start_16;
	// System.Single DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::expectedTime
	float ___expectedTime_17;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::didGetCorrectOnUpdate
	bool ___didGetCorrectOnUpdate_18;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<>9__13
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3CU3E9__13_19;
	// System.Action`1<UnityEngine.Vector3> DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<>9__14
	Action_1_tC2B4AB26EC30C6FC4AD8C9172FE509B3B4E1C26B * ___U3CU3E9__14_20;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<>9__16
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3CU3E9__16_21;
	// System.Action`1<System.Object> DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<>9__15
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ___U3CU3E9__15_22;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___U3CU3E4__this_0)); }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_pauseCount_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___pauseCount_1)); }
	inline int32_t get_pauseCount_1() const { return ___pauseCount_1; }
	inline int32_t* get_address_of_pauseCount_1() { return &___pauseCount_1; }
	inline void set_pauseCount_1(int32_t value)
	{
		___pauseCount_1 = value;
	}

	inline static int32_t get_offset_of_cubeRound_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___cubeRound_2)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeRound_2() const { return ___cubeRound_2; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeRound_2() { return &___cubeRound_2; }
	inline void set_cubeRound_2(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeRound_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeRound_2), (void*)value);
	}

	inline static int32_t get_offset_of_onStartPos_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___onStartPos_3)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_onStartPos_3() const { return ___onStartPos_3; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_onStartPos_3() { return &___onStartPos_3; }
	inline void set_onStartPos_3(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___onStartPos_3 = value;
	}

	inline static int32_t get_offset_of_onStartPosSpline_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___onStartPosSpline_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_onStartPosSpline_4() const { return ___onStartPosSpline_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_onStartPosSpline_4() { return &___onStartPosSpline_4; }
	inline void set_onStartPosSpline_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___onStartPosSpline_4 = value;
	}

	inline static int32_t get_offset_of_cubeSpline_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___cubeSpline_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeSpline_5() const { return ___cubeSpline_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeSpline_5() { return &___cubeSpline_5; }
	inline void set_cubeSpline_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeSpline_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeSpline_5), (void*)value);
	}

	inline static int32_t get_offset_of_cubeSeq_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___cubeSeq_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeSeq_6() const { return ___cubeSeq_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeSeq_6() { return &___cubeSeq_6; }
	inline void set_cubeSeq_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeSeq_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeSeq_6), (void*)value);
	}

	inline static int32_t get_offset_of_cubeBounds_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___cubeBounds_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeBounds_7() const { return ___cubeBounds_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeBounds_7() { return &___cubeBounds_7; }
	inline void set_cubeBounds_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeBounds_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeBounds_7), (void*)value);
	}

	inline static int32_t get_offset_of_didPassBounds_8() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___didPassBounds_8)); }
	inline bool get_didPassBounds_8() const { return ___didPassBounds_8; }
	inline bool* get_address_of_didPassBounds_8() { return &___didPassBounds_8; }
	inline void set_didPassBounds_8(bool value)
	{
		___didPassBounds_8 = value;
	}

	inline static int32_t get_offset_of_failPoint_9() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___failPoint_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_failPoint_9() const { return ___failPoint_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_failPoint_9() { return &___failPoint_9; }
	inline void set_failPoint_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___failPoint_9 = value;
	}

	inline static int32_t get_offset_of_setOnStartNum_10() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___setOnStartNum_10)); }
	inline int32_t get_setOnStartNum_10() const { return ___setOnStartNum_10; }
	inline int32_t* get_address_of_setOnStartNum_10() { return &___setOnStartNum_10; }
	inline void set_setOnStartNum_10(int32_t value)
	{
		___setOnStartNum_10 = value;
	}

	inline static int32_t get_offset_of_setPosOnUpdate_11() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___setPosOnUpdate_11)); }
	inline bool get_setPosOnUpdate_11() const { return ___setPosOnUpdate_11; }
	inline bool* get_address_of_setPosOnUpdate_11() { return &___setPosOnUpdate_11; }
	inline void set_setPosOnUpdate_11(bool value)
	{
		___setPosOnUpdate_11 = value;
	}

	inline static int32_t get_offset_of_setPosNum_12() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___setPosNum_12)); }
	inline int32_t get_setPosNum_12() const { return ___setPosNum_12; }
	inline int32_t* get_address_of_setPosNum_12() { return &___setPosNum_12; }
	inline void set_setPosNum_12(int32_t value)
	{
		___setPosNum_12 = value;
	}

	inline static int32_t get_offset_of_hasGroupTweensCheckStarted_13() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___hasGroupTweensCheckStarted_13)); }
	inline bool get_hasGroupTweensCheckStarted_13() const { return ___hasGroupTweensCheckStarted_13; }
	inline bool* get_address_of_hasGroupTweensCheckStarted_13() { return &___hasGroupTweensCheckStarted_13; }
	inline void set_hasGroupTweensCheckStarted_13(bool value)
	{
		___hasGroupTweensCheckStarted_13 = value;
	}

	inline static int32_t get_offset_of_previousXlt4_14() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___previousXlt4_14)); }
	inline float get_previousXlt4_14() const { return ___previousXlt4_14; }
	inline float* get_address_of_previousXlt4_14() { return &___previousXlt4_14; }
	inline void set_previousXlt4_14(float value)
	{
		___previousXlt4_14 = value;
	}

	inline static int32_t get_offset_of_onUpdateWasCalled_15() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___onUpdateWasCalled_15)); }
	inline bool get_onUpdateWasCalled_15() const { return ___onUpdateWasCalled_15; }
	inline bool* get_address_of_onUpdateWasCalled_15() { return &___onUpdateWasCalled_15; }
	inline void set_onUpdateWasCalled_15(bool value)
	{
		___onUpdateWasCalled_15 = value;
	}

	inline static int32_t get_offset_of_start_16() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___start_16)); }
	inline float get_start_16() const { return ___start_16; }
	inline float* get_address_of_start_16() { return &___start_16; }
	inline void set_start_16(float value)
	{
		___start_16 = value;
	}

	inline static int32_t get_offset_of_expectedTime_17() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___expectedTime_17)); }
	inline float get_expectedTime_17() const { return ___expectedTime_17; }
	inline float* get_address_of_expectedTime_17() { return &___expectedTime_17; }
	inline void set_expectedTime_17(float value)
	{
		___expectedTime_17 = value;
	}

	inline static int32_t get_offset_of_didGetCorrectOnUpdate_18() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___didGetCorrectOnUpdate_18)); }
	inline bool get_didGetCorrectOnUpdate_18() const { return ___didGetCorrectOnUpdate_18; }
	inline bool* get_address_of_didGetCorrectOnUpdate_18() { return &___didGetCorrectOnUpdate_18; }
	inline void set_didGetCorrectOnUpdate_18(bool value)
	{
		___didGetCorrectOnUpdate_18 = value;
	}

	inline static int32_t get_offset_of_U3CU3E9__13_19() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___U3CU3E9__13_19)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3CU3E9__13_19() const { return ___U3CU3E9__13_19; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3CU3E9__13_19() { return &___U3CU3E9__13_19; }
	inline void set_U3CU3E9__13_19(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3CU3E9__13_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__13_19), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_20() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___U3CU3E9__14_20)); }
	inline Action_1_tC2B4AB26EC30C6FC4AD8C9172FE509B3B4E1C26B * get_U3CU3E9__14_20() const { return ___U3CU3E9__14_20; }
	inline Action_1_tC2B4AB26EC30C6FC4AD8C9172FE509B3B4E1C26B ** get_address_of_U3CU3E9__14_20() { return &___U3CU3E9__14_20; }
	inline void set_U3CU3E9__14_20(Action_1_tC2B4AB26EC30C6FC4AD8C9172FE509B3B4E1C26B * value)
	{
		___U3CU3E9__14_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__14_20), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_21() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___U3CU3E9__16_21)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3CU3E9__16_21() const { return ___U3CU3E9__16_21; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3CU3E9__16_21() { return &___U3CU3E9__16_21; }
	inline void set_U3CU3E9__16_21(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3CU3E9__16_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__16_21), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__15_22() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___U3CU3E9__15_22)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get_U3CU3E9__15_22() const { return ___U3CU3E9__15_22; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of_U3CU3E9__15_22() { return &___U3CU3E9__15_22; }
	inline void set_U3CU3E9__15_22(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		___U3CU3E9__15_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__15_22), (void*)value);
	}
};


// TestingZLegacy/TimingType
struct TimingType_t45F13DECEBFB1E67A20F76FF233F367BB999C27F 
{
public:
	// System.Int32 TestingZLegacy/TimingType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TimingType_t45F13DECEBFB1E67A20F76FF233F367BB999C27F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TestingZLegacyExt/TimingType
struct TimingType_t578CF99DA23D1F87B1A67A57C8C872C3E24F55E6 
{
public:
	// System.Int32 TestingZLegacyExt/TimingType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TimingType_t578CF99DA23D1F87B1A67A57C8C872C3E24F55E6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UIScale/ScaleStatus
struct ScaleStatus_t10EF7D7816F5D321088B6700F763286A44D71363 
{
public:
	// System.Int32 UIScale/ScaleStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScaleStatus_t10EF7D7816F5D321088B6700F763286A44D71363, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// LTDescr
struct LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F  : public RuntimeObject
{
public:
	// System.Boolean LTDescr::toggle
	bool ___toggle_0;
	// System.Boolean LTDescr::useEstimatedTime
	bool ___useEstimatedTime_1;
	// System.Boolean LTDescr::useFrames
	bool ___useFrames_2;
	// System.Boolean LTDescr::useManualTime
	bool ___useManualTime_3;
	// System.Boolean LTDescr::usesNormalDt
	bool ___usesNormalDt_4;
	// System.Boolean LTDescr::hasInitiliazed
	bool ___hasInitiliazed_5;
	// System.Boolean LTDescr::hasExtraOnCompletes
	bool ___hasExtraOnCompletes_6;
	// System.Boolean LTDescr::hasPhysics
	bool ___hasPhysics_7;
	// System.Boolean LTDescr::onCompleteOnRepeat
	bool ___onCompleteOnRepeat_8;
	// System.Boolean LTDescr::onCompleteOnStart
	bool ___onCompleteOnStart_9;
	// System.Boolean LTDescr::useRecursion
	bool ___useRecursion_10;
	// System.Single LTDescr::ratioPassed
	float ___ratioPassed_11;
	// System.Single LTDescr::passed
	float ___passed_12;
	// System.Single LTDescr::delay
	float ___delay_13;
	// System.Single LTDescr::time
	float ___time_14;
	// System.Single LTDescr::speed
	float ___speed_15;
	// System.Single LTDescr::lastVal
	float ___lastVal_16;
	// System.UInt32 LTDescr::_id
	uint32_t ____id_17;
	// System.Int32 LTDescr::loopCount
	int32_t ___loopCount_18;
	// System.UInt32 LTDescr::counter
	uint32_t ___counter_19;
	// System.Single LTDescr::direction
	float ___direction_20;
	// System.Single LTDescr::directionLast
	float ___directionLast_21;
	// System.Single LTDescr::overshoot
	float ___overshoot_22;
	// System.Single LTDescr::period
	float ___period_23;
	// System.Single LTDescr::scale
	float ___scale_24;
	// System.Boolean LTDescr::destroyOnComplete
	bool ___destroyOnComplete_25;
	// UnityEngine.Transform LTDescr::trans
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___trans_26;
	// UnityEngine.Vector3 LTDescr::fromInternal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___fromInternal_27;
	// UnityEngine.Vector3 LTDescr::toInternal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___toInternal_28;
	// UnityEngine.Vector3 LTDescr::diff
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___diff_29;
	// UnityEngine.Vector3 LTDescr::diffDiv2
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___diffDiv2_30;
	// TweenAction LTDescr::type
	int32_t ___type_31;
	// LeanTweenType LTDescr::easeType
	int32_t ___easeType_32;
	// LeanTweenType LTDescr::loopType
	int32_t ___loopType_33;
	// System.Boolean LTDescr::hasUpdateCallback
	bool ___hasUpdateCallback_34;
	// LTDescr/EaseTypeDelegate LTDescr::easeMethod
	EaseTypeDelegate_t0285AB55B28F5B5A6688E966B51FBA837B2E84AF * ___easeMethod_35;
	// LTDescr/ActionMethodDelegate LTDescr::<easeInternal>k__BackingField
	ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 * ___U3CeaseInternalU3Ek__BackingField_36;
	// LTDescr/ActionMethodDelegate LTDescr::<initInternal>k__BackingField
	ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 * ___U3CinitInternalU3Ek__BackingField_37;
	// UnityEngine.SpriteRenderer LTDescr::spriteRen
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___spriteRen_38;
	// UnityEngine.RectTransform LTDescr::rectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___rectTransform_39;
	// UnityEngine.UI.Text LTDescr::uiText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___uiText_40;
	// UnityEngine.UI.Image LTDescr::uiImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___uiImage_41;
	// UnityEngine.UI.RawImage LTDescr::rawImage
	RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * ___rawImage_42;
	// UnityEngine.Sprite[] LTDescr::sprites
	SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* ___sprites_43;
	// LTDescrOptional LTDescr::_optional
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2 * ____optional_44;

public:
	inline static int32_t get_offset_of_toggle_0() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___toggle_0)); }
	inline bool get_toggle_0() const { return ___toggle_0; }
	inline bool* get_address_of_toggle_0() { return &___toggle_0; }
	inline void set_toggle_0(bool value)
	{
		___toggle_0 = value;
	}

	inline static int32_t get_offset_of_useEstimatedTime_1() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___useEstimatedTime_1)); }
	inline bool get_useEstimatedTime_1() const { return ___useEstimatedTime_1; }
	inline bool* get_address_of_useEstimatedTime_1() { return &___useEstimatedTime_1; }
	inline void set_useEstimatedTime_1(bool value)
	{
		___useEstimatedTime_1 = value;
	}

	inline static int32_t get_offset_of_useFrames_2() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___useFrames_2)); }
	inline bool get_useFrames_2() const { return ___useFrames_2; }
	inline bool* get_address_of_useFrames_2() { return &___useFrames_2; }
	inline void set_useFrames_2(bool value)
	{
		___useFrames_2 = value;
	}

	inline static int32_t get_offset_of_useManualTime_3() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___useManualTime_3)); }
	inline bool get_useManualTime_3() const { return ___useManualTime_3; }
	inline bool* get_address_of_useManualTime_3() { return &___useManualTime_3; }
	inline void set_useManualTime_3(bool value)
	{
		___useManualTime_3 = value;
	}

	inline static int32_t get_offset_of_usesNormalDt_4() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___usesNormalDt_4)); }
	inline bool get_usesNormalDt_4() const { return ___usesNormalDt_4; }
	inline bool* get_address_of_usesNormalDt_4() { return &___usesNormalDt_4; }
	inline void set_usesNormalDt_4(bool value)
	{
		___usesNormalDt_4 = value;
	}

	inline static int32_t get_offset_of_hasInitiliazed_5() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___hasInitiliazed_5)); }
	inline bool get_hasInitiliazed_5() const { return ___hasInitiliazed_5; }
	inline bool* get_address_of_hasInitiliazed_5() { return &___hasInitiliazed_5; }
	inline void set_hasInitiliazed_5(bool value)
	{
		___hasInitiliazed_5 = value;
	}

	inline static int32_t get_offset_of_hasExtraOnCompletes_6() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___hasExtraOnCompletes_6)); }
	inline bool get_hasExtraOnCompletes_6() const { return ___hasExtraOnCompletes_6; }
	inline bool* get_address_of_hasExtraOnCompletes_6() { return &___hasExtraOnCompletes_6; }
	inline void set_hasExtraOnCompletes_6(bool value)
	{
		___hasExtraOnCompletes_6 = value;
	}

	inline static int32_t get_offset_of_hasPhysics_7() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___hasPhysics_7)); }
	inline bool get_hasPhysics_7() const { return ___hasPhysics_7; }
	inline bool* get_address_of_hasPhysics_7() { return &___hasPhysics_7; }
	inline void set_hasPhysics_7(bool value)
	{
		___hasPhysics_7 = value;
	}

	inline static int32_t get_offset_of_onCompleteOnRepeat_8() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___onCompleteOnRepeat_8)); }
	inline bool get_onCompleteOnRepeat_8() const { return ___onCompleteOnRepeat_8; }
	inline bool* get_address_of_onCompleteOnRepeat_8() { return &___onCompleteOnRepeat_8; }
	inline void set_onCompleteOnRepeat_8(bool value)
	{
		___onCompleteOnRepeat_8 = value;
	}

	inline static int32_t get_offset_of_onCompleteOnStart_9() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___onCompleteOnStart_9)); }
	inline bool get_onCompleteOnStart_9() const { return ___onCompleteOnStart_9; }
	inline bool* get_address_of_onCompleteOnStart_9() { return &___onCompleteOnStart_9; }
	inline void set_onCompleteOnStart_9(bool value)
	{
		___onCompleteOnStart_9 = value;
	}

	inline static int32_t get_offset_of_useRecursion_10() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___useRecursion_10)); }
	inline bool get_useRecursion_10() const { return ___useRecursion_10; }
	inline bool* get_address_of_useRecursion_10() { return &___useRecursion_10; }
	inline void set_useRecursion_10(bool value)
	{
		___useRecursion_10 = value;
	}

	inline static int32_t get_offset_of_ratioPassed_11() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___ratioPassed_11)); }
	inline float get_ratioPassed_11() const { return ___ratioPassed_11; }
	inline float* get_address_of_ratioPassed_11() { return &___ratioPassed_11; }
	inline void set_ratioPassed_11(float value)
	{
		___ratioPassed_11 = value;
	}

	inline static int32_t get_offset_of_passed_12() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___passed_12)); }
	inline float get_passed_12() const { return ___passed_12; }
	inline float* get_address_of_passed_12() { return &___passed_12; }
	inline void set_passed_12(float value)
	{
		___passed_12 = value;
	}

	inline static int32_t get_offset_of_delay_13() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___delay_13)); }
	inline float get_delay_13() const { return ___delay_13; }
	inline float* get_address_of_delay_13() { return &___delay_13; }
	inline void set_delay_13(float value)
	{
		___delay_13 = value;
	}

	inline static int32_t get_offset_of_time_14() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___time_14)); }
	inline float get_time_14() const { return ___time_14; }
	inline float* get_address_of_time_14() { return &___time_14; }
	inline void set_time_14(float value)
	{
		___time_14 = value;
	}

	inline static int32_t get_offset_of_speed_15() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___speed_15)); }
	inline float get_speed_15() const { return ___speed_15; }
	inline float* get_address_of_speed_15() { return &___speed_15; }
	inline void set_speed_15(float value)
	{
		___speed_15 = value;
	}

	inline static int32_t get_offset_of_lastVal_16() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___lastVal_16)); }
	inline float get_lastVal_16() const { return ___lastVal_16; }
	inline float* get_address_of_lastVal_16() { return &___lastVal_16; }
	inline void set_lastVal_16(float value)
	{
		___lastVal_16 = value;
	}

	inline static int32_t get_offset_of__id_17() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ____id_17)); }
	inline uint32_t get__id_17() const { return ____id_17; }
	inline uint32_t* get_address_of__id_17() { return &____id_17; }
	inline void set__id_17(uint32_t value)
	{
		____id_17 = value;
	}

	inline static int32_t get_offset_of_loopCount_18() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___loopCount_18)); }
	inline int32_t get_loopCount_18() const { return ___loopCount_18; }
	inline int32_t* get_address_of_loopCount_18() { return &___loopCount_18; }
	inline void set_loopCount_18(int32_t value)
	{
		___loopCount_18 = value;
	}

	inline static int32_t get_offset_of_counter_19() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___counter_19)); }
	inline uint32_t get_counter_19() const { return ___counter_19; }
	inline uint32_t* get_address_of_counter_19() { return &___counter_19; }
	inline void set_counter_19(uint32_t value)
	{
		___counter_19 = value;
	}

	inline static int32_t get_offset_of_direction_20() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___direction_20)); }
	inline float get_direction_20() const { return ___direction_20; }
	inline float* get_address_of_direction_20() { return &___direction_20; }
	inline void set_direction_20(float value)
	{
		___direction_20 = value;
	}

	inline static int32_t get_offset_of_directionLast_21() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___directionLast_21)); }
	inline float get_directionLast_21() const { return ___directionLast_21; }
	inline float* get_address_of_directionLast_21() { return &___directionLast_21; }
	inline void set_directionLast_21(float value)
	{
		___directionLast_21 = value;
	}

	inline static int32_t get_offset_of_overshoot_22() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___overshoot_22)); }
	inline float get_overshoot_22() const { return ___overshoot_22; }
	inline float* get_address_of_overshoot_22() { return &___overshoot_22; }
	inline void set_overshoot_22(float value)
	{
		___overshoot_22 = value;
	}

	inline static int32_t get_offset_of_period_23() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___period_23)); }
	inline float get_period_23() const { return ___period_23; }
	inline float* get_address_of_period_23() { return &___period_23; }
	inline void set_period_23(float value)
	{
		___period_23 = value;
	}

	inline static int32_t get_offset_of_scale_24() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___scale_24)); }
	inline float get_scale_24() const { return ___scale_24; }
	inline float* get_address_of_scale_24() { return &___scale_24; }
	inline void set_scale_24(float value)
	{
		___scale_24 = value;
	}

	inline static int32_t get_offset_of_destroyOnComplete_25() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___destroyOnComplete_25)); }
	inline bool get_destroyOnComplete_25() const { return ___destroyOnComplete_25; }
	inline bool* get_address_of_destroyOnComplete_25() { return &___destroyOnComplete_25; }
	inline void set_destroyOnComplete_25(bool value)
	{
		___destroyOnComplete_25 = value;
	}

	inline static int32_t get_offset_of_trans_26() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___trans_26)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_trans_26() const { return ___trans_26; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_trans_26() { return &___trans_26; }
	inline void set_trans_26(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___trans_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trans_26), (void*)value);
	}

	inline static int32_t get_offset_of_fromInternal_27() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___fromInternal_27)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_fromInternal_27() const { return ___fromInternal_27; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_fromInternal_27() { return &___fromInternal_27; }
	inline void set_fromInternal_27(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___fromInternal_27 = value;
	}

	inline static int32_t get_offset_of_toInternal_28() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___toInternal_28)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_toInternal_28() const { return ___toInternal_28; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_toInternal_28() { return &___toInternal_28; }
	inline void set_toInternal_28(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___toInternal_28 = value;
	}

	inline static int32_t get_offset_of_diff_29() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___diff_29)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_diff_29() const { return ___diff_29; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_diff_29() { return &___diff_29; }
	inline void set_diff_29(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___diff_29 = value;
	}

	inline static int32_t get_offset_of_diffDiv2_30() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___diffDiv2_30)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_diffDiv2_30() const { return ___diffDiv2_30; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_diffDiv2_30() { return &___diffDiv2_30; }
	inline void set_diffDiv2_30(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___diffDiv2_30 = value;
	}

	inline static int32_t get_offset_of_type_31() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___type_31)); }
	inline int32_t get_type_31() const { return ___type_31; }
	inline int32_t* get_address_of_type_31() { return &___type_31; }
	inline void set_type_31(int32_t value)
	{
		___type_31 = value;
	}

	inline static int32_t get_offset_of_easeType_32() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___easeType_32)); }
	inline int32_t get_easeType_32() const { return ___easeType_32; }
	inline int32_t* get_address_of_easeType_32() { return &___easeType_32; }
	inline void set_easeType_32(int32_t value)
	{
		___easeType_32 = value;
	}

	inline static int32_t get_offset_of_loopType_33() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___loopType_33)); }
	inline int32_t get_loopType_33() const { return ___loopType_33; }
	inline int32_t* get_address_of_loopType_33() { return &___loopType_33; }
	inline void set_loopType_33(int32_t value)
	{
		___loopType_33 = value;
	}

	inline static int32_t get_offset_of_hasUpdateCallback_34() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___hasUpdateCallback_34)); }
	inline bool get_hasUpdateCallback_34() const { return ___hasUpdateCallback_34; }
	inline bool* get_address_of_hasUpdateCallback_34() { return &___hasUpdateCallback_34; }
	inline void set_hasUpdateCallback_34(bool value)
	{
		___hasUpdateCallback_34 = value;
	}

	inline static int32_t get_offset_of_easeMethod_35() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___easeMethod_35)); }
	inline EaseTypeDelegate_t0285AB55B28F5B5A6688E966B51FBA837B2E84AF * get_easeMethod_35() const { return ___easeMethod_35; }
	inline EaseTypeDelegate_t0285AB55B28F5B5A6688E966B51FBA837B2E84AF ** get_address_of_easeMethod_35() { return &___easeMethod_35; }
	inline void set_easeMethod_35(EaseTypeDelegate_t0285AB55B28F5B5A6688E966B51FBA837B2E84AF * value)
	{
		___easeMethod_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___easeMethod_35), (void*)value);
	}

	inline static int32_t get_offset_of_U3CeaseInternalU3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___U3CeaseInternalU3Ek__BackingField_36)); }
	inline ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 * get_U3CeaseInternalU3Ek__BackingField_36() const { return ___U3CeaseInternalU3Ek__BackingField_36; }
	inline ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 ** get_address_of_U3CeaseInternalU3Ek__BackingField_36() { return &___U3CeaseInternalU3Ek__BackingField_36; }
	inline void set_U3CeaseInternalU3Ek__BackingField_36(ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 * value)
	{
		___U3CeaseInternalU3Ek__BackingField_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CeaseInternalU3Ek__BackingField_36), (void*)value);
	}

	inline static int32_t get_offset_of_U3CinitInternalU3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___U3CinitInternalU3Ek__BackingField_37)); }
	inline ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 * get_U3CinitInternalU3Ek__BackingField_37() const { return ___U3CinitInternalU3Ek__BackingField_37; }
	inline ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 ** get_address_of_U3CinitInternalU3Ek__BackingField_37() { return &___U3CinitInternalU3Ek__BackingField_37; }
	inline void set_U3CinitInternalU3Ek__BackingField_37(ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 * value)
	{
		___U3CinitInternalU3Ek__BackingField_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CinitInternalU3Ek__BackingField_37), (void*)value);
	}

	inline static int32_t get_offset_of_spriteRen_38() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___spriteRen_38)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_spriteRen_38() const { return ___spriteRen_38; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_spriteRen_38() { return &___spriteRen_38; }
	inline void set_spriteRen_38(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___spriteRen_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spriteRen_38), (void*)value);
	}

	inline static int32_t get_offset_of_rectTransform_39() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___rectTransform_39)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_rectTransform_39() const { return ___rectTransform_39; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_rectTransform_39() { return &___rectTransform_39; }
	inline void set_rectTransform_39(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___rectTransform_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rectTransform_39), (void*)value);
	}

	inline static int32_t get_offset_of_uiText_40() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___uiText_40)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_uiText_40() const { return ___uiText_40; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_uiText_40() { return &___uiText_40; }
	inline void set_uiText_40(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___uiText_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___uiText_40), (void*)value);
	}

	inline static int32_t get_offset_of_uiImage_41() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___uiImage_41)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_uiImage_41() const { return ___uiImage_41; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_uiImage_41() { return &___uiImage_41; }
	inline void set_uiImage_41(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___uiImage_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___uiImage_41), (void*)value);
	}

	inline static int32_t get_offset_of_rawImage_42() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___rawImage_42)); }
	inline RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * get_rawImage_42() const { return ___rawImage_42; }
	inline RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A ** get_address_of_rawImage_42() { return &___rawImage_42; }
	inline void set_rawImage_42(RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * value)
	{
		___rawImage_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rawImage_42), (void*)value);
	}

	inline static int32_t get_offset_of_sprites_43() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___sprites_43)); }
	inline SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* get_sprites_43() const { return ___sprites_43; }
	inline SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77** get_address_of_sprites_43() { return &___sprites_43; }
	inline void set_sprites_43(SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* value)
	{
		___sprites_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sprites_43), (void*)value);
	}

	inline static int32_t get_offset_of__optional_44() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ____optional_44)); }
	inline LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2 * get__optional_44() const { return ____optional_44; }
	inline LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2 ** get_address_of__optional_44() { return &____optional_44; }
	inline void set__optional_44(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2 * value)
	{
		____optional_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____optional_44), (void*)value);
	}
};

struct LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_StaticFields
{
public:
	// System.Single LTDescr::val
	float ___val_45;
	// System.Single LTDescr::dt
	float ___dt_46;
	// UnityEngine.Vector3 LTDescr::newVect
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___newVect_47;

public:
	inline static int32_t get_offset_of_val_45() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_StaticFields, ___val_45)); }
	inline float get_val_45() const { return ___val_45; }
	inline float* get_address_of_val_45() { return &___val_45; }
	inline void set_val_45(float value)
	{
		___val_45 = value;
	}

	inline static int32_t get_offset_of_dt_46() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_StaticFields, ___dt_46)); }
	inline float get_dt_46() const { return ___dt_46; }
	inline float* get_address_of_dt_46() { return &___dt_46; }
	inline void set_dt_46(float value)
	{
		___dt_46 = value;
	}

	inline static int32_t get_offset_of_newVect_47() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_StaticFields, ___newVect_47)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_newVect_47() const { return ___newVect_47; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_newVect_47() { return &___newVect_47; }
	inline void set_newVect_47(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___newVect_47 = value;
	}
};


// LTRect
struct LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD  : public RuntimeObject
{
public:
	// UnityEngine.Rect LTRect::_rect
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ____rect_0;
	// System.Single LTRect::alpha
	float ___alpha_1;
	// System.Single LTRect::rotation
	float ___rotation_2;
	// UnityEngine.Vector2 LTRect::pivot
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___pivot_3;
	// UnityEngine.Vector2 LTRect::margin
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___margin_4;
	// UnityEngine.Rect LTRect::relativeRect
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___relativeRect_5;
	// System.Boolean LTRect::rotateEnabled
	bool ___rotateEnabled_6;
	// System.Boolean LTRect::rotateFinished
	bool ___rotateFinished_7;
	// System.Boolean LTRect::alphaEnabled
	bool ___alphaEnabled_8;
	// System.String LTRect::labelStr
	String_t* ___labelStr_9;
	// LTGUI/Element_Type LTRect::type
	int32_t ___type_10;
	// UnityEngine.GUIStyle LTRect::style
	GUIStyle_t29C59470ACD0A35C81EB0615653FD38C455A4726 * ___style_11;
	// System.Boolean LTRect::useColor
	bool ___useColor_12;
	// UnityEngine.Color LTRect::color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color_13;
	// System.Boolean LTRect::fontScaleToFit
	bool ___fontScaleToFit_14;
	// System.Boolean LTRect::useSimpleScale
	bool ___useSimpleScale_15;
	// System.Boolean LTRect::sizeByHeight
	bool ___sizeByHeight_16;
	// UnityEngine.Texture LTRect::texture
	Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___texture_17;
	// System.Int32 LTRect::_id
	int32_t ____id_18;
	// System.Int32 LTRect::counter
	int32_t ___counter_19;

public:
	inline static int32_t get_offset_of__rect_0() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ____rect_0)); }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  get__rect_0() const { return ____rect_0; }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * get_address_of__rect_0() { return &____rect_0; }
	inline void set__rect_0(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  value)
	{
		____rect_0 = value;
	}

	inline static int32_t get_offset_of_alpha_1() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___alpha_1)); }
	inline float get_alpha_1() const { return ___alpha_1; }
	inline float* get_address_of_alpha_1() { return &___alpha_1; }
	inline void set_alpha_1(float value)
	{
		___alpha_1 = value;
	}

	inline static int32_t get_offset_of_rotation_2() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___rotation_2)); }
	inline float get_rotation_2() const { return ___rotation_2; }
	inline float* get_address_of_rotation_2() { return &___rotation_2; }
	inline void set_rotation_2(float value)
	{
		___rotation_2 = value;
	}

	inline static int32_t get_offset_of_pivot_3() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___pivot_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_pivot_3() const { return ___pivot_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_pivot_3() { return &___pivot_3; }
	inline void set_pivot_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___pivot_3 = value;
	}

	inline static int32_t get_offset_of_margin_4() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___margin_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_margin_4() const { return ___margin_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_margin_4() { return &___margin_4; }
	inline void set_margin_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___margin_4 = value;
	}

	inline static int32_t get_offset_of_relativeRect_5() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___relativeRect_5)); }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  get_relativeRect_5() const { return ___relativeRect_5; }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * get_address_of_relativeRect_5() { return &___relativeRect_5; }
	inline void set_relativeRect_5(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  value)
	{
		___relativeRect_5 = value;
	}

	inline static int32_t get_offset_of_rotateEnabled_6() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___rotateEnabled_6)); }
	inline bool get_rotateEnabled_6() const { return ___rotateEnabled_6; }
	inline bool* get_address_of_rotateEnabled_6() { return &___rotateEnabled_6; }
	inline void set_rotateEnabled_6(bool value)
	{
		___rotateEnabled_6 = value;
	}

	inline static int32_t get_offset_of_rotateFinished_7() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___rotateFinished_7)); }
	inline bool get_rotateFinished_7() const { return ___rotateFinished_7; }
	inline bool* get_address_of_rotateFinished_7() { return &___rotateFinished_7; }
	inline void set_rotateFinished_7(bool value)
	{
		___rotateFinished_7 = value;
	}

	inline static int32_t get_offset_of_alphaEnabled_8() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___alphaEnabled_8)); }
	inline bool get_alphaEnabled_8() const { return ___alphaEnabled_8; }
	inline bool* get_address_of_alphaEnabled_8() { return &___alphaEnabled_8; }
	inline void set_alphaEnabled_8(bool value)
	{
		___alphaEnabled_8 = value;
	}

	inline static int32_t get_offset_of_labelStr_9() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___labelStr_9)); }
	inline String_t* get_labelStr_9() const { return ___labelStr_9; }
	inline String_t** get_address_of_labelStr_9() { return &___labelStr_9; }
	inline void set_labelStr_9(String_t* value)
	{
		___labelStr_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___labelStr_9), (void*)value);
	}

	inline static int32_t get_offset_of_type_10() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___type_10)); }
	inline int32_t get_type_10() const { return ___type_10; }
	inline int32_t* get_address_of_type_10() { return &___type_10; }
	inline void set_type_10(int32_t value)
	{
		___type_10 = value;
	}

	inline static int32_t get_offset_of_style_11() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___style_11)); }
	inline GUIStyle_t29C59470ACD0A35C81EB0615653FD38C455A4726 * get_style_11() const { return ___style_11; }
	inline GUIStyle_t29C59470ACD0A35C81EB0615653FD38C455A4726 ** get_address_of_style_11() { return &___style_11; }
	inline void set_style_11(GUIStyle_t29C59470ACD0A35C81EB0615653FD38C455A4726 * value)
	{
		___style_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___style_11), (void*)value);
	}

	inline static int32_t get_offset_of_useColor_12() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___useColor_12)); }
	inline bool get_useColor_12() const { return ___useColor_12; }
	inline bool* get_address_of_useColor_12() { return &___useColor_12; }
	inline void set_useColor_12(bool value)
	{
		___useColor_12 = value;
	}

	inline static int32_t get_offset_of_color_13() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___color_13)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_color_13() const { return ___color_13; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_color_13() { return &___color_13; }
	inline void set_color_13(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___color_13 = value;
	}

	inline static int32_t get_offset_of_fontScaleToFit_14() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___fontScaleToFit_14)); }
	inline bool get_fontScaleToFit_14() const { return ___fontScaleToFit_14; }
	inline bool* get_address_of_fontScaleToFit_14() { return &___fontScaleToFit_14; }
	inline void set_fontScaleToFit_14(bool value)
	{
		___fontScaleToFit_14 = value;
	}

	inline static int32_t get_offset_of_useSimpleScale_15() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___useSimpleScale_15)); }
	inline bool get_useSimpleScale_15() const { return ___useSimpleScale_15; }
	inline bool* get_address_of_useSimpleScale_15() { return &___useSimpleScale_15; }
	inline void set_useSimpleScale_15(bool value)
	{
		___useSimpleScale_15 = value;
	}

	inline static int32_t get_offset_of_sizeByHeight_16() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___sizeByHeight_16)); }
	inline bool get_sizeByHeight_16() const { return ___sizeByHeight_16; }
	inline bool* get_address_of_sizeByHeight_16() { return &___sizeByHeight_16; }
	inline void set_sizeByHeight_16(bool value)
	{
		___sizeByHeight_16 = value;
	}

	inline static int32_t get_offset_of_texture_17() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___texture_17)); }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * get_texture_17() const { return ___texture_17; }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE ** get_address_of_texture_17() { return &___texture_17; }
	inline void set_texture_17(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * value)
	{
		___texture_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___texture_17), (void*)value);
	}

	inline static int32_t get_offset_of__id_18() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ____id_18)); }
	inline int32_t get__id_18() const { return ____id_18; }
	inline int32_t* get_address_of__id_18() { return &____id_18; }
	inline void set__id_18(int32_t value)
	{
		____id_18 = value;
	}

	inline static int32_t get_offset_of_counter_19() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___counter_19)); }
	inline int32_t get_counter_19() const { return ___counter_19; }
	inline int32_t* get_address_of_counter_19() { return &___counter_19; }
	inline void set_counter_19(int32_t value)
	{
		___counter_19 = value;
	}
};

struct LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD_StaticFields
{
public:
	// System.Boolean LTRect::colorTouched
	bool ___colorTouched_20;

public:
	inline static int32_t get_offset_of_colorTouched_20() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD_StaticFields, ___colorTouched_20)); }
	inline bool get_colorTouched_20() const { return ___colorTouched_20; }
	inline bool* get_address_of_colorTouched_20() { return &___colorTouched_20; }
	inline void set_colorTouched_20(bool value)
	{
		___colorTouched_20 = value;
	}
};


// LeanAudioOptions
struct LeanAudioOptions_t594A044DA50FEB70E6FA086157B3DB457D0692AE  : public RuntimeObject
{
public:
	// LeanAudioOptions/LeanAudioWaveStyle LeanAudioOptions::waveStyle
	int32_t ___waveStyle_0;
	// UnityEngine.Vector3[] LeanAudioOptions::vibrato
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___vibrato_1;
	// UnityEngine.Vector3[] LeanAudioOptions::modulation
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___modulation_2;
	// System.Int32 LeanAudioOptions::frequencyRate
	int32_t ___frequencyRate_3;
	// System.Single LeanAudioOptions::waveNoiseScale
	float ___waveNoiseScale_4;
	// System.Single LeanAudioOptions::waveNoiseInfluence
	float ___waveNoiseInfluence_5;
	// System.Boolean LeanAudioOptions::useSetData
	bool ___useSetData_6;
	// LeanAudioStream LeanAudioOptions::stream
	LeanAudioStream_tDA1B6363B48B65291F4C928A5A706C159FD13712 * ___stream_7;

public:
	inline static int32_t get_offset_of_waveStyle_0() { return static_cast<int32_t>(offsetof(LeanAudioOptions_t594A044DA50FEB70E6FA086157B3DB457D0692AE, ___waveStyle_0)); }
	inline int32_t get_waveStyle_0() const { return ___waveStyle_0; }
	inline int32_t* get_address_of_waveStyle_0() { return &___waveStyle_0; }
	inline void set_waveStyle_0(int32_t value)
	{
		___waveStyle_0 = value;
	}

	inline static int32_t get_offset_of_vibrato_1() { return static_cast<int32_t>(offsetof(LeanAudioOptions_t594A044DA50FEB70E6FA086157B3DB457D0692AE, ___vibrato_1)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_vibrato_1() const { return ___vibrato_1; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_vibrato_1() { return &___vibrato_1; }
	inline void set_vibrato_1(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___vibrato_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vibrato_1), (void*)value);
	}

	inline static int32_t get_offset_of_modulation_2() { return static_cast<int32_t>(offsetof(LeanAudioOptions_t594A044DA50FEB70E6FA086157B3DB457D0692AE, ___modulation_2)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_modulation_2() const { return ___modulation_2; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_modulation_2() { return &___modulation_2; }
	inline void set_modulation_2(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___modulation_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___modulation_2), (void*)value);
	}

	inline static int32_t get_offset_of_frequencyRate_3() { return static_cast<int32_t>(offsetof(LeanAudioOptions_t594A044DA50FEB70E6FA086157B3DB457D0692AE, ___frequencyRate_3)); }
	inline int32_t get_frequencyRate_3() const { return ___frequencyRate_3; }
	inline int32_t* get_address_of_frequencyRate_3() { return &___frequencyRate_3; }
	inline void set_frequencyRate_3(int32_t value)
	{
		___frequencyRate_3 = value;
	}

	inline static int32_t get_offset_of_waveNoiseScale_4() { return static_cast<int32_t>(offsetof(LeanAudioOptions_t594A044DA50FEB70E6FA086157B3DB457D0692AE, ___waveNoiseScale_4)); }
	inline float get_waveNoiseScale_4() const { return ___waveNoiseScale_4; }
	inline float* get_address_of_waveNoiseScale_4() { return &___waveNoiseScale_4; }
	inline void set_waveNoiseScale_4(float value)
	{
		___waveNoiseScale_4 = value;
	}

	inline static int32_t get_offset_of_waveNoiseInfluence_5() { return static_cast<int32_t>(offsetof(LeanAudioOptions_t594A044DA50FEB70E6FA086157B3DB457D0692AE, ___waveNoiseInfluence_5)); }
	inline float get_waveNoiseInfluence_5() const { return ___waveNoiseInfluence_5; }
	inline float* get_address_of_waveNoiseInfluence_5() { return &___waveNoiseInfluence_5; }
	inline void set_waveNoiseInfluence_5(float value)
	{
		___waveNoiseInfluence_5 = value;
	}

	inline static int32_t get_offset_of_useSetData_6() { return static_cast<int32_t>(offsetof(LeanAudioOptions_t594A044DA50FEB70E6FA086157B3DB457D0692AE, ___useSetData_6)); }
	inline bool get_useSetData_6() const { return ___useSetData_6; }
	inline bool* get_address_of_useSetData_6() { return &___useSetData_6; }
	inline void set_useSetData_6(bool value)
	{
		___useSetData_6 = value;
	}

	inline static int32_t get_offset_of_stream_7() { return static_cast<int32_t>(offsetof(LeanAudioOptions_t594A044DA50FEB70E6FA086157B3DB457D0692AE, ___stream_7)); }
	inline LeanAudioStream_tDA1B6363B48B65291F4C928A5A706C159FD13712 * get_stream_7() const { return ___stream_7; }
	inline LeanAudioStream_tDA1B6363B48B65291F4C928A5A706C159FD13712 ** get_address_of_stream_7() { return &___stream_7; }
	inline void set_stream_7(LeanAudioStream_tDA1B6363B48B65291F4C928A5A706C159FD13712 * value)
	{
		___stream_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stream_7), (void*)value);
	}
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_pinvoke : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_com : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
};

// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// Readme
struct Readme_tD58D22679F01C9AD66EA323B2DE68BFF6DBDCEE7  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// UnityEngine.Texture2D Readme::icon
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___icon_4;
	// System.String Readme::title
	String_t* ___title_5;
	// Readme/Section[] Readme::sections
	SectionU5BU5D_t013C5C59593F9D61D69A33E91B8C1450419ED659* ___sections_6;
	// System.Boolean Readme::loadedLayout
	bool ___loadedLayout_7;

public:
	inline static int32_t get_offset_of_icon_4() { return static_cast<int32_t>(offsetof(Readme_tD58D22679F01C9AD66EA323B2DE68BFF6DBDCEE7, ___icon_4)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_icon_4() const { return ___icon_4; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_icon_4() { return &___icon_4; }
	inline void set_icon_4(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___icon_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___icon_4), (void*)value);
	}

	inline static int32_t get_offset_of_title_5() { return static_cast<int32_t>(offsetof(Readme_tD58D22679F01C9AD66EA323B2DE68BFF6DBDCEE7, ___title_5)); }
	inline String_t* get_title_5() const { return ___title_5; }
	inline String_t** get_address_of_title_5() { return &___title_5; }
	inline void set_title_5(String_t* value)
	{
		___title_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___title_5), (void*)value);
	}

	inline static int32_t get_offset_of_sections_6() { return static_cast<int32_t>(offsetof(Readme_tD58D22679F01C9AD66EA323B2DE68BFF6DBDCEE7, ___sections_6)); }
	inline SectionU5BU5D_t013C5C59593F9D61D69A33E91B8C1450419ED659* get_sections_6() const { return ___sections_6; }
	inline SectionU5BU5D_t013C5C59593F9D61D69A33E91B8C1450419ED659** get_address_of_sections_6() { return &___sections_6; }
	inline void set_sections_6(SectionU5BU5D_t013C5C59593F9D61D69A33E91B8C1450419ED659* value)
	{
		___sections_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sections_6), (void*)value);
	}

	inline static int32_t get_offset_of_loadedLayout_7() { return static_cast<int32_t>(offsetof(Readme_tD58D22679F01C9AD66EA323B2DE68BFF6DBDCEE7, ___loadedLayout_7)); }
	inline bool get_loadedLayout_7() const { return ___loadedLayout_7; }
	inline bool* get_address_of_loadedLayout_7() { return &___loadedLayout_7; }
	inline void set_loadedLayout_7(bool value)
	{
		___loadedLayout_7 = value;
	}
};


// GameEvents/OnARCameraPoseChangedDelegate
struct OnARCameraPoseChangedDelegate_t1E51F102F4DED797A7CA358A3A0D7D92BB5BE292  : public MulticastDelegate_t
{
public:

public:
};


// GameEvents/OnCoinsCollectedDelegate
struct OnCoinsCollectedDelegate_t73E03EBD90D903F0CEEEB4C5E86860BEF7290B94  : public MulticastDelegate_t
{
public:

public:
};


// GameEvents/OnFadeToBlackDelegate
struct OnFadeToBlackDelegate_tD0C3CF3048E7FD45B6BA5332BDE5BB05FD59FE3F  : public MulticastDelegate_t
{
public:

public:
};


// GameEvents/OnFirstPageTrackedDelegate
struct OnFirstPageTrackedDelegate_tB10EA0A4FC2EDB8A727310557768DA2D7E3E942B  : public MulticastDelegate_t
{
public:

public:
};


// GameEvents/OnFlickableDraggedDelegate
struct OnFlickableDraggedDelegate_tBAF20D4FC47CDFC5EEFDF658277B86EBBFD03E1B  : public MulticastDelegate_t
{
public:

public:
};


// GameEvents/OnFlickableTappedDelegate
struct OnFlickableTappedDelegate_t1C5F3C2CB74A16B63E9997D152CDDFA196465908  : public MulticastDelegate_t
{
public:

public:
};


// GameEvents/OnFlickableThrownDelegate
struct OnFlickableThrownDelegate_tE88AB9432A7582F497E8E34507D71BF606B1C272  : public MulticastDelegate_t
{
public:

public:
};


// GameEvents/OnPageGameBlockedDelegate
struct OnPageGameBlockedDelegate_t3C6BA7A1E6F919151BC27121DD48978603508B60  : public MulticastDelegate_t
{
public:

public:
};


// GameEvents/OnPageGameCompletedDelegate
struct OnPageGameCompletedDelegate_tEC600C09E877109BC0B80DFECFD4F95ABE0F29CB  : public MulticastDelegate_t
{
public:

public:
};


// GameEvents/OnPageGameLookAtRaycastDelegate
struct OnPageGameLookAtRaycastDelegate_t0A2901F0757A4782C71B8715980C933FC6ACEF35  : public MulticastDelegate_t
{
public:

public:
};


// GameEvents/OnPageGameStatusChangedDelegate
struct OnPageGameStatusChangedDelegate_t51DDB305321C9999BC37D3FB2AD067F4D0E5AE8C  : public MulticastDelegate_t
{
public:

public:
};


// GameEvents/OnPageGameTouchRaycastDelegate
struct OnPageGameTouchRaycastDelegate_t79ACF0C496B3D911F73287E3821D3D9DB70F9387  : public MulticastDelegate_t
{
public:

public:
};


// GameEvents/OnSplashWorldEnterDelegate
struct OnSplashWorldEnterDelegate_tE10056481597DD80FAD0E3FF0E0E174EAEE5D7CF  : public MulticastDelegate_t
{
public:

public:
};


// GameEvents/OnSplashWorldExitDelegate
struct OnSplashWorldExitDelegate_tD93D4940EAF1FF4E23A372D4D88439D4273F4557  : public MulticastDelegate_t
{
public:

public:
};


// GameEvents/OnSplashWorldLookAtRaycastDelegate
struct OnSplashWorldLookAtRaycastDelegate_tA4690438A19C04948CA6609796DCAC2EE70BF3B0  : public MulticastDelegate_t
{
public:

public:
};


// GameEvents/OnSplashWorldTouchRaycastDelegate
struct OnSplashWorldTouchRaycastDelegate_t52F1E630AFC268B9D26C5738F63805FF40B0D027  : public MulticastDelegate_t
{
public:

public:
};


// GameEvents/OnStarfishTappedDelegate
struct OnStarfishTappedDelegate_tDEDDAA2572797A17474E1AF26CF4DD06B0C60317  : public MulticastDelegate_t
{
public:

public:
};


// GameEvents/OnTapMoveDelegate
struct OnTapMoveDelegate_t723F9E3AED1681A747EFCEE8EF576177F038188D  : public MulticastDelegate_t
{
public:

public:
};


// GameEvents/OnTapOffDelegate
struct OnTapOffDelegate_t40D5D499048112C9CCF95E6FF8CB13323679CF81  : public MulticastDelegate_t
{
public:

public:
};


// GameEvents/OnTapOnDelegate
struct OnTapOnDelegate_tB00348CD112FB24724A207D1A265741539F5C9A1  : public MulticastDelegate_t
{
public:

public:
};


// GameEvents/OnTotalCoinsUpdatedDelegate
struct OnTotalCoinsUpdatedDelegate_tB29199907D790A984C111DF7FB8A6669C9C4C0A5  : public MulticastDelegate_t
{
public:

public:
};


// LTDescr/ActionMethodDelegate
struct ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05  : public MulticastDelegate_t
{
public:

public:
};


// LTDescr/EaseTypeDelegate
struct EaseTypeDelegate_t0285AB55B28F5B5A6688E966B51FBA837B2E84AF  : public MulticastDelegate_t
{
public:

public:
};


// TestingZLegacy/NextFunc
struct NextFunc_t26C44005D4488D17C5FD4DD6348013E239E566D6  : public MulticastDelegate_t
{
public:

public:
};


// TestingZLegacyExt/NextFunc
struct NextFunc_t9DC9B58570D8EB8797E476A50F92F650E8194840  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// CamFacing
struct CamFacing_tE648CA18E6FFDE13945AE3E9DC611A8E1E5A042A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform CamFacing::cameraToLookAt
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___cameraToLookAt_4;

public:
	inline static int32_t get_offset_of_cameraToLookAt_4() { return static_cast<int32_t>(offsetof(CamFacing_tE648CA18E6FFDE13945AE3E9DC611A8E1E5A042A, ___cameraToLookAt_4)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_cameraToLookAt_4() const { return ___cameraToLookAt_4; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_cameraToLookAt_4() { return &___cameraToLookAt_4; }
	inline void set_cameraToLookAt_4(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___cameraToLookAt_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cameraToLookAt_4), (void*)value);
	}
};


// CanvasSlider
struct CanvasSlider_t2E4DB998D7DB7EDE9365F7E097DBCFAFEC015417  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Video.VideoPlayer CanvasSlider::videoMainMV
	VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86 * ___videoMainMV_4;
	// UnityEngine.UI.Slider CanvasSlider::getSlider
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * ___getSlider_5;
	// System.Boolean CanvasSlider::slide
	bool ___slide_6;

public:
	inline static int32_t get_offset_of_videoMainMV_4() { return static_cast<int32_t>(offsetof(CanvasSlider_t2E4DB998D7DB7EDE9365F7E097DBCFAFEC015417, ___videoMainMV_4)); }
	inline VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86 * get_videoMainMV_4() const { return ___videoMainMV_4; }
	inline VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86 ** get_address_of_videoMainMV_4() { return &___videoMainMV_4; }
	inline void set_videoMainMV_4(VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86 * value)
	{
		___videoMainMV_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___videoMainMV_4), (void*)value);
	}

	inline static int32_t get_offset_of_getSlider_5() { return static_cast<int32_t>(offsetof(CanvasSlider_t2E4DB998D7DB7EDE9365F7E097DBCFAFEC015417, ___getSlider_5)); }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * get_getSlider_5() const { return ___getSlider_5; }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A ** get_address_of_getSlider_5() { return &___getSlider_5; }
	inline void set_getSlider_5(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * value)
	{
		___getSlider_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___getSlider_5), (void*)value);
	}

	inline static int32_t get_offset_of_slide_6() { return static_cast<int32_t>(offsetof(CanvasSlider_t2E4DB998D7DB7EDE9365F7E097DBCFAFEC015417, ___slide_6)); }
	inline bool get_slide_6() const { return ___slide_6; }
	inline bool* get_address_of_slide_6() { return &___slide_6; }
	inline void set_slide_6(bool value)
	{
		___slide_6 = value;
	}
};


// CoinCollector
struct CoinCollector_t3F96B98DD2A68A61E591CC0AE504CCD1CD66BEF8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 CoinCollector::totalCoins
	int32_t ___totalCoins_4;

public:
	inline static int32_t get_offset_of_totalCoins_4() { return static_cast<int32_t>(offsetof(CoinCollector_t3F96B98DD2A68A61E591CC0AE504CCD1CD66BEF8, ___totalCoins_4)); }
	inline int32_t get_totalCoins_4() const { return ___totalCoins_4; }
	inline int32_t* get_address_of_totalCoins_4() { return &___totalCoins_4; }
	inline void set_totalCoins_4(int32_t value)
	{
		___totalCoins_4 = value;
	}
};


// CoinSpin
struct CoinSpin_t0713B96AE94FEA28A43584F9D8F625BD66ADFEAB  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single CoinSpin::spinTime
	float ___spinTime_4;

public:
	inline static int32_t get_offset_of_spinTime_4() { return static_cast<int32_t>(offsetof(CoinSpin_t0713B96AE94FEA28A43584F9D8F625BD66ADFEAB, ___spinTime_4)); }
	inline float get_spinTime_4() const { return ___spinTime_4; }
	inline float* get_address_of_spinTime_4() { return &___spinTime_4; }
	inline void set_spinTime_4(float value)
	{
		___spinTime_4 = value;
	}
};


// Elevate
struct Elevate_t43B06C709340190FFA7E492403BD3F3471A464AE  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single Elevate::mvDuration
	float ___mvDuration_4;
	// System.Single Elevate::elevateDistance
	float ___elevateDistance_5;

public:
	inline static int32_t get_offset_of_mvDuration_4() { return static_cast<int32_t>(offsetof(Elevate_t43B06C709340190FFA7E492403BD3F3471A464AE, ___mvDuration_4)); }
	inline float get_mvDuration_4() const { return ___mvDuration_4; }
	inline float* get_address_of_mvDuration_4() { return &___mvDuration_4; }
	inline void set_mvDuration_4(float value)
	{
		___mvDuration_4 = value;
	}

	inline static int32_t get_offset_of_elevateDistance_5() { return static_cast<int32_t>(offsetof(Elevate_t43B06C709340190FFA7E492403BD3F3471A464AE, ___elevateDistance_5)); }
	inline float get_elevateDistance_5() const { return ___elevateDistance_5; }
	inline float* get_address_of_elevateDistance_5() { return &___elevateDistance_5; }
	inline void set_elevateDistance_5(float value)
	{
		___elevateDistance_5 = value;
	}
};


// ExampleSpline
struct ExampleSpline_tEFB86A02C368FD0E851EE5F9462E4155EA0BA13B  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform[] ExampleSpline::trans
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ___trans_4;
	// LTSpline ExampleSpline::spline
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 * ___spline_5;
	// UnityEngine.GameObject ExampleSpline::ltLogo
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___ltLogo_6;
	// UnityEngine.GameObject ExampleSpline::ltLogo2
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___ltLogo2_7;
	// System.Single ExampleSpline::iter
	float ___iter_8;

public:
	inline static int32_t get_offset_of_trans_4() { return static_cast<int32_t>(offsetof(ExampleSpline_tEFB86A02C368FD0E851EE5F9462E4155EA0BA13B, ___trans_4)); }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* get_trans_4() const { return ___trans_4; }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D** get_address_of_trans_4() { return &___trans_4; }
	inline void set_trans_4(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* value)
	{
		___trans_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trans_4), (void*)value);
	}

	inline static int32_t get_offset_of_spline_5() { return static_cast<int32_t>(offsetof(ExampleSpline_tEFB86A02C368FD0E851EE5F9462E4155EA0BA13B, ___spline_5)); }
	inline LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 * get_spline_5() const { return ___spline_5; }
	inline LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 ** get_address_of_spline_5() { return &___spline_5; }
	inline void set_spline_5(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 * value)
	{
		___spline_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spline_5), (void*)value);
	}

	inline static int32_t get_offset_of_ltLogo_6() { return static_cast<int32_t>(offsetof(ExampleSpline_tEFB86A02C368FD0E851EE5F9462E4155EA0BA13B, ___ltLogo_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_ltLogo_6() const { return ___ltLogo_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_ltLogo_6() { return &___ltLogo_6; }
	inline void set_ltLogo_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___ltLogo_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ltLogo_6), (void*)value);
	}

	inline static int32_t get_offset_of_ltLogo2_7() { return static_cast<int32_t>(offsetof(ExampleSpline_tEFB86A02C368FD0E851EE5F9462E4155EA0BA13B, ___ltLogo2_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_ltLogo2_7() const { return ___ltLogo2_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_ltLogo2_7() { return &___ltLogo2_7; }
	inline void set_ltLogo2_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___ltLogo2_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ltLogo2_7), (void*)value);
	}

	inline static int32_t get_offset_of_iter_8() { return static_cast<int32_t>(offsetof(ExampleSpline_tEFB86A02C368FD0E851EE5F9462E4155EA0BA13B, ___iter_8)); }
	inline float get_iter_8() const { return ___iter_8; }
	inline float* get_address_of_iter_8() { return &___iter_8; }
	inline void set_iter_8(float value)
	{
		___iter_8 = value;
	}
};


// Float
struct Float_t28B5D79DA5B24D79FA3F8F8DC65BCC9791E16307  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Animator Float::Animation
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___Animation_4;
	// System.Single Float::speed
	float ___speed_5;
	// System.Single Float::y
	float ___y_6;

public:
	inline static int32_t get_offset_of_Animation_4() { return static_cast<int32_t>(offsetof(Float_t28B5D79DA5B24D79FA3F8F8DC65BCC9791E16307, ___Animation_4)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_Animation_4() const { return ___Animation_4; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_Animation_4() { return &___Animation_4; }
	inline void set_Animation_4(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___Animation_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Animation_4), (void*)value);
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(Float_t28B5D79DA5B24D79FA3F8F8DC65BCC9791E16307, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_y_6() { return static_cast<int32_t>(offsetof(Float_t28B5D79DA5B24D79FA3F8F8DC65BCC9791E16307, ___y_6)); }
	inline float get_y_6() const { return ___y_6; }
	inline float* get_address_of_y_6() { return &___y_6; }
	inline void set_y_6(float value)
	{
		___y_6 = value;
	}
};


// GameManager
struct GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Camera GameManager::arCamera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___arCamera_4;
	// System.Boolean GameManager::syncingARCamera
	bool ___syncingARCamera_5;
	// UnityEngine.Pose GameManager::cameraStartPose
	Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  ___cameraStartPose_6;
	// UnityEngine.Pose GameManager::cameraCurrentPose
	Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  ___cameraCurrentPose_7;
	// UnityEngine.Pose GameManager::cameraPoseDelta
	Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  ___cameraPoseDelta_8;
	// UnityEngine.Light GameManager::gameLight
	Light_tA2F349FE839781469A0344CF6039B51512394275 * ___gameLight_9;
	// UnityEngine.AudioListener GameManager::arAudioListener
	AudioListener_t03B51B434A263F9AFD07AC8AA5CB4FE6402252A3 * ___arAudioListener_10;
	// PageGame GameManager::<CurrentPageGame>k__BackingField
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5 * ___U3CCurrentPageGameU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_arCamera_4() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___arCamera_4)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_arCamera_4() const { return ___arCamera_4; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_arCamera_4() { return &___arCamera_4; }
	inline void set_arCamera_4(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___arCamera_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arCamera_4), (void*)value);
	}

	inline static int32_t get_offset_of_syncingARCamera_5() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___syncingARCamera_5)); }
	inline bool get_syncingARCamera_5() const { return ___syncingARCamera_5; }
	inline bool* get_address_of_syncingARCamera_5() { return &___syncingARCamera_5; }
	inline void set_syncingARCamera_5(bool value)
	{
		___syncingARCamera_5 = value;
	}

	inline static int32_t get_offset_of_cameraStartPose_6() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___cameraStartPose_6)); }
	inline Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  get_cameraStartPose_6() const { return ___cameraStartPose_6; }
	inline Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A * get_address_of_cameraStartPose_6() { return &___cameraStartPose_6; }
	inline void set_cameraStartPose_6(Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  value)
	{
		___cameraStartPose_6 = value;
	}

	inline static int32_t get_offset_of_cameraCurrentPose_7() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___cameraCurrentPose_7)); }
	inline Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  get_cameraCurrentPose_7() const { return ___cameraCurrentPose_7; }
	inline Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A * get_address_of_cameraCurrentPose_7() { return &___cameraCurrentPose_7; }
	inline void set_cameraCurrentPose_7(Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  value)
	{
		___cameraCurrentPose_7 = value;
	}

	inline static int32_t get_offset_of_cameraPoseDelta_8() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___cameraPoseDelta_8)); }
	inline Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  get_cameraPoseDelta_8() const { return ___cameraPoseDelta_8; }
	inline Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A * get_address_of_cameraPoseDelta_8() { return &___cameraPoseDelta_8; }
	inline void set_cameraPoseDelta_8(Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  value)
	{
		___cameraPoseDelta_8 = value;
	}

	inline static int32_t get_offset_of_gameLight_9() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___gameLight_9)); }
	inline Light_tA2F349FE839781469A0344CF6039B51512394275 * get_gameLight_9() const { return ___gameLight_9; }
	inline Light_tA2F349FE839781469A0344CF6039B51512394275 ** get_address_of_gameLight_9() { return &___gameLight_9; }
	inline void set_gameLight_9(Light_tA2F349FE839781469A0344CF6039B51512394275 * value)
	{
		___gameLight_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gameLight_9), (void*)value);
	}

	inline static int32_t get_offset_of_arAudioListener_10() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___arAudioListener_10)); }
	inline AudioListener_t03B51B434A263F9AFD07AC8AA5CB4FE6402252A3 * get_arAudioListener_10() const { return ___arAudioListener_10; }
	inline AudioListener_t03B51B434A263F9AFD07AC8AA5CB4FE6402252A3 ** get_address_of_arAudioListener_10() { return &___arAudioListener_10; }
	inline void set_arAudioListener_10(AudioListener_t03B51B434A263F9AFD07AC8AA5CB4FE6402252A3 * value)
	{
		___arAudioListener_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arAudioListener_10), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCurrentPageGameU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___U3CCurrentPageGameU3Ek__BackingField_11)); }
	inline PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5 * get_U3CCurrentPageGameU3Ek__BackingField_11() const { return ___U3CCurrentPageGameU3Ek__BackingField_11; }
	inline PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5 ** get_address_of_U3CCurrentPageGameU3Ek__BackingField_11() { return &___U3CCurrentPageGameU3Ek__BackingField_11; }
	inline void set_U3CCurrentPageGameU3Ek__BackingField_11(PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5 * value)
	{
		___U3CCurrentPageGameU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCurrentPageGameU3Ek__BackingField_11), (void*)value);
	}
};


// GameScale
struct GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Vector3 GameScale::transformScale
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___transformScale_4;
	// System.Single GameScale::minTime
	float ___minTime_5;
	// System.Single GameScale::maxTime
	float ___maxTime_6;
	// System.Single GameScale::scaleTime
	float ___scaleTime_7;
	// System.Boolean GameScale::scaleObject
	bool ___scaleObject_8;
	// GameScale/ScaleStatus GameScale::<CurrentStatus>k__BackingField
	int32_t ___U3CCurrentStatusU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_transformScale_4() { return static_cast<int32_t>(offsetof(GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607, ___transformScale_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_transformScale_4() const { return ___transformScale_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_transformScale_4() { return &___transformScale_4; }
	inline void set_transformScale_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___transformScale_4 = value;
	}

	inline static int32_t get_offset_of_minTime_5() { return static_cast<int32_t>(offsetof(GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607, ___minTime_5)); }
	inline float get_minTime_5() const { return ___minTime_5; }
	inline float* get_address_of_minTime_5() { return &___minTime_5; }
	inline void set_minTime_5(float value)
	{
		___minTime_5 = value;
	}

	inline static int32_t get_offset_of_maxTime_6() { return static_cast<int32_t>(offsetof(GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607, ___maxTime_6)); }
	inline float get_maxTime_6() const { return ___maxTime_6; }
	inline float* get_address_of_maxTime_6() { return &___maxTime_6; }
	inline void set_maxTime_6(float value)
	{
		___maxTime_6 = value;
	}

	inline static int32_t get_offset_of_scaleTime_7() { return static_cast<int32_t>(offsetof(GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607, ___scaleTime_7)); }
	inline float get_scaleTime_7() const { return ___scaleTime_7; }
	inline float* get_address_of_scaleTime_7() { return &___scaleTime_7; }
	inline void set_scaleTime_7(float value)
	{
		___scaleTime_7 = value;
	}

	inline static int32_t get_offset_of_scaleObject_8() { return static_cast<int32_t>(offsetof(GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607, ___scaleObject_8)); }
	inline bool get_scaleObject_8() const { return ___scaleObject_8; }
	inline bool* get_address_of_scaleObject_8() { return &___scaleObject_8; }
	inline void set_scaleObject_8(bool value)
	{
		___scaleObject_8 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentStatusU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607, ___U3CCurrentStatusU3Ek__BackingField_9)); }
	inline int32_t get_U3CCurrentStatusU3Ek__BackingField_9() const { return ___U3CCurrentStatusU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CCurrentStatusU3Ek__BackingField_9() { return &___U3CCurrentStatusU3Ek__BackingField_9; }
	inline void set_U3CCurrentStatusU3Ek__BackingField_9(int32_t value)
	{
		___U3CCurrentStatusU3Ek__BackingField_9 = value;
	}
};


// Idle
struct Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Collections.IEnumerator Idle::coroutine
	RuntimeObject* ___coroutine_4;
	// System.Collections.IEnumerator Idle::coroutine2
	RuntimeObject* ___coroutine2_5;
	// UnityEngine.GameObject Idle::particle
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___particle_6;
	// UnityEngine.GameObject Idle::pointer
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___pointer_7;
	// System.Boolean Idle::showPointer
	bool ___showPointer_8;

public:
	inline static int32_t get_offset_of_coroutine_4() { return static_cast<int32_t>(offsetof(Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867, ___coroutine_4)); }
	inline RuntimeObject* get_coroutine_4() const { return ___coroutine_4; }
	inline RuntimeObject** get_address_of_coroutine_4() { return &___coroutine_4; }
	inline void set_coroutine_4(RuntimeObject* value)
	{
		___coroutine_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___coroutine_4), (void*)value);
	}

	inline static int32_t get_offset_of_coroutine2_5() { return static_cast<int32_t>(offsetof(Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867, ___coroutine2_5)); }
	inline RuntimeObject* get_coroutine2_5() const { return ___coroutine2_5; }
	inline RuntimeObject** get_address_of_coroutine2_5() { return &___coroutine2_5; }
	inline void set_coroutine2_5(RuntimeObject* value)
	{
		___coroutine2_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___coroutine2_5), (void*)value);
	}

	inline static int32_t get_offset_of_particle_6() { return static_cast<int32_t>(offsetof(Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867, ___particle_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_particle_6() const { return ___particle_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_particle_6() { return &___particle_6; }
	inline void set_particle_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___particle_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___particle_6), (void*)value);
	}

	inline static int32_t get_offset_of_pointer_7() { return static_cast<int32_t>(offsetof(Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867, ___pointer_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_pointer_7() const { return ___pointer_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_pointer_7() { return &___pointer_7; }
	inline void set_pointer_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___pointer_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointer_7), (void*)value);
	}

	inline static int32_t get_offset_of_showPointer_8() { return static_cast<int32_t>(offsetof(Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867, ___showPointer_8)); }
	inline bool get_showPointer_8() const { return ___showPointer_8; }
	inline bool* get_address_of_showPointer_8() { return &___showPointer_8; }
	inline void set_showPointer_8(bool value)
	{
		___showPointer_8 = value;
	}
};


// ImageRecognition
struct ImageRecognition_t3FA1D134672565B626D02FABA8BCB9732ACFAC23  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject[] ImageRecognition::bookPages
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___bookPages_4;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> ImageRecognition::spawnedPrefabs
	Dictionary_2_tADDBF4C67A82C92CD16099CD166D7F72E8DCB13C * ___spawnedPrefabs_5;
	// UnityEngine.XR.ARFoundation.ARTrackedImageManager ImageRecognition::m_trackedImageManager
	ARTrackedImageManager_tB916E34D053E6712190F2BAE46E21D76A0882FF2 * ___m_trackedImageManager_6;
	// UnityEngine.XR.ARFoundation.ARAnchorManager ImageRecognition::m_AnchorManager
	ARAnchorManager_t969330AB785F0DC41EF9F4390D77F7ABA30F7D0F * ___m_AnchorManager_7;
	// UnityEngine.XR.ARFoundation.ARRaycastManager ImageRecognition::m_RaycastManager
	ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * ___m_RaycastManager_8;

public:
	inline static int32_t get_offset_of_bookPages_4() { return static_cast<int32_t>(offsetof(ImageRecognition_t3FA1D134672565B626D02FABA8BCB9732ACFAC23, ___bookPages_4)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_bookPages_4() const { return ___bookPages_4; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_bookPages_4() { return &___bookPages_4; }
	inline void set_bookPages_4(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___bookPages_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bookPages_4), (void*)value);
	}

	inline static int32_t get_offset_of_spawnedPrefabs_5() { return static_cast<int32_t>(offsetof(ImageRecognition_t3FA1D134672565B626D02FABA8BCB9732ACFAC23, ___spawnedPrefabs_5)); }
	inline Dictionary_2_tADDBF4C67A82C92CD16099CD166D7F72E8DCB13C * get_spawnedPrefabs_5() const { return ___spawnedPrefabs_5; }
	inline Dictionary_2_tADDBF4C67A82C92CD16099CD166D7F72E8DCB13C ** get_address_of_spawnedPrefabs_5() { return &___spawnedPrefabs_5; }
	inline void set_spawnedPrefabs_5(Dictionary_2_tADDBF4C67A82C92CD16099CD166D7F72E8DCB13C * value)
	{
		___spawnedPrefabs_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spawnedPrefabs_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_trackedImageManager_6() { return static_cast<int32_t>(offsetof(ImageRecognition_t3FA1D134672565B626D02FABA8BCB9732ACFAC23, ___m_trackedImageManager_6)); }
	inline ARTrackedImageManager_tB916E34D053E6712190F2BAE46E21D76A0882FF2 * get_m_trackedImageManager_6() const { return ___m_trackedImageManager_6; }
	inline ARTrackedImageManager_tB916E34D053E6712190F2BAE46E21D76A0882FF2 ** get_address_of_m_trackedImageManager_6() { return &___m_trackedImageManager_6; }
	inline void set_m_trackedImageManager_6(ARTrackedImageManager_tB916E34D053E6712190F2BAE46E21D76A0882FF2 * value)
	{
		___m_trackedImageManager_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_trackedImageManager_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_AnchorManager_7() { return static_cast<int32_t>(offsetof(ImageRecognition_t3FA1D134672565B626D02FABA8BCB9732ACFAC23, ___m_AnchorManager_7)); }
	inline ARAnchorManager_t969330AB785F0DC41EF9F4390D77F7ABA30F7D0F * get_m_AnchorManager_7() const { return ___m_AnchorManager_7; }
	inline ARAnchorManager_t969330AB785F0DC41EF9F4390D77F7ABA30F7D0F ** get_address_of_m_AnchorManager_7() { return &___m_AnchorManager_7; }
	inline void set_m_AnchorManager_7(ARAnchorManager_t969330AB785F0DC41EF9F4390D77F7ABA30F7D0F * value)
	{
		___m_AnchorManager_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AnchorManager_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_RaycastManager_8() { return static_cast<int32_t>(offsetof(ImageRecognition_t3FA1D134672565B626D02FABA8BCB9732ACFAC23, ___m_RaycastManager_8)); }
	inline ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * get_m_RaycastManager_8() const { return ___m_RaycastManager_8; }
	inline ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F ** get_address_of_m_RaycastManager_8() { return &___m_RaycastManager_8; }
	inline void set_m_RaycastManager_8(ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * value)
	{
		___m_RaycastManager_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RaycastManager_8), (void*)value);
	}
};


// InputManager
struct InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Vector2 InputManager::touchStartPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___touchStartPosition_4;
	// System.DateTime InputManager::touchStartTime
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___touchStartTime_5;

public:
	inline static int32_t get_offset_of_touchStartPosition_4() { return static_cast<int32_t>(offsetof(InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A, ___touchStartPosition_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_touchStartPosition_4() const { return ___touchStartPosition_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_touchStartPosition_4() { return &___touchStartPosition_4; }
	inline void set_touchStartPosition_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___touchStartPosition_4 = value;
	}

	inline static int32_t get_offset_of_touchStartTime_5() { return static_cast<int32_t>(offsetof(InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A, ___touchStartTime_5)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_touchStartTime_5() const { return ___touchStartTime_5; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_touchStartTime_5() { return &___touchStartTime_5; }
	inline void set_touchStartTime_5(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___touchStartTime_5 = value;
	}
};


// LeanTester
struct LeanTester_t537C3FE41ED244252241E5B02F6B1BA6DA437297  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single LeanTester::timeout
	float ___timeout_4;

public:
	inline static int32_t get_offset_of_timeout_4() { return static_cast<int32_t>(offsetof(LeanTester_t537C3FE41ED244252241E5B02F6B1BA6DA437297, ___timeout_4)); }
	inline float get_timeout_4() const { return ___timeout_4; }
	inline float* get_address_of_timeout_4() { return &___timeout_4; }
	inline void set_timeout_4(float value)
	{
		___timeout_4 = value;
	}
};


// LeanTween
struct LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};

struct LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields
{
public:
	// System.Boolean LeanTween::throwErrors
	bool ___throwErrors_4;
	// System.Single LeanTween::tau
	float ___tau_5;
	// System.Single LeanTween::PI_DIV2
	float ___PI_DIV2_6;
	// LTSeq[] LeanTween::sequences
	LTSeqU5BU5D_t5C740B5E327381CE2E6FD41B011EA9A72355FD51* ___sequences_7;
	// LTDescr[] LeanTween::tweens
	LTDescrU5BU5D_t731554F94C09A2A31CB19EA8663E934537DF797F* ___tweens_8;
	// System.Int32[] LeanTween::tweensFinished
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___tweensFinished_9;
	// System.Int32[] LeanTween::tweensFinishedIds
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___tweensFinishedIds_10;
	// LTDescr LeanTween::tween
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * ___tween_11;
	// System.Int32 LeanTween::tweenMaxSearch
	int32_t ___tweenMaxSearch_12;
	// System.Int32 LeanTween::maxTweens
	int32_t ___maxTweens_13;
	// System.Int32 LeanTween::maxSequences
	int32_t ___maxSequences_14;
	// System.Int32 LeanTween::frameRendered
	int32_t ___frameRendered_15;
	// UnityEngine.GameObject LeanTween::_tweenEmpty
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____tweenEmpty_16;
	// System.Single LeanTween::dtEstimated
	float ___dtEstimated_17;
	// System.Single LeanTween::dtManual
	float ___dtManual_18;
	// System.Single LeanTween::dtActual
	float ___dtActual_19;
	// System.UInt32 LeanTween::global_counter
	uint32_t ___global_counter_20;
	// System.Int32 LeanTween::i
	int32_t ___i_21;
	// System.Int32 LeanTween::j
	int32_t ___j_22;
	// System.Int32 LeanTween::finishedCnt
	int32_t ___finishedCnt_23;
	// UnityEngine.AnimationCurve LeanTween::punch
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___punch_24;
	// UnityEngine.AnimationCurve LeanTween::shake
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shake_25;
	// System.Int32 LeanTween::maxTweenReached
	int32_t ___maxTweenReached_26;
	// System.Int32 LeanTween::startSearch
	int32_t ___startSearch_27;
	// LTDescr LeanTween::d
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * ___d_28;
	// System.Action`1<LTEvent>[] LeanTween::eventListeners
	Action_1U5BU5D_t930658157BBD9409C35E85E5A34B23FC4EEF0772* ___eventListeners_29;
	// UnityEngine.GameObject[] LeanTween::goListeners
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___goListeners_30;
	// System.Int32 LeanTween::eventsMaxSearch
	int32_t ___eventsMaxSearch_31;
	// System.Int32 LeanTween::EVENTS_MAX
	int32_t ___EVENTS_MAX_32;
	// System.Int32 LeanTween::LISTENERS_MAX
	int32_t ___LISTENERS_MAX_33;
	// System.Int32 LeanTween::INIT_LISTENERS_MAX
	int32_t ___INIT_LISTENERS_MAX_34;

public:
	inline static int32_t get_offset_of_throwErrors_4() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___throwErrors_4)); }
	inline bool get_throwErrors_4() const { return ___throwErrors_4; }
	inline bool* get_address_of_throwErrors_4() { return &___throwErrors_4; }
	inline void set_throwErrors_4(bool value)
	{
		___throwErrors_4 = value;
	}

	inline static int32_t get_offset_of_tau_5() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___tau_5)); }
	inline float get_tau_5() const { return ___tau_5; }
	inline float* get_address_of_tau_5() { return &___tau_5; }
	inline void set_tau_5(float value)
	{
		___tau_5 = value;
	}

	inline static int32_t get_offset_of_PI_DIV2_6() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___PI_DIV2_6)); }
	inline float get_PI_DIV2_6() const { return ___PI_DIV2_6; }
	inline float* get_address_of_PI_DIV2_6() { return &___PI_DIV2_6; }
	inline void set_PI_DIV2_6(float value)
	{
		___PI_DIV2_6 = value;
	}

	inline static int32_t get_offset_of_sequences_7() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___sequences_7)); }
	inline LTSeqU5BU5D_t5C740B5E327381CE2E6FD41B011EA9A72355FD51* get_sequences_7() const { return ___sequences_7; }
	inline LTSeqU5BU5D_t5C740B5E327381CE2E6FD41B011EA9A72355FD51** get_address_of_sequences_7() { return &___sequences_7; }
	inline void set_sequences_7(LTSeqU5BU5D_t5C740B5E327381CE2E6FD41B011EA9A72355FD51* value)
	{
		___sequences_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sequences_7), (void*)value);
	}

	inline static int32_t get_offset_of_tweens_8() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___tweens_8)); }
	inline LTDescrU5BU5D_t731554F94C09A2A31CB19EA8663E934537DF797F* get_tweens_8() const { return ___tweens_8; }
	inline LTDescrU5BU5D_t731554F94C09A2A31CB19EA8663E934537DF797F** get_address_of_tweens_8() { return &___tweens_8; }
	inline void set_tweens_8(LTDescrU5BU5D_t731554F94C09A2A31CB19EA8663E934537DF797F* value)
	{
		___tweens_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tweens_8), (void*)value);
	}

	inline static int32_t get_offset_of_tweensFinished_9() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___tweensFinished_9)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_tweensFinished_9() const { return ___tweensFinished_9; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_tweensFinished_9() { return &___tweensFinished_9; }
	inline void set_tweensFinished_9(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___tweensFinished_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tweensFinished_9), (void*)value);
	}

	inline static int32_t get_offset_of_tweensFinishedIds_10() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___tweensFinishedIds_10)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_tweensFinishedIds_10() const { return ___tweensFinishedIds_10; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_tweensFinishedIds_10() { return &___tweensFinishedIds_10; }
	inline void set_tweensFinishedIds_10(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___tweensFinishedIds_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tweensFinishedIds_10), (void*)value);
	}

	inline static int32_t get_offset_of_tween_11() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___tween_11)); }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * get_tween_11() const { return ___tween_11; }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F ** get_address_of_tween_11() { return &___tween_11; }
	inline void set_tween_11(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * value)
	{
		___tween_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tween_11), (void*)value);
	}

	inline static int32_t get_offset_of_tweenMaxSearch_12() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___tweenMaxSearch_12)); }
	inline int32_t get_tweenMaxSearch_12() const { return ___tweenMaxSearch_12; }
	inline int32_t* get_address_of_tweenMaxSearch_12() { return &___tweenMaxSearch_12; }
	inline void set_tweenMaxSearch_12(int32_t value)
	{
		___tweenMaxSearch_12 = value;
	}

	inline static int32_t get_offset_of_maxTweens_13() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___maxTweens_13)); }
	inline int32_t get_maxTweens_13() const { return ___maxTweens_13; }
	inline int32_t* get_address_of_maxTweens_13() { return &___maxTweens_13; }
	inline void set_maxTweens_13(int32_t value)
	{
		___maxTweens_13 = value;
	}

	inline static int32_t get_offset_of_maxSequences_14() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___maxSequences_14)); }
	inline int32_t get_maxSequences_14() const { return ___maxSequences_14; }
	inline int32_t* get_address_of_maxSequences_14() { return &___maxSequences_14; }
	inline void set_maxSequences_14(int32_t value)
	{
		___maxSequences_14 = value;
	}

	inline static int32_t get_offset_of_frameRendered_15() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___frameRendered_15)); }
	inline int32_t get_frameRendered_15() const { return ___frameRendered_15; }
	inline int32_t* get_address_of_frameRendered_15() { return &___frameRendered_15; }
	inline void set_frameRendered_15(int32_t value)
	{
		___frameRendered_15 = value;
	}

	inline static int32_t get_offset_of__tweenEmpty_16() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ____tweenEmpty_16)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__tweenEmpty_16() const { return ____tweenEmpty_16; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__tweenEmpty_16() { return &____tweenEmpty_16; }
	inline void set__tweenEmpty_16(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____tweenEmpty_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____tweenEmpty_16), (void*)value);
	}

	inline static int32_t get_offset_of_dtEstimated_17() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___dtEstimated_17)); }
	inline float get_dtEstimated_17() const { return ___dtEstimated_17; }
	inline float* get_address_of_dtEstimated_17() { return &___dtEstimated_17; }
	inline void set_dtEstimated_17(float value)
	{
		___dtEstimated_17 = value;
	}

	inline static int32_t get_offset_of_dtManual_18() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___dtManual_18)); }
	inline float get_dtManual_18() const { return ___dtManual_18; }
	inline float* get_address_of_dtManual_18() { return &___dtManual_18; }
	inline void set_dtManual_18(float value)
	{
		___dtManual_18 = value;
	}

	inline static int32_t get_offset_of_dtActual_19() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___dtActual_19)); }
	inline float get_dtActual_19() const { return ___dtActual_19; }
	inline float* get_address_of_dtActual_19() { return &___dtActual_19; }
	inline void set_dtActual_19(float value)
	{
		___dtActual_19 = value;
	}

	inline static int32_t get_offset_of_global_counter_20() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___global_counter_20)); }
	inline uint32_t get_global_counter_20() const { return ___global_counter_20; }
	inline uint32_t* get_address_of_global_counter_20() { return &___global_counter_20; }
	inline void set_global_counter_20(uint32_t value)
	{
		___global_counter_20 = value;
	}

	inline static int32_t get_offset_of_i_21() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___i_21)); }
	inline int32_t get_i_21() const { return ___i_21; }
	inline int32_t* get_address_of_i_21() { return &___i_21; }
	inline void set_i_21(int32_t value)
	{
		___i_21 = value;
	}

	inline static int32_t get_offset_of_j_22() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___j_22)); }
	inline int32_t get_j_22() const { return ___j_22; }
	inline int32_t* get_address_of_j_22() { return &___j_22; }
	inline void set_j_22(int32_t value)
	{
		___j_22 = value;
	}

	inline static int32_t get_offset_of_finishedCnt_23() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___finishedCnt_23)); }
	inline int32_t get_finishedCnt_23() const { return ___finishedCnt_23; }
	inline int32_t* get_address_of_finishedCnt_23() { return &___finishedCnt_23; }
	inline void set_finishedCnt_23(int32_t value)
	{
		___finishedCnt_23 = value;
	}

	inline static int32_t get_offset_of_punch_24() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___punch_24)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_punch_24() const { return ___punch_24; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_punch_24() { return &___punch_24; }
	inline void set_punch_24(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___punch_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___punch_24), (void*)value);
	}

	inline static int32_t get_offset_of_shake_25() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___shake_25)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_shake_25() const { return ___shake_25; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_shake_25() { return &___shake_25; }
	inline void set_shake_25(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___shake_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shake_25), (void*)value);
	}

	inline static int32_t get_offset_of_maxTweenReached_26() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___maxTweenReached_26)); }
	inline int32_t get_maxTweenReached_26() const { return ___maxTweenReached_26; }
	inline int32_t* get_address_of_maxTweenReached_26() { return &___maxTweenReached_26; }
	inline void set_maxTweenReached_26(int32_t value)
	{
		___maxTweenReached_26 = value;
	}

	inline static int32_t get_offset_of_startSearch_27() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___startSearch_27)); }
	inline int32_t get_startSearch_27() const { return ___startSearch_27; }
	inline int32_t* get_address_of_startSearch_27() { return &___startSearch_27; }
	inline void set_startSearch_27(int32_t value)
	{
		___startSearch_27 = value;
	}

	inline static int32_t get_offset_of_d_28() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___d_28)); }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * get_d_28() const { return ___d_28; }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F ** get_address_of_d_28() { return &___d_28; }
	inline void set_d_28(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * value)
	{
		___d_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___d_28), (void*)value);
	}

	inline static int32_t get_offset_of_eventListeners_29() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___eventListeners_29)); }
	inline Action_1U5BU5D_t930658157BBD9409C35E85E5A34B23FC4EEF0772* get_eventListeners_29() const { return ___eventListeners_29; }
	inline Action_1U5BU5D_t930658157BBD9409C35E85E5A34B23FC4EEF0772** get_address_of_eventListeners_29() { return &___eventListeners_29; }
	inline void set_eventListeners_29(Action_1U5BU5D_t930658157BBD9409C35E85E5A34B23FC4EEF0772* value)
	{
		___eventListeners_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eventListeners_29), (void*)value);
	}

	inline static int32_t get_offset_of_goListeners_30() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___goListeners_30)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_goListeners_30() const { return ___goListeners_30; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_goListeners_30() { return &___goListeners_30; }
	inline void set_goListeners_30(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___goListeners_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___goListeners_30), (void*)value);
	}

	inline static int32_t get_offset_of_eventsMaxSearch_31() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___eventsMaxSearch_31)); }
	inline int32_t get_eventsMaxSearch_31() const { return ___eventsMaxSearch_31; }
	inline int32_t* get_address_of_eventsMaxSearch_31() { return &___eventsMaxSearch_31; }
	inline void set_eventsMaxSearch_31(int32_t value)
	{
		___eventsMaxSearch_31 = value;
	}

	inline static int32_t get_offset_of_EVENTS_MAX_32() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___EVENTS_MAX_32)); }
	inline int32_t get_EVENTS_MAX_32() const { return ___EVENTS_MAX_32; }
	inline int32_t* get_address_of_EVENTS_MAX_32() { return &___EVENTS_MAX_32; }
	inline void set_EVENTS_MAX_32(int32_t value)
	{
		___EVENTS_MAX_32 = value;
	}

	inline static int32_t get_offset_of_LISTENERS_MAX_33() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___LISTENERS_MAX_33)); }
	inline int32_t get_LISTENERS_MAX_33() const { return ___LISTENERS_MAX_33; }
	inline int32_t* get_address_of_LISTENERS_MAX_33() { return &___LISTENERS_MAX_33; }
	inline void set_LISTENERS_MAX_33(int32_t value)
	{
		___LISTENERS_MAX_33 = value;
	}

	inline static int32_t get_offset_of_INIT_LISTENERS_MAX_34() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___INIT_LISTENERS_MAX_34)); }
	inline int32_t get_INIT_LISTENERS_MAX_34() const { return ___INIT_LISTENERS_MAX_34; }
	inline int32_t* get_address_of_INIT_LISTENERS_MAX_34() { return &___INIT_LISTENERS_MAX_34; }
	inline void set_INIT_LISTENERS_MAX_34(int32_t value)
	{
		___INIT_LISTENERS_MAX_34 = value;
	}
};


// LogoCinematic
struct LogoCinematic_t81BA1CD58574F59063CB8A3DB8ED9353EC8AC998  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject LogoCinematic::lean
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___lean_4;
	// UnityEngine.GameObject LogoCinematic::tween
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___tween_5;

public:
	inline static int32_t get_offset_of_lean_4() { return static_cast<int32_t>(offsetof(LogoCinematic_t81BA1CD58574F59063CB8A3DB8ED9353EC8AC998, ___lean_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_lean_4() const { return ___lean_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_lean_4() { return &___lean_4; }
	inline void set_lean_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___lean_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lean_4), (void*)value);
	}

	inline static int32_t get_offset_of_tween_5() { return static_cast<int32_t>(offsetof(LogoCinematic_t81BA1CD58574F59063CB8A3DB8ED9353EC8AC998, ___tween_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_tween_5() const { return ___tween_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_tween_5() { return &___tween_5; }
	inline void set_tween_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___tween_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tween_5), (void*)value);
	}
};


// MV
struct MV_t50F4510A4AF6B71BA52F27D2ECFB44FBAD8A37B7  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Video.VideoPlayer MV::videoMV
	VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86 * ___videoMV_4;
	// UnityEngine.GameObject MV::toEnable
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___toEnable_5;
	// UnityEngine.GameObject MV::toEnable2
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___toEnable2_6;
	// UnityEngine.GameObject MV::toDisable
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___toDisable_7;
	// UnityEngine.AudioSource MV::gameSFX
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___gameSFX_8;
	// UnityEngine.AudioSource MV::gameMusic
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___gameMusic_9;

public:
	inline static int32_t get_offset_of_videoMV_4() { return static_cast<int32_t>(offsetof(MV_t50F4510A4AF6B71BA52F27D2ECFB44FBAD8A37B7, ___videoMV_4)); }
	inline VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86 * get_videoMV_4() const { return ___videoMV_4; }
	inline VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86 ** get_address_of_videoMV_4() { return &___videoMV_4; }
	inline void set_videoMV_4(VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86 * value)
	{
		___videoMV_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___videoMV_4), (void*)value);
	}

	inline static int32_t get_offset_of_toEnable_5() { return static_cast<int32_t>(offsetof(MV_t50F4510A4AF6B71BA52F27D2ECFB44FBAD8A37B7, ___toEnable_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_toEnable_5() const { return ___toEnable_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_toEnable_5() { return &___toEnable_5; }
	inline void set_toEnable_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___toEnable_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___toEnable_5), (void*)value);
	}

	inline static int32_t get_offset_of_toEnable2_6() { return static_cast<int32_t>(offsetof(MV_t50F4510A4AF6B71BA52F27D2ECFB44FBAD8A37B7, ___toEnable2_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_toEnable2_6() const { return ___toEnable2_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_toEnable2_6() { return &___toEnable2_6; }
	inline void set_toEnable2_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___toEnable2_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___toEnable2_6), (void*)value);
	}

	inline static int32_t get_offset_of_toDisable_7() { return static_cast<int32_t>(offsetof(MV_t50F4510A4AF6B71BA52F27D2ECFB44FBAD8A37B7, ___toDisable_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_toDisable_7() const { return ___toDisable_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_toDisable_7() { return &___toDisable_7; }
	inline void set_toDisable_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___toDisable_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___toDisable_7), (void*)value);
	}

	inline static int32_t get_offset_of_gameSFX_8() { return static_cast<int32_t>(offsetof(MV_t50F4510A4AF6B71BA52F27D2ECFB44FBAD8A37B7, ___gameSFX_8)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_gameSFX_8() const { return ___gameSFX_8; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_gameSFX_8() { return &___gameSFX_8; }
	inline void set_gameSFX_8(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___gameSFX_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gameSFX_8), (void*)value);
	}

	inline static int32_t get_offset_of_gameMusic_9() { return static_cast<int32_t>(offsetof(MV_t50F4510A4AF6B71BA52F27D2ECFB44FBAD8A37B7, ___gameMusic_9)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_gameMusic_9() const { return ___gameMusic_9; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_gameMusic_9() { return &___gameMusic_9; }
	inline void set_gameMusic_9(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___gameMusic_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gameMusic_9), (void*)value);
	}
};


// OpenerVideo
struct OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Video.VideoPlayer OpenerVideo::canvasVideo
	VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86 * ___canvasVideo_4;
	// UnityEngine.GameObject OpenerVideo::toDisable
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___toDisable_5;
	// UnityEngine.GameObject OpenerVideo::toEnable
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___toEnable_6;
	// UnityEngine.UI.Slider OpenerVideo::getSlider
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * ___getSlider_7;
	// UnityEngine.AudioSource OpenerVideo::stopMusic
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___stopMusic_8;
	// UnityEngine.AudioSource OpenerVideo::playMusic
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___playMusic_9;
	// System.Boolean OpenerVideo::slide
	bool ___slide_10;

public:
	inline static int32_t get_offset_of_canvasVideo_4() { return static_cast<int32_t>(offsetof(OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7, ___canvasVideo_4)); }
	inline VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86 * get_canvasVideo_4() const { return ___canvasVideo_4; }
	inline VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86 ** get_address_of_canvasVideo_4() { return &___canvasVideo_4; }
	inline void set_canvasVideo_4(VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86 * value)
	{
		___canvasVideo_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvasVideo_4), (void*)value);
	}

	inline static int32_t get_offset_of_toDisable_5() { return static_cast<int32_t>(offsetof(OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7, ___toDisable_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_toDisable_5() const { return ___toDisable_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_toDisable_5() { return &___toDisable_5; }
	inline void set_toDisable_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___toDisable_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___toDisable_5), (void*)value);
	}

	inline static int32_t get_offset_of_toEnable_6() { return static_cast<int32_t>(offsetof(OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7, ___toEnable_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_toEnable_6() const { return ___toEnable_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_toEnable_6() { return &___toEnable_6; }
	inline void set_toEnable_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___toEnable_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___toEnable_6), (void*)value);
	}

	inline static int32_t get_offset_of_getSlider_7() { return static_cast<int32_t>(offsetof(OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7, ___getSlider_7)); }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * get_getSlider_7() const { return ___getSlider_7; }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A ** get_address_of_getSlider_7() { return &___getSlider_7; }
	inline void set_getSlider_7(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * value)
	{
		___getSlider_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___getSlider_7), (void*)value);
	}

	inline static int32_t get_offset_of_stopMusic_8() { return static_cast<int32_t>(offsetof(OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7, ___stopMusic_8)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_stopMusic_8() const { return ___stopMusic_8; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_stopMusic_8() { return &___stopMusic_8; }
	inline void set_stopMusic_8(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___stopMusic_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stopMusic_8), (void*)value);
	}

	inline static int32_t get_offset_of_playMusic_9() { return static_cast<int32_t>(offsetof(OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7, ___playMusic_9)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_playMusic_9() const { return ___playMusic_9; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_playMusic_9() { return &___playMusic_9; }
	inline void set_playMusic_9(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___playMusic_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playMusic_9), (void*)value);
	}

	inline static int32_t get_offset_of_slide_10() { return static_cast<int32_t>(offsetof(OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7, ___slide_10)); }
	inline bool get_slide_10() const { return ___slide_10; }
	inline bool* get_address_of_slide_10() { return &___slide_10; }
	inline void set_slide_10(bool value)
	{
		___slide_10 = value;
	}
};


// OrbTap
struct OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 OrbTap::coinCollectedOnTap
	int32_t ___coinCollectedOnTap_4;
	// UnityEngine.AudioSource OrbTap::orbTapVO
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___orbTapVO_5;
	// UnityEngine.GameObject OrbTap::colliderName
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___colliderName_6;
	// UnityEngine.Animator OrbTap::storyAnimation
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___storyAnimation_7;
	// UnityEngine.Animator OrbTap::splashAnimation
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___splashAnimation_8;
	// UnityEngine.AudioSource OrbTap::storyTapVO
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___storyTapVO_9;
	// UnityEngine.AudioSource OrbTap::splashTapVO
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___splashTapVO_10;

public:
	inline static int32_t get_offset_of_coinCollectedOnTap_4() { return static_cast<int32_t>(offsetof(OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E, ___coinCollectedOnTap_4)); }
	inline int32_t get_coinCollectedOnTap_4() const { return ___coinCollectedOnTap_4; }
	inline int32_t* get_address_of_coinCollectedOnTap_4() { return &___coinCollectedOnTap_4; }
	inline void set_coinCollectedOnTap_4(int32_t value)
	{
		___coinCollectedOnTap_4 = value;
	}

	inline static int32_t get_offset_of_orbTapVO_5() { return static_cast<int32_t>(offsetof(OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E, ___orbTapVO_5)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_orbTapVO_5() const { return ___orbTapVO_5; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_orbTapVO_5() { return &___orbTapVO_5; }
	inline void set_orbTapVO_5(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___orbTapVO_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___orbTapVO_5), (void*)value);
	}

	inline static int32_t get_offset_of_colliderName_6() { return static_cast<int32_t>(offsetof(OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E, ___colliderName_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_colliderName_6() const { return ___colliderName_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_colliderName_6() { return &___colliderName_6; }
	inline void set_colliderName_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___colliderName_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colliderName_6), (void*)value);
	}

	inline static int32_t get_offset_of_storyAnimation_7() { return static_cast<int32_t>(offsetof(OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E, ___storyAnimation_7)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_storyAnimation_7() const { return ___storyAnimation_7; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_storyAnimation_7() { return &___storyAnimation_7; }
	inline void set_storyAnimation_7(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___storyAnimation_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___storyAnimation_7), (void*)value);
	}

	inline static int32_t get_offset_of_splashAnimation_8() { return static_cast<int32_t>(offsetof(OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E, ___splashAnimation_8)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_splashAnimation_8() const { return ___splashAnimation_8; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_splashAnimation_8() { return &___splashAnimation_8; }
	inline void set_splashAnimation_8(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___splashAnimation_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___splashAnimation_8), (void*)value);
	}

	inline static int32_t get_offset_of_storyTapVO_9() { return static_cast<int32_t>(offsetof(OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E, ___storyTapVO_9)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_storyTapVO_9() const { return ___storyTapVO_9; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_storyTapVO_9() { return &___storyTapVO_9; }
	inline void set_storyTapVO_9(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___storyTapVO_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___storyTapVO_9), (void*)value);
	}

	inline static int32_t get_offset_of_splashTapVO_10() { return static_cast<int32_t>(offsetof(OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E, ___splashTapVO_10)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_splashTapVO_10() const { return ___splashTapVO_10; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_splashTapVO_10() { return &___splashTapVO_10; }
	inline void set_splashTapVO_10(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___splashTapVO_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___splashTapVO_10), (void*)value);
	}
};


// PageCompletion
struct PageCompletion_tC5898F7BAD8C861FFA45295E4EE89BA178AB8B54  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// PageGame PageCompletion::pageGame
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5 * ___pageGame_4;
	// System.Int32 PageCompletion::completionCount
	int32_t ___completionCount_5;
	// System.Collections.Generic.List`1<StarfishTap> PageCompletion::starfishToTap
	List_1_tA1ADFC988916A9C8B9DE6B0B0E277C1F7EEF099B * ___starfishToTap_6;
	// UnityEngine.GameObject PageCompletion::toEnable
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___toEnable_7;
	// UnityEngine.GameObject PageCompletion::toEnable2
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___toEnable2_8;

public:
	inline static int32_t get_offset_of_pageGame_4() { return static_cast<int32_t>(offsetof(PageCompletion_tC5898F7BAD8C861FFA45295E4EE89BA178AB8B54, ___pageGame_4)); }
	inline PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5 * get_pageGame_4() const { return ___pageGame_4; }
	inline PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5 ** get_address_of_pageGame_4() { return &___pageGame_4; }
	inline void set_pageGame_4(PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5 * value)
	{
		___pageGame_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pageGame_4), (void*)value);
	}

	inline static int32_t get_offset_of_completionCount_5() { return static_cast<int32_t>(offsetof(PageCompletion_tC5898F7BAD8C861FFA45295E4EE89BA178AB8B54, ___completionCount_5)); }
	inline int32_t get_completionCount_5() const { return ___completionCount_5; }
	inline int32_t* get_address_of_completionCount_5() { return &___completionCount_5; }
	inline void set_completionCount_5(int32_t value)
	{
		___completionCount_5 = value;
	}

	inline static int32_t get_offset_of_starfishToTap_6() { return static_cast<int32_t>(offsetof(PageCompletion_tC5898F7BAD8C861FFA45295E4EE89BA178AB8B54, ___starfishToTap_6)); }
	inline List_1_tA1ADFC988916A9C8B9DE6B0B0E277C1F7EEF099B * get_starfishToTap_6() const { return ___starfishToTap_6; }
	inline List_1_tA1ADFC988916A9C8B9DE6B0B0E277C1F7EEF099B ** get_address_of_starfishToTap_6() { return &___starfishToTap_6; }
	inline void set_starfishToTap_6(List_1_tA1ADFC988916A9C8B9DE6B0B0E277C1F7EEF099B * value)
	{
		___starfishToTap_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___starfishToTap_6), (void*)value);
	}

	inline static int32_t get_offset_of_toEnable_7() { return static_cast<int32_t>(offsetof(PageCompletion_tC5898F7BAD8C861FFA45295E4EE89BA178AB8B54, ___toEnable_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_toEnable_7() const { return ___toEnable_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_toEnable_7() { return &___toEnable_7; }
	inline void set_toEnable_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___toEnable_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___toEnable_7), (void*)value);
	}

	inline static int32_t get_offset_of_toEnable2_8() { return static_cast<int32_t>(offsetof(PageCompletion_tC5898F7BAD8C861FFA45295E4EE89BA178AB8B54, ___toEnable2_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_toEnable2_8() const { return ___toEnable2_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_toEnable2_8() { return &___toEnable2_8; }
	inline void set_toEnable2_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___toEnable2_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___toEnable2_8), (void*)value);
	}
};


// PageGame
struct PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject PageGame::videoMV
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___videoMV_4;
	// UnityEngine.Time PageGame::videoDuration
	Time_tCE5C6E624BDC86B30112C860F5622AFA25F1EC9F * ___videoDuration_5;
	// System.Boolean PageGame::hasOnPageGame
	bool ___hasOnPageGame_6;
	// SplashWorld PageGame::splashWorld
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC * ___splashWorld_7;
	// UnityEngine.AudioClip PageGame::completeGameAudio
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___completeGameAudio_8;
	// UnityEngine.Transform PageGame::exitObject
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___exitObject_9;
	// DefaultObserverEventHandler PageGame::targetEventHandler
	DefaultObserverEventHandler_t25FF9CE0FF0ED822CE21D6AE20C6A6E77105C02B * ___targetEventHandler_10;
	// System.Boolean PageGame::isTrackingImage
	bool ___isTrackingImage_11;
	// Vuforia.ImageTargetBehaviour PageGame::imageTargetBehaviour
	ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34 * ___imageTargetBehaviour_12;
	// PageGame/PageStatus PageGame::<CurrentStatus>k__BackingField
	int32_t ___U3CCurrentStatusU3Ek__BackingField_13;
	// System.Boolean PageGame::CanRepeat
	bool ___CanRepeat_14;
	// System.Int32 PageGame::<CompletionCount>k__BackingField
	int32_t ___U3CCompletionCountU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_videoMV_4() { return static_cast<int32_t>(offsetof(PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5, ___videoMV_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_videoMV_4() const { return ___videoMV_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_videoMV_4() { return &___videoMV_4; }
	inline void set_videoMV_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___videoMV_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___videoMV_4), (void*)value);
	}

	inline static int32_t get_offset_of_videoDuration_5() { return static_cast<int32_t>(offsetof(PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5, ___videoDuration_5)); }
	inline Time_tCE5C6E624BDC86B30112C860F5622AFA25F1EC9F * get_videoDuration_5() const { return ___videoDuration_5; }
	inline Time_tCE5C6E624BDC86B30112C860F5622AFA25F1EC9F ** get_address_of_videoDuration_5() { return &___videoDuration_5; }
	inline void set_videoDuration_5(Time_tCE5C6E624BDC86B30112C860F5622AFA25F1EC9F * value)
	{
		___videoDuration_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___videoDuration_5), (void*)value);
	}

	inline static int32_t get_offset_of_hasOnPageGame_6() { return static_cast<int32_t>(offsetof(PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5, ___hasOnPageGame_6)); }
	inline bool get_hasOnPageGame_6() const { return ___hasOnPageGame_6; }
	inline bool* get_address_of_hasOnPageGame_6() { return &___hasOnPageGame_6; }
	inline void set_hasOnPageGame_6(bool value)
	{
		___hasOnPageGame_6 = value;
	}

	inline static int32_t get_offset_of_splashWorld_7() { return static_cast<int32_t>(offsetof(PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5, ___splashWorld_7)); }
	inline SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC * get_splashWorld_7() const { return ___splashWorld_7; }
	inline SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC ** get_address_of_splashWorld_7() { return &___splashWorld_7; }
	inline void set_splashWorld_7(SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC * value)
	{
		___splashWorld_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___splashWorld_7), (void*)value);
	}

	inline static int32_t get_offset_of_completeGameAudio_8() { return static_cast<int32_t>(offsetof(PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5, ___completeGameAudio_8)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get_completeGameAudio_8() const { return ___completeGameAudio_8; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of_completeGameAudio_8() { return &___completeGameAudio_8; }
	inline void set_completeGameAudio_8(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		___completeGameAudio_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___completeGameAudio_8), (void*)value);
	}

	inline static int32_t get_offset_of_exitObject_9() { return static_cast<int32_t>(offsetof(PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5, ___exitObject_9)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_exitObject_9() const { return ___exitObject_9; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_exitObject_9() { return &___exitObject_9; }
	inline void set_exitObject_9(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___exitObject_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___exitObject_9), (void*)value);
	}

	inline static int32_t get_offset_of_targetEventHandler_10() { return static_cast<int32_t>(offsetof(PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5, ___targetEventHandler_10)); }
	inline DefaultObserverEventHandler_t25FF9CE0FF0ED822CE21D6AE20C6A6E77105C02B * get_targetEventHandler_10() const { return ___targetEventHandler_10; }
	inline DefaultObserverEventHandler_t25FF9CE0FF0ED822CE21D6AE20C6A6E77105C02B ** get_address_of_targetEventHandler_10() { return &___targetEventHandler_10; }
	inline void set_targetEventHandler_10(DefaultObserverEventHandler_t25FF9CE0FF0ED822CE21D6AE20C6A6E77105C02B * value)
	{
		___targetEventHandler_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___targetEventHandler_10), (void*)value);
	}

	inline static int32_t get_offset_of_isTrackingImage_11() { return static_cast<int32_t>(offsetof(PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5, ___isTrackingImage_11)); }
	inline bool get_isTrackingImage_11() const { return ___isTrackingImage_11; }
	inline bool* get_address_of_isTrackingImage_11() { return &___isTrackingImage_11; }
	inline void set_isTrackingImage_11(bool value)
	{
		___isTrackingImage_11 = value;
	}

	inline static int32_t get_offset_of_imageTargetBehaviour_12() { return static_cast<int32_t>(offsetof(PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5, ___imageTargetBehaviour_12)); }
	inline ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34 * get_imageTargetBehaviour_12() const { return ___imageTargetBehaviour_12; }
	inline ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34 ** get_address_of_imageTargetBehaviour_12() { return &___imageTargetBehaviour_12; }
	inline void set_imageTargetBehaviour_12(ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34 * value)
	{
		___imageTargetBehaviour_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___imageTargetBehaviour_12), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCurrentStatusU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5, ___U3CCurrentStatusU3Ek__BackingField_13)); }
	inline int32_t get_U3CCurrentStatusU3Ek__BackingField_13() const { return ___U3CCurrentStatusU3Ek__BackingField_13; }
	inline int32_t* get_address_of_U3CCurrentStatusU3Ek__BackingField_13() { return &___U3CCurrentStatusU3Ek__BackingField_13; }
	inline void set_U3CCurrentStatusU3Ek__BackingField_13(int32_t value)
	{
		___U3CCurrentStatusU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_CanRepeat_14() { return static_cast<int32_t>(offsetof(PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5, ___CanRepeat_14)); }
	inline bool get_CanRepeat_14() const { return ___CanRepeat_14; }
	inline bool* get_address_of_CanRepeat_14() { return &___CanRepeat_14; }
	inline void set_CanRepeat_14(bool value)
	{
		___CanRepeat_14 = value;
	}

	inline static int32_t get_offset_of_U3CCompletionCountU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5, ___U3CCompletionCountU3Ek__BackingField_15)); }
	inline int32_t get_U3CCompletionCountU3Ek__BackingField_15() const { return ___U3CCompletionCountU3Ek__BackingField_15; }
	inline int32_t* get_address_of_U3CCompletionCountU3Ek__BackingField_15() { return &___U3CCompletionCountU3Ek__BackingField_15; }
	inline void set_U3CCompletionCountU3Ek__BackingField_15(int32_t value)
	{
		___U3CCompletionCountU3Ek__BackingField_15 = value;
	}
};


// PageManager
struct PageManager_t2117D39B2F2C5C8A4EE38E159E54B69771568945  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// PageTracker PageManager::firstPageTracker
	PageTracker_tF742F2BC772F15FC123563EFFEA7932454A688A2 * ___firstPageTracker_4;
	// System.Collections.Generic.List`1<PageGame> PageManager::pages
	List_1_t4A2EDA437C1CBDE74B20DD2C43CE752D0469B0A0 * ___pages_5;

public:
	inline static int32_t get_offset_of_firstPageTracker_4() { return static_cast<int32_t>(offsetof(PageManager_t2117D39B2F2C5C8A4EE38E159E54B69771568945, ___firstPageTracker_4)); }
	inline PageTracker_tF742F2BC772F15FC123563EFFEA7932454A688A2 * get_firstPageTracker_4() const { return ___firstPageTracker_4; }
	inline PageTracker_tF742F2BC772F15FC123563EFFEA7932454A688A2 ** get_address_of_firstPageTracker_4() { return &___firstPageTracker_4; }
	inline void set_firstPageTracker_4(PageTracker_tF742F2BC772F15FC123563EFFEA7932454A688A2 * value)
	{
		___firstPageTracker_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___firstPageTracker_4), (void*)value);
	}

	inline static int32_t get_offset_of_pages_5() { return static_cast<int32_t>(offsetof(PageManager_t2117D39B2F2C5C8A4EE38E159E54B69771568945, ___pages_5)); }
	inline List_1_t4A2EDA437C1CBDE74B20DD2C43CE752D0469B0A0 * get_pages_5() const { return ___pages_5; }
	inline List_1_t4A2EDA437C1CBDE74B20DD2C43CE752D0469B0A0 ** get_address_of_pages_5() { return &___pages_5; }
	inline void set_pages_5(List_1_t4A2EDA437C1CBDE74B20DD2C43CE752D0469B0A0 * value)
	{
		___pages_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pages_5), (void*)value);
	}
};


// PageTracker
struct PageTracker_tF742F2BC772F15FC123563EFFEA7932454A688A2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// PageManager PageTracker::pageManager
	PageManager_t2117D39B2F2C5C8A4EE38E159E54B69771568945 * ___pageManager_4;
	// DefaultObserverEventHandler PageTracker::targetEventHandler
	DefaultObserverEventHandler_t25FF9CE0FF0ED822CE21D6AE20C6A6E77105C02B * ___targetEventHandler_5;
	// Idle PageTracker::idle
	Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867 * ___idle_6;

public:
	inline static int32_t get_offset_of_pageManager_4() { return static_cast<int32_t>(offsetof(PageTracker_tF742F2BC772F15FC123563EFFEA7932454A688A2, ___pageManager_4)); }
	inline PageManager_t2117D39B2F2C5C8A4EE38E159E54B69771568945 * get_pageManager_4() const { return ___pageManager_4; }
	inline PageManager_t2117D39B2F2C5C8A4EE38E159E54B69771568945 ** get_address_of_pageManager_4() { return &___pageManager_4; }
	inline void set_pageManager_4(PageManager_t2117D39B2F2C5C8A4EE38E159E54B69771568945 * value)
	{
		___pageManager_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pageManager_4), (void*)value);
	}

	inline static int32_t get_offset_of_targetEventHandler_5() { return static_cast<int32_t>(offsetof(PageTracker_tF742F2BC772F15FC123563EFFEA7932454A688A2, ___targetEventHandler_5)); }
	inline DefaultObserverEventHandler_t25FF9CE0FF0ED822CE21D6AE20C6A6E77105C02B * get_targetEventHandler_5() const { return ___targetEventHandler_5; }
	inline DefaultObserverEventHandler_t25FF9CE0FF0ED822CE21D6AE20C6A6E77105C02B ** get_address_of_targetEventHandler_5() { return &___targetEventHandler_5; }
	inline void set_targetEventHandler_5(DefaultObserverEventHandler_t25FF9CE0FF0ED822CE21D6AE20C6A6E77105C02B * value)
	{
		___targetEventHandler_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___targetEventHandler_5), (void*)value);
	}

	inline static int32_t get_offset_of_idle_6() { return static_cast<int32_t>(offsetof(PageTracker_tF742F2BC772F15FC123563EFFEA7932454A688A2, ___idle_6)); }
	inline Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867 * get_idle_6() const { return ___idle_6; }
	inline Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867 ** get_address_of_idle_6() { return &___idle_6; }
	inline void set_idle_6(Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867 * value)
	{
		___idle_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___idle_6), (void*)value);
	}
};


// DentedPixel.LTExamples.PathBezier
struct PathBezier_t3A13E1125CFCBAC9592FD3E738EBD51E56258816  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform[] DentedPixel.LTExamples.PathBezier::trans
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ___trans_4;
	// LTBezierPath DentedPixel.LTExamples.PathBezier::cr
	LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48 * ___cr_5;
	// UnityEngine.GameObject DentedPixel.LTExamples.PathBezier::avatar1
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___avatar1_6;
	// System.Single DentedPixel.LTExamples.PathBezier::iter
	float ___iter_7;

public:
	inline static int32_t get_offset_of_trans_4() { return static_cast<int32_t>(offsetof(PathBezier_t3A13E1125CFCBAC9592FD3E738EBD51E56258816, ___trans_4)); }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* get_trans_4() const { return ___trans_4; }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D** get_address_of_trans_4() { return &___trans_4; }
	inline void set_trans_4(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* value)
	{
		___trans_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trans_4), (void*)value);
	}

	inline static int32_t get_offset_of_cr_5() { return static_cast<int32_t>(offsetof(PathBezier_t3A13E1125CFCBAC9592FD3E738EBD51E56258816, ___cr_5)); }
	inline LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48 * get_cr_5() const { return ___cr_5; }
	inline LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48 ** get_address_of_cr_5() { return &___cr_5; }
	inline void set_cr_5(LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48 * value)
	{
		___cr_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cr_5), (void*)value);
	}

	inline static int32_t get_offset_of_avatar1_6() { return static_cast<int32_t>(offsetof(PathBezier_t3A13E1125CFCBAC9592FD3E738EBD51E56258816, ___avatar1_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_avatar1_6() const { return ___avatar1_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_avatar1_6() { return &___avatar1_6; }
	inline void set_avatar1_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___avatar1_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___avatar1_6), (void*)value);
	}

	inline static int32_t get_offset_of_iter_7() { return static_cast<int32_t>(offsetof(PathBezier_t3A13E1125CFCBAC9592FD3E738EBD51E56258816, ___iter_7)); }
	inline float get_iter_7() const { return ___iter_7; }
	inline float* get_address_of_iter_7() { return &___iter_7; }
	inline void set_iter_7(float value)
	{
		___iter_7 = value;
	}
};


// PathBezier2d
struct PathBezier2d_t39687CAFB63E5F8FBB8BB2EB2E30517FD6702C0B  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform[] PathBezier2d::cubes
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ___cubes_4;
	// UnityEngine.GameObject PathBezier2d::dude1
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___dude1_5;
	// UnityEngine.GameObject PathBezier2d::dude2
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___dude2_6;
	// LTBezierPath PathBezier2d::visualizePath
	LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48 * ___visualizePath_7;

public:
	inline static int32_t get_offset_of_cubes_4() { return static_cast<int32_t>(offsetof(PathBezier2d_t39687CAFB63E5F8FBB8BB2EB2E30517FD6702C0B, ___cubes_4)); }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* get_cubes_4() const { return ___cubes_4; }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D** get_address_of_cubes_4() { return &___cubes_4; }
	inline void set_cubes_4(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* value)
	{
		___cubes_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubes_4), (void*)value);
	}

	inline static int32_t get_offset_of_dude1_5() { return static_cast<int32_t>(offsetof(PathBezier2d_t39687CAFB63E5F8FBB8BB2EB2E30517FD6702C0B, ___dude1_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_dude1_5() const { return ___dude1_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_dude1_5() { return &___dude1_5; }
	inline void set_dude1_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___dude1_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dude1_5), (void*)value);
	}

	inline static int32_t get_offset_of_dude2_6() { return static_cast<int32_t>(offsetof(PathBezier2d_t39687CAFB63E5F8FBB8BB2EB2E30517FD6702C0B, ___dude2_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_dude2_6() const { return ___dude2_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_dude2_6() { return &___dude2_6; }
	inline void set_dude2_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___dude2_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dude2_6), (void*)value);
	}

	inline static int32_t get_offset_of_visualizePath_7() { return static_cast<int32_t>(offsetof(PathBezier2d_t39687CAFB63E5F8FBB8BB2EB2E30517FD6702C0B, ___visualizePath_7)); }
	inline LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48 * get_visualizePath_7() const { return ___visualizePath_7; }
	inline LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48 ** get_address_of_visualizePath_7() { return &___visualizePath_7; }
	inline void set_visualizePath_7(LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48 * value)
	{
		___visualizePath_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___visualizePath_7), (void*)value);
	}
};


// PathSpline2d
struct PathSpline2d_t0E183ACECD08FCA2241D336A772AD99F6B85D247  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform[] PathSpline2d::cubes
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ___cubes_4;
	// UnityEngine.GameObject PathSpline2d::dude1
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___dude1_5;
	// UnityEngine.GameObject PathSpline2d::dude2
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___dude2_6;
	// LTSpline PathSpline2d::visualizePath
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 * ___visualizePath_7;

public:
	inline static int32_t get_offset_of_cubes_4() { return static_cast<int32_t>(offsetof(PathSpline2d_t0E183ACECD08FCA2241D336A772AD99F6B85D247, ___cubes_4)); }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* get_cubes_4() const { return ___cubes_4; }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D** get_address_of_cubes_4() { return &___cubes_4; }
	inline void set_cubes_4(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* value)
	{
		___cubes_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubes_4), (void*)value);
	}

	inline static int32_t get_offset_of_dude1_5() { return static_cast<int32_t>(offsetof(PathSpline2d_t0E183ACECD08FCA2241D336A772AD99F6B85D247, ___dude1_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_dude1_5() const { return ___dude1_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_dude1_5() { return &___dude1_5; }
	inline void set_dude1_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___dude1_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dude1_5), (void*)value);
	}

	inline static int32_t get_offset_of_dude2_6() { return static_cast<int32_t>(offsetof(PathSpline2d_t0E183ACECD08FCA2241D336A772AD99F6B85D247, ___dude2_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_dude2_6() const { return ___dude2_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_dude2_6() { return &___dude2_6; }
	inline void set_dude2_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___dude2_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dude2_6), (void*)value);
	}

	inline static int32_t get_offset_of_visualizePath_7() { return static_cast<int32_t>(offsetof(PathSpline2d_t0E183ACECD08FCA2241D336A772AD99F6B85D247, ___visualizePath_7)); }
	inline LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 * get_visualizePath_7() const { return ___visualizePath_7; }
	inline LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 ** get_address_of_visualizePath_7() { return &___visualizePath_7; }
	inline void set_visualizePath_7(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 * value)
	{
		___visualizePath_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___visualizePath_7), (void*)value);
	}
};


// PathSplineEndless
struct PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject PathSplineEndless::trackTrailRenderers
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___trackTrailRenderers_4;
	// UnityEngine.GameObject PathSplineEndless::car
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___car_5;
	// UnityEngine.GameObject PathSplineEndless::carInternal
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___carInternal_6;
	// UnityEngine.GameObject[] PathSplineEndless::cubes
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___cubes_7;
	// System.Int32 PathSplineEndless::cubesIter
	int32_t ___cubesIter_8;
	// UnityEngine.GameObject[] PathSplineEndless::trees
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___trees_9;
	// System.Int32 PathSplineEndless::treesIter
	int32_t ___treesIter_10;
	// System.Single PathSplineEndless::randomIterWidth
	float ___randomIterWidth_11;
	// LTSpline PathSplineEndless::track
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 * ___track_12;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> PathSplineEndless::trackPts
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___trackPts_13;
	// System.Int32 PathSplineEndless::zIter
	int32_t ___zIter_14;
	// System.Single PathSplineEndless::carIter
	float ___carIter_15;
	// System.Single PathSplineEndless::carAdd
	float ___carAdd_16;
	// System.Int32 PathSplineEndless::trackMaxItems
	int32_t ___trackMaxItems_17;
	// System.Int32 PathSplineEndless::trackIter
	int32_t ___trackIter_18;
	// System.Single PathSplineEndless::pushTrackAhead
	float ___pushTrackAhead_19;
	// System.Single PathSplineEndless::randomIter
	float ___randomIter_20;

public:
	inline static int32_t get_offset_of_trackTrailRenderers_4() { return static_cast<int32_t>(offsetof(PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28, ___trackTrailRenderers_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_trackTrailRenderers_4() const { return ___trackTrailRenderers_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_trackTrailRenderers_4() { return &___trackTrailRenderers_4; }
	inline void set_trackTrailRenderers_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___trackTrailRenderers_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trackTrailRenderers_4), (void*)value);
	}

	inline static int32_t get_offset_of_car_5() { return static_cast<int32_t>(offsetof(PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28, ___car_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_car_5() const { return ___car_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_car_5() { return &___car_5; }
	inline void set_car_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___car_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___car_5), (void*)value);
	}

	inline static int32_t get_offset_of_carInternal_6() { return static_cast<int32_t>(offsetof(PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28, ___carInternal_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_carInternal_6() const { return ___carInternal_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_carInternal_6() { return &___carInternal_6; }
	inline void set_carInternal_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___carInternal_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___carInternal_6), (void*)value);
	}

	inline static int32_t get_offset_of_cubes_7() { return static_cast<int32_t>(offsetof(PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28, ___cubes_7)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_cubes_7() const { return ___cubes_7; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_cubes_7() { return &___cubes_7; }
	inline void set_cubes_7(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___cubes_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubes_7), (void*)value);
	}

	inline static int32_t get_offset_of_cubesIter_8() { return static_cast<int32_t>(offsetof(PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28, ___cubesIter_8)); }
	inline int32_t get_cubesIter_8() const { return ___cubesIter_8; }
	inline int32_t* get_address_of_cubesIter_8() { return &___cubesIter_8; }
	inline void set_cubesIter_8(int32_t value)
	{
		___cubesIter_8 = value;
	}

	inline static int32_t get_offset_of_trees_9() { return static_cast<int32_t>(offsetof(PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28, ___trees_9)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_trees_9() const { return ___trees_9; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_trees_9() { return &___trees_9; }
	inline void set_trees_9(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___trees_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trees_9), (void*)value);
	}

	inline static int32_t get_offset_of_treesIter_10() { return static_cast<int32_t>(offsetof(PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28, ___treesIter_10)); }
	inline int32_t get_treesIter_10() const { return ___treesIter_10; }
	inline int32_t* get_address_of_treesIter_10() { return &___treesIter_10; }
	inline void set_treesIter_10(int32_t value)
	{
		___treesIter_10 = value;
	}

	inline static int32_t get_offset_of_randomIterWidth_11() { return static_cast<int32_t>(offsetof(PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28, ___randomIterWidth_11)); }
	inline float get_randomIterWidth_11() const { return ___randomIterWidth_11; }
	inline float* get_address_of_randomIterWidth_11() { return &___randomIterWidth_11; }
	inline void set_randomIterWidth_11(float value)
	{
		___randomIterWidth_11 = value;
	}

	inline static int32_t get_offset_of_track_12() { return static_cast<int32_t>(offsetof(PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28, ___track_12)); }
	inline LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 * get_track_12() const { return ___track_12; }
	inline LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 ** get_address_of_track_12() { return &___track_12; }
	inline void set_track_12(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 * value)
	{
		___track_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___track_12), (void*)value);
	}

	inline static int32_t get_offset_of_trackPts_13() { return static_cast<int32_t>(offsetof(PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28, ___trackPts_13)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_trackPts_13() const { return ___trackPts_13; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_trackPts_13() { return &___trackPts_13; }
	inline void set_trackPts_13(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___trackPts_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trackPts_13), (void*)value);
	}

	inline static int32_t get_offset_of_zIter_14() { return static_cast<int32_t>(offsetof(PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28, ___zIter_14)); }
	inline int32_t get_zIter_14() const { return ___zIter_14; }
	inline int32_t* get_address_of_zIter_14() { return &___zIter_14; }
	inline void set_zIter_14(int32_t value)
	{
		___zIter_14 = value;
	}

	inline static int32_t get_offset_of_carIter_15() { return static_cast<int32_t>(offsetof(PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28, ___carIter_15)); }
	inline float get_carIter_15() const { return ___carIter_15; }
	inline float* get_address_of_carIter_15() { return &___carIter_15; }
	inline void set_carIter_15(float value)
	{
		___carIter_15 = value;
	}

	inline static int32_t get_offset_of_carAdd_16() { return static_cast<int32_t>(offsetof(PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28, ___carAdd_16)); }
	inline float get_carAdd_16() const { return ___carAdd_16; }
	inline float* get_address_of_carAdd_16() { return &___carAdd_16; }
	inline void set_carAdd_16(float value)
	{
		___carAdd_16 = value;
	}

	inline static int32_t get_offset_of_trackMaxItems_17() { return static_cast<int32_t>(offsetof(PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28, ___trackMaxItems_17)); }
	inline int32_t get_trackMaxItems_17() const { return ___trackMaxItems_17; }
	inline int32_t* get_address_of_trackMaxItems_17() { return &___trackMaxItems_17; }
	inline void set_trackMaxItems_17(int32_t value)
	{
		___trackMaxItems_17 = value;
	}

	inline static int32_t get_offset_of_trackIter_18() { return static_cast<int32_t>(offsetof(PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28, ___trackIter_18)); }
	inline int32_t get_trackIter_18() const { return ___trackIter_18; }
	inline int32_t* get_address_of_trackIter_18() { return &___trackIter_18; }
	inline void set_trackIter_18(int32_t value)
	{
		___trackIter_18 = value;
	}

	inline static int32_t get_offset_of_pushTrackAhead_19() { return static_cast<int32_t>(offsetof(PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28, ___pushTrackAhead_19)); }
	inline float get_pushTrackAhead_19() const { return ___pushTrackAhead_19; }
	inline float* get_address_of_pushTrackAhead_19() { return &___pushTrackAhead_19; }
	inline void set_pushTrackAhead_19(float value)
	{
		___pushTrackAhead_19 = value;
	}

	inline static int32_t get_offset_of_randomIter_20() { return static_cast<int32_t>(offsetof(PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28, ___randomIter_20)); }
	inline float get_randomIter_20() const { return ___randomIter_20; }
	inline float* get_address_of_randomIter_20() { return &___randomIter_20; }
	inline void set_randomIter_20(float value)
	{
		___randomIter_20 = value;
	}
};


// PathSplinePerformance
struct PathSplinePerformance_t36D2BBA36977A2DCABCA4AB139EB5F37FCBBC143  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject PathSplinePerformance::trackTrailRenderers
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___trackTrailRenderers_4;
	// UnityEngine.GameObject PathSplinePerformance::car
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___car_5;
	// UnityEngine.GameObject PathSplinePerformance::carInternal
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___carInternal_6;
	// System.Single PathSplinePerformance::circleLength
	float ___circleLength_7;
	// System.Single PathSplinePerformance::randomRange
	float ___randomRange_8;
	// System.Int32 PathSplinePerformance::trackNodes
	int32_t ___trackNodes_9;
	// System.Single PathSplinePerformance::carSpeed
	float ___carSpeed_10;
	// System.Single PathSplinePerformance::tracerSpeed
	float ___tracerSpeed_11;
	// LTSpline PathSplinePerformance::track
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 * ___track_12;
	// System.Int32 PathSplinePerformance::trackIter
	int32_t ___trackIter_13;
	// System.Single PathSplinePerformance::carAdd
	float ___carAdd_14;
	// System.Single PathSplinePerformance::trackPosition
	float ___trackPosition_15;

public:
	inline static int32_t get_offset_of_trackTrailRenderers_4() { return static_cast<int32_t>(offsetof(PathSplinePerformance_t36D2BBA36977A2DCABCA4AB139EB5F37FCBBC143, ___trackTrailRenderers_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_trackTrailRenderers_4() const { return ___trackTrailRenderers_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_trackTrailRenderers_4() { return &___trackTrailRenderers_4; }
	inline void set_trackTrailRenderers_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___trackTrailRenderers_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trackTrailRenderers_4), (void*)value);
	}

	inline static int32_t get_offset_of_car_5() { return static_cast<int32_t>(offsetof(PathSplinePerformance_t36D2BBA36977A2DCABCA4AB139EB5F37FCBBC143, ___car_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_car_5() const { return ___car_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_car_5() { return &___car_5; }
	inline void set_car_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___car_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___car_5), (void*)value);
	}

	inline static int32_t get_offset_of_carInternal_6() { return static_cast<int32_t>(offsetof(PathSplinePerformance_t36D2BBA36977A2DCABCA4AB139EB5F37FCBBC143, ___carInternal_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_carInternal_6() const { return ___carInternal_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_carInternal_6() { return &___carInternal_6; }
	inline void set_carInternal_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___carInternal_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___carInternal_6), (void*)value);
	}

	inline static int32_t get_offset_of_circleLength_7() { return static_cast<int32_t>(offsetof(PathSplinePerformance_t36D2BBA36977A2DCABCA4AB139EB5F37FCBBC143, ___circleLength_7)); }
	inline float get_circleLength_7() const { return ___circleLength_7; }
	inline float* get_address_of_circleLength_7() { return &___circleLength_7; }
	inline void set_circleLength_7(float value)
	{
		___circleLength_7 = value;
	}

	inline static int32_t get_offset_of_randomRange_8() { return static_cast<int32_t>(offsetof(PathSplinePerformance_t36D2BBA36977A2DCABCA4AB139EB5F37FCBBC143, ___randomRange_8)); }
	inline float get_randomRange_8() const { return ___randomRange_8; }
	inline float* get_address_of_randomRange_8() { return &___randomRange_8; }
	inline void set_randomRange_8(float value)
	{
		___randomRange_8 = value;
	}

	inline static int32_t get_offset_of_trackNodes_9() { return static_cast<int32_t>(offsetof(PathSplinePerformance_t36D2BBA36977A2DCABCA4AB139EB5F37FCBBC143, ___trackNodes_9)); }
	inline int32_t get_trackNodes_9() const { return ___trackNodes_9; }
	inline int32_t* get_address_of_trackNodes_9() { return &___trackNodes_9; }
	inline void set_trackNodes_9(int32_t value)
	{
		___trackNodes_9 = value;
	}

	inline static int32_t get_offset_of_carSpeed_10() { return static_cast<int32_t>(offsetof(PathSplinePerformance_t36D2BBA36977A2DCABCA4AB139EB5F37FCBBC143, ___carSpeed_10)); }
	inline float get_carSpeed_10() const { return ___carSpeed_10; }
	inline float* get_address_of_carSpeed_10() { return &___carSpeed_10; }
	inline void set_carSpeed_10(float value)
	{
		___carSpeed_10 = value;
	}

	inline static int32_t get_offset_of_tracerSpeed_11() { return static_cast<int32_t>(offsetof(PathSplinePerformance_t36D2BBA36977A2DCABCA4AB139EB5F37FCBBC143, ___tracerSpeed_11)); }
	inline float get_tracerSpeed_11() const { return ___tracerSpeed_11; }
	inline float* get_address_of_tracerSpeed_11() { return &___tracerSpeed_11; }
	inline void set_tracerSpeed_11(float value)
	{
		___tracerSpeed_11 = value;
	}

	inline static int32_t get_offset_of_track_12() { return static_cast<int32_t>(offsetof(PathSplinePerformance_t36D2BBA36977A2DCABCA4AB139EB5F37FCBBC143, ___track_12)); }
	inline LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 * get_track_12() const { return ___track_12; }
	inline LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 ** get_address_of_track_12() { return &___track_12; }
	inline void set_track_12(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 * value)
	{
		___track_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___track_12), (void*)value);
	}

	inline static int32_t get_offset_of_trackIter_13() { return static_cast<int32_t>(offsetof(PathSplinePerformance_t36D2BBA36977A2DCABCA4AB139EB5F37FCBBC143, ___trackIter_13)); }
	inline int32_t get_trackIter_13() const { return ___trackIter_13; }
	inline int32_t* get_address_of_trackIter_13() { return &___trackIter_13; }
	inline void set_trackIter_13(int32_t value)
	{
		___trackIter_13 = value;
	}

	inline static int32_t get_offset_of_carAdd_14() { return static_cast<int32_t>(offsetof(PathSplinePerformance_t36D2BBA36977A2DCABCA4AB139EB5F37FCBBC143, ___carAdd_14)); }
	inline float get_carAdd_14() const { return ___carAdd_14; }
	inline float* get_address_of_carAdd_14() { return &___carAdd_14; }
	inline void set_carAdd_14(float value)
	{
		___carAdd_14 = value;
	}

	inline static int32_t get_offset_of_trackPosition_15() { return static_cast<int32_t>(offsetof(PathSplinePerformance_t36D2BBA36977A2DCABCA4AB139EB5F37FCBBC143, ___trackPosition_15)); }
	inline float get_trackPosition_15() const { return ___trackPosition_15; }
	inline float* get_address_of_trackPosition_15() { return &___trackPosition_15; }
	inline void set_trackPosition_15(float value)
	{
		___trackPosition_15 = value;
	}
};


// PathSplineTrack
struct PathSplineTrack_tD6F0ED2BE052288BBFF0A20C938D993506F5B58A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject PathSplineTrack::car
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___car_4;
	// UnityEngine.GameObject PathSplineTrack::carInternal
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___carInternal_5;
	// UnityEngine.GameObject PathSplineTrack::trackTrailRenderers
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___trackTrailRenderers_6;
	// UnityEngine.Transform[] PathSplineTrack::trackOnePoints
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ___trackOnePoints_7;
	// LTSpline PathSplineTrack::track
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 * ___track_8;
	// System.Int32 PathSplineTrack::trackIter
	int32_t ___trackIter_9;
	// System.Single PathSplineTrack::trackPosition
	float ___trackPosition_10;

public:
	inline static int32_t get_offset_of_car_4() { return static_cast<int32_t>(offsetof(PathSplineTrack_tD6F0ED2BE052288BBFF0A20C938D993506F5B58A, ___car_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_car_4() const { return ___car_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_car_4() { return &___car_4; }
	inline void set_car_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___car_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___car_4), (void*)value);
	}

	inline static int32_t get_offset_of_carInternal_5() { return static_cast<int32_t>(offsetof(PathSplineTrack_tD6F0ED2BE052288BBFF0A20C938D993506F5B58A, ___carInternal_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_carInternal_5() const { return ___carInternal_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_carInternal_5() { return &___carInternal_5; }
	inline void set_carInternal_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___carInternal_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___carInternal_5), (void*)value);
	}

	inline static int32_t get_offset_of_trackTrailRenderers_6() { return static_cast<int32_t>(offsetof(PathSplineTrack_tD6F0ED2BE052288BBFF0A20C938D993506F5B58A, ___trackTrailRenderers_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_trackTrailRenderers_6() const { return ___trackTrailRenderers_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_trackTrailRenderers_6() { return &___trackTrailRenderers_6; }
	inline void set_trackTrailRenderers_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___trackTrailRenderers_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trackTrailRenderers_6), (void*)value);
	}

	inline static int32_t get_offset_of_trackOnePoints_7() { return static_cast<int32_t>(offsetof(PathSplineTrack_tD6F0ED2BE052288BBFF0A20C938D993506F5B58A, ___trackOnePoints_7)); }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* get_trackOnePoints_7() const { return ___trackOnePoints_7; }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D** get_address_of_trackOnePoints_7() { return &___trackOnePoints_7; }
	inline void set_trackOnePoints_7(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* value)
	{
		___trackOnePoints_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trackOnePoints_7), (void*)value);
	}

	inline static int32_t get_offset_of_track_8() { return static_cast<int32_t>(offsetof(PathSplineTrack_tD6F0ED2BE052288BBFF0A20C938D993506F5B58A, ___track_8)); }
	inline LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 * get_track_8() const { return ___track_8; }
	inline LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 ** get_address_of_track_8() { return &___track_8; }
	inline void set_track_8(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 * value)
	{
		___track_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___track_8), (void*)value);
	}

	inline static int32_t get_offset_of_trackIter_9() { return static_cast<int32_t>(offsetof(PathSplineTrack_tD6F0ED2BE052288BBFF0A20C938D993506F5B58A, ___trackIter_9)); }
	inline int32_t get_trackIter_9() const { return ___trackIter_9; }
	inline int32_t* get_address_of_trackIter_9() { return &___trackIter_9; }
	inline void set_trackIter_9(int32_t value)
	{
		___trackIter_9 = value;
	}

	inline static int32_t get_offset_of_trackPosition_10() { return static_cast<int32_t>(offsetof(PathSplineTrack_tD6F0ED2BE052288BBFF0A20C938D993506F5B58A, ___trackPosition_10)); }
	inline float get_trackPosition_10() const { return ___trackPosition_10; }
	inline float* get_address_of_trackPosition_10() { return &___trackPosition_10; }
	inline void set_trackPosition_10(float value)
	{
		___trackPosition_10 = value;
	}
};


// PathSplines
struct PathSplines_t5E3D3A23634F2898542C5C6F650D03565C9C2D6A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform[] PathSplines::trans
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ___trans_4;
	// LTSpline PathSplines::cr
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 * ___cr_5;
	// UnityEngine.GameObject PathSplines::avatar1
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___avatar1_6;
	// System.Single PathSplines::iter
	float ___iter_7;

public:
	inline static int32_t get_offset_of_trans_4() { return static_cast<int32_t>(offsetof(PathSplines_t5E3D3A23634F2898542C5C6F650D03565C9C2D6A, ___trans_4)); }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* get_trans_4() const { return ___trans_4; }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D** get_address_of_trans_4() { return &___trans_4; }
	inline void set_trans_4(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* value)
	{
		___trans_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trans_4), (void*)value);
	}

	inline static int32_t get_offset_of_cr_5() { return static_cast<int32_t>(offsetof(PathSplines_t5E3D3A23634F2898542C5C6F650D03565C9C2D6A, ___cr_5)); }
	inline LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 * get_cr_5() const { return ___cr_5; }
	inline LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 ** get_address_of_cr_5() { return &___cr_5; }
	inline void set_cr_5(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 * value)
	{
		___cr_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cr_5), (void*)value);
	}

	inline static int32_t get_offset_of_avatar1_6() { return static_cast<int32_t>(offsetof(PathSplines_t5E3D3A23634F2898542C5C6F650D03565C9C2D6A, ___avatar1_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_avatar1_6() const { return ___avatar1_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_avatar1_6() { return &___avatar1_6; }
	inline void set_avatar1_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___avatar1_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___avatar1_6), (void*)value);
	}

	inline static int32_t get_offset_of_iter_7() { return static_cast<int32_t>(offsetof(PathSplines_t5E3D3A23634F2898542C5C6F650D03565C9C2D6A, ___iter_7)); }
	inline float get_iter_7() const { return ___iter_7; }
	inline float* get_address_of_iter_7() { return &___iter_7; }
	inline void set_iter_7(float value)
	{
		___iter_7 = value;
	}
};


// PlayPause
struct PlayPause_tE57D2A634C24A66387C3115F3AF1B6663BC49BEA  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Video.VideoPlayer PlayPause::videoPlayer
	VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86 * ___videoPlayer_4;
	// UnityEngine.GameObject PlayPause::toDisable
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___toDisable_5;
	// UnityEngine.GameObject PlayPause::toEnable
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___toEnable_6;
	// UnityEngine.GameObject PlayPause::toEnable2
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___toEnable2_7;

public:
	inline static int32_t get_offset_of_videoPlayer_4() { return static_cast<int32_t>(offsetof(PlayPause_tE57D2A634C24A66387C3115F3AF1B6663BC49BEA, ___videoPlayer_4)); }
	inline VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86 * get_videoPlayer_4() const { return ___videoPlayer_4; }
	inline VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86 ** get_address_of_videoPlayer_4() { return &___videoPlayer_4; }
	inline void set_videoPlayer_4(VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86 * value)
	{
		___videoPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___videoPlayer_4), (void*)value);
	}

	inline static int32_t get_offset_of_toDisable_5() { return static_cast<int32_t>(offsetof(PlayPause_tE57D2A634C24A66387C3115F3AF1B6663BC49BEA, ___toDisable_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_toDisable_5() const { return ___toDisable_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_toDisable_5() { return &___toDisable_5; }
	inline void set_toDisable_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___toDisable_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___toDisable_5), (void*)value);
	}

	inline static int32_t get_offset_of_toEnable_6() { return static_cast<int32_t>(offsetof(PlayPause_tE57D2A634C24A66387C3115F3AF1B6663BC49BEA, ___toEnable_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_toEnable_6() const { return ___toEnable_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_toEnable_6() { return &___toEnable_6; }
	inline void set_toEnable_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___toEnable_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___toEnable_6), (void*)value);
	}

	inline static int32_t get_offset_of_toEnable2_7() { return static_cast<int32_t>(offsetof(PlayPause_tE57D2A634C24A66387C3115F3AF1B6663BC49BEA, ___toEnable2_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_toEnable2_7() const { return ___toEnable2_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_toEnable2_7() { return &___toEnable2_7; }
	inline void set_toEnable2_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___toEnable2_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___toEnable2_7), (void*)value);
	}
};


// Raycaster
struct Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.LayerMask Raycaster::raycastTapLayers
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___raycastTapLayers_4;
	// UnityEngine.LayerMask Raycaster::raycastOrbTapLayers
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___raycastOrbTapLayers_5;
	// UnityEngine.LayerMask Raycaster::raycastDragLayers
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___raycastDragLayers_6;
	// UnityEngine.LayerMask Raycaster::raycastThrowLayers
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___raycastThrowLayers_7;
	// System.Single Raycaster::raycastDistance
	float ___raycastDistance_8;
	// StarfishTap Raycaster::arCollider
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056 * ___arCollider_9;
	// SplashWorldTapThrow Raycaster::vrCollider
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F * ___vrCollider_10;
	// UnityEngine.ParticleSystem Raycaster::throwParticles
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___throwParticles_11;
	// System.Boolean Raycaster::throwOnRelease
	bool ___throwOnRelease_12;
	// UnityEngine.Camera Raycaster::mainCam
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___mainCam_13;

public:
	inline static int32_t get_offset_of_raycastTapLayers_4() { return static_cast<int32_t>(offsetof(Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139, ___raycastTapLayers_4)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_raycastTapLayers_4() const { return ___raycastTapLayers_4; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_raycastTapLayers_4() { return &___raycastTapLayers_4; }
	inline void set_raycastTapLayers_4(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___raycastTapLayers_4 = value;
	}

	inline static int32_t get_offset_of_raycastOrbTapLayers_5() { return static_cast<int32_t>(offsetof(Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139, ___raycastOrbTapLayers_5)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_raycastOrbTapLayers_5() const { return ___raycastOrbTapLayers_5; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_raycastOrbTapLayers_5() { return &___raycastOrbTapLayers_5; }
	inline void set_raycastOrbTapLayers_5(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___raycastOrbTapLayers_5 = value;
	}

	inline static int32_t get_offset_of_raycastDragLayers_6() { return static_cast<int32_t>(offsetof(Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139, ___raycastDragLayers_6)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_raycastDragLayers_6() const { return ___raycastDragLayers_6; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_raycastDragLayers_6() { return &___raycastDragLayers_6; }
	inline void set_raycastDragLayers_6(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___raycastDragLayers_6 = value;
	}

	inline static int32_t get_offset_of_raycastThrowLayers_7() { return static_cast<int32_t>(offsetof(Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139, ___raycastThrowLayers_7)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_raycastThrowLayers_7() const { return ___raycastThrowLayers_7; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_raycastThrowLayers_7() { return &___raycastThrowLayers_7; }
	inline void set_raycastThrowLayers_7(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___raycastThrowLayers_7 = value;
	}

	inline static int32_t get_offset_of_raycastDistance_8() { return static_cast<int32_t>(offsetof(Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139, ___raycastDistance_8)); }
	inline float get_raycastDistance_8() const { return ___raycastDistance_8; }
	inline float* get_address_of_raycastDistance_8() { return &___raycastDistance_8; }
	inline void set_raycastDistance_8(float value)
	{
		___raycastDistance_8 = value;
	}

	inline static int32_t get_offset_of_arCollider_9() { return static_cast<int32_t>(offsetof(Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139, ___arCollider_9)); }
	inline StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056 * get_arCollider_9() const { return ___arCollider_9; }
	inline StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056 ** get_address_of_arCollider_9() { return &___arCollider_9; }
	inline void set_arCollider_9(StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056 * value)
	{
		___arCollider_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arCollider_9), (void*)value);
	}

	inline static int32_t get_offset_of_vrCollider_10() { return static_cast<int32_t>(offsetof(Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139, ___vrCollider_10)); }
	inline SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F * get_vrCollider_10() const { return ___vrCollider_10; }
	inline SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F ** get_address_of_vrCollider_10() { return &___vrCollider_10; }
	inline void set_vrCollider_10(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F * value)
	{
		___vrCollider_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vrCollider_10), (void*)value);
	}

	inline static int32_t get_offset_of_throwParticles_11() { return static_cast<int32_t>(offsetof(Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139, ___throwParticles_11)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_throwParticles_11() const { return ___throwParticles_11; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_throwParticles_11() { return &___throwParticles_11; }
	inline void set_throwParticles_11(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___throwParticles_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___throwParticles_11), (void*)value);
	}

	inline static int32_t get_offset_of_throwOnRelease_12() { return static_cast<int32_t>(offsetof(Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139, ___throwOnRelease_12)); }
	inline bool get_throwOnRelease_12() const { return ___throwOnRelease_12; }
	inline bool* get_address_of_throwOnRelease_12() { return &___throwOnRelease_12; }
	inline void set_throwOnRelease_12(bool value)
	{
		___throwOnRelease_12 = value;
	}

	inline static int32_t get_offset_of_mainCam_13() { return static_cast<int32_t>(offsetof(Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139, ___mainCam_13)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_mainCam_13() const { return ___mainCam_13; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_mainCam_13() { return &___mainCam_13; }
	inline void set_mainCam_13(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___mainCam_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mainCam_13), (void*)value);
	}
};


// UnityTemplateProjects.SimpleCameraController
struct SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityTemplateProjects.SimpleCameraController/CameraState UnityTemplateProjects.SimpleCameraController::m_TargetCameraState
	CameraState_t3CFB67357E39129414AC5C9D628B9B47015CCE8C * ___m_TargetCameraState_5;
	// UnityTemplateProjects.SimpleCameraController/CameraState UnityTemplateProjects.SimpleCameraController::m_InterpolatingCameraState
	CameraState_t3CFB67357E39129414AC5C9D628B9B47015CCE8C * ___m_InterpolatingCameraState_6;
	// System.Single UnityTemplateProjects.SimpleCameraController::boost
	float ___boost_7;
	// System.Single UnityTemplateProjects.SimpleCameraController::positionLerpTime
	float ___positionLerpTime_8;
	// System.Single UnityTemplateProjects.SimpleCameraController::mouseSensitivity
	float ___mouseSensitivity_9;
	// UnityEngine.AnimationCurve UnityTemplateProjects.SimpleCameraController::mouseSensitivityCurve
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___mouseSensitivityCurve_10;
	// System.Single UnityTemplateProjects.SimpleCameraController::rotationLerpTime
	float ___rotationLerpTime_11;
	// System.Boolean UnityTemplateProjects.SimpleCameraController::invertY
	bool ___invertY_12;

public:
	inline static int32_t get_offset_of_m_TargetCameraState_5() { return static_cast<int32_t>(offsetof(SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2, ___m_TargetCameraState_5)); }
	inline CameraState_t3CFB67357E39129414AC5C9D628B9B47015CCE8C * get_m_TargetCameraState_5() const { return ___m_TargetCameraState_5; }
	inline CameraState_t3CFB67357E39129414AC5C9D628B9B47015CCE8C ** get_address_of_m_TargetCameraState_5() { return &___m_TargetCameraState_5; }
	inline void set_m_TargetCameraState_5(CameraState_t3CFB67357E39129414AC5C9D628B9B47015CCE8C * value)
	{
		___m_TargetCameraState_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TargetCameraState_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_InterpolatingCameraState_6() { return static_cast<int32_t>(offsetof(SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2, ___m_InterpolatingCameraState_6)); }
	inline CameraState_t3CFB67357E39129414AC5C9D628B9B47015CCE8C * get_m_InterpolatingCameraState_6() const { return ___m_InterpolatingCameraState_6; }
	inline CameraState_t3CFB67357E39129414AC5C9D628B9B47015CCE8C ** get_address_of_m_InterpolatingCameraState_6() { return &___m_InterpolatingCameraState_6; }
	inline void set_m_InterpolatingCameraState_6(CameraState_t3CFB67357E39129414AC5C9D628B9B47015CCE8C * value)
	{
		___m_InterpolatingCameraState_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InterpolatingCameraState_6), (void*)value);
	}

	inline static int32_t get_offset_of_boost_7() { return static_cast<int32_t>(offsetof(SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2, ___boost_7)); }
	inline float get_boost_7() const { return ___boost_7; }
	inline float* get_address_of_boost_7() { return &___boost_7; }
	inline void set_boost_7(float value)
	{
		___boost_7 = value;
	}

	inline static int32_t get_offset_of_positionLerpTime_8() { return static_cast<int32_t>(offsetof(SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2, ___positionLerpTime_8)); }
	inline float get_positionLerpTime_8() const { return ___positionLerpTime_8; }
	inline float* get_address_of_positionLerpTime_8() { return &___positionLerpTime_8; }
	inline void set_positionLerpTime_8(float value)
	{
		___positionLerpTime_8 = value;
	}

	inline static int32_t get_offset_of_mouseSensitivity_9() { return static_cast<int32_t>(offsetof(SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2, ___mouseSensitivity_9)); }
	inline float get_mouseSensitivity_9() const { return ___mouseSensitivity_9; }
	inline float* get_address_of_mouseSensitivity_9() { return &___mouseSensitivity_9; }
	inline void set_mouseSensitivity_9(float value)
	{
		___mouseSensitivity_9 = value;
	}

	inline static int32_t get_offset_of_mouseSensitivityCurve_10() { return static_cast<int32_t>(offsetof(SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2, ___mouseSensitivityCurve_10)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_mouseSensitivityCurve_10() const { return ___mouseSensitivityCurve_10; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_mouseSensitivityCurve_10() { return &___mouseSensitivityCurve_10; }
	inline void set_mouseSensitivityCurve_10(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___mouseSensitivityCurve_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mouseSensitivityCurve_10), (void*)value);
	}

	inline static int32_t get_offset_of_rotationLerpTime_11() { return static_cast<int32_t>(offsetof(SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2, ___rotationLerpTime_11)); }
	inline float get_rotationLerpTime_11() const { return ___rotationLerpTime_11; }
	inline float* get_address_of_rotationLerpTime_11() { return &___rotationLerpTime_11; }
	inline void set_rotationLerpTime_11(float value)
	{
		___rotationLerpTime_11 = value;
	}

	inline static int32_t get_offset_of_invertY_12() { return static_cast<int32_t>(offsetof(SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2, ___invertY_12)); }
	inline bool get_invertY_12() const { return ___invertY_12; }
	inline bool* get_address_of_invertY_12() { return &___invertY_12; }
	inline void set_invertY_12(bool value)
	{
		___invertY_12 = value;
	}
};


// Spawn
struct Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Vector3 Spawn::transformScale
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___transformScale_4;
	// System.Single Spawn::minTime
	float ___minTime_5;
	// System.Single Spawn::maxTime
	float ___maxTime_6;
	// System.Single Spawn::scaleTime
	float ___scaleTime_7;
	// System.Boolean Spawn::scaleObject
	bool ___scaleObject_8;
	// Spawn/ScaleStatus Spawn::<CurrentStatus>k__BackingField
	int32_t ___U3CCurrentStatusU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_transformScale_4() { return static_cast<int32_t>(offsetof(Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2, ___transformScale_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_transformScale_4() const { return ___transformScale_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_transformScale_4() { return &___transformScale_4; }
	inline void set_transformScale_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___transformScale_4 = value;
	}

	inline static int32_t get_offset_of_minTime_5() { return static_cast<int32_t>(offsetof(Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2, ___minTime_5)); }
	inline float get_minTime_5() const { return ___minTime_5; }
	inline float* get_address_of_minTime_5() { return &___minTime_5; }
	inline void set_minTime_5(float value)
	{
		___minTime_5 = value;
	}

	inline static int32_t get_offset_of_maxTime_6() { return static_cast<int32_t>(offsetof(Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2, ___maxTime_6)); }
	inline float get_maxTime_6() const { return ___maxTime_6; }
	inline float* get_address_of_maxTime_6() { return &___maxTime_6; }
	inline void set_maxTime_6(float value)
	{
		___maxTime_6 = value;
	}

	inline static int32_t get_offset_of_scaleTime_7() { return static_cast<int32_t>(offsetof(Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2, ___scaleTime_7)); }
	inline float get_scaleTime_7() const { return ___scaleTime_7; }
	inline float* get_address_of_scaleTime_7() { return &___scaleTime_7; }
	inline void set_scaleTime_7(float value)
	{
		___scaleTime_7 = value;
	}

	inline static int32_t get_offset_of_scaleObject_8() { return static_cast<int32_t>(offsetof(Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2, ___scaleObject_8)); }
	inline bool get_scaleObject_8() const { return ___scaleObject_8; }
	inline bool* get_address_of_scaleObject_8() { return &___scaleObject_8; }
	inline void set_scaleObject_8(bool value)
	{
		___scaleObject_8 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentStatusU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2, ___U3CCurrentStatusU3Ek__BackingField_9)); }
	inline int32_t get_U3CCurrentStatusU3Ek__BackingField_9() const { return ___U3CCurrentStatusU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CCurrentStatusU3Ek__BackingField_9() { return &___U3CCurrentStatusU3Ek__BackingField_9; }
	inline void set_U3CCurrentStatusU3Ek__BackingField_9(int32_t value)
	{
		___U3CCurrentStatusU3Ek__BackingField_9 = value;
	}
};


// SplashWorld
struct SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Camera SplashWorld::pageCamera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___pageCamera_4;
	// UnityEngine.LayerMask SplashWorld::raycastLayers
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___raycastLayers_5;
	// System.Single SplashWorld::raycastDistance
	float ___raycastDistance_6;
	// System.Boolean SplashWorld::mirrorARCamera
	bool ___mirrorARCamera_7;
	// System.Single SplashWorld::ARCameraMoveFactor
	float ___ARCameraMoveFactor_8;
	// UnityEngine.Pose SplashWorld::cameraStartPose
	Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  ___cameraStartPose_9;
	// UnityEngine.Light SplashWorld::pageLight
	Light_tA2F349FE839781469A0344CF6039B51512394275 * ___pageLight_10;
	// UnityEngine.AudioSource SplashWorld::musicSource
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___musicSource_11;
	// UnityEngine.AudioSource SplashWorld::sfxSource
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___sfxSource_12;
	// UnityEngine.AudioClip SplashWorld::gameMusic
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___gameMusic_13;
	// UnityEngine.AudioClip SplashWorld::startGameAudio
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___startGameAudio_14;
	// UnityEngine.AudioClip SplashWorld::completeGameAudio
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___completeGameAudio_15;
	// UnityEngine.Transform SplashWorld::exitObject
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___exitObject_16;

public:
	inline static int32_t get_offset_of_pageCamera_4() { return static_cast<int32_t>(offsetof(SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC, ___pageCamera_4)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_pageCamera_4() const { return ___pageCamera_4; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_pageCamera_4() { return &___pageCamera_4; }
	inline void set_pageCamera_4(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___pageCamera_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pageCamera_4), (void*)value);
	}

	inline static int32_t get_offset_of_raycastLayers_5() { return static_cast<int32_t>(offsetof(SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC, ___raycastLayers_5)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_raycastLayers_5() const { return ___raycastLayers_5; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_raycastLayers_5() { return &___raycastLayers_5; }
	inline void set_raycastLayers_5(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___raycastLayers_5 = value;
	}

	inline static int32_t get_offset_of_raycastDistance_6() { return static_cast<int32_t>(offsetof(SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC, ___raycastDistance_6)); }
	inline float get_raycastDistance_6() const { return ___raycastDistance_6; }
	inline float* get_address_of_raycastDistance_6() { return &___raycastDistance_6; }
	inline void set_raycastDistance_6(float value)
	{
		___raycastDistance_6 = value;
	}

	inline static int32_t get_offset_of_mirrorARCamera_7() { return static_cast<int32_t>(offsetof(SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC, ___mirrorARCamera_7)); }
	inline bool get_mirrorARCamera_7() const { return ___mirrorARCamera_7; }
	inline bool* get_address_of_mirrorARCamera_7() { return &___mirrorARCamera_7; }
	inline void set_mirrorARCamera_7(bool value)
	{
		___mirrorARCamera_7 = value;
	}

	inline static int32_t get_offset_of_ARCameraMoveFactor_8() { return static_cast<int32_t>(offsetof(SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC, ___ARCameraMoveFactor_8)); }
	inline float get_ARCameraMoveFactor_8() const { return ___ARCameraMoveFactor_8; }
	inline float* get_address_of_ARCameraMoveFactor_8() { return &___ARCameraMoveFactor_8; }
	inline void set_ARCameraMoveFactor_8(float value)
	{
		___ARCameraMoveFactor_8 = value;
	}

	inline static int32_t get_offset_of_cameraStartPose_9() { return static_cast<int32_t>(offsetof(SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC, ___cameraStartPose_9)); }
	inline Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  get_cameraStartPose_9() const { return ___cameraStartPose_9; }
	inline Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A * get_address_of_cameraStartPose_9() { return &___cameraStartPose_9; }
	inline void set_cameraStartPose_9(Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  value)
	{
		___cameraStartPose_9 = value;
	}

	inline static int32_t get_offset_of_pageLight_10() { return static_cast<int32_t>(offsetof(SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC, ___pageLight_10)); }
	inline Light_tA2F349FE839781469A0344CF6039B51512394275 * get_pageLight_10() const { return ___pageLight_10; }
	inline Light_tA2F349FE839781469A0344CF6039B51512394275 ** get_address_of_pageLight_10() { return &___pageLight_10; }
	inline void set_pageLight_10(Light_tA2F349FE839781469A0344CF6039B51512394275 * value)
	{
		___pageLight_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pageLight_10), (void*)value);
	}

	inline static int32_t get_offset_of_musicSource_11() { return static_cast<int32_t>(offsetof(SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC, ___musicSource_11)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_musicSource_11() const { return ___musicSource_11; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_musicSource_11() { return &___musicSource_11; }
	inline void set_musicSource_11(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___musicSource_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___musicSource_11), (void*)value);
	}

	inline static int32_t get_offset_of_sfxSource_12() { return static_cast<int32_t>(offsetof(SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC, ___sfxSource_12)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_sfxSource_12() const { return ___sfxSource_12; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_sfxSource_12() { return &___sfxSource_12; }
	inline void set_sfxSource_12(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___sfxSource_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sfxSource_12), (void*)value);
	}

	inline static int32_t get_offset_of_gameMusic_13() { return static_cast<int32_t>(offsetof(SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC, ___gameMusic_13)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get_gameMusic_13() const { return ___gameMusic_13; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of_gameMusic_13() { return &___gameMusic_13; }
	inline void set_gameMusic_13(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		___gameMusic_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gameMusic_13), (void*)value);
	}

	inline static int32_t get_offset_of_startGameAudio_14() { return static_cast<int32_t>(offsetof(SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC, ___startGameAudio_14)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get_startGameAudio_14() const { return ___startGameAudio_14; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of_startGameAudio_14() { return &___startGameAudio_14; }
	inline void set_startGameAudio_14(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		___startGameAudio_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___startGameAudio_14), (void*)value);
	}

	inline static int32_t get_offset_of_completeGameAudio_15() { return static_cast<int32_t>(offsetof(SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC, ___completeGameAudio_15)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get_completeGameAudio_15() const { return ___completeGameAudio_15; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of_completeGameAudio_15() { return &___completeGameAudio_15; }
	inline void set_completeGameAudio_15(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		___completeGameAudio_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___completeGameAudio_15), (void*)value);
	}

	inline static int32_t get_offset_of_exitObject_16() { return static_cast<int32_t>(offsetof(SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC, ___exitObject_16)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_exitObject_16() const { return ___exitObject_16; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_exitObject_16() { return &___exitObject_16; }
	inline void set_exitObject_16(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___exitObject_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___exitObject_16), (void*)value);
	}
};


// SplashWorldManager
struct SplashWorldManager_t07B30210F464581EE66482A3AF462999274DDF56  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// SplashWorld SplashWorldManager::currentSplashWorld
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC * ___currentSplashWorld_4;

public:
	inline static int32_t get_offset_of_currentSplashWorld_4() { return static_cast<int32_t>(offsetof(SplashWorldManager_t07B30210F464581EE66482A3AF462999274DDF56, ___currentSplashWorld_4)); }
	inline SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC * get_currentSplashWorld_4() const { return ___currentSplashWorld_4; }
	inline SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC ** get_address_of_currentSplashWorld_4() { return &___currentSplashWorld_4; }
	inline void set_currentSplashWorld_4(SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC * value)
	{
		___currentSplashWorld_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentSplashWorld_4), (void*)value);
	}
};


// SplashWorldTapThrow
struct SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean SplashWorldTapThrow::tapped
	bool ___tapped_4;
	// System.Single SplashWorldTapThrow::draggingSpeed
	float ___draggingSpeed_5;
	// System.Single SplashWorldTapThrow::maxDragDistance
	float ___maxDragDistance_6;
	// System.Single SplashWorldTapThrow::maxVelocity
	float ___maxVelocity_7;
	// System.Boolean SplashWorldTapThrow::usePhysicsForThrow
	bool ___usePhysicsForThrow_8;
	// System.Single SplashWorldTapThrow::throwTime
	float ___throwTime_9;
	// System.Single SplashWorldTapThrow::throwLiftY
	float ___throwLiftY_10;
	// System.Single SplashWorldTapThrow::maxThrowLiftY
	float ___maxThrowLiftY_11;
	// System.Single SplashWorldTapThrow::throwSpeedY
	float ___throwSpeedY_12;
	// System.Single SplashWorldTapThrow::throwForceFactor
	float ___throwForceFactor_13;
	// UnityEngine.Vector3 SplashWorldTapThrow::tapPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___tapPosition_14;
	// System.Boolean SplashWorldTapThrow::hittingThrowTarget
	bool ___hittingThrowTarget_15;
	// System.Single SplashWorldTapThrow::disappearTime
	float ___disappearTime_16;
	// System.Single SplashWorldTapThrow::snapTime
	float ___snapTime_17;
	// System.Int32 SplashWorldTapThrow::throwTweenX
	int32_t ___throwTweenX_18;
	// System.Int32 SplashWorldTapThrow::throwTweenY
	int32_t ___throwTweenY_19;
	// System.Int32 SplashWorldTapThrow::throwTweenZ
	int32_t ___throwTweenZ_20;
	// System.Int32 SplashWorldTapThrow::coinCollectedOnTap
	int32_t ___coinCollectedOnTap_21;
	// System.Int32 SplashWorldTapThrow::coinCollectedOnThrow
	int32_t ___coinCollectedOnThrow_22;
	// UnityEngine.Rigidbody SplashWorldTapThrow::rb
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___rb_23;
	// UnityEngine.AudioSource SplashWorldTapThrow::starfishTapVO
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___starfishTapVO_24;
	// UnityEngine.AudioSource SplashWorldTapThrow::starfishThrowVO
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___starfishThrowVO_25;
	// UnityEngine.Animator SplashWorldTapThrow::Animation
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___Animation_26;
	// UnityEngine.GameObject SplashWorldTapThrow::colliderName
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___colliderName_27;
	// System.Boolean SplashWorldTapThrow::tapDone
	bool ___tapDone_28;
	// System.Boolean SplashWorldTapThrow::throwDone
	bool ___throwDone_29;

public:
	inline static int32_t get_offset_of_tapped_4() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___tapped_4)); }
	inline bool get_tapped_4() const { return ___tapped_4; }
	inline bool* get_address_of_tapped_4() { return &___tapped_4; }
	inline void set_tapped_4(bool value)
	{
		___tapped_4 = value;
	}

	inline static int32_t get_offset_of_draggingSpeed_5() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___draggingSpeed_5)); }
	inline float get_draggingSpeed_5() const { return ___draggingSpeed_5; }
	inline float* get_address_of_draggingSpeed_5() { return &___draggingSpeed_5; }
	inline void set_draggingSpeed_5(float value)
	{
		___draggingSpeed_5 = value;
	}

	inline static int32_t get_offset_of_maxDragDistance_6() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___maxDragDistance_6)); }
	inline float get_maxDragDistance_6() const { return ___maxDragDistance_6; }
	inline float* get_address_of_maxDragDistance_6() { return &___maxDragDistance_6; }
	inline void set_maxDragDistance_6(float value)
	{
		___maxDragDistance_6 = value;
	}

	inline static int32_t get_offset_of_maxVelocity_7() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___maxVelocity_7)); }
	inline float get_maxVelocity_7() const { return ___maxVelocity_7; }
	inline float* get_address_of_maxVelocity_7() { return &___maxVelocity_7; }
	inline void set_maxVelocity_7(float value)
	{
		___maxVelocity_7 = value;
	}

	inline static int32_t get_offset_of_usePhysicsForThrow_8() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___usePhysicsForThrow_8)); }
	inline bool get_usePhysicsForThrow_8() const { return ___usePhysicsForThrow_8; }
	inline bool* get_address_of_usePhysicsForThrow_8() { return &___usePhysicsForThrow_8; }
	inline void set_usePhysicsForThrow_8(bool value)
	{
		___usePhysicsForThrow_8 = value;
	}

	inline static int32_t get_offset_of_throwTime_9() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___throwTime_9)); }
	inline float get_throwTime_9() const { return ___throwTime_9; }
	inline float* get_address_of_throwTime_9() { return &___throwTime_9; }
	inline void set_throwTime_9(float value)
	{
		___throwTime_9 = value;
	}

	inline static int32_t get_offset_of_throwLiftY_10() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___throwLiftY_10)); }
	inline float get_throwLiftY_10() const { return ___throwLiftY_10; }
	inline float* get_address_of_throwLiftY_10() { return &___throwLiftY_10; }
	inline void set_throwLiftY_10(float value)
	{
		___throwLiftY_10 = value;
	}

	inline static int32_t get_offset_of_maxThrowLiftY_11() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___maxThrowLiftY_11)); }
	inline float get_maxThrowLiftY_11() const { return ___maxThrowLiftY_11; }
	inline float* get_address_of_maxThrowLiftY_11() { return &___maxThrowLiftY_11; }
	inline void set_maxThrowLiftY_11(float value)
	{
		___maxThrowLiftY_11 = value;
	}

	inline static int32_t get_offset_of_throwSpeedY_12() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___throwSpeedY_12)); }
	inline float get_throwSpeedY_12() const { return ___throwSpeedY_12; }
	inline float* get_address_of_throwSpeedY_12() { return &___throwSpeedY_12; }
	inline void set_throwSpeedY_12(float value)
	{
		___throwSpeedY_12 = value;
	}

	inline static int32_t get_offset_of_throwForceFactor_13() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___throwForceFactor_13)); }
	inline float get_throwForceFactor_13() const { return ___throwForceFactor_13; }
	inline float* get_address_of_throwForceFactor_13() { return &___throwForceFactor_13; }
	inline void set_throwForceFactor_13(float value)
	{
		___throwForceFactor_13 = value;
	}

	inline static int32_t get_offset_of_tapPosition_14() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___tapPosition_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_tapPosition_14() const { return ___tapPosition_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_tapPosition_14() { return &___tapPosition_14; }
	inline void set_tapPosition_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___tapPosition_14 = value;
	}

	inline static int32_t get_offset_of_hittingThrowTarget_15() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___hittingThrowTarget_15)); }
	inline bool get_hittingThrowTarget_15() const { return ___hittingThrowTarget_15; }
	inline bool* get_address_of_hittingThrowTarget_15() { return &___hittingThrowTarget_15; }
	inline void set_hittingThrowTarget_15(bool value)
	{
		___hittingThrowTarget_15 = value;
	}

	inline static int32_t get_offset_of_disappearTime_16() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___disappearTime_16)); }
	inline float get_disappearTime_16() const { return ___disappearTime_16; }
	inline float* get_address_of_disappearTime_16() { return &___disappearTime_16; }
	inline void set_disappearTime_16(float value)
	{
		___disappearTime_16 = value;
	}

	inline static int32_t get_offset_of_snapTime_17() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___snapTime_17)); }
	inline float get_snapTime_17() const { return ___snapTime_17; }
	inline float* get_address_of_snapTime_17() { return &___snapTime_17; }
	inline void set_snapTime_17(float value)
	{
		___snapTime_17 = value;
	}

	inline static int32_t get_offset_of_throwTweenX_18() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___throwTweenX_18)); }
	inline int32_t get_throwTweenX_18() const { return ___throwTweenX_18; }
	inline int32_t* get_address_of_throwTweenX_18() { return &___throwTweenX_18; }
	inline void set_throwTweenX_18(int32_t value)
	{
		___throwTweenX_18 = value;
	}

	inline static int32_t get_offset_of_throwTweenY_19() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___throwTweenY_19)); }
	inline int32_t get_throwTweenY_19() const { return ___throwTweenY_19; }
	inline int32_t* get_address_of_throwTweenY_19() { return &___throwTweenY_19; }
	inline void set_throwTweenY_19(int32_t value)
	{
		___throwTweenY_19 = value;
	}

	inline static int32_t get_offset_of_throwTweenZ_20() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___throwTweenZ_20)); }
	inline int32_t get_throwTweenZ_20() const { return ___throwTweenZ_20; }
	inline int32_t* get_address_of_throwTweenZ_20() { return &___throwTweenZ_20; }
	inline void set_throwTweenZ_20(int32_t value)
	{
		___throwTweenZ_20 = value;
	}

	inline static int32_t get_offset_of_coinCollectedOnTap_21() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___coinCollectedOnTap_21)); }
	inline int32_t get_coinCollectedOnTap_21() const { return ___coinCollectedOnTap_21; }
	inline int32_t* get_address_of_coinCollectedOnTap_21() { return &___coinCollectedOnTap_21; }
	inline void set_coinCollectedOnTap_21(int32_t value)
	{
		___coinCollectedOnTap_21 = value;
	}

	inline static int32_t get_offset_of_coinCollectedOnThrow_22() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___coinCollectedOnThrow_22)); }
	inline int32_t get_coinCollectedOnThrow_22() const { return ___coinCollectedOnThrow_22; }
	inline int32_t* get_address_of_coinCollectedOnThrow_22() { return &___coinCollectedOnThrow_22; }
	inline void set_coinCollectedOnThrow_22(int32_t value)
	{
		___coinCollectedOnThrow_22 = value;
	}

	inline static int32_t get_offset_of_rb_23() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___rb_23)); }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * get_rb_23() const { return ___rb_23; }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A ** get_address_of_rb_23() { return &___rb_23; }
	inline void set_rb_23(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * value)
	{
		___rb_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rb_23), (void*)value);
	}

	inline static int32_t get_offset_of_starfishTapVO_24() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___starfishTapVO_24)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_starfishTapVO_24() const { return ___starfishTapVO_24; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_starfishTapVO_24() { return &___starfishTapVO_24; }
	inline void set_starfishTapVO_24(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___starfishTapVO_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___starfishTapVO_24), (void*)value);
	}

	inline static int32_t get_offset_of_starfishThrowVO_25() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___starfishThrowVO_25)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_starfishThrowVO_25() const { return ___starfishThrowVO_25; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_starfishThrowVO_25() { return &___starfishThrowVO_25; }
	inline void set_starfishThrowVO_25(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___starfishThrowVO_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___starfishThrowVO_25), (void*)value);
	}

	inline static int32_t get_offset_of_Animation_26() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___Animation_26)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_Animation_26() const { return ___Animation_26; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_Animation_26() { return &___Animation_26; }
	inline void set_Animation_26(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___Animation_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Animation_26), (void*)value);
	}

	inline static int32_t get_offset_of_colliderName_27() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___colliderName_27)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_colliderName_27() const { return ___colliderName_27; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_colliderName_27() { return &___colliderName_27; }
	inline void set_colliderName_27(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___colliderName_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colliderName_27), (void*)value);
	}

	inline static int32_t get_offset_of_tapDone_28() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___tapDone_28)); }
	inline bool get_tapDone_28() const { return ___tapDone_28; }
	inline bool* get_address_of_tapDone_28() { return &___tapDone_28; }
	inline void set_tapDone_28(bool value)
	{
		___tapDone_28 = value;
	}

	inline static int32_t get_offset_of_throwDone_29() { return static_cast<int32_t>(offsetof(SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F, ___throwDone_29)); }
	inline bool get_throwDone_29() const { return ___throwDone_29; }
	inline bool* get_address_of_throwDone_29() { return &___throwDone_29; }
	inline void set_throwDone_29(bool value)
	{
		___throwDone_29 = value;
	}
};


// StarfishTap
struct StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean StarfishTap::throwOnTap
	bool ___throwOnTap_4;
	// System.Int32 StarfishTap::coinCollectedOnTap
	int32_t ___coinCollectedOnTap_5;
	// UnityEngine.GameObject StarfishTap::coinCollected
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___coinCollected_6;
	// UnityEngine.AudioSource StarfishTap::uiSound
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___uiSound_7;
	// UnityEngine.Animator StarfishTap::Animation
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___Animation_8;
	// UnityEngine.GameObject StarfishTap::colliderName
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___colliderName_9;
	// System.Boolean StarfishTap::tapDone
	bool ___tapDone_10;
	// UnityEngine.Transform StarfishTap::throwTarget
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___throwTarget_11;
	// UnityEngine.GameObject StarfishTap::waterSplash
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___waterSplash_12;
	// System.Single StarfishTap::throwTime
	float ___throwTime_13;
	// System.Single StarfishTap::throwLiftY
	float ___throwLiftY_14;
	// System.Int32 StarfishTap::throwTweenX
	int32_t ___throwTweenX_15;
	// System.Int32 StarfishTap::throwTweenY
	int32_t ___throwTweenY_16;
	// System.Int32 StarfishTap::throwTweenZ
	int32_t ___throwTweenZ_17;
	// System.Int32 StarfishTap::scaleTween
	int32_t ___scaleTween_18;
	// System.Collections.IEnumerator StarfishTap::coroutine
	RuntimeObject* ___coroutine_19;
	// UnityEngine.Vector3 StarfishTap::activationPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___activationPosition_20;
	// UnityEngine.GameObject StarfishTap::toSpawn
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___toSpawn_21;
	// UnityEngine.GameObject StarfishTap::toDisable
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___toDisable_22;

public:
	inline static int32_t get_offset_of_throwOnTap_4() { return static_cast<int32_t>(offsetof(StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056, ___throwOnTap_4)); }
	inline bool get_throwOnTap_4() const { return ___throwOnTap_4; }
	inline bool* get_address_of_throwOnTap_4() { return &___throwOnTap_4; }
	inline void set_throwOnTap_4(bool value)
	{
		___throwOnTap_4 = value;
	}

	inline static int32_t get_offset_of_coinCollectedOnTap_5() { return static_cast<int32_t>(offsetof(StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056, ___coinCollectedOnTap_5)); }
	inline int32_t get_coinCollectedOnTap_5() const { return ___coinCollectedOnTap_5; }
	inline int32_t* get_address_of_coinCollectedOnTap_5() { return &___coinCollectedOnTap_5; }
	inline void set_coinCollectedOnTap_5(int32_t value)
	{
		___coinCollectedOnTap_5 = value;
	}

	inline static int32_t get_offset_of_coinCollected_6() { return static_cast<int32_t>(offsetof(StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056, ___coinCollected_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_coinCollected_6() const { return ___coinCollected_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_coinCollected_6() { return &___coinCollected_6; }
	inline void set_coinCollected_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___coinCollected_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___coinCollected_6), (void*)value);
	}

	inline static int32_t get_offset_of_uiSound_7() { return static_cast<int32_t>(offsetof(StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056, ___uiSound_7)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_uiSound_7() const { return ___uiSound_7; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_uiSound_7() { return &___uiSound_7; }
	inline void set_uiSound_7(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___uiSound_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___uiSound_7), (void*)value);
	}

	inline static int32_t get_offset_of_Animation_8() { return static_cast<int32_t>(offsetof(StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056, ___Animation_8)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_Animation_8() const { return ___Animation_8; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_Animation_8() { return &___Animation_8; }
	inline void set_Animation_8(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___Animation_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Animation_8), (void*)value);
	}

	inline static int32_t get_offset_of_colliderName_9() { return static_cast<int32_t>(offsetof(StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056, ___colliderName_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_colliderName_9() const { return ___colliderName_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_colliderName_9() { return &___colliderName_9; }
	inline void set_colliderName_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___colliderName_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colliderName_9), (void*)value);
	}

	inline static int32_t get_offset_of_tapDone_10() { return static_cast<int32_t>(offsetof(StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056, ___tapDone_10)); }
	inline bool get_tapDone_10() const { return ___tapDone_10; }
	inline bool* get_address_of_tapDone_10() { return &___tapDone_10; }
	inline void set_tapDone_10(bool value)
	{
		___tapDone_10 = value;
	}

	inline static int32_t get_offset_of_throwTarget_11() { return static_cast<int32_t>(offsetof(StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056, ___throwTarget_11)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_throwTarget_11() const { return ___throwTarget_11; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_throwTarget_11() { return &___throwTarget_11; }
	inline void set_throwTarget_11(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___throwTarget_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___throwTarget_11), (void*)value);
	}

	inline static int32_t get_offset_of_waterSplash_12() { return static_cast<int32_t>(offsetof(StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056, ___waterSplash_12)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_waterSplash_12() const { return ___waterSplash_12; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_waterSplash_12() { return &___waterSplash_12; }
	inline void set_waterSplash_12(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___waterSplash_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___waterSplash_12), (void*)value);
	}

	inline static int32_t get_offset_of_throwTime_13() { return static_cast<int32_t>(offsetof(StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056, ___throwTime_13)); }
	inline float get_throwTime_13() const { return ___throwTime_13; }
	inline float* get_address_of_throwTime_13() { return &___throwTime_13; }
	inline void set_throwTime_13(float value)
	{
		___throwTime_13 = value;
	}

	inline static int32_t get_offset_of_throwLiftY_14() { return static_cast<int32_t>(offsetof(StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056, ___throwLiftY_14)); }
	inline float get_throwLiftY_14() const { return ___throwLiftY_14; }
	inline float* get_address_of_throwLiftY_14() { return &___throwLiftY_14; }
	inline void set_throwLiftY_14(float value)
	{
		___throwLiftY_14 = value;
	}

	inline static int32_t get_offset_of_throwTweenX_15() { return static_cast<int32_t>(offsetof(StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056, ___throwTweenX_15)); }
	inline int32_t get_throwTweenX_15() const { return ___throwTweenX_15; }
	inline int32_t* get_address_of_throwTweenX_15() { return &___throwTweenX_15; }
	inline void set_throwTweenX_15(int32_t value)
	{
		___throwTweenX_15 = value;
	}

	inline static int32_t get_offset_of_throwTweenY_16() { return static_cast<int32_t>(offsetof(StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056, ___throwTweenY_16)); }
	inline int32_t get_throwTweenY_16() const { return ___throwTweenY_16; }
	inline int32_t* get_address_of_throwTweenY_16() { return &___throwTweenY_16; }
	inline void set_throwTweenY_16(int32_t value)
	{
		___throwTweenY_16 = value;
	}

	inline static int32_t get_offset_of_throwTweenZ_17() { return static_cast<int32_t>(offsetof(StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056, ___throwTweenZ_17)); }
	inline int32_t get_throwTweenZ_17() const { return ___throwTweenZ_17; }
	inline int32_t* get_address_of_throwTweenZ_17() { return &___throwTweenZ_17; }
	inline void set_throwTweenZ_17(int32_t value)
	{
		___throwTweenZ_17 = value;
	}

	inline static int32_t get_offset_of_scaleTween_18() { return static_cast<int32_t>(offsetof(StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056, ___scaleTween_18)); }
	inline int32_t get_scaleTween_18() const { return ___scaleTween_18; }
	inline int32_t* get_address_of_scaleTween_18() { return &___scaleTween_18; }
	inline void set_scaleTween_18(int32_t value)
	{
		___scaleTween_18 = value;
	}

	inline static int32_t get_offset_of_coroutine_19() { return static_cast<int32_t>(offsetof(StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056, ___coroutine_19)); }
	inline RuntimeObject* get_coroutine_19() const { return ___coroutine_19; }
	inline RuntimeObject** get_address_of_coroutine_19() { return &___coroutine_19; }
	inline void set_coroutine_19(RuntimeObject* value)
	{
		___coroutine_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___coroutine_19), (void*)value);
	}

	inline static int32_t get_offset_of_activationPosition_20() { return static_cast<int32_t>(offsetof(StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056, ___activationPosition_20)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_activationPosition_20() const { return ___activationPosition_20; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_activationPosition_20() { return &___activationPosition_20; }
	inline void set_activationPosition_20(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___activationPosition_20 = value;
	}

	inline static int32_t get_offset_of_toSpawn_21() { return static_cast<int32_t>(offsetof(StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056, ___toSpawn_21)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_toSpawn_21() const { return ___toSpawn_21; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_toSpawn_21() { return &___toSpawn_21; }
	inline void set_toSpawn_21(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___toSpawn_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___toSpawn_21), (void*)value);
	}

	inline static int32_t get_offset_of_toDisable_22() { return static_cast<int32_t>(offsetof(StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056, ___toDisable_22)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_toDisable_22() const { return ___toDisable_22; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_toDisable_22() { return &___toDisable_22; }
	inline void set_toDisable_22(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___toDisable_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___toDisable_22), (void*)value);
	}
};


// DentedPixel.LTExamples.TestingUnitTests
struct TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cube1
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cube1_4;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cube2
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cube2_5;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cube3
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cube3_6;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cube4
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cube4_7;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cubeAlpha1
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeAlpha1_8;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cubeAlpha2
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeAlpha2_9;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests::eventGameObjectWasCalled
	bool ___eventGameObjectWasCalled_10;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests::eventGeneralWasCalled
	bool ___eventGeneralWasCalled_11;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests::lt1Id
	int32_t ___lt1Id_12;
	// LTDescr DentedPixel.LTExamples.TestingUnitTests::lt2
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * ___lt2_13;
	// LTDescr DentedPixel.LTExamples.TestingUnitTests::lt3
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * ___lt3_14;
	// LTDescr DentedPixel.LTExamples.TestingUnitTests::lt4
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * ___lt4_15;
	// LTDescr[] DentedPixel.LTExamples.TestingUnitTests::groupTweens
	LTDescrU5BU5D_t731554F94C09A2A31CB19EA8663E934537DF797F* ___groupTweens_16;
	// UnityEngine.GameObject[] DentedPixel.LTExamples.TestingUnitTests::groupGOs
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___groupGOs_17;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests::groupTweensCnt
	int32_t ___groupTweensCnt_18;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests::rotateRepeat
	int32_t ___rotateRepeat_19;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests::rotateRepeatAngle
	int32_t ___rotateRepeatAngle_20;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::boxNoCollider
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___boxNoCollider_21;
	// System.Single DentedPixel.LTExamples.TestingUnitTests::timeElapsedNormalTimeScale
	float ___timeElapsedNormalTimeScale_22;
	// System.Single DentedPixel.LTExamples.TestingUnitTests::timeElapsedIgnoreTimeScale
	float ___timeElapsedIgnoreTimeScale_23;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests::pauseTweenDidFinish
	bool ___pauseTweenDidFinish_24;

public:
	inline static int32_t get_offset_of_cube1_4() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___cube1_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cube1_4() const { return ___cube1_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cube1_4() { return &___cube1_4; }
	inline void set_cube1_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cube1_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cube1_4), (void*)value);
	}

	inline static int32_t get_offset_of_cube2_5() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___cube2_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cube2_5() const { return ___cube2_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cube2_5() { return &___cube2_5; }
	inline void set_cube2_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cube2_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cube2_5), (void*)value);
	}

	inline static int32_t get_offset_of_cube3_6() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___cube3_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cube3_6() const { return ___cube3_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cube3_6() { return &___cube3_6; }
	inline void set_cube3_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cube3_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cube3_6), (void*)value);
	}

	inline static int32_t get_offset_of_cube4_7() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___cube4_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cube4_7() const { return ___cube4_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cube4_7() { return &___cube4_7; }
	inline void set_cube4_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cube4_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cube4_7), (void*)value);
	}

	inline static int32_t get_offset_of_cubeAlpha1_8() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___cubeAlpha1_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeAlpha1_8() const { return ___cubeAlpha1_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeAlpha1_8() { return &___cubeAlpha1_8; }
	inline void set_cubeAlpha1_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeAlpha1_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeAlpha1_8), (void*)value);
	}

	inline static int32_t get_offset_of_cubeAlpha2_9() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___cubeAlpha2_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeAlpha2_9() const { return ___cubeAlpha2_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeAlpha2_9() { return &___cubeAlpha2_9; }
	inline void set_cubeAlpha2_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeAlpha2_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeAlpha2_9), (void*)value);
	}

	inline static int32_t get_offset_of_eventGameObjectWasCalled_10() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___eventGameObjectWasCalled_10)); }
	inline bool get_eventGameObjectWasCalled_10() const { return ___eventGameObjectWasCalled_10; }
	inline bool* get_address_of_eventGameObjectWasCalled_10() { return &___eventGameObjectWasCalled_10; }
	inline void set_eventGameObjectWasCalled_10(bool value)
	{
		___eventGameObjectWasCalled_10 = value;
	}

	inline static int32_t get_offset_of_eventGeneralWasCalled_11() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___eventGeneralWasCalled_11)); }
	inline bool get_eventGeneralWasCalled_11() const { return ___eventGeneralWasCalled_11; }
	inline bool* get_address_of_eventGeneralWasCalled_11() { return &___eventGeneralWasCalled_11; }
	inline void set_eventGeneralWasCalled_11(bool value)
	{
		___eventGeneralWasCalled_11 = value;
	}

	inline static int32_t get_offset_of_lt1Id_12() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___lt1Id_12)); }
	inline int32_t get_lt1Id_12() const { return ___lt1Id_12; }
	inline int32_t* get_address_of_lt1Id_12() { return &___lt1Id_12; }
	inline void set_lt1Id_12(int32_t value)
	{
		___lt1Id_12 = value;
	}

	inline static int32_t get_offset_of_lt2_13() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___lt2_13)); }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * get_lt2_13() const { return ___lt2_13; }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F ** get_address_of_lt2_13() { return &___lt2_13; }
	inline void set_lt2_13(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * value)
	{
		___lt2_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lt2_13), (void*)value);
	}

	inline static int32_t get_offset_of_lt3_14() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___lt3_14)); }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * get_lt3_14() const { return ___lt3_14; }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F ** get_address_of_lt3_14() { return &___lt3_14; }
	inline void set_lt3_14(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * value)
	{
		___lt3_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lt3_14), (void*)value);
	}

	inline static int32_t get_offset_of_lt4_15() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___lt4_15)); }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * get_lt4_15() const { return ___lt4_15; }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F ** get_address_of_lt4_15() { return &___lt4_15; }
	inline void set_lt4_15(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * value)
	{
		___lt4_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lt4_15), (void*)value);
	}

	inline static int32_t get_offset_of_groupTweens_16() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___groupTweens_16)); }
	inline LTDescrU5BU5D_t731554F94C09A2A31CB19EA8663E934537DF797F* get_groupTweens_16() const { return ___groupTweens_16; }
	inline LTDescrU5BU5D_t731554F94C09A2A31CB19EA8663E934537DF797F** get_address_of_groupTweens_16() { return &___groupTweens_16; }
	inline void set_groupTweens_16(LTDescrU5BU5D_t731554F94C09A2A31CB19EA8663E934537DF797F* value)
	{
		___groupTweens_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___groupTweens_16), (void*)value);
	}

	inline static int32_t get_offset_of_groupGOs_17() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___groupGOs_17)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_groupGOs_17() const { return ___groupGOs_17; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_groupGOs_17() { return &___groupGOs_17; }
	inline void set_groupGOs_17(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___groupGOs_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___groupGOs_17), (void*)value);
	}

	inline static int32_t get_offset_of_groupTweensCnt_18() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___groupTweensCnt_18)); }
	inline int32_t get_groupTweensCnt_18() const { return ___groupTweensCnt_18; }
	inline int32_t* get_address_of_groupTweensCnt_18() { return &___groupTweensCnt_18; }
	inline void set_groupTweensCnt_18(int32_t value)
	{
		___groupTweensCnt_18 = value;
	}

	inline static int32_t get_offset_of_rotateRepeat_19() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___rotateRepeat_19)); }
	inline int32_t get_rotateRepeat_19() const { return ___rotateRepeat_19; }
	inline int32_t* get_address_of_rotateRepeat_19() { return &___rotateRepeat_19; }
	inline void set_rotateRepeat_19(int32_t value)
	{
		___rotateRepeat_19 = value;
	}

	inline static int32_t get_offset_of_rotateRepeatAngle_20() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___rotateRepeatAngle_20)); }
	inline int32_t get_rotateRepeatAngle_20() const { return ___rotateRepeatAngle_20; }
	inline int32_t* get_address_of_rotateRepeatAngle_20() { return &___rotateRepeatAngle_20; }
	inline void set_rotateRepeatAngle_20(int32_t value)
	{
		___rotateRepeatAngle_20 = value;
	}

	inline static int32_t get_offset_of_boxNoCollider_21() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___boxNoCollider_21)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_boxNoCollider_21() const { return ___boxNoCollider_21; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_boxNoCollider_21() { return &___boxNoCollider_21; }
	inline void set_boxNoCollider_21(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___boxNoCollider_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___boxNoCollider_21), (void*)value);
	}

	inline static int32_t get_offset_of_timeElapsedNormalTimeScale_22() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___timeElapsedNormalTimeScale_22)); }
	inline float get_timeElapsedNormalTimeScale_22() const { return ___timeElapsedNormalTimeScale_22; }
	inline float* get_address_of_timeElapsedNormalTimeScale_22() { return &___timeElapsedNormalTimeScale_22; }
	inline void set_timeElapsedNormalTimeScale_22(float value)
	{
		___timeElapsedNormalTimeScale_22 = value;
	}

	inline static int32_t get_offset_of_timeElapsedIgnoreTimeScale_23() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___timeElapsedIgnoreTimeScale_23)); }
	inline float get_timeElapsedIgnoreTimeScale_23() const { return ___timeElapsedIgnoreTimeScale_23; }
	inline float* get_address_of_timeElapsedIgnoreTimeScale_23() { return &___timeElapsedIgnoreTimeScale_23; }
	inline void set_timeElapsedIgnoreTimeScale_23(float value)
	{
		___timeElapsedIgnoreTimeScale_23 = value;
	}

	inline static int32_t get_offset_of_pauseTweenDidFinish_24() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___pauseTweenDidFinish_24)); }
	inline bool get_pauseTweenDidFinish_24() const { return ___pauseTweenDidFinish_24; }
	inline bool* get_address_of_pauseTweenDidFinish_24() { return &___pauseTweenDidFinish_24; }
	inline void set_pauseTweenDidFinish_24(bool value)
	{
		___pauseTweenDidFinish_24 = value;
	}
};


// TestingZLegacy
struct TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.AnimationCurve TestingZLegacy::customAnimationCurve
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___customAnimationCurve_4;
	// UnityEngine.Transform TestingZLegacy::pt1
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___pt1_5;
	// UnityEngine.Transform TestingZLegacy::pt2
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___pt2_6;
	// UnityEngine.Transform TestingZLegacy::pt3
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___pt3_7;
	// UnityEngine.Transform TestingZLegacy::pt4
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___pt4_8;
	// UnityEngine.Transform TestingZLegacy::pt5
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___pt5_9;
	// System.Int32 TestingZLegacy::exampleIter
	int32_t ___exampleIter_10;
	// System.String[] TestingZLegacy::exampleFunctions
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___exampleFunctions_11;
	// System.Boolean TestingZLegacy::useEstimatedTime
	bool ___useEstimatedTime_12;
	// UnityEngine.GameObject TestingZLegacy::ltLogo
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___ltLogo_13;
	// TestingZLegacy/TimingType TestingZLegacy::timingType
	int32_t ___timingType_14;
	// System.Int32 TestingZLegacy::descrTimeScaleChangeId
	int32_t ___descrTimeScaleChangeId_15;
	// UnityEngine.Vector3 TestingZLegacy::origin
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___origin_16;

public:
	inline static int32_t get_offset_of_customAnimationCurve_4() { return static_cast<int32_t>(offsetof(TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C, ___customAnimationCurve_4)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_customAnimationCurve_4() const { return ___customAnimationCurve_4; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_customAnimationCurve_4() { return &___customAnimationCurve_4; }
	inline void set_customAnimationCurve_4(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___customAnimationCurve_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___customAnimationCurve_4), (void*)value);
	}

	inline static int32_t get_offset_of_pt1_5() { return static_cast<int32_t>(offsetof(TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C, ___pt1_5)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_pt1_5() const { return ___pt1_5; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_pt1_5() { return &___pt1_5; }
	inline void set_pt1_5(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___pt1_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pt1_5), (void*)value);
	}

	inline static int32_t get_offset_of_pt2_6() { return static_cast<int32_t>(offsetof(TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C, ___pt2_6)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_pt2_6() const { return ___pt2_6; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_pt2_6() { return &___pt2_6; }
	inline void set_pt2_6(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___pt2_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pt2_6), (void*)value);
	}

	inline static int32_t get_offset_of_pt3_7() { return static_cast<int32_t>(offsetof(TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C, ___pt3_7)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_pt3_7() const { return ___pt3_7; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_pt3_7() { return &___pt3_7; }
	inline void set_pt3_7(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___pt3_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pt3_7), (void*)value);
	}

	inline static int32_t get_offset_of_pt4_8() { return static_cast<int32_t>(offsetof(TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C, ___pt4_8)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_pt4_8() const { return ___pt4_8; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_pt4_8() { return &___pt4_8; }
	inline void set_pt4_8(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___pt4_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pt4_8), (void*)value);
	}

	inline static int32_t get_offset_of_pt5_9() { return static_cast<int32_t>(offsetof(TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C, ___pt5_9)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_pt5_9() const { return ___pt5_9; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_pt5_9() { return &___pt5_9; }
	inline void set_pt5_9(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___pt5_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pt5_9), (void*)value);
	}

	inline static int32_t get_offset_of_exampleIter_10() { return static_cast<int32_t>(offsetof(TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C, ___exampleIter_10)); }
	inline int32_t get_exampleIter_10() const { return ___exampleIter_10; }
	inline int32_t* get_address_of_exampleIter_10() { return &___exampleIter_10; }
	inline void set_exampleIter_10(int32_t value)
	{
		___exampleIter_10 = value;
	}

	inline static int32_t get_offset_of_exampleFunctions_11() { return static_cast<int32_t>(offsetof(TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C, ___exampleFunctions_11)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_exampleFunctions_11() const { return ___exampleFunctions_11; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_exampleFunctions_11() { return &___exampleFunctions_11; }
	inline void set_exampleFunctions_11(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___exampleFunctions_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___exampleFunctions_11), (void*)value);
	}

	inline static int32_t get_offset_of_useEstimatedTime_12() { return static_cast<int32_t>(offsetof(TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C, ___useEstimatedTime_12)); }
	inline bool get_useEstimatedTime_12() const { return ___useEstimatedTime_12; }
	inline bool* get_address_of_useEstimatedTime_12() { return &___useEstimatedTime_12; }
	inline void set_useEstimatedTime_12(bool value)
	{
		___useEstimatedTime_12 = value;
	}

	inline static int32_t get_offset_of_ltLogo_13() { return static_cast<int32_t>(offsetof(TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C, ___ltLogo_13)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_ltLogo_13() const { return ___ltLogo_13; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_ltLogo_13() { return &___ltLogo_13; }
	inline void set_ltLogo_13(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___ltLogo_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ltLogo_13), (void*)value);
	}

	inline static int32_t get_offset_of_timingType_14() { return static_cast<int32_t>(offsetof(TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C, ___timingType_14)); }
	inline int32_t get_timingType_14() const { return ___timingType_14; }
	inline int32_t* get_address_of_timingType_14() { return &___timingType_14; }
	inline void set_timingType_14(int32_t value)
	{
		___timingType_14 = value;
	}

	inline static int32_t get_offset_of_descrTimeScaleChangeId_15() { return static_cast<int32_t>(offsetof(TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C, ___descrTimeScaleChangeId_15)); }
	inline int32_t get_descrTimeScaleChangeId_15() const { return ___descrTimeScaleChangeId_15; }
	inline int32_t* get_address_of_descrTimeScaleChangeId_15() { return &___descrTimeScaleChangeId_15; }
	inline void set_descrTimeScaleChangeId_15(int32_t value)
	{
		___descrTimeScaleChangeId_15 = value;
	}

	inline static int32_t get_offset_of_origin_16() { return static_cast<int32_t>(offsetof(TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C, ___origin_16)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_origin_16() const { return ___origin_16; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_origin_16() { return &___origin_16; }
	inline void set_origin_16(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___origin_16 = value;
	}
};


// TestingZLegacyExt
struct TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.AnimationCurve TestingZLegacyExt::customAnimationCurve
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___customAnimationCurve_4;
	// UnityEngine.Transform TestingZLegacyExt::pt1
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___pt1_5;
	// UnityEngine.Transform TestingZLegacyExt::pt2
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___pt2_6;
	// UnityEngine.Transform TestingZLegacyExt::pt3
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___pt3_7;
	// UnityEngine.Transform TestingZLegacyExt::pt4
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___pt4_8;
	// UnityEngine.Transform TestingZLegacyExt::pt5
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___pt5_9;
	// System.Int32 TestingZLegacyExt::exampleIter
	int32_t ___exampleIter_10;
	// System.String[] TestingZLegacyExt::exampleFunctions
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___exampleFunctions_11;
	// System.Boolean TestingZLegacyExt::useEstimatedTime
	bool ___useEstimatedTime_12;
	// UnityEngine.Transform TestingZLegacyExt::ltLogo
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___ltLogo_13;
	// TestingZLegacyExt/TimingType TestingZLegacyExt::timingType
	int32_t ___timingType_14;
	// System.Int32 TestingZLegacyExt::descrTimeScaleChangeId
	int32_t ___descrTimeScaleChangeId_15;
	// UnityEngine.Vector3 TestingZLegacyExt::origin
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___origin_16;

public:
	inline static int32_t get_offset_of_customAnimationCurve_4() { return static_cast<int32_t>(offsetof(TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF, ___customAnimationCurve_4)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_customAnimationCurve_4() const { return ___customAnimationCurve_4; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_customAnimationCurve_4() { return &___customAnimationCurve_4; }
	inline void set_customAnimationCurve_4(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___customAnimationCurve_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___customAnimationCurve_4), (void*)value);
	}

	inline static int32_t get_offset_of_pt1_5() { return static_cast<int32_t>(offsetof(TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF, ___pt1_5)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_pt1_5() const { return ___pt1_5; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_pt1_5() { return &___pt1_5; }
	inline void set_pt1_5(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___pt1_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pt1_5), (void*)value);
	}

	inline static int32_t get_offset_of_pt2_6() { return static_cast<int32_t>(offsetof(TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF, ___pt2_6)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_pt2_6() const { return ___pt2_6; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_pt2_6() { return &___pt2_6; }
	inline void set_pt2_6(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___pt2_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pt2_6), (void*)value);
	}

	inline static int32_t get_offset_of_pt3_7() { return static_cast<int32_t>(offsetof(TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF, ___pt3_7)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_pt3_7() const { return ___pt3_7; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_pt3_7() { return &___pt3_7; }
	inline void set_pt3_7(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___pt3_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pt3_7), (void*)value);
	}

	inline static int32_t get_offset_of_pt4_8() { return static_cast<int32_t>(offsetof(TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF, ___pt4_8)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_pt4_8() const { return ___pt4_8; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_pt4_8() { return &___pt4_8; }
	inline void set_pt4_8(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___pt4_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pt4_8), (void*)value);
	}

	inline static int32_t get_offset_of_pt5_9() { return static_cast<int32_t>(offsetof(TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF, ___pt5_9)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_pt5_9() const { return ___pt5_9; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_pt5_9() { return &___pt5_9; }
	inline void set_pt5_9(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___pt5_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pt5_9), (void*)value);
	}

	inline static int32_t get_offset_of_exampleIter_10() { return static_cast<int32_t>(offsetof(TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF, ___exampleIter_10)); }
	inline int32_t get_exampleIter_10() const { return ___exampleIter_10; }
	inline int32_t* get_address_of_exampleIter_10() { return &___exampleIter_10; }
	inline void set_exampleIter_10(int32_t value)
	{
		___exampleIter_10 = value;
	}

	inline static int32_t get_offset_of_exampleFunctions_11() { return static_cast<int32_t>(offsetof(TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF, ___exampleFunctions_11)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_exampleFunctions_11() const { return ___exampleFunctions_11; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_exampleFunctions_11() { return &___exampleFunctions_11; }
	inline void set_exampleFunctions_11(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___exampleFunctions_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___exampleFunctions_11), (void*)value);
	}

	inline static int32_t get_offset_of_useEstimatedTime_12() { return static_cast<int32_t>(offsetof(TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF, ___useEstimatedTime_12)); }
	inline bool get_useEstimatedTime_12() const { return ___useEstimatedTime_12; }
	inline bool* get_address_of_useEstimatedTime_12() { return &___useEstimatedTime_12; }
	inline void set_useEstimatedTime_12(bool value)
	{
		___useEstimatedTime_12 = value;
	}

	inline static int32_t get_offset_of_ltLogo_13() { return static_cast<int32_t>(offsetof(TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF, ___ltLogo_13)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_ltLogo_13() const { return ___ltLogo_13; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_ltLogo_13() { return &___ltLogo_13; }
	inline void set_ltLogo_13(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___ltLogo_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ltLogo_13), (void*)value);
	}

	inline static int32_t get_offset_of_timingType_14() { return static_cast<int32_t>(offsetof(TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF, ___timingType_14)); }
	inline int32_t get_timingType_14() const { return ___timingType_14; }
	inline int32_t* get_address_of_timingType_14() { return &___timingType_14; }
	inline void set_timingType_14(int32_t value)
	{
		___timingType_14 = value;
	}

	inline static int32_t get_offset_of_descrTimeScaleChangeId_15() { return static_cast<int32_t>(offsetof(TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF, ___descrTimeScaleChangeId_15)); }
	inline int32_t get_descrTimeScaleChangeId_15() const { return ___descrTimeScaleChangeId_15; }
	inline int32_t* get_address_of_descrTimeScaleChangeId_15() { return &___descrTimeScaleChangeId_15; }
	inline void set_descrTimeScaleChangeId_15(int32_t value)
	{
		___descrTimeScaleChangeId_15 = value;
	}

	inline static int32_t get_offset_of_origin_16() { return static_cast<int32_t>(offsetof(TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF, ___origin_16)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_origin_16() const { return ___origin_16; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_origin_16() { return &___origin_16; }
	inline void set_origin_16(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___origin_16 = value;
	}
};


// ThrowSuccess
struct ThrowSuccess_t1475359A9A0DE83C41109F51CCE0A5FD95AC9CB0  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// TitleReveal
struct TitleReveal_tB142F6AD33698465EF26283C15A05F87D5289D0B  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// UIManager
struct UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// TMPro.TextMeshProUGUI UIManager::pageCompleted
	TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * ___pageCompleted_4;
	// TMPro.TextMeshProUGUI UIManager::coinTotal
	TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * ___coinTotal_5;
	// UnityEngine.AudioSource UIManager::coinsCollected
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___coinsCollected_6;

public:
	inline static int32_t get_offset_of_pageCompleted_4() { return static_cast<int32_t>(offsetof(UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858, ___pageCompleted_4)); }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * get_pageCompleted_4() const { return ___pageCompleted_4; }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 ** get_address_of_pageCompleted_4() { return &___pageCompleted_4; }
	inline void set_pageCompleted_4(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * value)
	{
		___pageCompleted_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pageCompleted_4), (void*)value);
	}

	inline static int32_t get_offset_of_coinTotal_5() { return static_cast<int32_t>(offsetof(UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858, ___coinTotal_5)); }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * get_coinTotal_5() const { return ___coinTotal_5; }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 ** get_address_of_coinTotal_5() { return &___coinTotal_5; }
	inline void set_coinTotal_5(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * value)
	{
		___coinTotal_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___coinTotal_5), (void*)value);
	}

	inline static int32_t get_offset_of_coinsCollected_6() { return static_cast<int32_t>(offsetof(UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858, ___coinsCollected_6)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_coinsCollected_6() const { return ___coinsCollected_6; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_coinsCollected_6() { return &___coinsCollected_6; }
	inline void set_coinsCollected_6(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___coinsCollected_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___coinsCollected_6), (void*)value);
	}
};


// UIScale
struct UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Vector2 UIScale::transformScale
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___transformScale_4;
	// System.Single UIScale::minTime
	float ___minTime_5;
	// System.Single UIScale::maxTime
	float ___maxTime_6;
	// System.Single UIScale::scaleTime
	float ___scaleTime_7;
	// System.Single UIScale::vector3Multiply
	float ___vector3Multiply_8;
	// System.Boolean UIScale::scaleObject
	bool ___scaleObject_9;
	// UIScale/ScaleStatus UIScale::<CurrentStatus>k__BackingField
	int32_t ___U3CCurrentStatusU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_transformScale_4() { return static_cast<int32_t>(offsetof(UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55, ___transformScale_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_transformScale_4() const { return ___transformScale_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_transformScale_4() { return &___transformScale_4; }
	inline void set_transformScale_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___transformScale_4 = value;
	}

	inline static int32_t get_offset_of_minTime_5() { return static_cast<int32_t>(offsetof(UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55, ___minTime_5)); }
	inline float get_minTime_5() const { return ___minTime_5; }
	inline float* get_address_of_minTime_5() { return &___minTime_5; }
	inline void set_minTime_5(float value)
	{
		___minTime_5 = value;
	}

	inline static int32_t get_offset_of_maxTime_6() { return static_cast<int32_t>(offsetof(UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55, ___maxTime_6)); }
	inline float get_maxTime_6() const { return ___maxTime_6; }
	inline float* get_address_of_maxTime_6() { return &___maxTime_6; }
	inline void set_maxTime_6(float value)
	{
		___maxTime_6 = value;
	}

	inline static int32_t get_offset_of_scaleTime_7() { return static_cast<int32_t>(offsetof(UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55, ___scaleTime_7)); }
	inline float get_scaleTime_7() const { return ___scaleTime_7; }
	inline float* get_address_of_scaleTime_7() { return &___scaleTime_7; }
	inline void set_scaleTime_7(float value)
	{
		___scaleTime_7 = value;
	}

	inline static int32_t get_offset_of_vector3Multiply_8() { return static_cast<int32_t>(offsetof(UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55, ___vector3Multiply_8)); }
	inline float get_vector3Multiply_8() const { return ___vector3Multiply_8; }
	inline float* get_address_of_vector3Multiply_8() { return &___vector3Multiply_8; }
	inline void set_vector3Multiply_8(float value)
	{
		___vector3Multiply_8 = value;
	}

	inline static int32_t get_offset_of_scaleObject_9() { return static_cast<int32_t>(offsetof(UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55, ___scaleObject_9)); }
	inline bool get_scaleObject_9() const { return ___scaleObject_9; }
	inline bool* get_address_of_scaleObject_9() { return &___scaleObject_9; }
	inline void set_scaleObject_9(bool value)
	{
		___scaleObject_9 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentStatusU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55, ___U3CCurrentStatusU3Ek__BackingField_10)); }
	inline int32_t get_U3CCurrentStatusU3Ek__BackingField_10() const { return ___U3CCurrentStatusU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CCurrentStatusU3Ek__BackingField_10() { return &___U3CCurrentStatusU3Ek__BackingField_10; }
	inline void set_U3CCurrentStatusU3Ek__BackingField_10(int32_t value)
	{
		___U3CCurrentStatusU3Ek__BackingField_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6475[2] = 
{
	LogoCinematic_t81BA1CD58574F59063CB8A3DB8ED9353EC8AC998::get_offset_of_lean_4(),
	LogoCinematic_t81BA1CD58574F59063CB8A3DB8ED9353EC8AC998::get_offset_of_tween_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6476[4] = 
{
	PathBezier2d_t39687CAFB63E5F8FBB8BB2EB2E30517FD6702C0B::get_offset_of_cubes_4(),
	PathBezier2d_t39687CAFB63E5F8FBB8BB2EB2E30517FD6702C0B::get_offset_of_dude1_5(),
	PathBezier2d_t39687CAFB63E5F8FBB8BB2EB2E30517FD6702C0B::get_offset_of_dude2_6(),
	PathBezier2d_t39687CAFB63E5F8FBB8BB2EB2E30517FD6702C0B::get_offset_of_visualizePath_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6477[5] = 
{
	ExampleSpline_tEFB86A02C368FD0E851EE5F9462E4155EA0BA13B::get_offset_of_trans_4(),
	ExampleSpline_tEFB86A02C368FD0E851EE5F9462E4155EA0BA13B::get_offset_of_spline_5(),
	ExampleSpline_tEFB86A02C368FD0E851EE5F9462E4155EA0BA13B::get_offset_of_ltLogo_6(),
	ExampleSpline_tEFB86A02C368FD0E851EE5F9462E4155EA0BA13B::get_offset_of_ltLogo2_7(),
	ExampleSpline_tEFB86A02C368FD0E851EE5F9462E4155EA0BA13B::get_offset_of_iter_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6478[4] = 
{
	PathSpline2d_t0E183ACECD08FCA2241D336A772AD99F6B85D247::get_offset_of_cubes_4(),
	PathSpline2d_t0E183ACECD08FCA2241D336A772AD99F6B85D247::get_offset_of_dude1_5(),
	PathSpline2d_t0E183ACECD08FCA2241D336A772AD99F6B85D247::get_offset_of_dude2_6(),
	PathSpline2d_t0E183ACECD08FCA2241D336A772AD99F6B85D247::get_offset_of_visualizePath_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6479[17] = 
{
	PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28::get_offset_of_trackTrailRenderers_4(),
	PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28::get_offset_of_car_5(),
	PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28::get_offset_of_carInternal_6(),
	PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28::get_offset_of_cubes_7(),
	PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28::get_offset_of_cubesIter_8(),
	PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28::get_offset_of_trees_9(),
	PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28::get_offset_of_treesIter_10(),
	PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28::get_offset_of_randomIterWidth_11(),
	PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28::get_offset_of_track_12(),
	PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28::get_offset_of_trackPts_13(),
	PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28::get_offset_of_zIter_14(),
	PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28::get_offset_of_carIter_15(),
	PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28::get_offset_of_carAdd_16(),
	PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28::get_offset_of_trackMaxItems_17(),
	PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28::get_offset_of_trackIter_18(),
	PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28::get_offset_of_pushTrackAhead_19(),
	PathSplineEndless_tB439E214C653AD6F883A1168FE437C6EA42C4B28::get_offset_of_randomIter_20(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6480[12] = 
{
	PathSplinePerformance_t36D2BBA36977A2DCABCA4AB139EB5F37FCBBC143::get_offset_of_trackTrailRenderers_4(),
	PathSplinePerformance_t36D2BBA36977A2DCABCA4AB139EB5F37FCBBC143::get_offset_of_car_5(),
	PathSplinePerformance_t36D2BBA36977A2DCABCA4AB139EB5F37FCBBC143::get_offset_of_carInternal_6(),
	PathSplinePerformance_t36D2BBA36977A2DCABCA4AB139EB5F37FCBBC143::get_offset_of_circleLength_7(),
	PathSplinePerformance_t36D2BBA36977A2DCABCA4AB139EB5F37FCBBC143::get_offset_of_randomRange_8(),
	PathSplinePerformance_t36D2BBA36977A2DCABCA4AB139EB5F37FCBBC143::get_offset_of_trackNodes_9(),
	PathSplinePerformance_t36D2BBA36977A2DCABCA4AB139EB5F37FCBBC143::get_offset_of_carSpeed_10(),
	PathSplinePerformance_t36D2BBA36977A2DCABCA4AB139EB5F37FCBBC143::get_offset_of_tracerSpeed_11(),
	PathSplinePerformance_t36D2BBA36977A2DCABCA4AB139EB5F37FCBBC143::get_offset_of_track_12(),
	PathSplinePerformance_t36D2BBA36977A2DCABCA4AB139EB5F37FCBBC143::get_offset_of_trackIter_13(),
	PathSplinePerformance_t36D2BBA36977A2DCABCA4AB139EB5F37FCBBC143::get_offset_of_carAdd_14(),
	PathSplinePerformance_t36D2BBA36977A2DCABCA4AB139EB5F37FCBBC143::get_offset_of_trackPosition_15(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6481[7] = 
{
	PathSplineTrack_tD6F0ED2BE052288BBFF0A20C938D993506F5B58A::get_offset_of_car_4(),
	PathSplineTrack_tD6F0ED2BE052288BBFF0A20C938D993506F5B58A::get_offset_of_carInternal_5(),
	PathSplineTrack_tD6F0ED2BE052288BBFF0A20C938D993506F5B58A::get_offset_of_trackTrailRenderers_6(),
	PathSplineTrack_tD6F0ED2BE052288BBFF0A20C938D993506F5B58A::get_offset_of_trackOnePoints_7(),
	PathSplineTrack_tD6F0ED2BE052288BBFF0A20C938D993506F5B58A::get_offset_of_track_8(),
	PathSplineTrack_tD6F0ED2BE052288BBFF0A20C938D993506F5B58A::get_offset_of_trackIter_9(),
	PathSplineTrack_tD6F0ED2BE052288BBFF0A20C938D993506F5B58A::get_offset_of_trackPosition_10(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6482[4] = 
{
	PathSplines_t5E3D3A23634F2898542C5C6F650D03565C9C2D6A::get_offset_of_trans_4(),
	PathSplines_t5E3D3A23634F2898542C5C6F650D03565C9C2D6A::get_offset_of_cr_5(),
	PathSplines_t5E3D3A23634F2898542C5C6F650D03565C9C2D6A::get_offset_of_avatar1_6(),
	PathSplines_t5E3D3A23634F2898542C5C6F650D03565C9C2D6A::get_offset_of_iter_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6484[6] = 
{
	TimingType_t45F13DECEBFB1E67A20F76FF233F367BB999C27F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6485[2] = 
{
	U3CU3Ec_t8ABF57D71EAC0C260D48F0C6C5CCC9C3D247FBEC_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t8ABF57D71EAC0C260D48F0C6C5CCC9C3D247FBEC_StaticFields::get_offset_of_U3CU3E9__20_0_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6486[13] = 
{
	TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C::get_offset_of_customAnimationCurve_4(),
	TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C::get_offset_of_pt1_5(),
	TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C::get_offset_of_pt2_6(),
	TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C::get_offset_of_pt3_7(),
	TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C::get_offset_of_pt4_8(),
	TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C::get_offset_of_pt5_9(),
	TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C::get_offset_of_exampleIter_10(),
	TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C::get_offset_of_exampleFunctions_11(),
	TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C::get_offset_of_useEstimatedTime_12(),
	TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C::get_offset_of_ltLogo_13(),
	TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C::get_offset_of_timingType_14(),
	TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C::get_offset_of_descrTimeScaleChangeId_15(),
	TestingZLegacy_t048DD5C8761C5658DDF25DBBC933B1A9261FCC4C::get_offset_of_origin_16(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6488[6] = 
{
	TimingType_t578CF99DA23D1F87B1A67A57C8C872C3E24F55E6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6489[2] = 
{
	U3CU3Ec_tD7C586F26F27190F29F8B90744BCE69E24E2B0BB_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tD7C586F26F27190F29F8B90744BCE69E24E2B0BB_StaticFields::get_offset_of_U3CU3E9__20_0_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6490[13] = 
{
	TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF::get_offset_of_customAnimationCurve_4(),
	TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF::get_offset_of_pt1_5(),
	TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF::get_offset_of_pt2_6(),
	TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF::get_offset_of_pt3_7(),
	TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF::get_offset_of_pt4_8(),
	TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF::get_offset_of_pt5_9(),
	TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF::get_offset_of_exampleIter_10(),
	TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF::get_offset_of_exampleFunctions_11(),
	TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF::get_offset_of_useEstimatedTime_12(),
	TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF::get_offset_of_ltLogo_13(),
	TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF::get_offset_of_timingType_14(),
	TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF::get_offset_of_descrTimeScaleChangeId_15(),
	TestingZLegacyExt_t3D0E14E0776F8F478C131E75388C1F2CB53554DF::get_offset_of_origin_16(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6493[3] = 
{
	U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58_StaticFields::get_offset_of_U3CU3E9__113_0_1(),
	U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58_StaticFields::get_offset_of_U3CU3E9__114_0_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6494[48] = 
{
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_toggle_0(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_useEstimatedTime_1(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_useFrames_2(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_useManualTime_3(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_usesNormalDt_4(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_hasInitiliazed_5(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_hasExtraOnCompletes_6(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_hasPhysics_7(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_onCompleteOnRepeat_8(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_onCompleteOnStart_9(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_useRecursion_10(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_ratioPassed_11(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_passed_12(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_delay_13(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_time_14(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_speed_15(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_lastVal_16(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of__id_17(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_loopCount_18(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_counter_19(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_direction_20(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_directionLast_21(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_overshoot_22(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_period_23(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_scale_24(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_destroyOnComplete_25(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_trans_26(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_fromInternal_27(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_toInternal_28(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_diff_29(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_diffDiv2_30(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_type_31(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_easeType_32(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_loopType_33(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_hasUpdateCallback_34(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_easeMethod_35(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_U3CeaseInternalU3Ek__BackingField_36(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_U3CinitInternalU3Ek__BackingField_37(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_spriteRen_38(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_rectTransform_39(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_uiText_40(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_uiImage_41(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_rawImage_42(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_sprites_43(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of__optional_44(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_StaticFields::get_offset_of_val_45(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_StaticFields::get_offset_of_dt_46(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_StaticFields::get_offset_of_newVect_47(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6495[24] = 
{
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3CtoTransU3Ek__BackingField_0(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3CpointU3Ek__BackingField_1(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3CaxisU3Ek__BackingField_2(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ClastValU3Ek__BackingField_3(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3CorigRotationU3Ek__BackingField_4(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3CpathU3Ek__BackingField_5(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3CsplineU3Ek__BackingField_6(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_animationCurve_7(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_initFrameCount_8(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_color_9(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3CltRectU3Ek__BackingField_10(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConUpdateFloatU3Ek__BackingField_11(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConUpdateFloatRatioU3Ek__BackingField_12(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConUpdateFloatObjectU3Ek__BackingField_13(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConUpdateVector2U3Ek__BackingField_14(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConUpdateVector3U3Ek__BackingField_15(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConUpdateVector3ObjectU3Ek__BackingField_16(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConUpdateColorU3Ek__BackingField_17(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConUpdateColorObjectU3Ek__BackingField_18(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConCompleteU3Ek__BackingField_19(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConCompleteObjectU3Ek__BackingField_20(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConCompleteParamU3Ek__BackingField_21(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConUpdateParamU3Ek__BackingField_22(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConStartU3Ek__BackingField_23(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6496[9] = 
{
	LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222::get_offset_of_previous_0(),
	LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222::get_offset_of_current_1(),
	LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222::get_offset_of_tween_2(),
	LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222::get_offset_of_totalDelay_3(),
	LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222::get_offset_of_timeScale_4(),
	LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222::get_offset_of_debugIter_5(),
	LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222::get_offset_of_counter_6(),
	LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222::get_offset_of_toggle_7(),
	LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222::get_offset_of__id_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6497[3] = 
{
	LeanAudioStream_tDA1B6363B48B65291F4C928A5A706C159FD13712::get_offset_of_position_0(),
	LeanAudioStream_tDA1B6363B48B65291F4C928A5A706C159FD13712::get_offset_of_audioClip_1(),
	LeanAudioStream_tDA1B6363B48B65291F4C928A5A706C159FD13712::get_offset_of_audioArr_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6498[5] = 
{
	LeanAudio_t880217F92A02C8780710C41135754D5BF8BD14FE_StaticFields::get_offset_of_MIN_FREQEUNCY_PERIOD_0(),
	LeanAudio_t880217F92A02C8780710C41135754D5BF8BD14FE_StaticFields::get_offset_of_PROCESSING_ITERATIONS_MAX_1(),
	LeanAudio_t880217F92A02C8780710C41135754D5BF8BD14FE_StaticFields::get_offset_of_generatedWaveDistances_2(),
	LeanAudio_t880217F92A02C8780710C41135754D5BF8BD14FE_StaticFields::get_offset_of_generatedWaveDistancesCount_3(),
	LeanAudio_t880217F92A02C8780710C41135754D5BF8BD14FE_StaticFields::get_offset_of_longList_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6499[5] = 
{
	LeanAudioWaveStyle_tCFD7C1D135072B754C7CBA8422AE8DA6BE8D6CC2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6500[8] = 
{
	LeanAudioOptions_t594A044DA50FEB70E6FA086157B3DB457D0692AE::get_offset_of_waveStyle_0(),
	LeanAudioOptions_t594A044DA50FEB70E6FA086157B3DB457D0692AE::get_offset_of_vibrato_1(),
	LeanAudioOptions_t594A044DA50FEB70E6FA086157B3DB457D0692AE::get_offset_of_modulation_2(),
	LeanAudioOptions_t594A044DA50FEB70E6FA086157B3DB457D0692AE::get_offset_of_frequencyRate_3(),
	LeanAudioOptions_t594A044DA50FEB70E6FA086157B3DB457D0692AE::get_offset_of_waveNoiseScale_4(),
	LeanAudioOptions_t594A044DA50FEB70E6FA086157B3DB457D0692AE::get_offset_of_waveNoiseInfluence_5(),
	LeanAudioOptions_t594A044DA50FEB70E6FA086157B3DB457D0692AE::get_offset_of_useSetData_6(),
	LeanAudioOptions_t594A044DA50FEB70E6FA086157B3DB457D0692AE::get_offset_of_stream_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6502[4] = 
{
	U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6::get_offset_of_U3CU3E1__state_0(),
	U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6::get_offset_of_U3CU3E2__current_1(),
	U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6::get_offset_of_U3CU3E4__this_2(),
	U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6::get_offset_of_U3CpauseEndTimeU3E5__2_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6503[1] = 
{
	LeanTester_t537C3FE41ED244252241E5B02F6B1BA6DA437297::get_offset_of_timeout_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6504[6] = 
{
	LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields::get_offset_of_expected_0(),
	LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields::get_offset_of_tests_1(),
	LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields::get_offset_of_passes_2(),
	LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields::get_offset_of_timeout_3(),
	LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields::get_offset_of_timeoutStarted_4(),
	LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields::get_offset_of_testsFinished_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6505[52] = 
{
	TweenAction_tB7B9473B02F7CC04ED4083C3934285274119AE0B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6506[40] = 
{
	LeanTweenType_tAE51C34373F1326AC0BB9DB0F7EF1883603D55A9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6507[11] = 
{
	LeanProp_tEF273BF6EB0960483D03A097A40313D700DE3538::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6508[3] = 
{
	U3CU3Ec__DisplayClass193_0_tAD697F408BB575C622FD5E018C1F75AB28CBAF48::get_offset_of_d_0(),
	U3CU3Ec__DisplayClass193_0_tAD697F408BB575C622FD5E018C1F75AB28CBAF48::get_offset_of_smoothTime_1(),
	U3CU3Ec__DisplayClass193_0_tAD697F408BB575C622FD5E018C1F75AB28CBAF48::get_offset_of_maxSpeed_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6509[5] = 
{
	U3CU3Ec__DisplayClass194_0_t6FE45F50B4F93FED778E7D54F0EF582EA8252814::get_offset_of_d_0(),
	U3CU3Ec__DisplayClass194_0_t6FE45F50B4F93FED778E7D54F0EF582EA8252814::get_offset_of_smoothTime_1(),
	U3CU3Ec__DisplayClass194_0_t6FE45F50B4F93FED778E7D54F0EF582EA8252814::get_offset_of_maxSpeed_2(),
	U3CU3Ec__DisplayClass194_0_t6FE45F50B4F93FED778E7D54F0EF582EA8252814::get_offset_of_friction_3(),
	U3CU3Ec__DisplayClass194_0_t6FE45F50B4F93FED778E7D54F0EF582EA8252814::get_offset_of_accelRate_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6510[6] = 
{
	U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4::get_offset_of_d_0(),
	U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4::get_offset_of_smoothTime_1(),
	U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4::get_offset_of_maxSpeed_2(),
	U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4::get_offset_of_friction_3(),
	U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4::get_offset_of_accelRate_4(),
	U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4::get_offset_of_hitDamping_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6511[2] = 
{
	U3CU3Ec__DisplayClass196_0_t4FA263E9CE532093F4513F5750D54960760E3F78::get_offset_of_d_0(),
	U3CU3Ec__DisplayClass196_0_t4FA263E9CE532093F4513F5750D54960760E3F78::get_offset_of_moveSpeed_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6512[31] = 
{
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_throwErrors_4(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_tau_5(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_PI_DIV2_6(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_sequences_7(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_tweens_8(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_tweensFinished_9(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_tweensFinishedIds_10(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_tween_11(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_tweenMaxSearch_12(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_maxTweens_13(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_maxSequences_14(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_frameRendered_15(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of__tweenEmpty_16(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_dtEstimated_17(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_dtManual_18(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_dtActual_19(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_global_counter_20(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_i_21(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_j_22(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_finishedCnt_23(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_punch_24(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_shake_25(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_maxTweenReached_26(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_startSearch_27(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_d_28(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_eventListeners_29(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_goListeners_30(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_eventsMaxSearch_31(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_EVENTS_MAX_32(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_LISTENERS_MAX_33(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_INIT_LISTENERS_MAX_34(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6514[7] = 
{
	LTBezier_tD081588F24017B409080ADD7EC92332888511C41::get_offset_of_length_0(),
	LTBezier_tD081588F24017B409080ADD7EC92332888511C41::get_offset_of_a_1(),
	LTBezier_tD081588F24017B409080ADD7EC92332888511C41::get_offset_of_aa_2(),
	LTBezier_tD081588F24017B409080ADD7EC92332888511C41::get_offset_of_bb_3(),
	LTBezier_tD081588F24017B409080ADD7EC92332888511C41::get_offset_of_cc_4(),
	LTBezier_tD081588F24017B409080ADD7EC92332888511C41::get_offset_of_len_5(),
	LTBezier_tD081588F24017B409080ADD7EC92332888511C41::get_offset_of_arcLengths_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6515[8] = 
{
	LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48::get_offset_of_pts_0(),
	LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48::get_offset_of_length_1(),
	LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48::get_offset_of_orientToPath_2(),
	LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48::get_offset_of_orientToPath2d_3(),
	LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48::get_offset_of_beziers_4(),
	LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48::get_offset_of_lengthRatio_5(),
	LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48::get_offset_of_currentBezier_6(),
	LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48::get_offset_of_previousBezier_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6516[11] = 
{
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701_StaticFields::get_offset_of_DISTANCE_COUNT_0(),
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701_StaticFields::get_offset_of_SUBLINE_COUNT_1(),
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701::get_offset_of_distance_2(),
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701::get_offset_of_constantSpeed_3(),
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701::get_offset_of_pts_4(),
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701::get_offset_of_ptsAdj_5(),
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701::get_offset_of_ptsAdjLength_6(),
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701::get_offset_of_orientToPath_7(),
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701::get_offset_of_orientToPath2d_8(),
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701::get_offset_of_numSections_9(),
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701::get_offset_of_currPt_10(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6517[21] = 
{
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of__rect_0(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_alpha_1(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_rotation_2(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_pivot_3(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_margin_4(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_relativeRect_5(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_rotateEnabled_6(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_rotateFinished_7(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_alphaEnabled_8(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_labelStr_9(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_type_10(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_style_11(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_useColor_12(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_color_13(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_fontScaleToFit_14(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_useSimpleScale_15(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_sizeByHeight_16(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_texture_17(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of__id_18(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_counter_19(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD_StaticFields::get_offset_of_colorTouched_20(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6518[2] = 
{
	LTEvent_tFD435CD71D73BB5C8DBB11E20B4E3EF3ECC428EA::get_offset_of_id_0(),
	LTEvent_tFD435CD71D73BB5C8DBB11E20B4E3EF3ECC428EA::get_offset_of_data_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6519[3] = 
{
	Element_Type_tCB9236487D710B365968D3F055C4CCCA555A6BFE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6520[12] = 
{
	LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields::get_offset_of_RECT_LEVELS_0(),
	LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields::get_offset_of_RECTS_PER_LEVEL_1(),
	LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields::get_offset_of_BUTTONS_MAX_2(),
	LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields::get_offset_of_levels_3(),
	LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields::get_offset_of_levelDepths_4(),
	LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields::get_offset_of_buttons_5(),
	LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields::get_offset_of_buttonLevels_6(),
	LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields::get_offset_of_buttonLastFrame_7(),
	LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields::get_offset_of_r_8(),
	LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields::get_offset_of_color_9(),
	LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields::get_offset_of_isGUIEnabled_10(),
	LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields::get_offset_of_global_counter_11(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6522[5] = 
{
	ImageRecognition_t3FA1D134672565B626D02FABA8BCB9732ACFAC23::get_offset_of_bookPages_4(),
	ImageRecognition_t3FA1D134672565B626D02FABA8BCB9732ACFAC23::get_offset_of_spawnedPrefabs_5(),
	ImageRecognition_t3FA1D134672565B626D02FABA8BCB9732ACFAC23::get_offset_of_m_trackedImageManager_6(),
	ImageRecognition_t3FA1D134672565B626D02FABA8BCB9732ACFAC23::get_offset_of_m_AnchorManager_7(),
	ImageRecognition_t3FA1D134672565B626D02FABA8BCB9732ACFAC23::get_offset_of_m_RaycastManager_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6523[1] = 
{
	CamFacing_tE648CA18E6FFDE13945AE3E9DC611A8E1E5A042A::get_offset_of_cameraToLookAt_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6524[1] = 
{
	CoinCollector_t3F96B98DD2A68A61E591CC0AE504CCD1CD66BEF8::get_offset_of_totalCoins_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6525[2] = 
{
	U3CU3Ec_t5B5DA66907FD15891D64D11538E77E3537A4AB4C_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t5B5DA66907FD15891D64D11538E77E3537A4AB4C_StaticFields::get_offset_of_U3CU3E9__4_0_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6526[1] = 
{
	CoinSpin_t0713B96AE94FEA28A43584F9D8F625BD66ADFEAB::get_offset_of_spinTime_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6527[2] = 
{
	Elevate_t43B06C709340190FFA7E492403BD3F3471A464AE::get_offset_of_mvDuration_4(),
	Elevate_t43B06C709340190FFA7E492403BD3F3471A464AE::get_offset_of_elevateDistance_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6528[3] = 
{
	Float_t28B5D79DA5B24D79FA3F8F8DC65BCC9791E16307::get_offset_of_Animation_4(),
	Float_t28B5D79DA5B24D79FA3F8F8DC65BCC9791E16307::get_offset_of_speed_5(),
	Float_t28B5D79DA5B24D79FA3F8F8DC65BCC9791E16307::get_offset_of_y_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6550[21] = 
{
	GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields::get_offset_of_OnARCameraPoseChanged_0(),
	GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields::get_offset_of_OnPageGameStatusChanged_1(),
	GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields::get_offset_of_OnPageGameCompleted_2(),
	GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields::get_offset_of_OnPageGameBlocked_3(),
	GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields::get_offset_of_OnPageGameLookAtRaycast_4(),
	GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields::get_offset_of_OnPageGameTapRaycast_5(),
	GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields::get_offset_of_OnSplashWorldEnter_6(),
	GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields::get_offset_of_OnSplashWorldExit_7(),
	GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields::get_offset_of_OnSplashWorldLookAtRaycast_8(),
	GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields::get_offset_of_OnSplashWorldTapRaycast_9(),
	GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields::get_offset_of_OnTapOn_10(),
	GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields::get_offset_of_OnTapMove_11(),
	GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields::get_offset_of_OnTapOff_12(),
	GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields::get_offset_of_OnFirstPageTracked_13(),
	GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields::get_offset_of_OnStarfishTapped_14(),
	GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields::get_offset_of_OnFlickableTapped_15(),
	GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields::get_offset_of_OnFlickableDragged_16(),
	GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields::get_offset_of_OnFlickableThrown_17(),
	GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields::get_offset_of_OnFadeToBlack_18(),
	GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields::get_offset_of_OnCoinsCollected_19(),
	GameEvents_t304F2D5249A807B067595CF817CA809E1051942C_StaticFields::get_offset_of_OnTotalCoinsUpdated_20(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6551[8] = 
{
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1::get_offset_of_arCamera_4(),
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1::get_offset_of_syncingARCamera_5(),
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1::get_offset_of_cameraStartPose_6(),
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1::get_offset_of_cameraCurrentPose_7(),
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1::get_offset_of_cameraPoseDelta_8(),
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1::get_offset_of_gameLight_9(),
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1::get_offset_of_arAudioListener_10(),
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1::get_offset_of_U3CCurrentPageGameU3Ek__BackingField_11(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6552[4] = 
{
	ScaleStatus_t689BFBF9A323CB919946D3D3ACFF4FC81CE9345C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6553[3] = 
{
	U3CScaleObjectU3Ed__11_t461C201F3015C43C04CB97A6D8D45A7B73627A6D::get_offset_of_U3CU3E1__state_0(),
	U3CScaleObjectU3Ed__11_t461C201F3015C43C04CB97A6D8D45A7B73627A6D::get_offset_of_U3CU3E2__current_1(),
	U3CScaleObjectU3Ed__11_t461C201F3015C43C04CB97A6D8D45A7B73627A6D::get_offset_of_U3CU3E4__this_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6554[6] = 
{
	GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607::get_offset_of_transformScale_4(),
	GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607::get_offset_of_minTime_5(),
	GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607::get_offset_of_maxTime_6(),
	GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607::get_offset_of_scaleTime_7(),
	GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607::get_offset_of_scaleObject_8(),
	GameScale_tAA0BD1AE888325E9DFE465AD5C1D908B74526607::get_offset_of_U3CCurrentStatusU3Ek__BackingField_9(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6555[4] = 
{
	U3CWaitAndActivateU3Ed__6_t503E05974A5610E1E9848BFB9321AE1CE472AE6D::get_offset_of_U3CU3E1__state_0(),
	U3CWaitAndActivateU3Ed__6_t503E05974A5610E1E9848BFB9321AE1CE472AE6D::get_offset_of_U3CU3E2__current_1(),
	U3CWaitAndActivateU3Ed__6_t503E05974A5610E1E9848BFB9321AE1CE472AE6D::get_offset_of_waitTime_2(),
	U3CWaitAndActivateU3Ed__6_t503E05974A5610E1E9848BFB9321AE1CE472AE6D::get_offset_of_U3CU3E4__this_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6556[4] = 
{
	U3CWaitAndDeactivateU3Ed__7_t0B9854B91CC5946899CC4024F6D32F274A81643C::get_offset_of_U3CU3E1__state_0(),
	U3CWaitAndDeactivateU3Ed__7_t0B9854B91CC5946899CC4024F6D32F274A81643C::get_offset_of_U3CU3E2__current_1(),
	U3CWaitAndDeactivateU3Ed__7_t0B9854B91CC5946899CC4024F6D32F274A81643C::get_offset_of_waitTime_2(),
	U3CWaitAndDeactivateU3Ed__7_t0B9854B91CC5946899CC4024F6D32F274A81643C::get_offset_of_U3CU3E4__this_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6557[5] = 
{
	Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867::get_offset_of_coroutine_4(),
	Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867::get_offset_of_coroutine2_5(),
	Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867::get_offset_of_particle_6(),
	Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867::get_offset_of_pointer_7(),
	Idle_t1A2EF4427268B81B34AB01F33308173F2B3B7867::get_offset_of_showPointer_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6558[2] = 
{
	InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A::get_offset_of_touchStartPosition_4(),
	InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A::get_offset_of_touchStartTime_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6559[6] = 
{
	MV_t50F4510A4AF6B71BA52F27D2ECFB44FBAD8A37B7::get_offset_of_videoMV_4(),
	MV_t50F4510A4AF6B71BA52F27D2ECFB44FBAD8A37B7::get_offset_of_toEnable_5(),
	MV_t50F4510A4AF6B71BA52F27D2ECFB44FBAD8A37B7::get_offset_of_toEnable2_6(),
	MV_t50F4510A4AF6B71BA52F27D2ECFB44FBAD8A37B7::get_offset_of_toDisable_7(),
	MV_t50F4510A4AF6B71BA52F27D2ECFB44FBAD8A37B7::get_offset_of_gameSFX_8(),
	MV_t50F4510A4AF6B71BA52F27D2ECFB44FBAD8A37B7::get_offset_of_gameMusic_9(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6560[7] = 
{
	OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E::get_offset_of_coinCollectedOnTap_4(),
	OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E::get_offset_of_orbTapVO_5(),
	OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E::get_offset_of_colliderName_6(),
	OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E::get_offset_of_storyAnimation_7(),
	OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E::get_offset_of_splashAnimation_8(),
	OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E::get_offset_of_storyTapVO_9(),
	OrbTap_t07F0EE2B8AEC9A7FC2A9DB30D2A5BF7B0371BC8E::get_offset_of_splashTapVO_10(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6561[5] = 
{
	PageCompletion_tC5898F7BAD8C861FFA45295E4EE89BA178AB8B54::get_offset_of_pageGame_4(),
	PageCompletion_tC5898F7BAD8C861FFA45295E4EE89BA178AB8B54::get_offset_of_completionCount_5(),
	PageCompletion_tC5898F7BAD8C861FFA45295E4EE89BA178AB8B54::get_offset_of_starfishToTap_6(),
	PageCompletion_tC5898F7BAD8C861FFA45295E4EE89BA178AB8B54::get_offset_of_toEnable_7(),
	PageCompletion_tC5898F7BAD8C861FFA45295E4EE89BA178AB8B54::get_offset_of_toEnable2_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6562[7] = 
{
	PageStatus_t019890187BA0881F3266F253DBDDE533BB3C8738::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6563[12] = 
{
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5::get_offset_of_videoMV_4(),
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5::get_offset_of_videoDuration_5(),
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5::get_offset_of_hasOnPageGame_6(),
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5::get_offset_of_splashWorld_7(),
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5::get_offset_of_completeGameAudio_8(),
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5::get_offset_of_exitObject_9(),
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5::get_offset_of_targetEventHandler_10(),
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5::get_offset_of_isTrackingImage_11(),
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5::get_offset_of_imageTargetBehaviour_12(),
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5::get_offset_of_U3CCurrentStatusU3Ek__BackingField_13(),
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5::get_offset_of_CanRepeat_14(),
	PageGame_tBA4FE7403EDF431839405BEF29C6DB77DA699EF5::get_offset_of_U3CCompletionCountU3Ek__BackingField_15(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6564[2] = 
{
	PageManager_t2117D39B2F2C5C8A4EE38E159E54B69771568945::get_offset_of_firstPageTracker_4(),
	PageManager_t2117D39B2F2C5C8A4EE38E159E54B69771568945::get_offset_of_pages_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6565[3] = 
{
	PageTracker_tF742F2BC772F15FC123563EFFEA7932454A688A2::get_offset_of_pageManager_4(),
	PageTracker_tF742F2BC772F15FC123563EFFEA7932454A688A2::get_offset_of_targetEventHandler_5(),
	PageTracker_tF742F2BC772F15FC123563EFFEA7932454A688A2::get_offset_of_idle_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6566[10] = 
{
	Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139::get_offset_of_raycastTapLayers_4(),
	Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139::get_offset_of_raycastOrbTapLayers_5(),
	Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139::get_offset_of_raycastDragLayers_6(),
	Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139::get_offset_of_raycastThrowLayers_7(),
	Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139::get_offset_of_raycastDistance_8(),
	Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139::get_offset_of_arCollider_9(),
	Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139::get_offset_of_vrCollider_10(),
	Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139::get_offset_of_throwParticles_11(),
	Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139::get_offset_of_throwOnRelease_12(),
	Raycaster_t43794C1988EA33DFAB8F90C6D4B916F29EF87139::get_offset_of_mainCam_13(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6567[4] = 
{
	ScaleStatus_tB2E9FCEE91E30E92C70DFDFC1EE30BA4FB6764F0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6568[3] = 
{
	U3CScaleObjectU3Ed__11_t4E9DC2B458A4FA829BDAE1A00561A16F1A8A40F5::get_offset_of_U3CU3E1__state_0(),
	U3CScaleObjectU3Ed__11_t4E9DC2B458A4FA829BDAE1A00561A16F1A8A40F5::get_offset_of_U3CU3E2__current_1(),
	U3CScaleObjectU3Ed__11_t4E9DC2B458A4FA829BDAE1A00561A16F1A8A40F5::get_offset_of_U3CU3E4__this_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6569[6] = 
{
	Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2::get_offset_of_transformScale_4(),
	Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2::get_offset_of_minTime_5(),
	Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2::get_offset_of_maxTime_6(),
	Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2::get_offset_of_scaleTime_7(),
	Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2::get_offset_of_scaleObject_8(),
	Spawn_t6350A7809FB1CEED445D872EDC6BECECF01D75A2::get_offset_of_U3CCurrentStatusU3Ek__BackingField_9(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6570[13] = 
{
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC::get_offset_of_pageCamera_4(),
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC::get_offset_of_raycastLayers_5(),
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC::get_offset_of_raycastDistance_6(),
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC::get_offset_of_mirrorARCamera_7(),
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC::get_offset_of_ARCameraMoveFactor_8(),
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC::get_offset_of_cameraStartPose_9(),
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC::get_offset_of_pageLight_10(),
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC::get_offset_of_musicSource_11(),
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC::get_offset_of_sfxSource_12(),
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC::get_offset_of_gameMusic_13(),
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC::get_offset_of_startGameAudio_14(),
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC::get_offset_of_completeGameAudio_15(),
	SplashWorld_t3CADF43F7346D95AFF7AAA83E484E7691F7F84FC::get_offset_of_exitObject_16(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6571[1] = 
{
	SplashWorldManager_t07B30210F464581EE66482A3AF462999274DDF56::get_offset_of_currentSplashWorld_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6572[26] = 
{
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_tapped_4(),
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_draggingSpeed_5(),
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_maxDragDistance_6(),
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_maxVelocity_7(),
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_usePhysicsForThrow_8(),
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_throwTime_9(),
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_throwLiftY_10(),
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_maxThrowLiftY_11(),
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_throwSpeedY_12(),
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_throwForceFactor_13(),
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_tapPosition_14(),
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_hittingThrowTarget_15(),
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_disappearTime_16(),
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_snapTime_17(),
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_throwTweenX_18(),
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_throwTweenY_19(),
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_throwTweenZ_20(),
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_coinCollectedOnTap_21(),
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_coinCollectedOnThrow_22(),
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_rb_23(),
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_starfishTapVO_24(),
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_starfishThrowVO_25(),
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_Animation_26(),
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_colliderName_27(),
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_tapDone_28(),
	SplashWorldTapThrow_tD74E1A8F8E1C805DF1F837D62D1285D4C305858F::get_offset_of_throwDone_29(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6573[4] = 
{
	U3CU3Ec_tDADB0F86F4A25004FFE6463D967246A9D23C87D4_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tDADB0F86F4A25004FFE6463D967246A9D23C87D4_StaticFields::get_offset_of_U3CU3E9__20_0_1(),
	U3CU3Ec_tDADB0F86F4A25004FFE6463D967246A9D23C87D4_StaticFields::get_offset_of_U3CU3E9__20_1_2(),
	U3CU3Ec_tDADB0F86F4A25004FFE6463D967246A9D23C87D4_StaticFields::get_offset_of_U3CU3E9__20_3_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6574[4] = 
{
	U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_t610B7E25C1F7E862A6CB4B61E84B0023BAA56FA9::get_offset_of_U3CU3E1__state_0(),
	U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_t610B7E25C1F7E862A6CB4B61E84B0023BAA56FA9::get_offset_of_U3CU3E2__current_1(),
	U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_t610B7E25C1F7E862A6CB4B61E84B0023BAA56FA9::get_offset_of_waitTime_2(),
	U3CU3CTapU3Eg__WaitAndActivateU7C20_2U3Ed_t610B7E25C1F7E862A6CB4B61E84B0023BAA56FA9::get_offset_of_U3CU3E4__this_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6575[19] = 
{
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056::get_offset_of_throwOnTap_4(),
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056::get_offset_of_coinCollectedOnTap_5(),
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056::get_offset_of_coinCollected_6(),
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056::get_offset_of_uiSound_7(),
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056::get_offset_of_Animation_8(),
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056::get_offset_of_colliderName_9(),
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056::get_offset_of_tapDone_10(),
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056::get_offset_of_throwTarget_11(),
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056::get_offset_of_waterSplash_12(),
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056::get_offset_of_throwTime_13(),
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056::get_offset_of_throwLiftY_14(),
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056::get_offset_of_throwTweenX_15(),
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056::get_offset_of_throwTweenY_16(),
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056::get_offset_of_throwTweenZ_17(),
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056::get_offset_of_scaleTween_18(),
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056::get_offset_of_coroutine_19(),
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056::get_offset_of_activationPosition_20(),
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056::get_offset_of_toSpawn_21(),
	StarfishTap_t0C3A7413867E25728E9362769BC39DA064A1A056::get_offset_of_toDisable_22(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6578[3] = 
{
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858::get_offset_of_pageCompleted_4(),
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858::get_offset_of_coinTotal_5(),
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858::get_offset_of_coinsCollected_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6579[4] = 
{
	ScaleStatus_t10EF7D7816F5D321088B6700F763286A44D71363::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6580[3] = 
{
	U3CScaleObjectU3Ed__12_t8DC31192573F6DFEF7FBA86C992E3587EAA46457::get_offset_of_U3CU3E1__state_0(),
	U3CScaleObjectU3Ed__12_t8DC31192573F6DFEF7FBA86C992E3587EAA46457::get_offset_of_U3CU3E2__current_1(),
	U3CScaleObjectU3Ed__12_t8DC31192573F6DFEF7FBA86C992E3587EAA46457::get_offset_of_U3CU3E4__this_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6581[7] = 
{
	UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55::get_offset_of_transformScale_4(),
	UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55::get_offset_of_minTime_5(),
	UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55::get_offset_of_maxTime_6(),
	UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55::get_offset_of_scaleTime_7(),
	UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55::get_offset_of_vector3Multiply_8(),
	UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55::get_offset_of_scaleObject_9(),
	UIScale_t049B8375292E037D71DE9E57C0DC93FD257E1F55::get_offset_of_U3CCurrentStatusU3Ek__BackingField_10(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6582[3] = 
{
	CanvasSlider_t2E4DB998D7DB7EDE9365F7E097DBCFAFEC015417::get_offset_of_videoMainMV_4(),
	CanvasSlider_t2E4DB998D7DB7EDE9365F7E097DBCFAFEC015417::get_offset_of_getSlider_5(),
	CanvasSlider_t2E4DB998D7DB7EDE9365F7E097DBCFAFEC015417::get_offset_of_slide_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6583[7] = 
{
	OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7::get_offset_of_canvasVideo_4(),
	OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7::get_offset_of_toDisable_5(),
	OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7::get_offset_of_toEnable_6(),
	OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7::get_offset_of_getSlider_7(),
	OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7::get_offset_of_stopMusic_8(),
	OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7::get_offset_of_playMusic_9(),
	OpenerVideo_tDF94926CC1A7FC0B2E197DB5296653DCD4A338C7::get_offset_of_slide_10(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6584[4] = 
{
	PlayPause_tE57D2A634C24A66387C3115F3AF1B6663BC49BEA::get_offset_of_videoPlayer_4(),
	PlayPause_tE57D2A634C24A66387C3115F3AF1B6663BC49BEA::get_offset_of_toDisable_5(),
	PlayPause_tE57D2A634C24A66387C3115F3AF1B6663BC49BEA::get_offset_of_toEnable_6(),
	PlayPause_tE57D2A634C24A66387C3115F3AF1B6663BC49BEA::get_offset_of_toEnable2_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6585[4] = 
{
	Section_t9F25FADC74C202674AFDB11AE2AC4D332DE6CA1D::get_offset_of_heading_0(),
	Section_t9F25FADC74C202674AFDB11AE2AC4D332DE6CA1D::get_offset_of_text_1(),
	Section_t9F25FADC74C202674AFDB11AE2AC4D332DE6CA1D::get_offset_of_linkText_2(),
	Section_t9F25FADC74C202674AFDB11AE2AC4D332DE6CA1D::get_offset_of_url_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6586[4] = 
{
	Readme_tD58D22679F01C9AD66EA323B2DE68BFF6DBDCEE7::get_offset_of_icon_4(),
	Readme_tD58D22679F01C9AD66EA323B2DE68BFF6DBDCEE7::get_offset_of_title_5(),
	Readme_tD58D22679F01C9AD66EA323B2DE68BFF6DBDCEE7::get_offset_of_sections_6(),
	Readme_tD58D22679F01C9AD66EA323B2DE68BFF6DBDCEE7::get_offset_of_loadedLayout_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6587[6] = 
{
	CameraState_t3CFB67357E39129414AC5C9D628B9B47015CCE8C::get_offset_of_yaw_0(),
	CameraState_t3CFB67357E39129414AC5C9D628B9B47015CCE8C::get_offset_of_pitch_1(),
	CameraState_t3CFB67357E39129414AC5C9D628B9B47015CCE8C::get_offset_of_roll_2(),
	CameraState_t3CFB67357E39129414AC5C9D628B9B47015CCE8C::get_offset_of_x_3(),
	CameraState_t3CFB67357E39129414AC5C9D628B9B47015CCE8C::get_offset_of_y_4(),
	CameraState_t3CFB67357E39129414AC5C9D628B9B47015CCE8C::get_offset_of_z_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6588[9] = 
{
	0,
	SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2::get_offset_of_m_TargetCameraState_5(),
	SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2::get_offset_of_m_InterpolatingCameraState_6(),
	SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2::get_offset_of_boost_7(),
	SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2::get_offset_of_positionLerpTime_8(),
	SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2::get_offset_of_mouseSensitivity_9(),
	SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2::get_offset_of_mouseSensitivityCurve_10(),
	SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2::get_offset_of_rotationLerpTime_11(),
	SimpleCameraController_tB65216922BEA35E0BABF3D14EC92CDEA67C47FA2::get_offset_of_invertY_12(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6590[4] = 
{
	PathBezier_t3A13E1125CFCBAC9592FD3E738EBD51E56258816::get_offset_of_trans_4(),
	PathBezier_t3A13E1125CFCBAC9592FD3E738EBD51E56258816::get_offset_of_cr_5(),
	PathBezier_t3A13E1125CFCBAC9592FD3E738EBD51E56258816::get_offset_of_avatar1_6(),
	PathBezier_t3A13E1125CFCBAC9592FD3E738EBD51E56258816::get_offset_of_iter_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6591[21] = 
{
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_cubes_1(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_tweenIds_2(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_onCompleteCount_3(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_cubeToTrans_4(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_cubeDestEnd_5(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_cubeSpline_6(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_jumpTimeId_7(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_jumpCube_8(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_zeroCube_9(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_cubeScale_10(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_cubeRotate_11(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_cubeRotateA_12(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_cubeRotateB_13(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_onStartTime_14(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_beforePos_15(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_beforePos2_16(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_totalEasingCheck_17(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_totalEasingCheckSuccess_18(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_value2UpdateCalled_19(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_U3CU3E9__21_20(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6592[2] = 
{
	U3CU3Ec__DisplayClass22_1_t041360FB309BAE73F44DAC9B4689082AC2FF5973::get_offset_of_beforeX_0(),
	U3CU3Ec__DisplayClass22_1_t041360FB309BAE73F44DAC9B4689082AC2FF5973::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6593[3] = 
{
	U3CU3Ec__DisplayClass22_2_tF5B9A247AE90B21641EE6BA256550BF6E5CBC1E8::get_offset_of_totalTweenTypeLength_0(),
	U3CU3Ec__DisplayClass22_2_tF5B9A247AE90B21641EE6BA256550BF6E5CBC1E8::get_offset_of_CSU24U3CU3E8__locals2_1(),
	U3CU3Ec__DisplayClass22_2_tF5B9A247AE90B21641EE6BA256550BF6E5CBC1E8::get_offset_of_U3CU3E9__24_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6594[7] = 
{
	U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields::get_offset_of_U3CU3E9__22_3_1(),
	U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields::get_offset_of_U3CU3E9__22_22_2(),
	U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields::get_offset_of_U3CU3E9__22_7_3(),
	U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields::get_offset_of_U3CU3E9__22_12_4(),
	U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields::get_offset_of_U3CU3E9__22_18_5(),
	U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields::get_offset_of_U3CU3E9__26_0_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6595[23] = 
{
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_pauseCount_1(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_cubeRound_2(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_onStartPos_3(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_onStartPosSpline_4(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_cubeSpline_5(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_cubeSeq_6(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_cubeBounds_7(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_didPassBounds_8(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_failPoint_9(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_setOnStartNum_10(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_setPosOnUpdate_11(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_setPosNum_12(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_hasGroupTweensCheckStarted_13(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_previousXlt4_14(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_onUpdateWasCalled_15(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_start_16(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_expectedTime_17(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_didGetCorrectOnUpdate_18(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_U3CU3E9__13_19(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_U3CU3E9__14_20(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_U3CU3E9__16_21(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_U3CU3E9__15_22(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6596[5] = 
{
	U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65::get_offset_of_U3CU3E1__state_0(),
	U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65::get_offset_of_U3CU3E2__current_1(),
	U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65::get_offset_of_U3CU3E4__this_2(),
	U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65::get_offset_of_U3CU3E8__1_3(),
	U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65::get_offset_of_U3CdescriptionMatchCountU3E5__2_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6597[8] = 
{
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523::get_offset_of_U3CU3E1__state_0(),
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523::get_offset_of_U3CU3E2__current_1(),
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523::get_offset_of_U3CU3E4__this_2(),
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523::get_offset_of_U3CcubeCountU3E5__2_3(),
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523::get_offset_of_U3CtweensAU3E5__3_4(),
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523::get_offset_of_U3CaGOsU3E5__4_5(),
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523::get_offset_of_U3CtweensBU3E5__5_6(),
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523::get_offset_of_U3CbGOsU3E5__6_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6598[3] = 
{
	U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB::get_offset_of_U3CU3E1__state_0(),
	U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB::get_offset_of_U3CU3E2__current_1(),
	U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB::get_offset_of_U3CU3E4__this_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6599[21] = 
{
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_cube1_4(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_cube2_5(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_cube3_6(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_cube4_7(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_cubeAlpha1_8(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_cubeAlpha2_9(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_eventGameObjectWasCalled_10(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_eventGeneralWasCalled_11(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_lt1Id_12(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_lt2_13(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_lt3_14(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_lt4_15(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_groupTweens_16(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_groupGOs_17(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_groupTweensCnt_18(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_rotateRepeat_19(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_rotateRepeatAngle_20(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_boxNoCollider_21(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_timeElapsedNormalTimeScale_22(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_timeElapsedIgnoreTimeScale_23(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_pauseTweenDidFinish_24(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
