t  <Q                             _ENVIRONMENTREFLECTIONS_OFF    _RECEIVE_SHADOWS_OFF$   _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A   _SPECULARHIGHLIGHTS_OFF    _SPECULAR_SETUP �  ���,      8                             xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct FGlobals_Type
{
    half4 _GlossyEnvironmentColor;
    float4 _MainLightPosition;
    half4 _MainLightColor;
};

struct UnityPerDraw_Type
{
    float4 hlslcc_mtx4x4unity_ObjectToWorld[4];
    float4 hlslcc_mtx4x4unity_WorldToObject[4];
    float4 unity_LODFade;
    half4 unity_WorldTransformParams;
    half4 unity_LightData;
    half4 unity_LightIndices[2];
    float4 unity_ProbesOcclusion;
    half4 unity_SpecCube0_HDR;
    float4 unity_LightmapST;
    float4 unity_DynamicLightmapST;
    half4 unity_SHAr;
    half4 unity_SHAg;
    half4 unity_SHAb;
    half4 unity_SHBr;
    half4 unity_SHBg;
    half4 unity_SHBb;
    half4 unity_SHC;
};

struct UnityPerMaterial_Type
{
    float4 _BaseMap_ST;
    float4 _DetailAlbedoMap_ST;
    half4 _BaseColor;
    half4 _SpecColor;
    half4 _EmissionColor;
    half _Cutoff;
    half _Smoothness;
    half _Metallic;
    half _BumpScale;
    half _Parallax;
    half _OcclusionStrength;
    half _ClearCoatMask;
    half _ClearCoatSmoothness;
    half _DetailAlbedoMapScale;
    half _DetailNormalMapScale;
    half _Surface;
};

struct Mtl_FragmentIn
{
    float2 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    half3 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    float3 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    float3 TEXCOORD5 [[ user(TEXCOORD5) ]] ;
};

struct Mtl_FragmentOut
{
    half4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
};

fragment Mtl_FragmentOut xlatMtlMain(
    constant FGlobals_Type& FGlobals [[ buffer(0) ]],
    constant UnityPerDraw_Type& UnityPerDraw [[ buffer(1) ]],
    constant UnityPerMaterial_Type& UnityPerMaterial [[ buffer(2) ]],
    sampler sampler_BaseMap [[ sampler (0) ]],
    texture2d<half, access::sample > _BaseMap [[ texture(0) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float3 u_xlat0;
    half4 u_xlat16_0;
    bool u_xlatb0;
    half3 u_xlat16_1;
    half3 u_xlat16_2;
    half3 u_xlat16_3;
    half3 u_xlat16_5;
    half u_xlat16_13;
    u_xlat0.x = dot(input.TEXCOORD5.xyz, input.TEXCOORD5.xyz);
    u_xlat0.x = max(u_xlat0.x, 1.17549435e-38);
    u_xlat16_1.x = half(rsqrt(u_xlat0.x));
    u_xlat0.xyz = float3(u_xlat16_1.xxx) * input.TEXCOORD5.xyz;
    u_xlat16_1.x = dot(input.TEXCOORD3.xyz, u_xlat0.xyz);
    u_xlat16_1.x = clamp(u_xlat16_1.x, 0.0h, 1.0h);
    u_xlat16_1.x = (-u_xlat16_1.x) + half(1.0);
    u_xlat16_1.x = u_xlat16_1.x * u_xlat16_1.x;
    u_xlat16_1.x = u_xlat16_1.x * u_xlat16_1.x;
    u_xlat16_5.x = max(UnityPerMaterial._SpecColor.y, UnityPerMaterial._SpecColor.x);
    u_xlat16_5.x = max(u_xlat16_5.x, UnityPerMaterial._SpecColor.z);
    u_xlat16_0 = _BaseMap.sample(sampler_BaseMap, input.TEXCOORD0.xy);
    u_xlat16_5.x = fma(u_xlat16_0.w, UnityPerMaterial._Smoothness, u_xlat16_5.x);
    u_xlat16_5.x = clamp(u_xlat16_5.x, 0.0h, 1.0h);
    u_xlat16_5.xyz = u_xlat16_5.xxx + (-UnityPerMaterial._SpecColor.xyz);
    u_xlat16_1.xyz = fma(u_xlat16_1.xxx, u_xlat16_5.xyz, UnityPerMaterial._SpecColor.xyz);
    u_xlat16_13 = fma((-u_xlat16_0.w), UnityPerMaterial._Smoothness, half(1.0));
    u_xlat16_2.xyz = u_xlat16_0.xyz * UnityPerMaterial._BaseColor.xyz;
    u_xlat16_13 = u_xlat16_13 * u_xlat16_13;
    u_xlat16_13 = max(u_xlat16_13, half(0.0078125));
    u_xlat16_13 = fma(u_xlat16_13, u_xlat16_13, half(1.0));
    u_xlat16_13 = half(1.0) / u_xlat16_13;
    u_xlat0.xyz = float3(u_xlat16_1.xyz) * float3(u_xlat16_13);
    u_xlat16_1.xyz = half3(u_xlat0.xyz * float3(FGlobals._GlossyEnvironmentColor.xyz));
    u_xlat16_3.xyz = (-UnityPerMaterial._SpecColor.xyz) + half3(1.0, 1.0, 1.0);
    u_xlat16_2.xyz = u_xlat16_2.xyz * u_xlat16_3.xyz;
    u_xlat16_1.xyz = fma(input.TEXCOORD1.xyz, u_xlat16_2.xyz, u_xlat16_1.xyz);
    u_xlat16_13 = dot(input.TEXCOORD3.xyz, FGlobals._MainLightPosition.xyz);
    u_xlat16_13 = clamp(u_xlat16_13, 0.0h, 1.0h);
    u_xlat16_13 = u_xlat16_13 * UnityPerDraw.unity_LightData.z;
    u_xlat16_3.xyz = half3(u_xlat16_13) * FGlobals._MainLightColor.xyz;
    output.SV_Target0.xyz = fma(u_xlat16_2.xyz, u_xlat16_3.xyz, u_xlat16_1.xyz);
    u_xlatb0 = UnityPerMaterial._Surface==half(1.0);
    output.SV_Target0.w = (u_xlatb0) ? UnityPerMaterial._BaseColor.w : half(1.0);
    return output;
}
                               FGlobals(         _GlossyEnvironmentColor                         _MainLightPosition                          _MainLightColor                             UnityPerDraw(        unity_LightData                  �          UnityPerMaterialN      
   _BaseColor                       
   _SpecColor                   (      _Smoothness                  :      _Surface                 L             _BaseMap                  FGlobals              UnityPerDraw             UnityPerMaterial              