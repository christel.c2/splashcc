using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI pageCompleted;

    [SerializeField]
    private TextMeshProUGUI coinTotal;

    [SerializeField]
    private AudioSource coinsCollected;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnEnable()
    {
        GameEvents.OnPageGameStatusChanged += OnPageGameStatusChanged;
        GameEvents.OnTotalCoinsUpdated += OnTotalCoinsUpdated;
    }

    private void OnDisable()
    {
        GameEvents.OnPageGameStatusChanged -= OnPageGameStatusChanged;
        GameEvents.OnTotalCoinsUpdated -= OnTotalCoinsUpdated;
    }

    private void OnPageGameStatusChanged(PageGame.PageStatus prevStatus, PageGame page)
    {
        if (page.CurrentStatus == PageGame.PageStatus.Completed)
        {
            pageCompleted.gameObject.SetActive(true);
        }
    }

    private void OnTotalCoinsUpdated(int totalCoins)
    {
        coinTotal.text = totalCoins.ToString();
        //coinsCollected.Play(0);
    }
}
