using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PageCompletion : MonoBehaviour

{
    public PageGame pageGame;
    private int completionCount;

    [SerializeField]
    private List<StarfishTap> starfishToTap;

    //[SerializeField]
    //private GameObject toDisable;
    [SerializeField]
    private GameObject toEnable;
    [SerializeField]
    private GameObject toEnable2;

    // Start is called before the first frame update
    void Start()
    {
        pageGame = GetComponent<PageGame>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
   
    private void OnEnable() // tap, throw, spawn (page 2-6)
    {
        GameEvents.OnPageGameTapRaycast += OnPageGameTapRaycast;
    }

    private void OnDisable()
    {
        GameEvents.OnPageGameTapRaycast -= OnPageGameTapRaycast;
    }

    private void OnPageGameTapRaycast(Transform tapped, Vector3 hitPoint)
    {
        var starfishTapped = tapped.GetComponent<StarfishTap>();
   
        if (starfishTapped != null)
        {
            if (IsAllTapped() == true) // && IsAllSpawned
            {
                PageComplete();
            }
        }
    }

    private bool IsAllTapped()
    {
        foreach(var starfish in starfishToTap)
        {
            if (!starfish.tapDone)
            {
                return false;
            }
        }
        return true;
        Debug.Log("All Is Tapped: " + pageGame.name);
    }

    // stardrop completion (title page)

    // video completion MV (page 1)

    // splash world 5 completion (page 5)

    // splash world 6 completion (page 6)

    private void PageComplete()
    {
        //completionCount++;
        pageGame.SetStatus(PageGame.PageStatus.Completed);
        Debug.Log("Page Completed: " + pageGame.name);
        //GameEvents.OnPageGameCompleted?.Invoke(pageGame, completionCount);
        //toDisable.SetActive(false);
        toEnable.SetActive(true);
        toEnable2.SetActive(true);
    }
}
