using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class to enable an object to be dragged and thrown
/// in 3D space via touch / mouse.
/// Object follows raycast from touch screen position.
/// </summary>

public class StarfishTap : MonoBehaviour
{
    public bool throwOnTap = false;            // ie. 'grabbed' by raycast

    [SerializeField]
    private int coinCollectedOnTap = 10;
    [SerializeField]
    private GameObject coinCollected;

    [SerializeField]
    private AudioSource uiSound;
    [SerializeField]
    private Animator Animation;
    [SerializeField]
    private GameObject colliderName;

    public bool tapDone;

    [SerializeField]
    private Transform throwTarget;
    [SerializeField]
    private GameObject waterSplash;

    [SerializeField]
    private float throwTime = 2f;         // time to reach destination if not physics
    [SerializeField]
    private float throwLiftY = 0.001f;        // lift speed if not physics

    private int throwTweenX;
    private int throwTweenY;
    private int throwTweenZ;
    private int scaleTween;

    private IEnumerator coroutine;
    private Vector3 activationPosition;

    [SerializeField]
    private GameObject toSpawn;
    [SerializeField]
    private GameObject toDisable;

    public void Start()
    {
        Animation = GetComponent<Animator>();
        activationPosition = transform.position;
    }

    public void Tap()
    {
        // TODO: trigger animation?  play audio?
        if (uiSound != null)
        {
            uiSound.Play(0);
        }

        if (Animation != null)
        {
            Animation.SetBool("isTap", true);
        }

        if (toSpawn != null)
        {
            toSpawn.SetActive(true);
        }


        if (toDisable != null)
        {
            toDisable.SetActive(false);
        }

        if (coinCollected != null)
        {
            coinCollected.SetActive(true);
        }

        // fire activated event
        tapDone = true;
        Debug.Log("Tap Done: " + colliderName.name);
        GameEvents.OnStarfishTapped?.Invoke(this);
        GameEvents.OnCoinsCollected?.Invoke(coinCollectedOnTap);
        Debug.Log("Coins Collected: " + coinCollectedOnTap);

        //AnimationEvent "isTapDone";
        //Animation.SetBool("isIdle", true);
        Debug.Log("Tap Reset: " + colliderName.name);

        if (throwOnTap && throwTarget != null)
        {
            //lift(y)
            throwTweenY = LeanTween.moveY(gameObject, throwTarget.position.y, throwTime) // throwLiftY
                        .setEaseInQuad()
                        //.setLoopPingPong(1)
                        .setOnComplete(() =>
                        {
                            //DropToStart();
                        }).id;

            // move z
            throwTweenZ = LeanTween.moveZ(gameObject, throwTarget.position.z, throwTime)
                        .setEaseLinear().id;
            //.setEaseInSine().id;
            //.setEaseOutSine().id;

            // move x
            throwTweenX = LeanTween.moveX(gameObject, throwTarget.position.x, throwTime)
                        .setEaseLinear()
                        //.setEaseOutSine()
                        //.setEaseInSine()
                        .setOnComplete(() =>
                        {
                        }).id;

            coroutine = WaitAndActivate(throwTime);
            StartCoroutine(coroutine);
        }
        //void DropToStart()
        //{
        //    throwTweenY = LeanTween.moveY(gameObject, activationPosition.y, throwTime / 2)
        //        .setEaseInQuad()
        //        .id;
        //}

        // wait for throwTime perform the print()
        IEnumerator WaitAndActivate(float waitTime)
        {
            //while (true)
            {
                yield return new WaitForSeconds(waitTime);
                print("WaitAndPrint " + Time.time);
                if (waterSplash != null)
                {
                    waterSplash.SetActive(true);
                }
                scaleTween = LeanTween.scale(gameObject, Vector3.one * 0, 3f)
                .setEaseInBack()
                //.setDelay(1f)
                .setOnComplete(() =>
                {
                    //Destroy(gameObject);
                }).id;
            }
        }
    }
}
