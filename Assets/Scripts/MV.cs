using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class MV : MonoBehaviour
{
    [SerializeField]
    private VideoPlayer videoMV;

    [SerializeField]
    private GameObject toEnable;

    [SerializeField]
    private GameObject toEnable2;

    [SerializeField]
    private GameObject toDisable;

    [SerializeField]
    private AudioSource gameSFX;

    [SerializeField]
    private AudioSource gameMusic;


    private void Awake()
    {
        videoMV.isLooping = false;
        videoMV = GetComponent<VideoPlayer>();
    }

    // Start is called before the first frame update
    void Start()
    {
   
    }

    // Update is called once per frame
    void Update()
    {
        // if video is finished
        if (videoMV.frame == (long)videoMV.frameCount -1)
        {
            Debug.Log(videoMV.name + " Completed");

            if (toEnable != null)
            {
                toEnable.SetActive(true);
            }

            if (toEnable2 != null)
            {
                toEnable2.SetActive(true);
            }

            toDisable.SetActive(false);

            if (gameSFX != null)
            {
                gameSFX.Play(0);
            }

            if (gameMusic != null)
            {
                gameMusic.Play(0);
            }

            // reset for standby replay
            //videoMV.frame = 0;
        }
    }
}
