using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;


[RequireComponent(typeof(ImageTargetBehaviour))]
[RequireComponent(typeof(DefaultObserverEventHandler))]
[RequireComponent(typeof(AudioSource))]

/// <summary>
/// Controls Image Tracking, (optional) on-page game and (optional) SplashWorld
/// </summary>
public class PageGame : MonoBehaviour
{
    [Header("Audio")]
    [SerializeField]
    private GameObject videoMV;
    [SerializeField]
    private Time videoDuration;
    [SerializeField]
    private bool hasOnPageGame = false;         // play game on-page?
    [SerializeField]
    private SplashWorld splashWorld;            // optional 'VR' world / game

    public bool HasSplashWorld => splashWorld != null;

    [Header("Audio")]
    //[SerializeField]
    //private AudioSource gameScore;              // looping idle game mode
    //[SerializeField]
    //private AudioSource sfxSource;
    //[SerializeField]
    //private AudioClip gameMusic;
    //[SerializeField]
    //private AudioClip startGameAudio;
    [SerializeField]
    private AudioClip completeGameAudio;        // page completion audio

    [Space]

    [SerializeField]
    private Transform exitObject;               // tap to complete page game        // TODO: temp!

    private DefaultObserverEventHandler targetEventHandler;
    private bool isTrackingImage = false;

    private ImageTargetBehaviour imageTargetBehaviour;

    public enum PageStatus
    {
        Inactive,                       // target image not been tracked
        Video,                          // play video
        PageLoad,                       // load up game objects
        OnPageGame,                     // playing on-page game
        InSplashWorld,                  // playing 'splash world' game
        Completed                       // game(s) played and completed
    }

    public PageStatus CurrentStatus { get; private set; }

    public bool CanRepeat = true;
    public int CompletionCount { get; private set; }

    private void Awake()
    {
        targetEventHandler = GetComponent<DefaultObserverEventHandler>();
        imageTargetBehaviour = GetComponent<ImageTargetBehaviour>();
    }

    private void OnEnable()
    {
        targetEventHandler.OnTargetFound.AddListener(OnTargetFound);
        targetEventHandler.OnTargetLost.AddListener(OnTargetLost);

        StartListeningForTouch();
    //    GameEvents.OnPageGameCompleted += OnPageCompleted;
    }

    private void OnDisable()
    {
        targetEventHandler.OnTargetFound.RemoveListener(OnTargetFound);
        targetEventHandler.OnTargetLost.RemoveListener(OnTargetLost);

        StopListeningForTouch();
    //    GameEvents.OnPageGameCompleted -= OnPageCompleted;
    }

    // private void OnPageCompleted(PageGame page, int completionCount)
    // {
    //    if (page == this)
    //    {
    //        SetStatus(PageStatus.Completed);
    //    }
    // }

    // start listening for in-game touch events
    private void StartListeningForTouch()
    {
        GameEvents.OnPageGameTapRaycast += OnPageGameTapRaycast;
        GameEvents.OnSplashWorldTapRaycast += OnSplashWorldTapRaycast;
    }

    // stop listening for in-game touch events
    private void StopListeningForTouch()
    {
        GameEvents.OnPageGameTapRaycast -= OnPageGameTapRaycast;
        GameEvents.OnSplashWorldTapRaycast -= OnSplashWorldTapRaycast;
    }

    // TODO: don't think this is working (probably overridden by Vuforia?)
    public void EnableImageTargetting(bool enabled)
    {
        targetEventHandler.enabled = enabled;
        imageTargetBehaviour.enabled = enabled;
    }

    private void OnPageGameTapRaycast(Transform touched, Vector3 hitPoint)
    {
        // check if exit object was touched
        if (CurrentStatus == PageStatus.OnPageGame && exitObject != null && touched == exitObject)
        {
            if (HasSplashWorld)
            {
                EnterSplashWorld();
                EnableImageTargetting(false);
            }
            else
            {
                SetStatus(PageStatus.Completed);
                EnableImageTargetting(true);
            }
        }
    }

    private void OnSplashWorldTapRaycast(Transform touched, bool isExit)
    {
        if (CurrentStatus == PageStatus.InSplashWorld && isExit)
        {
            SetStatus(PageStatus.Completed);
            EnableImageTargetting(true);

            GameEvents.OnSplashWorldExit?.Invoke(splashWorld, this);       // SplashWorldManager

            if (CanRepeat)   // TODO: repeat? 
                SetStatus(PageStatus.Inactive);
        }
    }

    // child AR objects are activated 'on-page'
    private void OnTargetFound()
    {
        if (CurrentStatus == PageStatus.Completed && !CanRepeat)
        {
            isTrackingImage = false;
            //StopListeningForTouch();

            GameEvents.OnPageGameBlocked?.Invoke(this, "You've already Played this Page!!");
            return;
        }

        isTrackingImage = true;
        //StartListeningForTouch();

        if (hasOnPageGame)
        {
            SetStatus(PageStatus.OnPageGame);
        }
        else if (HasSplashWorld)
        {
            EnterSplashWorld();
        }
    }

    // child AR objects are deactivated 'on-page'
    private void OnTargetLost()
    {
        isTrackingImage = false;
        //StopListeningForTouch();

        //Debug.Log("OnTargetLost: " + name);

        //SetStatus(PageStatus.Inactive);

        //if (splashWorld != null && CurrentStatus == PageStatus.InSplashWorld)
        //{
        //    GameEvents.OnSplashWorldExit?.Invoke(splashWorld, this);        // SplashWorldManager
        //}
    }

    public void SetStatus(PageStatus newStatus)
    {
        if (CurrentStatus == newStatus)
            return;

        var prevStatus = CurrentStatus;
        CurrentStatus = newStatus;

        switch (CurrentStatus)
        {
            case PageStatus.Inactive:
                break;

            case PageStatus.Video:
                break;

            case PageStatus.PageLoad:
                break;

            case PageStatus.OnPageGame: // these are all activated by events now
                //PlaySFX(startGameAudio);
                //PlayMusic();
                break;

            case PageStatus.InSplashWorld:
                //StopMusic();
                break;

            case PageStatus.Completed:
                if (!HasSplashWorld)        // has own audio
                //    PlaySFX(completeGameAudio);
                //StopMusic();
                CompletionCount++;
                break;

            default:
                break;

        }

        GameEvents.OnPageGameStatusChanged?.Invoke(prevStatus, this);
    }

    private void EnterSplashWorld()
    {
        if (HasSplashWorld)
        {
            SetStatus(PageStatus.InSplashWorld);
            GameEvents.OnSplashWorldEnter?.Invoke(splashWorld, this);       // SplashWorldManager
        }
    }

    //private void PlayMusic()
    //{
    //    if (gameScore != null && gameMusic != null)
    //    {
    //        gameScore.loop = true;
    //        gameScore.clip = gameMusic;
    //        gameScore.Play();
    //    }
    //}

    //private void StopMusic()
    //{
    //    if (gameScore != null && gameMusic != null)
    //    {
    //        gameScore.Stop();
    //    }
    //}

    //private void PlaySFX(AudioClip sfxAudio)
    //{
    //    if (sfxSource != null && sfxAudio != null)
    //    {
    //        sfxSource.clip = sfxAudio;
    //        sfxSource.Play();
    //    }
    //}
}
