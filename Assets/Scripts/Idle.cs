using UnityEngine;
using System.Collections;


public class Idle : MonoBehaviour
{
    // In this example we show how to invoke a coroutine and
    // continue executing the function in parallel.

    private IEnumerator coroutine;
    private IEnumerator coroutine2;

    [SerializeField]
    private GameObject particle;
    [SerializeField]
    private GameObject pointer;
  
    public bool showPointer;

    void Start()
    {
        // - After 0 seconds, prints "Starting 0.0"
        // - After 0 seconds, prints "Before WaitAndPrint Finishes 0.0"
        // - After 3 seconds, prints "WaitAndPrint 3.0"
        print("Starting " + Time.time);

        // Start function WaitAndPrint as a coroutine.

        coroutine = WaitAndActivate(4.0f);
        StartCoroutine(coroutine);

        coroutine2 = WaitAndDeactivate(5.0f);
        StartCoroutine(coroutine2);

        print("Before WaitAndPrint Finishes " + Time.time);
    }

    // every 3 seconds perform the print()
    private IEnumerator WaitAndActivate(float waitTime)
    {
        while (true)
        {
            yield return new WaitForSeconds(waitTime);
            print("WaitAndPrint " + Time.time);
            particle.SetActive(true);

            if (showPointer)
            {
                pointer.SetActive(true);
            }
        }
    }

    // every 5 seconds perform the print()
    private IEnumerator WaitAndDeactivate(float waitTime)
    {
        while (true)
        {
            yield return new WaitForSeconds(waitTime);
            print("WaitAndPrint " + Time.time);
            particle.SetActive(false);
            pointer.SetActive(false);
        }
    }
}