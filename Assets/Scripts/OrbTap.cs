using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbTap : MonoBehaviour
{

    [SerializeField]
    private int coinCollectedOnTap = 10;

    [SerializeField]
    private AudioSource orbTapVO;
    [SerializeField]
    private GameObject colliderName;

    [SerializeField]
    private Animator storyAnimation;
    [SerializeField]
    private Animator splashAnimation;

    [SerializeField]
    private AudioSource storyTapVO;
    [SerializeField]
    private AudioSource splashTapVO;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Tap()
    {
        // TODO: trigger animation?  play audio?
        orbTapVO.Play(0);
        storyTapVO.Play(0);
        splashTapVO.Play(0);
        storyAnimation.SetBool("isTap", true);
        splashAnimation.SetBool("isTap", true);
        //Animation.SetBool("isTap", true);

        // fire activated event
        //tapDone = true;
        Debug.Log("Tap Done: " + colliderName.name);
        //GameEvents.OnStarfishTapped?.Invoke(this);
        GameEvents.OnCoinsCollected?.Invoke(coinCollectedOnTap);
        Debug.Log("Coins Collected: " + coinCollectedOnTap);
    }
}
