using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpin : MonoBehaviour

{
    private float spinTime = 0.3f;

    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void OnEnable()
    {
        GameEvents.OnCoinsCollected += OnCoinsCollected;        // eg. on completion of a page
    }

    public void OnDisable()
    {
        GameEvents.OnCoinsCollected -= OnCoinsCollected;
    }

    private void OnCoinsCollected(int coinsCollected)
    {

        LeanTween.rotateAround(gameObject, Vector3.up, 720, spinTime)
            .setEaseInOutSine()
                //.setDelay(1f)
                .setOnComplete(() =>
                {
  
                });
    }

    //leentweenreference: https://easings.net

    // Update is called once per frame
    void Update()
    {
        
    }
}
