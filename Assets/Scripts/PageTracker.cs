using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PageTracker : MonoBehaviour
{
    public PageManager pageManager;

    private DefaultObserverEventHandler targetEventHandler;

    public Idle idle;

    private void Awake()
    {
        targetEventHandler = GetComponent<DefaultObserverEventHandler>();
    }

    private void OnEnable()
    {
        targetEventHandler.OnTargetFound.AddListener(OnImageTracked);
    }

    public void OnImageTracked()
    {
        Debug.Log("First Image is tracked");
        pageManager.PageTracked(this, idle);
    }

    private void OnDisable()
    {
        targetEventHandler.OnTargetFound.RemoveListener(OnImageTracked);
    }

}
