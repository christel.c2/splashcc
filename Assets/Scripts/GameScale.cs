using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameScale : MonoBehaviour
{
    private Vector3 transformScale;

    [SerializeField]
    private float minTime = 4f;

    [SerializeField]
    private float maxTime = 7f;

    private float scaleTime = 0.1f;

    [SerializeField]
    private bool scaleObject = false;

    public enum ScaleStatus
    {
        isNormal,                       // original scale
        inTransition,                   // in between shrinking or growing
        isShrunk,                       // shrunk
    }

    public ScaleStatus CurrentStatus { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        transformScale = transform.localScale;

        if (scaleObject)
        {
            StartCoroutine(ScaleObject());
        }
    }

    private IEnumerator ScaleObject()
    {
        while (true)
        {
            if (CurrentStatus == ScaleStatus.inTransition)
            {
                yield return null;
            }

            yield return new WaitForSeconds(Random.Range(minTime, maxTime));

            if (CurrentStatus == ScaleStatus.isNormal)
            {
                Shrink();
            }

            else if (CurrentStatus == ScaleStatus.isShrunk)
            {
                Grow();
            }
        }
    }

    private void Shrink()
    {
        CurrentStatus = ScaleStatus.inTransition;

        LeanTween.scale(gameObject, Vector3.zero, scaleTime)
            .setEaseInBack()
                //.setDelay(1f)
                .setOnComplete(() =>
                {
                    CurrentStatus = ScaleStatus.isShrunk;
                });
    }

    private void Grow()
    {
        CurrentStatus = ScaleStatus.inTransition;
        
        LeanTween.scale(gameObject, transformScale, scaleTime)
            .setEaseInBack()
                //.setDelay(1f)
                .setOnComplete(() =>
                {
                    CurrentStatus = ScaleStatus.isNormal;
                });
    }
}
