using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIScale : MonoBehaviour
{
    private Vector2 transformScale;

    [SerializeField]
    private float minTime;

    [SerializeField]
    private float maxTime;

    private float scaleTime = 0.5f;
    private float vector3Multiply = 1.2f;

    [SerializeField]
    private bool scaleObject = false;
    

    public enum ScaleStatus
    {
        isNormal,                       // original scale
        inTransition,                   // in between shrinking or growing
        isShrunk,                       // shrunk
    }

    public ScaleStatus CurrentStatus { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        transformScale = transform.localScale;

        if (scaleObject)
        {
            StartCoroutine(ScaleObject());
        }
    }

    private IEnumerator ScaleObject()
    {
        while (true)
        {
            if (CurrentStatus == ScaleStatus.inTransition)
            {
                yield return null;
            }

            yield return new WaitForSeconds(Random.Range(minTime, maxTime));

            if (CurrentStatus == ScaleStatus.isNormal)
            {
                Shrink();
            }

            else if (CurrentStatus == ScaleStatus.isShrunk)
            {
                Grow();
            }
        }
    }

    private void Shrink()
    {
        CurrentStatus = ScaleStatus.inTransition;

        LeanTween.scale(gameObject, Vector3.one * vector3Multiply, scaleTime)
            .setEaseInOutBack()
                //.setDelay(1f)
                .setOnComplete(() =>
                {
                    CurrentStatus = ScaleStatus.isShrunk;
                });
    }

    private void Grow()
    {
        CurrentStatus = ScaleStatus.inTransition;
        
        LeanTween.scale(gameObject, transformScale, scaleTime)
            .setEaseInOutBack()
                //.setDelay(1f)
                .setOnComplete(() =>
                {
                    CurrentStatus = ScaleStatus.isNormal;
                });
    }
}
