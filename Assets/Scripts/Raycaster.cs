using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycaster : MonoBehaviour
{
    [SerializeField]
    private LayerMask raycastTapLayers;                 // for 'activating' objects to drag / throw on touch 'on'

    [SerializeField]
    private LayerMask raycastOrbTapLayers;                 // for 'activating' objects to drag / throw on touch 'on'

    [SerializeField]
    private LayerMask raycastDragLayers;                // for dragging object towards a point (eg. on ground, through water)

    [SerializeField]
    private LayerMask raycastThrowLayers;               // for throwing object towards a point (eg. on ground)

    private float raycastDistance = 1000f;
    private StarfishTap arCollider = null;              // object that raycast hit, previously called: capturedFlickable1
    private SplashWorldTapThrow vrCollider = null;      // object that raycast hit, previously called: capturedFlickable2

    [SerializeField]
    private ParticleSystem throwParticles;

    private bool throwOnRelease = true;                 // otherwise drag to release point

    private Camera mainCam;

    private void OnEnable()
    {
        GameEvents.OnTapOn += OnTapOn;
        GameEvents.OnTapMove += OnTapMove;
        GameEvents.OnTapOff += OnTapOff;
    }

    private void OnDisable()
    {
        GameEvents.OnTapOn -= OnTapOn;
        GameEvents.OnTapMove -= OnTapMove;
        GameEvents.OnTapOff -= OnTapOff;
    }

    private void Start()
    {
        // get Camera.main once only for efficiency
        mainCam = Camera.main;
    }

    // event handlers for touch on/ move / off events

    private void OnTapOn(Vector2 screenPosition, int touchCount)
    {
        RaycastToTap(screenPosition); // previously called TapRaycast
    }

    private void OnTapMove(Vector2 screenPosition, int touchCount, Vector2 direction, TimeSpan timeFromStart)
    {
        RaycastToDrag(screenPosition); // previously called DragToRaycast
    }

    private void OnTapOff(Vector2 screenPosition, int touchCount, Vector2 direction, TimeSpan timeFromStart)
    {
        RaycastToThrowOrIdle(screenPosition); // previously called ThrowToRaycast
    }

    // raycast for target collider layer from touch screenPosition and Flick component
    // activate the hit Flick (eg. animation)
    private void RaycastToTap(Vector2 screenPosition)
    {
        RaycastHit orbHitInfo = DoRaycast(screenPosition, raycastOrbTapLayers);

        if (orbHitInfo.transform != null)
        {
            Debug.Log($"TapRaycast HIT ORB: {orbHitInfo.transform.name}");
            var orb = orbHitInfo.transform.GetComponent<OrbTap>();

            if (orb != null)
            {
                orb.Tap();
            }
        }
        else
        {
            // raycast to detect what was touched
            RaycastHit hitInfo = DoRaycast(screenPosition, raycastTapLayers);

            if (hitInfo.transform != null)
            {
                Debug.Log($"TapRaycast HIT: {hitInfo.transform.name}");
                arCollider = hitInfo.transform.GetComponent<StarfishTap>();
                vrCollider = hitInfo.transform.GetComponent<SplashWorldTapThrow>();

                if (vrCollider != null)
                {
                    vrCollider.Tap();
                }
                else if (arCollider != null)
                {
                    arCollider.Tap();
                }
            }
        }
    }

    // raycast for throw 'destination' layer from touch screenPosition, drag hitFlick to hitPoint
    private void RaycastToDrag(Vector2 screenPosition)
    {
        if (vrCollider == null)
            return;

        // raycast to detect what was touched
        RaycastHit hitInfo = DoRaycast(screenPosition, raycastDragLayers);

        if (hitInfo.transform != null)
        {
            Debug.Log($"DragToRaycast HIT: {hitInfo.transform.name}");
            vrCollider.Drag(hitInfo.point, false);       // hitFlick maintains its original y pos
        }
    }

    // raycast for throw 'destination' layer from touch screenPosition, throw hitFlick in direction of hitPoint
    // simply drag if still raycasting capturedFlickable
    private void RaycastToThrowOrIdle(Vector2 screenPosition)
    {
        if (arCollider == null)
            return;

        if (vrCollider == null)
            return;

        // raycast to detect what was touched
        RaycastHit hitInfo = DoRaycast(screenPosition, raycastThrowLayers);

        // drag if raycasting hitFlick, else throw in direction of hit point
        if (hitInfo.transform != null)
        {
            Debug.Log($"ThrowToRaycast HIT: {hitInfo.transform.name}");

            if (throwOnRelease)
            {
                if (hitInfo.transform == vrCollider.transform)   // still raycasting same flickable object
                {
                    vrCollider.Drag(hitInfo.point, true);
                    vrCollider.Release();           // drop down/shrink if in target zone, else return to start position
                }
                else
                {
                    if (throwParticles != null)
                    {
                        throwParticles.transform.position = hitInfo.point;
                        throwParticles.Play();
                    }

                    vrCollider.Throw(hitInfo.point);
                }
            }
            else
            {
                vrCollider.Drag(hitInfo.point, false);
                vrCollider.Release();           // drop down/shrink if in target zone, else return to start position
            }

            vrCollider = null;
        }
        else    // raycast didn't hit anything in raycastThrowLayers, so just drag or snap back
        {
            Debug.Log($"ThrowToRaycast NO HIT!");
            //var touchOffWorldPos = mainCam.ScreenToWorldPoint(screenPosition);

            //capturedFlickable.Throw(touchOffWorldPos);
            vrCollider.Release();           // drop down/shrink if in target zone, else return to start position
            vrCollider = null;
        }
    }

    private RaycastHit DoRaycast(Vector2 touchPosition, LayerMask layers)
    {
        Ray ray = mainCam.ScreenPointToRay(touchPosition);

        // raycast to detect what was touched
        RaycastHit hitInfo;
        Physics.Raycast(ray, out hitInfo, raycastDistance, layers);

        return hitInfo;
    }
}