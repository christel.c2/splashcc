using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.EventSystems;

public class CanvasSlider : MonoBehaviour, IPointerDownHandler, IPointerUpHandler 
{
    public VideoPlayer videoMainMV;
    public Slider getSlider;
    bool slide = false;
        
    private void Awake()
    {
        getSlider = GetComponent<Slider>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void OnPointerDown(PointerEventData a)
    {
        slide = true;
        Debug.Log("Canvas Slider: On Pointer Down");
    }

    public void OnPointerUp(PointerEventData a)
    {
        if (videoMainMV.isPlaying)
        {
            float frame = (float)getSlider.value * (float)videoMainMV.frameCount;
            videoMainMV.frame = (long)frame;
            slide = false;
            Debug.Log("Canvas Slider to: " + videoMainMV.frame + "On Pointer Up");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!slide && videoMainMV.isPlaying)
        {
            getSlider.value = (float)videoMainMV.frame / (float)videoMainMV.frameCount;
        }
    }
}
