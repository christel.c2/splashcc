using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class OpenerVideo : MonoBehaviour
{
    [SerializeField]
    private VideoPlayer canvasVideo;

    [SerializeField]
    private GameObject toDisable;

    [SerializeField]
    private GameObject toEnable;

    [SerializeField]
    private Slider getSlider;

    [SerializeField]
    private AudioSource stopMusic;

    [SerializeField]
    private AudioSource playMusic;

    bool slide = false;

    private void Awake()
    {
        getSlider = GetComponent<Slider>();
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!slide && canvasVideo.isPlaying)
        {
            getSlider.value = (float)canvasVideo.frame / (float)canvasVideo.frameCount;
        }

        if (canvasVideo.frame == (long)canvasVideo.frameCount - 1)
        {
            toDisable.SetActive(false); // disable game object altogether after video finish playing
            toEnable.SetActive(true);

            if (stopMusic != null)
            {
                stopMusic.Stop();
            }

            if (playMusic != null)
            {
                playMusic.Play(0);
            }

            Debug.Log("Video Completed: " + canvasVideo.name);
        }
    }
}

